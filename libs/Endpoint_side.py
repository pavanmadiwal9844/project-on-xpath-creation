
import socket
import select
import struct
import sys
import time
import threading
import os
import sys 
import time

def handle(buffer):
    return buffer

#function for transfer of data
def transfer(src, dst, direction,type1):
    src_name = src.getsockname()
    src_address = src_name[0]   
    src_port = src_name[1]
    dst_name = dst.getsockname()
    dst_address = dst_name[0]
    dst_port = dst_name[1]
    

    while True:
        buffer = src.recv(0x400)
        if len(buffer) == 0:
            print("[-] No data received! Breaking...")
            os._exit(0)
            break

        if direction:
            print(f"[+] {src_address}:{src_port} >>> {dst_address}:{dst_port} [{len(buffer)}] \n")
        else:
            print(f"[+] {dst_address}:{dst_port} <<< {src_address}:{src_port} [{len(buffer)}] \n")

        
        
        dst.send(handle(buffer))
        print("hopefully sent")
    

    os._exit(0)
    print(f"[+] Closing connections! [{src_address}:{src_port}]")
    src.shutdown(socket.SHUT_RDWR)
    src.close()
    print(f"[+] Closing connections! [{dst_address}:{dst_port}]")
    dst.shutdown(socket.SHUT_RDWR)
    dst.close()


    




def server(port,type1):

    
    count =0
    starting = True 

    while starting:
    
        try :
            print("startecd")
            #connecting to server socket
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

            server_socket.connect(("127.0.0.1",7008))
            starting=False
            

            #attaching to 
            target_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            target_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


            msg = server_socket.recv(1024)
            # print(type(msg.decode()))
            if msg.decode()=="connected":
                # print("client connected")

                target_socket.connect(("127.0.0.1",5009))

                agent_listening = True
                check = True
        
                
                while True:    
                    print("stuck in while loop")
                    
                    while agent_listening:
                        print("going inside try")

                    
                        try:
                            
                            
                            agent_listening =False
                            
                            print("agent_listening : {}".format(agent_listening))
                            s = threading.Thread(target=transfer, args=(
                                server_socket, target_socket, False,type1))
                            r = threading.Thread(target=transfer, args=(
                                target_socket, server_socket, True,type1))
                            print("started")
                            check=False
                            s.start()
                            r.start()

                        except Exception as e:
                            print(e)
                            print("Agent not Listening 5009")
                            server_socket.close()
                            target_socket.close()
                            agent_listening=True

                

               

                    

            else:
                server_socket.close()
                target_socket.close()
                starting=False
                


        except Exception as e :
            server_socket.close()
            target_socket.close()
            starting=False
            


def main():
    server(7008,"python3")



if __name__ == "__main__":
    main()