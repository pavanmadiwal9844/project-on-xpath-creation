const DEBUGGER_PROTOCOL_VERSION="1.3";
 class Debugger {
  constructor(tabId) {
    this.tabId = tabId
    this.attach = this.attach.bind(this)
    this.detach = this.detach.bind(this)
    this.sendCommand = this.sendCommand.bind(this)
    this.getDocument = this.getDocument.bind(this)
    this.querySelector = this.querySelector.bind(this)
    
  }
  attach() {
    return new Promise(res => {
      if (!this.connection) {
        const target = { tabId: this.tabId }
        chrome.debugger.attach(target, DEBUGGER_PROTOCOL_VERSION, () => {
          this.connection = target
          res(this.connection)
        })
      } else {
        res(this.connection)
      }
    })
  }

  detach() {
    return new Promise(res => {
      if (this.connection) {
        chrome.debugger.detach(this.connection, () => {
          this.connection = undefined
          res()
        })
      } else {
        res()
      }
    })
  }

  sendCommand(command, params) {
    return new Promise((res, rej) => {
      chrome.debugger.sendCommand(this.connection, command, params, r => {
        if (chrome.runtime.lastError && chrome.runtime.lastError.message) {
          rej(new Error(chrome.runtime.lastError.message))
        } else {
          res(r)
        }
      })
    })
  }

  getDocument() {
    return this.sendCommand('DOM.getDocument', {
      pierce: true,
      depth: -1,
    }).then(doc => doc.root)
  }

  querySelector(selector, nodeId) {
    return this.sendCommand('DOM.querySelector', {
      selector,
      nodeId,
    }).then(res => res && res.nodeId)
  }

}
async function getDocNodeId(e) {
    try {
        const t = await e.getDocument()
        //   , n = this.getFrameIds();
        // if (n) {
        //     const e = Object(d["a"])(t)
        //       , r = this.getCdpFrame(e, n);
        //     if (r.documentNodeId)
        //         return r.documentNodeId;
        //     throw new Error("frame not found")
        // }
        return t.nodeId
    } catch (e) {
        console.log(e);
    }
}
async function doSendKeys(e, t,tabId) {
    // if ("Chrome" !== u["d"].browser.name || -1 === t.indexOf("${KEY_ENTER}"))
    //     // return this.sendMessage("sendKeys", e, t, n);
    //     return browser.sendMessage({sqaplayback:sendKeys,eval:e,value:t})
    {
        const r = new Debugger(tabId)
          , i = async e=>{
            await r.sendCommand("DOM.focus",{nodeId:e}),
            await r.sendCommand("Input.dispatchKeyEvent",{type:"keyDown",windowsVirtualKeyCode:13,key:"Enter",code:"Enter",text:"\r"}),
            await r.sendCommand("Input.dispatchKeyEvent",{type:"keyUp",windowsVirtualKeyCode:13,key:"Enter",code:"Enter",text:"\r"}),
            await new Promise(e=>setTimeout(e,500))
        }
        ;
        try {
            await r.attach();
            // const o = await this.convertToQuerySelector(e)
            const o=e
              , s = await r.querySelector(o,await this.getDocNodeId(r))
              , a = t.split("${KEY_ENTER}");
            let c = 0;
            for (; c < a.length; )
                a[c] ,
                c < a.length - 1 && await i(s),
                c++;
            return await r.detach(),
            {
                result: "success"
            }
        } catch (i) {
            if (await r.detach())
            return browser.tabs.sendMessage(tabId,{name:"error"});
            throw i

        }
    }
}
browser.runtime.onMessage.addListener((request, sender, sendResponse)=>{
  if( request.debug){
          doSendKeys(request.eval,request.value,sender.tab.id);
        }
      
   });