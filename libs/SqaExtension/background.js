window.browser = (function () {
  return window.msBrowser ||
    window.browser ||
    window.chrome;
})();
var baseUrl = "http://localhost:4012";
var frames = [];
var tab = '';
var framelocation = '';
var flag1 = false;
var WebHandler = function () {
  this.windowId = undefined;
  this.tabId == undefined;
  this.response = undefined;
  this.count = 0;
  this.rsend = undefined;
  this.tabs = [];

  WebHandler.prototype.status = () => {
    return $.ajax({
      type: "POST",
      url: baseUrl + "/v1/webstatus",
      data: JSON.stringify({ "document": "completed" }),
      contentType: "application/json",
      dataType: "json",
      async: false
    }).responseJSON;
  }

  WebHandler.prototype.sendStep = (request) => {
    $.ajax({
      type: "POST",
      url: baseUrl + "/v1/child",
      data: JSON.stringify(request),
      contentType: "application/json",
      dataType: "json",
      async: false
    });
  }
  WebHandler.prototype.TabHandler = () => {
    browser.tabs.query({ "active": true }, (tabs) => {
      Handler.request = Handler.status().name;
      if (Handler.request == "recorder") {
        for (let tab of tabs) {
          browser.tabs.sendMessage(tab.id, { name: 'getframes' });
          browser.tabs.sendMessage(tab.id, { name: "recorder", sqarecord: true });
        }
      }
    });
  }
}
var url = "";
browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.document == "completed") {
    if (!Handler.tabs.includes(sender.tab.id)) {
      Handler.tabs.push(sender.tab.id);
    }
    Handler.TabHandler();
  }
  if (request.document == 'frames') {
    flag2 = false;
    let index = Handler.tabs.indexOf(sender.tab.id).toString().trim();
    if (!frames[index])
      frames[index] = [];
    for (let frl of frames[index])
      if (frl.framelocation === request.frames.frameLocation) {
        frl.frame = request.frames.array;
        flag2 = true;
      }
    if (!flag2)
      frames[index].push({ framelocation: request.frames.frameLocation, frame: request.frames.array })
  }
  if (request.child_msg == "child") {
    flag = false;
    if (url != sender.tab.url) {
      url = sender.tab.url;
      framelocation = "";
    }
    let retab = Handler.tabs.indexOf(sender.tab.id).toString().trim();
    if (tab != retab) {
      tab = retab;
      let res = { name: "tab", text: 'tab', attributes: [], Value: tab, action: 'switchTowindow' };
      Handler.sendStep({ child: res });
    }
    for (let ch of request.child.attributes) {
      if (ch.name.toLowerCase() == 'framelocation') {
        if (framelocation != ch.value) {
          let res = { name: "SwitchtoParent", text: 'SwitchtoParent', attributes: [], action: 'switchToparent' };
          Handler.sendStep({ child: res });
          framelocation = ch.value;
          flag = true;
        }
        break;
      }
    }
    if (flag) {
      let iframe = {};
      let src = request.child.attributes.filter((re) => {
        if (re.name === 'srcPath')
          return re.value;
      });
      let fsrc;
      if (frames.length != 0)
        for (let fr of frames[tab]) {
          if (fr.framelocation === framelocation.slice(0, -2)) {
            for (let frs of fr.frame) {
              for (let attr of frs.attributes) {
                if (attr.name === 'src' && src.length != 0) {
                  let search = decodeURIComponent(src[0].value).split("&");
                  let fl = false;
                  for (let uri of search) {
                    if (!attr.value.includes(uri))
                      fl = true;
                  }
                  if (!fl) {
                    fsrc = frs;
                    break;
                  }
                }
              }
            }
            if (fsrc) {
              iframe = fsrc;
            }
            else
              iframe = fr.frame[framelocation.slice(-1)];
            break;
          }
        }
      iframe.action = "switchToframe";
      Handler.sendStep({ child: iframe })
      Handler.sendStep(request)
    }
    else {
      Handler.sendStep(request)
    }
    // }
  }
  if (request.play_back) {
    if (request.play_back == "recived")

      console.log("taking screen shot");
    if (request.captureScreenshot) {
      browser.tabs.captureVisibleTab(Handler.windowId, { "format": "png", quality: 100 }, function (baseUrl) {
        console.log(baseUrl);
        let step = {}, data = {};
        step.screenshot = baseUrl;
        step.result = 0;
        step.tcSeq = 1;
        step.itr = 1;
        request.step = step;
        setTimeout(() => {
          Handler.playback(request);
        }, 1000);
      });
    }
    else {
      setTimeout(() => {
        Handler.playback(request);
      }, 1000);
    }
  }
});
var Handler = new WebHandler();
