window.browser = (function () {
    return window.msBrowser ||
      window.browser ||
      window.chrome;
  })();
  
var EventManager=function()
{
    this.captureScreenshot= false;
  this.rec_ord=false;
  this.object_picker=false;
  //this.record={};
  this.recorder;
  this.child_object={};
  this.frameLocation="";
  this.keydown=false;
  this.mousedown=false;
  this.element={};
  this.scrollelement={};
  this.dblCtrlKey=0;
  this.freeze = false;
  this.tempvalue="";
  this.tempbool=false;
  this.index=0;
  this.sqa_exec=false;
  this.sqatest=null;
  this.browserName="";
  this.keys;
  this.inputTypes = [
      'text',
      'password',
      'file',
      'datetime',
      'datetime-local',
      'date',
      'month',
      'time',
      'week',
      'number',
      'range',
      'email',
      'url',
      'search',
      'tel',
      'color',
  ];
  this.inputcheck=["checkbox","radio"];
}
EventManager.prototype.DomReady=()=>
{
  var chkReadyState = setInterval(()=>{
      if (document.readyState == "complete") {
          if(window.location.href!="about:blank")
          {
              browser.runtime.sendMessage({document:"completed"});
              clearInterval(chkReadyState);
          }
      }
  },100)
  browser.runtime.onMessage.addListener(Sqa.BrowserEvent);
  Sqa.getframe();
  Sqa.getbrowserName();
  
}
EventManager.prototype.BrowserEvent=(request, sender, sendResponse)=>
{
          if(request.name=="recorder"){
              Sqa.rec_ord=true;
              if(Sqa.frameLocation=="parent")
                  browser.runtime.sendMessage({sqapagename:document.title});
                  
          }
          if(request.name=="objectpicker")
          {
              Sqa.object_picker=true;
              if(Sqa.frameLocation=="parent")
                  alert("Start recording ......");
          }
          if(request.name=="recorder_steps")
          {
              Sqa.rec_ord=true;
              if(Sqa.frameLocation=="parent")
                  alert("Start recording ......");
                  browser.runtime.sendMessage({sqapagename:document.title});
          }
          if( request.name=="Playback")
          {
              Sqa.index=request.index;
              if(request.testdata)
              {
                Sqa.captureScreenshot=request.captureScreenshot ;
                  Sqa.play(request.testdata);
              }
          }
      if(request.name=="error"){
          browser.runtime.sendMessage({play_back:"wait",index:Sqa.index})
          return;
      }
  Sqa.Attch();
}
EventManager.prototype.getframe=()=>
{
  Sqa.frameLocation="";
  let currentWindow =this.window;
  let currentParentWindow;
 
  while (currentWindow !== window.top) {
    currentParentWindow = currentWindow.parent;
    if (!currentParentWindow.frames.length) {
      if(currentWindow===currentParentWindow)
      break;
       else {
             Sqa.frameLocation+=":"+0;
              break;
       }
    }
    for (let idx = 0; idx < currentParentWindow.frames.length; idx++) {
      const frame = currentParentWindow.frames[idx];

      if (frame === currentWindow) {
        const index = (idx !== 0) ? idx - 1 : idx;
        Sqa.frameLocation = ":" + index + Sqa.frameLocation;
        currentWindow = currentParentWindow;
        break;
      }
    }
  }
  Sqa.frameLocation= "parent" + Sqa.frameLocation;
}

EventManager.prototype.genratecss=(element)=>
{
  if(element.id && element.id!="")
  {
      return ""+element.tagName.toLowerCase()+"[id='"+element.id+"']";
  }
  else if(element.name && element.name!=""){
      return ""+element.tagName.toLowerCase()+"[name='"+element.name+"']";
  }
  else if(element.classList[0] && element.class!=""){
      return ""+element.tagName.toLowerCase()+"."+element.classList[0]+"";
  }
  else 
  return null;
}

EventManager.prototype.genratepositioncss=(element)=>
{
  let parent,index,classes=[],className="";
  index=$(element).index()+1;
  parent=element.parentNode;
  if(parent.classList.length!=0){
      classes=parent.classList;
      for(let i=0;i<classes.length;i++)
      {
          className+="."+classes[i];
      }
      return ""+parent.tagName.toLowerCase()+className+">"+element.tagName.toLowerCase()+":nth-child("+index+")";   
  }
  else{
      return Sqa.genratepositioncss(parent)+">"+element.tagName.toLowerCase()+":nth-child("+index+")";
  }
}
EventManager.prototype.get_textIndicate=(element)=>
{
  if(element.tagName.toLowerCase()!="input"&&element.tagName.toLowerCase()!="select")
  {
     if(element.innerText!="")
     {
         let count=Sqa.gettextCount(element.innerText,element);
         if(count==1)
             return element.innerText;
         if (count > 1) 
         {
            let val = Sqa.findtextWithIndex(element.innerText, element);
             return val;
         }
     }   
     else
         return null;
  }
  else
     return null;

}
EventManager.prototype.gettextCount=(val,node)=>
{
  let result=document.getElementsByTagName(node.tagName);
      let elements=[];
      for(let ele of result)
      {
          if(ele.innerText==val)
              {
                  elements.push(ele);
              }
      }
      return elements.length;
}
EventManager.prototype.findtextWithIndex=(val,node)=>
{
  let index=0;
  let elements=[];
  let nodex;
  if(node.classList.length!=0){
      nodex=document.getElementsByClassName(node.classList[0]);
      for(let ele of nodex)
      {
          if(ele.innerText==val)
              elements.push(ele);
          else 
              break;
      }
      for(let ele of elements)
      {
          index++;
          if(ele.isSameNode(node))
              return val+":"+node.classList[0]+":"+index;     
      }
   
  }
  else return null;
}

EventManager.prototype.Attch=()=>
{
  if(Sqa.object_picker || Sqa.rec_ord)
  {
      let form=this.window.document.getElementsByTagName("form");
      if(form.length!=0)
      {
          for(let i=0;i<form.length;i++)
          {
          form[i].autocomplete="off";
          form[i].reset();
          }
      }
  }
  if(Sqa.object_picker)
  {
      this.window.document.addEventListener("mouseover",Sqa.mouseover,true);
      this.window.document.addEventListener("click",Sqa.objectpickerClick,true);
  }
  if(Sqa.rec_ord)
  {
      this.window.document.addEventListener("click",Sqa.Click,true);
      this.window.document.addEventListener("scroll",Sqa.scroll,true);
      this.window.document.addEventListener("mousemove",Sqa.mousemove,true);
      this.window.document.addEventListener("mouseup",Sqa.mouseup,true);
      this.window.document.addEventListener("change",Sqa.change,true);
      this.window.document.addEventListener("keydown",Sqa.Event_keydown,true);
      this.window.document.addEventListener("keydown",Sqa.validBox,true);
      this.window.document.addEventListener("dblclick",Sqa.dbClick,true);
      this.window.document.addEventListener("focusout",Sqa.focusout,true);
  }
}
EventManager.prototype.mouseover=()=>
{
  $("*").removeClass("object-picker-highlight");
  $(event.target).addClass("object-picker-highlight");
  event.stopPropagation();
  if ($(event.target).attr('href')) {
      var obj = {};
      obj["element"] = $(event.target); 
      obj["oldHref"] = $(event.target).attr('href');
      $(event.target).removeAttr("href"); 
  } else if ($(event.target.parentElement).attr("href")) {
      var obj = {};
      obj["element"] = $(event.target.parent); 
      obj["oldHref"] = $(event.target).attr('href');
      $(event.target.parentElement).removeAttr("href");
  }
}
EventManager.prototype.objectpickerClick=()=>
{
      event.stopPropagation();
      event.preventDefault();
      let record_element=event.target;
      if(($(event.target).parents("svg")).length!=0){
          let c=$(event.target).parents("svg");
          record_element=c[0].parentNode;
      }
      if(event.target.tagName.toLowerCase()=="svg"){
          record_element=event.target.parentNode;
      }
      Sqa.child_object={};
      Sqa.child_object=Sqa.createChildren(record_element);
      let css={},cssIndex={},innerText={};
      css.name="css";
      css.value=Sqa.genratecss(record_element);
      if(css.value!=null)
      Sqa.child_object.attributes.push(css);
      cssIndex.name="cssIndex";
      cssIndex.value=Sqa.genratepositioncss(record_element);
      Sqa.child_object.attributes.push(cssIndex);
      innerText.name="innerText";
      innerText.value=Sqa.get_textIndicate(record_element);
      if(innerText.value!=null)
          Sqa.child_object.attributes.push(innerText);
      let location={},tag={};
      location.name="FrameLocation";
      location.value=Sqa.frameLocation;
      Sqa.child_object.attributes.push(location);
      tag.name="TagName";
      tag.value=record_element.tagName.toLowerCase();
      Sqa.child_object.attributes.push(tag);
      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"objectpicker"});
}
EventManager.prototype.Click=()=>
{
  let record_element=event.target;
      if(($(event.target).parents("svg")).length!=0){
          let c=$(event.target).parents("svg");
          record_element=c[0].parentNode;
      }
      if(event.target.tagName.toLowerCase()=="svg"){
          record_element=event.target.parentNode;
      }
      Sqa.child_object={};
      Sqa.child_object=Sqa.createChildren(record_element);
      let css={},cssIndex={},innerText={};
      css.name="css";
      css.value=Sqa.genratecss(record_element);
      if(css.value!=null)
          Sqa.child_object.attributes.push(css);
      cssIndex.name="cssIndex";
      cssIndex.value=Sqa.genratepositioncss(record_element);
      Sqa.child_object.attributes.push(cssIndex);
      innerText.name="innerText";
      innerText.value=Sqa.get_textIndicate(record_element);
      if(innerText.value!=null)
          Sqa.child_object.attributes.push(innerText);
      let location={},tag={};
      location.name="FrameLocation";
      location.value=Sqa.frameLocation;
      Sqa.child_object.attributes.push(location);
      tag.name="TagName";
      tag.value=record_element.tagName.toLowerCase();
      Sqa.child_object.attributes.push(tag);
      Sqa.child_object.action="Click";
      if(Object.entries(Sqa.scrollelement).length!=0){
          browser.runtime.sendMessage({child:Sqa.scrollelement,child_msg:"child"});
          Sqa.scrollelement={};
      }
      switch(record_element.tagName.toLowerCase()){
          case "input":
                  Sqa.child_object.type=record_element.type;
                  if(Sqa.inputcheck.indexOf(record_element.type)>=0){
                      let checked={};
                      checked.name="checked";
                      checked.value=record_element.checked;
                      Sqa.child_object.attributes.push(checked);
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                  }
                  else{
                      if((Sqa.keydown && record_element.type=="submit")||Sqa.mousedown)
                      {   
                          return;
                      }
                      if(event.target.id!="sqa_overlay"&&event.target.id!="sqaid"&&record_element.tagName.toLowerCase()!="body")
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                  }
                  break;
          case "button":
                      if((Sqa.keydown && record_element.type=="submit")||Sqa.mousedown)
                      {   
                          return;
                      }
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                  break;
          default:
          if(event.target.id!="sqa_overlay"&&event.target.id!="sqaid"&&record_element.tagName.toLowerCase()!="body")
          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
      }
      
}
EventManager.prototype.scroll=()=>
{
  if(event.target!=document){
      let record_element=event.target;
      Sqa.mousedown=false;
      Sqa.child_object={};
      Sqa.child_object=Sqa.createChildren(record_element);
      let css={},cssIndex={};
      css.name="css";
      css.value=Sqa.genratecss(record_element);
      if(css.value!=null)
      Sqa.child_object.attributes.push(css);
      cssIndex.name="cssIndex";
      cssIndex.value=Sqa.genratepositioncss(record_element);
      Sqa.child_object.attributes.push(cssIndex);
      let location={},tag={},scrollx={},scrolly={},windowscroll={};
      location.name="FrameLocation";
      location.value=Sqa.frameLocation;
      Sqa.child_object.attributes.push(location);
      tag.name="TagName";
      tag.value=record_element.tagName.toLowerCase();
      Sqa.child_object.attributes.push(tag);
      Sqa.child_object.action="scrollIntoView";
      scrollx.name="scrollx";
      scrollx.value=record_element.scrollLeft;
      scrolly.name="scrolly";
      scrolly.value=record_element.scrollTop;
      windowscroll.name="windowscroll";
      windowscroll.value=window.scrollY;
      Sqa.child_object.attributes.push(scrollx);
      Sqa.child_object.attributes.push(scrolly);
      Sqa.child_object.attributes.push(windowscroll);
      Sqa.scrollelement=Sqa.child_object;
   }

}
EventManager.prototype.mousemove=()=>
{
  if (Sqa.freeze) {
      event.stopPropagation();
      event.preventDefault();
  }
  else{
      
      Sqa.element.value=event.target;
      Sqa.element.pageX=event.pageX;
      Sqa.element.pageY=event.pageY;
     
  }
}
EventManager.prototype.mouseup=()=>
{
  let record_element=event.target;
       Sqa.mousedown=false;
       Sqa.child_object={};
       Sqa.child_object=Sqa.createChildren(record_element);
       let css={},cssIndex={},innerText={};
       css.name="css";
       css.value=Sqa.genratecss(record_element);
       if(css.value!=null)
       Sqa.child_object.attributes.push(css);
       cssIndex.name="cssIndex";
       cssIndex.value=Sqa.genratepositioncss(record_element);
       Sqa.child_object.attributes.push(cssIndex);
       innerText.name="innerText";
       innerText.value=Sqa.get_textIndicate(record_element);
       if(innerText.value!=null)
       Sqa.child_object.attributes.push(innerText);
       let location={},tag={};
       location.name="FrameLocation";
       location.value=Sqa.frameLocation;
       Sqa.child_object.attributes.push(location);
       tag.name="TagName";
       tag.value=record_element.tagName.toLowerCase();
       Sqa.child_object.attributes.push(tag);
       Sqa.child_object.action="Click";
       switch(record_element.tagName.toLowerCase()){
           case "li":
                      Sqa.mousedown=true;
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                   break;
       }
}
EventManager.prototype.change=()=>
{
  let record_element=event.target;
        Sqa.child_object={};
        Sqa.child_object=Sqa.createChildren(record_element);
           let css={},cssIndex={},innerText={};
          css.name="css";
          css.value=Sqa.genratecss(record_element);
          if(css.value!=null)
          Sqa.child_object.attributes.push(css);
          cssIndex.name="cssIndex";
          cssIndex.value=Sqa.genratepositioncss(record_element);
          Sqa.child_object.attributes.push(cssIndex);
          innerText.name="innerText";
          innerText.value=Sqa.get_textIndicate(record_element);
          if(innerText.value!=null)
              Sqa.child_object.attributes.push(innerText);
        let location={},tag={};
        location.name="FrameLocation";
        location.value=Sqa.frameLocation;
        Sqa.child_object.attributes.push(location);
        tag.name="TagName";
        tag.value=record_element.tagName.toLowerCase();
        Sqa.child_object.attributes.push(tag);
        switch(record_element.tagName.toLowerCase()){
            case "input":
                  Sqa.child_object.type=record_element.type;
                  if(Sqa.inputTypes.indexOf(record_element.type)>=0)
                  {  
                      if(Sqa.keydown){
                          return;
                      }
                      Sqa.child_object.action="EnterText";
                      let value={};
                      value.name="Value";
                      value.value=record_element.value;
                      Sqa.child_object.Value=record_element.value;
                      Sqa.child_object.attributes.push(value);
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                  }                        
                  
                break;
            case "select":
            if(event.target.id!="sqa_overlay"&&event.target.id!="sqaid"){
                  Sqa.child_object.action="SelectItemFromDropdown";
                  Sqa.recorder = Sqa.lookupElement(Sqa.getXpath(record_element));
                  Sqa.recorder.addListener("focus",Sqa.select_Read(),false);
                  let value={};
                  value.name="Value";
                  value.value=record_element.value;
                  Sqa.child_object.Value=record_element.value;
                  Sqa.child_object.attributes.push(value);
                  browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                   }
                  break;
            case "textarea":
                  Sqa.child_object.action="EnterText";
                  let value1={};
                  value1.name="Value";
                  value1.value=record_element.value;
                  Sqa.child_object.Value=record_element.value;
                  Sqa.child_object.attributes.push(value1);
                  browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                  break;
            default:
            browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
          }
}
EventManager.prototype.Event_keydown=()=>
{
  let record_element=event.target;
          Sqa.keydown=false;
          switch(record_element.tagName.toLowerCase()){
              case "input":
                      if(event.keyCode!=13 && event.keyCode!=38 && event.keyCode!=40)
                          Sqa.tempbool=false;
                      if(event.keyCode==13){
                          Sqa.keydown=true;
                          if(!Sqa.tempvalue)
                          {
                              Sqa.child_object={};
                              Sqa.child_object=Sqa.createChildren(record_element);
                              let css={},cssIndex={},innerText={};
                              css.name="css";
                              css.value=Sqa.genratecss(record_element);
                              if(css.value!=null)
                              Sqa.child_object.attributes.push(css);
                              cssIndex.name="cssIndex";
                              cssIndex.value=Sqa.genratepositioncss(record_element);
                              Sqa.child_object.attributes.push(cssIndex);
                              innerText.name="innerText";
                              innerText.value=Sqa.get_textIndicate(record_element);
                              if(innerText.value!=null)
                              Sqa.child_object.attributes.push(innerText);
                              let location={},tag={};
                              location.name="FrameLocation";
                              location.value=Sqa.frameLocation;
                              Sqa.child_object.attributes.push(location);
                              tag.name="TagName";
                              tag.value=record_element.tagName
                              Sqa.child_object.attributes.push(tag);
                              Sqa.child_object.action="EnterText";
                              let value={};
                              value.name="Value";
                              value.value=record_element.value;
                              Sqa.child_object.Value=record_element.value;
                              Sqa.child_object.attributes.push(value);
                              browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                              Sqa.tempbool=false;
                          }
                          Sqa.child_object={};
                          Sqa.child_object=Sqa.createChildren(record_element);
                          let css={},cssIndex={},innerText={};
                          css.name="css";
                          css.value=Sqa.genratecss(record_element);
                          if(css.value!=null)
                          Sqa.child_object.attributes.push(css);
                          cssIndex.name="cssIndex";
                          cssIndex.value=Sqa.genratepositioncss(record_element);
                          Sqa.child_object.attributes.push(cssIndex);
                          innerText.name="innerText";
                          innerText.value=Sqa.get_textIndicate(record_element);
                          if(innerText.value!=null)
                          Sqa.child_object.attributes.push(innerText);
                          let location={},tag={};
                          location.name="FrameLocation";
                          location.value=Sqa.frameLocation;
                          Sqa.child_object.attributes.push(location);
                          tag.name="TagName";
                          tag.value=record_element.tagName
                          Sqa.child_object.attributes.push(tag);
                          Sqa.child_object.action="Enter";
                          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                       }
                       if((event.keyCode==38||event.keyCode==40)&&event.target.value!=''&&!Sqa.tempbool)
                       {
                          if(Sqa.tempvalue!=event.target.value)
                              {
                                  Sqa.tempbool=true;
                                  Sqa.tempvalue=event.target.value;
                              }
                       
                       if(Sqa.tempbool){
                          Sqa.child_object={};
                          Sqa.child_object=Sqa.createChildren(record_element);
                          let css={},cssIndex={},innerText={};
                          css.name="css";
                          css.value=Sqa.genratecss(record_element);
                          if(css.value!=null)
                              Sqa.child_object.attributes.push(css);
                          cssIndex.name="cssIndex";
                          cssIndex.value=Sqa.genratepositioncss(record_element);
                          Sqa.child_object.attributes.push(cssIndex);
                          innerText.name="innerText";
                          innerText.value=Sqa.get_textIndicate(record_element);
                          if(innerText.value!=null)
                              Sqa.child_object.attributes.push(innerText);
                          let location={},tag={};
                          location.name="FrameLocation";
                          location.value=Sqa.frameLocation;
                          Sqa.child_object.attributes.push(location);
                          tag.name="TagName";
                          tag.value=record_element.tagName
                          Sqa.child_object.attributes.push(tag);                
                          Sqa.child_object.action="EnterText";
                          let value={};
                          value.name="Value";
                          value.value=Sqa.tempvalue;
                          Sqa.child_object.Value=Sqa.tempvalue;
                          Sqa.child_object.attributes.push(value);
                          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                       }
                      }
                       if(event.keyCode==40){
                          Sqa.keydown=true;
                          Sqa.child_object={};
                          Sqa.child_object=Sqa.createChildren(record_element);
                          let css={},cssIndex={},innerText={};
                          css.name="css";
                          css.value=Sqa.genratecss(record_element);
                          if(css.value!=null)
                          Sqa.child_object.attributes.push(css);
                          cssIndex.name="cssIndex";
                          cssIndex.value=Sqa.genratepositioncss(record_element);
                          Sqa.child_object.attributes.push(cssIndex);
                          innerText.name="innerText";
                          innerText.value=Sqa.get_textIndicate(record_element);
                          if(innerText.value!=null)
                          Sqa.child_object.attributes.push(innerText);
                          let location={},tag={};
                          location.name="FrameLocation";
                          location.value=Sqa.frameLocation;
                          Sqa.child_object.attributes.push(location);
                          tag.name="TagName";
                          tag.value=record_element.tagName
                          Sqa.child_object.attributes.push(tag);
                          Sqa.child_object.action="ArrowDown";
                          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                       }
                       if(event.keyCode==38){
                          Sqa.keydown=true;
                          Sqa.child_object={};
                          Sqa.child_object=Sqa.createChildren(record_element);
                          let css={},cssIndex={},innerText={};
                          css.name="css";
                          css.value=Sqa.genratecss(record_element);
                          if(css.value!=null)
                          Sqa.child_object.attributes.push(css);
                          cssIndex.name="cssIndex";
                          cssIndex.value=Sqa.genratepositioncss(record_element);
                          Sqa.child_object.attributes.push(cssIndex);
                          innerText.name="innerText";
                          innerText.value=Sqa.get_textIndicate(record_element);
                          if(innerText.value!=null)
                          Sqa.child_object.attributes.push(innerText);
                          let location={},tag={};
                          location.name="FrameLocation";
                          location.value=Sqa.frameLocation;
                          Sqa.child_object.attributes.push(location);
                          tag.name="TagName";
                          tag.value=record_element.tagName
                          Sqa.child_object.attributes.push(tag);
                          Sqa.child_object.action="ArrowUp";
                          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                       }
              break;
          }
}
EventManager.prototype.validBox=(event)=>
{
  if (Sqa.dblCtrlKey != 0 && event.keyCode == 16)
         {
          Sqa.freeze=true;
             if(!document.getElementById("sqa_overlay")){
                  $($("body")[0]).append("<div id='sqa_overlay' style=' position: fixed;display: none;width: 25%;top: 0;left: 0;right: 0;bottom: 0;background-color: rgba(0,0,0,0.5);z-index: 9999999999;cursor: pointer;'>"+
                                          "<select  id='sqaid'>"+
                                          "<option value='select'>select</option>"+
                                          "<option value='exists'>Exists</option>"+
                                          "<option value='validateText'>Validate Text</option>"+
                                          "<option value='notExists'>NotExists</option>"+
                                          "<option value='validatecheck'>Validate Check</option>"+
                                          "</select>"+      
                                          "</div>"
                      );
             }
             Sqa.on();
             
        } else {
            //   Sqa.dblCtrlKey = setTimeout('dblCtrlKey = 0;', 300);
                Sqa.dblCtrlKey = event.keyCode;
            }
            setTimeout(()=>{
                Sqa.dblCtrlKey = 0;
            },1000)
        let e=document.getElementById("sqa_overlay");
        $(e).focusout(()=>{
              Sqa.off();
          })
}
EventManager.prototype.dbClick=()=>
{
  Sqa.child_object={};
          let record_element=event.target;
          Sqa.child_object=Sqa.createChildren(record_element);
          let css={},cssIndex={},innerText={};
          css.name="css";
          css.value=Sqa.genratecss(record_element);
          if(css.value!=null)
          Sqa.child_object.attributes.push(css);
          cssIndex.name="cssIndex";
          cssIndex.value=Sqa.genratepositioncss(record_element);
          Sqa.child_object.attributes.push(cssIndex);
          innerText.name="innerText";
          innerText.value=Sqa.get_textIndicate(record_element);
          if(innerText.value!=null)
          Sqa.child_object.attributes.push(innerText);
          let location={},tag={};
          location.name="FrameLocation";
          location.value=Sqa.frameLocation;
          Sqa.child_object.attributes.push(location);
          tag.name="TagName";
          tag.value=record_element.tagName
          Sqa.child_object.attributes.push(tag);
          Sqa.child_object.action="dbclick";
          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
}

EventManager.prototype.focusout=()=>
{
  if(event.target.hasAttribute("contenteditable"))
          {
                  Sqa.child_object={};
                  let record_element=event.target;
                  Sqa.child_object=Sqa.createChildren(record_element);
                  let css={},cssIndex={},innerText={};
                  css.name="css";
                  css.value=Sqa.genratecss(record_element);
                  if(css.value!=null)
                      Sqa.child_object.attributes.push(css);
                  cssIndex.name="cssIndex";
                  cssIndex.value=Sqa.genratepositioncss(record_element);
                  Sqa.child_object.attributes.push(cssIndex);
                  innerText.name="innerText";
                  innerText.value=Sqa.get_textIndicate(record_element);
                  if(innerText.value!=null)
                      Sqa.child_object.attributes.push(innerText);
                  let location={},tag={};
                  location.name="FrameLocation";
                  location.value=Sqa.frameLocation;
                  Sqa.child_object.attributes.push(location);
                  tag.name="TagName";
                  tag.value=record_element.tagName
                  Sqa.child_object.attributes.push(tag);
                  Sqa.child_object.action="ContentText";
                  let value={};
                  value.name="Value";
                  value.value=record_element.innerHTML.trim();
                  Sqa.child_object.Value=record_element.innerHTML.trim();
                  Sqa.child_object.attributes.push(value);
                  browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
          }
          else 
          {
              return;
          } 
          
}

EventManager.prototype.on=()=>
{
  $("#sqa_overlay").css({
      position:"absolute",
      display: "block",
      left: Sqa.element.pageX,
      top: Sqa.element.pageY
  });
  $("#sqaid").val("select");
}

EventManager.prototype.validate=()=>
{
  let selectvalue=document.getElementById("sqaid").value;
    let record_element=Sqa.element.value;
    Sqa.keydown=false;
    Sqa.child_object={};
    Sqa.child_object=Sqa.createChildren(record_element);
    let css={},cssIndex={},innerText={};
    css.name="css";
    css.value=Sqa.genratecss(record_element);
    if(css.value!=null)
    Sqa.child_object.attributes.push(css);
    cssIndex.name="cssIndex";
    cssIndex.value=Sqa.genratepositioncss(record_element);
    Sqa.child_object.attributes.push(cssIndex);
    innerText.name="innerText";
    innerText.value=Sqa.get_textIndicate(record_element);
    if(innerText.value!=null)
    Sqa.child_object.attributes.push(innerText);
    let location={},tag={};
    location.name="FrameLocation";
    location.value=Sqa.frameLocation;
    Sqa.child_object.attributes.push(location);
    tag.name="TagName";
    tag.value=record_element.tagName
    Sqa.child_object.attributes.push(tag);     

    switch(selectvalue){
      case "validateText":
                          Sqa.child_object.action=selectvalue;
                          let value={};
                          value.name="Value";
                          if(record_element.innerText)
                          {
                              value.value=record_element.innerText;
                              Sqa.child_object.Value=record_element.innerText;
                              Sqa.child_object.attributes.push(value);
                              Sqa.freeze=false;
                              browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                          }
                          else
                          {
                              alert("No Text Exists !!!!!!!!!");
                              return;
                          }
                          break;
      case "validatecheck":
                          if(record_element.checked==undefined)
                          {
                              alert("Not a checkBox !!!!!!!!!");
                              return;
                          }
                          let checked={};
                          checked.name="checked";
                          checked.value=record_element.checked;
                          Sqa.child_object.attributes.push(checked);
                          Sqa.child_object.action=selectvalue;
                          Sqa.freeze=false;
                          browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});
                          break;
      default:            
                      Sqa.child_object.action=selectvalue;
                      Sqa.freeze=false;
                      browser.runtime.sendMessage({child:Sqa.child_object,child_msg:"child"});

    }
}

EventManager.prototype.off=()=>
{
  let ee= document.getElementById("sqa_overlay");
  
  if(ee.style.display!="none"){
          Sqa.validate();
          ee.style.display = "none";
      }
      Sqa.dblCtrlKey=0;
}

EventManager.prototype.lookupElement=(value)=>{
  var results=[];
 
  
   try{
      node=document.evaluate(value, document, null, XPathResult.ANY_TYPE,null);
  
  while (nodex = node.iterateNext()) {
                  
      results.push(nodex);
     
          }
      return results[0];


}
catch(err){
  return ;
}
}

EventManager.prototype.select_Read=()=>
{
      this.value=" ";
  }
EventManager.prototype.checkHtmlEntities=(str)=>
{


  var res =null;
  
  var exp1="&nbsp;";
  var exp2="&lt;";
  var exp3="&gt;";
  var exp4="&amp;";
  var exp5="&quot;";
  var exp6="&apos;";
  var exp7="&pound;";
  var exp8="&yen;";
  var exp9="&euro;";
  var exp10="&copy;";
  var exp11="&reg;";
  var exp12="&cent;";
  
  if(str.includes(exp1)){
  res = str.replace(exp1, ' ');
  }
  else if(str.includes(exp2)){
  res = str.replace(exp2, '<');
  }
  else if(str.includes(exp3)){
  res = str.replace(exp3, '>');
  }
  else if(str.includes(exp4)){
  res = str.replace(exp4, '&');
  }
  else if(str.includes(exp5)){
  res = str.replace(exp5, '"');
  }
  else if(str.includes(exp6)){
  res = str.replace(exp6, "'");
  }
  else if(str.includes(exp7)){
  res = str.replace(exp7, '£');
  }
  else if(str.includes(exp8)){
  res = str.replace(exp8, '¥');
  }
  else if(str.includes(exp9)){
  res = str.replace(exp9, '€');
  }
  else if(str.includes(exp10)){
  res = str.replace(10, '©');
  }
  else if(str.includes(exp11)){
  res = str.replace(exp11, '®');
  }
  else if(str.includes(exp12)){
      res = str.replace(exp12, '¢');
      }
  else if(str.includes("\n")){
      var check="";
      check=str.trim();
      var c=check.split("\n");
      res=c[0];
  }
  else if(str.includes("\t")){
      var check="";
      check=str.trim();
      var c=check.split("\t");
      res=c[0];
  }
  else {
      return str;
  }
  
  return res;
  
  }
EventManager.prototype.createChildren=(currentElem)=>
  {
      var child = {};
      var data = [];
     
      if($(currentElem).text())
          {   
              child.text=$(currentElem).text().substr(1,6)+"_"+$(currentElem).prop("tagName").toLowerCase() + "_"  + (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);
              
           
              }
          else
         {
         
          child.text=$(currentElem).prop("tagName").toLowerCase() + "_" + (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);
          }
     
         
      var xpathname = {},xpathclass={},xpathId={},xpathPlaceholder={},xpathText={},xpathsrc={},xpathNoattr={};
      xpathNoattr.name="XpathNoattr";
      xpathNoattr.value=Sqa.getXpathNoattr(currentElem);
      if(xpathNoattr.value!=null)
      data.push(xpathNoattr);
      xpathsrc.name="xpathSrc";
      xpathsrc.value=Sqa.getXpathSrc(currentElem);
      if(xpathsrc.value!=null)
      data.push(xpathsrc);
      xpathname.name = "xpathName"
      xpathname.value="";
      xpathname.value = Sqa.getXpathname(currentElem);
      if(xpathname.value!=null)
      data.push(xpathname);
      xpathclass.name = "xpathClass"
      xpathclass.value="";
      xpathclass.value = Sqa.getXpathClass(currentElem);
      if(xpathclass.value!=null)
      data.push(xpathclass);
      xpathId.name = "xpathId"
      xpathId.value="";
      xpathId.value = Sqa.getXpathbyId(currentElem);
      if(xpathId.value!=null)
      data.push(xpathId);
      xpathPlaceholder.name = "xpathPlaceholder"
      xpathPlaceholder.value="";
      xpathPlaceholder.value = Sqa.getXpathplaceholder(currentElem);
      if(xpathPlaceholder.value!=null)
      data.push(xpathPlaceholder);
      xpathText.name = "xpathText";
      xpathText.value="";
      xpathText.value = Sqa.getXpathtext(currentElem);
      if(xpathText.value!=null)
      data.push(xpathText);
      var placeholder;
      $(currentElem).each(function(){
          $.each(this.attributes, function() {
              // this.attributes is not a plain object, but an array
              // of attribute nodes, which contain both the name and value
              if(this.name=="placeholder"){
                  placeholder=this.value;
              }
              if (this.specified) {
                  var obj = {}
                  obj.name = this.name;
                 
                  obj.value=this.value;
                  data.push(obj)
              }
          });
      }); //end of each attribute
      child.attributes = data;
      child.placeholder=placeholder;
      return child;
  }
EventManager.prototype.getXpathbyId=(node)=>
{
      let attr;
      let attrs=node.attributes;
      let i=attrs.length;
      let tagName=node.tagName;
      let map={};
      let j=0;
      let val="";
      let count=-1;
      let realCount=0;
      while(j<i){
          attr=attrs[j];
          if ((attr.name!='style')&& (attr.value.indexOf('\'') < 0)){
              map[attr.name]=attr.value;
              realCount++;

          }
          j++;
      }
      for(let key in map){
          if(!map[key]){
              delete map[key];
          }
      }
      i=Object.keys(map).length;
      if((i==0)&&(realCount==0)){
          return null;

      }
      else{
          if (Sqa.isNotEmpty(map['id'])) {

              val = "//" + tagName + "[@id='" + map['id'] + "']";
              count = Sqa.getXpathCount(val,node);
      
              if (count == 1) {
                  return val;
              }
              if (count > 1) {
                  val = Sqa.findXpathWithIndex(val, node);
                  return val;
              } else {
                  return Sqa.findXpathWithIndex("//"+node.tagName,node);
              }
          
          }
          else{
              return null;
          }

      }
  }
EventManager.prototype.getXpathplaceholder=(node)=>
  {
      let attr;
      let attrs=node.attributes;
      let i=attrs.length;
      let tagName=node.tagName;
      let map={};
      let j=0;
      let val="";
      let count=-1;
      let realCount=0;
      while(j<i){
          attr=attrs[j];
          if ((attr.name!='style')&& (attr.value.indexOf('\'') < 0)){
              map[attr.name]=attr.value;
              realCount++;

          }
          j++;
      }
      for(let key in map){
          if(!map[key]){
              delete map[key];
          }
      }
      i=Object.keys(map).length;
      if((i==0)&&(realCount==0)){
          return null;

      }
      else{
          if (Sqa.isNotEmpty(map['placeholder'])) {

              val = "//" + tagName + "[@placeholder='" + map['placeholder'] + "']";
              count = Sqa.getXpathCount(val,node);
      
              if (count == 1) {
                  return val;
              }
              if (count > 1) {
                  val = Sqa.findXpathWithIndex(val, node);
                  return val;
              } else {
                  return Sqa.findXpathWithIndex("//"+node.tagName,node);
              }
          }
          else{
              return null;
          }

      }
  }
EventManager.prototype.getXpathname=(node)=>
  {
      let attr;
      let attrs=node.attributes;
      let i=attrs.length;
      let tagName=node.tagName;
      let map={};
      let j=0;
      let val="";
      let count=-1;
      let realCount=0;
      while(j<i){
          attr=attrs[j];
          if ((attr.name!='style')&& (attr.value.indexOf('\'') < 0)){
              map[attr.name]=attr.value;
              realCount++;

          }
          j++;
      }
      for(let key in map){
          if(!map[key]){
              delete map[key];
          }
      }
      i=Object.keys(map).length;
      if((i==0)&&(realCount==0)){
          return null;

      }
      else{
          if (Sqa.isNotEmpty(map['name'])) {

              val = "//" + tagName + "[@name='" + map['name'] + "']";
              count = Sqa.getXpathCount(val,node);
      
              if (count == 1) {
                  return val;
              }
              if (count > 1) {
                  val = Sqa.findXpathWithIndex(val, node);
                  return val;
              } else {
                  return Sqa.findXpathWithIndex("//"+node.tagName,node);
              }
          }
          else{
              return null;
          }

      }
  }
EventManager.prototype.getXpathClass=(node)=>
 {
  let attr;
  let attrs=node.attributes;
  let i=attrs.length;
  let tagName=node.tagName;
  let map={};
  let j=0;
  let val="";
  let count=-1;
  let realCount=0;
  while(j<i){
      attr=attrs[j];
      if ((attr.name!='style')&& (attr.value.indexOf('\'') < 0)){
          map[attr.name]=attr.value;
          realCount++;

      }
      j++;
  }
  for(let key in map){
      if(!map[key]){
          delete map[key];
      }
  }
  i=Object.keys(map).length;
  if((i==0)&&(realCount==0)){
      return null;

  }
  else{
      if (Sqa.isNotEmpty(map['class'])) {

          val = "//" + tagName + "[@class='" + map['class'] + "']";
          count = Sqa.getXpathCount(val,node);
  
          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return Sqa.findXpathWithIndex("//"+node.tagName,node);
          }
      }
      else{
          return null;
      }

  }
 }
EventManager.prototype.getXpathSrc=(node)=>
 {
  let attr;
  let attrs=node.attributes;
  let i=attrs.length;
  let tagName=node.tagName;
  let map={};
  let j=0;
  let val="";
  let count=-1;
  let realCount=0;
  while(j<i){
      attr=attrs[j];
      if ((attr.name!='style')&& (attr.value.indexOf('\'') < 0)){
          map[attr.name]=attr.value;
          realCount++;

      }
      j++;
  }
  for(let key in map){
      if(!map[key]){
          delete map[key];
      }
  }
  i=Object.keys(map).length;
  if((i==0)&&(realCount==0)){
      return null;

  }
  else{
      if (Sqa.isNotEmpty(map['src'])) {

          val = "//" + tagName + "[@src='" + map['src'] + "']";
          count = Sqa.getXpathCount(val,node);
  
          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return Sqa.findXpathWithIndex("//"+node.tagName,node);
          }
      }
      else{
          return null;
      }

  }
 }
EventManager.prototype.getXpathNoattr=(node)=>
 {
  let attr;
  let attrs = node.attributes;
  let i = attrs.length;
  let tagName = node.tagName;
  let map = {};
  let j = 0;
  let val = '';
  let count = -1;
  let realCount = 0;

  while (j < i) {
      attr = attrs[j];

      if ((attr.name != "style") && (attr.value.indexOf('\'') < 0)) {
          map[attr.name] = attr.value;
          realCount++;
      }
      j++;

  }
  for(let key in map){
      if(!map[key]){
          delete map[key];
      }
  }
  
  i = Object.keys(map).length;
  // no attributes
  if (i == 0) {

      let text = node.innerHTML;
      let oldText=text;
      text= Sqa.checkHtmlEntities(text);
      if ((text.length > 0) && (!text.includes("<"))
              && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {

          text= Sqa.checkHtmlEntities(text);
          if(oldText==text){
          val = "//" + tagName + "[text()='" + text + "']";
        }
          else{
                  val = "//" + tagName + "[contains(text(),'" + text + "')]";
          }
          count = Sqa.getXpathCount(val,node);
          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return Sqa.findXpathWithIndex("//"+node.tagName,node);
          }
      } else {

          return Sqa.findXpathWithIndex("//"+node.tagName,node);

      }

  } // end if i==0


        

  if (realCount == 0) { // undefined case

    let xp = Sqa.findXpathWithIndex("//" + node.tagName, node);
      return xp;
  } // end of realCount==0

 }
EventManager.prototype.getXpath=(node)=>
{
  let attr;
  let attrs = node.attributes;
  let i = attrs.length;
  let tagName = node.tagName;
  let map = {};
  let j = 0;
  let val = '';
  let count = -1;
  let realCount = 0;

  while (j < i) {
      attr = attrs[j];

      if ((attr.name != "style") && (attr.value.indexOf('\'') < 0)) {
          map[attr.name] = attr.value;
          realCount++;
      }
      j++;

  }
  for(let key in map){
      if(!map[key]){
          delete map[key];
      }
  }
  
  i = Object.keys(map).length;
  // no attributes
  if (i == 0) {

      let text = node.innerHTML;
      let oldText=text;
      text= Sqa.checkHtmlEntities(text);
      if ((text.length > 0) && (!text.includes("<"))
              && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {

          text= Sqa.checkHtmlEntities(text);
          if(oldText==text){
          val = "//" + tagName + "[text()='" + text + "']";
        }
          else{
                  val = "//" + tagName + "[contains(text(),'" + text + "')]";
          }
          count = Sqa.getXpathCount(val,node);
          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return Sqa.findXpathWithIndex("//"+node.tagName,node);
          }
      } else {

          return Sqa.findXpathWithIndex("//"+node.tagName,node);

      }

  } // end if i==0


        

  if (realCount == 0) { // undefined case

    let xp = Sqa.findXpathWithIndex("//" + node.tagName, node);
      return xp;
  } // end of realCount==0

  // Since Id going to be unique , no need to check further attributes

  if (Sqa.isNotEmpty(map['id'])) {

      val = "//" + tagName + "[@id='" + map['id'] + "']";
      count = Sqa.getXpathCount(val,node);

      if (count == 1) {
          return val;
      }
     delete map['id'];
  }

  // find which attribute combination gives the xpath count 1


 
  for ( var attribute in map) {
      
      if (map.hasOwnProperty(attribute)) {

          // if(attribute=='class'){
            //  check_attr=map[attribute];
              val ="//"+tagName;
            
          let text = node.innerHTML;
          let oldText=text;
              text=Sqa.checkHtmlEntities(text);
         
          if ((text.length > 0) && (!text.includes("<"))
                  && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {
              if(oldText==text){
              val = val + "[text()='" + text + "']";
              }
              else{
                  val = val + "[contains(text(),'" + text + "')]";
              }
          }
  
      
          count = Sqa.getXpathCount(val,node);

          if (count == 1) {
              return val;
          }
          else{
              val = "//" + tagName + "[@" + attribute + "='" + map[attribute] + "']";
      
      let text = node.innerHTML;
      let oldText=text;
          text= Sqa.checkHtmlEntities(text);
     
      if ((text.length > 0) && (!text.includes("<"))
              && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {
          if(oldText==text){
          val = val + "[text()='" + text + "']";
          }
          else{
              val = val + "[contains(text(),'" + text + "')]";
          }
      }

  
      count = Sqa.getXpathCount(val,node);

      if (count == 1) {
          return val;
      }
      
      if (count > 1) {
          val = Sqa.findXpathWithIndex(val, node);
          return val;
      } else {
          return "No Unique Identifiers found";
      }
          }
          
       }
  }

}
EventManager.prototype.getXpathtext=(node)=>
{
  let attr;
  let attrs = node.attributes;
  let i = attrs.length;
  let tagName = node.tagName;
  let map = {};
  let j = 0;
  let val = '';
  let count = -1;
  let realCount = 0;

  while (j < i) {
      attr = attrs[j];

      if ((attr.name != "style") && (attr.value.indexOf('\'') < 0)) {
          map[attr.name] = attr.value;
          realCount++;
      }
      j++;

  }
  for(let key in map){
      if(!map[key]){
          delete map[key];
      }
  }
  
  i = Object.keys(map).length;
  // no attributes
  if ((i == 0)&&(realCount) ) {

      let text = node.innerHTML;
      let oldText=text;
      text= Sqa.checkHtmlEntities(text);
      if ((text.length > 0) && (!text.includes("<"))
              && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {

          text= Sqa.checkHtmlEntities(text);
          if(oldText==text){
          val = "//" + tagName + "[text()='" + text + "']";
        }
          else{
                  val = "//" + tagName + "[contains(text(),'" + text + "')]";
          }
          count = Sqa.getXpathCount(val,node);
          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return Sqa.findXpathWithIndex("//"+node.tagName,node);
          }
      } else {
      

          return Sqa.findXpathWithIndex("//"+node.tagName,node);

      }

  } // end if i==0

  // Since Id going to be unique , no need to check further attributes

 

  // find which attribute combination gives the xpath count 1


 
  for ( var attribute in map) {
      
      if (map.hasOwnProperty(attribute)) {

          // if(attribute=='class'){
            //  check_attr=map[attribute];
              val ="//"+tagName;
            
          let text = node.innerHTML;
          let oldText=text;
              text=Sqa.checkHtmlEntities(text);
         
          if ((text.length > 0) && (!text.includes("<"))
                  && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {
              if(oldText==text){
              val = val + "[text()='" + text + "']";
              }
              else{
                  val = val + "[contains(text(),'" + text + "')]";
              }
          }
          else{
              return null;
          }
      
          count = Sqa.getXpathCount(val,node);

          if (count == 1) {
              return val;
          }
          if (count > 1) {
              val = Sqa.findXpathWithIndex(val, node);
              return val;
          } else {
              return null;
          }
      //     else{
      //         val = "//" + tagName + "[@" + attribute + "='" + map[attribute] + "']";
      
      // let text = node.innerHTML;
      // let oldText=text;
      //     text= checkHtmlEntities(text);
     
      // if ((text.length > 0) && (!text.includes("<"))
      //         && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {
      //     if(oldText==text){
      //     val = val + "[text()='" + text + "']";
      //     }
      //     else{
      //         val = val + "[contains(text(),'" + text + "')]";
      //     }
      // }

  
      // count = getXpathCount(val,node);

      // if (count == 1) {
      //     return val;
      // }
      
      // if (count > 1) {
      //     val = findXpathWithIndex(val, node);
      //     return val;
      // } else {
      //     return "No Unique Identifiers found";
      // }
      //     }
          
       }
  }

}
EventManager.prototype.isNotEmpty=(val)=>
{
  return (val === undefined || val == null || val.length <= 0) ? false : true;
}
EventManager.prototype.getXpathCount=(val,node)=>
{
  var nodes=null;
  nodes = document.evaluate(val, node.ownerDocument, null, XPathResult.ANY_TYPE,null);

 var results = [], nodex;

 while (nodex = nodes.iterateNext()) {
     results.push(nodex);
 }
 return results.length;


}   
EventManager.prototype.findXpathWithIndex=(val,node)=> 
{   
  var text = node.innerHTML;
  var oldText=text;
      text= Sqa.checkHtmlEntities(text);
  if ((text.length > 0) && (!text.includes("<"))
          && (text.indexOf('\'') == -1) && (text.indexOf('"') == -1)) {
              if(oldText==text){
          val=val+"[text()='"+text+"']";
          }
          else{
              val=val+"[contains(text(),'"+text+"')]";
          }

  }

 nodes = document.evaluate(val, node.ownerDocument, null, XPathResult.ANY_TYPE,null);

var  nodex;
var index = 0;
while (nodex= nodes.iterateNext()) {

 index++;

 if (nodex.isSameNode(node)) {

     return "(" + val + ")[" + index + "]";
 }

}


}
EventManager.prototype.play=(testdata)=>
{
  if(testdata.frameLocation===Sqa.frameLocation){
      Sqa.playBack(testdata);
 // Sqa.frameLocation="";
  }
}
EventManager.prototype.getElement=(obj,name)=>
{
  let ele=[];
  if(name=="cssIndex"){
      Sqa.sqatest=name;
      if(obj.cssIndex!=undefined){
         let ele1=(document.querySelectorAll(obj.cssIndex));
         if(ele1.length<2){
             ele.push(ele1[0]);
         if(ele[0]!=null)
            return ele[0];
        else
            return Sqa.getElement(obj,"id");
     }
       else{
         return Sqa.getElement(obj,"id");
      }
     }
     else{
         return Sqa.getElement(obj,"id");
     }
  }
  if(name=="css"){
      Sqa.sqatest=name;
      if(obj.css!=undefined){
         let ele1=(document.querySelectorAll(obj.css));
         if(ele1.length<2){
             ele.push(ele1[0]);
             if(ele[0]!=null)
                 return ele[0];
             else
                 return Sqa.getElement(obj,"cssIndex");
      }
      else{
         return Sqa.getElement(obj,"cssIndex");
     }
     }
     else{
         return Sqa.getElement(obj,"cssIndex");
     }
  }
  if(name=="innerText"){
     Sqa.sqatest=name;
     if(obj.innerText!=undefined){
        let ele1=(document.getElementsByTagName(obj.tag));
        let checkele=[],text,index=0;
        if(obj.innerText.includes(":")){
             text=obj.innerText.split(":")[0];
             index=obj.innerText.split(":")[1];
        }
        else
        {
             text=obj.innerText;
        }
        for(let check of ele1)
        {
             if(check.innerText==text)
             {
                 checkele.push(check);
             }
        }
        if(index==0)
         ele.push(checkele[0]);
        else
         ele.push(checkele[index-1]);
         if(ele[0]!=null)
            return ele[0];
        else
            return Sqa.getElement(obj,"css");
    }
    else{
        return Sqa.getElement(obj,"css");
    }
 }
 if(name=="id"){
     Sqa.sqatest=name;
     if(obj.id!=undefined){
         ele.push(document.getElementById(obj.id));
         if(ele[0]!=null)
             return ele[0];
         else 
             return Sqa.getElement(obj,"name");
     }
     else{
         return Sqa.getElement(obj,"name");
     }
 }
 if(name=="xpathId")
 {   Sqa.sqatest=name;
     if(obj.xpathid!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathid, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
              return Sqa.getElement(obj,"xpathName");
     }
     else{
         return Sqa.getElement(obj,"xpathName");
     }
 }
 if(name=="xpathName")
 {   Sqa.sqatest=name;
     if(obj.xpathname!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathname, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
             return Sqa.getElement(obj,"xpathClass");
     }
     else{
         return Sqa.getElement(obj,"xpathClass");
     }
 }
 if(name=="xpathClass")
 {   Sqa.sqatest=name;
     if(obj.xpathclass!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathclass, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
           return Sqa.getElement(obj,"xpathText");
     }
     else{
         return Sqa.getElement(obj,"xpathText");
     }
 }
 if(name=="xpathText")
 {   Sqa.sqatest=name;
     if(obj.xpathtext!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathtext, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
             return Sqa.getElement(obj,"xpathPlaceholder");
     }
     else{
         return Sqa.getElement(obj,"xpathPlaceholder");
     }
 }
 if(name=="xpathPlaceholder")
 {   Sqa.sqatest=name;
     if(obj.xpathplaceholder!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathplaceholder, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
         return Sqa.getElement(obj,"XpathNoattr");
     }
     else{
         return Sqa.getElement(obj,"XpathNoattr");
     }
 }
 if(name=="XpathNoattr")
 {   Sqa.sqatest=name;
     if(obj.xpathNoattr!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathNoattr, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
             return Sqa.getElement(obj,"xpathSrc");
     }
     else{
         return Sqa.getElement(obj,"xpathSrc");
     }
 }
 if(name=="xpathSrc")
 {   Sqa.sqatest=name;
     if(obj.xpathsrc!=undefined)
     {
         let result=this.window.document.evaluate(obj.xpathsrc, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
             return Sqa.getElement(obj,"Xpath");
     }
     else{
         return Sqa.getElement(obj,"Xpath");
     }
 }
 if(name=="Xpath")
 {   Sqa.sqatest=name;
     if(obj.Xpath!=undefined)
     {
         let result=this.window.document.evaluate(obj.Xpath, this.window.document, null, XPathResult.ANY_TYPE, null);
         thisResult = result.iterateNext();
         while (thisResult)
         {
             ele.push(thisResult);
             thisResult = result.iterateNext();
         }
         if(ele[0]!=null)
             return ele[0];
         else    
             return null;
     }
     else{
         return null;
     }
 }
 if(name=="name")
 {   Sqa.sqatest=name;
     if(obj.name!=undefined){
         ele=(document.getElementsByName(obj.name));
         if(ele.length>1)
             return Sqa.getElement(obj,"xpathId");
         else{
             if(ele[0]!=null)
                 return ele[0];
             else  
                 return Sqa.getElement(obj,"xpathId");
         }
     }
     else{
         return Sqa.getElement(obj,"xpathId");
     }

 }

}
EventManager.prototype.playBack=(obj)=>
{
  let elements=null;
  try{   
      
         elements = Sqa.getElement(obj,"css");
   if(elements==null){
       if(obj.action=="Not Exists")
          Sqa.sqa_exec=true;
       else
          Sqa.sqa_exec=false
  }
 else{
     console.log(elements);
      elements.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
      switch(obj.action){
          case 'Click':
                      if(obj.tag=="IMG"||obj.tag=="img"){
                                  let ev=document.createEvent("MouseEvents");
                                  ev.initEvent('mouseover',true);
                                      elements.dispatchEvent(ev);
                                      elements.focus();
                                      elements.click();
                                      Sqa.sqa_exec=true;
                          }
                      else if(obj.tag=="OPTION"||obj.tag=="option"){
                          let parent=elements.parentNode;
                          parent.value=elements.value;
                          parent.click();
                          Sqa.sqa_exec=true;
                          }
                      else if(obj.tag=="INPUT"||obj.tag=="input"){
                              if(Sqa.inputcheck.indexOf(obj.type)>=0){
                                  if(!elements.checked){
                                      elements.focus();
                                      elements.click();
                                  
                                  }
                                  else if(elements.checked==false){
                                      elements.focus();
                                      elements.click();
                                  } 
                                  else{
                                      if(obj.checked==false){
                                          elements.focus();
                                          elements.click();
                                      }
                                  }
                                  
                                  Sqa.sqa_exec=true;
                              }
                              else{
                                  elements.focus();
                                  elements.click();
                                  Sqa.sqa_exec=true;
                              }
                          }
                      else{
                          let evt=document.createEvent("MouseEvents");
                          evt.initEvent('mouseover',true); 
                          elements.dispatchEvent(evt);
                          evt.initEvent('mousedown',true);
                          elements.dispatchEvent(evt);
                          evt.initEvent('mouseenter',true);
                          elements.dispatchEvent(evt);
                          evt.initEvent('mouseup',true);
                          elements.dispatchEvent(evt);
                          elements.focus();
                          elements.click();
                          Sqa.sqa_exec=true;
                          
                      }
              break;
          case "EnterText":
                  if((obj.tag=="INPUT"||obj.tag=="input")&& Sqa.inputTypes.indexOf(obj.type)>=0 ){
                      core.events.setValue(elements, '');
                      bot.action.type(elements, obj.Value);
                      Sqa.sqa_exec=true;
                  }
                  if(obj.tag=="textarea")
                  {
                      core.events.setValue(elements, '');
                      bot.action.type(elements, obj.Value);
                      Sqa.sqa_exec=true;
                  }
                      break;
          case "Select Item From Dropdown":
              if(obj.tag=="SELECT"||obj.tag=="select"){
                  $(elements).val(obj.Value);
                  let evt= new Event('change');
                  elements.dispatchEvent(evt);
              Sqa.sqa_exec=true;
              }
                  break;
          case "Enter":
                  if(Sqa.browserName=="Chrome"){
                      let eval=obj['cssIndex'];
                      browser.runtime.sendMessage({debug:true,value:"${KEY_ENTER}",eval:eval});
                  }
                  else
                  {
                      Sqa.keys = Sqa.replaceKeys(value)
                      bot.action.type(elements, Sqa.keys)
                      bot.events.fire(elements, bot.events.EventType.CHANGE);
                  }
                  Sqa.sqa_exec=true;
                  break;
          case 'dbclick':
                  $(elements).dblclick();
                  Sqa.sqa_exec=true;
              break;
          case "Content Text":
                  elements.innerHTML=obj.Value;
                  Sqa.sqa_exec=true;
              break;
          case "ArrowDown":
                          var customEvent;
                          customEvent= new KeyboardEvent("keydown", {bubbles : true, cancelable : true, keyCode : 40, shiftKey : true});
                          elements.dispatchEvent(customEvent);
                          Sqa.sqa_exec=true;
                          break;
          case "ArrowUp":
                      var customEvent;
                      customEvent= new KeyboardEvent("keydown", {bubbles : true, cancelable : true, keyCode : 38, shiftKey : true});
                      elements.dispatchEvent(customEvent);
                      Sqa.sqa_exec=true;
                      break;
          case "Exists":
                      if(elements)
                          Sqa.sqa_exec=true;
                      break;
          case "validateText":
                      if(elements.innerText==obj.Value)
                          Sqa.sqa_exec=true;
                          break;

          case "validatecheck": 
                  if(elements.checked==obj.checked)
                      Sqa.sqa_exec=true; 
                  break;
          case "Scroll To View":
                  window.scrollTo(0,obj.windowscroll);
                  elements.scroll(obj.scrollx,obj.scrolly);
                  Sqa.sqa_exec=true;
      }
}
 if(Sqa.sqa_exec==false){
  browser.runtime.sendMessage({play_back:"wait",index:Sqa.index})
 }
 else{
  browser.runtime.sendMessage({play_back:"recived",index:Sqa.index,sqatest:Sqa.sqatest,captureScreenshot:Sqa.captureScreenshot}) 
  sqa_exe=false;
 }

}

 catch(err){
  browser.runtime.sendMessage({play_back:"wait",index:Sqa.index})
     return;
 }

}
EventManager.prototype.getbrowserName=()=>
{
  if(navigator.userAgent.indexOf("Edge")!=-1){
      Sqa.browserName="Edge";
  }
  else if(navigator.userAgent.indexOf("Firefox")!=-1){
      Sqa.browserName="Firefox";
  }
  else if(navigator.userAgent.indexOf("Chrome")!=-1){
      Sqa.browserName="Chrome";
  }
  else if (navigator.userAgent.indexOf("Safari")!=-1){
      Sqa.browserName="Safari";
  }
}
EventManager.prototype.replaceKeys=(str)=>
{
  let keys = []
  let match = str.match(/\$\{\w+\}/g)
  if (!match) {
    keys.push(str)
  } else {
    let i = 0
    while (i < str.length) {
      let currentKey = match.shift(),
        currentKeyIndex = str.indexOf(currentKey, i)
      if (currentKeyIndex > i) {
        // push the string before the current key
        keys.push(str.substr(i, currentKeyIndex - i))
        i = currentKeyIndex
      }
      if (currentKey) {
        if (/^\$\{KEY_\w+\}/.test(currentKey)) {
          // is a key
          let keyName = currentKey.match(/\$\{KEY_(\w+)\}/)[1]
          let key = bot.Keyboard.Keys[keyName]
          if (key) {
            keys.push(key)
          } else {
            throw new Error(`Unrecognised key ${keyName}`)
          }
        } else {
          // not a key, and not a stored variable, push it as-is
          keys.push(currentKey)
        }
        i += currentKey.length
      } else if (i < str.length) {
        // push the rest of the string
        keys.push(str.substr(i, str.length))
        i = str.length
      }
    }
  }
  return keys

}
var Sqa = new EventManager();
$.ajax({global: true});
$(document).ajaxStop(Sqa.DomReady);
