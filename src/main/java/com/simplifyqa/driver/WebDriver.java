package com.simplifyqa.driver;

import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.web.DTO.Webcapabilities;
import com.simplifyqa.web.implementation.DriverOption;
import com.simplifyqa.web.implementation.WebAutomationManager;

public class WebDriver {

	private static int count;
	private DriverOption dOption;
	private Webcapabilities capabilities;
	private WebAutomationManager actionManager;
	private String hostAddress;
	private String sessionId = null;
	private Webdriversession wbDriver;
	private Logger logger = LoggerFactory.getLogger(WebDriver.class);
	private Boolean always_match = false;

	public WebDriver(Webcapabilities capabilities) {
		try {
			this.capabilities = capabilities;
			init();
		} catch (Exception e) {
			logger.error("Driver Error");
		}
	}

	public boolean init() throws InterruptedException, ExecutionException {

		try {

			if (this.capabilities.getCapabilities().has("alwaysMatch")) {
				dOption = new DriverOption(
						this.capabilities.getCapabilities().getJSONObject("alwaysMatch").get("browserName").toString());
				always_match = true;
			} else {
				dOption = new DriverOption(capabilities.getCapabilities().getString("browserName").toString());
				always_match = false;
			}
			hostAddress = this.dOption.init();
			Thread.sleep(3000);
			if (TestWDA()) {

				if (InitializeDependence.recording_browser.equals("firefox") || always_match) {
					System.out.println(this.capabilities.getCapabilities()
							.getJSONObject("alwaysMatch").get("browserName").toString().equals("firefox"));
					if (InitializeDependence.recording_session || this.capabilities.getCapabilities()
							.getJSONObject("alwaysMatch").get("browserName").toString().equals("firefox")) {
						InitializeDependence.firefox_playback=true;
						wbDriver = new Webdriversession();
						sessionId = this.wbDriver.createFirefoxSession(this.hostAddress,
								new JSONObject().put("capabilities", capabilities.getCapabilities()).toString());
						actionManager = new WebAutomationManager(sessionId, hostAddress);

						return true;
					} else {
						
						//for chrome on firefox
						wbDriver = new Webdriversession();
						sessionId = this.wbDriver.createSession(this.hostAddress,
								new JSONObject().put("capabilities", capabilities.getCapabilities()).toString());
						actionManager = new WebAutomationManager(sessionId, hostAddress);

						return true;
					}

				}

				else {
					wbDriver = new Webdriversession();
					sessionId = this.wbDriver.createSession(this.hostAddress,
							new JSONObject().put("capabilities", capabilities.getCapabilities()).toString());
					actionManager = new WebAutomationManager(sessionId, hostAddress);

					return true;

				}
			} else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean TestWDA() throws InterruptedException {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(String.format(hostAddress + "/%s", new Object[] { "status" }));
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			logger.info(res.toString());
			return true;
		} catch (Exception e) {
			if (count < 10) {
				count++;
				if (TestWDA())
					return true;
				else
					return false;
			} else {

				count = 0;
			}
			return false;

		}
	}

	public boolean closeDriver() {
		return this.dOption.destroy();
	}

	public Webdriversession getWbDriver() {
		return wbDriver;
	}

	public void setActionManger(WebAutomationManager actionManger) {
		this.actionManager = actionManger;
	}

	public void setWbDriver(Webdriversession wbDriver) {
		this.wbDriver = wbDriver;
	}

	public DriverOption getdOption() {
		return dOption;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public String getSessionId() {
		return sessionId;
	}

	public WebAutomationManager getActionManger() {
		return actionManager;
	}

}
