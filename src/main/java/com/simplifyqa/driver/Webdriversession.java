package com.simplifyqa.driver;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

//import com.fasterxml.uuid.Logger;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.Service;
import com.simplifyqa.web.DTO.Webcapabilities;
import com.simplifyqa.web.interFace.DriverSession;

public class Webdriversession implements DriverSession {

	private String sessionId;
	private String hostAddrss;
	private HashMap<String, String> headers = new HashMap<String, String>();

	public String getHostAddrss() {
		return hostAddrss;
	}

	public void setHostAddrss(String hostAddrss) {
		this.hostAddrss = hostAddrss;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	int count = 0;

	@Override
	public String createSession(String hostaddress, String params) {
		try {

			hostAddrss = hostaddress;
			headers.put("content-type", "application/json");
			JSONObject res;
			Thread.sleep(2000);
			res = new JSONObject(HttpUtility.SendPost(hostaddress + "/session", params, headers));
			sessionId = res.getJSONObject("value").getString("sessionId");
			return sessionId;

		} catch (Exception e) {
			return "error";

		}
	}

	@Override
	public String createFirefoxSession(String hostaddress, String params) {
		try {
			System.out.println("inside try");

			if (InitializeDependence.recording_browser.equals("firefox") || InitializeDependence.firefox_playback) {
				System.out.println("inside if");
				if (InitializeDependence.recording_session || InitializeDependence.firefox_playback) {
					System.out.println("try block");

					InitializeDependence.firefox_quicklaunch = true;
					hostAddrss = hostaddress;
					headers.put("content-type", "application/json");
					JSONObject res;
					res = new JSONObject(HttpUtility.SendPost(hostaddress + "/session", params, headers));
					Thread.sleep(3000);
					sessionId = res.getJSONObject("value").getString("sessionId");
					InitializeDependence.firefox_session = sessionId;
					InitializeDependence.recording_session = false;
					InitializeDependence.firefox_quicklaunch = false;
					InitializeDependence.firefox_playback=false;
					return sessionId;
				} else {
					System.out.println("inside else");
					return "error";
				}
			} else {
				hostAddrss = hostaddress;
				headers.put("content-type", "application/json");
				JSONObject res;
				res = new JSONObject(HttpUtility.SendPost(hostaddress + "/session", params, headers));
				sessionId = res.getJSONObject("value").getString("sessionId");
				return sessionId;
			}

		} catch (Exception e) {

			WebDriver driver;
			Webcapabilities obj = new Webcapabilities();
			JSONObject obj1 = new JSONObject(params);
			System.out.println(obj1 + "hdahfjkfjkqejf");
			obj.setCapabilities(obj1.getJSONObject("capabilities"));
			driver = new com.simplifyqa.driver.WebDriver(obj);
			hostaddress = driver.getdOption().init();
//			InitializeDependence.recording_session = false;
			return createSession(hostaddress, params);

		}
	}

	@Override
	public boolean deleteSession() {
		String host = this.hostAddrss + "/session/" + sessionId;
		JSONObject res = HttpUtility.delete(host);
		System.out.println(res.toString());
		if (res.has("value")) {
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		}
		return false;
	}

	@Override
	public JSONObject sessionStatus() {
		String host = this.hostAddrss + "/status";
		headers.put("content-type", "application/json");
		JSONObject res = HttpUtility.get(host, null, headers);
		System.out.println(res.get("value"));
		return res.getJSONObject("value");
	}

}
