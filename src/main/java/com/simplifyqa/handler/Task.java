package com.simplifyqa.handler;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;

public class Task {

	private Content b;
	private Events a;

	public final Events a() {
		return this.a;
	}

	public final Content b() {
		return this.b;
	}

	public Task(Events a) {
		this(a, null, null);
	}

	public Task(Events a, CompletableFuture<?> paramCompletableFuture, JSONObject paramObject) {
		this.b = new Content(paramCompletableFuture, paramObject);
		this.a = a;
	}
}
