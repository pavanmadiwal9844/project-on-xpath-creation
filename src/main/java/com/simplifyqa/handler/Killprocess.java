package com.simplifyqa.handler;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;

import com.simplifyqa.mobile.ios.Idevice;

public class Killprocess {

	public void killProcess() throws IOException {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime.getRuntime().exec("taskkill /F /IM " + "server.exe");
		} else if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
			try {
				Idevice.EndServer();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void killDebugger() throws IOException {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime.getRuntime().exec("taskkill /F /IM " + "Python.exe");
		} else if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
			try {
				Runtime.getRuntime().exec("killall Python");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void killdriver() throws IOException, InterruptedException {
		if (SystemUtils.IS_OS_WINDOWS) {
			String command = "Taskkill /F /IM chromedriver.exe";
			Process p = Runtime.getRuntime().exec(command);
			p.waitFor();
			command = "Taskkill /F /IM geckodriver.exe";
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			command = "Taskkill /F /IM msedgedriver.exe";
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			command = "Taskkill /F /IM IEDriverServer.exe";
			p = Runtime.getRuntime().exec(command);
			p.waitFor();

		} else if (SystemUtils.IS_OS_MAC)

		{
			String command = "pkill chromedriverMac";
			Process p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
			command = "pkill geckodriverMac";
			p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
			command = "pkill msedgedriverMac";
			p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
			command = "pkill safaridriver";
			p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
		} else {
			String command = "pkill chromedriverLinux";
			Process p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
			command = "pkill geckodriverLinux";
			p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
			command = "pkill msedgedriverLinux";
			p = Runtime.getRuntime().exec(command.split(" "));
			p.waitFor();
		}

	}

	public void killIproxy() {
		if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
			Idevice.EndRunnerapp();
		} else {
			Idevice.EndRunnerapp();
		}
	}
}
