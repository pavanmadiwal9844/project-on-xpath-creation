package com.simplifyqa.handler;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.agent.Browser;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.manager.DesktopStepManager;
import com.simplifyqa.manager.StepManager;

public class DesktopObjectHandler implements Runnable {
	
	private Logger logger = LoggerFactory.getLogger(DesktopObjectHandler.class);
	private ArrayBlockingQueue<DumpOBject> queue = new ArrayBlockingQueue<DumpOBject>(256);

	public final ArrayBlockingQueue<DumpOBject> a() {
		return this.queue;
	}

	private void status(Boolean start) {
		while (start) {
			try {
				this.checkobject(this.queue.take());	
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void checkobject(DumpOBject dmpobj) throws InterruptedException {
		Browser.stepnumber++;
		try {
		DesktopStepManager SqawebDriver= new DesktopStepManager();
		JSONObject object = new JSONObject();
		if(dmpobj.getAction().equals("LaunchApplication")) {
			if((object=SqawebDriver.SearchObject(dmpobj)).getJSONObject("data").getJSONArray("data").length()!=0) {
				//Send object to angular
				common(object, dmpobj);
			}
			else {
				object=SqawebDriver.SavaObject(dmpobj);
				common(object, dmpobj);
			}
		}
		else {
			if((object=SqawebDriver.checkDupobject(dmpobj)).getJSONArray("data").length()!=0) {
				common(object, dmpobj);
			}
			else {
				while(true) {
					if((object=SqawebDriver.SearchObject(dmpobj)).getJSONObject("data").getJSONArray("data").length()==0) {
						break;	
					}
					dmpobj.setText(dmpobj.getText()+"_"+new Random().nextInt(1000));
				}
				if((object=SqawebDriver.SavaObject(dmpobj))!=null) {
					common(object, dmpobj);
				}else {
					logger.info("error");
				}
				
			}
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void common(JSONObject object,DumpOBject dmpobj) {
		JSONObject step = new JSONObject();
		
		if (object.get("data") instanceof JSONObject) {
		if(null!=object.getJSONObject("data").optJSONArray("data")) {
			step.put("object",object.getJSONObject("data").getJSONArray("data").get(0) );
		} else {
			step.put("object",object.getJSONObject("data") );
		}
		}else {
			step.put("object",object.getJSONArray("data").getJSONObject(0) );
		}
		step.put("action", dmpobj.getAction());
		step.put("objtemplateId", GlobalDetail.user.getObjtemplateId());
		if(dmpobj.getValue()!=null) {
			step.put("value", dmpobj.getValue());
			JSONObject parame = new JSONObject();
			parame.put(Constants.NAME,"param_"+Browser.stepnumber);
			JSONObject parame1 =updateParamDetails(parame, dmpobj.getValue());
			JSONArray array = new JSONArray();
			array.put(parame1);
			step.put("parameters", array);
		}
		GlobalDetail.sendStpesSession.getAsyncRemote().sendText(step.toString());
	}
	private JSONObject updateParamDetails(JSONObject param, String value) {
		param.put("createdBy", GlobalDetail.user.getCreatedBy());
		param.put("customerId", GlobalDetail.user.getCustomerId());
		param.put("deleted", Boolean.FALSE);
		param.put(Constants.DEFAULTVALUE, value);
		param.put("type","LOCAL");
		param.put("paramtype", "Alphanumeric");
		param.put("paramAttributes", new JSONObject());
		param.put("projectId", GlobalDetail.user.getProjectId());
		param.put("updatedBy", GlobalDetail.user.getCreatedBy());
		param.put("description", param.getString(Constants.NAME));
		return param;
	}

     //@Override
	public void run() {
		this.status(true);
	}
	

}
