package com.simplifyqa.handler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import com.simplifyqa.mobile.android.Threadpool;
import com.simplifyqa.mobile.android.service;

public class SetautomaterService {
	private static service g;

	public static service g() {
		if (g == null) {
			g = new service();
		}
		return g;
	}

	private final ExecutorService e = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue());

	public ExecutorService e() {
		return this.e;
	}

}
