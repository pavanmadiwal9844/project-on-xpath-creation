package com.simplifyqa.handler;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.MainFrameRecord;
import com.simplifyqa.manager.MainFrameManager;

public class MainFrameObjectHandler implements Runnable {

	private ArrayBlockingQueue<DumpOBject> queue = new ArrayBlockingQueue<DumpOBject>(256);
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFrameObjectHandler.class);

	public final ArrayBlockingQueue<DumpOBject> a() {
		return this.queue;
	}

	private void status(Boolean start) {
		while (start) {
			try {
				this.checkobject(this.queue.take());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void checkobject(DumpOBject dmpobj) throws InterruptedException {
		try {
			MainFrameRecord.stepnumber++;
			MainFrameManager sqadriver = new MainFrameManager();
			JSONObject object = null;
			if (dmpobj.getAction().equalsIgnoreCase("launchapplication")) {
				object = sqadriver.SearchObject(dmpobj);
				if (object != null && object.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).length() != 0) {
					object = object.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0);
				} else {
					object = sqadriver.SavaObject(dmpobj);
					object = object.getJSONObject(Constants.DATA);
				}
			} else {
				object = sqadriver.checkDupobject(dmpobj);
				if (null != object && object.getJSONArray(Constants.DATA).length() != 0) {
					object = object.getJSONArray(Constants.DATA).getJSONObject(0);
				} else {
					while(true) {
						if((object=sqadriver.SearchObject(dmpobj)).getJSONObject("data").getJSONArray("data").length()==0) {
							break;	
						}
						dmpobj.setText(dmpobj.getText()+"_"+new Random().nextInt(1000));
					}
					object = sqadriver.SavaObject(dmpobj);
					object = object.getJSONObject(Constants.DATA);
				}
			}
			if (null == object) {
				throw new RuntimeException("error while saving object");
			}

			common(object, dmpobj);
		} catch (Exception e) {
			logger.error("error is while duplicate object&save object", e);
		}
	}

	private void common(JSONObject object, DumpOBject dmpobj)
			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException {
		JSONObject step = new JSONObject();
		step.put(Constants.OBJECT, object);
		step.put(Constants.ACTION, dmpobj.getAction());
		step.put(Constants.OBJTEMPLETEID, GlobalDetail.user.getObjtemplateId());
		if (dmpobj.getValue() != null) {
			step.put(Constants.VALUE, dmpobj.getValue());
		}
		System.out.println("sending step to angular   " + object.toString());

		if ("KeyBoardAction".equalsIgnoreCase(step.getString(Constants.ACTION))) {
			JSONArray params = checkKeyBoardParam(step);
			step.put(Constants.PARAMETERS, params);
		} else {
			step = checkingparameter(step);
		}
		logger.info("sending step to angular" + step.toString());
		Future response = GlobalDetail.sendStpesSession.getAsyncRemote().sendText(step.toString());
		System.out.println(response.isDone());
		logger.info(response.isDone() + "");
	}

	private JSONObject checkingparameter(JSONObject parameter)
			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException {

		JSONArray para = new JSONArray();
		if (!parameter.isNull(Constants.VALUE) && parameter.getString(Constants.VALUE) != null
				&& parameter.has(Constants.VALUE)) {
			JSONObject param = new JSONObject();
//			for (int i = 0; i < parameter.getJSONObject(Constants.OBJECT).getJSONArray(Constants.ATTRIBUTES)
//					.length(); i++) {
//				if (parameter.getJSONObject(Constants.OBJECT).getJSONArray(Constants.ATTRIBUTES).getJSONObject(i)
//						.getString(Constants.NAME).equalsIgnoreCase("Label")
//						&& !parameter.getJSONObject(Constants.OBJECT).getJSONArray(Constants.ATTRIBUTES)
//								.getJSONObject(i).isNull(Constants.VALUE)) {
//					String removedotparam = parameter.getJSONObject(Constants.OBJECT)
//							.getJSONArray(Constants.ATTRIBUTES).getJSONObject(i).getString(Constants.VALUE);
//					if(removedotparam.contains(".")) {
//						removedotparam  = removedotparam.replace('.', '_');
//					}
//					param.put(Constants.NAME, removedotparam+"_"+MainFrameRecord.stepnumber);
			String removedotparam = parameter.getJSONObject("object").getString("disname");
			if (removedotparam.contains(".")) {
				removedotparam = removedotparam.replace('.', '_');
			}
			param.put(Constants.NAME, removedotparam + "_" + MainFrameRecord.stepnumber);
//					break;
//				}
//			}
			if (!param.has(Constants.NAME)) {
				param.put(Constants.NAME, "param_" + MainFrameRecord.stepnumber);
				updateParamDetails(param, parameter.getString(Constants.VALUE));
			} else {

//				JSONObject response = searchParameter(param.getString(Constants.NAME));
//				if (response.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).length() > 0) {
//					param = response.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0);
//					param.remove("_id");
				updateParamDetails(param, parameter.getString(Constants.VALUE));
//				} else {
//					updateParamDetails(param, parameter.getString(Constants.VALUE));
//				}
			}

			para.put(param);
		}
		parameter.put(Constants.PARAMETERS, para);
		return parameter;

	}

	private JSONObject updateParamDetails(JSONObject param, String value) {
		param.put("createdBy", GlobalDetail.user.getCreatedBy());
		param.put("customerId", GlobalDetail.user.getCustomerId());
		param.put("deleted", Boolean.FALSE);
		param.put(Constants.DEFAULTVALUE, value);
		param.put("paramtype", "Alphanumeric");
		param.put("projectId", GlobalDetail.user.getProjectId());
		param.put("updatedBy", GlobalDetail.user.getCreatedBy());
		param.put("description", param.getString(Constants.NAME));
		return param;
	}

	// @Override
	public void run() {
		this.status(true);
	}

//	private JSONObject searchParameter(String name)
//			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException {
//
//		HashMap<String, String> hash = new HashMap<String, String>();
//		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
//		hash.put(Constants.AUTHORIZATION, GlobalDetail.user.getAuthkey());
//		SearchColumns searchs = new SearchColumns();
//		searchs.getSearchColumns().add(new searchColumnDTO<Integer>(Constants.CUSTOMERID,
//				GlobalDetail.user.getCustomerId(), "false", Constants.INTEGER));
//		searchs.getSearchColumns().add(new searchColumnDTO<Integer>(Constants.PROJECTID,
//				GlobalDetail.user.getProjectId(), "false", Constants.INTEGER));
//		searchs.getSearchColumns().add(new searchColumnDTO<String>(Constants.NAME, name, "false", "string"));
//		searchs.getSearchColumns()
//				.add(new searchColumnDTO<Boolean>(Constants.DELETED, Boolean.FALSE, "false", Constants.BOOLEAN));
//		searchs.setCollection("parameter");
//		searchs.setLimit(1);
//		searchs.setStartIndex(0);
//		JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
//		JSONObject response = new JSONObject(
//				HttpUtility.SendPost(GlobalDetail.serverIP + "/search", object.toString(), hash));
//		return response;
//	}

	private JSONArray checkKeyBoardParam(JSONObject step)
			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException {
		JSONArray params = new JSONArray();

		if (step.getString(Constants.VALUE) != null && step.has(Constants.VALUE)) {

			List<String> keys = java.util.Arrays.asList(step.getString(Constants.VALUE).split("\\+"));
			for (String key : keys) {
//				JSONObject response = searchParameter(key);
//				if (response.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).length() > 0) {
//					JSONObject param = response.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA)
//							.getJSONObject(0);
//					param.remove("_id");
//					param.put(Constants.DEFAULTVALUE, key);
//					params.put(param);
//				} else {
				JSONObject param = new JSONObject().put(Constants.NAME, key + "_" + MainFrameRecord.stepnumber);
				param.put(Constants.DEFAULTVALUE, key);
				updateParamDetails(param, key);
				params.put(param);

//				}
			}

		}

		return params;

	}

}
