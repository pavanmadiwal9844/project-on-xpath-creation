package com.simplifyqa.handler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.ios.Idevice;

public class handler implements Runnable {

	private static final Logger a = LoggerFactory.getLogger(handler.class);
	private String xml = null;
	public String deviceID = null;
	public String mbType = null;

	public ArrayBlockingQueue<Task> a() {
		return this.b;
	}

	private ArrayBlockingQueue<Task> b = new ArrayBlockingQueue<Task>(256);

	@SuppressWarnings({ "static-access", "unchecked" })
	private void taskTap(final Object obj) throws InterruptedException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = Execute.supplyAsync(() -> {
			try {
				Thread.currentThread().setName(this.deviceID);
				if (!this.mbType.equals("ios")) {
					AndroidMethod Sqa_driver = new AndroidMethod();
					Sqa_driver.setDevice(deviceID);
					if (GlobalDetail.sqaDriver.isEmpty())
						return false;
					Sqa_driver.setDriver(GlobalDetail.sqaDriver.get(g.b().b().getString("id")));
					if (g.b().b().has("tapElement")) {
						if (Sqa_driver.Tap("xpath", g.b().b().getString("tapElement"))) {
							a.info(" Tap Successfully at :{} ", g.b().b().getString("tapElement"));
							return true;
						} else {
							a.error("Tap Unsuccessful at:{}", g.b().b().getString("tapElement"));
							return false;
						}
					} else {
						if (Sqa_driver.TapByAdb(g.b().b().getInt("x"), g.b().b().getInt("y"))) {
							a.info(" Tap Successfully at :{} ,{}", g.b().b().getInt("x"), g.b().b().getInt("y"));
							return true;
						} else {
							a.error("Tap Unsuccessful at:{},{}", g.b().b().getInt("x"), g.b().b().getInt("y"));
							return false;
						}
					}
				} else {
					IosMethod sqaDriver = new IosMethod();
					if (GlobalDetail.iosSession.isEmpty())
						return false;
					sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
					sqaDriver.setDevice(deviceID);
					if (g.b().b().has("tapElement")) {
						if (sqaDriver.FindElement("xpath", g.b().b().getString("tapElement"))) {
							if (sqaDriver.Tap()) {
								a.info(" Tap Successfully at :{} ", g.b().b().getString("tapElement"));
								return true;
							} else
								return false;
						} else {
							a.error("Tap Unsuccessful at:{}", g.b().b().getString("tapElement"));
							return false;
						}
					} else {
						if (sqaDriver.Tap(g.b().b().getInt("x"), g.b().b().getInt("y"))) {
							a.info(" Tap Successfully at :{} ,{}", g.b().b().getInt("x"), g.b().b().getInt("y"));
							return true;
						} else {
							a.error("Tap Unsuccessful at:{},{}", g.b().b().getInt("x"), g.b().b().getInt("y"));
							return false;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("Tap Unsuccessful");
				return false;
			}
		});
		Execute.complete(Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	@SuppressWarnings({ "unchecked" })
	private void taskXml(final Object obj) throws InterruptedException, ExecutionException {
		Task g = (Task) obj;
		CompletableFuture<String> Execute = (CompletableFuture<String>) g.b().a();
		CompletableFuture<String> Execute1 = CompletableFuture.supplyAsync(() -> {
			try {
				String str = "";
				if (!this.mbType.equals("ios")) {
					if (GlobalDetail.sqaDriver.isEmpty())
						return null;
					str = GlobalDetail.sqaDriver.get(this.deviceID).getPageSource();
				} else {
					if (GlobalDetail.iosSession.isEmpty())
						return null;
					IosMethod sqaDriver = new IosMethod();
					sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
					sqaDriver.setDevice(deviceID);
					str = sqaDriver.getxmlDescription();
				}
//				a.info("supplyAsync Thread name " + Thread.currentThread().getName());
				if (str.equals(xml)) {
					return null;
				} else {
					xml = str;
					return xml;
				}
			} catch (Exception e) {
				e.printStackTrace();
				Idevice.StartWDA(deviceID);
//				a.error("Get xml Unsuccessful");
				return null;
			}
		});
		Execute.complete((String) Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	private void taskwifi(final Object obj) throws InterruptedException, ExecutionException {
		Task g = (Task) obj;
		@SuppressWarnings("unchecked")
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = CompletableFuture.supplyAsync(() -> {
			try {
				AndroidMethod Sqa_driver = new AndroidMethod();
				if (GlobalDetail.sqaDriver.isEmpty())
					return false;
				else {
					Thread.currentThread().setName(this.deviceID);
					a.info("wifi :{}", g.b().b().getBoolean("flag"));
					return Sqa_driver.wifiToggle(g.b().b().getBoolean("flag"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("wifi  Unsuccessful");
				return null;
			}
		});
		Execute.complete(Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	private void taskdata(final Object obj) throws InterruptedException, ExecutionException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = CompletableFuture.supplyAsync(() -> {
			try {
				AndroidMethod Sqa_driver = new AndroidMethod();
				if (GlobalDetail.sqaDriver.isEmpty())
					return false;
				else {
					Thread.currentThread().setName(this.deviceID);
					a.info("data :{}", g.b().b().getBoolean("flag"));
					return Sqa_driver.dataToggle(g.b().b().getBoolean("flag"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("data Unsuccessful");
				return null;
			}
		});
		Execute.complete(Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	private void taskscreen(final Object obj) throws InterruptedException, ExecutionException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = CompletableFuture.supplyAsync(() -> {
			try {
				if (!this.mbType.equals("ios")) {
					AndroidMethod Sqa_driver = new AndroidMethod();
					if (GlobalDetail.sqaDriver.isEmpty())
						return false;
					else {
						Thread.currentThread().setName(this.deviceID);
						a.info("orientation Successfully");
						return Sqa_driver.mbscreenrotation(g.b().b().getString("orientation"));
					}
				} else {
					if (GlobalDetail.iosSession.isEmpty())
						return false;
					else {
						IosMethod sqaDriver = new IosMethod();
						sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
						sqaDriver.setDevice(deviceID);
						if (sqaDriver.setOrientation(g.b().b().getString("orientation"))) {
							a.info(" orientation Successfully ");
							return true;
						} else {
							a.info(" orientation Unsuccessful ");
							return true;
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("data Unsuccessful");
				return null;
			}
		});
		Execute.complete(Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	private void taskbluetooth(final Object obj) throws InterruptedException, ExecutionException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = CompletableFuture.supplyAsync(() -> {
			try {
				AndroidMethod Sqa_driver = new AndroidMethod();
				if (GlobalDetail.sqaDriver.isEmpty())
					return false;
				else {
					Thread.currentThread().setName(this.deviceID);
					a.info("bluetooth :{}", g.b().b().getBoolean("flag"));
					return Sqa_driver.bluetoothToggle(g.b().b().getBoolean("flag"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error(" bluetooth Unsuccessful");
				return null;
			}
		});
		Execute.complete(Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	private void taskClear(final Object obj) throws InterruptedException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		@SuppressWarnings("unlikely-arg-type")
		CompletableFuture<Boolean> Execute1 = Execute.supplyAsync(() -> {
			try {
				if (!this.mbType.equals("ios")) {
					AndroidMethod Sqa_driver = new AndroidMethod();
					if (GlobalDetail.sqaDriver.isEmpty())
						return false;
					Thread.currentThread().setName(this.deviceID);
					Sqa_driver.setDriver(GlobalDetail.sqaDriver.get(GlobalDetail.sqaDriver.get(this.deviceID)));
					Sqa_driver.setDevice(g.b().b().getString("id"));
					if (g.b().b().has("xpath")) {
						if (Sqa_driver.ClearText("xpath", g.b().b().getString("xpath"))) {
							a.info(" Cleared Successfully ");
							return true;
						} else {
							a.error("Cleared Unsuccessful ");
							return false;
						}
					} else if (g.b().b().has("resource")) {
						if (Sqa_driver.ClearText("resource-id", g.b().b().getString("resource"))) {
							a.info(" Cleared Successfully ");
							return true;
						} else {
							a.error("Cleared Unsuccessful ");
							return false;
						}
					} else {
						if (Sqa_driver.ClearkeysByAdb()) {
							a.info(" Cleared Successfully ");
							return true;
						} else {
							a.error("Cleared Unsuccessful ");
							return false;
						}
					}
				} else {
					if (GlobalDetail.iosSession.isEmpty())
						return false;
					else {
						IosMethod sqaDriver = new IosMethod();
						sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
						sqaDriver.setDevice(deviceID);
						if (sqaDriver.FindElement("xpath", g.b().b().getString("xpath"))) {
							if (sqaDriver.cleartext()) {
								a.info(" Cleared Successfully ");
								return true;
							} else {
								a.error("Cleared Unsuccessful ");
								return false;
							}
						} else {
							a.error("Cleared Unsuccessful ");
							return false;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("Cleared Unsuccessful ");
				return false;
			}
		});
		Execute.complete((Boolean) Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	private void taskSwipe(final Object obj) throws InterruptedException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		@SuppressWarnings("unlikely-arg-type")
		CompletableFuture<Boolean> Execute1 = Execute.supplyAsync(() -> {
			try {
				int x1 = g.b().b().getInt("x1");
				int y1 = g.b().b().getInt("y1");
				int x2 = g.b().b().getInt("x2");
				int y2 = g.b().b().getInt("y2");
				double timeout = g.b().b().getDouble("timeout");
				int to = (int) timeout;
				if (!this.mbType.equals("ios")) {
					AndroidMethod Sqa_driver = new AndroidMethod();
					if (GlobalDetail.sqaDriver.isEmpty())
						return false;
					Thread.currentThread().setName(this.deviceID);
					Sqa_driver.setDriver(GlobalDetail.sqaDriver.get(GlobalDetail.sqaDriver.get(this.deviceID)));
					if (Sqa_driver.swipe(x1, y1, x2, y2, to)) {
						a.info("Swipe Successfully at :{} ,{},{},{}", x1, y1, x2, y2);
						return true;
					} else {
						a.error("Swipe Unsuccessful at:{} ,{},{},{}", x1, y1, x2, y2);
						return false;
					}
				} else {
					if (GlobalDetail.iosSession.isEmpty())
						return false;
					IosMethod sqaDriver = new IosMethod();
					Thread.currentThread().setName(this.deviceID);
					sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
					sqaDriver.setDevice(deviceID);
					if (sqaDriver.swipe(x1, y1, x2, y2, to)) {
						a.info("Swipe Successfully at :{} ,{},{},{}", x1, y1, x2, y2);
						return true;
					} else {
						a.error("Swipe Unsuccessful at:{} ,{},{},{}", x1, y1, x2, y2);
						return false;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("Swipe Unsuccessful");
				return false;
			}
		});
		Execute.complete((Boolean) Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private void taskKeyEvent(final Object obj) throws InterruptedException {
		Task g = (Task) obj;
		CompletableFuture<Boolean> Execute = (CompletableFuture<Boolean>) g.b().a();
		CompletableFuture<Boolean> Execute1 = Execute.supplyAsync(() -> {
			try {
				if (!this.mbType.equals("ios")) {
					AndroidMethod Sqa_driver = new AndroidMethod();
					if (GlobalDetail.sqaDriver.isEmpty())
						return false;
					Thread.currentThread().setName(this.deviceID);
					Sqa_driver.setDriver(GlobalDetail.sqaDriver.get(g.b().b().getString("id")));
					Sqa_driver.setDevice(g.b().b().getString("id"));
					if (g.b().b().has("Texts")) {
						if (g.b().b().has("xpath")) {
							if (Sqa_driver.Sendkeys("xpath", g.b().b().getString("xpath"),
									g.b().b().getString("Texts"))) {
								a.info("Enter value Successfully");
								return true;
							} else {
								a.info("Failed to Enter value");
								return false;
							}
						} else if (g.b().b().has("resource")) {
							if (Sqa_driver.Sendkeys("resource-id", g.b().b().getString("resource"),
									g.b().b().getString("Texts"))) {
								a.info("Enter value Successfully  ");
								return true;
							} else {
								a.info("Failed to Enter value");
								return false;
							}
						} else {
							if (Sqa_driver.sendkeysByOther(g.b().b().getString("Texts"))) {
								a.info("Enter value Successfully  ");
								return true;
							} else {
								a.info("Failed to Enter value");
								return false;
							}
						}
					} else {
						if (Sqa_driver.emulatorevent(g.b().b().getString("key"))) {
							a.info(" EmulatorEvent Successful");
							return true;
						} else {
							a.error("EmulatorEvent Unsuccessful");
							return false;
						}
					}
				} else {
					if (GlobalDetail.iosSession.isEmpty())
						return false;
					IosMethod sqaDriver = new IosMethod();
					sqaDriver.setSessionId(GlobalDetail.iosSession.get(deviceID));
					sqaDriver.setDevice(deviceID);
					if (g.b().b().has("Texts")) {
						if (g.b().b().has("xpath")) {
							if (sqaDriver.FindElement("xpath", g.b().b().getString("xpath"))) {
								if (sqaDriver.EnterText(g.b().b().getString("Texts"))) {
									a.info("Enter value Successfully  ");
									return true;
								} else {
									a.info("Failed to Enter value");
									return false;
								}
							} else {
								a.info("Failed to Enter value");
								return false;
							}
						} else if (g.b().b().has("resource")) {
							if (sqaDriver.FindElement("name", g.b().b().getString("resource"))) {
								if (sqaDriver.EnterText(g.b().b().getString("Texts"))) {
									a.info("Enter value Successfully  ");
									return true;
								} else {
									a.info("Failed to Enter value");
									return false;
								}
							} else {
								a.info("Failed to Enter value");
								return false;
							}
						} else {
							a.info("Failed to Enter value");
							return false;
						}
					} else {
						if (g.b().b().getString("key").equals("ioshome") && sqaDriver.Home()) {
							a.info(" EmulatorEvent Successful");
							return true;
						} else {
							a.error("EmulatorEvent Unsuccessful");
							return false;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				a.error("Key Event Unsuccessful");
				return false;
			}
		});
		Execute.complete((Boolean) Execute1.join());
		Thread.sleep(50);
		status(true);
	}

	@Override
	public void run() {
		status(true);
	}

	public void status(boolean report) {

		while (report) {
			Task g = null;

			try {

				g = this.b.take();
				if (g.a() == Events.Tap) {
					report = false;
					taskTap(g);
				} else if (g.a() == Events.xml) {
					report = false;
					taskXml(g);
				} else if (g.a() == Events.clear) {
					report = false;
					taskClear(g);
				} else if (g.a() == Events.swip) {
					report = false;
					taskSwipe(g);
				} else if (g.a() == Events.keyEvent) {
					report = false;
					taskKeyEvent(g);
				} else if (g.a() == Events.wifi) {
					report = false;
					taskwifi(g);
				} else if (g.a() == Events.data) {
					report = false;
					taskdata(g);
				} else if (g.a() == Events.bluetooth) {
					report = false;
					taskbluetooth(g);
				} else if (g.a() == Events.orientation) {
					report = false;
					taskscreen(g);
				} else {
					a.info("Running in initial state!!!!!!!");
				}
			} catch (InterruptedException a1) {
				a.error("interrupted", a1);
			}

			catch (ExecutionException e) {
				e.printStackTrace();
			}

		}

	}

}
