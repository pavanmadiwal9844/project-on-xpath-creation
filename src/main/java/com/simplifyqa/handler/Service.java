package com.simplifyqa.handler;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mobile.android.Threadpool;

public class Service {

	private static final Logger a = LoggerFactory.getLogger(Service.class);
	private final ExecutorService b = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue());
	
	private static final org.apache.log4j.Logger APIlogger = org.apache.log4j.LogManager
			.getLogger(HttpUtility.class);

	static LoggerClass APILogger = new LoggerClass();
	
	private handler c = new handler();

	private static Service d;

	public static Service a() {
		if (d == null) {
			d = new Service();
		}
		return d;
	}

	public void Reset() throws IOException {
		d = null;
	}
	public static Boolean saveLogs(Setupexec executionDetail,HashMap<String, String> header) {
	try { 
		File logFile = UserDir.getUserLogs();
			if (logFile.exists()) {
				JSONObject params = new JSONObject();
				params.put("customerId", executionDetail.getCustomerID());
				params.put("projectId",executionDetail.getProjectID());
				params.put("executionId", executionDetail.getExecutionID());
				params.put("log",  new String(Files.readAllBytes(Paths.get(logFile.getAbsolutePath())), StandardCharsets.UTF_8));
				
				APILogger.customloggAPI(APIlogger, "Payload ", null,params.toString(), "info");


				JSONObject obj=new JSONObject(HttpUtility.sendPost(executionDetail.getServerIp()+"/savelogs", params.toString(), header));
			
				APILogger.customloggAPI(APIlogger, "After API call Response. ", null, null, "info");
				APILogger.customloggAPI(APIlogger, "Response - ", obj, null, "info");


				return true;
			}else {
				return false;
			}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}
	}

	public void killProcess() throws IOException {
		new Killprocess().killProcess();
	}
	
	public void killDriver() throws IOException, InterruptedException {
		new Killprocess().killdriver();
	}

	public final Future<?> run() {
		return this.b.submit(this.c);
	}

	public void setTask(Boolean a) {
		this.c.status(a);
	}

	public void b(Events a) {
		a(new Task(a));
	}

	public void set(String str) {
		this.c.deviceID = str;
	}

	public void setType(String str) {
		this.c.mbType = str;
	}

	public void a(Task paramg) {

		this.c.a().remainingCapacity();

		boolean bool = false;
		try {
			this.c.a().add(paramg);
		} catch (IllegalStateException illegalStateException) {
			a.error("Failed to queue  remaining capacity: {}", Integer.valueOf(this.c.a().remainingCapacity()));
			bool = true;
		}

		if (bool) {
			try {
				this.c.a().clear();
				b(Events.Intail);
				return;
			} catch (Throwable paramg1) {
				a.error("Failed to reset Agent queue, Agent will stop", paramg1);
				Runtime.getRuntime().halt(-1);
			}
		}
	}

	public final void d() {
		if (!GlobalDetail.sqaDriver.isEmpty())
			GlobalDetail.sqaDriver.get(Thread.currentThread().getName());
		for (Process p : GlobalDetail.processList)
			p.destroyForcibly();
		a.info("Appium Stopped");
		this.b.shutdown();
		a.info("Agent state machine stopped.");

	}

	public final void destroy() {
		this.b.shutdown();
		a.info("Agent state machine stopped.");
	}
	
	public void killDebugger() throws IOException {
		new Killprocess().killDebugger();
	}
	
}
