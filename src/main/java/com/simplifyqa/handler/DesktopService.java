package com.simplifyqa.handler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;

public class DesktopService {
	
	private Logger a = LoggerFactory.getLogger(Service.class);
	private ExecutorService Executor = new ThreadPoolExecutor(0, 2147483647, 60L, TimeUnit.SECONDS,
			new SynchronousQueue<>());
	private DesktopObjectHandler objhandle = new DesktopObjectHandler();
	private static DesktopService s;

	public static DesktopService a() {
		if (s == null)
			s = new DesktopService();
		return s;
	}

	public Future<?> run() {
		return this.Executor.submit(objhandle);
	}

	public final void a(DumpOBject paramg) {

		this.objhandle.a().remainingCapacity();

		boolean bool = false;
		try {
			this.objhandle.a().add(paramg);
		} catch (IllegalStateException illegalStateException) {
			a.error("Failed to queue  remaining capacity: {}", Integer.valueOf(this.objhandle.a().remainingCapacity()));
			bool = true;
		}

		if (bool) {
			try {
				this.objhandle.a().clear();
				return;
			} catch (Throwable paramg1) {
				a.error("Failed to reset Agent queue, Agent will stop", paramg1);
				Runtime.getRuntime().halt(-1);
			}
		}
	}

	public void Reset() {
		s = null;
	}

	public final void d() {
		a.info("Appium Stopped");
		this.Executor.shutdown();
		a.info("Agent state machine stopped.");

	}


}
