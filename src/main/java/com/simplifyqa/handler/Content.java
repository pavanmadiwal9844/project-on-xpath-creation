package com.simplifyqa.handler;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;

public class Content {

	private CompletableFuture<?> comFuture;
	private JSONObject data;

	public final CompletableFuture<?> a() {
		return this.comFuture;
	}

	public final JSONObject b() {
		return this.data;
	}

	public Content(CompletableFuture<?> paramCompletableFuture, JSONObject paramObject) {
		this.comFuture = paramCompletableFuture;
		this.data = paramObject;
	}
}
