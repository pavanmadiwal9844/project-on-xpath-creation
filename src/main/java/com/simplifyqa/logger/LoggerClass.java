package com.simplifyqa.logger;

import java.io.IOException;

//import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
//import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public class LoggerClass {

	// rootLogger is Log4j2 Object..
	// msg is string msg to print..
	// Object object is used only when we wish to return object in logger object.
	// Example log.info("msg",+object);
	// String type is used only when we wish to return string in logger object.
	// Example log.info("msg"+string);
	// String type is used to define what type of logger we are calling. Example
	// info,debug and error..
	// At same time, one will null or both will be null, (Object object, String
	// string)

	public void customlogg(Logger rootLogger, String msg, Object object, String string, String type) {

		rootLogger.setLevel(Level.DEBUG);
		

		// Define log pattern layout
		PatternLayout layout;
		if (string == "tc done") {
			layout = new PatternLayout("%n %m %n");
			string = "-------------------------------------------------------------------------------------------------------------------";

		}
		else if(string==("jar")) {
			layout=new PatternLayout("%n %m %n");
			string="";
		}
		else {
			layout = new PatternLayout("%p %m %n");
		}

		try {

			rootLogger.removeAllAppenders();

			// Define file appender with layout and output log file name
			RollingFileAppender fileAppender = new RollingFileAppender(layout, "UserLogs.log");
			rootLogger.setAdditivity(false);

			rootLogger.addAppender(fileAppender);

		} catch (IOException e) {
			System.out.println("Failed to add appender !!");
		}
		// Let verify the log messages

		if (type == "info") {

			if (object != null) {
				rootLogger.info(msg + object.toString());
			} else if (string != null) {
				rootLogger.info(msg + string);
			} else {
				rootLogger.info(msg);
			}
		} else if (type == "error") {
			if (object != null) {
				rootLogger.error(msg + object.toString());
			} else if (string != null) {
				rootLogger.error(msg + string);
			} else {
				rootLogger.error(msg);
			}

		}

		else if (type == "debug") {

			if (object != null) {
				rootLogger.error(msg + object.toString());
			} else if (string != null) {
				rootLogger.error(msg + string);
			} else {
				rootLogger.error(msg);
			}
		}

		rootLogger.removeAllAppenders();
	}
	
	public void customloggAPI(Logger rootLogger, String msg, Object object, String string, String type) {



		rootLogger.setLevel(Level.DEBUG);



		// Define log pattern layout
		PatternLayout layout;
		if (string == "tc done") {
		layout = new PatternLayout("%n %m %n");
		string = "-------------------------------------------------------------------------------------------------------------------";



		}
		else if(string==("jar")) {
		layout=new PatternLayout("%n %m %n");
		string="";
		}
		else {
		layout = new PatternLayout("%p %m %n");
		}



		try {



		rootLogger.removeAllAppenders();



		// Define file appender with layout and output log file name
		RollingFileAppender fileAppender = new RollingFileAppender(layout, "APILogs.log");
		rootLogger.setAdditivity(false);



		rootLogger.addAppender(fileAppender);



		} catch (IOException e) {
		System.out.println("Failed to add appender !!");
		}
		// Let verify the log messages



		if (type == "info") {



		if (object != null) {
		rootLogger.info(msg + object.toString());
		} else if (string != null) {
		rootLogger.info(msg + string);
		} else {
		rootLogger.info(msg);
		}
		} else if (type == "error") {
		if (object != null) {
		rootLogger.error(msg + object.toString());
		} else if (string != null) {
		rootLogger.error(msg + string);
		} else {
		rootLogger.error(msg);
		}



		}



		else if (type == "debug") {



		if (object != null) {
		rootLogger.debug(msg + object.toString());
		} else if (string != null) {
		rootLogger.debug(msg + string);
		} else {
		rootLogger.debug(msg);
		}
		}



		rootLogger.removeAllAppenders();
		}



		//for api logs..

		}


