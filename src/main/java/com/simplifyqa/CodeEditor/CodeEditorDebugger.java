package com.simplifyqa.CodeEditor;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.ProcessBuilder.Redirect;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.javafaker.Cat;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.Service;
import com.simplifyqa.method.AndroidMethod;
import com.thoughtworks.xstream.io.path.Path;

import io.netty.handler.codec.http.HttpResponse;
import javassist.bytecode.stackmap.BasicBlock.Catch;
import okhttp3.MultipartBody;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;

public class CodeEditorDebugger {

	int port1 = 7008;
	int port2 = 9001;
	private static final Logger logger = LoggerFactory.getLogger(CodeEditorDebugger.class);
	public final static int SOCKET_PORT = 8000;
	public final static String SERVER = "127.0.0.1";
	public final static String FILE_TO_RECEIVED1 = "./libs/External_Jar/part1.jar";
	public final static String FILE_TO_RECEIVED2 = "./libs/External_Jar/part2.jar";
	String url = "http://172.104.183.14:4090/get_workspace";
	public final static int FILE_SIZE = 6022386;
	public boolean original = false;

	public String workspace;

//	public void Debugger() {
//
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//
//				try {
//
//					String sending_port = "./libs/Endpoint_side.py".concat(" " + port1);
//					Process process = new ProcessBuilder(
//							new String[] { "python3", "./libs/Endpoint_side.py", Integer.toString(port1), "python3" })
//									.start();
//					logger.info("CodeEditor Debbuger Started");
//					System.out.println("python3");
//
//					InputStream stdIn = process.getInputStream();
//					Reader isr = new InputStreamReader(stdIn);
//					BufferedReader br = new BufferedReader(isr);
//					String line = null;
//					Boolean xctrunnerquite = false;
//					System.out.println("output");
//					while ((line = br.readLine()) != null) {
//						logger.info(line);
//						System.out.println(line);
//
//						}
//
//				}
//
//				catch (IOException e) {
//					try {
//						System.out.println("python");
//						String sending_port = "./libs/Endpoint_side.py".concat(" " + port1);
//						Process process = new ProcessBuilder(new String[] { "python", "./libs/Endpoint_side.py",
//								Integer.toString(port1), "python3" }).start();
//						logger.info("CodeEditor Debbuger Started");
//
//						InputStream stdIn = process.getInputStream();
//						Reader isr = new InputStreamReader(stdIn);
//						BufferedReader br = new BufferedReader(isr);
//						String line = null;
//						Boolean xctrunnerquite = false;
//						System.out.println("output");
//
//						while ((line = br.readLine()) != null) {
////						logger.info(line);
////						System.out.println(line);
//
//						}
//					} catch (Exception e2) {
//						logger.info(e.toString());
//					}
//
//				} catch (Exception e2) {
//					logger.info(e2.toString());
//				}
//
//			}
//		}).start();
//	}

	public static boolean Debug() {
		int port1 = 7008;
		try {
			try {
				Thread t = new Thread(new DebugThread());
				t.start();
			} catch (Exception e) {
				logger.error("GlobusMonitor encountered exception. reason : " + e);
			}

			return true;
		} catch (Exception e2) {

			logger.info(e2.toString());
			return false;
		}
	}

	public static boolean FileUpdate(String projectname) {

		String path = "./libs/Codeeditor/CodeEditor.jar";
		try {

			if (transferCheck(projectname)) {
				URL url;
				url = new URL("http://localhost:9001/" + projectname + ".jar");
				ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
				FileOutputStream fileOutputStream = new FileOutputStream(path);
				FileChannel fileChannel = fileOutputStream.getChannel();
				fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
				return true;
			}

			return false;

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	public static boolean transferCheck(String projectname) {
		try {
			File file = new File("./libs/External_Jar/.uuid");
			String data = "";

			if (projectname != "") {
				URL url = new URL("http://localhost:9001/" + projectname + ".txt");

				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					content.append(inputLine);
				}
				in.close();
				if (file.exists()) {
					Scanner myReader = new Scanner(file);
					while (myReader.hasNextLine()) {
						data = data + myReader.nextLine();
					}
					myReader.close();
					if (!data.equalsIgnoreCase(content.toString())) {
						FileWriter myWriter = new FileWriter(file.getAbsolutePath());
						myWriter.write(content.toString());
						myWriter.close();
						return true;
					} else
						return false;
				} else {
					file.createNewFile();
					FileWriter myWriter = new FileWriter(file.getAbsolutePath());
					myWriter.write(content.toString());
					myWriter.close();
					return true;
				}

			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean transferCheck() {
		try {
			File file = new File("./libs/External_Jar/.uuid");
			String data = "";
			URL url = new URL("http://localhost:9001/uuid.txt");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			if (file.exists()) {
				Scanner myReader = new Scanner(file);
				while (myReader.hasNextLine()) {
					data = data + myReader.nextLine();
				}
				myReader.close();
				if (!data.equalsIgnoreCase(content.toString())) {
					FileWriter myWriter = new FileWriter(file.getAbsolutePath());
					myWriter.write(content.toString());
					myWriter.close();
					return true;
				} else
					return false;
			} else {
				file.createNewFile();
				FileWriter myWriter = new FileWriter(file.getAbsolutePath());
				myWriter.write(content.toString());
				myWriter.close();
				return true;
			}

		} catch (Exception e) {
			return false;
		}
	}

	public void jarupdate(String project) {

		try {
//			String project = "simplifyCode";
			BasicFileAttributes attr = Files.readAttributes(Paths.get("com.simplifyQA.Agent.jar"),
					BasicFileAttributes.class);

			FileTime creationTime = attr.lastModifiedTime();
			JSONObject param = new JSONObject();
			param.put("target_date", creationTime);
			param.put("project", project);
			if (postapi(param.toString(), "http://127.0.0.1:7009/versionmatch")) {

				if (UserPermission.JarUpdatePermisison()) {
					logger.info("Current jar file sending to docker");
					Thread.sleep(1000);
					if (postapi(param.toString(), "http://127.0.0.1:7009/versioncopy")) {
						jarfilesend(project);
						logger.info("jar file got updated");
					} else {
						logger.info("jar file did not get updated due to some reason");
					}

				}

				else {
					logger.info("using old jarfile");
				}
			} else {
				logger.info("jarfile already updated");
			}

		}

		catch (Exception e) {
			System.out.println(e);
		}

	}

	public void gitconfigupdate(String data) {

		try {

//			File gitfile = new File(
//					Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "giturl.txt" }).toString());
//			String data = FileUtils.readFileToString(gitfile, "UTF-8");

			JSONObject param = new JSONObject();
			param.put("url", data);
			param.put("project", "CodeEditor");
			if (postapi(param.toString(), "http://127.0.0.1:7009/git")) {
				logger.info("Git Config updated successfully");
			}

			else {
				logger.info("Git config file already updated");
			}

		}

		catch (Exception e) {
			System.out.println(e);
		}

	}

	public Boolean postapi(String param, String url) {
		try {

			CloseableHttpClient httpClient = HttpUtility.getHttpClient(url);
			HttpPost httpPost = new HttpPost(url);
			httpPost.addHeader("Content-Type", "application/json");

			StringEntity params = new StringEntity(param.toString(), "UTF-8");

			httpPost.setEntity(params);
			org.apache.http.HttpResponse response = httpClient.execute(httpPost);

			String result = EntityUtils.toString(response.getEntity());
			JSONObject res = new JSONObject(result);

			if (res.getBoolean("status"))
				return true;

			else
				return false;

		} catch (Exception e) {
			return false;
		}
	}

	void jarfilesend(String projectname) {

		try {
			CloseableHttpClient httpClient = HttpUtility.getHttpClient(url);
			System.out.println(Paths.get("com.simplifyQA.Agent.jar"));
			BasicFileAttributes attr = Files.readAttributes(Paths.get("com.simplifyQA.Agent.jar"),
					BasicFileAttributes.class);

			FileTime creationTime = attr.lastModifiedTime();

			File jar = new File(Paths.get("com.simplifyQA.Agent.jar").toString());

			File file = new File(Paths.get("com.simplifyQA.Agent.jar").toString());
			HttpPost post = new HttpPost("http://127.0.0.1:7009/jarversion");
			FileBody fileBody = new FileBody(file, ContentType.create("application/java-archive"));
			StringBody stringBody1 = new StringBody(projectname, ContentType.MULTIPART_FORM_DATA);
			StringBody stringBody2 = new StringBody(creationTime.toString(), ContentType.MULTIPART_FORM_DATA);
			//
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("demo", fileBody);
			builder.addPart("project", stringBody1);
			builder.addPart("target_date", stringBody2);
			HttpEntity entity = builder.build();
			//
			post.setEntity(entity);
			org.apache.http.HttpResponse response = httpClient.execute(post);

			String result = EntityUtils.toString(response.getEntity());
			JSONObject res = new JSONObject(result);
			logger.info(res.get("status").toString());

		}

		catch (Exception e) {
			System.out.println(e);
		}

	}

	public Boolean workspace(String id, String projectname, String giturl, String customerId) {
		try {

			Thread.sleep(2000);
			String api_url = "http://127.0.0.1:7009/project";
			JSONObject param = new JSONObject();

			param.put("project", projectname);
			param.put("id", id);
			param.put("url", giturl);
			param.put("customerId", customerId);

			HashMap<String, String> header = new HashMap<String, String>();
			header.put("Content-Type", "application/json");
			JSONObject response = HttpUtility.sendPost(api_url, param.toString(), header);
			jarupdate(projectname);

			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public void Debugger2() {

		new Thread(new Runnable() {

			@Override
			public void run() {

				try {

					int SOCKET_PORT = 6507;
					String SERVER = "127.0.0.1";
					String FILE_TO_RECEIVED = "./libs/Codeeditor_files/new.txt";
					int FILE_SIZE = 1234556;
					File file_new = new File("./libs/Codeeditor_files/codeeditor.txt");
					FileWriter writer = new FileWriter("./libs/Codeeditor_files/codeeditor.txt");
					while (true) {
						try {

							BufferedWriter buffer = new BufferedWriter(writer);
							String file_conent = "";
							String content1 = "";
							if (file_new.exists()) {
								content1 = new String(
										Files.readAllBytes(Paths.get("./libs/Codeeditor_files/codeeditor.txt")));

							}
							int bytesRead;
							int current = 0;
							FileOutputStream fos = null;
							BufferedOutputStream bos = null;
							Socket sock = null;
							try {
								sock = new Socket(SERVER, SOCKET_PORT);
//							System.out.println("Connecting...");

								// receive file
								byte[] mybytearray = new byte[FILE_SIZE];
								InputStream is = sock.getInputStream();
								fos = new FileOutputStream(FILE_TO_RECEIVED);
								bos = new BufferedOutputStream(fos);
								bytesRead = is.read(mybytearray, 0, mybytearray.length);
								current = bytesRead;

								do {
									bytesRead = is.read(mybytearray, current, (mybytearray.length - current));
									if (bytesRead >= 0)
										current += bytesRead;
								} while (bytesRead > -1);

								bos.write(mybytearray, 0, current);
								bos.flush();
//							System.out.println("File " + FILE_TO_RECEIVED + " downloaded (" + current + " bytes read)");

								String content = new String(
										Files.readAllBytes(Paths.get("./libs/Codeeditor_files/new.txt")));
//							System.out.println(content);
//							System.out.println(content1);

								if (!content.equals(content1)) {
									Thread.sleep(1000);

									Service.a().killDebugger();

									System.out.println("inside debug method going");
									Debug();
									file_new.createNewFile();
									FileWriter myWriter = new FileWriter(file_new.getAbsolutePath());
									myWriter.write(content.toString());
									myWriter.close();

								}
							} finally {
								if (fos != null)
									fos.close();
								if (bos != null)
									bos.close();
								if (sock != null)
									sock.close();
							}

						} catch (Exception e) {
//					System.out.println(e);
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		}).start();
	}

	public void filedelete() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						java.nio.file.Path filepath = Paths.get("./libs/Codeeditor/codeeditor.txt");
						long bytes = Files.size(filepath);

						long filesize = bytes / 1024;

						if (filesize > 1000) {
							File obj = new File("./libs/Codeeditor/codeeditor.txt");
							obj.delete();
						}

					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}).start();
	}

	public Boolean getallprojects() {
		try {

			Thread.sleep(2000);
			String api_url = "http://127.0.0.1:7009/getallprojects";
			JSONObject param = new JSONObject();

			HashMap<String, String> header = new HashMap<String, String>();
			header.put("Content-Type", "application/json");
			JSONObject response = HttpUtility.getcall(api_url);

			for (int i = 0; i < response.length(); i++) {

				FileUpdateforsync(response.get(String.valueOf(i)).toString());

			}

			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public static boolean FileUpdateforsync(String projectname) {

		String path = "./libs/Codeeditor/SyncJars/" + projectname + ".jar";
		try {

			URL url;
			url = new URL("http://localhost:9001/" + projectname + ".jar");
			ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
			FileOutputStream fileOutputStream = new FileOutputStream(path);
			FileChannel fileChannel = fileOutputStream.getChannel();
			fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
			logger.info("File saved " + projectname);
			return true;

		} catch (Exception e) {

			return false;
		}

	}

	public static JSONArray method_list(String customerId) throws MalformedURLException {
		try {
			JSONArray CustomMethodList = new JSONArray();
			ArrayList<String> c = new ArrayList<String>();
			String contents[] = UserDir.CustomMethodSyncing().list();
			ArrayList<String> paths = new ArrayList<String>();

			for (int i = 0; i < contents.length; i++) {
				System.out.println(contents[i]);
				if (contents[i].endsWith(".jar")) {
					c.add(contents[i]);
					paths.add(Paths.get(UserDir.CustomMethodSyncing().getAbsolutePath(), new String[] { contents[i] })
							.toString());
				}
			}
			ArrayList<String> jarlist = new ArrayList<String>();
			for (String num : c) {
				try {
					jarlist.add(UserDir.CustomMethodSyncing().toURL() + num);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			URL[] jarUrl = new URL[jarlist.size()];
			URLClassLoader[] loader = new URLClassLoader[jarlist.size()];

			for (int i = 0; i < jarlist.size(); i++) {
				jarUrl[i] = new URL(jarlist.get(i));
				loader[i] = new URLClassLoader(new URL[] { jarUrl[i] });
			}

			for (int i = 0; i < paths.size(); i++) {

				if (validatecustomerId(i, customerId, paths)) {

					JarFile jar = new JarFile(paths.get(i));

					Set<Class<?>> classes = new HashSet<>();

					for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
						JarEntry entry = entries.nextElement();
						String file = entry.getName();

						if (file.startsWith(".properties")) {
							System.out.println(file.toLowerCase());
						}

						if (file.endsWith(".class") && file.startsWith("SimplifyQACodeeditor")) {
							System.out.println(file);
							String classname = file.replace('/', '.').substring(0, file.length() - 6).split("\\$")[0];

							try {
								Class<?> ce = loader[i].loadClass(classname);

								System.out.println("class loaded");
								classes.add(ce);

								System.out.println(classes.toString());
								List<Method> result = new ArrayList<Method>();
								for (Method method : ce.getDeclaredMethods()) {

									int modifiers = method.getModifiers();
									System.out.println(method.getName());
//							
									if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)
											|| Modifier.isPrivate(modifiers)) {
										JSONObject current_meth = new JSONObject();

										current_meth = AddCustomMethodDetails(method);
										CustomMethodList.put(current_meth);

									}
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					}
				} else {
					continue;
				}
			}
			logger.info(CustomMethodList.toString());
			logger.info(String.valueOf(CustomMethodList.length()));
			return CustomMethodList;
		} catch (Exception e) {
			return new JSONArray();
			// TODO: handle exception
		}
	}

	public static boolean validatecustomerId(int i, String customerId, ArrayList<String> paths) {
		try {
			JarFile jar = new JarFile(paths.get(i));

			Set<Class<?>> classes = new HashSet<>();

			for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
				FileReader reader = null;
				String content = null;
				JarEntry entry = entries.nextElement();
				String file = entry.getName();

				if (file.startsWith(".properties")) {
					logger.info("Properties File not Found.");
					InputStream input = jar.getInputStream(entry);
					String customerid_ce = process(input);

					if (customerId.equals(customerid_ce)) {
						logger.info("Customerid's match");
						return true;
					} else {
						logger.info("Customerid's do not match");
						return false;
					}

				} else {
					continue;
				}
			}
			return false;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	private static String process(InputStream input) throws IOException {
		InputStreamReader isr = new InputStreamReader(input);
		BufferedReader reader = new BufferedReader(isr);
		String line;
		String return_line = null;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
			return_line = line;
		}
		reader.close();
		return return_line;
	}

	public static JSONObject AddCustomMethodDetails(Method method) {
		try {

			JSONObject current_method = new JSONObject();
			JSONArray method_type = new JSONArray();
			JSONArray param_list = new JSONArray();

			int modifiers = method.getModifiers();
			System.out.println(method.getName());
//	

			// Things to be added
			current_method.put("actionname", method.getName());
			current_method.put("action_displayname", method.getName());
			current_method.put("action_description", method.getName());
			current_method.put("paraname_equals_curobj", false);
			current_method.put("param_count", method.getParameterCount());
			current_method.put("object_template","sqa_record");
//				System.out.println(method.getParameterCount());
//				System.out.println(method.getTypeParameters());
//				method.getParameters();

			Parameter[] parameters = method.getParameters();
			List<String> parameterNames = new ArrayList<>();

			for (Parameter parameter : parameters) {
				JSONObject current_param = new JSONObject();

				String parameterName = parameter.getName();
				parameterNames.add(parameterName);
				if (parameter.getType().toString().equals("class java.lang.String")) {
					current_param.put("type", "String");
				} else {
					current_param.put("type", parameter.getType());
				}

				current_param.put("name", parameter.getName());
				param_list.put(current_param);

			}
			current_method.put("params", param_list);
			return current_method;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new JSONObject();
		}
	}
}
