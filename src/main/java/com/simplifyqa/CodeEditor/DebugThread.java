package com.simplifyqa.CodeEditor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class DebugThread implements Runnable {

	public  DebugThread() throws IOException {
		try {
	      String[] command = {"python3", "./libs/Codeeditor_files/Endpoint_side.py",Integer.toString(7008), "python3"};
	      ProcessBuilder pb = new ProcessBuilder(command);
	      
	      pb.redirectOutput(new File("./libs/Codeeditor/codeeditor.txt"));
	      
	          //Here in above file you will get the feed back from C++ application
	      String result;
	      String overall="";
	      
	          Process p = pb.start();
	          p.waitFor();
	          BufferedReader br = new BufferedReader(
	                  new InputStreamReader(p.getInputStream()));
	              while ((result = br.readLine()) != null){
	                  overall = overall + "\n" + result;
	              }
	              //p.destroy();
	              System.out.println(result);

	      } catch (Exception e) {
	          e.printStackTrace();
	          
	          String[] command = {"python", "./libs/Codeeditor_files/Endpoint_side.py",Integer.toString(7008), "python"};
		      ProcessBuilder pb = new ProcessBuilder(command);
		      
		      pb.redirectOutput(new File("./libs/Codeeditor/codeeditor.txt"));
		      
		          //Here in above file you will get the feed back from C++ application
		      String result;
		      String overall="";
		      
		          Process p = pb.start();
		          try {
					p.waitFor();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		          BufferedReader br = new BufferedReader(
		                  new InputStreamReader(p.getInputStream()));
		              while ((result = br.readLine()) != null){
		                  overall = overall + "\n" + result;
		              }
		              //p.destroy();
		              System.out.println(result);
	          
	          
	      }


}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}