package com.simplifyqa.DTO;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApiRequest {
	private String type;
	private String url;
	private JSONArray query_params;
	private JSONObject authorization;
	private JSONArray headers;
	private JSONObject request_body;
	private JSONObject request_body_type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public JSONArray getQuery_params() {
		return query_params;
	}

	public void setQuery_params(JSONArray query_params) {
		this.query_params = query_params;
	}

	public JSONObject getAuthorization() {
		return authorization;
	}

	public void setAuthorization(JSONObject authorization) {
		this.authorization = authorization;
	}

	public JSONArray getHeaders() {
		return headers;
	}

	public void setHeaders(JSONArray headers) {
		this.headers = headers;
	}

	public JSONObject getRequest_body() {
		return request_body;
	}

	public void setRequest_body(JSONObject request_body) {
		this.request_body = request_body;
	}

	public JSONObject getRequest_body_type() {
		return request_body_type;
	}

	public void setRequest_body_type(JSONObject request_body_type) {
		this.request_body_type = request_body_type;
	}

}
