package com.simplifyqa.DTO;

public class executionData {

	private long executionId;
	private long projectId;
	private long customerId;
	private long testcaseId;
	private int tcSeq;
	public static String apiName="/v1/executionData";

	public long getExecutionId() {
		return executionId;
	}

	public void setExecutionId(long executionId) {
		this.executionId = executionId;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(long testcaseId) {
		this.testcaseId = testcaseId;
	}

	public int getTcSeq() {
		return tcSeq;
	}

	public void setTcSeq(int tcSeq) {
		this.tcSeq = tcSeq;
	}

}
