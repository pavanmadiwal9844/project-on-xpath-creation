package com.simplifyqa.DTO;

import java.util.ArrayList;

public class CodeEditorDTO {

	/**
	 * @author Srinivas
	 */

	private String className;
	private ArrayList<String> methods = new ArrayList<String>();

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public ArrayList<String> getMethods() {
		return methods;
	}

	public void setMethods(ArrayList<String> methods) {
		this.methods = methods;
	}

}
