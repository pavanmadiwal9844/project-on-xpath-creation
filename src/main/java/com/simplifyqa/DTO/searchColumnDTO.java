package com.simplifyqa.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class searchColumnDTO<T> {

	private String column;
	private T value;
	private Boolean regEx;

	public Boolean getRegEx() {
		return regEx;
	}

	public void setRegEx(Boolean regEx) {
		this.regEx = regEx;
	}

	private String type;

	public String getColumn() {
		return column;
	}

	public searchColumnDTO(String column, T value, Boolean regEx, String type) {
		super();
		this.column = column;
		this.value = value;
		this.regEx = regEx;
		this.type = type;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
