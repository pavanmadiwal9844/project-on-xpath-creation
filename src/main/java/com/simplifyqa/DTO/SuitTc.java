package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public class SuitTc {

	private List<JsonNode> testcasesuites = new ArrayList<JsonNode>();

	public List<JsonNode> getTestcasesuites() {
		return testcasesuites;
	}

	public void setTestcasesuites(List<JsonNode> testcasesuites) {
		this.testcasesuites = testcasesuites;
	}


}
