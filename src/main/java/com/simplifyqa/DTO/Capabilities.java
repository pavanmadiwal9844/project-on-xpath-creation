package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

public class Capabilities {

	private List<FirstMatch> firstMatch =  new ArrayList<FirstMatch>();

	public List<FirstMatch> getFirstMatch() {
		return firstMatch;
	}

	public void setFirstMatch(List<FirstMatch> firstMatch) {
		this.firstMatch = firstMatch;
	}
	
}
