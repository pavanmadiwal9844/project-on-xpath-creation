package com.simplifyqa.DTO;

public class UserDetail {
	
	private Integer customerId = null;
	private Integer projectId = null;
	private Integer objtemplateId = null;
	private String authkey = null;
	private Integer createdBy = null;
	private Integer testcaseId= null;

	public Integer getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getAuthkey() {
		return authkey;
	}

	public void setAuthkey(String authkey) {
		this.authkey = authkey;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getObjtemplateId() {
		return objtemplateId;
	}

	public void setObjtemplateId(Integer objtemplateId) {
		this.objtemplateId = objtemplateId;
	}




}
