package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class FirstMatch {

	private String bundleId = null;
	private Integer eventloopIdleDelaySec = -1;
	private Boolean shouldWaitForQuiescence = false;
	private Boolean shouldUseTestManagerForVisibilityDetection = false;
	private Integer maxTypingFrequency = -1;
	private Boolean shouldUseSingletonTestManager = false;
	private List<String> arguments = new ArrayList<String>();
	private JSONObject environment = new JSONObject();

	public List<String> getArguments() {
		return arguments;
	}

	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}

	public JSONObject getEnvironment() {
		return environment;
	}

	public void setEnvironment(JSONObject environment) {
		this.environment = environment;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public Integer getEventloopIdleDelaySec() {
		return eventloopIdleDelaySec;
	}

	public void setEventloopIdleDelaySec(Integer eventloopIdleDelaySec) {
		this.eventloopIdleDelaySec = eventloopIdleDelaySec;
	}

	public Boolean getShouldWaitForQuiescence() {
		return shouldWaitForQuiescence;
	}

	public void setShouldWaitForQuiescence(Boolean shouldWaitForQuiescence) {
		this.shouldWaitForQuiescence = shouldWaitForQuiescence;
	}

	public Boolean getShouldUseTestManagerForVisibilityDetection() {
		return shouldUseTestManagerForVisibilityDetection;
	}

	public void setShouldUseTestManagerForVisibilityDetection(Boolean shouldUseTestManagerForVisibilityDetection) {
		this.shouldUseTestManagerForVisibilityDetection = shouldUseTestManagerForVisibilityDetection;
	}

	public Integer getMaxTypingFrequency() {
		return maxTypingFrequency;
	}

	public void setMaxTypingFrequency(Integer maxTypingFrequency) {
		this.maxTypingFrequency = maxTypingFrequency;
	}

	public Boolean getShouldUseSingletonTestManager() {
		return shouldUseSingletonTestManager;
	}

	public void setShouldUseSingletonTestManager(Boolean shouldUseSingletonTestManager) {
		this.shouldUseSingletonTestManager = shouldUseSingletonTestManager;
	}

}
