package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.JsonNode;

public class Step {

	private Integer actionId;
	private String actionname;
	private Integer createdBy;
	private Integer customerId;
	private boolean deleted;
	private String description;
	private String object_name;
	private Integer projectId;
	private List<JsonNode> parameters = new ArrayList<JsonNode>();
	private int seq;
	private String type;
	private int version;
	private boolean captureScreenshot;
	private Integer objectId;
	private Integer parentId;
	private Integer functionId;
	private String id;
	private Integer moduleId;
	private String screenshot = null;
	private Integer result = null;
	private Integer tcSeq = null;
	private Integer itr = null;
	private String stepId = null;
	private Integer testcaseId = null;
	private JsonNode data = null;
	private Boolean skip = false;
	private Integer suiteId = null;
	private Integer executionId = null;
	private Object object = new Object();
	private Action action = new Action();
	private String ifstring = null;
	private Boolean isConditional = false;
	@JsonAlias({ "if" })
	private boolean iF = false;
	@JsonAlias({ "else" })
	private boolean eLse = false;
	private boolean endif = false;
	private String seq2 = null;
	private List<IfelseChildren> children = new ArrayList<IfelseChildren>();
	private String functionCode;
	private String responseData = null;
	private Boolean isApi = false;
	private String fnVersion;
	private boolean continueOnFailure;
	private String objectType;
	private boolean isLoop;
	private boolean startFor;
	private boolean endFor;
	private Integer loopDuration;

	public Integer getLoopDuration() {
		return loopDuration;
	}

	public void setLoopDuration(Integer loopDuration) {
		this.loopDuration = loopDuration;
	}

	public boolean isStartFor() {
		return startFor;
	}

	public void setStartFor(boolean startFor) {
		this.startFor = startFor;
	}

	public boolean isEndFor() {
		return endFor;
	}

	public void setEndFor(boolean endFor) {
		this.endFor = endFor;
	}

	public boolean isLoop() {
		return isLoop;
	}

	public void setLoop(boolean isLoop) {
		this.isLoop = isLoop;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public boolean isContinueOnFailure() {
		return continueOnFailure;
	}

	public void setContinueOnFailure(boolean continueOnFailure) {
		this.continueOnFailure = continueOnFailure;
	}

	public String getFnVersion() {
		return fnVersion;
	}

	public void setFnVersion(String fnVersion) {
		this.fnVersion = fnVersion;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public Boolean getIsApi() {
		return isApi;
	}

	public void setIsApi(Boolean isApi) {
		this.isApi = isApi;
	}

	public String getFunctionCode() {
		return functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	public Boolean getIsConditional() {
		return isConditional;
	}

	public String getSeq2() {
		return seq2;
	}

	public void setSeq2(String seq2) {
		this.seq2 = seq2;
	}

	public void setIsConditional(Boolean isConditional) {
		this.isConditional = isConditional;
	}

	public List<IfelseChildren> getChildren() {
		return children;
	}

	public void setChildren(List<IfelseChildren> children) {
		this.children = children;
	}

	public String getIfstring() {
		return ifstring;
	}

	public void setIfstring(String ifstring) {
		this.ifstring = ifstring;
	}

	public boolean isiF() {
		return iF;
	}

	public void setiF(boolean iF) {
		this.iF = iF;
	}

	public boolean iseLse() {
		return eLse;
	}

	public void seteLse(boolean eLse) {
		this.eLse = eLse;
	}

	public boolean isEndif() {
		return endif;
	}

	public void setEndif(boolean endif) {
		this.endif = endif;
	}

	public Boolean getSkip() {
		return skip;
	}

	public void setSkip(Boolean skip) {
		this.skip = skip;
	}

	public JsonNode getData() {
		return data;
	}

	public void setData(JsonNode data) {
		this.data = data;
	}

	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public String getActionname() {
		return actionname;
	}

	public void setActionname(String actionname) {
		this.actionname = actionname;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getObject_name() {
		return object_name;
	}

	public void setObject_name(String object_name) {
		this.object_name = object_name;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public List<JsonNode> getParameters() {
		return parameters;
	}

	public void setParameters(List<JsonNode> parameters) {
		this.parameters = parameters;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public boolean isCaptureScreenshot() {
		return captureScreenshot;
	}

	public void setCaptureScreenshot(boolean captureScreenshot) {
		this.captureScreenshot = captureScreenshot;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Integer getTcSeq() {
		return tcSeq;
	}

	public void setTcSeq(Integer tcSeq) {
		this.tcSeq = tcSeq;
	}

	public Integer getItr() {
		return itr;
	}

	public void setItr(Integer itr) {
		this.itr = itr;
	}

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public Integer getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public Integer getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Integer executionId) {
		this.executionId = executionId;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

}
