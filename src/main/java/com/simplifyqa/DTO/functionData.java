package com.simplifyqa.DTO;

import com.fasterxml.jackson.databind.JsonNode;

public class functionData {

	private Integer executionId;
	private Integer customerId;
	private Integer projectId;
	private JsonNode functiondataItr = null;
	public static String apiName = "/v1/functionStepData";
	private Integer functionId;
	private String environmentType = null;
	private Integer version;
	
	public String getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}

	public Integer getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	public JsonNode getFunctiondataItr() {
		return functiondataItr;
	}

	public void setFunctiondataItr(JsonNode functiondataItr) {
		this.functiondataItr = functiondataItr;
	}

	public Integer getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Integer executionId) {
		this.executionId = executionId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer fnVersion) {
		this.version = fnVersion;
	}
	
	
}
