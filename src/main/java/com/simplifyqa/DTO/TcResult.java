package com.simplifyqa.DTO;

public class TcResult {

	private Integer testcaseId = null;
	private Integer suiteId = null;
	private Integer tcSeq;
	private int version;
	private String testcaseCode;
	private String resultTc;
	private Integer customerId;
	private Integer projectId;
	private Integer executionId;
	private int itr;

	public Integer getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public Integer getTcSeq() {
		return tcSeq;
	}

	public void setTcSeq(Integer tcSeq) {
		this.tcSeq = tcSeq;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTestcaseCode() {
		return testcaseCode;
	}

	public void setTestcaseCode(String testcaseCode) {
		this.testcaseCode = testcaseCode;
	}

	public String getResultTc() {
		return resultTc;
	}

	public void setResultTc(String resultTc) {
		this.resultTc = resultTc;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Integer executionId) {
		this.executionId = executionId;
	}

	public int getItr() {
		return itr;
	}

	public void setItr(int itr) {
		this.itr = itr;
	}

}
