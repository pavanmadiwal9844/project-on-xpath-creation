package com.simplifyqa.DTO;

public class StartExecution {

	private Integer customerId;
	private Integer projectId;
	private Integer executionId;
	private Integer testcaseId;
	private Integer suiteId = null;
	private int tcSeq;
	private int version;
	private String testcaseCode;
	private String resultTc;
	private int itr;
	private String deviceId;
	private boolean executionPlanScenario = false;
	private Integer scenarioFolderId;
	private String expPlnnedDate = null;
	private Integer expAssignedTo = null;
	private Integer execplanId = null;
	private Integer seq = null;
	private Integer execplanexecId = null;

	public Integer getExecplanexecId() {
		return execplanexecId;
	}

	public void setExecplanexecId(Integer execplanexecId) {
		this.execplanexecId = execplanexecId;
	}

	public Integer getExpAssignedTo() {
		return expAssignedTo;
	}

	public void setExpAssignedTo(Integer expAssignedTo) {
		this.expAssignedTo = expAssignedTo;
	}

	public Integer getScenarioFolderId() {
		return scenarioFolderId;
	}

	public void setScenarioFolderId(Integer scenarioFolderId) {
		this.scenarioFolderId = scenarioFolderId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public boolean isExecutionPlanScenario() {
		return executionPlanScenario;
	}

	public void setExecutionPlanScenario(boolean executionPlanScenario) {
		this.executionPlanScenario = executionPlanScenario;
	}

	public String getExpPlnnedDate() {
		return expPlnnedDate;
	}

	public void setExpPlnnedDate(String expPlnnedDate) {
		this.expPlnnedDate = expPlnnedDate;
	}

	public Integer getExecplanId() {
		return execplanId;
	}

	public void setExecplanId(Integer execplanId) {
		this.execplanId = execplanId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Integer executionId) {
		this.executionId = executionId;
	}

	public Integer getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public int getTcSeq() {
		return tcSeq;
	}

	public void setTcSeq(int tcSeq) {
		this.tcSeq = tcSeq;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTestcaseCode() {
		return testcaseCode;
	}

	public void setTestcaseCode(String testcaseCode) {
		this.testcaseCode = testcaseCode;
	}

	public String getResultTc() {
		return resultTc;
	}

	public void setResultTc(String resultTc) {
		this.resultTc = resultTc;
	}

	public int getItr() {
		return itr;
	}

	public void setItr(int itr) {
		this.itr = itr;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
