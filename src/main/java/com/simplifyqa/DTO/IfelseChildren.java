package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonObject;

public class IfelseChildren {

	Integer updatedBy = null;
	String type = null;
	Integer version = null;
	String createdOn = null;
	String condition = null;
	boolean deleted = false;
	Integer customerId = null;
	String id = null;
	Integer moduleId = null;
	Integer ProjectId = null;
	JsonNode parameter2;
	Integer seq;
	JsonNode parameter1;
	String objectType;
	String operator;
	String actionname=null;
	private List<JsonNode> parameters = new ArrayList<JsonNode>();
	private Object object = new Object();
	String description;
	String object_name;
	private Action action = new Action();

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getObject_name() {
		return object_name;
	}

	public void setObject_name(String object_name) {
		this.object_name = object_name;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public List<JsonNode> getParameters() {
		return parameters;
	}

	public void setParameters(List<JsonNode> parameters) {
		this.parameters = parameters;
	}

	public String getActionname() {
		return actionname;
	}

	public void setActionname(String actionname) {
		this.actionname = actionname;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}


	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getProjectId() {
		return ProjectId;
	}

	public void setProjectId(Integer projectId) {
		ProjectId = projectId;
	}

	public JsonNode getParameter2() {
		return parameter2;
	}

	public void setParameter2(JsonNode parameter2) {
		this.parameter2 = parameter2;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public JsonNode getParameter1() {
		return parameter1;
	}

	public void setParameter1(JsonNode parameter1) {
		this.parameter1 = parameter1;
	}

}
