package com.simplifyqa.DTO;

public class Options {

	private Integer x = null;
	private Integer y = null;
	private Integer ms = null;

	public Options() {
	}
	public Options(int startX, int startY) {
		this.x=startX;
		this.y=startY;
	}

	public Options(int timeout) {
		this.ms=timeout;
	}
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getMs() {
		return ms;
	}

	public void setMs(Integer ms) {
		this.ms = ms;
	}

}
