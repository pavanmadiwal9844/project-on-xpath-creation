package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class SearchColumns {
	@SuppressWarnings("rawtypes")
	private List<searchColumnDTO> searchColumns=new ArrayList<searchColumnDTO>();
	private int startIndex;
	private long limit;
	private String collection;

	@SuppressWarnings("rawtypes")
	public List<searchColumnDTO> getSearchColumns() {
		return searchColumns;
	}

	@SuppressWarnings("rawtypes")
	public void setSearchColumns(List<searchColumnDTO> searchColumns) {
		this.searchColumns = searchColumns;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

}
