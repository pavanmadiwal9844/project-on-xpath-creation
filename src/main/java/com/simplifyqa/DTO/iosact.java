package com.simplifyqa.DTO;


public class iosact {

	private String action = null;
	private Options options = new Options();


	public iosact(String string, Options options2) {
		this.action=string;
		this.options=options2;
	}

	public iosact() {
		// TODO Auto-generated constructor stub
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Options getOptions() {
		return options;
	}

	public void setOptions(Options options) {
		this.options = options;
	}

}
