package com.simplifyqa.DTO;

public class FindElement {

	private String using = null;
	private String value = null;

	public String getUsing() {
		return using;
	}

	public void setUsing(String using) {
		this.using = using;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
