package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;

public class DumpOBject {

	private List<JsonNode> attributes = new ArrayList<JsonNode>();
	private String action = null;
	private String text = null;
	private String value = null;

	public List<JsonNode> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JsonNode> attributes) {
		this.attributes = attributes;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}





}
