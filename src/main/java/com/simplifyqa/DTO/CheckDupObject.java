package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public class CheckDupObject {

	public List<JsonNode> attributes = new ArrayList<JsonNode>();
	private Integer customerId = null;
	private Integer projectId = null;
	private String disname = null;

	public String getDisname() {
		return disname;
	}

	public void setDisname(String disname) {
		this.disname = disname;
	}

	public List<JsonNode> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JsonNode> attributes) {
		this.attributes = attributes;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

}
