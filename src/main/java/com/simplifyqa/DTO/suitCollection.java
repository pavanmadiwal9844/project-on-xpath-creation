package com.simplifyqa.DTO;

public class suitCollection {

	private String execplanScenarioId = null;
	private Integer suiteId = null;
	private Integer seq = null;

	public String getExecplanScenarioId() {
		return execplanScenarioId;
	}

	public void setExecplanScenarioId(String execplanScenarioId) {
		this.execplanScenarioId = execplanScenarioId;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

}
