package com.simplifyqa.DTO;

public class IosStart {

	private Capabilities capabilities = new Capabilities();

	public Capabilities getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Capabilities capabilities) {
		this.capabilities = capabilities;
	}

}
