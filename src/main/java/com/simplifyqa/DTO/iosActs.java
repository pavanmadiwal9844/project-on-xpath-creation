package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

public class iosActs {

	private List<iosact> actions = new ArrayList<iosact>();

	public List<iosact> getActions() {
		return actions;
	}

	public void setActions(List<iosact> actions) {
		this.actions = actions;
	}

}
