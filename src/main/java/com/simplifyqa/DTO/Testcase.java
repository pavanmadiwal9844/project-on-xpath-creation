package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;

@JsonInclude(Include.NON_EMPTY)
public class Testcase {
	private List<JsonNode> testdata = new ArrayList<JsonNode>();
	private List<Step> steps = new ArrayList<Step>();

	public List<JsonNode> getTestdata() {
		return testdata;
	}

	public void setTestdata(List<JsonNode> testdata) {
		this.testdata = testdata;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public void setSteps(List<Step> steps) {
		this.steps = steps;
	}
}
