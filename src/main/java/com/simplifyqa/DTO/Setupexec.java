package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.simplifyqa.mobile.ios.Idevice;

@JsonInclude(Include.NON_NULL)
public class Setupexec {

	private Integer executionID;
	private String authKey;
	private Integer projectID;
	private String serverIp;
	private Integer customerID;
	private Integer testcaseID = null;
	private Integer moduleID;
	private int tcSeq;
	private String testcaseCode;
	private Integer suiteID = null;
	private int version;
	private String customIP;
	private String devicesID;
	private String mbType;
	private String tcType;
	private String browserName;
	private Integer execplanId = null;
	private Integer scenarioFolderId = null;
	private Boolean executionPlanScenario = false;
	private Integer expAssignedTo = null;
	private String expPlnnedDate = null;
	private Integer execplanexecId = null;
	private List<suitCollection> suiteIds = new ArrayList<suitCollection>();
	private String environmentType = null;
	private Integer createdBy = null;
	private static Logger logger = LoggerFactory.getLogger(Idevice.class);
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}

	public List<suitCollection> getSuiteIds() {
		return suiteIds;
	}

	public void setSuiteIds(List<suitCollection> suiteIds) {
		this.suiteIds = suiteIds;
	}

	public Integer getExecplanId() {
		return execplanId;
	}

	public void setExecplanId(Integer execplanId) {
		this.execplanId = execplanId;
	}

	public Integer getScenarioFolderId() {
		return scenarioFolderId;
	}

	public void setScenarioFolderId(Integer scenarioFolderId) {
		this.scenarioFolderId = scenarioFolderId;
	}

	public Boolean getExecutionPlanScenario() {
		return executionPlanScenario;
	}

	public void setExecutionPlanScenario(Boolean executionPlanScenario) {
		this.executionPlanScenario = executionPlanScenario;
	}

	public Integer getExpAssignedTo() {
		return expAssignedTo;
	}

	public void setExpAssignedTo(Integer expAssignedTo) {
		this.expAssignedTo = expAssignedTo;
	}

	public String getExpPlnnedDate() {
		return expPlnnedDate;
	}

	public void setExpPlnnedDate(String expPlnnedDate) {
		this.expPlnnedDate = expPlnnedDate;
	}

	public Integer getExecplanexecId() {
		return execplanexecId;
	}

	public void setExecplanexecId(Integer execplanexecId) {
		this.execplanexecId = execplanexecId;
	}

	public String getBrowserName() {
		return browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public String getTcType() {
		return tcType;
	}

	public void setTcType(String tcType) {
		this.tcType = tcType;
	}

	public String getMbType() {
		return mbType;
	}

	public void setMbType(String mbType) {
		this.mbType = mbType;
	}

	public String getDevicesID() {
		return devicesID;
	}

	public void setDevicesID(String devicesID) {
		this.devicesID = devicesID;
	}

	public String getCustomIP() {
		return customIP;
	}

	public void setCustomIP(String customIP) {
		this.customIP = customIP;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Integer getSuiteID() {
		return suiteID;
	}

	public void setSuiteID(Integer suiteID) {
		this.suiteID = suiteID;
	}

	public Integer getExecutionID() {
		return executionID;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public void setExecutionID(Integer executionID) {
		this.executionID = executionID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public Integer getTestcaseID() {
		return testcaseID;
	}

	public void setTestcaseID(Integer testcaseID) {
		this.testcaseID = testcaseID;
	}

	public Integer getModuleID() {
		return moduleID;
	}

	public void setModuleID(Integer moduleID) {
		this.moduleID = moduleID;
	}

	public int getTcSeq() {
		return tcSeq;
	}

	public void setTcSeq(int tcSeq) {
		this.tcSeq = tcSeq;
	}

	public String getTestcaseCode() {
		return testcaseCode;
	}

	public void setTestcaseCode(String testcaseCode) {
		this.testcaseCode = testcaseCode;
	}

}
