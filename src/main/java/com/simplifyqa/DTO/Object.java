package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;



public class Object {

	private List<JsonNode> attributes = new ArrayList<JsonNode>();
	private long id;
	public Object jsonkey;

	public List<JsonNode> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JsonNode> attributes) {
		this.attributes = attributes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
