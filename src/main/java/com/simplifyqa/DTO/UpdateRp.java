package com.simplifyqa.DTO;

public class UpdateRp {
	private Integer customerId;
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	private Integer projectId;
	private Integer testcaseId;
	private String key;
	private String value;
	

}
