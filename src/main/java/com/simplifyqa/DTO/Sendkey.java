package com.simplifyqa.DTO;

import java.util.ArrayList;
import java.util.List;

public class Sendkey {

	private List<String> value = new ArrayList<String>();
	private String text = null;

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
