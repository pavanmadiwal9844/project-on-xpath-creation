package com.simplifyqa.DTO;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
public class SaveObject {

	private List<JsonNode> attributes = new ArrayList<JsonNode>();

	public SaveObject(Integer customerId, Integer projectId, Integer createdBy, boolean deleted, String description,
			String disname, String pageName, Integer updatedBy, Integer objtemplateId) {
		this.customerId = customerId;
		this.projectId = projectId;
		this.createdBy = createdBy;
		this.deleted = deleted;
		this.description = description;
		this.disname = disname;
		this.pageName = pageName;
		this.updatedBy = updatedBy;
		this.objtemplateId = objtemplateId;
	}

	public List<JsonNode> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JsonNode> attributes) {
		this.attributes = attributes;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisname() {
		return disname;
	}

	public void setDisname(String disname) {
		this.disname = disname;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getObjtemplateId() {
		return objtemplateId;
	}

	public void setObjtemplateId(Integer objtemplateId) {
		this.objtemplateId = objtemplateId;
	}

	private Integer customerId = null;
	private Integer projectId = null;
	private Integer createdBy;
	private boolean deleted;
	private String description;
	private String disname;
	private String pageName;
	private Integer updatedBy;
	private Integer objtemplateId;

}
