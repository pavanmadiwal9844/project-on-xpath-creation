package com.simplifyqa.DTO;

import java.util.ArrayList;

import org.json.JSONObject;

public class WebSteps {

	private ArrayList<JSONObject> arraysteps = new ArrayList<JSONObject>();
	private ArrayList<JSONObject> steps = new ArrayList<JSONObject>();
	private Integer index ;
	private Boolean reference ;
	private Integer testcaseId;
	private Boolean status;
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Integer testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Integer getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Integer executionId) {
		this.executionId = executionId;
	}

	private Integer executionId;

	public Boolean getReference() {
		return reference;
	}

	public void setReference(Boolean reference) {
		this.reference = reference;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public ArrayList<JSONObject> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<JSONObject> steps) {
		this.steps = steps;
	}

	public ArrayList<JSONObject> getArraysteps() {
		return arraysteps;
	}

	public void setArraysteps(ArrayList<JSONObject> arraysteps) {
		this.arraysteps = arraysteps;
	}

}
