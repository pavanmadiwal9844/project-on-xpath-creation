package com.simplifyqa.customMethod;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;

@MultipartConfig
public class Classnamesfromjar extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Classnamesfromjar.class);
	public String classname = null;
	public static String fileName = null;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	private static final MultipartConfigElement MULTI_PART_CONFIG = new MultipartConfigElement(
			UserDir.getExternaljar().toString());

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String contentType = req.getContentType();
		File fileSaveDir = new File(UserDir.getExternaljar().toString());
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}
		if (contentType != null && contentType.startsWith("multipart/")) {
			req.setAttribute(Request.MULTIPART_CONFIG_ELEMENT, MULTI_PART_CONFIG);
			for (Part part : req.getParts()) {
				fileName = extractFileName(part);
				fileName = new File(fileName).getName();
				part.write(fileName);
			}
		}
		List<Class> allClasses = new Classnamesfromjar().getAllKnownClasses();
		if (allClasses.size() != 0 && allClasses != null) {
			logger.info("Class Extraction Successful");
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", new JSONObject().put(fileName, allClasses)));
		} else {
			resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("Error", "No class found in the uploaded JAR"));
		}
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	public static List<Class> getAllKnownClasses() {
		List<Class> classFiles = new ArrayList<Class>();
		File f = new File(UserDir.getExternaljar() + File.separator + fileName);
		classFiles.addAll(getClassesFromPath(f));
		return classFiles;
	}

	private static Collection<? extends Class> getClassesFromPath(File path) {
		if (path.isFile()) {
//			return getClassesFromDirectory(path);
//		} else {
			return getClassesFromJarFile(path);
		} else {
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	private static List<Class> getClassesFromJarFile(File path) {
		List<Class> classes = new ArrayList<Class>();

		try {
			if (path.canRead()) {
				try {
					JarFile jar = new JarFile(path);
					Enumeration<JarEntry> en = jar.entries();
					while (en.hasMoreElements()) {
						JarEntry entry = en.nextElement();
						if (entry.getName().endsWith(".class")) {
							String className = fromFileToClassName(entry.getName());
//							System.out.println(className);
							Class claz = null;
							try {
								claz = new URLClassLoader(new URL[] { path.toURL() }).loadClass(className);
								System.out.println(claz);
								classes.add(claz);
							} catch (Exception e) {
								e.printStackTrace();
							} catch (NoClassDefFoundError e) {
//								e.printStackTrace();
							} catch (UnsupportedClassVersionError e) {
//								e.printStackTrace();
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to read classes from jar file: " + path, e);
		}
		return classes;
	}

	private static String fromFileToClassName(final String fileName) {
		return fileName.substring(0, fileName.length() - 6).replaceAll("/|\\\\", "\\.");
	}

}
