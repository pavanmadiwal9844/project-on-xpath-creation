package com.simplifyqa.customMethod;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;
import org.python.antlr.ast.Str;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.ObjectInspector.objectservice;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.EditorContent;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.execution.ExecutionService;

import net.bytebuddy.asm.Advice.This;

public class Editorclassloader extends ClassLoader {

	@SuppressWarnings("rawtypes")
	private Class myObjectClass;
	private static Class custom = null;
	public String classname = null;
	private Logger logger = LoggerFactory.getLogger(Editorclassloader.class);
	private static final String CLASS_SUFFIX = ".class";

	public Editorclassloader(ClassLoader parent) {
		super(parent);
	}

	public Boolean loadClass(@SuppressWarnings("rawtypes") Class myObjectClass) {
		try {
			System.out.println("my bject class");
			this.myObjectClass = myObjectClass;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "resource", "hiding", "deprecation" })
	public Class loadclass(String name, Setupexec executiondetail, HashMap<String, String> header)
			throws ClassNotFoundException, ClientProtocolException, IOException, InterruptedException {
//		String error;
//		if (!name.contains("csmethods.CustomMethods"))
//			return super.loadClass(name);
//		classname = name.split("\\.")[1];
//		if (name.equals("csmethods.CustomMethodsandroid")) {
//			error = getCustomMethod(classname);
//		} else {
//			error = getCustomMethod(classname, executiondetail, header);
//		}
//		if (error.equals("")) {
		Thread.sleep(2000);
		try {
			ArrayList<String> c = new ArrayList<String>();
			String contents[] = UserDir.Codeeditor().list();
			//String contents[] = UserDir.getExternaljar().list();
			for (int i = 0; i < contents.length; i++) {
				if (contents[i].contains(".jar"))
					c.add(contents[i]);
			}
			ArrayList<String> jarlist = new ArrayList<String>();
			for (String num : c) {
				//jarlist.add(UserDir.getExternaljar().toURL() + num);
				jarlist.add(UserDir.Codeeditor().toURL() + num);
			}
			URL[] myUrl = new URL[jarlist.size()];
			for (int i = 0; i < jarlist.size(); i++) {
				myUrl[i] = new URL(jarlist.get(i));
			}
			if (myUrl.length != 0)
				return new URLClassLoader(myUrl).loadClass(name);
			else {
//					myUrl= new URL[0];
//					myUrl[0]=new URL(UserDir.getExternaljar().getAbsolutePath());
				return new URLClassLoader(new URL[] { UserDir.getExternaljar().toURL() }).loadClass(name);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		}
		System.out.println("Compilation error");
		return null;
	}

	private String getCustomMethod(String classname)
			throws JSONException, ParseException, ClientProtocolException, IOException {
		String serverVersion;
		String customMethod;
		Setupexec executionD = new Setupexec();
		executionD.setAuthKey(GlobalDetail.refdetails.get("authkey"));
		executionD.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
		executionD.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
		executionD.setServerIp(GlobalDetail.refdetails.get("executionIp"));
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("content-type", "application/json");
		System.out.println(Thread.currentThread().getName());
		headers.put("authorization", executionD.getAuthKey());
		if (executionD.getCustomIP() == null) {
			JSONObject response = HttpUtility.get(executionD.getServerIp() + "/publicData/custommethodUrl", null,
					headers);
			executionD.setCustomIP(response.getString("data"));
		}
		String localversion = null;
		File versionfile = new File(
				Paths.get("libs" + File.separator + "External_Jar" + File.separator + "version.txt").toString());
		if (versionfile.exists()) {
			localversion = new String(Files.readAllBytes(
					Paths.get("libs" + File.separator + "External_jar" + File.separator + "version.txt")));
		} else
			localversion = "";
		JSONObject getMethod = HttpUtility.get(
				executionD.getCustomIP() + "/v1/custommethod/" + executionD.getCustomerID() + "/android", null,
				headers);
		serverVersion = getMethod.getJSONObject("method").getString("version");
		if (localversion.equals("") || localversion != serverVersion) {
			JSONObject methodvalue = HttpUtility.get(
					executionD.getCustomIP() + "/v1/custommethod/" + executionD.getCustomerID() + "/android", null,
					headers);
			customMethod = (String) methodvalue.getJSONObject("method").getJSONArray("methodString").get(0);
			new EditorContent().downloadMethods(customMethod, classname + ".java", serverVersion,
					executionD.getCustomIP(), executionD.getCustomerID());
			return new EditorContent().custommethodCompile(classname + ".java");
		} else {
			return new EditorContent().custommethodCompile(classname + ".java");
		}
	}

	private String getCustomMethod(String classname, Setupexec executiondetail, HashMap<String, String> header)
			throws JSONException, ParseException, ClientProtocolException, IOException {
		String serverVersion;
		String customMethod;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("content-type", "application/json");
		if (executiondetail != null)
			headers.put("authorization", executiondetail.getAuthKey());
		else {
			System.out.println(GlobalDetail.refdetails.get("authkey"));
			headers.put("authorization", GlobalDetail.refdetails.get("authkey").toString());
		}
		String type = null;
		String customIP = null;
		if (classname.contains("mainframe"))
			type = "mainframe";
		else if (classname.contains("ios"))
			type = "ios";
		else
			type = "web";
		if (executiondetail != null) {
			if (executiondetail.getCustomIP() == null) {
				JSONObject response = HttpUtility.get(executiondetail.getServerIp() + "/publicData/custommethodUrl",
						null, headers);
				customIP = response.getString("data");
			}
		} else {
			JSONObject response = HttpUtility
					.get(GlobalDetail.refdetails.get("executionIp") + "/publicData/custommethodUrl", null, headers);
			System.out.println(response.toString());
			customIP = response.getString("data");
		}
		String localversion = null;
		File versionfile = new File(
				Paths.get("libs" + File.separator + "External_Jar" + File.separator + "version.txt").toString());
		if (versionfile.exists()) {
			localversion = new String(Files.readAllBytes(
					Paths.get("libs" + File.separator + "External_jar" + File.separator + "version.txt")));
		} else
			localversion = "";
		JSONObject getMethod;
		if (executiondetail != null) {
			getMethod = HttpUtility.get(customIP + "/v1/custommethod/" + executiondetail.getCustomerID() + "/" + type,
					null, headers);
		} else {
			getMethod = HttpUtility.get(
					customIP + "/v1/custommethod/" + GlobalDetail.refdetails.get("customerId") + "/" + type, null,
					headers);
		}
		serverVersion = getMethod.getJSONObject("method").getString("version");
		if (localversion.equals("") || !localversion.equals(serverVersion)) {
			JSONObject methodvalue;
			if (executiondetail != null) {
				methodvalue = HttpUtility.get(
						customIP + "/v1/custommethod/" + executiondetail.getCustomerID() + "/" + type, null, headers);
				customMethod = (String) methodvalue.getJSONObject("method").getJSONArray("methodString").get(0);
				new EditorContent().downloadMethods(customMethod, classname + ".java", serverVersion, customIP,
						executiondetail.getCustomerID());
				return new EditorContent().custommethodCompile(classname + ".java");
			} else {
				methodvalue = HttpUtility.get(
						customIP + "/v1/custommethod/" + GlobalDetail.refdetails.get("customerId") + "/" + type, null,
						headers);
				customMethod = (String) methodvalue.getJSONObject("method").getJSONArray("methodString").get(0);
				new EditorContent().downloadMethods(customMethod, classname + ".java", serverVersion, customIP,
						Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				return new EditorContent().custommethodCompile(classname + ".java");
			}

		} else {
			return new EditorContent().custommethodCompile(classname + ".java");
		}
	}

	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public boolean ExecustomMethod(String MethodName, String[] value, String classname) {
		try {
			logger.info("Executing CustomMethod by first approach");
			Object obj = myObjectClass.newInstance();
			Field[] fields = myObjectClass.getDeclaredFields();
			com.simplifyqa.DTO.Object currentObject;
			for (Field field : fields) {

				if (field.getDeclaredAnnotationsByType(SqaAutowired.class).length > 0) {
//					if (classname.contains("android")) {
					currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
//					} else {
//						currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
//					}
					field.setAccessible(true);
					field.set(obj, currentObject);
				}
			}
			if (value == (null)) {
				Method method = myObjectClass.getDeclaredMethod(MethodName);
				return invokemethod(method, obj, value);
			} else {
				Class[] cArg = new Class[value.length];
				for (int i = 0; i < value.length; i++) {
					cArg[i] = String.class;
				}
				Method method = myObjectClass.getDeclaredMethod(MethodName, cArg);
				Type type[] = method.getGenericParameterTypes();
				System.out.println();
				return invokemethod(method, obj, value);
			}
		} catch (Exception e) {
			logger.info("Executing CustomMethod by second approach");
			try {
				Class<?> userDefineClass = Class.forName("com.simplifyqa.custommethod.UserDefindedMethods");
				Object obj = userDefineClass.newInstance();
				Field[] fields = userDefineClass.getDeclaredFields();
				com.simplifyqa.DTO.Object currentObject;
				for (Field field : fields) {

					if (field.getDeclaredAnnotationsByType(SqaAutowired.class).length > 0) {
						if (classname.contains("android")) {
							currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
						} else {
							currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
						}
						field.setAccessible(true);
						field.set(obj, currentObject);
					}
				}
				if (value == (null)) {
					Method method = userDefineClass.getDeclaredMethod(MethodName);
					return invokemethod(method, obj, value);
				} else {
					Class[] cArg = new Class[value.length];
					for (int i = 0; i < value.length; i++) {
						cArg[i] = String.class;
					}
					Method method = userDefineClass.getDeclaredMethod(MethodName, cArg);
					Type type[] = method.getGenericParameterTypes();
					return invokemethod(method, obj, value);
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
					| SecurityException | MalformedURLException | InterruptedException e1) {
				e1.printStackTrace();
				return false;
			}

		}
	}

	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public boolean ExecustomMethodwithproperdatatype(String MethodName, String[] value, String classname,
			Class[] objectypes) {
		try {
			classname = classname.trim();
			MethodName = MethodName.trim();
			logger.info("Executing CustomMethod by first approach");
			Object obj = myObjectClass.newInstance();
			
			
			for (Method method : myObjectClass.getDeclaredMethods()) {
				Class<?>[] method_class = method.getParameterTypes();
				if(method.getName().equals(MethodName)) {
					
			for (int k = 0; k < method.getParameters().length; k++) {
//				
				if(method_class[k].toString().equals("float") && objectypes[k].equals(Float.class)) {
					objectypes[k]=float.class;
				}
				
				if(method_class[k].toString().equals("double") && objectypes[k].equals(Double.class)) {
					System.out.println(objectypes[k].toString());
					objectypes[k]=double.class;
				}
			}
				}
			}
			
			


			Field[] fields = myObjectClass.getDeclaredFields();
			com.simplifyqa.DTO.Object currentObject;
			for (Field field : fields) {

				if (field.getDeclaredAnnotationsByType(SqaAutowired.class).length > 0) {
//					if (classname.contains("android")) {
					currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
//					} else {
//						currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
//					}
					field.setAccessible(true);
					field.set(obj, currentObject);
				}
			}
			if (value == (null)) {
				Method method = myObjectClass.getDeclaredMethod(MethodName);
				return invokemethod(method, obj, value);
			} else {
				Class[] cArg = new Class[value.length];
				for (int i = 0; i < value.length; i++) {
					cArg[i] = String.class;
				}
				Method method = myObjectClass.getDeclaredMethod(MethodName, objectypes);
				Type type[] = method.getGenericParameterTypes();
				System.out.println("Done");

				Object[] object = new Object[value.length];

				for (int j = 0; j < value.length; j++) {
					object[j] = value[j].toString();
				}
//				return invokemethod(method, obj, value);
				return invokemethodalldatatype(method, obj, value, objectypes);
			}
		} catch (Exception e) {
			logger.info("EXCEPTION FOR NOT WORKING : " + e.toString());
			logger.info("Executing CustomMethod by second approach");
			try {
				Class<?> userDefineClass = Class.forName("com.simplifyqa.custommethod.UserDefindedMethods");
				Object obj = userDefineClass.newInstance();
				Field[] fields = userDefineClass.getDeclaredFields();
				com.simplifyqa.DTO.Object currentObject;
				for (Field field : fields) {

					if (field.getDeclaredAnnotationsByType(SqaAutowired.class).length > 0) {
						if (classname.contains("android")) {
							currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
						} else {
							currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
						}
						field.setAccessible(true);
						field.set(obj, currentObject);
					}
				}
				if (value == (null)) {
					Method method = userDefineClass.getDeclaredMethod(MethodName);
					return invokemethod(method, obj, value);
				} else {
					Class[] cArg = new Class[value.length];
					for (int i = 0; i < value.length; i++) {
						cArg[i] = String.class;
					}
					Method method = userDefineClass.getDeclaredMethod(MethodName, cArg);
					Type type[] = method.getGenericParameterTypes();
					return invokemethod(method, obj, value);
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
					| SecurityException | MalformedURLException | InterruptedException e1) {
				e1.printStackTrace();
				return false;
			}

		}
	}

	public JSONObject ExecustomMethodDpMf(String MethodName, String[] value, String classname) {
		try {
			boolean stepResult;
			Object obj = myObjectClass.newInstance();
			Field[] fields = myObjectClass.getDeclaredFields();
			com.simplifyqa.DTO.Object currentObject;
			for (Field field : fields) {

				if (field.getDeclaredAnnotationsByType(SqaAutowired.class).length > 0) {
					if (classname.contains("android")) {
						currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
					} else {
						currentObject = GlobalDetail.object.get(Thread.currentThread().getName());
					}
					field.setAccessible(true);
					field.set(obj, currentObject);
				}
			}
			if (value == (null)) {
				@SuppressWarnings("unchecked")
				Method method = myObjectClass.getDeclaredMethod(MethodName);
				return (JSONObject) method.invoke(obj, value);

			} else {
				Class[] cArg = new Class[value.length];
				for (int i = 0; i < value.length; i++) {
					cArg[i] = String.class;
				}
				Method method = myObjectClass.getDeclaredMethod(MethodName, cArg);
				Type type[] = method.getGenericParameterTypes();
				return (JSONObject) method.invoke(obj, value);

			}
		} catch (

		Exception e) {
//			e.printStackTrace();
			try {
				Class<?> c = Class.forName("com.simlifyqa.SeleniumFW.UserDefinedMethods");
				return null;
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return null;
			}

		}
	}

	public boolean invokemethod(Method method, Object obj, String[] value)
			throws InterruptedException, MalformedURLException {
		try {

//			Object[] object= new Object[value.length];

//			return (boolean) method.invoke(obj,new Locale(value[0],Integer.parseInt(value[1])));
			return (boolean) method.invoke(obj, value);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return false;
		}
	}

	// method for invoking methods of custom class (all data types accepted)
	public boolean invokemethodalldatatype(Method method, Object obj, String[] value, Class[] objecttype)
			throws InterruptedException, MalformedURLException {
		try {
			

			Object[] object = new Object[value.length];
			Class<?>[] method_class = method.getParameterTypes();

			for (int i = 0; i < value.length; i++) {
				if (objecttype[i].equals(String.class)) {
					object[i] = value[i];
					continue;
				}

				if (objecttype[i].equals(int.class)) {
					object[i] = Integer.parseInt(value[i]);

					continue;
				}

				if (objecttype[i].equals(boolean.class)) {
					object[i] = Boolean.parseBoolean(value[i]);
					continue;
				}

				if (objecttype[i].equals(Float.class) || objecttype[i].equals(float.class)) {
					object[i] = Float.parseFloat(value[i]);
					continue;
				}

				if (objecttype[i].equals(Double.class) || objecttype[i].equals(double.class)) {
					object[i] = Double.parseDouble(value[i]);
					continue;
				}
			}
			method.setAccessible(true);
			return (boolean) method.invoke(obj, object);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			return false;
		}

	}

	public static boolean custommethodpresent(String customclassname, int parameters, String[] objecttypes) {

		try {

			ArrayList<String> c = new ArrayList<String>();
			String contents[] = UserDir.Codeeditor().list();
			ArrayList<String> paths = new ArrayList<String>();

			for (int i = 0; i < contents.length; i++) {
				System.out.println(contents[i]);
				if (contents[i].endsWith(".jar")) {
					c.add(contents[i]);
					paths.add(
							Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Codeeditor", contents[i] })
									.toString());
				}
			}
			ArrayList<String> jarlist = new ArrayList<String>();
			for (String num : c) {
				try {
					jarlist.add(UserDir.Codeeditor().toURL() + num);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			URL[] jarUrl = new URL[jarlist.size()];
			URLClassLoader[] loader = new URLClassLoader[jarlist.size()];

			for (int i = 0; i < jarlist.size(); i++) {
				jarUrl[i] = new URL(jarlist.get(i));
				loader[i] = new URLClassLoader(new URL[] { jarUrl[i] });
			}

			for (int i = 0; i < contents.length; i++) {

				JarFile jar = new JarFile(paths.get(i));

				Set<Class<?>> classes = new HashSet<>();

				for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
					JarEntry entry = entries.nextElement();
					String file = entry.getName();

					if (file.endsWith(".class") && file.startsWith("SimplifyQACodeeditor")) {
						System.out.println(file);
						String classname = file.replace('/', '.').substring(0, file.length() - 6).split("\\$")[0];

						try {
							Class<?> ce = loader[i].loadClass(classname);

							System.out.println("class loaded");
							classes.add(ce);

							System.out.println(classes.toString());
							List<Method> result = new ArrayList<Method>();
							for (Method method : ce.getDeclaredMethods()) {
								int modifiers = method.getModifiers();
								System.out.println(method.getName());
//								
								if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)
										|| Modifier.isPrivate(modifiers)) {
									if (method.getName().equals(customclassname.trim())
											&& parameters == method.getParameters().length) {
										Class<?>[] method_class = method.getParameterTypes();
										for (int k = 0; k < method.getParameters().length; k++) {
//											
											if(method_class[k].toString().equals("float") && objecttypes[k].equals("class java.lang.Float")) {
												objecttypes[k]="float";
											}
											
											if(method_class[k].toString().equals("double") && objecttypes[k].equals("class java.lang.Double")) {
												objecttypes[k]="double";
											}
											
											
											
											System.out.println("method class : " + method_class[k]);
											System.out.println("object_class : " + objecttypes[k]);
											if (method_class[k].toString().equals(objecttypes[k]))
												continue;
											else {
												System.out.println("param types dont match");
												return false;
											}
										}
										InitializeDependence.runtime_classname = classname;
										System.out.println(
												"current runtime class :" + InitializeDependence.runtime_classname);
										return true;
									}
								} else {
									System.out.println("not public or protected");
								}
							}

						} catch (Throwable e) {
							System.out.println("WARNING: failed to instantiate " + classname + " from " + file);
						}
					} else {
						continue;
					}
				}

			}

			return false;
		} catch (Exception e) {
			return false;

			// TODO: handle exception
		}
	}

	public static Set<String> getClassNamesFromJarFile(String filepath)
			throws IOException, SecurityException, ClassNotFoundException {

		File givenFile = new File(filepath);
		Set<String> classNames = new HashSet<>();
		try (JarFile jarFile = new JarFile(givenFile)) {
			Enumeration<JarEntry> e = jarFile.entries();
			while (e.hasMoreElements()) {
				JarEntry jarEntry = e.nextElement();
				if (jarEntry.getName().endsWith(".class") && jarEntry.getName().startsWith("com/android")) {
					System.out.println(jarEntry.getName() + ".class");

					String className = jarEntry.getName().replace("/", ".").replace(".class", "");

					classNames.add(className);
				}

			}

			classcheck(classNames);
			return classNames;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return classNames;
		}
	}

	public static boolean classcheck(Set<String> clssnames) {

		for (String classnames_ce : clssnames) {
			try {
				Class<?> userDefineClass = Class.forName(classnames_ce);
				Object obj = userDefineClass.newInstance();
				Field[] fields = userDefineClass.getDeclaredFields();

				return true;
//				getmethodsnameclass(classnames_ce);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return false;

			}

		}

		return true;
	}

//	public static boolean getmethodsnameclass(String clazz) throws ClassNotFoundException {
//		try {
//		URL myUrl = new URL(clazz); 
//		URLClassLoader(myUrl).loadClass(clazz);
//		List<Method> result = new ArrayList<Method>();
//	    while (class_ce != null) {
//	        for (Method method : class_ce.getDeclaredMethods()) {
//	            int modifiers = method.getModifiers();
//	            if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)) {
//	                result.add(method);
//	            }
//	        }
//	        class_ce = class_ce.getSuperclass();
//	    }
//	    
//	    return true;
//		}
//		
//		catch (Exception e) {
//			// TODO: handle exception
//			return false;
//		}
//		
//	}
//	

}
