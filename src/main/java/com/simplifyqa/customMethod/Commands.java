package com.simplifyqa.customMethod;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.json.JSONArray;
import org.json.JSONObject;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.Stopdebug;

public class Commands extends Thread {
	public static ProcessBuilder builder;
	public static Process process;
	public static JSONArray line;
	public static String type;
	public static BufferedWriter buf;
	public static InputStream out;
	public static OutputStream in;
	public static StringBuffer status = new StringBuffer();
	InputStream err;

	public void run() {
		builder = new ProcessBuilder("jdb", "-attach", "9000");
		builder.directory(UserDir.getAapt());
//		builder.redirectOutput(ProcessBuilder.Redirect.to(new File("debugattach.log")));
//		builder.redirectError(ProcessBuilder.Redirect.to(new File("debugerrattach.log")));
		try {

			process = builder.start();

			Thread.sleep(2000);
			System.out.println("jdb attach 9000");
			out = process.getInputStream();
			in = process.getOutputStream();
			err = process.getErrorStream();
			buf = new BufferedWriter(new OutputStreamWriter(in));
			byte[] buffer = new byte[4000];
			int no = out.available();
			if (no > 0) {
				int n = out.read(buffer, 0, Math.min(no, buffer.length));
				System.out.println(new String(buffer, 0, n));
			}
//			System.out.println(line.length());
			for (int i = 0; i < line.length(); i++) {
				buf.write("stop at csmethods.CustomMethods" + type + ":" + line.getInt(i));
				System.out.println("stop at csmethods.CustomMethods" + type + ":" + line.getInt(i));
				buf.newLine();
				buf.flush();
				buffer = new byte[4000];
				no = out.available();
				if (no > 0) {
					int n = out.read(buffer, 0, Math.min(no, buffer.length));
					System.out.println(new String(buffer, 0, n));
				}
			}
			buf.write("run");
			System.out.println("run");
			status = new StringBuffer();
			buf.newLine();
			buf.flush();
			Thread.sleep(4000);
			buffer = new byte[4000];
			no = out.available();
			if (no > 0) {
				int n = out.read(buffer, 0, Math.min(no, buffer.length));
				System.out.println("someting" + new String(buffer, 0, n));
				status.append(new String(buffer, 0, n));
			}
			if (status.toString().contains("Nothing suspended")) {
				new Stopdebug().stopDebug();
			} else {
				livecommands();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static String linenumber = null;
	static StringBuffer variables = new StringBuffer();

	public static void livecommands() throws IOException, InterruptedException {
		String debugline = null;
		String debugvar = null;
		boolean processalive = false;
		if (process.isAlive()) {
			processalive = true;
			System.out.println("alive");
		}
		while (processalive == true) {
//			System.out.println("while");
			byte[] dbuffer = new byte[4000];
			int nos = out.available();
			if (nos > 0) {
				int n = out.read(dbuffer, 0, Math.min(nos, dbuffer.length));
				status.append(new String(dbuffer, 0, n));
			}
//				status = "Breakpoint hit: thread=main, csmethods.CustomMethods.dummy(), line=6 bci=3";
			StringBuffer linevar = new StringBuffer();
			boolean end = false;
			if (status.toString().contains("line=") && status.toString().contains("Breakpoint hit")
					|| status.toString().contains("line=") || status.toString().contains("Step completed")) {
				System.out.println("socket sending");
				buf.write("locals");
				linevar = new StringBuffer();
				buf.newLine();
				buf.flush();
				Thread.sleep(2000);
				byte[] buffer = new byte[4000];
				int no = out.available();
				if (status != null && status.toString().contains("line=")) {
					String li[] = status.toString().split("line=");
					String nu[] = li[1].split(" ");
					linenumber = null;
					linenumber = nu[0];
				}
				if (no > 0) {
					int n = out.read(buffer, 0, Math.min(no, buffer.length));
					linevar.append(new String(buffer, 0, n));
					System.out.println(new String(buffer, 0, n));
					if (linevar.toString().contains("line=") && linevar.toString().contains("Breakpoint hit:")) {
						String li[] = linevar.toString().split("line=");
						String nu[] = li[1].split(" ");
						linenumber = null;
						linenumber = nu[0];
					} else if (linevar.toString().contains("line=")) {
						String li[] = linevar.toString().split("line=");
						String nu[] = li[1].split(" ");
						linenumber = null;
						linenumber = nu[0];
					}
					variables = new StringBuffer();
					if (linevar.toString().contains("Local variables:")) {
						String[] s = linevar.toString().split("Local variables:");
						if (s.length > 1) {
							variables.append(s[1]);
						}
					} else if (status.toString().contains("Local variables")) {
						String[] s = status.toString().split("Local variables:");
						variables.append(s[1]);
					} else {
						variables.append("no local variable");
					}
				}
				if (linenumber != null) {
					if (Integer.parseInt(linenumber) >= line.getInt(line.length() - 1) + 1) {
						end = true;
					}
				}
				System.out.println(linenumber + "" + variables);
				debugline = linenumber;
				debugvar = variables.toString();
				JSONObject s = new JSONObject();
				s.put("line", debugline);
				s.put("variable", debugvar);
				if (end == true) {
					s.put("end", "End point reached");
				} else {
					s.put("end", end);
				}
				GlobalDetail.debugSession.getBasicRemote().sendText(s.toString());
				processalive = false;
			} else if (status.toString().contains("Nothing suspended")) {
				new Stopdebug().stopDebug();
			}
		}
	}

	public static void stepOver() throws IOException, InterruptedException {
		buf.write("next");
		System.out.println("step up");
		status = new StringBuffer();
		buf.newLine();
		buf.flush();
		Thread.sleep(300);
		byte[] buffer = new byte[4000];
		int no = out.available();
		if (no > 0) {
			int n = out.read(buffer, 0, Math.min(no, buffer.length));
			status.append(new String(buffer, 0, n));
			System.out.println(new String(buffer, 0, n));
		}
		livecommands();
	}

	public static void continueNextpoint() throws IOException, InterruptedException {
		buf.write("cont");
		status = new StringBuffer();
		System.out.println("cont");
		buf.newLine();
		buf.flush();
		byte[] buffer = new byte[4000];
		int no = out.available();
		System.out.println("No" + no);
		if (no > 0) {
			int n = out.read(buffer, 0, Math.min(no, buffer.length));
			status.append(new String(buffer, 0, n));
			System.out.println(new String(buffer, 0, n));
		}
		livecommands();
	}
}