package com.simplifyqa.customMethod;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;

public class Methodnamesfromjar extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Classnamesfromjar.class);
	public String classname = null;
	public static String jarname = null;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONObject resobj = new JSONObject(jb.toString());
		classname = resobj.getString("classname");
		jarname = resobj.getString("jarname");
		List<Method> allMethods = null;
		try {
			allMethods = new Methodnamesfromjar().methods(new File(UserDir.getExternaljar() + File.separator + jarname),
					classname);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (allMethods.size() != 0 && allMethods != null) {
			logger.info("Method Extraction done");
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", new JSONObject().put(classname, allMethods)));
		} else {
			resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", "Upload the dependency JAR's"));
		}
	}

	@SuppressWarnings("resource")
	public List<Method> methods(File path, String classname) throws ClassNotFoundException, MalformedURLException {
		@SuppressWarnings("deprecation")
		Class<?> claz = new URLClassLoader(new URL[] { path.toURL() }).loadClass(classname);
		List<Method> methods = new ArrayList<Method>();
		for (Method m : claz.getDeclaredMethods()) {
			System.out.println(m.toGenericString());
			methods.add(m);
		}
		return methods;
	}

}
