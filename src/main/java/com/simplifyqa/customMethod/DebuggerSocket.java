package com.simplifyqa.customMethod;

import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.simplifyqa.agent.GlobalDetail;

@ServerEndpoint(value = "/Debug")
public class DebuggerSocket {
	@OnOpen
	public void onOpen(Session session) throws IOException {
		System.out.println("WebSocket opened: " + session.getId());
	}

	@OnMessage
	public void onMessage(String msg, Session session) {
		GlobalDetail.debugSession = session;
	}

	@OnClose
	public void onClose(CloseReason reason, Session session) {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
	}
}
