package com.simplifyqa.customMethod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.Utility.HttpUtility;

public class Maven extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		String groupid = resobj.getString("groupid");
		String artifact = resobj.getString("artifactid");
		String version = resobj.getString("mavenjarversion");
		String[] splittedartifact = artifact.split("\\.");

		String jarname = splittedartifact[splittedartifact.length - 1];
		if (artifact.contains(".")) {
			artifact = artifact.replace(".", "/");
		}
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		InputStream in = null;
		try {
			in = HttpUtility.getFile("https://repo1.maven.org/maven2/" + groupid + "/" + artifact + "/" + version + "/"
					+ jarname + "-" + version + ".jar", null, headers);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(HttpStatus.NO_CONTENT_204);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("error", "download failed"));
		}
		int inByte;
		FileOutputStream fout = new FileOutputStream(new File(Paths.get(".") + File.separator + "libs" + File.separator
				+ "External_Jar" + File.separator + jarname+"-"+version + ".jar"));
		while ((inByte = in.read()) != -1)
			fout.write(inByte);
		fout.close();
		Classnamesfromjar classnames = new Classnamesfromjar();
		classnames.fileName = jarname + ".jar";
		List<Class> allClasses = classnames.getAllKnownClasses();
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(new JSONObject().put("data", allClasses));
	}
}
