package com.simplifyqa.customMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.json.JSONArray;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Sqabind {
	String actionname();
	String action_displayname();
	String action_description();
	String paraname_equals_curobj();
	String[] paramnames();
	String[] paramtypes();
	String object_template()	;


}
