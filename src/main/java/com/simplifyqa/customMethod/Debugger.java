package com.simplifyqa.customMethod;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;

public class Debugger extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int processid;
	public static ProcessBuilder processs;
	public static Process p;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		System.out.println(resobj.toString());
		JSONArray lines = resobj.getJSONArray("debugline");
		String type = resobj.getString("type");
		if (SystemUtils.IS_OS_WINDOWS) {
			processs = new ProcessBuilder("java", "-agentlib:jdwp=transport=dt_shmem,server=y,address=9000",
					"-Dport=4013", "-cp", Paths.get("libs").toString() + File.separator + "csmethods;.", "-jar",
					Paths.get("com.simplifyQA.Agent.jar").toString());
		} else if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
			processs = new ProcessBuilder("java", "-agentlib:jdwp=transport=dt_socket,address=9000,server=y",
					"-Dport=4013", "-cp", UserDir.getExternaljar().toString() + File.separator + "csmethods;.", "-jar",
					Paths.get("com.simplifyQA.Agent.jar").toString());
		}
//		processs.environment().put("JAVA_HOME", System.getProperty("java.home"));
		processs.directory(Paths.get(".").toFile());
		processs.redirectOutput(ProcessBuilder.Redirect.to(new File("debug.log")));
		processs.redirectError(ProcessBuilder.Redirect.to(new File("debugerr.log")));
		p = processs.start();
		InputStream firstout = p.getInputStream();
		OutputStream firstin = p.getOutputStream();
		InputStream firsterr = p.getErrorStream();

		System.out.println(p.isAlive());
		byte[] buffer = new byte[4000];
		int no = firstout.available();
		if (no > 0) {
			int n = firstout.read(buffer, 0, Math.min(no, buffer.length));
			System.out.println(new String(buffer, 0, n));
		}
		no = firsterr.available();
		if (no > 0) {
			int n = firsterr.read(buffer, 0, Math.min(no, buffer.length));
			System.out.println(new String(buffer, 0, n));
		}
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(p.isAlive());
		if (p.isAlive()) {
			Commands.line = lines;
			Commands.type = type;
			Commands cmd = new Commands();
			cmd.start();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(new JSONObject().put("data", "debug started"));
	}

}
