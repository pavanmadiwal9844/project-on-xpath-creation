package com.simplifyqa.desktop.impl;

import java.io.File;

import com.simplifyqa.desktop.Interface.DesktopInterface;

public class DesktopImpl implements DesktopInterface{

	@Override
	public Process startRecorderExe(String recorderExepath, String dirPath) {
		try {
		return Runtime.getRuntime().exec(recorderExepath,null,new File(dirPath));
		}catch (Exception e) {
			return null;
		}
	}

	@Override
	public Process startPlaybackExe(String playbackExepath, String dirPath) {
		try {
			return Runtime.getRuntime().exec(playbackExepath,null,new File(dirPath));
			}catch (Exception e) {
				return null;
			}
	}

	@Override
	public Boolean stopRecorderExe(Process recorderProcess) {
		try {
			recorderProcess.destroyForcibly();
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	@Override
	public Boolean stopPlaybackExe(Process playbackProcess) {
		try {
			playbackProcess.destroyForcibly();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public Boolean stopPlaybackExe(String playbackProcess) {
		try {
			Runtime.getRuntime().exec("taskkill /F /IM " + playbackProcess);
			return true;
		}catch (Exception e) {
			return false;
		}
	}


	

}
