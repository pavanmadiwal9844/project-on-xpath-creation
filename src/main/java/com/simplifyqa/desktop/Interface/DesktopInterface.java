package com.simplifyqa.desktop.Interface;

public interface DesktopInterface {

	public Boolean stopRecorderExe(Process recorderProcess);
	public Boolean stopPlaybackExe(Process playbackProcess);
	public Process startRecorderExe(String recorderExepath, String dirPath);
	public Process startPlaybackExe(String playbackExepath, String dirPath);
}
