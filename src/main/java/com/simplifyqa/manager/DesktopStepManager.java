package com.simplifyqa.manager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.CheckDupObject;
import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.DTO.SaveObject;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.desktoprecorder.DTO.OriginalData;
import com.simplifyqa.desktoprecorder.DTO.Parameter;
import com.simplifyqa.desktoprecorder.DTO.Step;
import com.simplifyqa.desktoprecorder.DTO.Testcase;

public class DesktopStepManager {

	public static org.slf4j.Logger logger = LoggerFactory.getLogger(DesktopStepManager.class);

	public JSONObject checkDupobject(DumpOBject dBject) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.user.getAuthkey());
			CheckDupObject CheckDup = new CheckDupObject();
			CheckDup.setAttributes(dBject.getAttributes());
			CheckDup.setCustomerId(GlobalDetail.user.getCustomerId());
			CheckDup.setProjectId(GlobalDetail.user.getProjectId());
			System.out.println(new ObjectMapper().writeValueAsString(CheckDup));
			logger.info(new ObjectMapper().writeValueAsString(CheckDup));
			JSONObject response = new JSONObject((HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/objectdup",
					new ObjectMapper().writeValueAsString(CheckDup), hash)));

			if (response.getJSONArray("data").length() == 0) {
				System.out.println("save object and send to angular");
				logger.info("save object and send to angular");
			} else {
				System.out.println("Send object to angular");
				logger.info("Send object to angular");
			}
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	public DumpOBject DumpObject(DumpOBject dmpobj, JSONObject object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			dmpobj = mapper.readValue(object.toString(), DumpOBject.class);
			logger.info(new ObjectMapper().writeValueAsString(dmpobj));
			System.out.println(new ObjectMapper().writeValueAsString(dmpobj));
			return dmpobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject SearchObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.user.getAuthkey());

			SearchColumns searchs = new SearchColumns();
			searchs.getSearchColumns()
					.add(new searchColumnDTO("customerId", GlobalDetail.user.getCustomerId().toString(),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
			searchs.getSearchColumns().add(new searchColumnDTO("projectId", GlobalDetail.user.getProjectId().toString(),
					Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
			if ((dmp.getText().contains("+"))) {
				String str = dmp.getText().replace('+', '_');
				searchs.getSearchColumns().add(
						new searchColumnDTO("disname", str, Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
			} else {
				searchs.getSearchColumns().add(new searchColumnDTO("disname", dmp.getText(),
						Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
			}

			searchs.setCollection("object");
			searchs.setLimit(100000);
			searchs.setStartIndex(0);
			JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
			JSONObject deleted = new JSONObject();
			deleted.put("column", "deleted");
			deleted.put("value", false);
			deleted.put("regEx", false);
			deleted.put("type", "boolean");

			System.out.println(object.getJSONArray("searchColumns").put(deleted));
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/search", object.toString(), hash));
			logger.info(response.toString());
			System.out.println(response);

			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject SavaObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.user.getAuthkey());
			SaveObject sv = new SaveObject(GlobalDetail.user.getCustomerId(), GlobalDetail.user.getProjectId(),
					GlobalDetail.user.getCreatedBy(), false, dmp.getText(), dmp.getText(), GlobalDetail.PageName,
					GlobalDetail.user.getCreatedBy(), GlobalDetail.user.getObjtemplateId());
			sv.setAttributes(dmp.getAttributes());
			System.out.println(new ObjectMapper().writeValueAsString(sv));
			logger.info(new ObjectMapper().writeValueAsString(sv));
			JSONObject response = new JSONObject((HttpUtility.SendPost(GlobalDetail.serverIP + "/saveObject/object",
					new ObjectMapper().writeValueAsString(sv), hash)));
			System.out.println(response);
			logger.info(response.toString());
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	public void formatData(JSONObject s, Testcase tc) throws JsonProcessingException, IOException {
		Step st = new Step();
		JSONObject objm = new JSONObject();
		Parameter p = new Parameter();
		if (s.getJSONArray("parameters").length() != 0) {
			// s.getJSONArray("parameters").getJSONObject(0).get("defaultValue");
			if (s.getJSONArray("parameters").getJSONObject(0).has("defaultValue")) {
				p.setName("parm_" + new Random().nextInt(10000));
				st.getParameter().add(p);
				tc.getTestdata().add(0,
						(new ObjectMapper().readTree(new JSONObject()
								.put(p.getName(),
										s.getJSONArray("parameters").getJSONObject(0).getString("defaultValue"))
								.toString())));
			}

		}

		objm.put("disname", s.getJSONArray("selectedObject").getJSONObject(0).get("disname"));
		objm.put("attributes", s.getJSONArray("selectedObject").getJSONObject(0).get("attributes"));
		st.setObject(new ObjectMapper().readTree(objm.toString()));
		st.setAction(s.getString("action"));
		tc.getSteps().add(st);
		OriginalData or = new OriginalData();
		or.setData(tc);
		JSONObject js = new JSONObject(new ObjectMapper().writeValueAsString(or));
		System.out.println(js.toString());
		logger.info(js.toString());
		GlobalDetail.DesktopSteps = js;
	}

}
