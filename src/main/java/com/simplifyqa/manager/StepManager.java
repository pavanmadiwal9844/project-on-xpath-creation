package com.simplifyqa.manager;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.CheckDupObject;
import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.DTO.SaveObject;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.WebSteps;
import com.simplifyqa.DTO.executionData;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.chromes.Browsers;
import com.simplifyqa.agent.AgentStart;

public class StepManager {
	final static Logger logger = LoggerFactory.getLogger(StepManager.class);

	@SuppressWarnings("static-access")
	public JSONObject checkDupobject(DumpOBject dBject) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
			HttpUtility htul = new HttpUtility();
			CheckDupObject CheckDup = new CheckDupObject();
			CheckDup.setAttributes(dBject.getAttributes());
			CheckDup.setCustomerId(GlobalDetail.userDetail.getCustomerId());
			CheckDup.setProjectId(GlobalDetail.userDetail.getProjectId());
			CheckDup.setDisname(dBject.getText());
			JSONObject response = new JSONObject((htul.SendPost(GlobalDetail.serverIP + "/v1/objectdup",
					new ObjectMapper().writeValueAsString(CheckDup), hash)));

			if (response.getJSONArray("data").length() == 0) {
				logger.info("New Object:{}", dBject.getText());
			} else {
				logger.info("Duplicate Object:{}", dBject.getText());

			}
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean launchBrowser(String BrowserName, String url) {
		try {
			switch (BrowserName) {
			case "chrome":
				Browsers.chrome(url);
				break;
			case "firefox":
				Browsers.firefox(url);
				break;
			case "internetEx":
				Browsers.internetEx(url);
				break;
			case "edge":
				Browsers.edge(url);
				break;
			default:
				break;
			}

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public DumpOBject DumpObject(DumpOBject dmpobj, JSONObject object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			dmpobj = mapper.readValue(object.toString(), DumpOBject.class);
			return dmpobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject SearchObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.userDetail.getAuthkey());

			SearchColumns searchs = new SearchColumns();
			searchs.getSearchColumns()
					.add(new searchColumnDTO("customerId", GlobalDetail.userDetail.getCustomerId().toString(),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
			searchs.getSearchColumns()
					.add(new searchColumnDTO("projectId", GlobalDetail.userDetail.getProjectId().toString(),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
			searchs.getSearchColumns().add(new searchColumnDTO("disname", dmp.getText(),
					Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
			
			
			searchs.setCollection("object");
			searchs.setLimit(100000);
			searchs.setStartIndex(0);
			JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
			JSONObject deleted = new JSONObject();
			deleted.put("column", "deleted");
			deleted.put("value", false);
			deleted.put("regEx", false);
			deleted.put("type", "boolean");
			
			//added
			object.getJSONArray("searchColumns").put(deleted);
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/search", object.toString(), hash));

			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject SavaObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
			SaveObject sv = new SaveObject(GlobalDetail.userDetail.getCustomerId(),
					GlobalDetail.userDetail.getProjectId(), GlobalDetail.userDetail.getCreatedBy(), false,
					dmp.getText(), dmp.getText(), GlobalDetail.PageName, GlobalDetail.userDetail.getCreatedBy(),
					GlobalDetail.userDetail.getObjtemplateId());
			sv.setAttributes(dmp.getAttributes());
			JSONObject response = new JSONObject((HttpUtility.SendPost(GlobalDetail.serverIP + "/saveObject/object",
					new ObjectMapper().writeValueAsString(sv), hash)));
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean objectplay(JSONObject node) {
		try {
			if (node.getString("action").equals("LaunchApplication")) {
				launchBrowser(node.getString("BrowserName"), node.getString("URL"));
			} else {
				// playback object
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Boolean stepExcecutor(JSONObject testcase, WebSteps web) {
		try {
			String brName = testcase.keys().next();
			GlobalDetail.web = web;
			GlobalDetail.web.setIndex(1);
			for (int i = 0; i < testcase.getJSONObject(brName).getJSONArray("steps").length(); i++) {
				if (testcase.getJSONObject(brName).getJSONArray("steps").getJSONObject(i).has("selectedObject")) {
					playBackStep(testcase.getJSONObject(brName).getJSONArray("steps").getJSONObject(i), i, web);
				} else {
					playBackStep1(testcase.getJSONObject(brName).getJSONArray("steps").getJSONObject(i), i, web);
				}
			}
			Lauchapp(testcase);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Boolean Lauchapp(JSONObject tc) {
		try {
			String brName = tc.keys().next();
			JSONArray steps = tc.getJSONObject(brName).getJSONArray("steps");
			JSONArray td = tc.getJSONObject(brName).getJSONArray("testdata");
			for (int i = 0; i < steps.length(); i++) {
				if (steps.getJSONObject(i).getString("actionname").equals("Launch Application")) {
					for (int j = 0; j < td.length(); j++) {
						if (td.getJSONObject(i).has(
								steps.getJSONObject(i).getJSONArray("parameters").getJSONObject(0).getString("name"))) {
							launchBrowser(brName, td.getJSONObject(i).getString(steps.getJSONObject(i)
									.getJSONArray("parameters").getJSONObject(0).getString("name")));
							break;
						} else
							continue;
					}

				}
			}
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	public Boolean playBackStep(JSONObject obj, int index, WebSteps web) {
		try {
			JSONObject json = new JSONObject();
			json.put("action", obj.getString("actionname"));

			if (obj.getJSONArray("parameters").length() != 0) {
				json.put("Value", obj.getJSONArray("parameters").getJSONObject(0).getString("defaultValue"));
			}

			for (int i = 0; i < obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
					.length(); i++) {
				String name = obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
						.getJSONObject(i).getString("name");
				switch (name) {
				case "id":
					json.put("id", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "class":
					json.put("class", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "name":
					json.put("name", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "xpathName":
					json.put("xpathName", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "xpathClass":
					json.put("xpathClass", obj.getJSONArray("selectedObject").getJSONObject(0)
							.getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathId":
					json.put("xpathId", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "xpathPlaceholder":
					json.put("xpathPlaceholder", obj.getJSONArray("selectedObject").getJSONObject(0)
							.getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathText":
					json.put("xpathText", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "Xpath":
					json.put("Xpath", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "XpathNoattr":
					json.put("XpathNoattr", obj.getJSONArray("selectedObject").getJSONObject(0)
							.getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathSrc":
					json.put("xpathSrc", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "TagName":
					json.put("tag", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "type":
					json.put("type", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "FrameLocation":
					json.put("frameLocation", obj.getJSONArray("selectedObject").getJSONObject(0)
							.getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "checked":
					json.put("checked", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "css":
					json.put("css", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "cssIndex":
					json.put("cssIndex", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "scrollx":
					json.put("scrollx", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "scrolly":
					json.put("scrolly", obj.getJSONArray("selectedObject").getJSONObject(0).getJSONArray("attributes")
							.getJSONObject(i).getString("value"));
					break;
				case "windowscroll":
					json.put("windowscroll", obj.getJSONArray("selectedObject").getJSONObject(0)
							.getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				}
			}
			JSONObject playbackstep = new JSONObject();
			playbackstep.put("testdata", json);
			playbackstep.put("index", index);
			playbackstep.put("name", "Playback");
			if (obj.has("captureScreenshot")) {
				playbackstep.put("captureScreenshot", obj.getBoolean("captureScreenshot"));
			}
			web.getArraysteps().add(playbackstep);
			GlobalDetail.web = web;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public JSONObject fetchingData(JSONObject srch, String auth) {

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", auth);
		try {
			JSONObject response = new JSONObject(
					(HttpUtility.SendPost(GlobalDetail.serverIP + "/search", srch.toString(), hash)));
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject fetchingTestCases(executionData exec, String auth) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("content-type", "application/json");
		hash.put("authorization", auth);
		HttpUtility htul = new HttpUtility();

		try {
			@SuppressWarnings("unused")
			JSONObject json = new JSONObject(new ObjectMapper().writeValueAsString(exec));
			JSONObject js = new JSONObject(new ObjectMapper().writeValueAsString(exec));
			@SuppressWarnings("static-access")
			JSONObject response = new JSONObject(
					(htul.SendPost(GlobalDetail.serverIP + "/v1/executionData", js.toString(), hash)));
			return response;
		}

		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Boolean playBackStep1(JSONObject obj, int index, WebSteps web) {
		try {
			JSONObject json = new JSONObject();
			web.getSteps().add(obj);

			json.put("action", obj.getString("actionname"));
			if (obj.getJSONArray("parameters").length() != 0) {
				json.put("Value", obj.getJSONArray("parameters").getJSONObject(0).getString("defaultValue"));
			}

			for (int i = 0; i < obj.getJSONObject("object").getJSONArray("attributes").length(); i++) {
				String name = obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("name");
				switch (name) {
				case "id":
					json.put("id",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "class":
					json.put("class",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "name":
					json.put("name",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathName":
					json.put("xpathName",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathClass":
					json.put("xpathClass",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathId":
					json.put("xpathId",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathPlaceholder":
					json.put("xpathPlaceholder",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathText":
					json.put("xpathText",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "Xpath":
					json.put("Xpath",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "XpathNoattr":
					json.put("XpathNoattr",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "xpathSrc":
					json.put("xpathSrc",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "TagName":
					json.put("tag",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "type":
					json.put("type",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "FrameLocation":
					json.put("frameLocation",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "checked":
					json.put("checked",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "css":
					json.put("css",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "cssIndex":
					json.put("cssIndex",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "scrollx":
					json.put("scrollx",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "scrolly":
					json.put("scrolly",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				case "windowscroll":
					json.put("windowscroll",
							obj.getJSONObject("object").getJSONArray("attributes").getJSONObject(i).getString("value"));
					break;
				}

			}
			JSONObject playbackstep = new JSONObject();
			playbackstep.put("testdata", json);
			playbackstep.put("index", index);
			playbackstep.put("name", "Playback");
			playbackstep.put("id", obj.get("id"));
			if (obj.has("captureScreenshot")) {
				playbackstep.put("captureScreenshot", obj.getBoolean("captureScreenshot"));
			}
			web.getArraysteps().add(playbackstep);
			FOR("recived", 0);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("static-access")
	public void startExecution(JSONObject js, String auth) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", auth);
		HttpUtility htul = new HttpUtility();
		try {
			@SuppressWarnings("unused")
			JSONObject response = new JSONObject(
					htul.SendPost(GlobalDetail.serverIP + "/v1/executionTestcase", js.toString(), hash));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void steps_pass_fail(JSONObject js) {
		if (GlobalDetail.userDetail.getAuthkey() != null) {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("Content-type", "application/json");
			hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
			try {

				@SuppressWarnings("unused")
				JSONObject response = new JSONObject(
						HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/executionresult/step", js.toString(), hash));

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public void FOR(String str, int index) {
		try {
			if (str.equals("recived")) {
				JSONObject start = new JSONObject();
				JSONObject data1 = new JSONObject();
				JSONArray data = new JSONArray();
				start = GlobalDetail.web.getSteps().get(index);
				if (GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").length() != 0) {
					data1.put(
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("name"),
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("defaultValue"));
				}
				data.put(data1);
				start.put("data", data);
				start.put("testcaseId", GlobalDetail.web.getTestcaseId());
				start.put("tcSeq", 1);
				start.put("version", 1);
				start.put("testcaseCode", GlobalDetail.testcaseCode);
				start.put("resultTc", "Passed");
				start.put("customerId", GlobalDetail.userDetail.getCustomerId());
				start.put("projectId", GlobalDetail.userDetail.getProjectId());
				start.put("executionId", GlobalDetail.web.getExecutionId());
				start.put("itr", 1);
				testcase_pass_fail(start);
				savetestcase_result(start);
				JSONObject finish = new JSONObject();
				finish.put("executionId", GlobalDetail.web.getExecutionId());
				finish.put("projectId", GlobalDetail.userDetail.getProjectId());
				finish.put("customerId", GlobalDetail.userDetail.getCustomerId());
				finish.put("testcaseId", GlobalDetail.web.getTestcaseId());
				finishExecution(finish);

			} else if (str.equals("wait")) {
				JSONObject step = new JSONObject();
				JSONArray data = new JSONArray();
				JSONObject data1 = new JSONObject();
				step = GlobalDetail.web.getArraysteps().get(index);
				if (GlobalDetail.web.getArraysteps().get(index).has("parameters")) {
					if (GlobalDetail.web.getArraysteps().get(index).getJSONArray("parameters").length() != 0) {
						data1.put(
								GlobalDetail.web.getArraysteps().get(index).getJSONArray("parameters").getJSONObject(0)
										.getString("name"),
								GlobalDetail.web.getArraysteps().get(index).getJSONArray("parameters").getJSONObject(0)
										.getString("defaultValue"));
					}
				}

				data.put(data1);
				step.put("data", data);
				step.put("result", 0);
				step.put("tcSeq", 1);
				step.put("itr", 1);
				step.put("stepId", (GlobalDetail.web.getArraysteps().get(index)).getString("id"));
				step.put("testcaseId", GlobalDetail.web.getTestcaseId());
				step.put("executionId", GlobalDetail.web.getExecutionId());
				steps_pass_fail(step);

				JSONObject start = new JSONObject();
				start.put("testcaseId", GlobalDetail.web.getTestcaseId());
				start.put("tcSeq", 1);
				start.put("version", 1);
				start.put("testcaseCode", GlobalDetail.testcaseCode);
				start.put("resultTc", "Failed");
				start.put("customerId", GlobalDetail.userDetail.getCustomerId());
				start.put("projectId", GlobalDetail.userDetail.getProjectId());
				start.put("executionId", GlobalDetail.web.getExecutionId());
				start.put("itr", 1);
				testcase_pass_fail(start);
				savetestcase_result(start);
				JSONObject finish = new JSONObject();
				finish.put("executionId", GlobalDetail.web.getExecutionId());
				finish.put("projectId", GlobalDetail.userDetail.getProjectId());
				finish.put("customerId", GlobalDetail.userDetail.getCustomerId());
				finish.put("testcaseId", GlobalDetail.web.getTestcaseId());
				finishExecution(finish);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("static-access")
	public void testcase_pass_fail(JSONObject js) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
		HttpUtility htul = new HttpUtility();
		try {
			JSONObject response = new JSONObject(
					htul.SendPost(GlobalDetail.serverIP + "/v1/executionTestcase", js.toString(), hash));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("static-access")
	public void savetestcase_result(JSONObject js) {

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
		try {
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/saveresult/testcase", js.toString(), hash));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("static-access")
	public void finishExecution(JSONObject js) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", GlobalDetail.userDetail.getAuthkey());
		HttpUtility htul = new HttpUtility();
		try {
			JSONObject response = new JSONObject(
					htul.SendPost(GlobalDetail.serverIP + "/v1/finishExecution", js.toString(), hash));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void pass_Fail(JSONObject json, int index) {
		try {
			if ((json.getString("play_back")).equals("recived")) {

				JSONObject step = new JSONObject();
				JSONArray data = new JSONArray();
				JSONObject data1 = new JSONObject();
				step = GlobalDetail.web.getSteps().get(index);

				if (GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").length() != 0) {
					data1.put(
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("name"),
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("defaultValue"));
				}
				data.put(data1);
				step.put("data", data);
				if (json.has("captureScreenshot")) {
					if (GlobalDetail.web.getArraysteps().get(index).getBoolean("captureScreenshot") == true) {
						step.put("screenshot", json.getJSONObject("step").getString("screenshot"));
					}
				}
				step.put("result", 1);
				step.put("tcSeq", 1);
				step.put("itr", 1);
				step.put("stepId", (GlobalDetail.web.getArraysteps().get(index)).getString("id"));
				step.put("testcaseId", GlobalDetail.web.getTestcaseId());
				step.put("executionId", GlobalDetail.web.getExecutionId());
				steps_pass_fail(step);

			} else if ((json.getString("play_back")).equals("wait")) {

				JSONObject step = new JSONObject();
				JSONArray data = new JSONArray();
				JSONObject data1 = new JSONObject();
				step = GlobalDetail.web.getSteps().get(index);

				if (GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").length() != 0) {
					data1.put(
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("name"),
							GlobalDetail.web.getSteps().get(index).getJSONArray("parameters").getJSONObject(0)
									.getString("defaultValue"));
				}

				data.put(data1);
				step.put("data", data);
				if (json.has("captureScreenshot")) {
					if (GlobalDetail.web.getArraysteps().get(index).getBoolean("captureScreenshot") == true) {
						step.put("screenshot", json.getJSONObject("step").getString("screenshot"));
					}
				}
				step.put("result", 0);
				step.put("tcSeq", 1);
				step.put("itr", 1);
				step.put("stepId", (GlobalDetail.web.getArraysteps().get(json.getInt("index"))).getString("id"));
				step.put("testcaseId", GlobalDetail.web.getTestcaseId());
				step.put("executionId", GlobalDetail.web.getExecutionId());
				steps_pass_fail(step);

			}

			if (json.has("count")) {
				if (json.getInt("count") == 15) {
					FOR("wait", index);
				}
			} else if (index == GlobalDetail.web.getArraysteps().size()) {
				FOR("recived", index);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
