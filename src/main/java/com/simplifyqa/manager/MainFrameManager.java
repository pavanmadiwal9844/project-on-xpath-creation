package com.simplifyqa.manager;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.DTO.SaveObject;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.UpdateRp;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.desktoprecorder.DTO.OriginalData;
import com.simplifyqa.desktoprecorder.DTO.Parameter;
import com.simplifyqa.desktoprecorder.DTO.Step;
import com.simplifyqa.desktoprecorder.DTO.Testcase;

public class MainFrameManager {

	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFrameManager.class);

	public DumpOBject DumpObject(DumpOBject dmpobj, JSONObject object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			dmpobj = mapper.readValue(object.toString(), DumpOBject.class);
			logger.info(new ObjectMapper().writeValueAsString(dmpobj));
			System.out.println(new ObjectMapper().writeValueAsString(dmpobj));
			return dmpobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject SearchObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
			hash.put(Constants.AUTHORIZATION, GlobalDetail.user.getAuthkey());

			SearchColumns searchs = new SearchColumns();
			searchs.getSearchColumns()
					.add(new searchColumnDTO<String>(Constants.CUSTOMERID, GlobalDetail.user.getCustomerId().toString(),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
			searchs.getSearchColumns()
					.add(new searchColumnDTO<String>(Constants.PROJECTID, GlobalDetail.user.getProjectId().toString(),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
			searchs.getSearchColumns().add(new searchColumnDTO<String>(Constants.DISNAME, dmp.getText(),
					Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.STRING));
			searchs.setCollection(Constants.OBJECT);
			searchs.setLimit(100000);
			searchs.setStartIndex(0);
			JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
			JSONObject deleted = new JSONObject();
			deleted.put("column", Constants.DELETED);
			deleted.put(Constants.VALUE, false);
			deleted.put("regEx", false);
			deleted.put("type", Constants.BOOLEAN);
			System.out.println(object.getJSONArray("searchColumns").put(deleted));
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/search", object.toString(), hash));
			logger.info(response.toString());
			System.out.println(response);

			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject SavaObject(DumpOBject dmp) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
			hash.put(Constants.AUTHORIZATION, GlobalDetail.user.getAuthkey());
			SaveObject sv = new SaveObject(GlobalDetail.user.getCustomerId(), GlobalDetail.user.getProjectId(),
					GlobalDetail.user.getCreatedBy(), false, dmp.getText(), dmp.getText(), GlobalDetail.PageName,
					GlobalDetail.user.getCreatedBy(), GlobalDetail.user.getObjtemplateId());
			sv.setAttributes(dmp.getAttributes());
			System.out.println(new ObjectMapper().writeValueAsString(sv));
			logger.info(new ObjectMapper().writeValueAsString(sv));
			JSONObject response = new JSONObject((HttpUtility.SendPost(GlobalDetail.serverIP + "/saveObject/object",
					new ObjectMapper().writeValueAsString(sv), hash)));
			System.out.println(response);
			logger.info(response.toString());
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	public JSONObject checkDupobject(DumpOBject dBject) {
		try {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
			hash.put(Constants.AUTHORIZATION, GlobalDetail.user.getAuthkey());
			String str = dBject.getAttributes().toString();
			JSONArray js = new JSONArray(str);
			JSONObject js1 = new JSONObject();
			JSONObject js2 = new JSONObject();
			for (int i = 0; i < js.length(); i++) {
				if ((js.getJSONObject(i).getString("name")).equalsIgnoreCase("Position")) {
					js1 = js.getJSONObject(i);
				}
				if ((js.getJSONObject(i).getString("name")).equalsIgnoreCase("Label")) {
					js2 = js.getJSONObject(i);
				}
			}
			JSONArray dupattributes = new JSONArray();
			dupattributes.put(js1);
			dupattributes.put(js2);
			JSONObject ss = new JSONObject();
			ss.put(Constants.CUSTOMERID, GlobalDetail.user.getCustomerId());
			ss.put(Constants.PROJECTID, GlobalDetail.user.getProjectId());
			ss.put(Constants.ATTRIBUTES, dupattributes);
			JSONObject response = new JSONObject(
					(HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/objectdup", ss.toString(), hash)));

			if (response.getJSONArray(Constants.DATA).length() == 0) {
				System.out.println("save object and send to angular");
				logger.info("save object and send to angular");
			} else {
				System.out.println("Send object to angular");
				logger.info("Send object to angular");
			}
			return response;
		} catch (Exception e) {
			logger.error("error while object duplicate check ", e);
			return null;
		}
	}

	public void formatData(JSONObject s, Testcase tc, JSONObject parameters)
			throws JsonProcessingException, IOException {
		Step st = new Step();
		JSONObject objm = new JSONObject();
		if (s.getJSONArray(Constants.PARAMETERS).length() != 0) {
			for (int i = 0; i < s.getJSONArray(Constants.PARAMETERS).length(); i++) {
				JSONObject param = s.getJSONArray(Constants.PARAMETERS).getJSONObject(i);
				if (parameters.has(param.getString(Constants.NAME))) {
					String value = parameters.getString(param.getString(Constants.NAME));
					if (null != value && value.trim().length() > 0) {
						Parameter para1 = new Parameter();
						para1.setName(param.getString(Constants.NAME));
						para1.setDefaultValue(value);
						st.getParameter().add(para1);
						tc.getTestdata().add(i, (new ObjectMapper()
								.readTree(new JSONObject().put(para1.getName(), para1.getDefaultValue()).toString())));
					} else if (param.has(Constants.DEFAULTVALUE)) {
						Parameter para1 = new Parameter();
						para1.setName(param.getString(Constants.NAME));
						para1.setDefaultValue(param.getString(Constants.DEFAULTVALUE));
						st.getParameter().add(para1);
						tc.getTestdata().add(i, (new ObjectMapper()
								.readTree(new JSONObject().put(para1.getName(), para1.getDefaultValue()).toString())));
					}
				}

			}
		}
		objm.put(Constants.DISNAME, s.getJSONArray(Constants.SELECTEDOBJECT).getJSONObject(0).get(Constants.DISNAME));
		objm.put(Constants.ATTRIBUTES,
				s.getJSONArray(Constants.SELECTEDOBJECT).getJSONObject(0).get(Constants.ATTRIBUTES));
		st.setObject(new ObjectMapper().readTree(objm.toString()));
		st.setAction(s.getString(Constants.ACTION));
		tc.getSteps().add(st);
		OriginalData or = new OriginalData();
		or.setData(tc);
		JSONObject js = new JSONObject(new ObjectMapper().writeValueAsString(or));
		System.out.println(js.toString());
		logger.info(js.toString());
		GlobalDetail.mainframeSteps = js;
	}

	public Testcase formatData1(JSONObject s, Testcase tc, JSONObject parameters) throws IOException {
		if (s.has("isConditional") && s.getBoolean("isConditional") == true) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			com.simplifyqa.DTO.Step step = mapper.readValue(s.toString(), com.simplifyqa.DTO.Step.class);
			System.out.println(new ObjectMapper().writeValueAsString(step));
			tc.getSteps().add(step);
		} else {
			Step st = new Step();
			JSONObject objm = new JSONObject();
			if (s.getJSONArray(Constants.PARAMETERS).length() != 0) {
				for (int i = 0; i < s.getJSONArray(Constants.PARAMETERS).length(); i++) {
					JSONObject param = s.getJSONArray(Constants.PARAMETERS).getJSONObject(i);
					if (parameters.has(param.getString(Constants.NAME))) {
						String value = parameters.getString(param.getString(Constants.NAME));
						if (null != value && value.trim().length() > 0) {
							Parameter para1 = new Parameter();
							para1.setName(param.getString(Constants.NAME));
							para1.setDefaultValue(value);
							st.getParameter().add(para1);
							tc.getTestdata().add(i, (new ObjectMapper().readTree(
									new JSONObject().put(para1.getName(), para1.getDefaultValue()).toString())));
						} else if (param.has(Constants.DEFAULTVALUE)) {
							Parameter para1 = new Parameter();
							para1.setName(param.getString(Constants.NAME));
							para1.setDefaultValue(param.getString(Constants.DEFAULTVALUE));
							st.getParameter().add(para1);
							tc.getTestdata().add(i, (new ObjectMapper().readTree(
									new JSONObject().put(para1.getName(), para1.getDefaultValue()).toString())));
						}

					}

				}
			}
			objm.put(Constants.CAPTURESCREENSHOT, Boolean.FALSE);
			if (s.has(Constants.CAPTURESCREENSHOT)) {
				objm.put(Constants.CAPTURESCREENSHOT, s.getBoolean(Constants.CAPTURESCREENSHOT));
			}
			objm.put(Constants.SKIP, Boolean.FALSE);
			if (s.has(Constants.SKIP) && s.getBoolean(Constants.SKIP)) {
				objm.put(Constants.SKIP, Boolean.TRUE);
			}
			objm.put(Constants.DISNAME, s.getJSONObject(Constants.OBJECT).get(Constants.DISNAME));
			objm.put(Constants.ATTRIBUTES, s.getJSONObject(Constants.OBJECT).get(Constants.ATTRIBUTES));
			st.setObject(new ObjectMapper().readTree(objm.toString()));
			st.setAction(s.getJSONObject(Constants.ACTION).getString(Constants.NAME));
			System.out.println(new ObjectMapper().writeValueAsString(st));
			tc.getSteps().add(st);
		}
		return tc;
	}

	public void steps_pass_fail(JSONObject js) {
		if (GlobalDetail.userDetail.getAuthkey() != null) {
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
			hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());
			System.out.println(js.toString());
			try {
				String response = HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/executionresult/step",
						js.toString(), hash);
				logger.info(response.toString());
				System.out.println(response + "frome the steppassfail");
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public void FOR(JSONObject json, int index) {
		try {
			index--;
			if (json.getString(Constants.PLAY_BACK).equalsIgnoreCase(Constants.PASSED)
					|| json.getString(Constants.PLAY_BACK).equalsIgnoreCase("skipped")) {
				JSONObject start = new JSONObject();
				JSONObject data1 = new JSONObject();
				JSONArray data = new JSONArray();
				start = GlobalDetail.mainframeref.getJSONObject(index);
				if (!json.isNull(Constants.TESTDATA)) {
					for (int i = 0; i < GlobalDetail.mainframeref.getJSONObject(index)
							.getJSONArray(Constants.PARAMETERS).length(); i++) {
						if (GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
								.getJSONObject(i).has(Constants.PROTECTED)
								&& GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
										.getJSONObject(i).getBoolean(Constants.PROTECTED)
								&& GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
										.getJSONObject(i).getBoolean(Constants.PROJECTID)) {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, "******");
						} else {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, json.getString(Constants.TESTDATA));

						}
					}

				}
				data.put(data1);
				start.put(Constants.DATA, data);
				start.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				start.put(Constants.TCSEQ, 1);
				start.put(Constants.VERSION, 1);
				start.put(Constants.TESTCASECODE, GlobalDetail.testcaseCode);
				start.put(Constants.RESULTTC, Constants.PASSED);
				start.put(Constants.CUSTOMERID, GlobalDetail.userDetail.getCustomerId());
				start.put(Constants.PROJECTID, GlobalDetail.userDetail.getProjectId());
				start.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				start.put(Constants.ITR, 1);
				testcase_pass_fail(start);
				savetestcase_result(start);
				JSONObject finish = new JSONObject();
				finish.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				finish.put(Constants.PROJECTID, GlobalDetail.userDetail.getProjectId());
				finish.put(Constants.CUSTOMERID, GlobalDetail.userDetail.getCustomerId());
				finish.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				finishExecution(finish);

			} else if (json.getString(Constants.PLAY_BACK).equalsIgnoreCase(Constants.FAILED)) {
				JSONObject start = new JSONObject();
				start.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				start.put(Constants.TCSEQ, 1);
				start.put(Constants.VERSION, 1);
				start.put(Constants.TESTCASECODE, GlobalDetail.testcaseCode);
				start.put(Constants.RESULTTC, Constants.FAILED);
				start.put(Constants.CUSTOMERID, GlobalDetail.userDetail.getCustomerId());
				start.put(Constants.PROJECTID, GlobalDetail.userDetail.getProjectId());
				start.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				start.put(Constants.ITR, 1);
				testcase_pass_fail(start);
				savetestcase_result(start);
				JSONObject finish = new JSONObject();
				finish.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				finish.put(Constants.PROJECTID, GlobalDetail.userDetail.getProjectId());
				finish.put(Constants.CUSTOMERID, GlobalDetail.userDetail.getCustomerId());
				finish.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				finishExecution(finish);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void finishExecution(JSONObject js) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
		hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());

		try {
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/finishExecution", js.toString(), hash));
			logger.info(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void pass_Fail(JSONObject json, int index) {
		index--;
		try {
			if ((json.getString(Constants.PLAY_BACK)).equalsIgnoreCase(Constants.PASSED)) {
				JSONObject step = new JSONObject();
				JSONArray data = new JSONArray();
				JSONObject data1 = new JSONObject();
				step = GlobalDetail.mainframeref.getJSONObject(index);
				if (!json.isNull(Constants.TESTDATA)) {
					for (int i = 0; i < GlobalDetail.mainframeref.getJSONObject(index)
							.getJSONArray(Constants.PARAMETERS).length(); i++) {
						if (GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
								.getJSONObject(i).has(Constants.PROTECTED)
								&& GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
										.getJSONObject(i).getBoolean(Constants.PROTECTED)) {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, "******");
						} else {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, json.getString("testdata"));

						}
					}
				}
				data.put(data1);
				step.put(Constants.DATA, data);
				if (!json.isNull(Constants.CAPTURESCREENSHOT)) {
					if (GlobalDetail.mainframeref.getJSONObject(index).has(Constants.CAPTURESCREENSHOT)) {
						if (GlobalDetail.mainframeref.getJSONObject(index)
								.getBoolean(Constants.CAPTURESCREENSHOT) == true) {
							step.put(Constants.SCREENSHOT, json.getString(Constants.CAPTURESCREENSHOT));
						}
					}
				}
				step.put(Constants.RESULT, 1);
				step.put(Constants.TCSEQ, 1);
				step.put(Constants.ITR, 1);
				step.put(Constants.STEPID, (GlobalDetail.mainframeref.getJSONObject(index)).getString(Constants.ID));
				step.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				step.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				System.out.println("the step execution is completed:" + json.getInt("index"));
				steps_pass_fail(step);

			} else if ((json.getString(Constants.PLAY_BACK)).equalsIgnoreCase(Constants.FAILED)) {
				JSONObject step = new JSONObject();
				JSONArray data = new JSONArray();
				JSONObject data1 = new JSONObject();
				step = GlobalDetail.mainframeref.getJSONObject(index);
				if (!json.isNull(Constants.TESTDATA)) {
					for (int i = 0; i < GlobalDetail.mainframeref.getJSONObject(index)
							.getJSONArray(Constants.PARAMETERS).length(); i++) {
						if (GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
								.getJSONObject(i).has(Constants.PROTECTED)
								&& GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
										.getJSONObject(i).getBoolean(Constants.PROTECTED)) {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, "******");
						} else {
							String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
									.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
							if (removedotparam.contains(".")) {
								removedotparam = removedotparam.replace('.', '_');
							}
							data1.put(removedotparam, json.getString("testdata"));
						}
					}
				}
				data.put(data1);
				step.put(Constants.DATA, data);
				step.put(Constants.RESULT, 0);
				step.put(Constants.TCSEQ, 1);
				step.put(Constants.ITR, 1);
				step.put(Constants.STEPID, (GlobalDetail.mainframeref.getJSONObject(index)).getString(Constants.ID));
				step.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
				step.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
				steps_pass_fail(step);
			} else if ((json.getString(Constants.PLAY_BACK)).equalsIgnoreCase("skipped")) {
				if ((json.getString(Constants.PLAY_BACK)).equalsIgnoreCase("skipped")) {
					JSONObject step = new JSONObject();
					JSONArray data = new JSONArray();
					JSONObject data1 = new JSONObject();
					step = GlobalDetail.mainframeref.getJSONObject(index);
					if (!json.isNull(Constants.TESTDATA)) {
						for (int i = 0; i < GlobalDetail.mainframeref.getJSONObject(index)
								.getJSONArray(Constants.PARAMETERS).length(); i++) {
							if (GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
									.getJSONObject(i).has(Constants.PROTECTED)
									&& GlobalDetail.mainframeref.getJSONObject(index).getJSONArray(Constants.PARAMETERS)
											.getJSONObject(i).getBoolean(Constants.PROTECTED)) {
								String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
										.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
								if (removedotparam.contains(".")) {
									removedotparam = removedotparam.replace('.', '_');
								}
								data1.put(removedotparam, "******");
							} else {
								String removedotparam = GlobalDetail.mainframeref.getJSONObject(index)
										.getJSONArray(Constants.PARAMETERS).getJSONObject(i).getString(Constants.NAME);
								if (removedotparam.contains(".")) {
									removedotparam = removedotparam.replace('.', '_');
								}
								data1.put(removedotparam, json.getString("testdata"));

							}
						}
					}
					data.put(data1);
					step.put(Constants.DATA, data);
					if (!json.isNull(Constants.CAPTURESCREENSHOT)) {
						if (GlobalDetail.mainframeref.getJSONObject(index).has(Constants.CAPTURESCREENSHOT)) {
							if (GlobalDetail.mainframeref.getJSONObject(index)
									.getBoolean(Constants.CAPTURESCREENSHOT) == true) {
								step.put(Constants.SCREENSHOT, json.getString(Constants.CAPTURESCREENSHOT));
							}
						}
					}
					step.put(Constants.RESULT, 2);
					step.put(Constants.TCSEQ, 1);
					step.put(Constants.ITR, 1);
					step.put(Constants.STEPID,
							(GlobalDetail.mainframeref.getJSONObject(index)).getString(Constants.ID));
					step.put(Constants.TESTCASEID, GlobalDetail.web.getTestcaseId());
					step.put(Constants.EXECUTIONID, GlobalDetail.web.getExecutionId());
					steps_pass_fail(step);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void testcase_pass_fail(JSONObject js) {
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
		hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());
		try {
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/executionTestcase", js.toString(), hash));
			logger.info(response.toString());
			System.out.println(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void savetestcase_result(JSONObject js) {

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
		hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());
		try {
			JSONObject response = new JSONObject(
					HttpUtility.SendPost(GlobalDetail.serverIP + "/saveresult/testcase", js.toString(), hash));
			System.out.println(response.toString());
			logger.info(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public JSONObject getRuntimeParameter(JSONObject runtimeparam)
			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException {

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
		hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());

		SearchColumns searchs = new SearchColumns();
		searchs.getSearchColumns()
				.add(new searchColumnDTO<Integer>(Constants.CUSTOMERID, GlobalDetail.userDetail.getCustomerId(),
						Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
		searchs.getSearchColumns()
				.add(new searchColumnDTO<Integer>(Constants.PROJECTID, GlobalDetail.userDetail.getProjectId(),
						Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
		searchs.getSearchColumns().add(new searchColumnDTO<String>("key", runtimeparam.getString("param").trim(),
				Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
		searchs.getSearchColumns().add(new searchColumnDTO<Boolean>(Constants.DELETED, Boolean.FALSE,
				Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.BOOLEAN));
		searchs.setCollection("runtime_parameters");
		searchs.setLimit(1);
		searchs.setStartIndex(0);
		JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
		JSONObject response = new JSONObject(
				HttpUtility.SendPost(GlobalDetail.serverIP + "/search", object.toString(), hash));
		logger.info(response.toString());
		return response;

	}

	public JSONObject saveRuntimeParameter(JSONObject param) throws KeyManagementException, JSONException,
			NoSuchAlgorithmException, KeyStoreException, JsonProcessingException, IOException {

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put(Constants.CONTENTTYPE, Constants.APPLICATIONJSON);
		hash.put(Constants.AUTHORIZATION, GlobalDetail.userDetail.getAuthkey());
		UpdateRp update = new UpdateRp();
		update.setCustomerId(GlobalDetail.userDetail.getCustomerId());
		update.setProjectId(GlobalDetail.userDetail.getProjectId());
		update.setTestcaseId(GlobalDetail.userDetail.getTestcaseId());
		update.setKey(param.getString(Constants.PARAMNAME).trim());
		update.setValue(param.getString(Constants.PARAMVALUE).trim());

		JSONObject response = new JSONObject(HttpUtility.SendPost(GlobalDetail.serverIP + "/v1/updaterp",
				new ObjectMapper().writeValueAsString(update), hash));
		logger.info(response.toString());
		return response;

	}
}
