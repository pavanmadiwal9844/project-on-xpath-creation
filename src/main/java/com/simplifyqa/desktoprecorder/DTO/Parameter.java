package com.simplifyqa.desktoprecorder.DTO;

public class Parameter {

	String name=null;
	String defaultValue =null;

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
