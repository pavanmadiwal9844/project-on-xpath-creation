package com.simplifyqa.desktoprecorder.DTO;

import java.util.ArrayList;
import java.util.List;

import com.simplifyqa.DTO.searchColumnDTO;

public class RuntimeParameters {
	private List<searchColumnDTO> searchColumns=new ArrayList<searchColumnDTO>();
	private int startIndex;
	private int limit;	
	private String collection;

}
