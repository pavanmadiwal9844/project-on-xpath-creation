package com.simplifyqa.desktoprecorder.DTO;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public class Testcase {
	
	private List<JsonNode> Testdata = new ArrayList<JsonNode>();
	private List<Object> steps = new ArrayList<Object>();

	public List<JsonNode> getTestdata() {
		return Testdata;
	}

	public void setTestdata(List<JsonNode> testdata) {
		Testdata = testdata;
	}

	public List<Object> getSteps() {
		return steps;
	}

	public void setSteps(List<Object> steps) {
		this.steps = steps;
	}


}
