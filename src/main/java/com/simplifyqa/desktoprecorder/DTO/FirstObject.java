package com.simplifyqa.desktoprecorder.DTO;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class FirstObject {

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<JSONObject> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JSONObject> attributes) {
		this.attributes = attributes;
	}

	public String action;
	public String Text;
	public String value;
	public List<JSONObject> attributes = new ArrayList<JSONObject>();


}
