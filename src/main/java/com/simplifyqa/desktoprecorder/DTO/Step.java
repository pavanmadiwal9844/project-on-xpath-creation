package com.simplifyqa.desktoprecorder.DTO;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;

public class Step {

	private JsonNode object ;
	private String action;
	private  java.util.List<Parameter>  parameter=new ArrayList<Parameter>();

	public JsonNode getObject() {
		return object;
	}

	public void setObject(JsonNode object) {
		this.object = object;
	}


	public java.util.List<Parameter> getParameter() {
		return parameter;
	}

	public void setParameter(java.util.List<Parameter> parameter) {
		this.parameter = parameter;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
