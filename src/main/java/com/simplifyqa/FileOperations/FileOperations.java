package com.simplifyqa.FileOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileOperations {

	private static final Logger logger = LoggerFactory.getLogger(FileOperations.class);

	public static Workbook openexcel(String excelfilepath) {

		Workbook workbook = null;

		try {

			File file = new File(excelfilepath);

			if (file.isFile() && file.exists()) {

				FileInputStream fip = new FileInputStream(file);

				if (excelfilepath.contains(".xlsx")) {

					workbook = new XSSFWorkbook(fip);

				} else if (excelfilepath.contains(".xls")) {

					workbook = new HSSFWorkbook(fip);

				}

				logger.info(excelfilepath + " is open");

			} else {

				logger.error(excelfilepath + " either not exist" + " or can't open");

			}

		} catch (Exception e) {

			e.printStackTrace();

			return null;

		}

		return workbook;

	}

	public static boolean closeexcel(String excelfilepath) {
		try {
			FileInputStream fip = new FileInputStream(excelfilepath);
			fip.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean writetoexcel(String excelfilepath, Workbook workbook) {
		try {
			FileOutputStream out = new FileOutputStream(new File(excelfilepath));
			workbook.write(out);
			// out.close();
			logger.info(excelfilepath + "written successfully");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
