package com.simplifyqa.sap.Interface;

public interface SapInterface {

	public Process startRecorderExe(String recorderExepath, String dirPath);

	public Process startPlaybackExe(String playbackExepath, String dirPath);

	public Boolean stopRecorderExe(Process recorderProcess);

	public Boolean stopPlaybackExe(Process playbackProcess);

}
