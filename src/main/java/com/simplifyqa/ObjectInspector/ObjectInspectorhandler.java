package com.simplifyqa.ObjectInspector;

import java.util.concurrent.ArrayBlockingQueue;

import org.json.JSONObject;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GlobalDetail;

public class ObjectInspectorhandler implements Runnable {

	public static String url;
	public static String browser;
	private ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(256);

	public final ArrayBlockingQueue<String> a() {
		return this.queue;
	}

	private void status(Boolean start) {
		while (start) {
			try {
				checkobject(this.queue.take());	
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	private void checkobject(String str) throws InterruptedException {
		Thread.sleep(2000);
		if (!url.equals(InitializeDependence.webDriver.get(browser).getUrl())
				&& !GlobalDetail.webInspectpr.isOpen()) {
			url = InitializeDependence.webDriver.get(browser).getUrl();
			Thread.sleep(1000);
			InitializeDependence.webDriver.get(browser).executeScript(
					"!function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){\"undefined\"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:\"Module\"}),Object.defineProperty(e,\"__esModule\",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&\"object\"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,\"default\",{enumerable:!0,value:e}),2&t&&\"string\"!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,\"a\",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p=\"\",n(n.s=0)}([function(e,t){!function(){console.log(\"added\");var e=document.createElement(\"script\");e.src=\"http://localhost:4012/v1/inspect\",e.type=\"text/javascript\";var t=document.createElement(\"link\");t.rel=\"stylesheet\",t.type=\"text/css\",t.href=\"http://localhost:4012/parse\",document.getElementsByTagName(\"head\")[0].appendChild(e),document.getElementsByTagName(\"head\")[0].appendChild(t)}()}]);");
			
		} else if (!url.equals(InitializeDependence.webDriver.get(browser).getUrl())
				&& GlobalDetail.webInspectpr.isOpen()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			GlobalDetail.webInspectpr.getAsyncRemote()
					.sendText(new JSONObject().put("inspect", "Attch").toString());
		} else if (url.equals(InitializeDependence.webDriver.get(browser).getUrl())
				&& !GlobalDetail.webInspectpr.isOpen()) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			url = InitializeDependence.webDriver.get(browser).getUrl();
			InitializeDependence.webDriver.get(browser).executeScript(
					"!function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){\"undefined\"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:\"Module\"}),Object.defineProperty(e,\"__esModule\",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&\"object\"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,\"default\",{enumerable:!0,value:e}),2&t&&\"string\"!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,\"a\",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p=\"\",n(n.s=0)}([function(e,t){!function(){console.log(\"added\");var e=document.createElement(\"script\");e.src=\"http://localhost:4012/v1/inspect\",e.type=\"text/javascript\";var t=document.createElement(\"link\");t.rel=\"stylesheet\",t.type=\"text/css\",t.href=\"http://localhost:4012/parse\",document.getElementsByTagName(\"head\")[0].appendChild(e),document.getElementsByTagName(\"head\")[0].appendChild(t)}()}]);");
		}
	}

	@Override
	public void run() {
		status(true);
		
	}
	
}
