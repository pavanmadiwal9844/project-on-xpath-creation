package com.simplifyqa.ObjectInspector;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GetMainFrameSteps;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.method.WebMethod;
import com.simplifyqa.mobile.android.service;

public class ObjectInspector {

	public static String url;
	private String browser;
	public static Boolean inspect;
	private int count = 0;
	private String platform = null;

	public Boolean startInpector(JSONObject req) {
		try {
			count = 0;
			if (req.has("type")) {
				if (req.getString("type").toLowerCase().equals("web")) {

					if (SystemUtils.IS_OS_WINDOWS)
						platform = "windows";
					else if (SystemUtils.IS_OS_MAC)
						platform = "mac";
					else
						platform = "linux";
					ObjectInspectorhandler.browser = browser = req.getString("browser");
					InitializeDependence.webDriver.put(browser, new WebMethod());
//					InitializeDependence.webDriver.get(browser).launchapplication(browser, platform,
//							req.getString("url"));
					inspect = true;
					Thread.sleep(3000);
					ObjectInspectorhandler.url = InitializeDependence.webDriver.get(browser).getUrl();
					service.g().e().submit(() -> {
						count = 1;
						try {
							startDeviceManager();
							checkoutUrl();

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return;
					});
					return InitializeDependence.webDriver.get(browser).executeScript(
							"!function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){\"undefined\"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:\"Module\"}),Object.defineProperty(e,\"__esModule\",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&\"object\"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,\"default\",{enumerable:!0,value:e}),2&t&&\"string\"!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,\"a\",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p=\"\",n(n.s=0)}([function(e,t){!function(){console.log(\"added\");var e=document.createElement(\"script\");e.src=\"http://localhost:4012/v1/inspect\",e.type=\"text/javascript\";var t=document.createElement(\"link\");t.rel=\"stylesheet\",t.type=\"text/css\",t.href=\"http://localhost:4012/parse\",document.getElementsByTagName(\"head\")[0].appendChild(e),document.getElementsByTagName(\"head\")[0].appendChild(t)}()}]);");
				} else if(req.getString("type").toLowerCase().equals("sap"))
				{
					return InitializeDependence.sapAction.startInspector();
				}
				else if(req.getString("type").toLowerCase().equals("desktop"))
				{
					return InitializeDependence.dtAction.startInspector();
				}
				else if(req.getString("type").toLowerCase().equals("mainframe"))
				{
				 GetMainFrameSteps.exename =null;
				 return	InitializeDependence.mfAction.startInspector();
				}
				else
					return false;
			} else
				return false;
		} catch (Exception e) {
			return false;
		}
	}

	public void checkoutUrl() throws InterruptedException {

		for (; inspect;) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			objectservice.a().a("get");
		}
	}

	private void startDeviceManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				objectservice.a().run();
			}
		}).start();

	}
}
