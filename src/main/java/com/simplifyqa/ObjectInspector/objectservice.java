package com.simplifyqa.ObjectInspector;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.mobile.android.Threadpool;

public class objectservice {
	private static final Logger a = LoggerFactory.getLogger(objectservice.class);
	private final ExecutorService b = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue());

	private ObjectInspectorhandler c = new ObjectInspectorhandler();

	private static objectservice d;

	public static objectservice a() {
		if (d == null) {
			d = new objectservice();
		}
		return d;
	}

	public void Reset() throws IOException {
		d = null;
	}

	public final Future<?> run() {
		return this.b.submit(this.c);
	}

	public void b(String a) {
		a(a);
	}

	public void a(String paramg) {

		this.c.a().remainingCapacity();

		boolean bool = false;
		try {
			this.c.a().add(paramg);
		} catch (IllegalStateException illegalStateException) {
			a.error("Failed to queue  remaining capacity: {}", Integer.valueOf(this.c.a().remainingCapacity()));
			bool = true;
		}

		if (bool) {
			try {
				this.c.a().clear();
				return;
			} catch (Throwable paramg1) {
				a.error("Failed to reset Agent queue, Agent will stop", paramg1);
				Runtime.getRuntime().halt(-1);
			}
		}
	}

	public final void destroy() {
		this.b.shutdown();
		a.info("Agent state machine stopped.");
	}

}
