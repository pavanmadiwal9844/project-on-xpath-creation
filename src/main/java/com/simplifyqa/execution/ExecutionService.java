package com.simplifyqa.execution;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutionService {

	private Logger logger = LoggerFactory.getLogger(ExecutionService.class);
	private ExecutorService Executor = new ThreadPoolExecutor(0, 2147483647, 60L, TimeUnit.SECONDS,
			new SynchronousQueue<>());

	public void set(String str) {
		this.executionhandler.brname=str;
	}
	private Executionhandler executionhandler = new Executionhandler();
	private static ExecutionService service;

	public static ExecutionService a() {
		if (service == null)
			service = new ExecutionService();
		return service;
	}

	public Future<?> run() {
		return this.Executor.submit(executionhandler);
	}

	public final void a(ExecutionTask task) {

		this.executionhandler.getQueue().remainingCapacity();

		boolean bool = false;
		try {
			this.executionhandler.getQueue().add(task);
		} catch (IllegalStateException illegalStateException) {
			logger.error("Failed to queue  remaining capacity: {}",
					Integer.valueOf(this.executionhandler.getQueue().remainingCapacity()));
			bool = true;
		}

		if (bool) {
			try {
				this.executionhandler.getQueue().clear();
				return;
			} catch (Throwable paramg1) {
				logger.error("Failed to reset Execution queue, Agent will stop", paramg1);
				Runtime.getRuntime().halt(-1);
			}
		}
	}

	public void Reset() {
		service = null;
	}

	public final void d() {
		this.Executor.shutdown();
		logger.info("Execution Service Stopped.");

	}
}
