package com.simplifyqa.execution;

import java.util.concurrent.ArrayBlockingQueue;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.execution.Impl.AutomationImpl;
import com.simplifyqa.execution.Interface.ExecutionUtil;
import com.simplifyqa.logger.LoggerClass;

public class Executionhandler implements Runnable {

	private Logger logger = LoggerFactory.getLogger(Executionhandler.class);
	public String brname;
	private ArrayBlockingQueue<ExecutionTask> queue = new ArrayBlockingQueue<ExecutionTask>(5000, true);
	private static final org.apache.log4j.Logger APIlogger = org.apache.log4j.LogManager
			.getLogger(HttpUtility.class);

	static LoggerClass APILogger = new LoggerClass();

	public ArrayBlockingQueue<ExecutionTask> getQueue() {
		return this.queue;
	}

	private void startExecution(ExecutionTask req) {
		try {
			InitializeDependence.serverCall.postsfITRexecution(req.getExeDetial(), req.getItr(), req.getStatus(),
					req.getHeader());

		} catch (Exception e) {
			logger.info("Unable to start Execution :{}", e);
		}
	}

	private void stepResult(ExecutionTask req) {
		try {
			Thread.currentThread().setName(brname);
			InitializeDependence.serverCall.postStepsRc(req.getExeDetial(), req.getHeader(), req.getItr(),
					req.getTcData(), req.getStatus(), req.getScreenshot(), req.getStep());
		} catch (Exception e) {
			logger.info("Unable to Pass step Result:{}", e);
		}
	}

	private void testcaseResult(ExecutionTask req) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "iNSIDE HANDLER Execution result - ", null, "---", "info");
			InitializeDependence.serverCall.postsfITRexecution(req.getExeDetial(), req.getItr(), req.getStatus(),
					req.getHeader());
			
			APILogger.customloggAPI(APIlogger, "Testcase Status Code", null, req.getStatus() + " " + req.getItr(), "info");
			if (!req.getStatus().equals(UtilEnum.INPROGRESS.value())) {
				APILogger.customloggAPI(APIlogger, "Going insde handler if case", null, req.getStatus() + " " + req.getItr(), "info");
				InitializeDependence.serverCall.savetestcase_result(req.getExeDetial(), req.getItr(), req.getStatus(),
						req.getHeader());
			}
		} catch (Exception e) {
			APILogger.customloggAPI(APIlogger, "Unable to pass testcase", null, req.getStatus() + " " + req.getItr(), "info");
			logger.info("Unable to Pass Testcase result:{}", e);
		}
	}

	private void finishExecution(ExecutionTask req) {
		try {
			InitializeDependence.serverCall.postFinishExecution(req.getExeDetial(), req.getHeader());
		} catch (Exception e) {
			logger.info("Unable to Pass Finish Eexecution:{}", e);
		}
	}

	private void status(Boolean flag) {
		try {
			ExecutionTask task;
			while (flag) {
				task = this.queue.take();
				if (task.getTask().equals(ExecutionOption.STARTEXECUTION)) {
					startExecution(task);
				} else if (task.getTask().equals(ExecutionOption.STEPRESULT)) {
					stepResult(task);
				} else if (task.getTask().equals(ExecutionOption.TESTCASERESULT)) {
					testcaseResult(task);
					Thread.sleep(100);
				} else if (task.getTask().equals(ExecutionOption.FINISHEXECUITON)) {
					finishExecution(task);
				}
			}

		} catch (Exception e) {
			logger.error("Error to status execution handler:{}", e);
		}
	}

	@Override
	public void run() {
		status(true);
	}
}
