package com.simplifyqa.execution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;
import org.python.antlr.PythonParser.attr_return;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rits.cloning.Cloner;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.SuitTc;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.DTO.suitCollection;
import com.simplifyqa.Utility.ActionClass;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.jenkinsocket;
import com.simplifyqa.execution.Impl.DesktopExecutionImpl;
import com.simplifyqa.execution.Impl.Executionimpl;
import com.simplifyqa.execution.Impl.MobileExecutorImpl;
import com.simplifyqa.execution.Impl.SapExecutionImpl;
import com.simplifyqa.execution.Impl.WebExecutorImpl;
import com.simplifyqa.execution.Interface.Executor;
import com.simplifyqa.handler.Service;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;

import io.appium.java_client.AppiumDriver;

public class AutomationExecuiton {

	final static Logger logger = LoggerFactory.getLogger(AutomationExecuiton.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	LoggerClass logerOBJ = new LoggerClass();

	private Setupexec executionDetail = null;
	private HashMap<String, String> header = null;
	private Executor executor;
	private SuitTc suit;
	private HashMap<String, String> driversession = new java.util.HashMap<String, String>();
	private String deviceId = null;
	boolean oncontinue_check;

	private static final org.apache.log4j.Logger APIlogger = org.apache.log4j.LogManager
			.getLogger(HttpUtility.class);

	static LoggerClass APILogger = new LoggerClass();

	public AutomationExecuiton(Setupexec executionDetail, HashMap<String, String> header, String deviceId) {
		this.executionDetail = executionDetail;
		this.header = header;
		this.deviceId = deviceId;
		init();
	}

	public AutomationExecuiton(Setupexec executionDetail, HashMap<String, String> header) {
		this.executionDetail = executionDetail;
		this.header = header;
//		this.executor = new Executionimpl();
		init();
	}

	public AutomationExecuiton(Testcase tcfnObject, Executor executor) {
		this.executor = executor;
		testcaseExecution(tcfnObject, null);
	}

	private void init() {
		try {
			InitializeDependence.suite = false;
			InitializeDependence.funcItr_suite = 0;
			System.out.println("You are executing version : " + InitializeDependence.jar_version);
			File file = new File("UserLogs.log");

			if (file.delete()) {
				System.out.println("File deleted successfully");
			} else {
				System.out.println("Failed to delete the file");
			}
			
			File APIfile = new File("APILogs.log");



			if (APIfile.delete()) {
			System.out.println("APILogs file deleted successfully");
			} else {
			System.out.println("APILogs failed to delete the file");
			}
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			JSONObject req = new JSONObject();
			req.put(UtilEnum.SERVERIP.value(), executionDetail.getServerIp());
			req.put(UtilEnum.CUSTOMERID.value(), executionDetail.getCustomerID());
			req.put(UtilEnum.PROJECTID.value(), executionDetail.getProjectID());
			req.put(UtilEnum.EXECUTIONID.value(), executionDetail.getExecutionID());
			JSONObject res = InitializeDependence.serverCall.getExecutionDetail(req, header);

			// added to get cuurent project name
			JSONObject response = InitializeDependence.serverCall.getCurrentProjectDetail(req, header);
			executionDetail.setCreatedBy(Integer.parseInt(res.getJSONObject(UtilEnum.DATA.value())
					.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).get("createdBy").toString()));
			executionDetail.setBrowserName(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
					.getJSONObject(0).has("browserName")
							? res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
									.getJSONObject(0).getString("browserName")
							: res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
									.getJSONObject(0).getJSONArray("devices").get(0).toString());
			if (!res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0)
					.has(UtilEnum.SUITEID.value())
					&& !res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0)
							.has(UtilEnum.SUITEIDS.value())) {
				executionDetail.setTestcaseID(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getInt(UtilEnum.TESTCASEID.value()));
				executionDetail.setTcSeq(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getInt(UtilEnum.TCSEQ.value()));
				executionDetail.setTestcaseCode(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString(UtilEnum.TESTCASECODE.value()));
				executionDetail.setVersion(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getInt(UtilEnum.VERSION.value()));
				executionDetail.setModuleID(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getInt(UtilEnum.MODULEID.value()));
				executionDetail.setEnvironmentType(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("environmentType"));
				executionDetail.setTcType(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).get("tcType").toString());
				logger.info("Testcase received:{}", executionDetail.getTestcaseID());
				if (executionDetail.getTcType().toLowerCase().equals("web"))
					executor = new WebExecutorImpl();
				else if (executionDetail.getTcType().toLowerCase().equals("mobile"))
					executor = new MobileExecutorImpl();
				else if (executionDetail.getTcType().toLowerCase().equals("mainframe"))
					executor = new Executionimpl();
				else if (executionDetail.getTcType().toLowerCase().equals("sap"))
					executor = new SapExecutionImpl();
				else if (executionDetail.getTcType().toLowerCase().equals("desktop"))
					executor = new DesktopExecutionImpl();
				else if (executionDetail.getTcType().toLowerCase().equals("hybrid"))
					executor = null;
			} else if (res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0)
					.has(UtilEnum.SUITEIDS.value())) {
				System.out.println(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getInt("execplanId"));
				executionDetail.setExecplanId(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getInt("execplanId"));
				executionDetail.setScenarioFolderId(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getInt("scenarioFolderId"));

				executionDetail.setExecutionPlanScenario(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getBoolean("executionPlanScenario"));

				executionDetail.setExpAssignedTo(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getInt("expAssignedTo"));

				executionDetail.setExpPlnnedDate(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("expPlnnedDate"));

				executionDetail.setExecplanexecId(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getInt("execplanexecId"));
				executionDetail.setEnvironmentType(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("environmentType"));

				for (int i = 0; i < res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getJSONArray(UtilEnum.SUITEIDS.value()).length(); i++) {
					mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
					mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
					suitCollection suc = mapper.readValue(
							res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
									.getJSONObject(0).getJSONArray(UtilEnum.SUITEIDS.value()).get(i).toString(),
							suitCollection.class);
					executionDetail.getSuiteIds().add(suc);
				}

			} else {
				executionDetail.setEnvironmentType(res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("environmentType"));
				executionDetail.setSuiteID(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getInt(UtilEnum.SUITEID.value()));
				suit = mapper.readValue(res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).toString(), SuitTc.class);
				logger.info("Suite received:{}", executionDetail.getSuiteID());
				logerOBJ.customlogg(userlogger,
						"=============================  <<<<<< SUITE STARTED>>>>>>  =============================",
						null, null, "info");

				// userlogs...
				String suitInfo = executionDetail.getSuiteID().toString();
				logerOBJ.customlogg(userlogger, "SUITE ID :", null, suitInfo, "info");

			}
			logger.info("execution happening with data:" + res.toString());

			if (!res.getJSONObject("data").getJSONArray("data").getJSONObject(0).has("suiteId")) {

				if (res.getJSONObject("data").getJSONArray("data").getJSONObject(0).has("name")) {
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");
					logerOBJ.customlogg(userlogger,
							"=============================  << TESTCASE STARTED >>  =============================",
							null, "", "info");

					logerOBJ.customlogg(userlogger, "TC Name -",
							res.getJSONObject("data").getJSONArray("data").getJSONObject(0).get("name"), null, "info");

					// TC Description..
				}
				if (res.getJSONObject("data").getJSONArray("data").getJSONObject(0).has("tcType")) {
					logerOBJ.customlogg(userlogger, "TC Type -",
							res.getJSONObject("data").getJSONArray("data").getJSONObject(0).get("tcType"), null,
							"info");
				}

				if (res.getJSONObject("data").getJSONArray("data").getJSONObject(0).has("description")) {
					logerOBJ.customlogg(userlogger, "TC Description -",
							res.getJSONObject("data").getJSONArray("data").getJSONObject(0).get("description"), null,
							"info");
				}
			}

			startExecution();
		} catch (Exception e) {

			e.printStackTrace();
			logger.error("Unabel to get testcase or suit {}", e);
		}
	}

	public static boolean medwait = false;
	public static boolean minwait = false;
	public static boolean maxwait = false;

	private void startExecution() {
		try {

			Setupexec exeSetupexec = null;
			hybridExecution = false;
			Testcase tcfnObject;
			System.out.println("You are execution jar version : " + InitializeDependence.jar_version);
			JSONObject reqUserdetail = new JSONObject();
			reqUserdetail.put(UtilEnum.CUSTOMERID.value(), executionDetail.getCustomerID());
			reqUserdetail.put(UtilEnum.SERVERIP.value(), executionDetail.getServerIp());
			reqUserdetail.put(UtilEnum.userId.value(), executionDetail.getCreatedBy());
			InitializeDependence.userdetails(reqUserdetail, header);
			for (int i = 0; i < InitializeDependence.userdetail.getJSONArray("preference").length(); i++) {
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.equals("timeout")) {
					InitializeDependence.timeout = Integer.parseInt(InitializeDependence.userdetail
							.getJSONArray("preference").getJSONObject(i).getString("value"));
				}
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name").trim()
						.equals("download_path")) {
					InitializeDependence.downloadpath = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value");
				}
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name").trim()
						.equals("minwait")) {
					InitializeDependence.minwait = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value");
					minwait = true;
				}
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name").trim()
						.equals("medwait")) {
					InitializeDependence.medwait = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value");
					medwait = true;
				}
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name").trim()
						.equals("maxwait")) {
					InitializeDependence.maxwait = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value");
					maxwait = true;
				}

				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.toLowerCase().equals("existtimeout")) {

					if ((InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("value"))
							.equals("")) {
						InitializeDependence.existTimeout = -1;

					}

					else {

						InitializeDependence.existTimeout = Integer.parseInt(InitializeDependence.userdetail
								.getJSONArray("preference").getJSONObject(i).getString("value"));
					}
				}

				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.equals("loader")) {
					InitializeDependence.loader_present = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value").toString();

				}
				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.equals("loader_xpath")) {
					InitializeDependence.loader_xpath = InitializeDependence.userdetail.getJSONArray("preference")
							.getJSONObject(i).getString("value").toString();
				}

				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.toLowerCase().equals("loader_waitime")) {

					if ((InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("value"))
							.equals("")) {
						InitializeDependence.loader_waitime = -1;

					}

					else {

						InitializeDependence.loader_waitime = Integer.parseInt(InitializeDependence.userdetail
								.getJSONArray("preference").getJSONObject(i).getString("value"));
					}
				}

				if (InitializeDependence.userdetail.getJSONArray("preference").getJSONObject(i).getString("name")
						.toLowerCase().equals("suite_new_session")) {

					InitializeDependence.suite_new_session = Boolean.parseBoolean(InitializeDependence.userdetail
							.getJSONArray("preference").getJSONObject(i).getString("value"));
				}
			}
			if (executionDetail.getSuiteID() == null && executionDetail.getSuiteIds().isEmpty()) {
				tcfnObject = InitializeDependence.serverCall.getTCstep(executionDetail, header);
				exeSetupexec = null;
				Cloner cloner = new Cloner();
				exeSetupexec = cloner.deepClone(executionDetail);
				testcaseExecution(tcfnObject, exeSetupexec);
				logger.info("Execution  started with testcaseID:{}", executionDetail.getTestcaseID());
				// logerOBJ.customlogg(userlogger,executionDetail.getTestcaseID().toString(),null,null,"info");

			} else if (!executionDetail.getSuiteIds().isEmpty()) {
				hybridExecution = true;
				JSONArray re = InitializeDependence.serverCall.getScenarioExecTcs(executionDetail, header)
						.getJSONArray(UtilEnum.DATA.value());
				for (int i = 0; i < re.length(); i++) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
					mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
					executionDetail.setSuiteID(re.getJSONObject(i).getInt(UtilEnum.SUITEID.value()));
					suit = mapper.readValue(re.getJSONObject(i).toString(), SuitTc.class);

					for (int j = 0; j < suit.getTestcasesuites().size(); j++) {
						executionDetail.setTestcaseCode(suit.getTestcasesuites().get(j).get("code").asText());
						executionDetail.setTestcaseID(
								suit.getTestcasesuites().get(j).get(UtilEnum.TESTCASEID.value()).asInt());
						executionDetail.setTcSeq(suit.getTestcasesuites().get(j).get("seq").asInt());
						executionDetail
								.setModuleID(suit.getTestcasesuites().get(j).get(UtilEnum.MODULEID.value()).asInt());
						executionDetail
								.setVersion(suit.getTestcasesuites().get(j).get(UtilEnum.VERSION.value()).asInt());
						executionDetail.setTcType(suit.getTestcasesuites().get(j).has("tcType")
								? suit.getTestcasesuites().get(j).get("tcType").asText()
								: "web");
//						if (executionDetail.getTcType().equals("mobile"))
//							executionDetail.setMbType(suit.getTestcasesuites().get(j).get("mbType").asText());
						tcfnObject = InitializeDependence.serverCall.getTCstep(executionDetail, header);
						if (executionDetail.getTcType().toLowerCase().equals("web"))
							executor = new WebExecutorImpl();

						else if (executionDetail.getTcType().toLowerCase().equals("mobile")) {
							if (DeviceManager.devices.get(executionDetail.getDevicesID()) != null) {
								executionDetail.setMbType("android");
							} else {
								for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
									if (map.getValue().getString("id").equals(executionDetail.getDevicesID())) {
										executionDetail.setMbType("ios");
										break;
									}
							}
							executor = new MobileExecutorImpl();
						} else if (executionDetail.getTcType().toLowerCase().equals("mainframe"))
							executor = new Executionimpl();
						else if (executionDetail.getTcType().toLowerCase().equals("desktop"))
							executor = new DesktopExecutionImpl();
						else if (executionDetail.getTcType().toLowerCase().equals("sap"))
							executor = new SapExecutionImpl();
						else if (executionDetail.getTcType().toLowerCase().equals("hybrid"))
							executor = null;
						exeSetupexec = null;
						Cloner cloner = new Cloner();
						exeSetupexec = cloner.deepClone(executionDetail);
						testcaseExecution(tcfnObject, exeSetupexec);
						logger.info("Execution  started with testcaseID:{}", executionDetail.getTestcaseID());
					}
				}

			} else {
				InitializeDependence.suite = true;
				for (int i = 0; i < suit.getTestcasesuites().size(); i++) {
					logerOBJ.customlogg(userlogger,
							"readched....",
							null, "tc done", "info");
					
					InitializeDependence.suite_tescase_count = suit.getTestcasesuites().size();
					InitializeDependence.suite_testcase_sequence = i;
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");
					logerOBJ.customlogg(userlogger,
							"=============================  << TESTCASE STARTED >>  =============================",
							null, "", "info");
					logerOBJ.customlogg(userlogger, "TC Name -", null,
							suit.getTestcasesuites().get(i).get("testcaseName").toString(), "info");
					logerOBJ.customlogg(userlogger, "TC Type -", null,
							suit.getTestcasesuites().get(i).get("tcType").toString(), "info");
					logerOBJ.customlogg(userlogger, "TC Sequence : ", null,
							suit.getTestcasesuites().get(i).get("tcSeqref").toString(), "info");
					hybridExecution = true;
					executionDetail.setTestcaseCode(
							suit.getTestcasesuites().get(i).get(UtilEnum.TESTCASECODE.value()).asText());
					executionDetail
							.setTestcaseID(suit.getTestcasesuites().get(i).get(UtilEnum.TESTCASEID.value()).asInt());
					executionDetail.setTcSeq(suit.getTestcasesuites().get(i).get(UtilEnum.TCSEQ.value()).asInt());
					executionDetail.setModuleID(suit.getTestcasesuites().get(i).get(UtilEnum.MODULEID.value()).asInt());
					executionDetail.setVersion(suit.getTestcasesuites().get(i).get(UtilEnum.VERSION.value()).asInt());
					executionDetail.setTcType(suit.getTestcasesuites().get(i).get("tcType").asText());
//					if (executionDetail.getTcType().equals("mobile"))
//						executionDetail.setMbType(suit.getTestcasesuites().get(i).get("mbType").asText());
					tcfnObject = InitializeDependence.serverCall.getTCstep(executionDetail, header);
					if (executionDetail.getTcType().toLowerCase().equals("web"))
						executor = new WebExecutorImpl();
					else if (executionDetail.getTcType().toLowerCase().equals("mobile")) {
						if (DeviceManager.devices.get(executionDetail.getDevicesID()) != null) {
							executionDetail.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(executionDetail.getDevicesID())) {
									executionDetail.setMbType("ios");
									break;
								}
						}
						executor = new MobileExecutorImpl();
					} else if (executionDetail.getTcType().toLowerCase().equals("mainframe"))
						executor = new Executionimpl();
					else if (executionDetail.getTcType().toLowerCase().equals("desktop"))
						executor = new DesktopExecutionImpl();
					else if (executionDetail.getTcType().toLowerCase().equals("sap"))
						executor = new SapExecutionImpl();
					else if (executionDetail.getTcType().toLowerCase().equals("hybrid"))
						executor = null;
					exeSetupexec = null;
					Cloner cloner = new Cloner();
					exeSetupexec = cloner.deepClone(executionDetail);

					testcaseExecution(tcfnObject, exeSetupexec);
					Thread.sleep(4000);
					logger.info("Execution  started with testcaseID:{}", executionDetail.getTestcaseID());

				}
			}
			System.out.println(InitializeDependence.webDriver.get(sessionId));
			// API for saving user logs
			Service.saveLogs(executionDetail, header);
			logger.info("Execution Finish:{}", executionDetail.getExecutionID());

			// logerOBJ.customlogg(userlogger,"Execution finished
			// ",null,executionDetail.getTestcaseID().toString(),"info");
			if (executionDetail.getMbType() != null && executionDetail.getMbType().equals(UtilEnum.ANDROID.value())) {
				if (!GlobalDetail.websocket.isEmpty()) {
					GlobalDetail.websocket.get(executionDetail.getDevicesID()).getBasicRemote().sendText("off");
					GlobalDetail.websocket.get(executionDetail.getDevicesID()).close();
					GlobalDetail.websocket.remove(executionDetail.getDevicesID());
					GlobalDetail.screenshot.remove(executionDetail.getDevicesID());

					if (!InitializeDependence.aMirror.isEmpty()) {
						InitializeDependence.aMirror.get(executionDetail.getDevicesID()).stopScreenListener();
						InitializeDependence.aMirror.remove(executionDetail.getDevicesID());
					}
				}
			}
			if (executionDetail.getMbType() != null && executionDetail.getMbType().equals(UtilEnum.IOS.value())) {
				if (!GlobalDetail.websocket.isEmpty()) {
					GlobalDetail.websocket.get(executionDetail.getDevicesID()).getBasicRemote().sendText("off");
					GlobalDetail.websocket.get(executionDetail.getDevicesID()).close();
					GlobalDetail.websocket.remove(executionDetail.getDevicesID());
					GlobalDetail.screenshot.remove(executionDetail.getDevicesID());
				}
			}
			if (hybridExecution) {
				if (!GlobalDetail.ReferencePlayback) {
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
							ExecutionOption.FINISHEXECUITON, null));
				}
			}
			if (jenkinsocket.socket != null) {
				jenkinsocket.socket.disconnect();
				jenkinsocket.socket.connect();
			}

		} catch (

		Exception e) {
			ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
					ExecutionOption.FINISHEXECUITON, null));
			logger.info("Unable to start Exexution: {}", e);
			try {
				Thread.currentThread().join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			System.out.println(InitializeDependence.webDriver.get(sessionId));
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
			System.gc();
			runtime.freeMemory();
			System.out.println(InitializeDependence.webDriver.get(sessionId));
		}
	}

	private Step fnStep;
	private int itr;
	private String sessionId = null;
	private Boolean hybridExecution = false;

	public void testcaseExecution(Testcase tcfnObject, Setupexec execSetupexec) {
		try {

			if (!GlobalDetail.Wdaproxy.isEmpty()) {
				if (GlobalDetail.Wdaproxy.containsKey(execSetupexec.getDevicesID())) {
					if (!ideviceMaganger.devices.get(execSetupexec.getDevicesID()).has("simulator")) {
						GlobalDetail.Wdaproxy.remove(execSetupexec.getDevicesID());
						GlobalDetail.Wdascreen.remove(execSetupexec.getDevicesID());
						InitializeDependence.xctestrun.remove(execSetupexec.getDevicesID());
						Idevice.startSession(execSetupexec.getDevicesID());
					} else {
						Idevice.startSession(execSetupexec.getDevicesID());
					}
				}
			}
//			ArrayList<Step> ifstep = new ArrayList<Step>();

			boolean flag = false;
			boolean ifFlag = false;
			boolean loopFlag = false;
			boolean if_inside_for = false;
			boolean for_inside_if = false;
			int resumeStep = -1;

			int iteration = tcfnObject.getTestdata().size();
			for (int i = 0; i < tcfnObject.getTestdata().size(); i++) {
				APILogger.customloggAPI(APIlogger, "ITERATION ------------------ ",i+1, null, "info");
				APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
				
				InitializeDependence.global_itr = i + 1;

				InitializeDependence.current_step = 0;
				ArrayList<Step> ifstep = new ArrayList<Step>();
				// loops for iteration
				ArrayList<Step> loopstep = new ArrayList<Step>();

				if (iteration != 1) {
					logerOBJ.customlogg(userlogger,
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------",
							null, "tc done", "info");
					logerOBJ.customlogg(userlogger, "Iteration : ", null, String.valueOf(i + 1), "info");
					logerOBJ.customlogg(userlogger,
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------",
							null, "tc done", "info");

				}
				InitializeDependence.iteration = false;
				// stop mirror on iterations mobile

				if (executionDetail != null) {
					if (i == iteration - 1 && executionDetail.getTcType().toLowerCase().equals("mobile")) {
						InitializeDependence.iteration = true;
					}
				}
				oncontinue_check = true;
				flag = false;
				InitializeDependence.generaldriver = new GeneralMethod();
				GlobalDetail.screenshot.clear();
				GlobalDetail.runTime.clear();
				if (i == 0 && tcfnObject.getSteps().get(0).getSeq() == 1
						&& (tcfnObject.getSteps().get(0).getSeq2() == null
								|| tcfnObject.getSteps().get(0).getSeq2().equals("1")
								|| tcfnObject.getSteps().get(0).getSeq2().equals("1.1"))) {
					sessionId = UUID.randomUUID().toString();
				}
				InitializeDependence.seq = 1;
				if (!GlobalDetail.ReferencePlayback) {
					ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
							UtilEnum.INPROGRESS.value(), null, ExecutionOption.STARTEXECUTION, null));
				}

				if (GlobalDetail.ResumeStep != null) {
					if (GlobalDetail.ResumeStep.split("\\.").length == 1) {
						resumeStep = Integer.parseInt(GlobalDetail.ResumeStep);
						GlobalDetail.ResumeStep = null;
					} else {
						resumeStep = Integer.parseInt(GlobalDetail.ResumeStep.split("\\.")[0]);
						GlobalDetail.ResumeStep = GlobalDetail.ResumeStep.split("\\.", 2)[1];
					}
				}
				System.out.println(InitializeDependence.webDriver.get(sessionId));
				for (Step step : tcfnObject.getSteps()) {

					InitializeDependence.protected_password = new JSONObject();

					// logs addition for protected type password
					List<JsonNode> currentObjParams = step.getParameters();
//					JsonNode re =currentObjParams.get(counter);
					int param_counts = 0;
					for (JsonNode attr : currentObjParams) {
						if (attr.has("protected")) {
							InitializeDependence.protected_password.put(String.valueOf(param_counts),
									attr.get("protected").asBoolean());
						}

						param_counts++;
					}

					// till here

					InitializeDependence.current_step = InitializeDependence.current_step + 1;
					step.setSeq(InitializeDependence.current_step);
					if (resumeStep != -1)
						if (InitializeDependence.current_step < resumeStep)
							continue;
//					if(counter < GlobalDetail.ResumeStep && counter != (int) GlobalDetail.ResumeStep)
//						continue;
					// User logs...........
					String convertSeqToString = String.valueOf(InitializeDependence.current_step);
					logerOBJ.customlogg(userlogger, "Step No. : ", null, convertSeqToString, "info");

					String convertgetDescriptionToString = String.valueOf(step.getDescription());
					logerOBJ.customlogg(userlogger, "Step Description :", null, convertgetDescriptionToString, "info");

					String convertObjectToString = String.valueOf(step.getObject_name());
					logerOBJ.customlogg(userlogger, "Object Name :", null, convertObjectToString, "info");

					String convertActionNameToString = String.valueOf(step.getActionname());
					logerOBJ.customlogg(userlogger, "Step Action :", null, convertActionNameToString, "info");

					int jsonLength = step.getParameters().size();
					if (jsonLength == 0) {
						String convertParamaterCountToString = String.valueOf(step.getParameters().size());
						// step.getParameters().size();
						logerOBJ.customlogg(userlogger, "Step Parameters Count : ", null, convertParamaterCountToString,
								"info");
					}

					for (int loop = 0; loop < jsonLength; loop++) {

						logerOBJ.customlogg(userlogger, "Parameter Name -", step.getParameters().get(loop).get("name"),
								null, "info");

						if (step.getParameters().get(loop).has("defaultValue")) {
							logerOBJ.customlogg(userlogger, "Parameter Value -",
									step.getParameters().get(loop).get("defaultValue"), null, "info");
						} else {
							if (step.getParameters().get(loop).has("protected")) {
								if (Boolean.parseBoolean(step.getParameters().get(loop).get("protected").toString())) {

									String dem = step.getParameters().get(loop).get("name").asText();

									int param_length = tcfnObject.getTestdata().get(i).get(dem).asText().length();
									String val = tcfnObject.getTestdata().get(i).get(dem).asText();
									for (int pass_size = 0; pass_size < param_length; pass_size++) {

										val = val.replace(val.charAt(pass_size), '*');
									}
									logerOBJ.customlogg(userlogger, "Parameter value", val, "-", "info");

								} else {
									logerOBJ.customlogg(userlogger, "Parameter value", null, "-", "info");
								}
							} else {
								logerOBJ.customlogg(userlogger, "Parameter value", null, "-", "info");
							}

						}

					}

					// loops implementation
					if (step.isStartFor()) {
						if (!for_inside_if) {
							loopstep.add(step);
							if_inside_for = true;
							loopFlag = true;
						}

						else {
							ifstep.add(step);
						}

						continue;
					}

					if (step.isiF()) {

						if (!if_inside_for) {
							for_inside_if = true;
							ifstep = new ArrayList<Step>();
							ifstep.add(step);
							ifFlag = true;
							continue;
						}

						else {
							loopstep.add(step);
							continue;
						}
					}
					if (step.iseLse()) {

						if (!if_inside_for)
							ifstep.add(step);

						else
							loopstep.add(step);
						continue;
					}
					if (step.isEndif()) {

						if (!if_inside_for) {
							for_inside_if = false;

							ifFlag = false;
							ifstep.add(step);
							if (ifExecution(ifstep, tcfnObject.getTestdata().get(i), i, -1, sessionId,
									tcfnObject.getSteps().size(), execSetupexec)) {

								if (!GlobalDetail.ReferencePlayback) {
									logger.info("Testcase Failed:{}", execSetupexec.getTestcaseID());
									// Userlogs...........
									String testcaseFailedReason = String.valueOf(execSetupexec.getTestcaseID());

									// Userlogs...........

									ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
											UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
									executor.stopExecution(execSetupexec, header, hybridExecution);
									logerOBJ.customlogg(userlogger, "TestCase Failed", null, "", "error");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");

								}
								step.setDescription(step.getObject_name());
								flag = true;
								break;
							} else {
								if (!GlobalDetail.ReferencePlayback)
									InitializeDependence.seq++;
								continue;

							}
						}

						else {
							loopstep.add(step);
							continue;
						}
					}

					if (step.isEndFor()) {

						if_inside_for = false;

						if (!for_inside_if) {

							loopFlag = false;
							loopstep.add(step);
							if (forExecution(loopstep, tcfnObject.getTestdata().get(i), itr, -1, sessionId,
									tcfnObject.getSteps().size(), execSetupexec))

							{

								if (!GlobalDetail.ReferencePlayback) {
									ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
											UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
									executor.stopExecution(execSetupexec, header, hybridExecution);

								}

								step.setDescription(step.getObject_name());
								flag = true;
								break;
							} else {
								if (!GlobalDetail.ReferencePlayback) {
									InitializeDependence.seq++;
									logger.info("Loop got executed");
								}

								continue;

							}
						} else {
							ifstep.add(step);
							continue;
						}

					}
					if (loopFlag) {
						loopstep.add(step);
						continue;
					}
					if (ifFlag) {
						if (!if_inside_for)
							ifstep.add(step);

						else
							loopstep.add(step);
						continue;
					}
					if (step.getSkip()) {
						logerOBJ.customlogg(userlogger, "Step Skipped...", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							ref.put("stepResult", "passed");
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						InitializeDependence.seq++;
						if (!GlobalDetail.ReferencePlayback) {
							logger.info("Step Skiped:{}", execSetupexec.getTestcaseID());
							String[] td = null;
							if (!step.getParameters().isEmpty()) {
								td = new String[step.getParameters().size()];
								for (int j = 0; j < step.getParameters().size(); j++) {
									td[j] = tcfnObject.getTestdata().get(i)
											.get(step.getParameters().get(j).get("name").asText()).asText();
								}
							}
							if (step.getFunctionId() != null) {
								step.setDescription(step.getObject_name());
							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, td,
									UtilEnum.SKIP.value(), null, ExecutionOption.STEPRESULT, step));
						} else {
							if (step.getFunctionId() != null) {
								JSONObject ref = new JSONObject();
								InitializeDependence.seq = InitializeDependence.current_step + 1;
								Testcase fnObject = InitializeDependence.serverCall.getFNstep(execSetupexec, step,
										tcfnObject.getTestdata().get(i), header);
								for (Step step1 : fnObject.getSteps()) {
									ref.clear();
									ref.put("stepno", InitializeDependence.seq);
									ref.put("stepResult", "false condition");
									ref.put("seq", String.valueOf(InitializeDependence.current_step) + "."
											+ String.valueOf(step1.getSeq()));
									GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
									InitializeDependence.seq = InitializeDependence.seq + 1;
								}
							}
						}
						continue;
					}

					// resume to be added
					if (step.getFunctionId() != null) {
						InitializeDependence.seq = step.getSeq() + 1;
						itr = i;
						fnStep = step;
						if (initFNexecution(step, tcfnObject.getTestdata().get(i), sessionId, itr, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", step.getObject_name());
							if (!GlobalDetail.ReferencePlayback) {
								logger.info("Testcase Failed:{}", execSetupexec.getTestcaseID());
								ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
										UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
//								executor.stopExecution(execSetupexec, header, hybridExecution);

								logerOBJ.customlogg(userlogger, "TestCase Failed", null, "", "error");
								logerOBJ.customlogg(userlogger,
										"----------------------------------------------------------------------------------------",
										null, "tc done", "info");

								if (!step.isContinueOnFailure())
									executor.stopExecution(execSetupexec, header, hybridExecution);
							}

							step.setDescription(step.getObject_name());

							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");

							if (!step.isContinueOnFailure()) {
								flag = true;
								break;
							}

							else {
								oncontinue_check = false;
								continue;
							}
						} else {
							if (GlobalDetail.referencestep != null) {
								JSONObject ref = new JSONObject();
								ref.put("stepno", step.getSeq());
								ref.put("stepResult", "passed");
								if (step.getSeq2() != null)
									ref.put("seq", step.getSeq2());
								else
									ref.put("seq", step.getSeq());
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							}
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;
						}
					}

					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtoweb.value())) {
						InitializeDependence.ios_device = false;
						hybridExecution = true;
						String brower = tcfnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText().toLowerCase().trim();
						execSetupexec.setBrowserName(brower);
						executor = new WebExecutorImpl();

						if (!driversession.containsKey(brower)) {
							driversession.put(brower, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							ref.put("stepResult", "passed");
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						sessionId = driversession.get(brower);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(brower));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, tcfnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;
					}
					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtomobile.value())) {
						hybridExecution = true;
						String deivceId = tcfnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText();
						execSetupexec.setDevicesID(deivceId);
						if (DeviceManager.devices.get(deivceId) != null) {
							execSetupexec.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(deivceId)) {
									execSetupexec.setMbType("ios");
									break;
								}
						}
						executor = new MobileExecutorImpl();
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							ref.put("stepResult", "passed");
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						System.out.println(InitializeDependence.getTdArray(fnStep, tcfnObject.getTestdata().get(i),
								executionDetail, sessionId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, tcfnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtodesktop.value())) {
						hybridExecution = true;
						String deivceId = tcfnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText();
						execSetupexec.setDevicesID(deivceId);
						executor = new DesktopExecutionImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							ref.put("stepResult", "passed");
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, tcfnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");

						continue;

					}
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", step.getSeq());
						ref.put("stepResult", "current");
						if (step.getSeq2() != null)
							ref.put("seq", step.getSeq2());
						else
							ref.put("seq", step.getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
					if (executor.stepExecution(execSetupexec, step, tcfnObject.getTestdata().get(i), i, header,
							sessionId)) {
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							InitializeDependence.seq++;
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							if (tcfnObject.getSteps().size() == step.getSeq()) {
								JSONObject pass = new JSONObject();
								pass.put("execution", "passed");
								GlobalDetail.referencestep.getAsyncRemote().sendText(pass.toString());
							}

						}

						flag = false;
						continue;
					}

					// resume to be added
					else {
						InitializeDependence.closePreLoader(sessionId);
						System.out.println(InitializeDependence.webDriver.get(sessionId));
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", step.getSeq());
							ref.put("stepResult", "failed");
							if (step.getSeq2() != null)
								ref.put("seq", step.getSeq2());
							else
								ref.put("seq", step.getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							JSONObject fail = new JSONObject().put("execution", "failed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(fail.toString());

							// userlogs....
							if (step.getSeq2() != null) {
								System.out.println(step.getSeq2());
								String tcSeq2 = String.valueOf(step.getSeq2());
								// logerOBJ.customlogg(userlogger,"Function Step Sequence :", null, tcSeq2,
								// "info");
							}
							// userlogs....
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						logger.info("Step Failed by Object name:{}", step.getObject_name());

						// String functionTcFailed= String.valueOf(step.getObject_name());
						// logerOBJ.customlogg(userlogger,"Function Step Failed By Object :", null,
						// functionTcFailed, "error");

						if (!GlobalDetail.ReferencePlayback) {
							InitializeDependence.closePreLoader(sessionId.toLowerCase().trim());
							logger.info("Testcase Failed:{}", execSetupexec.getTestcaseID());
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
//							executor.stopExecution(execSetupexec, header, hybridExecution);

							logerOBJ.customlogg(userlogger, "TestCase Failed", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");

							if (!step.isContinueOnFailure())
								executor.stopExecution(execSetupexec, header, hybridExecution);
						}
						System.out.println(InitializeDependence.webDriver.get(sessionId));
						if (!step.isContinueOnFailure()) {
							flag = true;
							break;
						}

						else {
							oncontinue_check = false;
							continue;
						}
					}
				}
				if (flag == false) {

					if (oncontinue_check) {
						if (!GlobalDetail.ReferencePlayback) {
							
							InitializeDependence.closePreLoader(sessionId.toLowerCase().trim());
							logger.info("Testcase Passed:{}", execSetupexec.getTestcaseID());
							
							Thread.sleep(3000);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header,InitializeDependence.global_itr-1, null,
									UtilEnum.PASSED.value(), null, ExecutionOption.TESTCASERESULT, null));
							executor.stopExecution(execSetupexec, header, hybridExecution);
							logerOBJ.customlogg(userlogger, "TestCase Passed", null, "", "info");

							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							// logerOBJ.customlogg(userlogger,"------------------------------------------------",
							// null, "", "info");
//						InitializeDependence.webDriver.get(sessionId).deleteSession();
						} else {
							logger.info("Testcase Passed:{}");
							logerOBJ.customlogg(userlogger, "TestCase Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
//							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
//									UtilEnum.PASSED.value(), null, ExecutionOption.TESTCASERESULT, null));

						}
					}

					else {
						if (!GlobalDetail.ReferencePlayback) {
							InitializeDependence.closePreLoader(sessionId.toLowerCase().trim());
							logger.info("Testcase Failed:{}", execSetupexec.getTestcaseID());
							Thread.sleep(3000);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header,InitializeDependence.global_itr-1, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
							executor.stopExecution(execSetupexec, header, hybridExecution);
//							InitializeDependence.webDriver.get(sessionId).deleteSession();
						} else {
							logger.info("Testcase Failed:{}");
							logerOBJ.customlogg(userlogger, "TestCase Failed", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
					}
				}
				if (hybridExecution) {
					if (!driversession.isEmpty()) {
						for (Entry<String, String> map : driversession.entrySet()) {
							if (!InitializeDependence.androidDriver.isEmpty()
									&& InitializeDependence.androidDriver.containsKey(map.getKey())) {
								((AppiumDriver) InitializeDependence.androidDriver.get(map.getKey()).driver).closeApp();
								InitializeDependence.androidDriver.remove(map.getKey());
								if (GlobalDetail.runTime.containsKey(map.getKey().toLowerCase().trim()))
									GlobalDetail.runTime.remove(map.getKey().toLowerCase().trim());
							}
							if (!InitializeDependence.iosDriver.isEmpty()
									&& InitializeDependence.iosDriver.containsKey(map.getKey())) {
								InitializeDependence.iosDriver.get(map.getKey()).closeApp();
								InitializeDependence.iosDriver.remove(map.getKey());
								if (GlobalDetail.runTime.containsKey(map.getKey().toLowerCase().trim()))
									GlobalDetail.runTime.remove(map.getKey().toLowerCase().trim());
							}

							System.out.println(driversession.get(map.getValue()));

							if (!InitializeDependence.webDriver.isEmpty()
									&& InitializeDependence.webDriver.containsKey(map.getValue())) {
								if (InitializeDependence.webDriver.get(map.getValue()).driver != null) {
									InitializeDependence.webDriver.get(map.getValue()).deleteSession();
									InitializeDependence.webDriver.get(map.getValue()).closeDriver();
								}
								if (InitializeDependence.webDriver.get(map.getValue()).isStWebExe())
									InitializeDependence.webDriver.get(map.getValue()).setStWebExe(false);
								InitializeDependence.webDriver.remove(map.getValue());
								if (GlobalDetail.runTime.containsKey(map.getValue().toLowerCase().trim()))
									GlobalDetail.runTime.remove(map.getValue().toLowerCase().trim());
							}

							// added
							if (!InitializeDependence.webDriver.isEmpty()
									&& InitializeDependence.webDriver.containsKey(sessionId)) {
								System.out.println("got inside the added if condition");
								if (InitializeDependence.webDriver.get(sessionId).driver != null) {
									InitializeDependence.webDriver.get(sessionId).deleteSession();
									InitializeDependence.webDriver.get(sessionId).closeDriver();
								}
								if (InitializeDependence.webDriver.get(sessionId).isStWebExe())
									InitializeDependence.webDriver.get(sessionId).setStWebExe(false);
								InitializeDependence.webDriver.remove(sessionId);
								if (GlobalDetail.runTime.containsKey(sessionId.toLowerCase().trim()))
									GlobalDetail.runTime.remove(sessionId.toLowerCase().trim());
							}
						}
					} else {
						if (!InitializeDependence.androidDriver.isEmpty()
								&& InitializeDependence.androidDriver.containsKey(sessionId)) {
							((AppiumDriver) InitializeDependence.androidDriver.get(sessionId).driver).closeApp();
							InitializeDependence.androidDriver.remove(sessionId);
							if (GlobalDetail.runTime.containsKey(execSetupexec.getDevicesID().toLowerCase().trim()))
								GlobalDetail.runTime.remove(execSetupexec.getDevicesID().toLowerCase().trim());
						}
						if (!InitializeDependence.iosDriver.isEmpty()
								&& InitializeDependence.iosDriver.containsKey(sessionId)) {
							InitializeDependence.iosDriver.get(sessionId);
							InitializeDependence.iosDriver.remove(sessionId);
							if (GlobalDetail.runTime.containsKey(execSetupexec.getDevicesID().toLowerCase().trim()))
								GlobalDetail.runTime.remove(execSetupexec.getDevicesID().toLowerCase().trim());
						}
						if (!InitializeDependence.webDriver.isEmpty()
								&& InitializeDependence.webDriver.containsKey(sessionId)) {

							// added
							if (InitializeDependence.suite_new_session) {
								if (InitializeDependence.webDriver.get(sessionId).driver != null) {
									InitializeDependence.webDriver.get(sessionId).deleteSession();
									InitializeDependence.webDriver.get(sessionId).closeDriver();
								}
								if (InitializeDependence.webDriver.get(sessionId).isStWebExe())
									InitializeDependence.webDriver.get(sessionId).setStWebExe(false);
								InitializeDependence.webDriver.remove(sessionId);
								if (GlobalDetail.runTime.containsKey(sessionId.toLowerCase().trim()))
									GlobalDetail.runTime.remove(sessionId.toLowerCase().trim());
							} else {
								logger.info("Continuing with same session");
							}
						}

						// added
						
						if (InitializeDependence.suite) {
							suitesessioncheck(iteration);
						}
//							if (!InitializeDependence.webDriver.isEmpty()
//									&& InitializeDependence.suite_tescase_count == InitializeDependence.suite_testcase_sequence
//											+ 1
//									&& !InitializeDependence.suite_new_session
//									&& InitializeDependence.global_itr == iteration) {
//
//								if (InitializeDependence.suite_tescase_count != 1) {
//									InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//											.deleteSession();
//									InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//											.closeDriver();
//								} else if (InitializeDependence.suite_tescase_count == 1) {
//
//									if (iteration == 1) {
//										InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//												.deleteSession();
//										InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//												.closeDriver();
//									} else if (InitializeDependence.global_itr == iteration) {
//										InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//												.deleteSession();
//										InitializeDependence.webDriver.get(InitializeDependence.suite_session)
//												.closeDriver();
//
//									}
//
//								}
//							}
//
//						}
					}
					InitializeDependence.closePreLoader(sessionId.toLowerCase().trim());
				}
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}

	}
	
	public void suitesessioncheck(int iteration) {
		if (!InitializeDependence.webDriver.isEmpty()
				&& InitializeDependence.suite_tescase_count == InitializeDependence.suite_testcase_sequence
						+ 1
				&& !InitializeDependence.suite_new_session
				&& InitializeDependence.global_itr == iteration) {

			if (InitializeDependence.suite_tescase_count != 1) {
				InitializeDependence.webDriver.get(InitializeDependence.suite_session)
						.deleteSession();
				InitializeDependence.webDriver.get(InitializeDependence.suite_session)
						.closeDriver();
			} else if (InitializeDependence.suite_tescase_count == 1) {

				if (iteration == 1) {
					InitializeDependence.webDriver.get(InitializeDependence.suite_session)
							.deleteSession();
					InitializeDependence.webDriver.get(InitializeDependence.suite_session)
							.closeDriver();
				} else if (InitializeDependence.global_itr == iteration) {
					InitializeDependence.webDriver.get(InitializeDependence.suite_session)
							.deleteSession();
					InitializeDependence.webDriver.get(InitializeDependence.suite_session)
							.closeDriver();

				}
			}
			}
		
	}
	public Boolean initFNexecution(Step tcfnObject, JsonNode tcData, String sessionId, int itr,
			Setupexec execSetupexec) {

		// Userlogs.........
		String functionIdToString = String.valueOf(tcfnObject.getFunctionId());
		logerOBJ.customlogg(userlogger, "FUNCTION CODE  :", null, functionIdToString, "info");
		// logger.info("FUNCTION CODE : "+tcfnObject.getFunctionId());
		String functionNameToString = String.valueOf(tcfnObject.getObject_name());
		logerOBJ.customlogg(userlogger, "FUNCTION NAME  :", null, functionNameToString, "info");

		// Userlogs.........
		Testcase fnObject = InitializeDependence.serverCall.getFNstep(execSetupexec, tcfnObject, tcData, header);
//		fnObject = executor.exeIfEvaluation(fnObject);
		return fnExecution(fnObject, sessionId, itr, execSetupexec, tcfnObject.getSeq());

	}

	public Boolean fnExecution(Testcase fnObject, String sessionId, int itr, Setupexec execSetupexec, int seq) {

		boolean flag = false;
		boolean ifFlag = false;
		boolean loopFlag = false;
		int functionCounter = 0;
		boolean if_inside_for = false;
		boolean for_inside_if = false;
		ArrayList<Step> ifstep = new ArrayList<Step>();
		ArrayList<Step> loopstep = new ArrayList<Step>();
		int resumeStep = -1;

		try {

			if (functionCounter == 0) {
				String current_step = String.valueOf(InitializeDependence.current_step);
				// logerOBJ.customlogg(userlogger,"Function Step Sequence :", null,
				// current_step, "info");
				logerOBJ.customlogg(userlogger,
						"-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
						null, null, "info");
				functionCounter = 1;

			}
			if (GlobalDetail.ResumeStep != null) {
				if (GlobalDetail.ResumeStep.split("\\.").length == 1) {
					resumeStep = Integer.parseInt(GlobalDetail.ResumeStep);
					GlobalDetail.ResumeStep = null;
				} else {
					resumeStep = Integer.parseInt(GlobalDetail.ResumeStep.split("\\.")[0]);
					GlobalDetail.ResumeStep = GlobalDetail.ResumeStep.split("\\.", 2)[1];
				}
			}
			for (int i = 0; i < fnObject.getTestdata().size(); i++) {
				InitializeDependence.funcItr_suite = i + 1;
				for (Step step : fnObject.getSteps()) {
					step.setSeq(InitializeDependence.current_step);
					String first = String.valueOf(seq);
					String second = String.valueOf(functionCounter);
					String converted = first + "." + second;
					step.setSeq2(converted);
					// System.out.println(step.getActionname());fn
					logerOBJ.customlogg(userlogger, "Function Step Sequence main :", null, converted, "info");
					functionCounter++;
					if (resumeStep != -1)
						if (step.getSeq() < resumeStep)
							continue;
//					if(GlobalDetail.ResumeStep < Float.parseFloat(converted))
//						continue;
					String convertgetDescriptionToString = String.valueOf(step.getDescription());
					logerOBJ.customlogg(userlogger, "Step Description :", null, convertgetDescriptionToString, "info");

					String convertObjectToString = String.valueOf(step.getObject_name());
					logerOBJ.customlogg(userlogger, "Object Name :", null, convertObjectToString, "info");

					String convertActionNameToString = String.valueOf(step.getActionname());
					logerOBJ.customlogg(userlogger, "Step Action :", null, convertActionNameToString, "info");
					// Adding
					if (step.getFunctionId() == null) {
						int jsonLength = step.getParameters().size();
						if (jsonLength == 0) {
							String convertParamaterCountToString = String.valueOf(step.getParameters().size());
							// step.getParameters().size();
							logerOBJ.customlogg(userlogger, "Step Parameters Count : ", null,
									convertParamaterCountToString, "info");
						}

						for (int loop = 0; loop < jsonLength; loop++) {

							logerOBJ.customlogg(userlogger, "Parameter Name -",
									step.getParameters().get(loop).get("name"), null, "info");

							if (step.getParameters().get(loop).has("defaultValue")) {
								logerOBJ.customlogg(userlogger, "Parameter Value -",
										step.getParameters().get(loop).get("defaultValue"), null, "info");
							} else {

								logerOBJ.customlogg(userlogger, "Parameter value", null, "-", "info");

							}

						}
					}

					if (step.isStartFor()) {
						if (!for_inside_if) {
							loopstep.add(step);
							if_inside_for = true;
							loopFlag = true;
						}

						else {
							ifstep.add(step);
						}

						continue;
					}

					if (step.isiF()) {
						if (!if_inside_for) {
							for_inside_if = true;
							ifstep = new ArrayList<Step>();
							ifstep.add(step);
							ifFlag = true;
							continue;
						}

						else {
							loopstep.add(step);
							continue;
						}
					}
					if (step.iseLse()) {
						if (!if_inside_for)
							ifstep.add(step);

						else
							loopstep.add(step);
						continue;
					}
					if (step.isEndif()) {
						if (!if_inside_for) {
							for_inside_if = false;

							ifFlag = false;
							ifstep.add(step);
							if (ifExecution(ifstep, fnObject.getTestdata().get(i), itr, -1, sessionId,
									fnObject.getSteps().size(), execSetupexec)) {

								if (!GlobalDetail.ReferencePlayback) {
									logger.info("Testcase Failed:{}", execSetupexec.getTestcaseID());
									// Userlogs...........
									String testcaseFailedReason = String.valueOf(execSetupexec.getTestcaseID());

									// Userlogs...........

									ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
											UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));
									executor.stopExecution(execSetupexec, header, hybridExecution);
									logerOBJ.customlogg(userlogger, "TestCase Failed", null, "", "error");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");

								}
								step.setDescription(step.getObject_name());
								flag = true;
								break;
							} else {
								if (GlobalDetail.referencestep != null)
									InitializeDependence.seq += 1;
								continue;

							}
						}

						else {
							loopstep.add(step);
							continue;
						}
					}

					if (step.isEndFor()) {

						if_inside_for = false;

						if (!for_inside_if) {

							loopFlag = false;
							loopstep.add(step);
							if (forExecution(loopstep, fnObject.getTestdata().get(i), itr, -1, sessionId,
									fnObject.getSteps().size(), execSetupexec))

							{

								if (!GlobalDetail.ReferencePlayback) {
									ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, null,
											UtilEnum.FAILED.value(), null, ExecutionOption.TESTCASERESULT, null));

								}

								step.setDescription(step.getObject_name());
								flag = true;
								break;
							} else {
								if (GlobalDetail.referencestep != null) {
									InitializeDependence.seq += 1;
									logger.info("Loop got executed");
								}

								continue;

							}
						} else {
							ifstep.add(step);
							continue;
						}

					}
					if (loopFlag) {
						loopstep.add(step);
						continue;
					}

					if (ifFlag) {
						ifstep.add(step);
						continue;
					}
					if (step.getSkip()) {
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("stepResult", "passed");
							ref.put("seq", converted.toString());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						if (!GlobalDetail.ReferencePlayback) {
							logger.info("Step Skiped:{}", execSetupexec.getTestcaseID());
							String[] td = null;
							if (!step.getParameters().isEmpty()) {
								td = new String[step.getParameters().size()];
								for (int j = 0; j < step.getParameters().size(); j++) {
									td[j] = fnObject.getTestdata().get(i)
											.get(step.getParameters().get(j).get("name").asText()).asText();
								}
							}
							ExecutionService.a().set(sessionId.toString());
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, itr, td,
									UtilEnum.SKIP.value(), null, ExecutionOption.STEPRESULT, step));
						}
						if (GlobalDetail.referencestep != null)
							InitializeDependence.seq += 1;
						flag = false;
						continue;
					}

					// added for resume step
					if (step.getFunctionId() != null) {
						fnStep = step;

						InitializeDependence.seq = step.getSeq() + 1;

						if (initFNexecution(step, fnObject.getTestdata().get(i), sessionId, itr, execSetupexec)) {

							logger.info("Step Failed by Object name:{}", step.getObject_name());
							step.setDescription(step.getObject_name());

							if (!step.isContinueOnFailure()) {
								flag = true;
								break;
							}

							else {
								oncontinue_check = false;
								if (GlobalDetail.referencestep != null)
									InitializeDependence.seq += 1;
								continue;
							}

						} else {
							if (GlobalDetail.referencestep != null) {
								JSONObject ref = new JSONObject();
								ref.put("stepno", InitializeDependence.seq);
								ref.put("stepResult", "passed");
								ref.put("seq", converted.toString());
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							}
							if (GlobalDetail.referencestep != null)
								InitializeDependence.seq += 1;
							continue;
						}
					}
					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtoweb.value())) {
						hybridExecution = true;
						String brower = fnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText().toLowerCase().trim();
						execSetupexec.setBrowserName(brower);
						executor = new WebExecutorImpl();

						if (!driversession.containsKey(brower)) {
							driversession.put(brower, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("seq", converted.toString());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						sessionId = driversession.get(brower);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(brower));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, fnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						if (GlobalDetail.referencestep != null)
							InitializeDependence.seq += 1;
						// logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------",
						// null, "tc done", "info");
						continue;
					}
					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtomobile.value())) {
						hybridExecution = true;
						String deivceId = fnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText();
						execSetupexec.setDevicesID(deivceId);
						if (DeviceManager.devices.get(deivceId) != null) {
							execSetupexec.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(deivceId)) {
									execSetupexec.setMbType("ios");
									break;
								}
						}
						executor = new MobileExecutorImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("seq", converted.toString());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, fnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,

								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						if (GlobalDetail.referencestep != null)
							InitializeDependence.seq += 1;
						continue;

					}
					if (step.getAction().getName().toLowerCase().equals(ActionClass.switchtodesktop.value())) {
						hybridExecution = true;
						String deivceId = fnObject.getTestdata().get(i)
								.get(step.getParameters().get(0).get("name").asText()).asText();
						executor = new DesktopExecutionImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("seq", converted.toString());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(step, fnObject.getTestdata().get(i),
												executionDetail, sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, step));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						if (GlobalDetail.referencestep != null)
							InitializeDependence.seq += 1;
						continue;

					}
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", InitializeDependence.seq);
						ref.put("stepResult", "current");
						ref.put("seq", converted.toString());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
					}
					// added for resume step
					if (executor.stepExecution(execSetupexec, step, fnObject.getTestdata().get(i), itr, header,
							sessionId)) {
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("seq", converted.toString());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						}
						if (GlobalDetail.referencestep != null)
							InitializeDependence.seq += 1;
						flag = false;
						continue;
					} else {
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", InitializeDependence.seq);
							ref.put("stepResult", "failed");
							ref.put("seq", converted.toString());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							JSONObject fail = new JSONObject().put("execution", "failed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(fail.toString());
						}
						if (!step.isContinueOnFailure()) {
							flag = true;
							break;
						}

						else {
							if (GlobalDetail.referencestep != null)
								InitializeDependence.seq += 1;
							oncontinue_check = false;
							continue;
						}
					}
				}
				if (flag == true)
					break;
				else
					continue;
			}
			// Service.saveLogs(executionDetail,header);
			return flag;
		} catch (

		Exception e) {
			e.printStackTrace();
			ExecutionService.a().set(sessionId.toString());
			ExecutionService.a().a(new ExecutionTask(execSetupexec, header, itr, null, UtilEnum.FAILED.value(), null,
					ExecutionOption.STEPRESULT, fnStep));
			logerOBJ.customlogg(userlogger, "Step Failed", null, "", "error");
			logerOBJ.customlogg(userlogger,
					"----------------------------------------------------------------------------------------", null,
					"tc done", "info");
			return true;
		}
	}

	private boolean ifExecution(ArrayList<Step> tcifSteps, JsonNode testData, int itr, int itrfn, String sessionID,
			int total, Setupexec execSetupexec) {

		try {
			ArrayList<Step> ifstep = new ArrayList<Step>();
			List<JsonNode> parameters = new ArrayList<JsonNode>();
			JSONArray condition_check = new JSONArray();
			boolean flag = false;
			boolean ifFlag = false;
			boolean iscondtion = false;
			int isobjecttype = 0;
			int no_condition = 0;
			int for_start = 0;
			int count_for_steps = 0;
			int count = 0;
			boolean loopFlag = false;
			ArrayList<Step> loopstep = new ArrayList<Step>();

			int iF = -1, eLse = -1, endIf = -1;
			for (int i = 0; i < tcifSteps.size(); i++) {
				if (tcifSteps.get(i).isiF()) {
					if (iF == -1)
						iF = i;

				}
				if (tcifSteps.get(i).iseLse()) {

					eLse = i;
				}
				if (tcifSteps.get(i).isEndif()) {

					endIf = i;
				}

				// added for checking if condition children is object

				if (tcifSteps.get(i).getChildren().size() != 0) {
					for (int k = 0; k < tcifSteps.get(i).getChildren().size(); k++) {

						if (tcifSteps.get(i).getChildren().get(k).getObjectType() != null) {
							count++;
							if (tcifSteps.get(i).getChildren().get(k).getObjectType().equals("object"))
								isobjecttype++;
						}
					}
				}

			}

			ArrayList<Step> send = new ArrayList<Step>();
			send.add(tcifSteps.get(iF));
			send.add(tcifSteps.get(eLse));
			if (GlobalDetail.referencestep != null) {
				JSONObject ref = new JSONObject();
				ref.put("stepno", tcifSteps.get(iF).getSeq());
				ref.put("stepResult", "current");
				if (tcifSteps.get(iF).getSeq2() != null)
					ref.put("seq", tcifSteps.get(iF).getSeq2());
				else
					ref.put("seq", tcifSteps.get(iF).getSeq());
				GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

			}
			if (count == 0) {
				if (!executionDetail.getTcType().toLowerCase().equals("mobile")) {
					iscondtion = InitializeDependence.ifelseeval(tcifSteps.get(iF), testData, execSetupexec, sessionID);
				}

				else {
					iscondtion = InitializeDependence.ifelseeval(tcifSteps.get(iF), testData, execSetupexec,
							executionDetail.getDevicesID());
				}

			} else {

				iscondtion = InitializeDependence.ifelseeval(tcifSteps, testData, execSetupexec, sessionID, iF,
						executor, header, itr);
			}
//			InitializeDependence.current_step = InitializeDependence.current_step+1;
//			tcifSteps.get(iF).setSeq(InitializeDependence.current_step);
			if (GlobalDetail.referencestep != null) {
				JSONObject ref = new JSONObject();
				ref.put("stepno", tcifSteps.get(iF).getSeq());
				ref.put("stepResult", "passed");
				if (tcifSteps.get(iF).getSeq2() != null)
					ref.put("seq", tcifSteps.get(iF).getSeq2());
				else
					ref.put("seq", tcifSteps.get(iF).getSeq());
				GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

			}
			if (iscondtion) {
				for (int i = iF + 1; i < eLse; i++) {

					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(i).getSeq());
						ref.put("stepResult", "current");
						if (tcifSteps.get(i).getSeq2() != null)
							ref.put("seq", tcifSteps.get(i).getSeq2());
						else
							ref.put("seq", tcifSteps.get(i).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
					if (tcifSteps.get(i).isiF()) {
						ifstep = new ArrayList<Step>();
						ifstep.add(tcifSteps.get(i));
						ifFlag = true;
						continue;
					}
					if (tcifSteps.get(i).iseLse()) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}
					if (tcifSteps.get(i).isEndif()) {
						ifFlag = false;
						ifstep.add(tcifSteps.get(i));
						if (ifExecution(ifstep, testData, itr, itrfn, sessionId, tcifSteps.size(), execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							logerOBJ.customlogg(userlogger, "Testcase Failed :", null,
									tcifSteps.get(i).getObject_name(), "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;

						}
					}
					if (ifFlag) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}

					if (tcifSteps.get(i).isStartFor()) {
						loopstep.add(tcifSteps.get(i));
						loopFlag = true;
						for_start = i;
						count_for_steps++;
						continue;
					}

					if (tcifSteps.get(i).isEndFor()) {

						loopFlag = false;
						count_for_steps++;
						loopstep.add(tcifSteps.get(i));

						if (forExecution(loopstep, testData, itr, itrfn, sessionID, count_for_steps, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							logerOBJ.customlogg(userlogger, "Testcase Failed :", null,
									tcifSteps.get(i).getObject_name(), "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						}

						else {
							if (!GlobalDetail.ReferencePlayback) {
								InitializeDependence.seq++;
								logger.info("Loop got executed");
							}
							continue;

						}
					}

					if (loopFlag) {
						loopstep.add(tcifSteps.get(i));
						count_for_steps++;
						continue;
					}

					if (tcifSteps.get(i).getSkip()) {
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (!GlobalDetail.ReferencePlayback) {
							logger.info("Step Skiped:{}", execSetupexec.getTestcaseID());
							logerOBJ.customlogg(userlogger, "Step Sequence Skiped :", null, "", "info");
							String[] td = null;
							if (!tcifSteps.get(i).getParameters().isEmpty()) {
								td = new String[tcifSteps.get(i).getParameters().size()];
								for (int j = 0; j < tcifSteps.get(i).getParameters().size(); j++) {
									td[j] = testData.get(tcifSteps.get(i).getParameters().get(j).get("name").asText())
											.asText();
								}
							}
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, td,
									UtilEnum.SKIP.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						}
						flag = false;
						continue;
					}
					if (tcifSteps.get(i).getFunctionId() != null) {
						fnStep = tcifSteps.get(i);
						InitializeDependence.seq = tcifSteps.get(i).getSeq() + 1;
						if (initFNexecution(tcifSteps.get(i), testData, sessionId, itrfn, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (GlobalDetail.referencestep != null) {
								JSONObject ref = new JSONObject();
								ref.put("stepno", tcifSteps.get(i).getSeq());
								ref.put("stepResult", "passed");
								if (tcifSteps.get(i).getSeq2() != null)
									ref.put("seq", tcifSteps.get(i).getSeq2());
								else
									ref.put("seq", tcifSteps.get(i).getSeq());
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

							}
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;
						}
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase().equals(ActionClass.switchtoweb.value())) {
						hybridExecution = true;
						String brower = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText().toLowerCase().trim();
						execSetupexec.setBrowserName(brower);
						executor = new WebExecutorImpl();
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (!driversession.containsKey(brower)) {
							driversession.put(brower, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(brower);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(brower));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtomobile.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (DeviceManager.devices.get(deivceId) != null) {
							execSetupexec.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(deivceId)) {
									execSetupexec.setMbType("ios");
									break;
								}
						}
						executor = new MobileExecutorImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtodesktop.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						executor = new DesktopExecutionImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (executor.stepExecution(execSetupexec, tcifSteps.get(i), testData, itr, header, sessionID)) {
						flag = false;
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							if ((total - 1) == tcifSteps.get(i).getSeq()) {
								JSONObject pass = new JSONObject();
								pass.put("execution", "passed");
								GlobalDetail.referencestep.getAsyncRemote().sendText(pass.toString());
							}
							InitializeDependence.seq++;

						}
						continue;
					} else {
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "failed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							JSONObject fail = new JSONObject().put("execution", "failed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(fail.toString());
						}
						flag = true;
						break;
					}

				}
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(eLse).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(eLse).getSeq2() != null)
						ref.put("seq", tcifSteps.get(eLse).getSeq2());
					else
						ref.put("seq", tcifSteps.get(eLse).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
					for (int i = eLse + 1; i < endIf; i++) {
						ref.clear();
						ref.put("stepno", tcifSteps.get(i).getSeq());
						ref.put("stepResult", "false condition");
						if (tcifSteps.get(i).getSeq2() != null)
							ref.put("seq", tcifSteps.get(i).getSeq2());
						else
							ref.put("seq", tcifSteps.get(i).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						if (tcifSteps.get(i).getFunctionId() != null) {
							InitializeDependence.seq = tcifSteps.get(i).getSeq() + 1;
							Testcase fnObject = InitializeDependence.serverCall.getFNstep(execSetupexec,
									tcifSteps.get(i), testData, header);
							for (Step step : fnObject.getSteps()) {
								ref.clear();
								ref.put("stepno", InitializeDependence.seq);
								ref.put("stepResult", "false condition");
								ref.put("seq", Float.parseFloat(tcifSteps.get(i).getSeq() + "." + step.getSeq()));
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
								InitializeDependence.seq++;
							}
						}
					}
				}
				if (!flag) {
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(endIf).getSeq());
						ref.put("stepResult", "passed");
						if (tcifSteps.get(endIf).getSeq2() != null)
							ref.put("seq", tcifSteps.get(endIf).getSeq2());
						else
							ref.put("seq", tcifSteps.get(endIf).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
				} else {
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(endIf).getSeq());
						ref.put("stepResult", "failed");
						if (tcifSteps.get(endIf).getSeq2() != null)
							ref.put("seq", tcifSteps.get(endIf).getSeq2());
						else
							ref.put("seq", tcifSteps.get(endIf).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
				}
			} else {
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(eLse).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(eLse).getSeq2() != null)
						ref.put("seq", tcifSteps.get(eLse).getSeq2());
					else
						ref.put("seq", tcifSteps.get(eLse).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
					for (int i = iF + 1; i < eLse; i++) {
						ref.clear();
						ref.put("stepno", tcifSteps.get(i).getSeq());
						ref.put("stepResult", "false condition");
						if (tcifSteps.get(i).getSeq2() != null)
							ref.put("seq", tcifSteps.get(i).getSeq2());
						else
							ref.put("seq", tcifSteps.get(i).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
						if (tcifSteps.get(i).getFunctionId() != null) {
							InitializeDependence.seq = tcifSteps.get(i).getSeq() + 1;
							Testcase fnObject = InitializeDependence.serverCall.getFNstep(execSetupexec,
									tcifSteps.get(i), testData, header);
							for (Step step : fnObject.getSteps()) {
								ref.clear();
								ref.put("stepno", InitializeDependence.seq);
								ref.put("stepResult", "false condition");
								ref.put("seq", Float.parseFloat(tcifSteps.get(i).getSeq() + "." + step.getSeq()));
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
								InitializeDependence.seq++;
							}
						}

					}
				}
				for (int i = eLse + 1; i < endIf; i++) {

					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(i).getSeq());
						ref.put("stepResult", "current");
						if (tcifSteps.get(i).getSeq2() != null)
							ref.put("seq", tcifSteps.get(i).getSeq2());
						else
							ref.put("seq", tcifSteps.get(i).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
					if (tcifSteps.get(i).isiF()) {
						ifstep = new ArrayList<Step>();
						ifstep.add(tcifSteps.get(i));
						ifFlag = true;
						continue;
					}
					if (tcifSteps.get(i).iseLse()) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}
					if (tcifSteps.get(i).isEndif()) {
						ifFlag = false;
						ifstep.add(tcifSteps.get(i));
						if (ifExecution(ifstep, testData, itr, itrfn, sessionId, tcifSteps.size(), execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;

						}
					}
					if (ifFlag) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}

					if (tcifSteps.get(i).isStartFor()) {
						loopstep.add(tcifSteps.get(i));
						loopFlag = true;
						for_start = i;
						count_for_steps++;
						continue;
					}

					if (tcifSteps.get(i).isEndFor()) {

						loopFlag = false;
						count_for_steps++;
						loopstep.add(tcifSteps.get(i));

						if (forExecution(loopstep, testData, itr, itrfn, sessionID, count_for_steps, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							logerOBJ.customlogg(userlogger, "Testcase Failed :", null,
									tcifSteps.get(i).getObject_name(), "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						}

						else {
							if (!GlobalDetail.ReferencePlayback) {
								InitializeDependence.seq++;
								logger.info("Loop got executed");
							}
							continue;

						}
					}

					if (loopFlag) {
						loopstep.add(tcifSteps.get(i));
						count_for_steps++;
						continue;
					}
					if (tcifSteps.get(i).getSkip()) {
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (!GlobalDetail.ReferencePlayback) {
							logger.info("Step Skiped:{}", execSetupexec.getTestcaseID());
							String[] td = null;
							if (!tcifSteps.get(i).getParameters().isEmpty()) {
								td = new String[tcifSteps.get(i).getParameters().size()];
								for (int j = 0; j < tcifSteps.get(i).getParameters().size(); j++) {
									td[j] = testData.get(tcifSteps.get(i).getParameters().get(j).get("name").asText())
											.asText();
								}
							}
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, td,
									UtilEnum.SKIP.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						}
						flag = false;
						continue;
					}
					if (tcifSteps.get(i).getFunctionId() != null) {
						fnStep = tcifSteps.get(i);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						InitializeDependence.seq = tcifSteps.get(i).getSeq() + 1;
						if (initFNexecution(tcifSteps.get(i), testData, sessionId, itrfn, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;
						}
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase().equals(ActionClass.switchtoweb.value())) {
						hybridExecution = true;
						String brower = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText().toLowerCase().trim();
						execSetupexec.setBrowserName(brower);
						executor = new WebExecutorImpl();

						if (!driversession.containsKey(brower)) {
							driversession.put(brower, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						sessionId = driversession.get(brower);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(brower));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtomobile.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						if (DeviceManager.devices.get(deivceId) != null) {
							execSetupexec.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(deivceId)) {
									execSetupexec.setMbType("ios");
									break;
								}
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						executor = new MobileExecutorImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtodesktop.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						executor = new DesktopExecutionImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (executor.stepExecution(execSetupexec, tcifSteps.get(i), testData, itr, header, sessionID)) {
						flag = false;
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							if ((total - 1) == tcifSteps.get(i).getSeq()) {
								JSONObject pass = new JSONObject();
								pass.put("execution", "passed");
								GlobalDetail.referencestep.getAsyncRemote().sendText(pass.toString());
							}
							InitializeDependence.seq++;

						}
						continue;
					} else {
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "failed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							JSONObject fail = new JSONObject().put("execution", "failed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(fail.toString());
						}
						flag = true;
						break;
					}
				}
				if (!flag) {
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(endIf).getSeq());
						ref.put("stepResult", "passed");
						if (tcifSteps.get(endIf).getSeq2() != null)
							ref.put("seq", tcifSteps.get(endIf).getSeq2());
						else
							ref.put("seq", tcifSteps.get(endIf).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
				} else {
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(endIf).getSeq());
						ref.put("stepResult", "failed");
						if (tcifSteps.get(endIf).getSeq2() != null)
							ref.put("seq", tcifSteps.get(endIf).getSeq2());
						else
							ref.put("seq", tcifSteps.get(endIf).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
				}
			}
			// Service.saveLogs(executionDetail,header);
			return flag;
		} catch (

		Exception e) {
			return true;
		}

	}

	private boolean forExecution(ArrayList<Step> tcifSteps, JsonNode testData, int itr, int itrfn, String sessionID,
			int total, Setupexec execSetupexec) {

		try {
			ArrayList<Step> ifstep = new ArrayList<Step>();
			boolean flag = false;
			boolean ifFlag = false;
			boolean iscondtion = false;
			int loop_no = 0;
			int iF = -1, eLse = -1, endIf = -1;
			for (int i = 0; i < tcifSteps.size(); i++) {
				if (tcifSteps.get(i).isStartFor()) {
					if (iF == -1) {
						iF = i;
						loop_no = tcifSteps.get(i).getLoopDuration();

					}

				}
				if (tcifSteps.get(i).isEndFor()) {

					endIf = i;
				}
			}
			if (GlobalDetail.referencestep != null) {
				JSONObject ref = new JSONObject();
				ref.put("stepno", tcifSteps.get(iF).getSeq());
				ref.put("stepResult", "current");
				if (tcifSteps.get(iF).getSeq2() != null)
					ref.put("seq", tcifSteps.get(iF).getSeq2());
				else
					ref.put("seq", tcifSteps.get(iF).getSeq());
				GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

			}
			for (int duration = 0; duration < loop_no; duration++) {

				for (int i = iF + 1; i < endIf; i++) {
					if (GlobalDetail.referencestep != null) {
						JSONObject ref = new JSONObject();
						ref.put("stepno", tcifSteps.get(i).getSeq());
						ref.put("stepResult", "current");
						if (tcifSteps.get(i).getSeq2() != null)
							ref.put("seq", tcifSteps.get(i).getSeq2());
						else
							ref.put("seq", tcifSteps.get(i).getSeq());
						GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

					}
//					tcifSteps.get(i).setSeq(i);
					if (tcifSteps.get(i).isiF()) {
						ifstep = new ArrayList<Step>();
						ifstep.add(tcifSteps.get(i));
						ifFlag = true;
						continue;
					}
					if (tcifSteps.get(i).iseLse()) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}
					if (tcifSteps.get(i).isEndif()) {
						ifFlag = false;
						ifstep.add(tcifSteps.get(i));
						if (ifExecution(ifstep, testData, itr, itrfn, sessionId, tcifSteps.size(), execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							logerOBJ.customlogg(userlogger, "Testcase Failed :", null,
									tcifSteps.get(i).getObject_name(), "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;

						}
					}
					if (ifFlag) {
						ifstep.add(tcifSteps.get(i));
						continue;
					}
					if (tcifSteps.get(i).getSkip()) {
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (!GlobalDetail.ReferencePlayback) {
							logger.info("Step Skiped:{}", execSetupexec.getTestcaseID());
							logerOBJ.customlogg(userlogger, "Step Sequence Skiped :", null, "", "info");
							String[] td = null;
							if (!tcifSteps.get(i).getParameters().isEmpty()) {
								td = new String[tcifSteps.get(i).getParameters().size()];
								for (int j = 0; j < tcifSteps.get(i).getParameters().size(); j++) {
									td[j] = testData.get(tcifSteps.get(i).getParameters().get(j).get("name").asText())
											.asText();
								}
							}
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(execSetupexec, header, i, td,
									UtilEnum.SKIP.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						}
						flag = false;
						continue;
					}
					if (tcifSteps.get(i).getFunctionId() != null) {
						fnStep = tcifSteps.get(i);
						InitializeDependence.seq = tcifSteps.get(iF).getSeq() + 1;
						if (initFNexecution(tcifSteps.get(i), testData, sessionId, itrfn, execSetupexec)) {
							logger.info("Step Failed by Object name:{}", tcifSteps.get(i).getObject_name());
							tcifSteps.get(i).setDescription(tcifSteps.get(i).getObject_name());
							flag = true;
							break;
						} else {
							if (GlobalDetail.referencestep != null) {
								JSONObject ref = new JSONObject();
								ref.put("stepno", tcifSteps.get(i).getSeq());
								ref.put("stepResult", "passed");
								if (tcifSteps.get(i).getSeq2() != null)
									ref.put("seq", tcifSteps.get(i).getSeq2());
								else
									ref.put("seq", tcifSteps.get(i).getSeq());
								GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

							}
							if (!GlobalDetail.ReferencePlayback)
								InitializeDependence.seq++;
							continue;
						}
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase().equals(ActionClass.switchtoweb.value())) {
						hybridExecution = true;
						String brower = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText().toLowerCase().trim();
						execSetupexec.setBrowserName(brower);
						executor = new WebExecutorImpl();
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						if (!driversession.containsKey(brower)) {
							driversession.put(brower, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(brower);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(brower));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;
					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtomobile.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						if (DeviceManager.devices.get(deivceId) != null) {
							execSetupexec.setMbType("android");
						} else {
							for (Entry<String, JSONObject> map : ideviceMaganger.devices.entrySet())
								if (map.getValue().getString("id").equals(deivceId)) {
									execSetupexec.setMbType("ios");
									break;
								}
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						executor = new MobileExecutorImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}
					if (tcifSteps.get(i).getAction().getName().toLowerCase()
							.equals(ActionClass.switchtodesktop.value())) {
						hybridExecution = true;
						String deivceId = testData.get(tcifSteps.get(i).getParameters().get(0).get("name").asText())
								.asText();
						executor = new DesktopExecutionImpl();

						if (!driversession.containsKey(deivceId)) {
							driversession.put(deivceId, UUID.randomUUID().toString());
						}
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

						}
						sessionId = driversession.get(deivceId);
						InitializeDependence.closePreLoader(sessionId);
						ExecutionService.a().set(driversession.get(deivceId));
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, i,
										InitializeDependence.getTdArray(tcifSteps.get(i), testData, executionDetail,
												sessionId),
										UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcifSteps.get(i)));
						logerOBJ.customlogg(userlogger, "Step autoExe Passed", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						continue;

					}

					if (executor.stepExecution(execSetupexec, tcifSteps.get(i), testData, itr, header, sessionID)) {
						flag = false;
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "passed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							if ((total - 1) == tcifSteps.get(i).getSeq()) {
								JSONObject pass = new JSONObject();
								pass.put("execution", "passed");
								GlobalDetail.referencestep.getAsyncRemote().sendText(pass.toString());
							}
							InitializeDependence.seq++;

						}
						continue;
					} else {
						InitializeDependence.closePreLoader(sessionId);
						if (GlobalDetail.referencestep != null) {
							JSONObject ref = new JSONObject();
							ref.put("stepno", tcifSteps.get(i).getSeq());
							ref.put("stepResult", "failed");
							if (tcifSteps.get(i).getSeq2() != null)
								ref.put("seq", tcifSteps.get(i).getSeq2());
							else
								ref.put("seq", tcifSteps.get(i).getSeq());
							GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());
							JSONObject fail = new JSONObject().put("execution", "failed");
							GlobalDetail.referencestep.getAsyncRemote().sendText(fail.toString());
						}

						if (!tcifSteps.get(i).isContinueOnFailure()) {
							flag = true;
							break;
						} else {
							continue;
						}
					}

				}
				if (flag)
					break;

			}
			if (!flag) {
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(iF).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(iF).getSeq2() != null)
						ref.put("seq", tcifSteps.get(iF).getSeq2());
					else
						ref.put("seq", tcifSteps.get(iF).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

				}
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(iF).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(endIf).getSeq2() != null)
						ref.put("seq", tcifSteps.get(endIf).getSeq2());
					else
						ref.put("seq", tcifSteps.get(endIf).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

				}
			} else {
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(iF).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(iF).getSeq2() != null)
						ref.put("seq", tcifSteps.get(iF).getSeq2());
					else
						ref.put("seq", tcifSteps.get(iF).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

				}
				if (GlobalDetail.referencestep != null) {
					JSONObject ref = new JSONObject();
					ref.put("stepno", tcifSteps.get(iF).getSeq());
					ref.put("stepResult", "passed");
					if (tcifSteps.get(endIf).getSeq2() != null)
						ref.put("seq", tcifSteps.get(endIf).getSeq2());
					else
						ref.put("seq", tcifSteps.get(endIf).getSeq());
					GlobalDetail.referencestep.getAsyncRemote().sendText(ref.toString());

				}
			}
			return flag;
		} catch (

		Exception e) {
			return true;
		}
	}

}
