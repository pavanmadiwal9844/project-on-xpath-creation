package com.simplifyqa.execution.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.Utility.ActionClass;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.execution.ExecutionOption;
import com.simplifyqa.execution.ExecutionService;
import com.simplifyqa.execution.ExecutionTask;
import com.simplifyqa.execution.Interface.Executor;
import com.simplifyqa.method.MainframeMethod;

public class Executionimpl implements Executor {

	final static Logger logger = LoggerFactory.getLogger(Executionimpl.class);
	private String brName = "mainframe";

	private MainframeMethod action = new MainframeMethod();

//	@Override
//	public Testcase exeIfEvaluation(Testcase tcfnIFeval) {
//		try {
//			ScriptEngineManager jsM = new ScriptEngineManager();
//			ScriptEngine scriptEngine = jsM.getEngineByName("JavaScript");
//			Integer iF = -1, eLse = -1, endIf = -1;
//			String ifstring = "";
//			Boolean condition = false;
//
//			for (int k = 0; k < tcfnIFeval.getTestdata().size(); k++) {
//				for (int i = 0; i < tcfnIFeval.getSteps().size(); i++) {
//					if (tcfnIFeval.getSteps().get(i).isiF()) {
//						iF = i;
//						ifstring = tcfnIFeval.getSteps().get(i).getIfstring();
//						for (int j = 0; j < tcfnIFeval.getSteps().get(i).getChildren().size(); j++) {
//							ifstring = ifstring.replace(
//									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter1().get(0)
//											.get(UtilEnum.NAME.value()).asText(),
//									tcfnIFeval
//											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
//													.getParameter1().get(0).get(UtilEnum.NAME.value()).asText())
//											.toString());
//							ifstring = ifstring.replace(
//									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter2().get(0)
//											.get(UtilEnum.NAME.value()).asText(),
//									tcfnIFeval
//											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
//													.getParameter2().get(0).get(UtilEnum.NAME.value()).asText())
//											.toString());
//
//						}
//						ifstring = ifstring.replace(UtilEnum.IF.value(), "").replace(" ", "");
//						condition = (Boolean) scriptEngine.eval(ifstring);
//					} else if (tcfnIFeval.getSteps().get(i).iseLse()) {
//						eLse = i;
//					} else if (tcfnIFeval.getSteps().get(i).isEndif()) {
//						endIf = i;
//					}
//
//				}
//			}
//			List<Step> tempStep = new ArrayList<Step>();
//			if (iF != -1 && eLse != -1 && endIf != -1) {
//				if (condition) {
//					for (int i = eLse; i <= endIf; i++) {
//						tempStep.add(tcfnIFeval.getSteps().get(i));
//					}
//					tempStep.add(tcfnIFeval.getSteps().get(iF));
//				} else {
//					for (int i = iF; i <= eLse; i++) {
//						tempStep.add(tcfnIFeval.getSteps().get(i));
//					}
//					tempStep.add(tcfnIFeval.getSteps().get(endIf));
//				}
//				for (int i = 0; i < tempStep.size(); i++) {
//					tcfnIFeval.getSteps().remove(tempStep.get(i));
//				}
//			}
//			return tcfnIFeval;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//
//	}

	@Override
	public Boolean stepExecution(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String sessionIDs) {
		JSONObject res = null;
		String screenshot = null;
		MainframeMethod.semCon = new Semaphore(0);
		MainframeMethod.semProd = new Semaphore(1);
		InitializeDependence.mfAction.setDetail(executionDetail, header, itr);
		String[] td = null;
		if (executionDetail != null && executionDetail.getBrowserName() != null) {
			String brName = executionDetail.getBrowserName();
			ExecutionService.a().set(brName.toLowerCase().trim());
		}

		try {
			switch (tcfnStep.getAction().getName().toLowerCase()) {
			case "launchapp":
				if ((res = InitializeDependence.mfAction.launchApp()) != null) {
					if (executionDetail != null)
//						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
//								UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return true;
				} else {
					if (executionDetail != null)
//						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
//								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
				}
			case "click":
			case "doubleclick":
			case "deletetext":
				if ((res = (JSONObject) InitializeDependence.mfAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, Boolean.class)
						.invoke(InitializeDependence.mfAction, tcfnStep.getObject(),
								tcfnStep.isCaptureScreenshot())) != null) {
					JsonNode result = new ObjectMapper().readTree(res.toString());
					if (executionDetail != null) {
//						td = new String[tcfnStep.getParameters().size()];
//						td[0] = result.get("testdata").asText();
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, res.getString("play_back"),
										result.get(UtilEnum.SCREENSHOT.value()).asText(), ExecutionOption.STEPRESULT,
										tcfnStep));
					}
					if (result.get("action").asText().toLowerCase().equals("readtextandstore")) {
						res.put("paramname", tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText());
						res.put("paramvalue", result.get("testdata").asText());
						InitializeDependence.serverCall.postruntimeParameter(executionDetail, res, header);
						return true;
					} else if (result.get("play_back").asText().toLowerCase()
							.equals(UtilEnum.PASSED.value().toLowerCase()))
						return true;
					else
						return false;
				} else {
					if (executionDetail != null) {
						td = new String[tcfnStep.getParameters().size()];
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
					}
				}
			case "readtextandstore":
				td = new String[tcfnStep.getParameters().size()];
				if (tcfnStep.getFunctionCode() != null) {
					if (!GlobalDetail.runTime.isEmpty()
							&& GlobalDetail.runTime.containsKey(brName.toLowerCase().trim())) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							try {
								td[i] = GlobalDetail.runTime.get(brName.toLowerCase().trim())
										.get(tcData.get(tcfnStep.getFunctionCode())
												.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
										.asText();
							} catch (Exception e) {
								td[i] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				} else if (!GlobalDetail.runTime.isEmpty()
						&& GlobalDetail.runTime.containsKey(brName.toLowerCase().trim())) {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						try {
							td[i] = GlobalDetail.runTime.get(brName.toLowerCase().trim())
									.get(tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
									.asText();
						} catch (Exception e) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				} else {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				}
				if (executionDetail != null) {
					JSONObject result = InitializeDependence.mfAction.readtextandstore(tcfnStep.getObject(), td[0]);
					if (result.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
						ExecutionService.a().set(brName.toLowerCase().trim());
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						Thread.sleep(500);
						return true;
					} else {
						screenshot = InitializeDependence.webDriver.get(brName.toLowerCase()).takeScreenshot();
						ExecutionService.a().set(brName.toLowerCase().trim());
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						Thread.sleep(500);
						return false;
					}
				} else {
					JSONObject result = InitializeDependence.mfAction.readtextandstore(tcfnStep.getObject(), td[0]);
					if (result.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
						return true;
					} else {
						return false;
					}
				}

			case "typetext":
			case "keyboardaction":
			case "validatepartialtext":
			case "validatetext":
			case "waitfortext":
			case "findonscreen":
			case "launchapplication":
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((res = (JSONObject) InitializeDependence.mfAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, String.class,
								Boolean.class)
						.invoke(InitializeDependence.mfAction, tcfnStep.getObject(), td[0],
								tcfnStep.isCaptureScreenshot())) != null) {
					JsonNode result = new ObjectMapper().readTree(res.toString());
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, result.get("play_back").asText(),
										result.get(UtilEnum.SCREENSHOT.value()).asText(), ExecutionOption.STEPRESULT,
										tcfnStep));

					if (result.get("play_back").asText().toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()))
						return true;
					else
						return false;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			case "getfromruntime":
				td = new String[tcfnStep.getParameters().size()];
				String runTimepar;
				Boolean flag = false;
				if (executionDetail != null) {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						if ((runTimepar = InitializeDependence.mfAction.getruntimevariable(td[i])) != null) {
							if (GlobalDetail.runTime.containsKey(brName.toLowerCase().trim())) {
								((ObjectNode) GlobalDetail.runTime.get(brName.toLowerCase().trim())).put(
										tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText(),
										runTimepar);
							} else {
								GlobalDetail.runTime.put(brName.toLowerCase().trim(),
										new ObjectMapper().readTree(new JSONObject()
												.put(tcData.get(tcfnStep.getParameters().get(i).get("name").asText())
														.asText(), runTimepar)
												.toString()));
							}
						} else {
							flag = true;
							break;
						}
					}
					if (flag) {
						ExecutionService.a().set(brName.toLowerCase().trim());
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						Thread.sleep(500);
						return false;
					} else {
						ExecutionService.a().set(brName.toLowerCase().trim());
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						Thread.sleep(500);
						return true;
					}
				} else {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						if (tcfnStep.getFunctionCode() != null) {
							for (int i1 = 0; i1 < tcfnStep.getParameters().size(); i1++) {
								td[i1] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
						} else
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						if ((runTimepar = InitializeDependence.mfAction.getruntimevariable(td[i])) != null) {
							if (tcfnStep.getFunctionCode() != null) {
								if (GlobalDetail.runTime.containsKey(brName.toLowerCase().trim())) {
									((ObjectNode) GlobalDetail.runTime.get(brName.toLowerCase().trim())).put(
											tcData.get(tcfnStep.getFunctionCode())
													.get(tcfnStep.getParameters().get(i).get("name").asText()).asText(),
											runTimepar);
								} else {
									GlobalDetail.runTime
											.put(brName.toLowerCase().trim(),
													new ObjectMapper().readTree(new JSONObject()
															.put(tcData.get(tcfnStep.getFunctionCode())
																	.get(tcfnStep.getParameters().get(i).get("name")
																			.asText())
																	.asText(), runTimepar)
															.toString()));
								}
							} else {
								if (GlobalDetail.runTime.containsKey(brName.toLowerCase().trim())) {
									((ObjectNode) GlobalDetail.runTime.get(brName.toLowerCase().trim()))
											.put(tcfnStep.getParameters().get(i).get("name").asText(), runTimepar);
								} else {
									GlobalDetail.runTime.put(brName.toLowerCase().trim(),
											new ObjectMapper().readTree(new JSONObject()
													.put(tcfnStep.getParameters().get(i).get("name").asText(),
															runTimepar)
													.toString()));
								}
							}
						} else {
							flag = true;
							break;
						}
					}
					if (flag) {
						return false;
					} else {
						return true;
					}
				}

			case "wait":
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((Boolean) InitializeDependence.mfAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Integer.class)
						.invoke(InitializeDependence.mfAction, Integer.parseInt(td[0]))) {
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
										tcData.get(tcfnStep.getParameters().get(0).get("name").asText()).asText(),
										ExecutionOption.STEPRESULT, tcfnStep));
					return true;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			default:
				if (tcfnStep.getParameters().isEmpty())
					if (action.CallMethod(tcfnStep.getAction().getName(), null, executionDetail, header)) {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return true;
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
					}
				else {
					String[] ParaName = new String[tcfnStep.getParameters().size()];
					String[] value = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						ParaName[i] = "";
						ParaName[i] = tcfnStep.getParameters().get(i).get("name").asText();
					}
					for (int i = 0; i < ParaName.length; i++)
						value[i] = tcData.get(ParaName[i]).asText();
					if (action.CallMethod(tcfnStep.getAction().getName(), value, executionDetail, header)) {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return true;
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean hybridExecution) {
		if (GlobalDetail.mainFrSession != null) {
			GlobalDetail.mainFrSession.getAsyncRemote()
					.sendText(new JSONObject().put("action", "closeapplication").toString());
			try {
				Thread.sleep(2500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		InitializeDependence.mfAction.stopplayback();
		if (executionDetail != null && !hybridExecution) {
			ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
					ExecutionOption.FINISHEXECUITON, null));

		}
	}

	@Override
	public Testcase exeIfEvaluation(Testcase tcfnIFeval) {
		// TODO Auto-generated method stub
		return null;
	}

}
