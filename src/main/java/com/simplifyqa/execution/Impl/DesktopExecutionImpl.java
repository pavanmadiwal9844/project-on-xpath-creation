package com.simplifyqa.execution.Impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rits.cloning.Cloner;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.Utility.ActionClass;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.execution.ExecutionOption;
import com.simplifyqa.execution.ExecutionService;
import com.simplifyqa.execution.ExecutionTask;
import com.simplifyqa.execution.Interface.Executor;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.DesktopMethod;
import com.simplifyqa.method.GeneralMethod;


public class DesktopExecutionImpl implements Executor {
	final static Logger logger = LoggerFactory.getLogger(DesktopExecutionImpl.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(DesktopExecutionImpl.class);
	LoggerClass logerOBJ = new LoggerClass();

	@Override
	public Testcase exeIfEvaluation(Testcase tcfnIFeval) {
		try {
			ScriptEngineManager jsM = new ScriptEngineManager();
			ScriptEngine scriptEngine = jsM.getEngineByName("JavaScript");
			Integer iF = -1, eLse = -1, endIf = -1;
			String ifstring = "";
			Boolean condition = false;
			for (int k = 0; k < tcfnIFeval.getTestdata().size(); k++) {
				for (int i = 0; i < tcfnIFeval.getSteps().size(); i++) {
					if (tcfnIFeval.getSteps().get(i).isiF()) {
						iF = i;
						ifstring = tcfnIFeval.getSteps().get(i).getIfstring();
						for (int j = 0; j < tcfnIFeval.getSteps().get(i).getChildren().size(); j++) {
							ifstring = ifstring.replace(
									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter1().get(0)
											.get(UtilEnum.NAME.value()).asText(),
									tcfnIFeval
											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
													.getParameter1().get(0).get(UtilEnum.NAME.value()).asText())
											.toString());
							ifstring = ifstring.replace(
									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter2().get(0)
											.get(UtilEnum.NAME.value()).asText(),
									tcfnIFeval
											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
													.getParameter2().get(0).get(UtilEnum.NAME.value()).asText())
											.toString());

						}
						ifstring = ifstring.replace(UtilEnum.IF.value(), "").replace(" ", "");
						condition = (Boolean) scriptEngine.eval(ifstring);
					} else if (tcfnIFeval.getSteps().get(i).iseLse()) {
						eLse = i;
					} else if (tcfnIFeval.getSteps().get(i).isEndif()) {
						endIf = i;
					}

				}
			}
			List<Step> tempStep = new ArrayList<Step>();
			if (iF != -1 && eLse != -1 && endIf != -1) {
				if (condition) {
					for (int i = eLse; i <= endIf; i++) {
						tempStep.add(tcfnIFeval.getSteps().get(i));
					}
					tempStep.add(tcfnIFeval.getSteps().get(iF));
				} else {
					for (int i = iF; i <= eLse; i++) {
						tempStep.add(tcfnIFeval.getSteps().get(i));
					}
					tempStep.add(tcfnIFeval.getSteps().get(endIf));
				}
				for (int i = 0; i < tempStep.size(); i++) {
					tcfnIFeval.getSteps().remove(tempStep.get(i));
				}
			}
			return tcfnIFeval;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Boolean stepExecution(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String sessionID) {
		JSONObject res = null;
		InitializeDependence.dtAction.setDetail(executionDetail, header, itr);
		String[] td = null;
		DesktopMethod.semCon = new Semaphore(0);
		DesktopMethod.semProd = new Semaphore(1);
		if (executionDetail != null && executionDetail.getBrowserName() != null) {
			String brName = executionDetail.getBrowserName();
			ExecutionService.a().set(brName.toLowerCase().trim());
			if (InitializeDependence.generaldriver == null) {
				InitializeDependence.generaldriver = new GeneralMethod(executionDetail, header, tcfnStep.getSeq());
			}
		}
		try {
			if (InitializeDependence.actionlist.get(ActionClass.DKACTION.value())
					.contains(tcfnStep.getAction().getName().toLowerCase())) {
				switch (tcfnStep.getAction().getName().toLowerCase()) {
				case "launchapp":
					if ((res = InitializeDependence.dtAction.launchApplication()) != null) {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						{
							logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return true;
						}
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						{
							logerOBJ.customlogg(userlogger, "Step Failed", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return false;
						}
					}

				case "click":
				case "mouseclick":
				case "expand":
				case "toggle":
				case "check":
				case "select":
				case "rightclick":
				case "doubleclick":
				case "readtextandstore":
				case "waitforelement":
					if ((res = (JSONObject) InitializeDependence.dtAction.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class,
									Boolean.class)
							.invoke(InitializeDependence.dtAction, tcfnStep.getObject(),
									tcfnStep.isCaptureScreenshot())) != null) {
						JsonNode result = new ObjectMapper().readTree(res.toString());
						if (executionDetail != null) {
//						td = new String[tcfnStep.getParameters().size()];
//						td[0] = result.get("testdata").asText();
							ExecutionService.a()
									.a(new ExecutionTask(executionDetail, header, itr, td, res.getString("play_back"),
											result.get(UtilEnum.SCREENSHOT.value()).asText(),
											ExecutionOption.STEPRESULT, tcfnStep));
						}
						if (result.get("action").asText().toLowerCase().equals("readtextandstore")) {
							res.put("paramname", tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText());
							res.put("paramvalue", result.get("testdata").asText());
							InitializeDependence.serverCall.postruntimeParameter(executionDetail, res, header);
							return true;
						} else if (result.get("play_back").asText().toLowerCase()
								.equals(UtilEnum.PASSED.value().toLowerCase())) {
							logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return true;
						} else
							return false;
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						{
							logerOBJ.customlogg(userlogger, "Step Failed", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return false;
						}
					}
				case "launchapplication":

					if (executionDetail != null) {
						td = new String[tcfnStep.getParameters().size()];
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						if (tcfnStep.getFunctionCode() != null) {
							td = new String[tcfnStep.getParameters().size()];
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
							}
						} else {
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
						}
					}
					if ((res = (JSONObject) InitializeDependence.dtAction.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, String.class,
									Boolean.class, String.class)
							.invoke(InitializeDependence.dtAction, tcfnStep.getObject(), td[0],
									tcfnStep.isCaptureScreenshot(), tcfnStep.getObject_name())) != null) {
						JsonNode result = new ObjectMapper().readTree(res.toString());
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									result.get("play_back").asText(), result.get(UtilEnum.SCREENSHOT.value()).asText(),
									ExecutionOption.STEPRESULT, tcfnStep));

						if (result.get("play_back").asText().toLowerCase()
								.equals(UtilEnum.PASSED.value().toLowerCase())) {

							logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return true;
						} else
							return false;
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						{
							logerOBJ.customlogg(userlogger, "Step Failed", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return false;
						}
					}

				case "entertext":
				case "keyboardentertext":
				case "deletetext":
				case "keyboardaction":
				case "waitfortext":
				case "validatepartialtext":
				case "validatetext":
				case "findonscreen":
					if (executionDetail != null) {
						td = new String[tcfnStep.getParameters().size()];
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						if (tcfnStep.getFunctionCode() != null) {
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
							}
						} else {
							td = new String[tcfnStep.getParameters().size()];
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
						}
					}
					if ((res = (JSONObject) InitializeDependence.dtAction.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, String.class,
									Boolean.class)
							.invoke(InitializeDependence.dtAction, tcfnStep.getObject(), td[0],
									tcfnStep.isCaptureScreenshot())) != null) {

						JsonNode result = new ObjectMapper().readTree(res.toString());
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									result.get("play_back").asText(), result.get(UtilEnum.SCREENSHOT.value()).asText(),
									ExecutionOption.STEPRESULT, tcfnStep));

						if (result.get("play_back").asText().toLowerCase()
								.equals(UtilEnum.PASSED.value().toLowerCase())) {
							logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return true;
						} else
							return false;
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
					}
//				case "getfromruntime":
//					res = InitializeDependence.dtAction
//							.getfromruntime(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText());
//					if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
//						((ObjectNode) tcData).put(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText(),
//								res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
//										.getJSONObject(0).getString("value"));
//						td[0] = tcData.get(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText())
//								.asText();
//						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
//								UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
//						Thread.sleep(500);
//						return true;
//					} else {
//						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
//								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
//						Thread.sleep(500);
//						return false;
//					}

				case "hold":
					if (executionDetail != null) {
						td = new String[tcfnStep.getParameters().size()];
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						if (tcfnStep.getFunctionCode() != null) {
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
							}
						} else {
							for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
								td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
						}
					}
					if ((Boolean) InitializeDependence.dtAction.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Integer.class)
							.invoke(InitializeDependence.dtAction, Integer.parseInt(td[0]))) {
						if (executionDetail != null)
							ExecutionService.a()
									.a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
											tcData.get(tcfnStep.getParameters().get(0).get("name").asText()).asText(),
											ExecutionOption.STEPRESULT, tcfnStep));
						{
							logerOBJ.customlogg(userlogger, "Step Passed", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							return true;
						}
					} else {
						if (executionDetail != null)
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
						return false;
					}
				default:
//				if (tcfnStep.getParameters().isEmpty())
//					return action.CallMethod(tcfnStep.getAction().getName(), null, executionDetail, header);
//				else {
//					String[] ParaName = new String[tcfnStep.getParameters().size()];
//					String[] value = new String[tcfnStep.getParameters().size()];
//					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
//						ParaName[i] = "";
//						ParaName[i] = tcfnStep.getParameters().get(i).get("name").asText();
//					}
//					for (int i = 0; i < ParaName.length; i++)
//						value[i] = tcData.get(ParaName[i]).asText();
//					return action.CallMethod(tcfnStep.getAction().getName(), value, executionDetail, header);
//				}
					return true;
				}
			} else if (InitializeDependence.actionlist.get(ActionClass.GNACTION.value())
					.contains(tcfnStep.getAction().getName().toLowerCase())) {
				String screenshot = null;
				td = new String[tcfnStep.getParameters().size()];
				td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionID);
				if (td.length > 0) {
					return findandExeuteMethod(executionDetail, tcfnStep, tcData, itr, header, td,sessionID);
				} else {
					if ((boolean) InitializeDependence.generaldriver.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
							.invoke(InitializeDependence.generaldriver)) {
						if (executionDetail != null) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = null;
							} else {
								screenshot = null;
							}
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return true;
					} else {
						if (executionDetail != null) {
							screenshot = null;
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");

						}
						return false;
					}
				}

			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return false;
		}
	}

	@Override
	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean hybridExecution) {
		InitializeDependence.dtAction.stopplayback();
		if (executionDetail != null && !hybridExecution) {
			ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
					ExecutionOption.FINISHEXECUITON, null));

		}
	}

	public Boolean findandExeuteMethod(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String[] td,String sessionID) {
		String screenshot = null;
		Class[] cArg = null;
		InitializeDependence.generaldriver.curStep = tcfnStep;
		InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
		InitializeDependence.generaldriver.curExecution = executionDetail;
		InitializeDependence.generaldriver.setExepectedValue(null);

		try {
			cArg = new Class[td.length];
			for (int i = 0; i < td.length; i++)
				cArg[i] = String.class;
			if ((boolean) InitializeDependence.generaldriver.getClass()
					.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
					.invoke(InitializeDependence.generaldriver, td)) {
				if (executionDetail != null) {
					if (tcfnStep.isCaptureScreenshot()) {
						screenshot = null;
					} else {
						screenshot = null;
					}
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionID);
					if (InitializeDependence.generaldriver.getExepectedValue() != null
							&& tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")) {
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					if (InitializeDependence.generaldriver.getExepectedValue() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						tcfnStep.getParameters().add(
								new ObjectMapper().readTree(new JSONObject().put("name", "ActualValue").toString()));
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
					}

					if (InitializeDependence.generaldriver.getJsonkey() != null
							&& InitializeDependence.generaldriver.getJsonvalue() != null) {

						for (int i = td.length; i > 2; i--) {
//							System.out.println(td[i]);
							tcfnStep.getParameters().remove(td.length - 1);
						}

						td[0] = new Cloner().deepClone(InitializeDependence.generaldriver.getJsonkey());

						td[1] = new Cloner().deepClone(InitializeDependence.generaldriver.getJsonvalue());
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "JsonKey");
						((ObjectNode) tcfnStep.getParameters().get(1)).put("name", "JsonValue");
					}
					ExecutionService.a().set(sessionID);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");
					InitializeDependence.generaldriver.exepectedValue = null;
					InitializeDependence.generaldriver.setJsonkey(null);
					InitializeDependence.generaldriver.setJsonvalue(null);
				}
				return true;
			} else {
				if (executionDetail != null) {
					screenshot = null;
					ExecutionService.a().set(sessionID);
					if (tcfnStep.getChildren().size() == 0) {
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
				}
				return false;
			}
		} catch (Exception e) {
			try {
				if ((boolean) InitializeDependence.generaldriver.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), String[].class)
						.invoke(InitializeDependence.generaldriver, td )) {
					if (executionDetail != null) {
						if (tcfnStep.isCaptureScreenshot()) {
							screenshot = null;
						} else {
							screenshot = null;
						}
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionID);
						if (InitializeDependence.generaldriver.getExepectedValue() != null) {
							td = Arrays.copyOf(td, td.length + 1);
							td[td.length - 1] = new Cloner()
									.deepClone(InitializeDependence.generaldriver.getExepectedValue());
							tcfnStep.getParameters().add(new ObjectMapper()
									.readTree(new JSONObject().put("name", "ActualValue").toString()));
							((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
						}
						ExecutionService.a().set(sessionID);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					return true;
				} else {
					if (executionDetail != null) {
						screenshot = null;
						ExecutionService.a().set(sessionID);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
					return false;
				}
			} catch (Exception ex) {
				try {
					InitializeDependence.generaldriver.curStep = tcfnStep;
					InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
					Map<String, String> tdMap = new HashMap<String, String>();
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						tdMap.put(tcfnStep.getParameters().get(i).get("name").asText(), td[i]);
					}
					InitializeDependence.generaldriver.setExepectedValue(null);
					if ((boolean) InitializeDependence.generaldriver.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Map.class)
							.invoke(InitializeDependence.generaldriver, tdMap)) {
						if (InitializeDependence.generaldriver.getRpParam() != null
								&& InitializeDependence.generaldriver.getRpParam().size() != 0) {
							tcfnStep.getParameters().clear();
							td = new String[InitializeDependence.generaldriver.getRpParam().size()];
							for (Entry<String, String> map : InitializeDependence.generaldriver.getRpParam()
									.entrySet()) {
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", map.getKey()).toString()));
								td[tcfnStep.getParameters().size() - 1] = map.getValue();
							}
						}
						if (executionDetail != null) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = null;
							} else {
								screenshot = null;
							}
							td = new String[tcfnStep.getParameters().size()];
							td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionID);
							if (InitializeDependence.generaldriver.getExepectedValue() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[td.length - 1] = new Cloner()
										.deepClone(InitializeDependence.generaldriver.getExepectedValue());
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", "ActualValue").toString()));
								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
							}
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							InitializeDependence.generaldriver.exepectedValue = null;
						}
						return true;
					} else {
						if (executionDetail != null) {
							screenshot = null;
							ExecutionService.a().set(sessionID);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return false;
					}
				} catch (Exception ev) {
					logger.info("unable to execute :{}", tcfnStep.getAction().getName());
					if (executionDetail != null) {
						screenshot = null;
						ExecutionService.a().set(sessionID);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
					return false;
				}
			}
		}
	}

}
