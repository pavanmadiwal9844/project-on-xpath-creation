package com.simplifyqa.execution.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.method.DesktopMethod;
import com.simplifyqa.method.SapMethod;
import com.simplifyqa.execution.ExecutionOption;
import com.simplifyqa.execution.ExecutionService;
import com.simplifyqa.execution.ExecutionTask;
import com.simplifyqa.execution.Interface.Executor;

public class SapExecutionImpl implements Executor {
	final static Logger logger = LoggerFactory.getLogger(DesktopExecutionImpl.class);

	@Override
	public Testcase exeIfEvaluation(Testcase tcfnIFeval) {
		try {
			ScriptEngineManager jsM = new ScriptEngineManager();
			ScriptEngine scriptEngine = jsM.getEngineByName("JavaScript");
			Integer iF = -1, eLse = -1, endIf = -1;
			String ifstring = "";
			Boolean condition = false;
			for (int k = 0; k < tcfnIFeval.getTestdata().size(); k++) {
				for (int i = 0; i < tcfnIFeval.getSteps().size(); i++) {
					if (tcfnIFeval.getSteps().get(i).isiF()) {
						iF = i;
						ifstring = tcfnIFeval.getSteps().get(i).getIfstring();
						for (int j = 0; j < tcfnIFeval.getSteps().get(i).getChildren().size(); j++) {
							ifstring = ifstring.replace(
									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter1().get(0)
											.get(UtilEnum.NAME.value()).asText(),
									tcfnIFeval
											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
													.getParameter1().get(0).get(UtilEnum.NAME.value()).asText())
											.toString());
							ifstring = ifstring.replace(
									tcfnIFeval.getSteps().get(i).getChildren().get(j).getParameter2().get(0)
											.get(UtilEnum.NAME.value()).asText(),
									tcfnIFeval
											.getTestdata().get(k).get(tcfnIFeval.getSteps().get(i).getChildren().get(j)
													.getParameter2().get(0).get(UtilEnum.NAME.value()).asText())
											.toString());

						}
						ifstring = ifstring.replace(UtilEnum.IF.value(), "").replace(" ", "");
						condition = (Boolean) scriptEngine.eval(ifstring);
					} else if (tcfnIFeval.getSteps().get(i).iseLse()) {
						eLse = i;
					} else if (tcfnIFeval.getSteps().get(i).isEndif()) {
						endIf = i;
					}

				}
			}
			List<Step> tempStep = new ArrayList<Step>();
			if (iF != -1 && eLse != -1 && endIf != -1) {
				if (condition) {
					for (int i = eLse; i <= endIf; i++) {
						tempStep.add(tcfnIFeval.getSteps().get(i));
					}
					tempStep.add(tcfnIFeval.getSteps().get(iF));
				} else {
					for (int i = iF; i <= eLse; i++) {
						tempStep.add(tcfnIFeval.getSteps().get(i));
					}
					tempStep.add(tcfnIFeval.getSteps().get(endIf));
				}
				for (int i = 0; i < tempStep.size(); i++) {
					tcfnIFeval.getSteps().remove(tempStep.get(i));
				}
			}
			return tcfnIFeval;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Boolean stepExecution(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String sessionID) {
		JSONObject res = null;
		InitializeDependence.sapAction.setDetail(executionDetail, header, itr);
		String[] td = null;
		SapMethod.semCon = new Semaphore(0);
		SapMethod.semProd = new Semaphore(1);
		if (executionDetail != null && executionDetail.getBrowserName() != null) {
			String brName = executionDetail.getBrowserName();
			ExecutionService.a().set(brName.toLowerCase().trim());
		}
		try {
			switch (tcfnStep.getAction().getName().toLowerCase()) {
			case "launchapp":
				if ((res = InitializeDependence.sapAction.launchApplication()) != null) {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return true;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}

			case "click":
			case "mouseclick":
			case "rightclick":
			case "doubleclick":
			case "readtextandstore":
			case "waitforelement":
			case "deletetext":
				if ((res = (JSONObject) InitializeDependence.sapAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, Boolean.class)
						.invoke(InitializeDependence.sapAction, tcfnStep.getObject(),
								tcfnStep.isCaptureScreenshot())) != null) {
					JsonNode result = new ObjectMapper().readTree(res.toString());
					if (executionDetail != null) {
//						td = new String[tcfnStep.getParameters().size()];
//						td[0] = result.get("testdata").asText();
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, res.getString("play_back"),
										result.get(UtilEnum.SCREENSHOT.value()).asText(), ExecutionOption.STEPRESULT,
										tcfnStep));
					}
					if (result.get("action").asText().toLowerCase().equals("readtextandstore")) {
						res.put("paramname", tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText());
						res.put("paramvalue", result.get("testdata").asText());
						InitializeDependence.serverCall.postruntimeParameter(executionDetail, res, header);
						return true;
					} else if (result.get("play_back").asText().toLowerCase()
							.equals(UtilEnum.PASSED.value().toLowerCase()))
						return true;
					else
						return false;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			case "launchapplication":

				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						td = new String[tcfnStep.getParameters().size()];
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((res = (JSONObject) InitializeDependence.sapAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, String.class,
								Boolean.class, String.class)
						.invoke(InitializeDependence.sapAction, tcfnStep.getObject(), td[0],
								tcfnStep.isCaptureScreenshot(), tcfnStep.getObject_name())) != null) {
					JsonNode result = new ObjectMapper().readTree(res.toString());
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, result.get("play_back").asText(),
										result.get(UtilEnum.SCREENSHOT.value()).asText(), ExecutionOption.STEPRESULT,
										tcfnStep));

					if (result.get("play_back").asText().toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()))
						return true;
					else
						return false;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}

			case "wait":
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((Boolean) InitializeDependence.sapAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Integer.class)
						.invoke(InitializeDependence.sapAction, Integer.parseInt(td[0]))) {
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
										tcData.get(tcfnStep.getParameters().get(0).get("name").asText()).asText(),
										ExecutionOption.STEPRESULT, tcfnStep));
					return true;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			case "entertext":
			case "keyboardaction":
			case "keyboardentertext":
			case "waitfortext":
			case "validatepartialtext":
			case "validatetext":
			case "findonscreen":
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((res = (JSONObject) InitializeDependence.sapAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Object.class, String.class,
								Boolean.class)
						.invoke(InitializeDependence.sapAction, tcfnStep.getObject(), td[0],
								tcfnStep.isCaptureScreenshot())) != null) {

					JsonNode result = new ObjectMapper().readTree(res.toString());
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, result.get("play_back").asText(),
										result.get(UtilEnum.SCREENSHOT.value()).asText(), ExecutionOption.STEPRESULT,
										tcfnStep));

					if (result.get("play_back").asText().toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()))
						return true;
					else
						return false;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			case "getfromruntime":
				td = new String[tcfnStep.getParameters().size()];
				res = InitializeDependence.sapAction
						.getfromruntime(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText());
				if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
					((ObjectNode) tcData).put(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText(),
							res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
									.getJSONObject(0).getString("value"));
					td[0] = tcData.get(tcfnStep.getParameters().get(0).get(UtilEnum.NAME.value()).asText()).asText();
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
							null, ExecutionOption.STEPRESULT, tcfnStep));
					Thread.sleep(500);
					return true;
				} else {
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
							UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					Thread.sleep(500);
					return false;
				}

			case "hold":
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					if (tcfnStep.getFunctionCode() != null) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getFunctionCode())
									.get(tcfnStep.getParameters().get(0).get("name").asText()).asText();
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						}
					}
				}
				if ((Boolean) InitializeDependence.sapAction.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Integer.class)
						.invoke(InitializeDependence.sapAction, Integer.parseInt(td[0]))) {
					if (executionDetail != null)
						ExecutionService.a()
								.a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
										tcData.get(tcfnStep.getParameters().get(0).get("name").asText()).asText(),
										ExecutionOption.STEPRESULT, tcfnStep));
					return true;
				} else {
					if (executionDetail != null)
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
					return false;
				}
			default:
//				if (tcfnStep.getParameters().isEmpty())
//					return action.CallMethod(tcfnStep.getAction().getName(), null, executionDetail, header);
//				else {
//					String[] ParaName = new String[tcfnStep.getParameters().size()];
//					String[] value = new String[tcfnStep.getParameters().size()];
//					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
//						ParaName[i] = "";
//						ParaName[i] = tcfnStep.getParameters().get(i).get("name").asText();
//					}
//					for (int i = 0; i < ParaName.length; i++)
//						value[i] = tcData.get(ParaName[i]).asText();
//					return action.CallMethod(tcfnStep.getAction().getName(), value, executionDetail, header);
//				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return false;
		}
	}

	@Override
	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean send) {
		InitializeDependence.sapAction.stopplayback();
		if (executionDetail != null) {
			ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
					ExecutionOption.FINISHEXECUITON, null));

		}

	}

}
