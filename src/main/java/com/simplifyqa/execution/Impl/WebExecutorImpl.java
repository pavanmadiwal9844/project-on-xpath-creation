package com.simplifyqa.execution.Impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rits.cloning.Cloner;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.Utility.ActionClass;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.execution.ExecutionOption;
import com.simplifyqa.execution.ExecutionService;
import com.simplifyqa.execution.ExecutionTask;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.WebMethod;
import com.simplifyqa.sqadrivers.webdriver;

public class WebExecutorImpl extends Executionimpl {

	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	LoggerClass logerOBJ = new LoggerClass();

	private String sessionId;
	private String brName = "";
	private static String lastsessionId = "";

	public WebExecutorImpl(String brName) {
		this.brName = brName;
	}

	public WebExecutorImpl() {
	}

	@SuppressWarnings("unused")
	public Boolean stepExecution(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String SessionID) {
		JSONObject res = null;
		String screenshot = null;
		String[] td = null;
		String runTime = null;
		if (GlobalDetail.ResumePlayback) {
			lastsessionId = sessionId = GlobalDetail.PlaybackSessionID;
		} else {
//			if(!InitializeDependence.suite_new_session) {
			if (!lastsessionId.equals("") && SessionID == null) {
				sessionId = lastsessionId;
			} else {
				// for sttrium
				if (InitializeDependence.suite) {
					if (!InitializeDependence.suite_new_session && InitializeDependence.suite_testcase_sequence != 0) {
						sessionId = lastsessionId;
						InitializeDependence.suite_session = sessionId;
					} else if (!InitializeDependence.suite_new_session
							&& InitializeDependence.suite_testcase_sequence == 0
							&& InitializeDependence.funcItr_suite != 1 && InitializeDependence.funcItr_suite != 0) {
						sessionId = lastsessionId;
						InitializeDependence.suite_session = sessionId;
					} else if (!InitializeDependence.suite_new_session
							&& InitializeDependence.suite_testcase_sequence == 0 && itr != 0) {
						sessionId = lastsessionId;
						InitializeDependence.suite_session = sessionId;
					} else {
						lastsessionId = sessionId = SessionID;
						InitializeDependence.suite_session=sessionId;
					}
				}
				// else case
				else {
					if (!lastsessionId.equals("") && SessionID == null) {
						sessionId = lastsessionId;
					} else {
						if (InitializeDependence.suite_new_session
								&& InitializeDependence.suite_testcase_sequence != 0) {
							sessionId = lastsessionId;
							InitializeDependence.suite_session = sessionId;
						} else {
							lastsessionId = sessionId = SessionID;
						}
					}

				}
			}
//			}else {
//				sessionId=lastsessionId;
//			}
		}
//		System.out.println(InitializeDependence.codeeditorwebdriver.get("cafb6de2-a6a2-4850-8bb3-682151b5597e"));
		if (!GlobalDetail.ReferencePlayback) {
			GlobalDetail.refdetails.put("authkey", executionDetail.getAuthKey());
			GlobalDetail.refdetails.put("customerId", executionDetail.getCustomerID().toString());
			GlobalDetail.refdetails.put("executionIp", executionDetail.getServerIp());
			GlobalDetail.refdetails.put("projectId", executionDetail.getProjectID().toString());

			if (!InitializeDependence.webDriver.containsKey(sessionId)) {

				// code editor webdriver
				InitializeDependence.codeeditorwebdriver.put(sessionId, new webdriver());
				InitializeDependence.codeeditorwebdriver.get(sessionId).brname = executionDetail.getBrowserName()
						.toLowerCase();

				InitializeDependence.webDriver.put(sessionId, new WebMethod());
				InitializeDependence.webDriver.get(sessionId).brname = executionDetail.getBrowserName().toLowerCase();
				InitializeDependence.showPreLoader(sessionId);
				InitializeDependence.webDriver.get(sessionId).setStWebExe(true);
				InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();
			}
			if (InitializeDependence.generaldriver == null) {
				InitializeDependence.generaldriver = new GeneralMethod(executionDetail, header, tcfnStep.getSeq());

			}

			// code editor webdriver
			InitializeDependence.codeeditorwebdriver.get(sessionId).curExecution = executionDetail;
			InitializeDependence.webDriver.get(sessionId).header = header;
			InitializeDependence.webDriver.get(sessionId).currentSeq = tcfnStep.getSeq();
			InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();

		} else {
			if (!InitializeDependence.webDriver.containsKey(sessionId) && itr == 0 && (tcfnStep.getSeq2() == null
					|| tcfnStep.getSeq2().equals("1") || tcfnStep.getSeq2().equals("1.1"))) {
				// code editor webdriver
				GlobalDetail.PlaybackSessionID = sessionId;
				InitializeDependence.codeeditorwebdriver.put(sessionId, new webdriver());
				InitializeDependence.codeeditorwebdriver.get(sessionId).brname = executionDetail.getBrowserName()
						.toLowerCase();
				;

				InitializeDependence.webDriver.put(sessionId, new WebMethod());
				InitializeDependence.webDriver.get(sessionId).brname = executionDetail.getBrowserName().toLowerCase();
				InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();

//				InitializeDependence.webDriver.get(sessionId).header = header;
				InitializeDependence.webDriver.get(sessionId).currentSeq = tcfnStep.getSeq();

				InitializeDependence.webDriver.get(sessionId).curExecution = executionDetail;
				InitializeDependence.webDriver.get(sessionId).header = header;
//				InitializeDependence.webDriver.get(sessionId).currentSeq = tcfnStep.getSeq();
//				InitializeDependence.webDriver.get(sessionId).curObject=tcfnStep.getObject();
			}
			if (InitializeDependence.generaldriver == null && itr == 0 && (tcfnStep.getSeq2() == null
					|| tcfnStep.getSeq2().equals("1") || tcfnStep.getSeq2().equals("1.1"))) {
				if (header == null)
					header = new HashMap<String, String>();
				InitializeDependence.generaldriver = new GeneralMethod(executionDetail, header, tcfnStep.getSeq());
			}
		}
		System.out.println(InitializeDependence.webDriver.get(sessionId));
		if (!InitializeDependence.webDriver.get(sessionId).isStWebExe() && itr == 0
				&& (tcfnStep.getSeq2() == null || tcfnStep.getSeq2().equals("1") || tcfnStep.getSeq2().equals("1.1"))) {

			InitializeDependence.showPreLoader(sessionId);
			InitializeDependence.webDriver.get(sessionId).setStWebExe(true);
			InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();

		}

		InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();
		InitializeDependence.webDriver.get(sessionId).curExecution = executionDetail;

		InitializeDependence.generaldriver.curExecution = executionDetail;
		InitializeDependence.generaldriver.curStep = tcfnStep;
		InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();

		GlobalDetail.object.put(sessionId, tcfnStep.getObject());
		Thread.currentThread().setName(sessionId);
		ExecutionService.a().set(sessionId);
		try {
			Cloner cloner = new Cloner();
			Step step = cloner.deepClone(tcfnStep);
			if (executeAction(executionDetail, step, tcData, itr, header)) {
				tcfnStep.setIsApi(step.getIsApi());
				tcfnStep.setResponseData(step.getResponseData());
				return true;
			} else
				return false;
		} catch (

		Exception e) {
			e.printStackTrace();
			td = new String[tcfnStep.getParameters().size()];
			td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
			if (!GlobalDetail.ReferencePlayback) {
				try {
					screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");

				} catch (Exception e1) {
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							null, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");
				}
			}
			return false;
		}
	}

	private Boolean executeAction(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header) {
		String screenshot = null;
		String[] td = null;
		try {
			Class[] cArg = null;
			if (InitializeDependence.actionlist.get(ActionClass.WBACTION.value())
					.contains(tcfnStep.getAction().getName().toLowerCase())) {

				InitializeDependence.webDriver.get(sessionId).curObject = tcfnStep.getObject();
				InitializeDependence.webDriver.get(sessionId).currentSeq = tcfnStep.getSeq();
//				if(tcfnStep.getChildren().size()!=0) {
//					
//				}
				td = new String[tcfnStep.getParameters().size()];
				td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
				if (td.length > 0) {
					cArg = new Class[td.length];
					for (int i = 0; i < td.length; i++)
						cArg[i] = String.class;
					InitializeDependence.webDriver.get(sessionId).setExepectedValue(null);
					if ((boolean) InitializeDependence.webDriver.get(sessionId).getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
							.invoke(InitializeDependence.webDriver.get(sessionId), td)) {
						InitializeDependence.closePreLoader(sessionId);
						if (!GlobalDetail.ReferencePlayback) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							} else {
								screenshot = null;
							}
							td = new String[tcfnStep.getParameters().size()];
							td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
							if (InitializeDependence.webDriver.get(sessionId).getExepectedValue() != null
									&& InitializeDependence.webDriver.get(sessionId).getActualvalue() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[td.length - 1] = new Cloner().deepClone(
										InitializeDependence.webDriver.get(sessionId).getActualvalue().toString());
								td[0] = new Cloner().deepClone(
										InitializeDependence.webDriver.get(sessionId).getExepectedValue().toString());
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", "ActualValue").toString()));

								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExpectedValue");
							}

							if (InitializeDependence.webDriver.get(sessionId).getItemselected() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[0] = new Cloner().deepClone(
										InitializeDependence.webDriver.get(sessionId).getItemselected().toString());
//								tcfnStep.getParameters().add(new ObjectMapper()
//										.readTree(new JSONObject().put("name", "ActualValue").toString()));

								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "Item Selected");
							}

//							if(InitializeDependence.webDriver.getEx!=null &&
//									InitializeDependence.generaldriver.getJsonvalue()!=null) {
//								
//								for(int i=td.length;i>2;i--) {
////									System.out.println(td[i]);
//								tcfnStep.getParameters().remove(td.length-1);
//								}
//				
//								td[0] = new Cloner()
//										.deepClone(InitializeDependence.generaldriver.getJsonkey());
//						
//								td[1] = new Cloner()
//										.deepClone(InitializeDependence.generaldriver.getJsonvalue());
//								((ObjectNode) tcfnStep.getParameters().get(0)).put("name","JsonKey");
//								((ObjectNode) tcfnStep.getParameters().get(1)).put("name", "JsonValue");
//							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							InitializeDependence.webDriver.get(sessionId).setActualvalue(null);
							InitializeDependence.webDriver.get(sessionId).setExepectedValue(null);
							InitializeDependence.generaldriver.exepectedValue = null;
							InitializeDependence.webDriver.get(sessionId).setItemselected(null);
						}
						return true;
					} else {
						if (!GlobalDetail.ReferencePlayback) {
							screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							ExecutionService.a().set(sessionId);
							td = new String[tcfnStep.getParameters().size()];
							td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
							if (InitializeDependence.webDriver.get(sessionId).getExepectedValue() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[td.length - 1] = new Cloner().deepClone(
										InitializeDependence.webDriver.get(sessionId).getActualvalue().toString());
								td[0] = new Cloner().deepClone(
										InitializeDependence.webDriver.get(sessionId).getExepectedValue().toString());
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", "ActualValue").toString()));

								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExpectedValue");
							}

							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							InitializeDependence.webDriver.get(sessionId).setActualvalue(null);
							InitializeDependence.webDriver.get(sessionId).setExepectedValue(null);
						}
						return false;
					}
				} else {
					if ((boolean) InitializeDependence.webDriver.get(sessionId).getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
							.invoke(InitializeDependence.webDriver.get(sessionId))) {
						if (!GlobalDetail.ReferencePlayback) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							} else {
								screenshot = null;
							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return true;
					} else {
						System.out.println(InitializeDependence.webDriver.get(sessionId));
						if (!GlobalDetail.ReferencePlayback) {
							System.out.println(InitializeDependence.webDriver.get(sessionId));
							screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return false;
					}
				}

			} else if (InitializeDependence.actionlist.get(ActionClass.GNACTION.value())
					.contains(tcfnStep.getAction().getName().toLowerCase())) {
				td = new String[tcfnStep.getParameters().size()];
				td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
				if (td.length > 0) {
					return findandExeuteMethod(executionDetail, tcfnStep, tcData, itr, header, td);
				} else {
					if ((boolean) InitializeDependence.generaldriver.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
							.invoke(InitializeDependence.generaldriver)) {
						if (!GlobalDetail.ReferencePlayback) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = null;
							} else {
								screenshot = null;
							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return true;
					} else {
						if (!GlobalDetail.ReferencePlayback) {
							screenshot = null;
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");

						}
						return false;
					}
				}
			} else {
				if (tcfnStep.getParameters().isEmpty()) {
					td = new String[tcfnStep.getParameters().size()];
					String[] empty = new String[1];
					Class[] cArgument = new Class[td.length];
					if (Editorclassloader.custommethodpresent(tcfnStep.getAction().getName(),
							tcfnStep.getParameters().size(), empty)) {
						webdriver.myClass = null;
						webdriver.customMethod = null;
						if (InitializeDependence.codeeditorwebdriver.get(sessionId)
								.CallMethod(tcfnStep.getAction().getName(), null, executionDetail, header, cArgument)) {
							if (!GlobalDetail.ReferencePlayback) {
								screenshot = null;
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,
										"----------------------------------------------------------------------------------------",
										null, "tc done", "info");
							}
							return true;
						} else {
							if (!GlobalDetail.ReferencePlayback) {
								ExecutionService.a().set(sessionId);
								screenshot = null;
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,
										"----------------------------------------------------------------------------------------",
										null, "tc done", "info");

							}
							return false;
						}

					} else {
						logger.info("No Such Method Found");
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
						logger.info("unable to execute :{}", tcfnStep.getAction().getName());
						if (!GlobalDetail.ReferencePlayback) {
							screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}

						return false;
					}
				} else {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);

					String[] objtype = new String[td.length];
					Class[] cArgument = new Class[td.length];
					for (int i = 0; i < td.length; i++) {
						System.out.println(tcfnStep.getParameters().get(i).get("paramtype"));
						String para = tcfnStep.getParameters().get(i).get("paramtype").asText();
						String para2 = "Alphanumeric";
						if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Alphanumeric")
								|| tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Json")
								|| tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Alphabets")) {
							objtype[i] = "class java.lang.String";
							cArgument[i] = String.class;
						} else if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Numbers")) {
							objtype[i] = "int";
							cArgument[i] = int.class;
						} else if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Boolean")) {
							objtype[i] = "boolean";
							cArgument[i] = boolean.class;
						}

						else if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("float")) {
							objtype[i] = "class java.lang.Float";
							cArgument[i] = Float.class;
						}

						else if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("double")) {
							objtype[i] = "class java.lang.Double";
							cArgument[i] = Double.class;
						}

						else if (tcfnStep.getParameters().get(i).get("paramtype").asText().equals("double")) {
							objtype[i] = "class java.lang.Double";
							cArgument[i] = Double.class;
						}

					}

					if (Editorclassloader.custommethodpresent(tcfnStep.getAction().getName(),
							tcfnStep.getParameters().size(), objtype)) {
						logger.info("custommethodpresent returned true");
						webdriver.myClass = null;
						webdriver.customMethod = null;

						if (InitializeDependence.codeeditorwebdriver.get(sessionId)
								.CallMethod(tcfnStep.getAction().getName(), td, executionDetail, header, cArgument)) {
							if (!GlobalDetail.ReferencePlayback) {
								try {

//								System.out.println(GeneralMethod.curStep.getResponseData());
									if (GeneralMethod.curStep.getResponseData() != null) {
										if (!GeneralMethod.curStep.getResponseData().isEmpty()) {
											tcfnStep.setIsApi(GeneralMethod.curStep.getIsApi());
											tcfnStep.setResponseData(GeneralMethod.curStep.getResponseData());
											GeneralMethod.curStep.setResponseData(null);
										}
									}

									ExecutionService.a().set(sessionId);
									screenshot = null;
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");
								}

							}
							return true;
						} else {
							if (!GlobalDetail.ReferencePlayback) {
								try {
									screenshot = null;
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,
											"----------------------------------------------------------------------------------------",
											null, "tc done", "info");
								}
							}
							return false;
						}
					} else {
						logger.info("No Such Method Found");
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
						logger.info("unable to execute :{}", tcfnStep.getAction().getName());
						if (!GlobalDetail.ReferencePlayback) {
							screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}

						return false;
					}

				}

			}
		}

		// catch block where screenshot is captured
		catch (Exception e) {
			td = new String[tcfnStep.getParameters().size()];
			td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
			logger.info("unable to execute :{}", tcfnStep.getAction().getName());
			if (!GlobalDetail.ReferencePlayback) {
				screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
				ExecutionService.a().set(sessionId);
				ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
						screenshot, ExecutionOption.STEPRESULT, tcfnStep));
				logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
				logerOBJ.customlogg(userlogger,
						"----------------------------------------------------------------------------------------",
						null, "tc done", "info");
			}
			return false;
		}

	}

	private Boolean findandExeuteMethod(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String[] td) {
		String screenshot = null;
		Class[] cArg = null;
		InitializeDependence.generaldriver.curStep = tcfnStep;
		InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
		InitializeDependence.generaldriver.curExecution = executionDetail;
		InitializeDependence.generaldriver.setExepectedValue(null);

		try {
			cArg = new Class[td.length];
			for (int i = 0; i < td.length; i++)
				cArg[i] = String.class;
			if ((boolean) InitializeDependence.generaldriver.getClass()
					.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
					.invoke(InitializeDependence.generaldriver, td)) {
				if (!GlobalDetail.ReferencePlayback) {
					if (tcfnStep.isCaptureScreenshot()) {
						screenshot = null;
					} else {
						screenshot = null;
					}
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
					if (InitializeDependence.generaldriver.getExepectedValue() != null
							&& tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")) {
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					if (InitializeDependence.generaldriver.getExepectedValue() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						tcfnStep.getParameters().add(
								new ObjectMapper().readTree(new JSONObject().put("name", "ActualValue").toString()));
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
					}

					if (InitializeDependence.generaldriver.getJsonkey() != null
							&& InitializeDependence.generaldriver.getJsonvalue() != null) {

						for (int i = td.length; i > 2; i--) {
//							System.out.println(td[i]);
							tcfnStep.getParameters().remove(td.length - 1);
						}

						td[0] = new Cloner().deepClone(InitializeDependence.generaldriver.getJsonkey());

						td[1] = new Cloner().deepClone(InitializeDependence.generaldriver.getJsonvalue());
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "JsonKey");
						((ObjectNode) tcfnStep.getParameters().get(1)).put("name", "JsonValue");
					}

					if (InitializeDependence.generaldriver.getMemoryUtilization() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						// td[td.length - 1] = new Cloner()
						// .deepClone(InitializeDependence.generaldriver.getMemoryUtilization());

						td[1] = new Cloner().deepClone(InitializeDependence.generaldriver.getMemoryUtilization());
						tcfnStep.getParameters().add(new ObjectMapper()
								.readTree(new JSONObject().put("name", "Memory Utilization").toString()));
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "Application");
						// ((ObjectNode) tcfnStep.getParameters().get(1)).put("name", "Memory
						// Utilization");
					}

					if (InitializeDependence.param_description_value != null
							&& InitializeDependence.param_description_name != null) {
						td = Arrays.copyOf(td, td.length + 1);
						System.out.println(td.length);
						td[td.length - 1] = new Cloner().deepClone(InitializeDependence.param_description_value);
						tcfnStep.getParameters()
								.add(new ObjectMapper().readTree(new JSONObject().put("name", "generated").toString()));
						((ObjectNode) tcfnStep.getParameters().get(td.length - 1)).put("name",
								InitializeDependence.param_description_name);

					}

					// removing testdata params from reports
					if (InitializeDependence.remove_params_fromreports) {
						for (int i = td.length; i > 1; i--) {
//							System.out.println(td[i]);
							tcfnStep.getParameters().remove(td.length - 1);
						}

					}

					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
					logerOBJ.customlogg(userlogger,
							"----------------------------------------------------------------------------------------",
							null, "tc done", "info");
					InitializeDependence.generaldriver.exepectedValue = null;
					InitializeDependence.generaldriver.setJsonkey(null);
					InitializeDependence.generaldriver.setJsonvalue(null);
					InitializeDependence.generaldriver.setMemoryUtilization(null);
					InitializeDependence.remove_params_fromreports = false;
					InitializeDependence.param_description_name = null;
					InitializeDependence.param_description_value = null;

				}
				return true;
			} else {
				if (!GlobalDetail.ReferencePlayback) {
					// added
					screenshot = InitializeDependence.webDriver.get(sessionId).takeScreenshot();
					ExecutionService.a().set(sessionId);
					if (tcfnStep.getChildren().size() == 0) {
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
				}
				return false;
			}
		} catch (Exception e) {
			try {
				if ((boolean) InitializeDependence.generaldriver.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), String[].class)
						.invoke(InitializeDependence.generaldriver, new Object[] { td })) {
					if (!GlobalDetail.ReferencePlayback) {
						if (tcfnStep.isCaptureScreenshot()) {
							screenshot = null;
						} else {
							screenshot = null;
						}
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
						if (InitializeDependence.generaldriver.getExepectedValue() != null) {
							td = Arrays.copyOf(td, td.length + 1);
							td[td.length - 1] = new Cloner()
									.deepClone(InitializeDependence.generaldriver.getExepectedValue());
							tcfnStep.getParameters().add(new ObjectMapper()
									.readTree(new JSONObject().put("name", "ActualValue").toString()));
							((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
						}
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					return true;
				} else {
					if (!GlobalDetail.ReferencePlayback) {
						screenshot = null;
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
					return false;
				}
			} catch (Exception ex) {
				try {
					InitializeDependence.generaldriver.curStep = tcfnStep;
					InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
					Map<String, String> tdMap = new HashMap<String, String>();
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						tdMap.put(tcfnStep.getParameters().get(i).get("name").asText(), td[i]);
					}
					InitializeDependence.generaldriver.setExepectedValue(null);
					if ((boolean) InitializeDependence.generaldriver.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Map.class)
							.invoke(InitializeDependence.generaldriver, tdMap)) {
						if (InitializeDependence.generaldriver.getRpParam() != null
								&& InitializeDependence.generaldriver.getRpParam().size() != 0) {
							tcfnStep.getParameters().clear();
							td = new String[InitializeDependence.generaldriver.getRpParam().size()];
							for (Entry<String, String> map : InitializeDependence.generaldriver.getRpParam()
									.entrySet()) {
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", map.getKey()).toString()));
								td[tcfnStep.getParameters().size() - 1] = map.getValue();
							}
						}
						if (!GlobalDetail.ReferencePlayback) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = null;
							} else {
								screenshot = null;
							}
							td = new String[tcfnStep.getParameters().size()];
							td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
							if (InitializeDependence.generaldriver.getExepectedValue() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[td.length - 1] = new Cloner()
										.deepClone(InitializeDependence.generaldriver.getExepectedValue());
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", "ActualValue").toString()));
								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
							InitializeDependence.generaldriver.exepectedValue = null;
						}
						return true;
					} else {
						if (!GlobalDetail.ReferencePlayback) {
							screenshot = null;
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,
									"----------------------------------------------------------------------------------------",
									null, "tc done", "info");
						}
						return false;
					}
				} catch (Exception ev) {
					logger.info("unable to execute :{}", tcfnStep.getAction().getName());
					if (!GlobalDetail.ReferencePlayback) {
						screenshot = null;
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger, "Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,
								"----------------------------------------------------------------------------------------",
								null, "tc done", "info");
					}
					return false;
				}
			}
		}
	}

	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean hybridExecution) {
		try {
			if (hybridExecution)
				return;
			else {
				if (InitializeDependence.webDriver.get(sessionId).driver != null) {
					InitializeDependence.webDriver.get(sessionId).deleteSession();
					InitializeDependence.webDriver.get(sessionId).closeDriver();
				}
				if (InitializeDependence.webDriver.get(sessionId).isStWebExe())
					InitializeDependence.webDriver.get(sessionId).setStWebExe(false);
				InitializeDependence.webDriver.remove(sessionId);
				if (GlobalDetail.runTime.containsKey(sessionId.toLowerCase().trim()))
					GlobalDetail.runTime.remove(sessionId.toLowerCase().trim());
			}
			if (!GlobalDetail.ReferencePlayback) {
				ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
						ExecutionOption.FINISHEXECUITON, null));
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (GlobalDetail.runTime.containsKey(sessionId.toLowerCase().trim()))
				GlobalDetail.runTime.remove(sessionId.toLowerCase().trim());
			if (!GlobalDetail.ReferencePlayback && !hybridExecution)
				ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
						ExecutionOption.FINISHEXECUITON, null));
		}
	}

}
