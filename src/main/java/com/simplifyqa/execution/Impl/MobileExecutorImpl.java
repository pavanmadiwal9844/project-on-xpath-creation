package com.simplifyqa.execution.Impl;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ini4j.Ini;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rits.cloning.Cloner;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.executionData;
import com.simplifyqa.Utility.ActionClass;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.execution.ExecutionOption;
import com.simplifyqa.execution.ExecutionService;
import com.simplifyqa.execution.ExecutionTask;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.sqadrivers.androiddriver;
import com.simplifyqa.sqadrivers.iosdriver;
import com.simplifyqa.sqadrivers.webdriver;

import io.appium.java_client.AppiumDriver;

public class MobileExecutorImpl extends Executionimpl {
	
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);
	LoggerClass logerOBJ = new LoggerClass();
	private String deviceId = "";
	private String sessionId;

	@SuppressWarnings("unused")
	public Boolean stepExecution(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String SessionID) {
		String screenshot = null;
		String[] td = null;
		try {
			sessionId = SessionID;
			if (executionDetail != null) {
				deviceId = executionDetail.getDevicesID();
				GlobalDetail.refdetails.put("authkey", executionDetail.getAuthKey());
				GlobalDetail.refdetails.put("customerId", executionDetail.getCustomerID().toString());
				GlobalDetail.refdetails.put("executionIp", executionDetail.getServerIp());
				GlobalDetail.refdetails.put("projectId", executionDetail.getProjectID().toString());
				
				
				if (executionDetail.getMbType().toLowerCase().equals(UtilEnum.ANDROID.value())
						&& !InitializeDependence.androidDriver.containsKey(sessionId)) {
					
					//code editor androiddriver
					InitializeDependence.codeeditorandroiddriver.put(sessionId,new androiddriver());
					InitializeDependence.androidDriver.put(sessionId,
							new AndroidMethod(executionDetail, tcfnStep.getSeq(), header));
					InitializeDependence.androidDriver.get(sessionId).setDevice(deviceId);
					InitializeDependence.androidDriver.get(sessionId).SetUiautomor2(deviceId, true);
					Thread.sleep(500);
					if (!InitializeDependence.Appiumrun.containsKey(deviceId))
						InitializeDependence.androidDriver.get(sessionId).runapp("io.appium.settings",
								"io.appium.settings.Settings");

				}
				if (executionDetail.getMbType().toLowerCase().equals(UtilEnum.IOS.value())
						&& !InitializeDependence.iosDriver.containsKey(sessionId)) {
					
					InitializeDependence.codeeditoriosdriver.put(sessionId,new iosdriver());
					InitializeDependence.iosDriver.put(sessionId,
							new IosMethod(executionDetail, tcfnStep.getSeq(), header));
					InitializeDependence.iosDriver.get(sessionId).setDevice(deviceId);
					InitializeDependence.iosDriver.get(sessionId).startSession(deviceId);
					Thread.sleep(2000);
					InitializeDependence.iosDriver.get(sessionId).setSessionId(GlobalDetail.iosSession.get(deviceId));
				}
				if (InitializeDependence.generaldriver == null) {
					InitializeDependence.generaldriver = new GeneralMethod(executionDetail, header, tcfnStep.getSeq());
				}

			} else {
				if (!InitializeDependence.androidDriver.containsKey(sessionId) && itr == 0 && tcfnStep.getSeq() == 1
						&& (tcfnStep.getSeq2() == null || tcfnStep.getSeq2().equals("1")
								|| tcfnStep.getSeq2().equals("1.1"))) {
					if (header == null)
						header = new HashMap<String, String>();
					InitializeDependence.androidDriver.put(sessionId,
							new AndroidMethod(executionDetail, tcfnStep.getSeq(), header));
					InitializeDependence.androidDriver.get(sessionId).setDevice(deviceId);
					InitializeDependence.androidDriver.get(sessionId).SetUiautomor2(deviceId, true);
					Thread.sleep(500);
					InitializeDependence.androidDriver.get(sessionId).runapp("io.appium.settings",
							"io.appium.settings.Settings");
				}
				if (!InitializeDependence.iosDriver.containsKey(sessionId) && itr == 0 && tcfnStep.getSeq() == 1
						&& (tcfnStep.getSeq2() == null || tcfnStep.getSeq2().equals("1")
								|| tcfnStep.getSeq2().equals("1.1"))) {
					if (header == null)
						header = new HashMap<String, String>();
					InitializeDependence.iosDriver.put(sessionId,
							new IosMethod(executionDetail, tcfnStep.getSeq(), header));
					InitializeDependence.iosDriver.get(sessionId).setDevice(deviceId);
					InitializeDependence.iosDriver.get(sessionId).startSession(deviceId);
					InitializeDependence.iosDriver.get(sessionId).setSessionId(GlobalDetail.iosSession.get(deviceId));
				}
				if (InitializeDependence.generaldriver == null && itr == 0 && tcfnStep.getSeq() == 1
						&& (tcfnStep.getSeq2() == null || tcfnStep.getSeq2().equals("1")
								|| tcfnStep.getSeq2().equals("1.1"))) {
					if (header == null)
						header = new HashMap<String, String>();
					InitializeDependence.generaldriver = new GeneralMethod(executionDetail, header, tcfnStep.getSeq());
				}
			}
			GlobalDetail.object.put(sessionId, tcfnStep.getObject());
			Thread.currentThread().setName(deviceId);
			ExecutionService.a().set(sessionId);
			Cloner cloner = new Cloner();
			Step step = cloner.deepClone(tcfnStep);
			if (executeAction(executionDetail, step, tcData, itr, header)) {
				tcfnStep.setIsApi(step.getIsApi());
				tcfnStep.setResponseData(step.getResponseData());
				return true;
			} else
				return false;
		} catch (

		Exception e) {
			e.printStackTrace();
			td = new String[tcfnStep.getParameters().size()];
			td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, deviceId.toLowerCase().trim());
			if (executionDetail != null) {
				try {
					screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				} catch (Exception e1) {
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							null, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				}
			}
			return false;
		}

	}

	private String createImageFromByte(byte[] binaryData, Setupexec executionDetail) {
		try {
			Thread.sleep(1000);
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(Base64.getEncoder().encodeToString(binaryData));
			return sb.toString();
		} catch (Exception e) {
			if (executionDetail.getMbType() != null)
				if (executionDetail.getMbType().toLowerCase().equals("android")) {
					StringBuilder sb = new StringBuilder();
					sb.append("data:image/png;base64,");
					sb.append((InitializeDependence.androidDriver.get(sessionId)).getDriver()
							.getScreenshotAs(OutputType.BASE64));
					return sb.toString();
				} else {
					return (InitializeDependence.iosDriver.get(sessionId)).Screenshot();
				}
			else
				return null;
		}
	}

	private Boolean executeAction(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header) {
		String screenshot = null;
		String[] td = null;
		try {
			Class[] cArg = null;
			if (executionDetail.getMbType().equals(UtilEnum.ANDROID.value())) {
				InitializeDependence.androidDriver.get(sessionId).curobject = tcfnStep.getObject();
				InitializeDependence.androidDriver.get(sessionId).currentSeq = tcfnStep.getSeq();
				InitializeDependence.androidDriver.get(sessionId).setDevice(deviceId);
				if (InitializeDependence.actionlist.get(ActionClass.ANACTION.value())
						.contains(tcfnStep.getAction().getName().toLowerCase().trim())) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (td.length > 0) {
						return findandExeuteAndroidMethod(executionDetail, tcfnStep, tcData, itr, header, td);
					} else {

						if ((boolean) InitializeDependence.androidDriver.get(sessionId).getClass()
								.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
								.invoke(InitializeDependence.androidDriver.get(sessionId))) {
							if (executionDetail != null) {
								if (tcfnStep.isCaptureScreenshot()) {
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
								} else {
									screenshot = null;
								}
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					}

				} else if (InitializeDependence.actionlist.get(ActionClass.GNACTION.value())
						.contains(tcfnStep.getAction().getName().toLowerCase())) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (td.length > 0) {
						return findandExeuteMethod(executionDetail, tcfnStep, tcData, itr, header, td);
					} else {
						if ((boolean) InitializeDependence.generaldriver.getClass()
								.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
								.invoke(InitializeDependence.generaldriver)) {
							if (executionDetail != null) {
								if (tcfnStep.isCaptureScreenshot()) {
									screenshot = null;
								} else {
									screenshot = null;
								}
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								screenshot = null;
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					}
				} else {
					if (tcfnStep.getParameters().isEmpty()) {
						td = new String[tcfnStep.getParameters().size()];
						String[] empty=new String[1];
						Class[] cArgument = new Class[td.length];
						if(Editorclassloader.custommethodpresent(tcfnStep.getAction().getName(),tcfnStep.getParameters().size(),empty)) {
							androiddriver.myClass=null;
							androiddriver.customMethod=null;
						if (InitializeDependence.codeeditorandroiddriver.get(sessionId).CallMethod(tcfnStep.getAction().getName(),
								null, executionDetail, header,cArgument)) {
							if (executionDetail != null) {
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								ExecutionService.a().set(sessionId);
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
						}else {
							logger.info("No such Method found....");
							return false;
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
						
						String[] objtype = new String[td.length];
						Class[] cArgument = new Class[td.length];
						for(int i=0;i<td.length;i++) {
							System.out.println(tcfnStep.getParameters().get(i).get("paramtype"));
							String para = tcfnStep.getParameters().get(i).get("paramtype").asText();
							String para2="Alphanumeric";
							if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Alphanumeric") || 
									tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Json") || 
									tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Alphabets")) { 
								objtype[i]="class java.lang.String";
								cArgument[i]=String.class;
							}
							else if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Numbers"))
							{
								objtype[i]="int";
								cArgument[i]=int.class;
							}
							else if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("Boolean"))
							{
								objtype[i]="boolean";
								cArgument[i]=boolean.class;
							}
							
							else if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("float"))
							{
								objtype[i]="class java.lang.Float";
								cArgument[i]=Float.class;
							}
							
							else if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("double"))
							{
								objtype[i]="class java.lang.Double";
								cArgument[i]=Double.class;	
							}
							
							else if(tcfnStep.getParameters().get(i).get("paramtype").asText().equals("double"))
							{
								objtype[i]="class java.lang.Double";
								cArgument[i]=Double.class;	
							}
							
							
						}
						if(Editorclassloader.custommethodpresent(tcfnStep.getAction().getName(),tcfnStep.getParameters().size(),objtype)) {
						
						if (InitializeDependence.codeeditorandroiddriver.get(sessionId).CallMethod(tcfnStep.getAction().getName(),
								td, executionDetail, header,cArgument)) {
							if (executionDetail != null) {
								try {
									ExecutionService.a().set(sessionId);
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								}

							}
							return true;
						} else {
							if (executionDetail != null) {
								try {
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								}
							}
							return false;
						}
						}
						else {
							logger.info("No Such Custom method Found...");
							return false;
						}
					}
				}
			} else if (executionDetail.getMbType().equals(UtilEnum.IOS.value())) {
				InitializeDependence.iosDriver.get(sessionId).curObject = tcfnStep.getObject();
				InitializeDependence.iosDriver.get(sessionId).currentSeq = tcfnStep.getSeq();
				InitializeDependence.iosDriver.get(sessionId).setDevice(deviceId);
				if (InitializeDependence.actionlist.get(ActionClass.IOACTION.value())
						.contains(tcfnStep.getAction().getName().toLowerCase())) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (td.length > 0) {
						cArg = new Class[td.length];
						for (int i = 0; i < td.length; i++)
							cArg[i] = String.class;
						InitializeDependence.iosDriver.get(sessionId).setExepectedValue(null);
						if ((boolean) InitializeDependence.iosDriver.get(sessionId).getClass()
								.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
								.invoke(InitializeDependence.iosDriver.get(sessionId), td)) {
							if (executionDetail != null) {
								if (tcfnStep.isCaptureScreenshot()) {
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
								} else {
									screenshot = null;
								}
								td = new String[tcfnStep.getParameters().size()];
								td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
										deviceId.toLowerCase().trim());
								if (InitializeDependence.generaldriver.getExepectedValue() != null) {
									td = Arrays.copyOf(td, td.length + 1);
									td[td.length - 1] = new Cloner()
											.deepClone(InitializeDependence.generaldriver.getExepectedValue());
									tcfnStep.getParameters().add(new ObjectMapper()
											.readTree(new JSONObject().put("name", "ActualValue").toString()));
									((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
								}
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								InitializeDependence.generaldriver.exepectedValue = null;
							}
							return true;
						} else {
							if (executionDetail != null) {
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					} else {
						if ((boolean) InitializeDependence.iosDriver.get(sessionId).getClass()
								.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
								.invoke(InitializeDependence.iosDriver.get(sessionId))) {
							if (executionDetail != null) {
								if (tcfnStep.isCaptureScreenshot()) {
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
								} else {
									screenshot = null;
								}
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					}
				} else if (InitializeDependence.actionlist.get(ActionClass.GNACTION.value())
						.contains(tcfnStep.getAction().getName().toLowerCase())) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (td.length > 0) {
						return findandExeuteMethod(executionDetail, tcfnStep, tcData, itr, header, td);
					} else {
						if ((boolean) InitializeDependence.generaldriver.getClass()
								.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase())
								.invoke(InitializeDependence.generaldriver)) {
							if (executionDetail != null) {
								if (tcfnStep.isCaptureScreenshot()) {
									screenshot = null;
								} else {
									screenshot = null;
								}
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								screenshot = null;
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					}
				} else {
					if (tcfnStep.getParameters().isEmpty()) {
						if (InitializeDependence.codeeditoriosdriver.get(sessionId).CallMethod(tcfnStep.getAction().getName(),
								null, executionDetail, header)) {
							if (executionDetail != null) {
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().set(sessionId);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return true;
						} else {
							if (executionDetail != null) {
								ExecutionService.a().set(sessionId);
								screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
										executionDetail);
								ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, null,
										UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
								logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
								logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							}
							return false;
						}
					} else {
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, sessionId);
						if (InitializeDependence.iosDriver.get(sessionId).CallMethod(tcfnStep.getAction().getName(), td,
								executionDetail, header)) {
							if (executionDetail != null) {
								try {
									ExecutionService.a().set(sessionId);
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.PASSED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								}

							}
							return true;
						} else {
							if (executionDetail != null) {
								try {
									screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId),
											executionDetail);
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								} catch (Exception e) {
									ExecutionService.a().set(sessionId);
									ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
											UtilEnum.FAILED.value(), null, ExecutionOption.STEPRESULT, tcfnStep));
									logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
									logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
								}
							}
							return false;
						}
					}
				}
			} else {
				if (executionDetail != null) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				}
				logger.info("unable to execute :{}", tcfnStep.getAction().getName());
				return false;

			}
		} catch (

		Exception e) {
			td = new String[tcfnStep.getParameters().size()];
			td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, deviceId.toLowerCase().trim());
			e.printStackTrace();
			if (executionDetail != null) {
				screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
				ExecutionService.a().set(sessionId);
				ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
						screenshot, ExecutionOption.STEPRESULT, tcfnStep));
				logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
				logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
			}
			logger.info("unable to execute :{}", tcfnStep.getAction().getName());
			return false;
		}

	}

	private Boolean findandExeuteMethod(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String[] td) {
		String screenshot = null;
		Class[] cArg = null;
		InitializeDependence.generaldriver.curStep = tcfnStep;
		InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
		InitializeDependence.generaldriver.setExepectedValue(null);
		try {
			cArg = new Class[td.length];
			for (int i = 0; i < td.length; i++)
				cArg[i] = String.class;
			if ((boolean) InitializeDependence.generaldriver.getClass()
					.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
					.invoke(InitializeDependence.generaldriver, td)) {
				if (executionDetail != null) {
					if (tcfnStep.isCaptureScreenshot()) {
						screenshot = null;
					} else {
						screenshot = null;
					}
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (InitializeDependence.generaldriver.getExepectedValue() != null
							&& tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")) {
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					if (InitializeDependence.generaldriver.getExepectedValue() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						tcfnStep.getParameters().add(
								new ObjectMapper().readTree(new JSONObject().put("name", "ActualValue").toString()));
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
					}
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
					InitializeDependence.generaldriver.exepectedValue = null;
				}
				return true;
			} else {
				if (executionDetail != null) {
					screenshot = null;
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				}
				return false;
			}
		} catch (Exception e) {
			try {
				if ((boolean) InitializeDependence.generaldriver.getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), String[].class)
						.invoke(InitializeDependence.generaldriver, new Object[] { td })) {
					if (executionDetail != null) {
						if (tcfnStep.isCaptureScreenshot()) {
							screenshot = null;
						} else {
							screenshot = null;
						}
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
								deviceId.toLowerCase().trim());
						if (InitializeDependence.generaldriver.getExepectedValue() != null) {
							td = Arrays.copyOf(td, td.length + 1);
							td[td.length - 1] = new Cloner()
									.deepClone(InitializeDependence.generaldriver.getExepectedValue());
							tcfnStep.getParameters().add(new ObjectMapper()
									.readTree(new JSONObject().put("name", "ActualValue").toString()));
							((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
						}
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
						logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					return true;
				} else {
					if (executionDetail != null) {
						screenshot = null;
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
					}
					return false;
				}
			} catch (Exception ex) {
				try {
					InitializeDependence.generaldriver.curStep = tcfnStep;
					InitializeDependence.generaldriver.currentSeq = tcfnStep.getSeq();
					Map<String, String> tdMap = new HashMap<String, String>();
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						tdMap.put(tcfnStep.getParameters().get(i).get("name").asText(), td[i]);
					}
					InitializeDependence.generaldriver.setExepectedValue(null);
					if ((boolean) InitializeDependence.generaldriver.getClass()
							.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), Map.class)
							.invoke(InitializeDependence.generaldriver, tdMap)) {
						if (InitializeDependence.generaldriver.getRpParam() != null
								&& InitializeDependence.generaldriver.getRpParam().size() != 0) {
							tcfnStep.getParameters().clear();
							td = new String[InitializeDependence.generaldriver.getRpParam().size()];
							for (Entry<String, String> map : InitializeDependence.generaldriver.getRpParam()
									.entrySet()) {
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", map.getKey()).toString()));
								td[tcfnStep.getParameters().size() - 1] = map.getValue();
							}
						}
						if (executionDetail != null) {
							if (tcfnStep.isCaptureScreenshot()) {
								screenshot = null;
							} else {
								screenshot = null;
							}
							td = new String[tcfnStep.getParameters().size()];
							td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
									deviceId.toLowerCase().trim());

							if (InitializeDependence.generaldriver.getExepectedValue() != null) {
								td = Arrays.copyOf(td, td.length + 1);
								td[td.length - 1] = new Cloner()
										.deepClone(InitializeDependence.generaldriver.getExepectedValue());
								tcfnStep.getParameters().add(new ObjectMapper()
										.readTree(new JSONObject().put("name", "ActualValue").toString()));
								((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
							}
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
							logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
							InitializeDependence.generaldriver.exepectedValue = null;
						}
						return true;
					} else {
						if (executionDetail != null) {
							screenshot = null;
							ExecutionService.a().set(sessionId);
							ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
									UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
							logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
							logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
						}
						return false;
					}
				} catch (Exception ev) {
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (executionDetail != null) {
						screenshot = null;
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
					}
					logger.info("unable to execute :{}", tcfnStep.getAction().getName());
					return false;
				}
			}
		}
	}

	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean hybridExecution) {
		try {
			if (hybridExecution)
				return;
			else {
				if (executionDetail.getMbType() != null
						&& executionDetail.getMbType().equals(UtilEnum.ANDROID.value())) {
					
					if(InitializeDependence.iteration) {
					
					if (!InitializeDependence.androidDriver.isEmpty()) {
						if (!GlobalDetail.websocket.isEmpty()) {
							GlobalDetail.websocket.get(executionDetail.getDevicesID()).getBasicRemote().sendText("off");
							GlobalDetail.websocket.remove(executionDetail.getDevicesID());
							
							if(!InitializeDependence.aMirror.isEmpty()) {
							InitializeDependence.aMirror.get(executionDetail.getDevicesID()).stopScreenListener();
							InitializeDependence.aMirror.remove(executionDetail.getDevicesID());
							}
						}
						((AppiumDriver) InitializeDependence.androidDriver.get(sessionId).getDriver()).closeApp();
						InitializeDependence.androidDriver.remove(sessionId);
						GlobalDetail.runTime.remove(executionDetail.getDevicesID().toLowerCase().trim());
					}}
				}
				if (executionDetail.getMbType() != null && executionDetail.getMbType().equals(UtilEnum.IOS.value())) {
					if (!InitializeDependence.iosDriver.isEmpty()) {
						if (!GlobalDetail.websocket.isEmpty()) {
							GlobalDetail.websocket.get(executionDetail.getDevicesID()).getBasicRemote().sendText("off");
							GlobalDetail.websocket.get(executionDetail.getDevicesID()).close();
							GlobalDetail.websocket.remove(executionDetail.getDevicesID());
						}
						InitializeDependence.iosDriver.get(sessionId).closeApp();
						InitializeDependence.iosDriver.remove(sessionId);
						if (GlobalDetail.runTime.containsKey(executionDetail.getDevicesID().toLowerCase().trim()))
							GlobalDetail.runTime.remove(executionDetail.getDevicesID().toLowerCase().trim());
					}
				}
				InitializeDependence.generaldriver = null;
				if (executionDetail != null)
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
							ExecutionOption.FINISHEXECUITON, null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (GlobalDetail.runTime.containsKey(executionDetail.getDevicesID().toLowerCase().trim()))
				GlobalDetail.runTime.remove(executionDetail.getDevicesID().toLowerCase().trim());
			if (executionDetail != null && !hybridExecution)
				ExecutionService.a().a(new ExecutionTask(executionDetail, header, 0, null, null, null,
						ExecutionOption.FINISHEXECUITON, null));
		}
	}

	private Boolean findandExeuteAndroidMethod(Setupexec executionDetail, Step tcfnStep, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String[] td) {
		String screenshot = null;
		Class[] cArg = null;
		try {
			cArg = new Class[td.length];
			for (int i = 0; i < td.length; i++)
				cArg[i] = String.class;
			InitializeDependence.androidDriver.get(sessionId).setExepectedValue(null);
			if ((boolean) InitializeDependence.androidDriver.get(sessionId).getClass()
					.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
					.invoke(InitializeDependence.androidDriver.get(sessionId), td)) {
				if (executionDetail != null) {
					if (tcfnStep.isCaptureScreenshot()) {
						screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
					} else {
						screenshot = null;
					}
					td = new String[tcfnStep.getParameters().size()];
					td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
							deviceId.toLowerCase().trim());
					if (InitializeDependence.generaldriver.getExepectedValue() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.generaldriver.getExepectedValue());
						tcfnStep.getParameters().add(
								new ObjectMapper().readTree(new JSONObject().put("name", "ActualValue").toString()));
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
					}
					
					if (InitializeDependence.androidDriver.get(sessionId).getExepectedValue() != null) {
						td = Arrays.copyOf(td, td.length + 1);
						td[td.length - 1] = new Cloner()
								.deepClone(InitializeDependence.androidDriver.get(sessionId).getActualvalue());
						td[0]=new Cloner()
								.deepClone(InitializeDependence.androidDriver.get(sessionId).getExepectedValue().toString());
						tcfnStep.getParameters().add(new ObjectMapper()
								.readTree(new JSONObject().put("name", "ActualValue").toString()));
						
						((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExpectedValue");
					}
					
					
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.PASSED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
					InitializeDependence.generaldriver.exepectedValue = null;
					InitializeDependence.androidDriver.get(sessionId).setActualvalue(null);
					InitializeDependence.androidDriver.get(sessionId).setExepectedValue(null);
				}
				return true;
			} else {
				
				if (InitializeDependence.androidDriver.get(sessionId).getExepectedValue() != null) {
					td = Arrays.copyOf(td, td.length + 1);
					td[td.length - 1] = new Cloner()
							.deepClone(InitializeDependence.androidDriver.get(sessionId).getActualvalue());
					td[0]=new Cloner()
							.deepClone(InitializeDependence.androidDriver.get(sessionId).getExepectedValue().toString());
					tcfnStep.getParameters().add(new ObjectMapper()
							.readTree(new JSONObject().put("name", "ActualValue").toString()));
					
					((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExpectedValue");
				}
				if (executionDetail != null) {
					screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				}
				
				InitializeDependence.androidDriver.get(sessionId).setActualvalue(null);
				InitializeDependence.androidDriver.get(sessionId).setExepectedValue(null);
				return false;
			}
		} catch (Exception e) {
			try {
				InitializeDependence.androidDriver.get(sessionId).setExepectedValue(null);
				if ((boolean) InitializeDependence.androidDriver.get(sessionId).getClass()
						.getDeclaredMethod(tcfnStep.getAction().getName().toLowerCase(), cArg)
						.invoke(InitializeDependence.androidDriver.get(sessionId), new Object[] { td })) {
					if (executionDetail != null) {
						if (tcfnStep.isCaptureScreenshot()) {
							screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
						} else {
							screenshot = null;
						}
						td = new String[tcfnStep.getParameters().size()];
						td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail,
								deviceId.toLowerCase().trim());
						if (InitializeDependence.generaldriver.getExepectedValue() != null) {
							td = Arrays.copyOf(td, td.length + 1);
							td[td.length - 1] = new Cloner()
									.deepClone(InitializeDependence.generaldriver.getExepectedValue());
							tcfnStep.getParameters().add(new ObjectMapper()
									.readTree(new JSONObject().put("name", "ActualValue").toString()));
							((ObjectNode) tcfnStep.getParameters().get(0)).put("name", "ExecptedValue");
						}
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.PASSED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger,"Step Passed.", null, "", "info");
						logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
						InitializeDependence.generaldriver.exepectedValue = null;
					}
					return true;
				} else {
					if (executionDetail != null) {
						screenshot = createImageFromByte(GlobalDetail.screenshot.get(deviceId), executionDetail);
						ExecutionService.a().set(sessionId);
						ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td,
								UtilEnum.FAILED.value(), screenshot, ExecutionOption.STEPRESULT, tcfnStep));
						logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
						logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
					}
					return false;
				}
			} catch (Exception ex) {
				td = InitializeDependence.getTdArray(tcfnStep, tcData, executionDetail, deviceId.toLowerCase().trim());
				if (executionDetail != null) {
					screenshot = null;
					ExecutionService.a().set(sessionId);
					ExecutionService.a().a(new ExecutionTask(executionDetail, header, itr, td, UtilEnum.FAILED.value(),
							screenshot, ExecutionOption.STEPRESULT, tcfnStep));
					logerOBJ.customlogg(userlogger,"Step Failed.", null, "", "error");
					logerOBJ.customlogg(userlogger,"----------------------------------------------------------------------------------------", null, "tc done", "info");
				}
				logger.info("unable to execute :{}", tcfnStep.getAction().getName());
				return false;
			}
		}
	}
}
