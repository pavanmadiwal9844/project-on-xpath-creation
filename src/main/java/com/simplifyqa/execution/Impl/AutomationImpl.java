package com.simplifyqa.execution.Impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.CodeEditor.CodeEditorDebugger;
import com.simplifyqa.DTO.FinishExe;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.StartExecution;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.DTO.UpdateRp;
import com.simplifyqa.DTO.executionData;
import com.simplifyqa.DTO.functionData;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.execution.Interface.ExecutionUtil;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mobile.ios.Idevice;

public class AutomationImpl implements ExecutionUtil {

	final static Logger logger = LoggerFactory.getLogger(AutomationImpl.class);
	private static final org.apache.log4j.Logger APIlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

			LoggerClass APILogger = new LoggerClass();
			

	@Override
	public JSONObject getExecutionDetail(JSONObject req, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside ExecutionDetail", null, null, "info");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			SearchColumns searchcolumns = new SearchColumns();
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.CUSTOMERID.value(),
							req.getInt(UtilEnum.CUSTOMERID.value()), Boolean.parseBoolean(UtilEnum.FALSE.value()),
							UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.PROJECTID.value(),
							req.getInt(UtilEnum.PROJECTID.value()), Boolean.parseBoolean(UtilEnum.FALSE.value()),
							UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.ID.value(), req.getInt(UtilEnum.EXECUTIONID.value()),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns().add(new searchColumnDTO<Boolean>(UtilEnum.DELETED.value(), Boolean.FALSE,
					Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.BOOLEAN.value()));
			searchcolumns.setStartIndex(0);
			searchcolumns.setLimit(100000);
			searchcolumns.setCollection(UtilEnum.EXECUTION.value());
			APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
			response = new JSONObject(
					HttpUtility.SendPost(req.getString(UtilEnum.SERVERIP.value()) + UtilEnum.SEARCH.value(),
							new ObjectMapper().writeValueAsString(searchcolumns), header));
			
			APILogger.customloggAPI(APIlogger, "After API call. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response - ", response, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			
			//execution detail get current project name
			
			logger.info("Execution detail for executionId {}", req.getInt(UtilEnum.EXECUTIONID.value()));
			logger.info("executing with data" + response.toString());
			return response;
		} catch (Exception e) {
			logger.info("Unable to get Execution Detail {}", e);
			return null;
		}

	}

	@Override
	public Testcase getTCstep(Setupexec exedetial, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside getTCstep", null, null, "info");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			if (exedetial.getExecplanId() != null) {
				HttpUtility.udid=exedetial.getDevicesID();
				JSONObject req = new JSONObject();
				req.put("customerId", exedetial.getCustomerID());
				req.put("executionId", exedetial.getExecutionID());
				req.put("projectId", exedetial.getProjectID());
				req.put("testcaseId", exedetial.getTestcaseID());
				req.put("tcSeq", exedetial.getTcSeq());
				req.put("execplanId", exedetial.getExecplanId());
				req.put("suiteId", exedetial.getSuiteID());
				for (int i = 0; i < exedetial.getSuiteIds().size(); i++) {
					if (exedetial.getSuiteIds().get(i).getSuiteId().equals(exedetial.getSuiteID()))
						req.put("execplanScenarioId", exedetial.getSuiteIds().get(i).getExecplanScenarioId());
				}
				APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
				response = new JSONObject(HttpUtility.SendPost(exedetial.getServerIp() + UtilEnum.EXECUTIONDATA.value(),
						req.toString(), header));
				APILogger.customloggAPI(APIlogger, "After API call Response1. ", null, null, "info");
				APILogger.customloggAPI(APIlogger, "Response 1 - ", response, null, "info");
				APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
				mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				return mapper.readValue(response.getJSONObject(UtilEnum.DATA.value()).toString(), Testcase.class);
			}
			executionData Tcrequest = new executionData();
			Tcrequest.setCustomerId(exedetial.getCustomerID());
			Tcrequest.setExecutionId(exedetial.getExecutionID());
			Tcrequest.setProjectId(exedetial.getProjectID());
			Tcrequest.setTestcaseId(exedetial.getTestcaseID());
			Tcrequest.setTcSeq(exedetial.getTcSeq());
			response = new JSONObject(HttpUtility.SendPost(exedetial.getServerIp() + UtilEnum.EXECUTIONDATA.value(),
					new ObjectMapper().writeValueAsString(Tcrequest), header));
			APILogger.customloggAPI(APIlogger, "After API call Response2. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response 2 - ", response, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapper.readValue(response.getJSONObject(UtilEnum.DATA.value()).toString(), Testcase.class);
		} catch (Exception e) {
			logger.info("Unable to get Testcase {}", e);
			return null;
		}
	}

	@Override
	public void postStepsRc(Setupexec exeDetial, HashMap<String, String> header, Integer itr, String[] tcData,
			String status, String screenshot, Step step) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside postStepsRc ", null, null, "info");
			
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			HttpUtility.udid=exeDetial.getDevicesID();
			Integer result;
			JSONObject stepres = new JSONObject();
			if (status.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()))
				result = 1;
			else if (status.toLowerCase().equals(UtilEnum.FAILED.value().toLowerCase())) {
				result = 0;
				step.setCaptureScreenshot(true);
			} else
				result = 2;
			if (GlobalDetail.debugSession != null) {
				if (result == 1) {
					GlobalDetail.debugSession.getBasicRemote().sendText("step passed");
				} else if (result == 0) {
					GlobalDetail.debugSession.getBasicRemote().sendText("step failed");
				} else if (result == 2) {
					GlobalDetail.debugSession.getBasicRemote().sendText("step skipped");
				}
			}
			if (step.isCaptureScreenshot() == true) {
				step.setScreenshot(screenshot);
				if (step.getParameters().isEmpty())
					step.setData(null);
				else {
					JSONObject data = new JSONObject();
					for (int i = 0; i < step.getParameters().size(); i++) {
						if (step.getParameters().get(i).has("protected")
								&& step.getParameters().get(i).get("protected").asBoolean()) {
							data.put(step.getParameters().get(i).get("name").asText(), "*******");
						} else {
							data.put(step.getParameters().get(i).get(UtilEnum.NAME.value()).asText(), tcData[i]);
						}
					}
					List<JSONObject> value = new ArrayList<JSONObject>();
					value.add(data);
					step.setData(new ObjectMapper().readTree(value.toString()));
				}
				step.setResult(result);
				step.setTcSeq(exeDetial.getTcSeq());
				step.setTestcaseId(exeDetial.getTestcaseID());
				step.setSuiteId(exeDetial.getSuiteID());
				step.setExecutionId(exeDetial.getExecutionPlanScenario() ? exeDetial.getExecplanexecId()
						: exeDetial.getExecutionID());
				step.setStepId(step.getId());
				step.setItr(itr + 1);
				stepres = new JSONObject(new ObjectMapper().writeValueAsString(step));
				if (step.getIsApi()) {
					stepres.put("apiResponse", step.getIsApi());
					stepres.put("apiResponseUrl", "NA");
					stepres.put("responseData", step.getResponseData());
				}
				if (exeDetial.getExecutionPlanScenario()) {
					stepres.put("executionPlanScenario", exeDetial.getExecutionPlanScenario());
					for (int i = 0; i < exeDetial.getSuiteIds().size(); i++) {
						if (exeDetial.getSuiteIds().get(i).getSuiteId().equals(exeDetial.getSuiteID())) {
							stepres.put("execplanScenarioId", exeDetial.getSuiteIds().get(i).getExecplanScenarioId());
//							stepres.put("seq", exeDetial.getSuiteIds().get(i).getSeq());
						}
					}
					stepres.put("execplanexecId", exeDetial.getExecplanexecId());
					stepres.put("execplanId", exeDetial.getExecplanId());

				}
				String res= HttpUtility.SendPost(exeDetial.getServerIp() + UtilEnum.EXECUTIONRESULT.value(),
						stepres.toString(), header);
				APILogger.customloggAPI(APIlogger, "After API call Response2. ", null, null, "info");
				APILogger.customloggAPI(APIlogger, "Response 2 - ", res, null, "info");
				APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
				logger.info(res);
				
				
				APILogger.customloggAPI(APIlogger, "<<<<<<<<<<Function Step>>>>>>>>>>> Number ", null, stepres.get("seq2").toString(), "info");
				APILogger.customloggAPI(APIlogger, "<<<<<<<<<<Step>>>>>>>>>>> Number ", null, stepres.get("seq").toString(), "info");
				
				
			} else {
				if (step.getParameters().isEmpty())
					step.setData(null);
				else {
					JSONObject data = new JSONObject();
					for (int i = 0; i < step.getParameters().size(); i++) {
						if (step.getParameters().get(i).has("protected")
								&& step.getParameters().get(i).get("protected").asBoolean()) {
							data.put(step.getParameters().get(i).get("name").asText(), "*******");
						} else {
							data.put(step.getParameters().get(i).get(UtilEnum.NAME.value()).asText(), tcData[i]);
						}
					}
					List<JSONObject> value = new ArrayList<JSONObject>();
					value.add(data);
					step.setData(new ObjectMapper().readTree(value.toString()));
				}
				step.setResult(result);
				step.setTcSeq(exeDetial.getTcSeq());
				step.setTestcaseId(exeDetial.getTestcaseID());
				step.setSuiteId(exeDetial.getSuiteID());
				step.setExecutionId(exeDetial.getExecutionPlanScenario() ? exeDetial.getExecplanexecId()
						: exeDetial.getExecutionID());
				step.setStepId(step.getId());
				step.setItr(itr + 1);
//				if(!Idevice.TestWDA(exeDetial.getDevicesID())) {
//					InitializeDependence.xctestrun.remove(exeDetial.getDevicesID());
//					Idevice.startSession(exeDetial.getDevicesID());
//				}
				logger.info("StepId :{} ObjectName:{} Result:{} ", step.getId(), step.getObject_name(),
						step.getResult());
				stepres = new JSONObject(new ObjectMapper().writeValueAsString(step));
				if (step.getIsApi()) {
					stepres.put("apiResponse", step.getIsApi());
					stepres.put("apiResponseUrl", "NA");
					stepres.put("responseData", step.getResponseData());
				}
				if (exeDetial.getExecutionPlanScenario()) {
					stepres.put("executionPlanScenario", exeDetial.getExecutionPlanScenario());
					for (int i = 0; i < exeDetial.getSuiteIds().size(); i++) {
						if (exeDetial.getSuiteIds().get(i).getSuiteId().equals(exeDetial.getSuiteID())) {
							stepres.put("execplanScenarioId", exeDetial.getSuiteIds().get(i).getExecplanScenarioId());
//							stepres.put("seq", exeDetial.getSuiteIds().get(i).getSeq());
						}
					}
					stepres.put("execplanexecId", exeDetial.getExecplanexecId());
					stepres.put("execplanId", exeDetial.getExecplanId());

				}
				String res2=HttpUtility.SendPost(exeDetial.getServerIp() + UtilEnum.EXECUTIONRESULT.value(),
						stepres.toString(), header);
				APILogger.customloggAPI(APIlogger, "After API call Response2. ", null, null, "info");
				APILogger.customloggAPI(APIlogger, "Response 2 - ", res2, null, "info");
				APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
				logger.info(res2);
				
				APILogger.customloggAPI(APIlogger, "<<<<<<<<<<Function Step>>>>>>>>>>> Number ", null, stepres.get("seq2").toString(), "info");
				APILogger.customloggAPI(APIlogger, "<<<<<<<<<<Step>>>>>>>>>>> Number ", null, stepres.get("seq").toString(), "info");
			}
			
			InitializeDependence.step_pass=0;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Unable to pass step Result {} ", e);
			
			if(InitializeDependence.step_pass<10) {
				try {
				Thread.sleep(1000);}
				catch (Exception e1) {
					// TODO: handle exception
				e1.printStackTrace();
				InitializeDependence.step_pass++;
				postStepsRc(exeDetial,header,itr,tcData,status,screenshot,step);
				}
			}
			else {
				e.printStackTrace();
				logger.info("Unable to pass step Result {} ", e);
				InitializeDependence.step_pass=0;
			}
			
		}

	}

	@Override
	public void postsfITRexecution(Setupexec exedetial, Integer itr, String status, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside postsITRexecution", null, null, "info");
			logger.info("inside post itr");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			StartExecution start = new StartExecution();
			HttpUtility.udid=exedetial.getDevicesID();
			start.setCustomerId(exedetial.getCustomerID());
			start.setExecutionId(
					exedetial.getExecutionPlanScenario() ? exedetial.getExecplanexecId() : exedetial.getExecutionID());
			start.setProjectId(exedetial.getProjectID());
			start.setItr(itr + 1);
			start.setTcSeq(exedetial.getTcSeq());
			start.setTestcaseCode(exedetial.getTestcaseCode());
			start.setTestcaseId(exedetial.getTestcaseID());
			start.setVersion(exedetial.getVersion());
			start.setResultTc(status);
			start.setSuiteId(exedetial.getSuiteID());
			start.setExecutionPlanScenario(exedetial.getExecutionPlanScenario());
			start.setScenarioFolderId(exedetial.getScenarioFolderId());
			start.setExpPlnnedDate(exedetial.getExpPlnnedDate());
			start.setExecplanId(exedetial.getExecplanId());
			start.setExpAssignedTo(exedetial.getExpAssignedTo());
			start.setExecplanexecId(exedetial.getExecplanexecId());
			for (int i = 0; i < exedetial.getSuiteIds().size(); i++) {
				if (exedetial.getSuiteIds().get(i).getSuiteId().equals(exedetial.getSuiteID()))
					start.setSeq(exedetial.getSuiteIds().get(i).getSeq());
			}
			
			APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
			JSONObject res = HttpUtility.sendPost(exedetial.getServerIp() + UtilEnum.EXECUTIONTESTCASE.value(),
					new ObjectMapper().writeValueAsString(start), header);
			
			APILogger.customloggAPI(APIlogger, "After API call Response. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response - ", res, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			
			logger.info("response : "+res);
			logger.info(res.toString());
		} catch (IOException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			e.printStackTrace();
			logger.error("error", e);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void postFinishExecution(Setupexec exedetial, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside postFinishExecution", null, null, "info");
			
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			FinishExe finishex = new FinishExe();
			HttpUtility.udid=exedetial.getDevicesID();
			finishex.setCustomerId(exedetial.getCustomerID());
			finishex.setExecutionId(exedetial.getExecutionID());
			finishex.setProjectId(exedetial.getProjectID());
			finishex.setTestcaseId(exedetial.getTestcaseID());
			finishex.setSuiteId(exedetial.getSuiteID());
			APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
			JSONObject res = HttpUtility.sendPost(exedetial.getServerIp() + UtilEnum.FINISHEXECUTION.value(),
					new ObjectMapper().writeValueAsString(finishex), header);
			APILogger.customloggAPI(APIlogger, "After API call. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response - ", res, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			logger.info(res.toString());
			if (GlobalDetail.debugSession != null)
				GlobalDetail.debugSession.getBasicRemote().sendText("ExecutionOver");
		} catch (IOException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.error("error", e);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Testcase getFNstep(Setupexec exedetail, Step fnStep, JsonNode tcData, HashMap<String, String> header) {

		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside getFNstep ", null, null, "info");
			
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			functionData funcstepData = new functionData();
			HttpUtility.udid=exedetail.getDevicesID();
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			funcstepData.setCustomerId(exedetail.getCustomerID());
			funcstepData.setExecutionId(exedetail.getExecutionID());
			funcstepData.setProjectId(exedetail.getProjectID());
			funcstepData.setFunctionId(fnStep.getFunctionId());
			funcstepData.setEnvironmentType(exedetail.getEnvironmentType());
			if(fnStep.getFnVersion()!=null)
				funcstepData.setVersion(Integer.parseInt(fnStep.getFnVersion()));
			
			
			funcstepData
					.setFunctiondataItr(tcData.get(fnStep.getFunctionCode()).get(UtilEnum.ITERATIONSELECTED.value()));
			
			APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
			
			response = new JSONObject(HttpUtility.SendPost(exedetail.getServerIp() + UtilEnum.FUNCTIONSTEPDATA.value(),
					new ObjectMapper().writeValueAsString(funcstepData), header));
			
			APILogger.customloggAPI(APIlogger, "After API call. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response - ", response, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");


//			if(InitializeDependence.suite) {
//			checkFnIteration(response);
//			}
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapper.readValue(response.getJSONObject(UtilEnum.DATA.value()).toString(), Testcase.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}
	
	public void checkFnIteration(JSONObject res) {
		int function_itr=res.getJSONObject("data").getJSONArray("testdata").length();
		if(InitializeDependence.suite) {
			InitializeDependence.funcItr_suite=function_itr;
		}
	}

	@Override
	public void postlogFile(JSONObject req, HashMap<String, String> header) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postruntimeParameter(Setupexec exedetail, JSONObject param, HashMap<String, String> header) {

		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside postruntimeParameter ", null, null, "info");
			
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			UpdateRp update = new UpdateRp();
			HttpUtility.udid=exedetail.getDevicesID();
			update.setCustomerId(exedetail.getCustomerID());
			update.setProjectId(exedetail.getProjectID());
			update.setTestcaseId(exedetail.getTestcaseID());
			update.setKey(param.getString(Constants.PARAMNAME).trim());
			update.setValue(param.getString(Constants.PARAMVALUE));

			HttpUtility.SendPost(exedetail.getServerIp() + UtilEnum.UPDSTERP.value(),
					new ObjectMapper().writeValueAsString(update), header);
			APILogger.customloggAPI(APIlogger, "After API call. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			
		} catch (Exception e) {
			logger.error("Error in Storing parameter", e);
		}

	}

	@Override
	public JSONObject getruntimeParameter(Setupexec exedetail, String paramName, HashMap<String, String> header) {
		try {
			
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside getruntimeParameter ", null, null, "info");


			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			HttpUtility.udid=exedetail.getDevicesID();
			SearchColumns searchs = new SearchColumns();
			searchs.getSearchColumns().add(new searchColumnDTO<Integer>(UtilEnum.CUSTOMERID.value(),
					exedetail.getCustomerID(), Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchs.getSearchColumns().add(new searchColumnDTO<Integer>(UtilEnum.PROJECTID.value(),
					exedetail.getProjectID(), Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchs.getSearchColumns().add(new searchColumnDTO<String>(UtilEnum.KEY.value(), paramName,
					Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
			searchs.getSearchColumns().add(new searchColumnDTO<Boolean>(UtilEnum.DELETED.value(), Boolean.FALSE,
					Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.BOOLEAN.value()));
			searchs.setCollection("runtime_parameters");
			searchs.setLimit(1);
			searchs.setStartIndex(0);
			JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
			return new JSONObject(
					HttpUtility.SendPost(exedetail.getServerIp() + UtilEnum.SEARCH.value(), object.toString(), header));
		} catch (Exception e) {
			logger.error("Error in geting parameter", e);
		}
		return null;
	}

	@Override
	public JSONObject getScenarioExecTcs(Setupexec exedetail, HashMap<String, String> header) {

		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside getScenarioExecTcs ", null, null, "info");
			
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;

			JSONObject req = new JSONObject();
			JSONObject response = new JSONObject();
			req.put("execplanId", exedetail.getExecplanId());
			req.put("suiteIds", exedetail.getSuiteIds());
			req.put("scenarioFolderId", exedetail.getScenarioFolderId());
			req.put("expAssignedTo", exedetail.getExpAssignedTo());
			req.put("expPlnnedDate", exedetail.getExpPlnnedDate());
			req.put("execplanexecId", exedetail.getExecplanexecId());
			req.put("customerId", exedetail.getCustomerID());
			req.put("projectId", exedetail.getProjectID());
			HttpUtility.udid=exedetail.getDevicesID();
			APILogger.customloggAPI(APIlogger, "Before API call. ", null, null, "info");
			
			response = new JSONObject(
					HttpUtility.SendPost(exedetail.getServerIp() + UtilEnum.SCENARIO.value(), req.toString(), header));
			APILogger.customloggAPI(APIlogger, "After API call. ", null, null, "info");
			APILogger.customloggAPI(APIlogger, "Response - ", response, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			return response;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject deleteruntime(Setupexec exedetail, JSONObject req, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside deleteruntime ", null, null, "info");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			HttpUtility.udid=exedetail.getDevicesID();
			return new JSONObject(
					HttpUtility.SendPost(exedetail.getServerIp() + "/v1/deleterp", req.toString(), header));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void savetestcase_result(Setupexec exedetial, Integer itr, String status, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside savetestcase result ", null, null, "info");
			logger.info("save testcase called");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			HttpUtility.udid=exedetial.getDevicesID();
			StartExecution start = new StartExecution();
			start.setCustomerId(exedetial.getCustomerID());
			start.setExecutionId(
					exedetial.getExecutionPlanScenario() ? exedetial.getExecplanexecId() : exedetial.getExecutionID());
			start.setProjectId(exedetial.getProjectID());
			start.setItr(itr + 1);
			start.setTcSeq(exedetial.getTcSeq());
			start.setTestcaseCode(exedetial.getTestcaseCode());
			start.setTestcaseId(exedetial.getTestcaseID());
			start.setVersion(exedetial.getVersion());
			start.setResultTc(status);
			start.setSuiteId(exedetial.getSuiteID());
			start.setExecutionPlanScenario(exedetial.getExecutionPlanScenario());
			start.setScenarioFolderId(exedetial.getScenarioFolderId());
			start.setExpPlnnedDate(exedetial.getExpPlnnedDate());
			start.setExecplanId(exedetial.getExecplanId());
			start.setExpAssignedTo(exedetial.getExpAssignedTo());
			start.setExecplanexecId(exedetial.getExecplanexecId());
			for (int i = 0; i < exedetial.getSuiteIds().size(); i++) {
				if (exedetial.getSuiteIds().get(i).getSuiteId().equals(exedetial.getSuiteID()))
					start.setSeq(exedetial.getSuiteIds().get(i).getSeq());
			}
			String resul=HttpUtility.sendPost(exedetial.getServerIp() + UtilEnum.SAVETCRESULT.value(),
					new ObjectMapper().writeValueAsString(start), header).toString();
			
			APILogger.customloggAPI(APIlogger, "TESTCASE RESPONSE ",resul, null, "info");
			
		} catch (IOException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.error("error", e);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public JSONObject getCurrentProjectDetail(JSONObject req, HashMap<String, String> header) {
		try {
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			APILogger.customloggAPI(APIlogger, "inside getCurrentProjectDetail ", null, null, "info");
			InitializeDependence.api_recount=0;
			InitializeDependence.check_api_count=0;
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			SearchColumns searchcolumns = new SearchColumns();
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.ID.value(),
							req.getInt(UtilEnum.PROJECTID.value()), Boolean.parseBoolean(UtilEnum.FALSE.value()),
							UtilEnum.INTEGER.value()));
			
			searchcolumns.setStartIndex(0);
			searchcolumns.setLimit(100000);
			searchcolumns.setCollection(UtilEnum.PROJECT.value());
//			searchcolumns.setCollection(UtilEnum.EXECUTION.value());
			
			header.remove("authorization");
			header.put("authorization","XPrsCRiuDE0HlurNLjjo1dcthtkF7shuAwWu6NyMj2H2NllhMHlMyfzdSfrflZvVvqAXCBBl5BnlTnO0B7mjKodULbIQFUQtHc6GUto9GCJd0jJNi4DGtZFT");
			
			if(InitializeDependence.ce_ref_play) {
				response = new JSONObject(
						HttpUtility.SendPost(req.getString("executionIp") + UtilEnum.SEARCH.value(),
								new ObjectMapper().writeValueAsString(searchcolumns), header));
	
			}
			else {
			response = new JSONObject(
					HttpUtility.SendPost(req.getString(UtilEnum.SERVERIP.value()) + UtilEnum.SEARCH.value(),
							new ObjectMapper().writeValueAsString(searchcolumns), header));
			}
			
			
			// project wise jar generation
			if(!getCustomerLevelflag(req,header)) {
			InitializeDependence.current_ce_project = response.getJSONObject("data").getJSONArray("data").getJSONObject(0).get("name").toString().replace(" ","");
			if(CodeEditorDebugger.FileUpdate(InitializeDependence.current_ce_project)) {
				Thread.sleep(1500);
				logger.info("Jar successfully updated for project :" + InitializeDependence.current_ce_project);
			}
			
			else {
				logger.info("Jar file already updated for current project");
			}
			
//			logger.info("Execution detail for executionId {}", req.getInt(UtilEnum.EXECUTIONID.value()));
			logger.info("executing with data" + response.toString());
			}
			
			
			// customer wise jar generation
			else {
				
				if(CodeEditorDebugger.FileUpdate(req.get(UtilEnum.CUSTOMERID.value()).toString())) {
					logger.info("Jar successfully updated for project :" + InitializeDependence.current_ce_project);
				}
				
				else {
					logger.info("Jar file already updated for current project");
				}
				
//				logger.info("Execution detail for executionId {}", req.getInt(UtilEnum.EXECUTIONID.value()));
				logger.info("executing with data" + response.toString());
				
			}
			APILogger.customloggAPI(APIlogger, "Response - ", response, null, "info");
			APILogger.customloggAPI(APIlogger, "", null, "tc done", "info");
			return response;
		} catch (Exception e) {
			
			logger.info("Unable to get Execution Detail {}", e);
			return null;
		}

	}
	
	public boolean getCustomerLevelflag(JSONObject req, HashMap<String, String> header) {
		
		try {
		JSONObject response;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		SearchColumns searchcolumns = new SearchColumns();
		searchcolumns.getSearchColumns()
				.add(new searchColumnDTO<Integer>(UtilEnum.ID.value(),
						req.getInt(UtilEnum.CUSTOMERID.value()), Boolean.parseBoolean(UtilEnum.FALSE.value()),
						UtilEnum.INTEGER.value()));
		
		searchcolumns.setStartIndex(0);
		searchcolumns.setLimit(100000);
		searchcolumns.setCollection(UtilEnum.CUSTOMER.value());
		
		header.remove("authorization");
		header.put("authorization","XPrsCRiuDE0HlurNLjjo1dcthtkF7shuAwWu6NyMj2H2NllhMHlMyfzdSfrflZvVvqAXCBBl5BnlTnO0B7mjKodULbIQFUQtHc6GUto9GCJd0jJNi4DGtZFT");
		
		if(InitializeDependence.ce_ref_play) {
			response = new JSONObject(
					HttpUtility.SendPost(req.getString("executionIp") + UtilEnum.SEARCH.value(),
							new ObjectMapper().writeValueAsString(searchcolumns), header));

		}
		else {
		response = new JSONObject(
				HttpUtility.SendPost(req.getString(UtilEnum.SERVERIP.value()) + UtilEnum.SEARCH.value(),
						new ObjectMapper().writeValueAsString(searchcolumns), header));
		}
		
		String resp  = response.getJSONObject("data").getJSONArray("data").getJSONObject(0).get("codeEditorCustomerLevel").toString();
		
		if(resp=="true") {
			return true;
			
			
		}
		else {
			return false;
		}
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
	} 

	
}
