package com.simplifyqa.execution;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Testcase;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.MainFramePlayBack;
import com.simplifyqa.execution.Impl.DesktopExecutionImpl;
import com.simplifyqa.execution.Impl.Executionimpl;
import com.simplifyqa.execution.Impl.MobileExecutorImpl;
import com.simplifyqa.execution.Impl.SapExecutionImpl;
import com.simplifyqa.execution.Impl.WebExecutorImpl;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mobile.android.service;
import com.simplifyqa.mobile.ios.Idevice;

public class Startexecution {
	private Setupexec setting = new Setupexec();
	private HashMap<String, String> header = new HashMap<String, String>();
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(Startexecution.class);
	LoggerClass user_logs = new LoggerClass();
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	public void execute() throws Exception {
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		System.gc();
		runtime.freeMemory();
		if (setting.getDevicesID() != null) {
			service.g().e().submit(() -> {
				new AutomationExecuiton(setting, header, setting.getDevicesID());
				return true;
			});
		} else if (setting.getTcType() != null && setting.getTcType().toLowerCase().equals("hybrid")) {
			service.g().e().submit(() -> {
				new AutomationExecuiton(setting, header);
				return true;
			});
		} else {
			service.g().e().submit(() -> {
				new AutomationExecuiton(setting, header);
				return true;
			});
		}

	}

	public void Setexecution(JSONObject requestbody)
			throws JsonParseException, JsonMappingException, IOException, InterruptedException, ExecutionException {
		ObjectMapper mapper = new ObjectMapper();
		GlobalDetail.refdetails.put("authkey", requestbody.getString("authKey"));
		GlobalDetail.refdetails.put("customerId", requestbody.get("customerID").toString());
		GlobalDetail.refdetails.put("executionIp",
				requestbody.has("serverIp") ? requestbody.getString("serverIp") : requestbody.getString("executionIp"));

		GlobalDetail.refdetails.put("projectId", requestbody.get("projectID").toString());
		mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		this.setting = mapper.readValue(requestbody.toString(), Setupexec.class);
		System.out.println(mapper.writeValueAsString(setting));
		header.put("content-type", "application/json");
		header.put("authorization", setting.getAuthKey());
//		if (GlobalDetail.runTime.containsKey(setting.getBrowserName().toLowerCase())) {
//			GlobalDetail.runTime.remove(setting.getBrowserName().toLowerCase());
//		}
		startExecutionManager();

	}

//	public void startrefPlayback(JSONObject request) {
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			GlobalDetail.refdetails.put("authkey", request.getString("authKey"));
//			GlobalDetail.refdetails.put("customerId", request.get("customerId").toString());
//			GlobalDetail.refdetails.put("executionIp",
//					request.has("serverIp") ? request.getString("serverIp") : request.getString("executionIp"));
//
//			GlobalDetail.refdetails.put("projectId", request.get("projectId").toString());
//			InitializeDependence.seq = request.getInt("seq");
//			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
//			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//			Testcase testcase = mapper.readValue(request.getJSONObject("testcase").toString(), Testcase.class);
//			
//			user_logs.customlogg(userlogger,"----------------------------------------------------------------------------------------",
//					null, "tc done", "info");
//			
//			user_logs.customlogg(userlogger,"JAR VERSION : "+ InitializeDependence.jar_version +" (REFERENCE PLAYBACK)",null,"jar","info");
//			
//			user_logs.customlogg(userlogger,"----------------------------------------------------------------------------------------",
//					null, "tc done", "info");
//			
//			switch (request.getString("type").toLowerCase()) {
//			case "web":
//				service.g().e().submit(() -> {
//					System.out.println(request.getString("browser"));
//					new AutomationExecuiton(testcase, new WebExecutorImpl(request.getString("browser")));
//					return true;
//				});
//				break;
//			case "mainframe":
//				service.g().e().submit(() -> {
//					new AutomationExecuiton(testcase, new Executionimpl());
//					return true;
//				});
//				break;
//			case "desktop":
//				service.g().e().submit(() -> {
//					new AutomationExecuiton(testcase, new DesktopExecutionImpl());
//					return true;
//				});
//				break;
//			case "sap":
//				service.g().e().submit(() -> {
//					new AutomationExecuiton(testcase, new SapExecutionImpl());
//					return true;
//				});
//				break;
//
//			case "mobile":
//				service.g().e().submit(() -> {
//					new AutomationExecuiton(testcase, new MobileExecutorImpl());
//					return true;
//				});
//				break;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.info(e.getMessage());
//		}
//	}

	private void startExecutionManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ExecutionService.a().run().get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

//	public static void main(String[] args) throws Exception {
////		String s = "{customerID:3356, projectID:8148, executionID:66488,authKey:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmVjMWNhZjkwYmYwMDJiZmFmZDBjYTYiLCJ1c2VydHlwZSI6Im5vbi1hZG1pbiIsInByb2plY3RzIjpbODI2Nyw4MTY2LDgzMDgsODIyOSw4MTc2LDgzMDksODI0OCw4MzI0LDgzMDMsODE0OCw4NDc2LDg0NjYsODQ1NCw4MzMyLDg0OTksODQ2NSw4NTI4LDg1MzUsODQ2NCw4NTQ3LDg1MzQsODU2MSw4NTU2XSwidXNlcm5hbWUiOiJuaXJtYWwuc0BzaW1wbGlmeTN4LmNvbSIsIm5hbWUiOiJOaXJtYWwiLCJwcml2aWxlZ2VzIjpbInZpZXdfc3VpdGVzIiwiYWRkX3N1aXRlcyIsImVkaXRfc3VpdGVzIiwidmlld191c2Vyc3RvcnkiLCJlZGl0X3VzZXJzdG9yeSIsImFkZF91c2Vyc3RvcnkiLCJkZWxldGVfdXNlcnN0b3J5Iiwidmlld19kZWZlY3RzIiwiYWRkX2RlZmVjdHMiLCJlZGl0X2RlZmVjdHMiLCJ2aWV3X3Byb2plY3QiLCJ2aWV3X21vZHVsZSIsImFkZF9tb2R1bGUiLCJlZGl0X21vZHVsZSIsImRlbGV0ZV9tb2R1bGUiLCJ2aWV3X2NvbmZpZyIsImFkZF9jb25maWciLCJlZGl0X2NvbmZpZyIsImRlbGV0ZV9jb25maWciLCJ2aWV3X29iamVjdCIsImFkZF9vYmplY3QiLCJlZGl0X29iamVjdCIsImRlbGV0ZV9vYmplY3QiLCJ2aWV3X2Z1bmN0aW9uIiwiYWRkX2Z1bmN0aW9uIiwiZWRpdF9mdW5jdGlvbiIsImRlbGV0ZV9mdW5jdGlvbiIsInZpZXdfcGFyYW1ldGVyIiwiYWRkX3BhcmFtZXRlciIsImVkaXRfcGFyYW1ldGVyIiwiZGVsZXRlX3BhcmFtZXRlciIsInZpZXdfdGVzdGNhc2UiLCJhZGRfdGVzdGNhc2UiLCJlZGl0X3Rlc3RjYXNlIiwiZGVsZXRlX3Rlc3RjYXNlIiwidmlld190ZXN0ZGF0YSIsImFkZF90ZXN0ZGF0YSIsImVkaXRfdGVzdGRhdGEiLCJ2aWV3X2luaXRpYXRpdmUiLCJ2aWV3X2ZlYXR1cmUiLCJlZGl0X2ZlYXR1cmUiLCJkZWxldGVfZmVhdHVyZSIsImFkZF9mZWF0dXJlIiwidmlld190YXNrIiwiYWRkX3Rhc2siLCJlZGl0X3Rhc2siLCJkZWxldGVfdGFzayIsImVkaXRfaW5pdGlhdGl2ZSIsImRlbGV0ZV9pbml0aWF0aXZlIiwiYWRkX2luaXRpYXRpdmUiLCJ2aWV3X3FhZGFzaGJvYXJkIiwidmlld19hbmFseXRpY3MiLCJ2aWV3X3JlcG9ydCIsImRlbGV0ZV9zdWl0ZXMiLCJ2aWV3X2ltcGFjdGFuYWx5c2lzIiwidmlld19zY2hlZHVsZXIiLCJlZGl0X3NjaGVkdWxlciIsImFkZF9zY2hlZHVsZXIiLCJjYW5jZWxfc2NoZWR1bGVyIiwiamFyX3VwbG9hZCIsImxpdGVfZXhlY3V0aW9uIiwibW9iaWxlX2F1dG9tYXRpb24iLCJhcHByb3ZlciIsInJldmlld2VyIiwiaHlicmlkX2V4ZWN1dGlvbiIsIm1haW5mcmFtZV9hdXRvbWF0aW9uIiwidmlld19kb3dubG9hZHMiLCJkZXNrdG9wX2F1dG9tYXRpb24iLCJ2aWV3X2N1c3RvbW1ldGhvZHdlYiIsInZpZXdfY3VzdG9tbWV0aG9kYW5kcm9pZCIsInZpZXdfY3VzdG9tbWV0aG9kaW9zIiwidmlld19jdXN0b21tZXRob2RtYWluZnJhbWUiLCJ3ZWJfcmVjb3JkYW5kcGxheWJhY2siLCJtb2JpbGVfaW5zcGVjdG9yIiwibWFpbmZyYW1lX2luc3BlY3RvciIsIndlYl9pbnNwZWN0b3IiLCJyZW1vdGVfZXhlY3V0aW9uIiwidmlld19leGVjdXRpb25wbGFuIiwiYWRkX2V4ZWN1dGlvbnBsYW4iLCJlZGl0X2V4ZWN1dGlvbnBsYW4iLCJkZWxldGVfZXhlY3V0aW9ucGxhbiIsImV4ZWN1dGVfZXhlY3V0aW9ucGxhbiIsImxvY2FsX2V4ZWN1dGlvbiIsIm1hbnVhbF9leGVjdXRpb24iLCJ2aWV3X3JlbGVhc2VwbGFuIiwidmlld19teWJvYXJkIiwidmlld19kYXRhZ2VuZXJhdGlvbiIsInZpZXdfY3VzdG9tRWRpdG9yIiwicGFyYWxsZWxfcmVtb3RlX2V4ZWN1dGlvbiIsInZpZXdfdXNlcmFuYWx5dGljcyIsInZpZXdfcnVudGltZXBhcmFtZXRlcnMiLCJkZWxldGVfcnVudGltZXBhcmFtZXRlcnMiLCJ2aWV3X2RhdGF1cGxvYWQiLCJ2aWV3X2NvbXBhcmlzb24iLCJhbGxvd191cGxvYWRfY29tcGFyaXNpb24iLCJ2aWV3X2VyYXZhbGlkYXRpb24iXSwiZGFzaGJvYXJkIjoiUFJPSkVDVCIsInJvbGVJZCI6MTAyLCJlbWFpbCI6Im5pcm1hbC5zQHNpbXBsaWZ5M3guY29tIiwiY3VzdG9tZXJJZCI6MzM1NiwidXBkYXRlZEJ5Ijo0NTEyLCJjcmVhdGVkT24iOiIyMDE4LTExLTE0VDEzOjAxOjM1Ljc4MFoiLCJ1cGRhdGVkT24iOiIyMDIwLTA5LTA3VDEwOjE5OjEwLjgyM1oiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjUxMjgsImNvZGUiOiJVUi0zMzU2NTEyOCIsImdpdGxhYiI6e30sInR5cGUiOiJ3ZWIiLCJpc0FnaWxlIjp0cnVlLCJza0VuYWJsZWQiOnRydWUsImxhYmVscyI6eyJpbml0aWF0aXZlIjoiSW5pdGlhdGl2ZSIsInVzZXJzdG9yeSI6IlVzZXIgU3RvcnkiLCJpdGVyYXRpb24iOiJJdGVyYXRpb24iLCJhY3Rpb24iOiJBY3Rpb24iLCJicmFuY2giOiJCcmFuY2giLCJjbGllbnQiOiJDbGllbnQiLCJkZWZlY3QiOiJEZWZlY3QiLCJmZWF0dXJlIjoiRmVhdHVyZSIsImZ1bmN0aW9uIjoiRnVuY3Rpb24iLCJtb2R1bGUiOiJNb2R1bGUiLCJvYmplY3R0ZW1wbGF0ZSI6Ik9iamVjdCBUZW1wbGF0ZSIsIm9iamVjdCI6Ik9iamVjdCIsInBhcmFtZXRlciI6IlBhcmFtZXRlciIsInByb2plY3QiOiJQcm9qZWN0IiwicmVsZWFzZSI6IlJlbGVhc2UiLCJyb2xlIjoiUm9sZSIsInN1aXRlcyI6IlN1aXRlcyIsInRhc2siOiJUYXNrIiwidGVzdGNhc2UiOiJUZXN0IENhc2UiLCJ0ZXN0ZGF0YSI6IlRlc3QgRGF0YSIsInVzZXIiOiJVc2VyIn0sImlhdCI6MTYwMjU3MjIyMCwiZXhwIjoxNjAyNjU4NjIwfQ.SFU_FESYTuzujbXcPc-FwZJOXr-YG3stkDZXPSJ8ac8, serverIp:https://qa.simplifyqa.com};";
//
//		JSONObject test = new JSONObject();
//		test.put("customerID", 3356);
//		test.put("projectID", 8148);
//		test.put("executionID", 66488);
//		test.put("authKey",
//				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjozMzU2LCJjcmVhdGVkQnkiOjUwODgsInByb2plY3RJZCI6ODE0OCwiYXV0b21hdGlvbiI6dHJ1ZSwicmVsZWFzZUlkIjoxNjcsIml0ZXJhdGlvbklkIjo0NDAsInN0YXR1cyI6IklOUFJPR1JFU1MiLCJyZW1vdGVFeGVjdXRpb24iOnRydWUsInJlbW90ZSI6W3siaXAiOiJyZHAyLnNpbXBsaWZ5cWEuY29tIiwidXNlciI6InJvb3QiLCJwd2QiOiJzaW1xYXJkcGF0M3giLCJpbWFnZSI6InNpbXBxYTIuMiIsImNvbnRyb2xsZXJQb3J0Ijo0NDEzLCJ2bmNQb3J0Ijo2MzI3LCJkb2NrZXJuYW1lIjoibTBvY2o1dW5kZWZpbmVkIiwiYmluTG9jYXRpb24iOiIvcm9vdC9iaW5hcmllcy9xYWVudi9qYXJzIiwiY2VydExvY2F0aW9uIjoiL3Jvb3Qvc3NsIiwiaHR0cFR5cGUiOiJodHRwczovLyIsIm1heENvbnRhaW5lckNvdW50IjoxNSwidXJsIjoiaHR0cHM6Ly9yZHAyLnNpbXBsaWZ5cWEuY29tOjYzMjcvdm5jLmh0bWw_cGFzc3dvcmQ9dGVzdDEyJmF1dG9jb25uZWN0PXRydWUifV0sImV4ZWN1dGlvbkNvZGUiOiJFWC0zMzU2NjY0ODIiLCJleGVjcGxhbklkIjo3ODAsImVudmlyb25tZW50VHlwZSI6ImdlbmVyYWwiLCJzdWl0ZUlkcyI6W3siZXhlY3BsYW5TY2VuYXJpb0lkIjoiNWY4NDFhZWIxMzA2YjE4NmUxNjI0MDU4Iiwic3VpdGVJZCI6MjEyMiwic2VxIjoxfSx7ImV4ZWNwbGFuU2NlbmFyaW9JZCI6IjVmODQxYWViMTMwNmIxODZlMTYyNDA1OSIsInN1aXRlSWQiOjIyMDIsInNlcSI6Mn1dLCJzY2VuYXJpb0ZvbGRlcklkIjo5OCwiZXhlY3V0aW9uUGxhblNjZW5hcmlvIjp0cnVlLCJleHBBc3NpZ25lZFRvIjo1MDg4LCJleHBQbG5uZWREYXRlIjoiMjAyMC0xMC0xMlQwMDowMDowMFoiLCJleGVjcGxhbmV4ZWNJZCI6NjY0ODIsInR5cGUiOiJleGVjdXRpb24iLCJkZXZpY2VzIjpbIkNocm9tZSJdLCJpYXQiOjE2MDI1NzE1NTUsImV4cCI6MTYwMjY1Nzk1NX0.sH4KFUMdqyv57NAmaM2yWVvHoFNqERh8WGhRT8-6le8");
//		test.put("serverIp", "http://localhost:4011");
//
//		try {
//			Startexecution ss = new Startexecution();
//			ss.Setexecution(test);
//			ss.execute();
//		} catch (IOException | InterruptedException | ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
