package com.simplifyqa.execution.Interface;

import java.util.HashMap;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;

public interface ExecutionUtil {

	public JSONObject getExecutionDetail(JSONObject req, HashMap<String, String> header);

	public Testcase getTCstep(Setupexec exedetail, HashMap<String, String> header);

	public void postStepsRc(Setupexec exeDetial, HashMap<String, String> header, Integer itr, String[] tcData,
			String status, String screenshot, Step step);

	public void postsfITRexecution(Setupexec exedetial, Integer itr, String status, HashMap<String, String> header);

	public void savetestcase_result(Setupexec exedetial, Integer itr, String status, HashMap<String, String> header);

	public void postFinishExecution(Setupexec exedetial, HashMap<String, String> header);

	public Testcase getFNstep(Setupexec exedetail, Step fnStep, JsonNode tcData, HashMap<String, String> header);

	public void postlogFile(JSONObject req, HashMap<String, String> header);

	public void postruntimeParameter(Setupexec exedetail, JSONObject param, HashMap<String, String> header);

	public JSONObject getruntimeParameter(Setupexec exedetail, String paramName, HashMap<String, String> header);

	public JSONObject getScenarioExecTcs(Setupexec exedetail, HashMap<String, String> header);

	public JSONObject deleteruntime(Setupexec exedetail, JSONObject req, HashMap<String, String> header);
	
	public JSONObject getCurrentProjectDetail(JSONObject req, HashMap<String, String> header);

}
