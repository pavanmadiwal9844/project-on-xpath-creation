package com.simplifyqa.execution.Interface;

import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.Testcase;

public interface Executor {

	public Boolean stepExecution(Setupexec executionDetail, Step tcfnObject, JsonNode tcData, Integer itr,
			HashMap<String, String> header, String sessionID);

	public Testcase exeIfEvaluation(Testcase tcfnIFeval);

	public void stopExecution(Setupexec executionDetail, HashMap<String, String> header, Boolean hybridExecution);
}
