package com.simplifyqa.execution;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.ios.Timer;

import io.appium.java_client.AppiumDriver;

public class Playback<T> implements Runnable {

	private String deviceId;
	private JSONArray steps;
	private Boolean start;
	private static final Logger logger = LoggerFactory.getLogger(Playback.class);
	private T driverAction;
	private String mbType = "";
	private GeneralMethod atAction = new GeneralMethod();

	@SuppressWarnings("unchecked")
	public boolean setup(String device, JSONArray steps, Boolean start, String mbType)
			throws InstallException, IOException, TimeoutException, AdbCommandRejectedException,
			ShellCommandUnresponsiveException, InterruptedException {
		this.deviceId = device;
		this.steps = steps;
		this.start = start;
		this.mbType = mbType;
		Thread.currentThread().setName(deviceId);
		if (mbType.equals("android")) {
			Thread.sleep(1000);
			this.driverAction = (T) new AndroidMethod();
			return ((AndroidMethod) this.driverAction).SetUiautomor2(deviceId, true);
		} else {
			driverAction = ((T) new IosMethod());
			((IosMethod) driverAction).setIdeviceId(deviceId);
			return true;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public void init() throws InterruptedException, InstallException, IOException, ClassNotFoundException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException,
			NoSuchMethodException, SecurityException, JSONException, TimeoutException, AdbCommandRejectedException,
			ShellCommandUnresponsiveException, SyncException {
		if (!mbType.equals("ios")) {
			if (!GlobalDetail.hostname.isEmpty()) {
				((AndroidMethod) driverAction).hostname = GlobalDetail.hostname.get(deviceId);
				((AndroidMethod) driverAction).startapp(steps.getJSONObject(0).getString("package"),
						steps.getJSONObject(0).getString("activity"));
			} else {
				((AndroidMethod) driverAction).SetUiautomor2(deviceId, false);
				Thread.sleep(5000);
				((AndroidMethod) driverAction).resetapp(steps.getJSONObject(0).getString("package"),
						steps.getJSONObject(0).getString("activity"));
			}
		} else {
			((IosMethod) driverAction).launch_app(steps.getJSONObject(0).getString("bundleId"));
		}
		Thread.sleep(1000);
		Boolean flag = false;
		for (int i = 1; i < steps.length(); i++) {
			JSONObject step = steps.getJSONObject(i);
			int Mobile_width = -1, Mobile_height = -1, startX = -1, startY = -1, endX, endY = -1, timeout = 1000,
					x = -1, y = -1;
			if (flag) {
				break;
			}
			switch (step.getString("actionname").toLowerCase()) {
			case "click":
				if (!mbType.equals("ios")) {
					if (step.has("xpath")) {
						logger.info(step.getString("xpath"));
						if (((AndroidMethod) driverAction).Tap("xpath", step.getString("xpath")))

							logger.info("Executed by xpath :{}", i + 1);
					} else {
						if (step.has("x") && step.has("y")) {
							logger.info("Error unable to find Element by xpath . Trying by coordinate:{}", i + 1);
							Mobile_width = ((AndroidMethod) driverAction).width();
							Mobile_height = ((AndroidMethod) driverAction).height();
							x = (int) ((step.getDouble("x") * Mobile_width) / 100);
							y = (int) ((step.getDouble("y") * Mobile_height) / 100);
							if (((AndroidMethod) driverAction).TapByAdb(x, y)) {
								logger.info("Executed by coordinate:{}", i + 1);
							} else {
								logger.info("Error unable to click by coordinate:{}", i + 1);
								flag = true;
							}
						} else {
							logger.info("Error unable by xpath :{}", i + 1);
							flag = true;
						}
					}
				} else {
					if (step.has("xpath")) {
						if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath")))

							if (((IosMethod) driverAction).Tap()) {
								logger.info("Executed by xpath :{}", i + 1);
								logger.info(step.getString("xpath"));
							} else {
								if (step.has("x") && step.has("y")) {
									logger.info("Error unable to find Element by xpath . Trying by coordinate:{}",
											i + 1);
									Mobile_width = ((IosMethod) driverAction).width();
									Mobile_height = ((IosMethod) driverAction).height();
									x = (int) ((step.getDouble("x") * Mobile_width) / 100);
									y = (int) ((step.getDouble("y") * Mobile_height) / 100);
									if (((IosMethod) driverAction).Tap(x, y)) {
										logger.info("Executed by coordinate:{}", i + 1);
									} else {
										logger.info("Error unable to click by coordinate:{}", i + 1);
										flag = true;
									}
								} else {
									logger.info("Error unable by xpath :{}", i + 1);
									flag = true;
								}
							}
					} else {
						if (step.has("x") && step.has("y")) {
							logger.info("Error unable to find Element by xpath . Trying by coordinate:{}", i + 1);
							Mobile_width = ((AndroidMethod) driverAction).width();
							Mobile_height = ((AndroidMethod) driverAction).height();
							x = (int) ((step.getDouble("x") * Mobile_width) / 100);
							y = (int) ((step.getDouble("y") * Mobile_height) / 100);
							if (((AndroidMethod) driverAction).TapByAdb(x, y)) {
								logger.info("Executed by coordinate:{}", i + 1);
							} else {
								logger.info("Error unable to click by coordinate:{}", i + 1);
								flag = true;
							}
						} else {
							logger.info("Error unable by xpath :{}", i + 1);
							flag = true;
						}
					}
				}
				break;

			case "swipeevent":
				if (!this.mbType.equals("ios")) {
					Mobile_width = ((AndroidMethod) driverAction).width();
					Mobile_height = ((AndroidMethod) driverAction).height();
					startX = (int) (Math.round(step.getDouble("x1") * Mobile_width) / 100);
					startY = (int) (Math.round(step.getDouble("y1") * Mobile_height) / 100);
					endX = (int) (Math.round(step.getDouble("x2") * Mobile_width) / 100);
					endY = (int) (Math.round(step.getDouble("y2") * Mobile_height) / 100);
					timeout = (int) (double) Math.round(step.getDouble("timeout"));
					Thread.sleep(2000);
					if (Mobile_height != -1 && Mobile_width != -1 && startX != -1 && startY != -1 && endX != -1
							&& endY != -1)
						if (((AndroidMethod) driverAction).swipe(startX, startY, endX, endY, timeout))
							logger.info("Executed {} {} {} {} {},:{}", startX, startY, endX, endY, timeout, i + 1);
						else
							logger.info("Error {} {} {} {} {}:{}", startX, startY, endX, endY, timeout, i + 1);

					else
						logger.info("Error unable to do swipe:{}", i + 1);
				} else {
					Mobile_width = ((IosMethod) driverAction).width();
					Mobile_height = ((IosMethod) driverAction).height();
					startX = (int) (Math.round(step.getDouble("x1") * Mobile_width) / 100);
					startY = (int) (Math.round(step.getDouble("y1") * Mobile_height) / 100);
					endX = (int) (Math.round(step.getDouble("x2") * Mobile_width) / 100);
					endY = (int) (Math.round(step.getDouble("y2") * Mobile_height) / 100);
					timeout = (int) (double) Math.round(step.getDouble("timeout"));
					Thread.sleep(2000);
					if (Mobile_height != -1 && Mobile_width != -1 && startX != -1 && startY != -1 && endX != -1
							&& endY != -1)
						if (((IosMethod) driverAction).swipe(startX, startY, endX, endY, timeout))
							logger.info("Executed {} {} {} {} {},:{}", startX, startY, endX, endY, timeout, i + 1);
						else
							logger.info("Error {} {} {} {} {}:{}", startX, startY, endX, endY, timeout, i + 1);

					else
						logger.info("Error unable to do swipe:{}", i + 1);
				}

				break;
			case "scrolltillelementverticalup":
				if (!this.mbType.equals("ios")) {
					if (step.has("x")) {
						if (((AndroidMethod) driverAction).scrollTillElementVertical("xpath", step.getString("xpath"),
								false, (int) Math.round(step.getDouble("x"))))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						if (((AndroidMethod) driverAction).scrollTillElementVertical("xpath", step.getString("xpath"),
								false))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "scrolltillelementverticaldown":
				if (!this.mbType.equals("ios")) {
					if (step.has("x")) {
						if (((AndroidMethod) driverAction).scrollTillElementVertical("xpath", step.getString("xpath"),
								true, (int) Math.round(step.getDouble("x"))))
							logger.info("Executed by xpath");
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						if (((AndroidMethod) driverAction).scrollTillElementVertical("xpath", step.getString("xpath"),
								true))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "scrolltillelementhorizontalleft":
				if (!this.mbType.equals("ios")) {
					if (step.has("y")) {
						if (((AndroidMethod) driverAction).scrollTillElementHorizontal("xpath", step.getString("xpath"),
								true, (int) Math.round(step.getDouble("y"))))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						if (((AndroidMethod) driverAction).scrollTillElementHorizontal("xpath", step.getString("xpath"),
								true))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;

			case "scrolltillelementhorizontalright":
				if (!this.mbType.equals("ios")) {
					if (step.has("y")) {
						if (((AndroidMethod) driverAction).scrollTillElementHorizontal("xpath", step.getString("xpath"),
								false, (int) Math.round(step.getDouble("y"))))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						if (((AndroidMethod) driverAction).scrollTillElementHorizontal("xpath", step.getString("xpath"),
								false))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;

			case "holdandswipe":
				if (!this.mbType.equals("ios")) {
					Mobile_width = ((AndroidMethod) driverAction).width();
					Mobile_height = ((AndroidMethod) driverAction).height();
					startX = (int) Math.round(step.getInt("x1") * Mobile_width) / 100;
					startY = (int) Math.round(step.getInt("y1") * Mobile_height) / 100;
					endX = (int) Math.round(step.getInt("x2") * Mobile_width) / 100;
					endY = (int) Math.round(step.getInt("y2") * Mobile_height) / 100;

					if (Mobile_height != -1 && Mobile_width != -1 && startX != -1 && startY != -1 && endX != -1
							&& endY != -1)
						if (((AndroidMethod) driverAction).holdelement(startX, startY, endX, endY))
							logger.info("Executed {} {} {} {} {}:{}", startX, startY, endX, endY, i + 1);
						else
							logger.info("Error {} {} {} {} {}:{}", startX, startY, endX, endY, i + 1);

					else
						logger.info("Error unable to do hold:{}", i + 1);
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "longpress":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).LongPress("xpath", step.getString("xpath")))
						logger.info("Executed by xpath:{}", i + 1);
					else
						logger.info("Error:{}", i + 1);
				} else {
					if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath"))) {
						if (((IosMethod) driverAction).Longpress())
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						logger.info("Error unable to find Element by xpath:{}", i + 1);
					}

				}
				break;

			case "entertext":
				if (!this.mbType.equals("ios")) {
					if (step.has("xpath")) {
						if (((AndroidMethod) driverAction).Sendkeys("xpath", step.getString("xpath"),
								step.getString("value")))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						if (((AndroidMethod) driverAction).sendkeysByOther(step.getString("value")))
							logger.info("Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				} else {
					if (step.has("xpath")) {
						if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath"))) {
							if (((IosMethod) driverAction).EnterText(step.getString("value")))
								logger.info("Executed by xpath:{}", i + 1);
							else
								logger.info("Error unable to find Element by xpath:{}", i + 1);
						} else {
							logger.info("Error unable to find Element by xpath:{}", i + 1);
						}
					} else {
						logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				}
				break;
			case "enter":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).enter())
						logger.info("Executed Enter:{}", i + 1);
					else
						logger.info("Error unable to Enter:{}", i + 1);
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "emulatorevent":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).emulatorevent(step.getString("value")))
						logger.info("Executed EmulatorEvent:{}", i + 1);
					else
						logger.info("Error in EmulatorEvent:{}", i + 1);
				} else {
					if (step.getString("value").toLowerCase().equals("ioshome")) {
						if (((IosMethod) driverAction).Home())
							logger.info("Executed EmulatorEvent:{}", i + 1);
						else
							logger.info("Error in EmulatorEvent:{}", i + 1);
					} else {
						logger.info("Executed EmulatorEvent:{}", i + 1);
					}
				}
				break;

			case "exists":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).exists("xpath", step.getString("xpath")))
						logger.info("Executed by xpath:{}", i + 1);
					else
						logger.info("Error unable to find Element by xpath:{}", i + 1);
				} else {
					if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath")))
						logger.info("Executed by xpath:{}", i + 1);
					else
						logger.info("Error unable to find Element by xpath:{}", i + 1);
				}
				break;
			case "validatetext":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).validatetext("xpath", step.getString("xpath"),
							step.getString("value")))
						logger.info(" Executed by xpath:{}", i + 1);
					else
						logger.info("Error unable to find Element by xpath:{}", i + 1);
				} else {
					if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath"))) {
						if (((IosMethod) driverAction).validateText(step.getString("value")))
							logger.info(" Executed by xpath:{}", i + 1);
						else
							logger.info("Error unable to find Element by xpath:{}", i + 1);
					} else {
						logger.info("Error unable to find Element by xpath:{}", i + 1);
					}
				}
				break;
			case "notexists":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).exists("xpath", step.getString("xpath")))
						logger.info("Error unable to find Element by xpath:{}", i + 1);
					else
						logger.info("Executed by xpath:{}", i + 1);
				} else {
					if (new Timer().until(((IosMethod) driverAction), "xpath", step.getString("xpath")))
						logger.info("Error unable to find Element by xpath:{}", i + 1);
					else
						logger.info("Executed by xpath:{}", i + 1);
				}
				break;
			case "scrolldown":
				if (!this.mbType.equals("ios")) {
					if (step.has("x")) {
						((AndroidMethod) driverAction).scrolldown((int) Math.round(step.getDouble("x")));
						logger.info("Scrolled Down by x coordinate:{} :{}", step.getInt("x"), i + 1);
					}

					else {
						((AndroidMethod) driverAction).scrollDown();
						logger.info("Scrlled Down:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);

				}
				break;
			case "scrollup":
				if (!this.mbType.equals("ios")) {
					if (step.has("x")) {
						((AndroidMethod) driverAction).scrollup((int) Math.round(step.getDouble("x")));
						logger.info("Scrolled Up by x coordinate:{}:{}", step.getInt("x"), i + 1);
					}

					else {
						((AndroidMethod) driverAction).scrollUp();
						logger.info("Scrlled Up:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "scrollright":
				if (!this.mbType.equals("ios"))
					if (step.has("y")) {
						((AndroidMethod) driverAction).scrollRight((int) Math.round(step.getDouble("y")));
						logger.info("Scrolled Right by y coordinate:{}:{}", step.getInt("y"), i + 1);
					}

					else {
						((AndroidMethod) driverAction).scrollRight();
						logger.info("Scrlled Right:{}", i + 1);
					}
				else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "scrollleft":
				if (!this.mbType.equals("ios")) {
					if (step.has("y")) {
						((AndroidMethod) driverAction).scrollLeft((int) Math.round(step.getDouble("y")));
						logger.info("Scrolled Left by y coordinate:{}:{}", step.getInt("y"), i + 1);
					}

					else {
						((AndroidMethod) driverAction).scrollLeft();
						logger.info("Scrlled Left:{}", i + 1);
					}
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "mbwifitoggle":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).wifiToggle(step.getBoolean("value")))
						logger.info("Executed wifi Toggle :{}", i + 1);
					else
						logger.info("Error in wifi Toggle:{}", i + 1);
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "mbdatatoggle":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).dataToggle(step.getBoolean("value")))
						logger.info("Executed data Toggle :{}", i + 1);
					else
						logger.info("Error in data Toggle:{}", i + 1);
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "mbbluetoothtoggle":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).bluetoothToggle(step.getBoolean("value")))
						logger.info("Executed  bluetooth Toggle :{}", i + 1);
					else
						logger.info("Error in bluetooth Toggle:{}", i + 1);
				} else {
					logger.info("Not implemented for Ios", i + 1);
				}
				break;
			case "mbscreenrotation":
				if (!this.mbType.equals("ios")) {
					if (((AndroidMethod) driverAction).mbscreenrotation(step.getString("value")))
						logger.info("Executed  mbscreenrotation :{}", i + 1);
					else
						logger.info("Error in mbscreenrotation:{}", i + 1);
				} else {
					if (((IosMethod) driverAction).setOrientation(step.getString("value")))
						logger.info("Executed  mbscreenrotation :{}", i + 1);
					else
						logger.info("Error in mbscreenrotation:{}", i + 1);
				}
				break;
			default:
//				if (!step.has("value")) {
//					if (atAction.CallMethod(step.getString("actionname"), null, null, null))
//						logger.info("Executed:{}", i + 1);
//					else
//						logger.info("Failed:{}", i + 1);
//				} else {
//
//				}

			}
		}
		if (flag) {
			if (!this.mbType.equals("ios"))
				((AppiumDriver) ((AndroidMethod) driverAction).getDriver()).closeApp();
			GlobalDetail.websocket.get(deviceId).getBasicRemote().sendText("off");
			logger.info("Failed to Execute Testcase");
//			GlobalDetail.hostname = null;
		} else {
			if (!this.mbType.equals("ios"))
				((AppiumDriver) ((AndroidMethod) driverAction).getDriver()).closeApp();
			GlobalDetail.websocket.get(deviceId).getBasicRemote().sendText("on");
			logger.info("Executed Testcase");
//			GlobalDetail.hostname = null;
		}

	}

	@Override
	public void run() {
		try {
			Thread.currentThread().setName(deviceId);
			Thread.sleep(1000);
			init();
		} catch (InstallException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException | NoSuchMethodException | SecurityException
				| JSONException | InterruptedException | IOException | TimeoutException | AdbCommandRejectedException
				| ShellCommandUnresponsiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SyncException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
