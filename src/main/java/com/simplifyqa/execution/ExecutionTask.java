package com.simplifyqa.execution;

import java.util.HashMap;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;

public class ExecutionTask {

	private Setupexec exeDetial;
	private HashMap<String, String> header;
	private Integer itr = null;
	private String []tcData;
	private String status;
	private String screenshot;
	private ExecutionOption task;
	private Step step;

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public String getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}

	public Setupexec getExeDetial() {
		return exeDetial;
	}

	public void setExeDetial(Setupexec exeDetial) {
		this.exeDetial = exeDetial;
	}

	public Integer getItr() {
		return itr;
	}

	public void setItr(Integer itr) {
		this.itr = itr;
	}

	public String []getTcData() {
		return tcData;
	}

	public void setTcData(String []tcData) {
		this.tcData = tcData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setHeader(HashMap<String, String> header) {
		this.header = header;
	}

	public HashMap<String, String> getHeader() {
		return header;
	}

	public ExecutionOption getTask() {
		return task;
	}

	public void setTask(ExecutionOption task) {
		this.task = task;
	}

	public ExecutionTask(Setupexec exeDetial, HashMap<String, String> header, Integer itr, String []tcData, String status,
			String screenshot, ExecutionOption task, Step step) {
		super();
		this.exeDetial = exeDetial;
		this.header = header;
		this.itr = itr;
		this.tcData = tcData;
		this.status = status;
		this.screenshot = screenshot;
		this.task = task;
		this.step = step;
	}


}
