package com.simplifyqa.mainframe.Impl;

import java.io.File;

import com.simplifyqa.mainframe.Interface.MainframeInterface;

public class MainframeImpl implements MainframeInterface{
	

	@Override
	public Process startRecorderExe(String recorderExepath, String dirPath) {
		try {
		return Runtime.getRuntime().exec(recorderExepath,null,new File(dirPath));
		}catch (Exception e) {
			return null;
		}
	}

	@Override
	public Process startPlaybackExe(String playbackExepath, String dirPath) {
		try {
			return Runtime.getRuntime().exec(playbackExepath,null,new File(dirPath));
			}catch (Exception e) {
				return null;
			}
	}

	@Override
	public Boolean stopRecorderExe(Process recorderProcess) {
		try {
			recorderProcess.destroyForcibly();
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	@Override
	public Boolean stopPlaybackExe(Process playbackProcess) {
		try {
			playbackProcess.destroyForcibly();
			return true;
		}catch (Exception e) {
			return false;
		}
	}

}
