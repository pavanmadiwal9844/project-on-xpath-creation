package com.simplifyqa.chromes;

import com.simplifyqa.driver.*;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.basic.BasicComboBoxUI.ListDataHandler;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.mysql.jdbc.Driver;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.CreateStep;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.web.DTO.Webcapabilities;

import netscape.javascript.JSObject;

import com.simplifyqa.agent.AgentStart;

public class Browsers {
	public static com.simplifyqa.driver.WebDriver driver = null;
	public static Logger logger = LoggerFactory.getLogger(CreateStep.class);

	private static String macBrowser() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "browser.sh" }).toString();
	}

	private static String macBrowserEdge() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "msedge.sh" }).toString();
	}

	private static String macBrowserFirefox() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "firefox.sh" }).toString();
	}

	public static Boolean chrome(String URL) {
		try {
			InitializeDependence.recording_browser = "chrome";
			String path = Paths.get("libs" + File.separator + "SqaExtension").toAbsolutePath().toString();
//	    	System.out.println("cmd /c start chrome --start-maximized --test-type --disable-infobars --user-data-dir=tmp --load-extension=\"" + path+"\" "+URL);
			if (SystemUtils.IS_OS_WINDOWS) {
				Runtime.getRuntime().exec(
						"cmd /c start chrome --start-maximized --test-type --allow-running-insecure-content --disable-infobars --user-data-dir=tmp --load-extension=\""
								+ path + "\" " + URL);
				logger.info(
						"start chrome --start-maximized --test-type --disable-infobars --user-data-dir=tmp --load-extension=\""
								+ path + "\" " + URL);

			} else {
				Runtime.getRuntime().exec(macBrowser() + " " + path + " " + URL, null, new File(
						(Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool" }).toString())));

			}
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	public static boolean internetEx(String URL) {
		try {
			InitializeDependence.recording_browser = "ie";
			Runtime.getRuntime().exec("cmd /c start iexplore \"" + URL + "\"");

			ProcessBuilder ieRecordProcessBuilder = new ProcessBuilder(
					Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "ie_tools", "SimplifyIERecord.exe" })
							.toString());
			Process ieRecordProcess = ieRecordProcessBuilder.start();
			AgentStart.internetExplorerProcess = ieRecordProcess;
			InitializeDependence.recording_browser = "ie";
			return true;

		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

	}

	public static Boolean edge(String URL) {
		try {
			InitializeDependence.recording_browser = "edge";
			String path = Paths.get("libs" + File.separator + "SqaExtension").toAbsolutePath().toString();
//	    	System.out.println("cmd /c start chrome --start-maximized --test-type --disable-infobars --user-data-dir=tmp --load-extension=\"" + path+"\" "+URL);
			if (SystemUtils.IS_OS_WINDOWS) {
				Runtime.getRuntime().exec(
						"cmd /c start msedge --start-maximized --test-type --allow-running-insecure-content --disable-infobars --user-data-dir=tmp --load-extension=\""
								+ path + "\" " + URL);
				logger.info(
						"start msedge --start-maximized --test-type --disable-infobars --user-data-dir=tmp --load-extension=\""
								+ path + "\" " + URL);
			} else {
				Runtime.getRuntime().exec(macBrowserEdge() + " " + path + " " + URL, null, new File(
						(Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool" }).toString())));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Boolean firefox(String URL) {
		try {

			InitializeDependence.recording_browser = "firefox";
			InitializeDependence.recording_session=true;

			String path = Paths.get("libs/extensions/firefox" + File.separator + "simplifyqa-1.6-fx.xpi")
					.toAbsolutePath().toString();

			if (SystemUtils.IS_OS_WINDOWS) {
				setFirefoxCapabilities("firefox", "windows", URL);
				logger.info("Firefox browser launched with URL : " + URL);
			} else {
				setFirefoxCapabilities("firefox", "mac", URL);
				logger.info("Firefox browser launched with URL : " + URL);
			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean setFirefoxCapabilities(String browserName, String platformName, String URL) {
		
		com.simplifyqa.driver.WebDriver webdri;
		String browser = browserName;
		Webcapabilities capabilities = new Webcapabilities();
		JSONObject alwaysmatch, cap, firstMatch, moz_options;
		JSONArray firstMatchArray = new JSONArray();
		cap = new JSONObject();
		alwaysmatch = new JSONObject();
		firstMatch = new JSONObject();
		moz_options = new JSONObject();
		String[] windows_path = {
				Paths.get("libs/extensions/firefox" + File.separator + "simplifyqa-1.6-fx.xpi")
				.toAbsolutePath().toString(), URL };
		String[] mac_path = { Paths.get("libs/extensions/firefox" + File.separator + "simplifyqa-1.6-fx.xpi")
				.toAbsolutePath().toString(),URL };

		alwaysmatch.put("browserName", "firefox");
		

		if (SystemUtils.IS_OS_WINDOWS) {
//			alwaysmatch.put("platformName","windows");
			List<String> list1 = new ArrayList<String>();
			for (String lang : windows_path) {
				list1.add(lang);
			}
			moz_options.put("binary", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");
			moz_options.put("args", list1);

		} else {
//			alwaysmatch.put("platformName","mac");
			List<String> list2 = new ArrayList<String>();
			for (String lang : mac_path) {
				list2.add(lang);
			}
			moz_options.put("binary", "/Applications/Firefox.app/Contents/MacOS/firefox");
			moz_options.put("args", list2);

		}

		alwaysmatch.put("moz:firefoxOptions", moz_options);
		
		cap.put("alwaysMatch", alwaysmatch);

		capabilities.setCapabilities(cap);

		driver=new com.simplifyqa.driver.WebDriver(capabilities);

		GlobalDetail.webDriver.put("firefox_rec", driver);

		return true;
	}
}
