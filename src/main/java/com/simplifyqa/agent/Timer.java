package com.simplifyqa.agent;

import com.simplifyqa.handler.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Timer extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  private static final Logger logger = LoggerFactory.getLogger(Timer.class);
  
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setStatus(200);
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.addHeader("Access-Control-Allow-Headers", "Origin");
    resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
    resp.addHeader("Access-Control-Allow-Headers", "Content-Type");
    resp.addHeader("Access-Control-Allow-Methods", "POST");
    resp.addHeader("Access-Control-Allow-Methods", "OPTIONS");
    resp.getWriter().println();
  }
  
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      BufferedReader reader = req.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line); 
    } catch (Exception e) {
      e.printStackTrace();
    } 
    JSONObject resobj = new JSONObject(jb.toString());
    System.out.println(resobj.toString());
    Iterator<String> keyItr = resobj.keys();
    while (keyItr.hasNext()) {
      JSONObject rq;
      String key = keyItr.next();
      String str1;
      switch ((str1 = key).hashCode()) {
        case 1557372922:
          if (!str1.equals("destroy"))
            continue; 
          Service.a().destroy();
          logger.info("Timer Stopped Agent");
          System.out.println("Timer Stopped Agent");
          Service.a().Reset();
          try {
            Service.a().run().get();
          } catch (InterruptedException|java.util.concurrent.ExecutionException e) {
            e.printStackTrace();
          } 
          resp.setStatus(200);
          resp.addHeader("Access-Control-Allow-Origin", "*");
          resp.addHeader("Access-Control-Allow-Headers", "Origin");
          resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
          resp.addHeader("Access-Control-Allow-Headers", "Content-Type");
          resp.addHeader("Access-Control-Allow-Methods", "POST");
          resp.addHeader("Access-Control-Allow-Methods", "OPTIONS");
          rq = new JSONObject();
          rq.put("success", "Destroyed");
          resp.getWriter().println(rq);
      } 
    } 
  }
}
