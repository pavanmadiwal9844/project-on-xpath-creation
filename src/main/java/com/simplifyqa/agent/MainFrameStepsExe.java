package com.simplifyqa.agent;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.Constants;

public class MainFrameStepsExe extends HttpServlet {
	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFrameStepsExe.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {

			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
			resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
			resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
			System.out.println(GlobalDetail.mainframeSteps);
			logger.info("Sending whole steps to exe:"+GlobalDetail.mainframeSteps.toString());
			resp.getWriter().println(GlobalDetail.mainframeSteps);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		}
		logger.info("responding");
	}

}
