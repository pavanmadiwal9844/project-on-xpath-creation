package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.*;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.WebService;
import com.simplifyqa.manager.StepManager;

public class Browser extends HttpServlet {

	/**
	 * @author = "bhaskar"
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(Browser.class);
	public static int stepnumber;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
		try {
			GlobalDetail.rcopl = new JSONObject();
			GlobalDetail.rcopl.put("name", "recorder");
			startSerivceManager();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	//pause button get
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Browser.stepnumber = 0;
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JSONObject resobject = new JSONObject(re.toString());

		try {
			StepManager SqaDriver = new StepManager();
			SqaDriver.launchBrowser(resobject.getString("browser"), resobject.getString("URL"));
			JSONObject object = new JSONObject();
			object.put("action", "LaunchApplication");
			object.put("text", "Launch");
			object.put("value", resobject.getString("URL"));
			object.put("attributes", new JSONArray());
			DumpOBject dmpobj = new DumpOBject();
			UserDetail urdl = new UserDetail();
			File file = new File(UserDir.getAapt() + "/config.properties");
			if (file.exists()) {
				FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
				Properties prop = new Properties();
				prop.load(ip);
				if (prop.containsKey("serverurl"))
					resobject.put("serverURL", prop.getProperty("serverurl"));
			}
			GlobalDetail.serverIP = resobject.getString("serverURL");
			urdl.setCustomerId(resobject.getInt("customerId"));
			urdl.setAuthkey(resobject.getString("authKey"));
			urdl.setProjectId(resobject.getInt("projectId"));
			urdl.setObjtemplateId(resobject.getInt("objtemplateId"));
			urdl.setCreatedBy(resobject.getInt("createdBy"));
			GlobalDetail.userDetail = urdl;
			if ((dmpobj = SqaDriver.DumpObject(dmpobj, object)) != null) {
				Thread.sleep(1500);
				WebService.a().a(dmpobj);
				logger.info("Launched Browser {} with URL {}", resobject.get("browser"), resobject.get("URL"));
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "Launched"));

			} else {
				logger.info("Error");
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "Error"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void startSerivceManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					WebService.a().Reset();
					WebService.a().run().get();
					System.out.println("Started handler");

				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				System.out.println("stoped handler");
			}
		}).start();

	}

}
