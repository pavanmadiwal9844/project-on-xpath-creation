package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.handler.MainFrameService;
import com.simplifyqa.manager.MainFrameManager;

public class GetMainFrameSteps extends HttpServlet {
	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(GetMainFrameSteps.class);
	public static String exename = null;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			logger.info(resobj.toString());
			JSONObject object = new JSONObject();
			if(exename==null) {
				for (int i = 0; i < resobj.getJSONObject(Constants.CHILD).getJSONArray(Constants.ATTRIBUTES).length(); i++) {
					String exename1 = resobj.getJSONObject(Constants.CHILD).getJSONArray(Constants.ATTRIBUTES).getJSONObject(i).getString(Constants.NAME);
					if(exename1.equalsIgnoreCase("exename")&&exename1.trim().length()!=0) {
						exename = resobj.getJSONObject(Constants.CHILD).getJSONArray(Constants.ATTRIBUTES).getJSONObject(i).getString(Constants.VALUE);
					}
				}
				
			}
			object.put(Constants.ACTION, resobj.getJSONObject(Constants.CHILD).getString(Constants.ACTION));
			object.put(Constants.TEXT, resobj.getJSONObject(Constants.CHILD).getString(Constants.TEXT));
			object.put(Constants.ATTRIBUTES, resobj.getJSONObject(Constants.CHILD).getJSONArray(Constants.ATTRIBUTES));
			if (resobj.getJSONObject(Constants.CHILD).get(Constants.VALUE) != null)
				object.put(Constants.VALUE, resobj.getJSONObject(Constants.CHILD).get(Constants.VALUE));
			logger.info(object.toString());
			DumpOBject dmpobj = new DumpOBject();

			MainFrameManager sqaDriver = new MainFrameManager();
			if ((dmpobj = sqaDriver.DumpObject(dmpobj, object)) != null)
				MainFrameService.a().a(dmpobj);

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println(new JSONObject().put("success", Boolean.TRUE));
	}

//	private JSONObject settingUnique(JSONObject json) {
//		int j = 0;
//		if (json.getJSONObject("child").getJSONArray("attributes").length() > 0) {
//			for (int i = 0; i < json.getJSONObject("child").getJSONArray("attributes").length(); i++) {
//				String attribute = json.getJSONObject("child").getJSONArray("attributes").getJSONObject(i)
//						.getString("name");
//				if (attribute.equalsIgnoreCase("label")
//						&& !json.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).isNull("value")
//						&& json.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).getString("value")
//								.trim().length() > 0) {
//					json.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).put("unique", Boolean.TRUE);
//					return json;
//				} else if (attribute.equalsIgnoreCase("position")) {
//					j = i;
//				}
//			}
//			json.getJSONObject("child").getJSONArray("attributes").getJSONObject(j).put("unique", Boolean.TRUE);
//			return json;
//
//		}
//		return json;
//	}

}
