package com.simplifyqa.agent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.CodeEditorDTO;
import com.simplifyqa.Utility.UserDir;

public class CodeEditor extends HttpServlet {

	/**
	 * @author Srinivas
	 */

	private static final long serialVersionUID = 1L;
	public static Logger logger = LoggerFactory.getLogger(CodeEditor.class);
	public static String name = "allclasses-frame.html";
	public static ArrayList<String> list = new ArrayList<String>();
	public static List<CodeEditorDTO> classes = new ArrayList<CodeEditorDTO>();
	JSONObject editorsuggestions = new JSONObject();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.setStatus(HttpStatus.OK_200);
		resp.getWriter().println(convert());
	}
	
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	public JSONObject convert() throws IOException {
		File file = new File(UserDir.getdocpath() + File.separator + name);
		org.jsoup.nodes.Document doc = Jsoup.parse(file, "utf-8");
		Elements links = doc.select("a[href]");
		for (Element link : links) {
			list.add(link.attr("href"));
		}
		CodeEditorDTO dto = new CodeEditorDTO();
		@SuppressWarnings("rawtypes")
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			String path = iter.next().toString();
			File hfile = new File(UserDir.getdocpath() + File.separator + path);
			Document docvar = Jsoup.parse(hfile, "utf-8");
			Elements classes = docvar.getElementsByClass("typeNameLabel");
			String pattern = "\\<.*?\\>";
			dto.setClassName(classes.toString().replaceAll(pattern, ""));
			Elements methods = docvar.getElementsByAttributeValue("name", "method.detail");
			Elements li = new Elements();
			for (Element ele : methods) {
				li = ele.parent().getElementsByTag("pre");
				String[] genMethods = li.toString().trim().replaceAll(pattern, "").replaceAll("&nbsp;", " ")
						.replaceAll("\r\n", "").split("\n");
				for (String met : genMethods) {
					dto.getMethods()
							.add(met.replaceAll("&lt;", "<").replaceAll("&gt;", ">").trim().replaceAll(" +", " ")
									.replaceAll("public ", "").replaceAll("^\\S+\\s+", "")
									.replaceAll("\\s+(throws)(.+)$", ""));
				}
			}
			editorsuggestions.put(dto.getClassName(), dto.getMethods());
			dto.getMethods().clear();
		}
		return editorsuggestions;

	}

}
