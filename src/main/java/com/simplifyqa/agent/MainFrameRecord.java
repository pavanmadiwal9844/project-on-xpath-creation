package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.RuntimeBeanNameReference;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.DTO.UserDetail;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.DesktopService;
import com.simplifyqa.handler.MainFrameService;
import com.simplifyqa.handler.WebService;
import com.simplifyqa.manager.StepManager;
import com.simplifyqa.method.DesktopMethod;

public class MainFrameRecord extends HttpServlet {

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFrameRecord.class);
	public static int stepnumber = 0;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
		try {
			startSerivceManager();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		try {
			stepnumber = 0;
			StringBuffer re = new StringBuffer();
			String line = null;
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			JSONObject resobject = new JSONObject(re.toString());
			GetMainFrameSteps.exename =null;
			UserDetail urdl = new UserDetail();
			urdl.setCustomerId(resobject.getInt(Constants.CUSTOMERID));
			urdl.setAuthkey(resobject.getString(Constants.AUTHKEY));
			urdl.setProjectId(resobject.getInt(Constants.PROJECTID));
			urdl.setObjtemplateId(resobject.getInt("objtemplateId"));
			urdl.setCreatedBy(resobject.getInt("createdBy"));
			GlobalDetail.user = urdl;
			GlobalDetail.serverIP = resobject.getString(Constants.SERVERIP);
			if (resobject.getString("type").equalsIgnoreCase("mainframe")) {
				InitializeDependence.mfAction.startRecorder();
			}else if(resobject.getString("type").equalsIgnoreCase("sap"))
			{
				StartDesktopRecorder.startSerivceManager();
				InitializeDependence.sapAction.startRecorder();
			}
			else if (resobject.getString("type").equalsIgnoreCase("desktop")) {
				resobject.put("URL", resobject.getString("URL").replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]", ""));
				DesktopMethod.launchprocess = resobject.getString("URL");
				Process run = Runtime.getRuntime().exec(new String[] {"cmd","/c",Paths.get(resobject.getString("URL")).toAbsolutePath().toString()});
				JSONObject object = new JSONObject();
				object.put("action", "LaunchApplication");
				object.put("text", "Launch");
				object.put("value", resobject.getString("URL"));
				object.put("attributes", new JSONArray());
				StartDesktopRecorder.startSerivceManager();
				InitializeDependence.dtAction.startRecorder();
				StepManager SqaDriver = new StepManager();
				DumpOBject dmpobj = new DumpOBject();
				if ((dmpobj = SqaDriver.DumpObject(dmpobj, object)) != null) {
					Thread.sleep(1500);
					DesktopService.a().a(dmpobj);
				}
			}

			resp.getWriter().println(new JSONObject().put("data", "Launched"));
		} catch (Exception e) {
			e.printStackTrace();
			resp.getWriter().println(new JSONObject().put("data", "WrongPath").toString());
		}
	}

	private void startSerivceManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MainFrameService.a().Reset();
					MainFrameService.a().run().get();
					logger.info("Started handler");

				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				logger.info("stoped handler");
			}
		}).start();

	}

	private void startRecorder() throws IOException, InterruptedException {
		String path1 = Paths.get("libs" + File.separator + "MainFrame_RP" + File.separator + "Record" + File.separator)
				.toAbsolutePath().toString();
		String path = Paths.get("libs" + File.separator + "MainFrame_RP" + File.separator + "Record" + File.separator
				+ "SimplyRecord.exe").toAbsolutePath().toString();
		Runtime run = Runtime.getRuntime();
		run.exec(path, null, new File(path1));
	}

}
