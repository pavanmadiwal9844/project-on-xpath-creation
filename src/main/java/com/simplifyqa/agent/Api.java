package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.SystemUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.ApiRequest;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;

public class Api extends HttpServlet {
	private final static Logger logger = LoggerFactory.getLogger(Sync.class);
	

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try {

		} catch (Exception e) {
			System.out.println(e);
			JSONObject error = new JSONObject();
			error.put("error", e.toString());
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.setStatus(HttpStatus.OK_200);
			resp.getWriter().print(error);

		}
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();
		JSONObject apiresponse = new JSONObject();

		String line = null;
		try {
			ApiRequest request_data = new ApiRequest();
			HashMap<String, String> header = new HashMap<String, String>();
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);

			JSONObject res = new JSONObject(re.toString());

			JSONObject req_data = new JSONObject(res.get("request").toString());

			// set api request DTO values
			request_data.setAuthorization(req_data.getJSONObject("authorization"));
			request_data.setHeaders(req_data.getJSONArray("headers")); // done
			request_data.setType(req_data.get("type").toString()); // done
			request_data.setUrl(req_data.get("url").toString()); // done
			request_data.setQuery_params(req_data.getJSONArray("query_params"));// can be sent directly
			request_data.setRequest_body_type(req_data.getJSONObject("request_body_type"));
			request_data.setRequest_body(req_data.getJSONObject("request_body"));

			// set headers value
			for (int i = 0; i < request_data.getHeaders().length(); i++) {
				if (request_data.getHeaders().getJSONObject(i).has("active")) {
					if (Boolean.parseBoolean(request_data.getHeaders().getJSONObject(i).get("active").toString())) {
						header.put(request_data.getHeaders().getJSONObject(i).get("key").toString(),
								request_data.getHeaders().getJSONObject(i).get("value").toString());
					}
				}
			}

			// set request body
			String request_body_api = request_data.getRequest_body_type()
					.getJSONObject(request_data.getRequest_body().get("active").toString()).toString();

			HttpResponse response = HttpUtility.newpostapiimplementation(request_data.getUrl(), request_data.getType(),
					request_body_api, request_data.getQuery_params(), header);

			apiresponse.put("statuscode", response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == 200) {
				apiresponse.put("response", EntityUtils.toString(response.getEntity()));
			}

			logger.info("Sending response");
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
//			apiresponse.put("status", 200);
			resp.getWriter().println(apiresponse);

		} catch (Exception e) {
			JSONObject res= new JSONObject();
			res.put("error", e.toString());
			resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(res);
			
			e.printStackTrace();
		}

	}

}
