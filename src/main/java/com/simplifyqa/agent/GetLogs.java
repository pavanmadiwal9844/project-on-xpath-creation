package com.simplifyqa.agent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;

public class GetLogs extends HttpServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Apps.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			JSONObject res;
			int exeId = Integer.parseInt(req.getPathInfo().substring(1));
			File logFile = UserDir.getlogfile();
			if (logFile.exists()) {
				res = new JSONObject();
				JSONObject logData = new JSONObject();
				logData.put("executionId", exeId);
				logData.put("log",
						new String(Files.readAllBytes(Paths.get(logFile.getAbsolutePath())), StandardCharsets.UTF_8));
				res.put("success", "true");
				res.put("data", logData);
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(res);
			} else {
				res = new JSONObject();
				res.put("success", "false");
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(res);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
