package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.Utility.UserDir;

public class GetIpAddress extends HttpServlet {

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject reque = new JSONObject(re.toString());
		JSONObject json = new JSONObject();
		if(reque.getString("type").equalsIgnoreCase("machinedetails")) {
		InetAddress myIP = InetAddress.getLocalHost();
		String i2 = myIP.getHostAddress();
		String i1 = myIP.getHostName();
		InetAddress localHost = InetAddress.getLocalHost();
		NetworkInterface ni = NetworkInterface.getByInetAddress(localHost);
		byte[] hardwareAddress = ni.getHardwareAddress();
		String[] hexadecimal = new String[hardwareAddress.length];
		for (int i = 0; i < hardwareAddress.length; i++) {
			hexadecimal[i] = String.format("%02X", hardwareAddress[i]);
		}
		String macAddress = String.join("-", hexadecimal);
		json.put("machinname", i1);
		json.put("ipaddress", i2);
		json.put("macaddress", macAddress);
		}
		 if(reque.getString("type").equalsIgnoreCase("uuid")||reque.getString("type").equalsIgnoreCase("machinedetails")) {
			String path =  Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "jenkins.properties" }).toString();
			File file = new File(path);
			if(file.exists()) {
			Properties p=new Properties(); 
			FileReader reader = new FileReader(path);
		    p.load(reader);
		    if(file.exists())
		     json.put("uuid", p.getProperty("uuid"));
		}
		}if(reque.getString("type").equalsIgnoreCase("save")){
			String path =  Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "jenkins.properties" }).toString();
			File proxyfile = new File(path);
			File file = new File(path);
			FileWriter fileWritter = new FileWriter(path, true);
		    BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		    bufferWritter.append("uuid:"+reque.getString("uuid"));
			Properties p=new Properties();  
			p.put("uuid", reque.getString("uuid"));
			FileOutputStream outputStrem = new FileOutputStream(path);
			p.store(outputStrem, "this is for jenkins");
			
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(json);
	}

	// public static void main(String[] args) throws UnknownHostException,
	// SocketException {
	//
	// InetAddress localHost = InetAddress.getLocalHost();
	// NetworkInterface ni = NetworkInterface.getByInetAddress(localHost);
	// byte[] hardwareAddress = ni.getHardwareAddress();
	// String[] hexadecimal = new String[hardwareAddress.length];
	// for (int i = 0; i < hardwareAddress.length; i++) {
	// hexadecimal[i] = String.format("%02X", hardwareAddress[i]);
	// }
	// String macAddress = String.join("-", hexadecimal);
	//
	// System.out.println(macAddress);
	//
	// //Secondly, let's get the MAC address for a given local IP address:
	//
	// InetAddress localIP = InetAddress.getByName("192.168.43.81");
	// NetworkInterface ni1 = NetworkInterface.getByInetAddress(localIP);
	// byte[] macAddress1 = ni1.getHardwareAddress();
	// for(byte b:macAddress1)
	// System.out.print(b);
	//
	// //for all mac addresses
	//
	// Enumeration<NetworkInterface> networkInterfaces =
	// NetworkInterface.getNetworkInterfaces();
	// while (networkInterfaces.hasMoreElements()) {
	// NetworkInterface ni2 = networkInterfaces.nextElement();
	// byte[] hardwareAddress2 = ni2.getHardwareAddress();
	// if (hardwareAddress2 != null) {
	// String[] hexadecimalFormat = new String[hardwareAddress2.length];
	// for (int i = 0; i < hardwareAddress2.length; i++) {
	// hexadecimalFormat[i] = String.format("%02X", hardwareAddress2[i]);
	// }
	// System.out.println(String.join("-", hexadecimalFormat));
	// }
	// }
	// }

}
