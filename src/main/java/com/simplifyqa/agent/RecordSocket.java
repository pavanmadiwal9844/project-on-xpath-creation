package com.simplifyqa.agent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import com.simplifyqa.ObjectInspector.ObjectInspector;
import com.simplifyqa.ObjectInspector.objectservice;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.method.WebMethod;

import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;

@ServerEndpoint(value = "/SqaSocket")
public class RecordSocket {

	@OnOpen
	public void onOpen(Session session) throws IOException, SocketTimeoutException {
		System.out.println("WebSocket opened: " + session.getId());
		System.out.println(session.isOpen());
		GlobalDetail.sendStpesSession = session;
	}

	@OnMessage
	public void OnMessage(String res, Session session) throws IOException, SocketTimeoutException {

		try {
			JSONObject req = new JSONObject(res);
			if (req.has("webobjectinspector")) {
				if (req.getString("webobjectinspector").equals("start")) {
					if (GlobalDetail.webInspectpr != null)
						GlobalDetail.webInspectpr.close();
					GlobalDetail.webInspectpr = session;
				} else if (req.getString("webobjectinspector").equals("validate")) {
					GlobalDetail.sendStpesSession.getAsyncRemote().sendText(req.toString());
				}
			} else {
				if (req.has("duplicate")&&req.getBoolean("duplicate")) {
					setDuplicate();
					GlobalDetail.webInspectpr.getAsyncRemote().sendText(req.toString());
				} else if (req.has("inspect") && req.getString("inspect").equals("Detach")) {
					if (!InitializeDependence.webDriver.isEmpty()) {
						for (Entry<String, WebMethod> map : InitializeDependence.webDriver.entrySet()) {
							map.getValue().deleteSession();
							map.getValue().closeDriver();
						}
						ObjectInspector.inspect = false;
						objectservice.a().destroy();
					} else {
						InitializeDependence.mfAction.stopRecorder();
						InitializeDependence.sapAction.stopRecorder();
						InitializeDependence.dtAction.stopRecorder();
						GlobalDetail.mainFrSession= null;
						GlobalDetail.sapsession= null;
						GlobalDetail.desktopsession= null;
					}

				} else if (req.has("currentPage") || req.has("identifier")) {
					GlobalDetail.webInspectpr.getAsyncRemote().sendText(req.toString());
				}
				if(req.has("mainframe")&&req.getJSONObject("mainframe").has("currentPage")) {
					if(GlobalDetail.mainFrSession==null) {
						InitializeDependence.mfAction.startValidation();
						GlobalDetail.sem = new Semaphore(0);
						GlobalDetail.sem.acquire();
					}
					GlobalDetail.mainFrSession.getAsyncRemote().sendText(req.getJSONObject("mainframe").toString());;
				}
				if(req.has("sap")&&req.getJSONObject("sap").has("currentPage")) {
					if(GlobalDetail.sapsession==null) {
						InitializeDependence.sapAction.startValidation();
						GlobalDetail.sem = new Semaphore(0);
						GlobalDetail.sem.acquire();
					}
					GlobalDetail.sapsession.getAsyncRemote().sendText(req.getJSONObject("sap").toString());;
				}
				if(req.has("desktop")&&req.getJSONObject("desktop").has("currentPage")) {
					if(GlobalDetail.desktopsession==null) {
						InitializeDependence.dtAction.startValidation();
						GlobalDetail.sem = new Semaphore(0);
						GlobalDetail.sem.acquire();
					}
					GlobalDetail.desktopsession.getAsyncRemote().sendText(req.getJSONObject("desktop").toString());;
				}
				GlobalDetail.sendStpesSession = session;

			}
		} catch (Exception e) {
			GlobalDetail.sendStpesSession = session;

		}

	}
	
	public static void setDuplicate() {
		final JFrame frame = new JFrame();
		frame.setUndecorated(true);

		frame.getContentPane().setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		frame.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height / 2;
		int width = screenSize.width / 2;
		frame.setSize(width, height);
		frame.setLocationRelativeTo(null);
		frame.setState((int) Float.MAX_VALUE);
		frame.setFocusable(true);
		ImageIcon loading;
		loading = new ImageIcon(new File((UserDir.getImage() + File.separator + "duplicate.gif")).getAbsolutePath());
		frame.add(new JLabel("", loading,JLabel.CENTER));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setAlwaysOnTop(true);
		frame.setVisible(true);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame.setVisible(false);
	}

	@OnClose
	public void onClose() throws IOException, SocketTimeoutException {
		System.out.println("Closing a WebSocket due to ");
	}

}
