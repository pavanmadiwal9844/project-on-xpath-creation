package com.simplifyqa.agent;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.python.antlr.ast.Str;
import org.sikuli.natives.SysUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.command.PullImageResultCallback;
import com.github.dockerjava.api.model.AccessMode;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DockerClientBuilder;
import com.simplifyqa.CodeEditor.CodeEditorDebugger;
import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.WebService;
import com.simplifyqa.manager.StepManager;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;

import io.grpc.netty.shaded.io.netty.util.internal.SocketUtils;
import okhttp3.internal.http.RealResponseBody;

public class CodeEditorLocal extends HttpServlet {

	private boolean image_present = false;
	private boolean container_present = false;
	private final static Logger logger = LoggerFactory.getLogger(CreateStep.class);
	private boolean M1=false;
	private boolean mac=false;
	
	private Boolean getPorts(int[] ports) throws IOException {
		int[] portss = new int[4];
		int count=0;
		
		
		for (int port : ports) {
	        try {
	            
	            	ServerSocket obj = new ServerSocket(port);
	            	portss[count]=port;
	            	count++;
	            	if(count==4)
	            		break;
	            
	            
	        } catch (IOException ex) {
	            continue; // try next port
	        }
	    }
		if(portss.length==4)
			return true;
		
		throw new IOException("no free port found");
		
	}
	
	private String MACOS_arch() {
		try {
			String cmd = "sysctl -n machdep.cpu.brand_string";
	        Runtime run = Runtime.getRuntime();
	        Process pr = run.exec(cmd);
	        pr.waitFor();
	        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
	        String line = "";
	        while ((line=buf.readLine())!=null) {
	        System.out.println(line);
	        return line;
	        }
	        
	        return "";
		}
		catch (Exception e) {
			return "";
		}
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {	
		try {
			
			
			JSONObject apiresponse = new JSONObject();
			apiresponse.put("url", "http://127.0.0.1:7000/#/workspaces/");
			String current_image = "56437/simplifyqa_codeeditor:7.0";
			String image_id ="sha256:d0e029b36434368938f6c7bdeacc8a16f52d58d66d658c09fe9dba65227dd515";
//			String image_id = "sha256:5d56cb4723b187d06a3ef3f7fce4d60326826c3742dd624de750733bd68b6377";

			
			if (SystemUtils.IS_OS_MAC) {
				apiresponse.remove("url");
				apiresponse.put("url", "http://127.0.0.1:7001/#/workspaces/");

				mac=true;
				String type=MACOS_arch();
				
				//for M1 mac
				if(type.equals("Apple M1")) {
					M1=true;
					image_id="sha256:e21c5c4475d5879841eae5197f327083b91a717a8f00bf943756163d6d6821c6";
					current_image="56437/simplifyqa_codeeditor:M1_4";
				}

				com.github.dockerjava.api.DockerClient dockerclient = DockerClientBuilder.getInstance().build();
				List<com.github.dockerjava.api.model.Image> images = dockerclient.listImagesCmd().exec();
				List<com.github.dockerjava.api.model.Container> containers = dockerclient.listContainersCmd().exec();
				


				for (int i = 0; i < images.size(); i++) {
					System.out.println(images.get(i).getRepoTags()[0].toString());
					if (current_image.equals(images.get(i).getRepoTags()[0].toString())) {
						image_present = true;
						System.out.println("image present");
					}
				}

				if (!image_present) {
					
					//for M1 mac
					if(M1) {
						logger.info("Download M1 image for CodeEditor");
						dockerclient.pullImageCmd("56437/simplifyqa_codeeditor").withTag("M1_4")
						.exec(new PullImageResultCallback()).awaitCompletion();
					}else {
						logger.info("Download Mac Intel image for CodeEditor");
					dockerclient.pullImageCmd("56437/simplifyqa_codeeditor").withTag("7.0")
							.exec(new PullImageResultCallback()).awaitCompletion();
					}
					image_present = true;
				}

				for (int i = 0; i < containers.size(); i++) {
					System.out.println(containers.get(i).getImageId());
					if (containers.get(i).getImageId().equals(image_id)) {

						if (containers.get(i).getState().equals("running")) {
//						System.out.println("code editor is ready to use");
							container_present = true;
						}

						// starting container
						else if (containers.get(i).getState().equals("stopped")
								|| containers.get(i).getState().equals("exited")) {

							dockerclient.startContainerCmd(containers.get(i).getId());
							container_present = true;
						}
					}
					
					else {
						if (containers.get(i).getState().equals("running")) {
							System.out.println(containers.get(i).getId().toString());
		
							dockerclient.stopContainerCmd(containers.get(i).getId()).exec();
							dockerclient.removeContainerCmd(containers.get(i).getId()).exec();
						}
						
						
						if (containers.get(i).getState().equals("stopped")
								|| containers.get(i).getState().equals("exited")) {
							dockerclient.removeContainerCmd(containers.get(i).getId().toString());
						}
					}

				}

				if (image_present && !container_present) {
					ExposedPort tcp3000 = ExposedPort.tcp(3000);
					ExposedPort tcp5006 = ExposedPort.tcp(5006);
					ExposedPort tcp8001 = ExposedPort.tcp(8001);
					ExposedPort tcp19010 = ExposedPort.tcp(19010);
					ExposedPort tcp5501  = ExposedPort.tcp(5501);

					Ports portBindings = new Ports();
					portBindings.bind(tcp3000, Ports.Binding.bindPort(7001));
					portBindings.bind(tcp5006, Ports.Binding.bindPort(7008));
					portBindings.bind(tcp8001, Ports.Binding.bindPort(9001));
					portBindings.bind(tcp19010, Ports.Binding.bindPort(7009));
					portBindings.bind(tcp5501, Ports.Binding.bindPort(6507));
					


					Volume volume = new Volume("/data");
					CreateContainerResponse container = dockerclient.createContainerCmd(current_image)
							.withBinds(new Bind("/Applications/SimplifyQA-Agent.app/Contents/Resources/app/CodeEditorDependencies",volume,AccessMode.ro))
							.withExposedPorts(tcp3000, tcp5006, tcp8001, tcp19010,tcp5501)
//		            .withHostConfig(newHostConfig()
//		                    .withPortBindings(portBindings))
							.withPortBindings(portBindings).exec();

					dockerclient.startContainerCmd(container.getId()).exec();
					
					
				}
				
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.setStatus(HttpStatus.OK_200);
				apiresponse.put("status", 200);
				apiresponse.put("error", "");
				image_present=false;
				container_present=false;
				resp.getWriter().print(apiresponse);

			} else if (SystemUtils.IS_OS_WINDOWS) {
//			

				com.spotify.docker.client.DockerClient dockerclient_win = DefaultDockerClient.fromEnv().build();

				List<com.spotify.docker.client.messages.Image> images_win = dockerclient_win
						.listImages(DockerClient.ListImagesParam.allImages());
				List<Container> containers_win = dockerclient_win
						.listContainers(DockerClient.ListContainersParam.allContainers());

				// check image present or not
				for (int i = 0; i < images_win.size(); i++) {
					if (current_image.equals(images_win.get(i).repoTags().get(0).toString())) {
						image_present = true;
						System.out.println("image present");
					}
				}

				// download image
				if (!image_present) {
					dockerclient_win.pull(current_image);
					image_present = true;
				}

				// check if container running or not
				for (int i = 0; i < containers_win.size(); i++) {
					System.out.println(containers_win.get(i).imageId());
					if (containers_win.get(i).imageId().equals(image_id)) {

						if (containers_win.get(i).state().equals("running")) {
//						System.out.println("code editor is ready to use");
							container_present = true;
						}

						// starting container
						else if (containers_win.get(i).state().equals("stopped")
								|| containers_win.get(i).state().equals("exited")) {

							dockerclient_win.startContainer(containers_win.get(i).id());
							container_present = true;
						}
					}
					
					else {
						if (containers_win.get(i).state().equals("running")) {
							System.out.println(containers_win.get(i).id());
							dockerclient_win.stopContainer(containers_win.get(i).id(),0);
							dockerclient_win.removeContainer(containers_win.get(i).id());
						}
						
						else if (containers_win.get(i).state().equals("stopped")
								|| containers_win.get(i).state().equals("exited")) {
							dockerclient_win.removeContainer(containers_win.get(i).id());
						}
					}

				}

				if (image_present && !container_present) {
					int i = -1;
					final String[] ports = { "3000", "5006", "8001", "19010","5501" };
					final String[] input_ports = { "7000", "7008", "9001", "7009" ,"6507"};
					final Map<String, List<PortBinding>> portBindings = new HashMap<>();
					for (String port : ports) {
						i++;
						List<PortBinding> hostPorts = new ArrayList<>();
						hostPorts.add(PortBinding.of("0.0.0.0", input_ports[i]));
						portBindings.put(port, hostPorts);
					}
					
					

					final HostConfig hostConfig = HostConfig.builder().portBindings(portBindings)
							.appendBinds(com.spotify.docker.client.messages.HostConfig.Bind.from("C:\\Program Files (x86)\\Simplify3x\\SimplifyQA\\CodeEditorDependencies").to("/data").readOnly(true).build())
							.build();
					final ContainerConfig containerConfig = ContainerConfig.builder().hostConfig(hostConfig)
							.image(current_image).exposedPorts(ports).build();

					final ContainerCreation creation = dockerclient_win.createContainer(containerConfig);
					final String id = creation.id();

					final ContainerInfo info = dockerclient_win.inspectContainer(id);
					dockerclient_win.startContainer(id);

				}
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.setStatus(HttpStatus.OK_200);
				apiresponse.put("status", 200);
				apiresponse.put("error", "");
				image_present=false;
				container_present=false;
				resp.getWriter().print(apiresponse);

			}

		} catch (Exception e) {
			System.out.println(e);
			JSONObject error = new JSONObject();
			error.put("error", e.toString());
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.setStatus(HttpStatus.OK_200);
			error.put("status", 200);
			image_present=false;
			container_present=false;
			resp.getWriter().print(error);

		}
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();
		CodeEditorDebugger giturl = new CodeEditorDebugger();

		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			
			
		
			
			JSONObject api_request = new JSONObject(re.toString());
			
			if(api_request.get("girUrl").toString().equals("")) {
				resp.setStatus(HttpStatus.BAD_REQUEST_400);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				JSONObject apiresponse = new JSONObject(re.toString());
//				apiresponse.put("url","http://127.0.0.1:7000/#/workspaces/"+api_request.get("projectName").toString().replace(" ",""));
				apiresponse.put("status",406);
				resp.getWriter().println(apiresponse);
				return;
			}
			
			else {
			
			System.out.println(api_request.get("projectName").toString().replace(" ",""));
			
			
			//project wise workspace
			if(!api_request.getBoolean("codeEditorCustomerLevel")){
			
			if(giturl.workspace(api_request.get("projectId").toString(),api_request.get("projectName").toString(),api_request.get("girUrl").toString(),api_request.get("customerId").toString())) {
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				JSONObject apiresponse = new JSONObject(re.toString());
				Thread.sleep(3000);
				if(!mac) {
				apiresponse.put("url","http://127.0.0.1:7000/#/workspaces/"+api_request.get("projectName").toString().replace(" ",""));
				}
				else {
				apiresponse.put("url","http://127.0.0.1:7001/#/workspaces/"+api_request.get("projectName").toString().replace(" ",""));}

				
				apiresponse.put("status",200);
				resp.getWriter().println(apiresponse);
//				return;
			}
			
			else {
				resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println();
//				e.printStackTrace();
				return;
				
			}
			
			}
			
			//customer wise workspace
			else {
				if(giturl.workspace(api_request.get("projectId").toString(),api_request.get("customerId").toString(),api_request.get("girUrl").toString(),api_request.get("customerId").toString())) {
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					JSONObject apiresponse = new JSONObject(re.toString());
					if(!mac) {
					apiresponse.put("url","http://127.0.0.1:7000/#/workspaces/"+api_request.get("customerId").toString());
					}
					else {
						apiresponse.put("url","http://127.0.0.1:7001/#/workspaces/"+api_request.get("customerId").toString());

					}
					
					apiresponse.put("status",200);
					resp.getWriter().println(apiresponse);
//					return;
				}
				
				else {
					resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println();
//					e.printStackTrace();
					return;
					
				}
			}
			
			
			
			
			
			
			}

		} catch (Exception e) {
			resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println();
			e.printStackTrace();
		}

		

	}

}
