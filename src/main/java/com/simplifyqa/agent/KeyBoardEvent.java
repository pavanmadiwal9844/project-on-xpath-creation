package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.handler.Task;

public class KeyBoardEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(KeyBoardEvent.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);

			// for pause and resume steps
			if (InitializeDependence.mobile_pause) {
				JSONObject resobj = new JSONObject(jb.toString());
				System.out.println(resobj.toString());
				Iterator<String> keyItr = resobj.keys();
				while (keyItr.hasNext()) {
					String key = keyItr.next();
					switch (key) {
					case "keyEvent":
						JSONObject obj = new JSONObject();
						obj = (JSONObject) resobj.get(key);
						CompletableFuture<?> cf = new CompletableFuture<>();
						logger.info("Keyevent Start");
						Service.a().a(new Task(Events.keyEvent, cf, obj));
						boolean res = (Boolean) cf.join();
					}

				}
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "skipped"));

				return;
			}
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		System.out.println(resobj.toString());
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			String key = keyItr.next();
			switch (key) {
			case "keyEvent":
				JSONObject obj = new JSONObject();
				obj = (JSONObject) resobj.get(key);
				CompletableFuture<?> cf = new CompletableFuture<>();
				logger.info("Keyevent Start");
				Service.a().a(new Task(Events.keyEvent, cf, obj));
				boolean res = (Boolean) cf.join();
				if (res) {
					logger.info("keyborard Ended");
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "Executed"));
				} else {
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "Failed"));
				}

			}
		}
	}
}
