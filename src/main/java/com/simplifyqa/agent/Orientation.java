package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.driver.WebDriver;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.handler.Task;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;

public class Orientation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Install.class);

	
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		System.out.println(resobj.toString());
		if(resobj.has("getorientation")) {
			if(resobj.getJSONObject("getorientation").getString("type").equals("android")) {
				AndroidMethod sqaDriver = new AndroidMethod();
				sqaDriver.setDriver(GlobalDetail.sqaDriver.get(resobj.getJSONObject("getorientation").getString("id")));
				Thread.currentThread().setName(resobj.getJSONObject("getorientation").getString("id"));
				sqaDriver.setDevice(resobj.getJSONObject("getorientation").getString("id"));
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("orientation", sqaDriver.getOrientation()));
			}
			else {
				IosMethod sqaDriver = new IosMethod();
				sqaDriver.setSessionId(GlobalDetail.iosSession.get(resobj.getJSONObject("getorientation").getString("id")));
				sqaDriver.setDevice(resobj.getJSONObject("getorientation").getString("id"));
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("orientation", sqaDriver.getOrientation()));
			}
		}
		if (resobj.has("setorientation")) {
			CompletableFuture<?> cf = new CompletableFuture<>();
			logger.info("orientation Start");
			Service.a().a(new Task(Events.orientation, cf, resobj.getJSONObject("setorientation")));
			boolean res = (Boolean) cf.join();
			if (res) {
				logger.info("orientation Ended");
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "Executed"));
			} else {
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "Failed"));
			}
		} else {
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			logger.info("Error");
			resp.getWriter().println(new JSONObject().put("data", "Failed"));
		}

	}

}
