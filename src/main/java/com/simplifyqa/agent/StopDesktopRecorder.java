package com.simplifyqa.agent;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StopDesktopRecorder extends HttpServlet{
	
	/**
	 * author Bhaskar
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		stopDesktoprecorder();
	}

	public void stopDesktoprecorder() throws IOException {
		StartDesktopRecorder obj = new StartDesktopRecorder();
		obj.stopRecorder();
	}

}
