package com.simplifyqa.agent;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.Service;
import com.simplifyqa.mobile.android.Setautomator2;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import dorkbox.notify.Notify;
import dorkbox.notify.Pos;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class StopNotification extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  private static final Image Image = null;
  
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setStatus(200);
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.addHeader("Access-Control-Allow-Headers", "*");
    resp.addHeader("Access-Control-Allow-Methods", "*");
    resp.getWriter().println();
  }
  
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.setStatus(200);
    Stop();
  }
  
  public static void Stop() {
    try {
      Service.a().killProcess();
      Service.a().killDriver();
      Idevice.EndRunnerapp();
      for (String key : ideviceMaganger.devices.keySet()) {
        if (((JSONObject)ideviceMaganger.devices.get(key)).has("simulator"))
          Idevice.shutdownSimulator(key); 
      }
      Setautomator2.killEmulator();
      Notify.create()
        .image(ImageIO.read(new File(
              Paths.get(UserDir.getImage().getAbsolutePath(), new String[] { "loader.png" }).toUri())))
        .title("SimplifyQA Agent").text("Stopping Agent...").position(Pos.BOTTOM_RIGHT).darkStyle()
        .hideAfter(3000).show();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } 
  }
}
