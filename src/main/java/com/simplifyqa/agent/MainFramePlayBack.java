package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.execution.Startexecution;

public class MainFramePlayBack extends HttpServlet {

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFramePlayBack.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			File file = new File(UserDir.getAapt() + "/config.properties");
			if (file.exists()) {
				FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
				Properties prop = new Properties();
				prop.load(ip);
				if (prop.containsKey("serverurl"))
					resobj.put("serverIp", prop.getProperty("serverurl"));
			}
			if(resobj.has("StepIndex")) {
				GlobalDetail.ResumeStep = resobj.get("StepIndex").toString();
				GlobalDetail.ResumePlayback = true;
				}
			GlobalDetail.ReferencePlayback = true;
			logger.info(resobj.toString());
			Startexecution start = new Startexecution();
			start.Setexecution(resobj);
			start.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println(new JSONObject().put("PlayBack", "working"));

	}

}
