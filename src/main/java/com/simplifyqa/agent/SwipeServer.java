package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.handler.Task;

public class SwipeServer extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SwipeServer.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		InitializeDependence.check++;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);

			//for pause and resume steps
			if (InitializeDependence.mobile_pause) {

				JSONObject resobj = new JSONObject(jb.toString());
				Iterator<String> keyItr = resobj.keys();
				while (keyItr.hasNext()) {
					String key = keyItr.next();
					switch (key) {
					case "swipeEvent":
						JSONObject obj = new JSONObject();
						obj = (JSONObject) resobj.get(key);
						CompletableFuture<?> cf = new CompletableFuture<>();
						Service.a().a(new Task(Events.swip, cf, obj));
						logger.info("Swipe started");
						boolean res = (Boolean) cf.join();
					}
				}
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(new JSONObject().put("data", "skipped"));

				return;
			}

			if (InitializeDependence.mobile_pause)
				return;
		} catch (Exception e)

		{
			InitializeDependence.check = 0;
			e.printStackTrace();
		}

		InitializeDependence.check = 0;
		System.out.println(jb.toString());
		JSONObject resobj = new JSONObject(jb.toString());
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			String key = keyItr.next();
			switch (key) {
			case "swipeEvent":
				JSONObject obj = new JSONObject();
				obj = (JSONObject) resobj.get(key);
				CompletableFuture<?> cf = new CompletableFuture<>();
				Service.a().a(new Task(Events.swip, cf, obj));
				logger.info("Swipe started");
				boolean res = (Boolean) cf.join();
				if (res) {
					logger.info("Swipe Ended");
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "Executed"));
				} else {
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "Failed"));
				}
				break;
			}
		}
	}
}
