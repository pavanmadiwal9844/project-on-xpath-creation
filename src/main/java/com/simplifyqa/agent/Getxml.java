package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.joox.JOOX;
import org.joox.Match;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.handler.Task;

import emoji4j.EmojiUtils;

public class Getxml extends HttpServlet {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Getxml.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.setContentType("text/css");
		resp.setStatus(HttpStatus.OK_200);
		resp.getWriter().println(new String(Files.readAllBytes(
				Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "SqaExtension", "object.css" }))));
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CompletableFuture<?> cf = new CompletableFuture<>();
		// logger.info("Xml Started");
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		Service.a().a(new Task(Events.xml, cf, new JSONObject()));
		String res;
		try {
			if ((res = (String) cf.join()) == null) {
				// logger.info("Xml Ended");
				resp.setStatus(HttpStatus.NO_CONTENT_204);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println();
			} else {
				// logger.info("Xml Ended");
//				String removeAllEmojis = EmojiUtils.removeAllEmojis(res);
				Document parse;
				try {
					parse = DocumentBuilderFactory.newInstance().newDocumentBuilder()
							.parse(new ByteArrayInputStream(res.getBytes("UTF-8")));
					// android(parse, count_attr(parse, "resource-id"));
					if (resobj.getString("type").equals("android"))
						android(parse, count_attr(parse, "resource-id"));
					else {
						ios(parse, count_attr(parse, "name"));
					}
					String re = DOMTOString(parse);
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("document", re));
				} catch (Exception e) {
					e.printStackTrace();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private static String[] attributes = { "text", "content-desc", "value", "label", "name", "class" };

	public static ArrayList<Element> findNodes(String attribute, Document document) throws XPathExpressionException {
		try {
			NodeList nodeList = document.getElementsByTagName("*");
			Element element;
			ArrayList<Element> nodes = new ArrayList<Element>();
			for (int i = 0; i < nodeList.getLength(); i++) {
				element = (Element) nodeList.item(i);
				if (!element.getAttribute(attribute).equalsIgnoreCase("")) {
					nodes.add(element);
				}
			}
			return nodes;
		} catch (Exception e) {
			return null;
		}
	}

	public static ArrayList<Element> ClassNodes(Element element, Document document) throws XPathExpressionException {
		try {
			NodeList list2 = checkXpath(
					"//" + element.getTagName() + "[@class = '" + element.getAttribute("class") + "']", document);
			ArrayList<Element> nodes = new ArrayList<Element>();
			for (int i = 0; i < list2.getLength(); i++) {
				element = (Element) list2.item(i);
				nodes.add(element);
			}
			return nodes;
		} catch (Exception e) {
			return null;
		}
	}

	private static String flowXpath(Element element, Document document) throws XPathExpressionException {
		String text = "";
		Element nearest = null;
		ArrayList<Element> list = new ArrayList<Element>();
		for (int i = 0; i < attributes.length; i++) {
			if ((element.getAttribute(attributes[i])) != "") {
				if (!attributes[i].equalsIgnoreCase("class")) {
					list = findNodes(attributes[i], document);
				} else {
					list = ClassNodes(element, document);
				}
				if (!list.isEmpty() && list.size() != 1) {
					break;
				}
			}

		}
		if (!list.isEmpty() && list.size() != 1) {
			for (int i = 0; i < list.size(); i++) {
				nearest = (Element) list.get(i);
				if (JOOX.$(element).xpath().equalsIgnoreCase(JOOX.$(nearest).xpath())) {
					if (i != list.size() - 1) {
						nearest = list.get(i + 1);
						break;
					} else {
						nearest = list.get(i - 1);
						break;
					}
				}
			}
			int size;
			String common = "";
			String path_element = JOOX.$(element).xpath().replace("hierarchy[1]", "");
			String path_nearest = JOOX.$(nearest).xpath().replace("hierarchy[1]", "");
			String[] elements_list = path_element.split("/");
			String[] nearest_list = path_nearest.split("/");
			if (elements_list.length < nearest_list.length) {
				size = elements_list.length;
			} else {
				size = nearest_list.length;
			}
			for (int i = 0; i < size; i++) {
				if (elements_list[i].equalsIgnoreCase(nearest_list[i])) {
					if (i != size - 1) {
						common = common + elements_list[i] + "/";
					} else {
						common = common + elements_list[i];
					}
				} else {
					break;
				}
			}
			path_element = path_element.replace(common, "");
			path_nearest = path_nearest.replace(common, "");
			common = common + "/" + path_nearest;
			for (int i = 0; i < path_nearest.split("/").length; i++) {
				common = common + "/..";
			}
			common = common + "/" + path_element;
			return common;
		} else {
			return null;
		}

	}

	private static void android(Document document, HashMap<String, Integer> hashmap) throws XPathExpressionException {
		NodeList nodeList = document.getElementsByTagName("*");
		for (int i = 0; i < nodeList.getLength(); i++) {
			HashMap<String, String> xpath = new HashMap<String, String>();
			String Absolute_path, indexed_xpath = null;
			org.w3c.dom.Node node = nodeList.item(i);
			Element element = (Element) node;
			String flowxpath = flowXpath(element, document);
			if (flowxpath != null) {
				element.setAttribute("FlowXpathByPosition", flowxpath);
			}
			NodeList list = null;
			Absolute_path = JOOX.$(element).xpath().replace("hierarchy[1]", "");
			element.setAttribute("xpath", Absolute_path);
			xpath = byattr(element);
			if (!xpath.isEmpty()) {
				for (String x : xpath.keySet()) {
					list = checkXpath(xpath.get(x), document);
					if (list != null) {
						if (list.getLength() == 1) {
							element.setAttribute(x, xpath.get(x));
						} else {
							indexed_xpath = indexing(list, element, xpath.get(x), new StringBuilder(""));
							// xpath = JOOX.$(element).xpath().replace("hierarchy[1]", "");
							element.setAttribute(x, indexed_xpath);
						}
					}
				}
			}
			if (Absolute_path != null) {
				final boolean contains = Absolute_path.contains("android.webkit.WebView");
				final String attribute;
				if (!StringUtils.isEmpty((CharSequence) (attribute = element.getAttribute("resource-id")))
						&& hashmap.containsKey(attribute) && hashmap.get(attribute) == 1 && !contains) {
					element.setAttribute("unique", "resource-id");
					element.removeAttribute("xpath");
					element.setAttribute("xpath", "//" + JOOX.$(element).tag() + "[@resource-id='" + attribute + "']");
				} else {
					element.setAttribute("unique", "xpath");
				}
			}
		}
	}

	private static String indexing(NodeList list, Element element, String xpath, final StringBuilder str) {
		for (int i = 0; i < list.getLength(); i++) {
			org.w3c.dom.Node node = list.item(i);
			Element element2 = (Element) node;
			if (JOOX.$(element).xpath().equalsIgnoreCase(JOOX.$(element2).xpath())) {
				i = i + 1;
				return str.append("(" + xpath + ")[" + i + "]").toString();
			}
		}
		return null;
	}

	private static void ios(Document document, HashMap<String, Integer> hashmap) throws XPathExpressionException {
		NodeList nodeList = document.getElementsByTagName("*");
		for (int i = 0; i < nodeList.getLength(); i++) {
			HashMap<String, String> xpath = new HashMap<String, String>();
			String Absolute_path, indexed_xpath = null;
			org.w3c.dom.Node node = nodeList.item(i);
			Element element = (Element) node;
			NodeList list = null;
			String flowxpath = flowXpath(element, document);
			if (flowxpath != null) {
				element.setAttribute("FlowXpathByPosition", flowxpath);
			}
			Absolute_path = JOOX.$(element).xpath().replace("hierarchy[1]", "");
			element.setAttribute("xpath", Absolute_path);
			xpath = byattr(element);
			if (!xpath.isEmpty()) {
				for (String x : xpath.keySet()) {
					list = checkXpath(xpath.get(x), document);
					if (list != null) {
						if (list.getLength() == 1) {
							element.setAttribute(x, xpath.get(x));
						} else {
							indexed_xpath = indexing(list, element, xpath.get(x), new StringBuilder(""));
							// xpath = JOOX.$(element).xpath().replace("hierarchy[1]", "");
							element.setAttribute(x, indexed_xpath);
						}
					}
				}
			}
			final String attribute;
			int round1 = 0;
			int round2 = 0;
			int round3 = 0;
			int round4 = 0;
			if (element.getNodeName() != "hierarchy") {
				String att1 = element.getAttribute("x");
				String att2 = element.getAttribute("y");
				String att3 = element.getAttribute("width");
				String att4 = element.getAttribute("height");
				element.removeAttribute("x");
				element.removeAttribute("y");
				element.removeAttribute("width");
				element.removeAttribute("height");
				round1 = Integer.parseInt(att1);
				round2 = Integer.parseInt(att2);
				round3 = (Integer.parseInt(att1) + Integer.parseInt(att3));
				round4 = (Integer.parseInt(att2) + Integer.parseInt(att4));
				element.setAttribute("bounds", "[" + round1 + "," + round2 + "][" + round3 + "," + round4 + "]");
				element.setAttribute("unique", "xpath");
			}
			// if (!StringUtils.isEmpty((CharSequence) (attribute =
			// element.getAttribute("name")))
			// && hashmap.containsKey(attribute) && hashmap.get(attribute) == 1) {
			// element.setAttribute("unique", "name");
			// element.removeAttribute("xpath");
			// element.setAttribute("xpath", "//" + JOOX.$(element).tag() + "[@name='" +
			// attribute + "']");
			// } else {
			// element.setAttribute("unique", "xpath");
			// }
		}
	}

	private static HashMap<String, Integer> count_attr(final Document document, final String s) {
		final HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		final Iterator<Element> iterator = JOOX.$(document).xpath("//*").iterator();
		while (iterator.hasNext()) {
			final String attribute;
			if (!(attribute = iterator.next().getAttribute(s)).isEmpty()) {
				hashMap.put(attribute, ((hashMap.get(attribute) == null) ? 0 : hashMap.get(attribute)) + 1);
			}
		}
		return hashMap;
	}

	private static HashMap byattr(Element element) {
		HashMap<String, String> xpath = new HashMap<String, String>();
		if (!element.getTagName().toLowerCase().equals("hierarchy")) {
			String attr;
			if ((attr = element.getAttribute("text")) != null && !attr.isEmpty()) {
				xpath.put("XpathByText", "//" + element.getTagName() + "[@text = '" + attr + "']");
			}
			if ((attr = element.getAttribute("content-desc")) != null && !attr.isEmpty()) {
				xpath.put("XpathByContent-desc", "//" + element.getTagName() + "[@content-desc = '" + attr + "']");
			}
			if ((attr = element.getAttribute("class")) != null && !attr.isEmpty()) {
				xpath.put("XpathByClass", "//" + element.getTagName() + "[@class = '" + attr + "']");
			}
			if ((attr = element.getAttribute("name")) != null && !attr.isEmpty()) {
				xpath.put("XpathByName", "//" + element.getTagName() + "[@name = '" + attr + "']");
			}
			if ((attr = element.getAttribute("label")) != null && !attr.isEmpty()) {
				xpath.put("XpathByLabel", "//" + element.getTagName() + "[@label = '" + attr + "']");

			}
			if ((attr = element.getAttribute("value")) != null && !attr.isEmpty()) {
				xpath.put("XpathByValue", "//" + element.getTagName() + "[@value = '" + attr + "']");
			}

		}
		return xpath;
	}

	public static NodeList checkXpath(String x, Document document) throws XPathExpressionException {
		try {
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();
			XPathExpression expr = xpath.compile(x);
			NodeList nodes = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
			return nodes;
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static String DOMTOString(final Document n) throws Throwable {
		final StringWriter writer = new StringWriter();
		Throwable t = null;
		try {
			final Transformer transformer;
			(transformer = TransformerFactory.newInstance().newTransformer()).setOutputProperty("method", "html");
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			transformer.setOutputProperty("indent", "no");
			transformer.setOutputProperty("standalone", "yes");
			transformer.transform(new DOMSource(n), new StreamResult(writer));
			final String string = writer.toString();
			writer.close();
			return string;
		} catch (Throwable t3) {
			final Throwable t2 = t3;
			t = t3;
			throw t2;
		} finally {
			if (t != null) {
				try {
					writer.close();
				} catch (Throwable exception) {
					t.addSuppressed(exception);
				}
			} else {
				writer.close();
			}
		}
	}
}
