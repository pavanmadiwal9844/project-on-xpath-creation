package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Extensionexe extends HttpServlet {

	private JSONObject resobj;
	private String BrowserName = "";
	private static final long serialVersionUID = 1L;
	final Logger logger = LoggerFactory.getLogger(Extensionexe.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.setStatus(HttpStatus.OK_200);
		JSONObject rq = new JSONObject();
		rq.put("data", resobj);
		resp.getWriter().println(rq);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		File file = new java.io.File("exception.log");
		PrintStream ps = new java.io.PrintStream(file);
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace(ps);
		}
		resobj = new JSONObject(jb.toString());
		resobj.remove("application");
		resobj.remove("path");
		try {
			BrowserName = resobj.getString("browser");
			String Hosturl = resobj.getString("hosturl");
			switch (BrowserName) {
			case "Edge":
				Runtime.getRuntime()
						.exec("cmd /c  start microsoft-edge:" + Hosturl + "  --new-window --start-maximized");
				break;
			case "Firefox":
				Runtime.getRuntime().exec("cmd /c start firefox " + Hosturl + " --new-window --start-maximized");
				break;
			case "Chrome":
				Runtime.getRuntime().exec("cmd /c start chrome  " + Hosturl + "  --new-window  --start-maximized ");
				break;
			}
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			JSONObject res = new JSONObject();
			res.put("data", "success");
			resp.getWriter().println(res);

		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(HttpStatus.FORBIDDEN_403);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			JSONObject res = new JSONObject();
			res.put("data", "Failed");
			res.put("browser", BrowserName);
			resp.getWriter().println(res);

		}
	}
}
