package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.UserDetail;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.handler.DesktopService;

public class StartDesktopRecorder extends HttpServlet {
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(StartDesktopRecorder.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
		try {
			startSerivceManager();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			StringBuffer re = new StringBuffer();
			String line = null;
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			JSONObject resobject = new JSONObject(re.toString());
			UserDetail urdl = new UserDetail();
			urdl.setCustomerId(resobject.getInt("customerId"));
			urdl.setAuthkey(resobject.getString("authKey"));
			urdl.setProjectId(resobject.getInt("projectId"));
			urdl.setObjtemplateId(resobject.getInt("objtemplateId"));
			urdl.setCreatedBy(resobject.getInt("createdBy"));
			GlobalDetail.user = urdl;
			startRecorder();
			GlobalDetail.serverIP = resobject.getString("serverIp");
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", "Launched"));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void startRecorder() throws IOException, InterruptedException {
		String path = Paths.get("libs" + File.separator + "Desktop Recorder" + File.separator + "RecordUi.exe").toAbsolutePath().toString();
		Runtime run = Runtime.getRuntime();
		run.exec(path);
	}

	public void stopRecorder() throws IOException {
		Runtime.getRuntime().exec("taskkill /F /IM " + "RecordUi.exe");
	}

	public static void startSerivceManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					DesktopService.a().Reset();
					DesktopService.a().run().get();
					logger.info("Started handler");

				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				logger.info("stoped handler");
			}
		}).start();

	}



}
