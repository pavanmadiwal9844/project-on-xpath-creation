package com.simplifyqa.agent;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.UserDetail;
import com.simplifyqa.DTO.WebSteps;
import com.simplifyqa.desktoprecorder.DTO.OriginalData;
import com.simplifyqa.desktoprecorder.DTO.Testcase;
import com.simplifyqa.driver.WebDriver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;
import javax.websocket.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GlobalDetail {
  public static HashMap<String, Session> websocket = new HashMap<>();
  
  public static Session mainFrSession;
  
  public static Session desktopsession;
  
  public static Session sapsession;
  
  public static HashMap<String, byte[]> screenshot = (HashMap)new HashMap<>();
  
  public static HashMap<String, Thread> Run = new HashMap<>();
  
  public static HashMap<String, RemoteWebDriver> sqaDriver = new HashMap<>();
  
  public static HashMap<String, WebDriver> webDriver = new HashMap<>();
  
  public static HashMap<String, String> iosSession = new HashMap<>();
  
  public static int maXTimeout = 90;
  
  public static String xml = null;
  
  public static HashMap<String, String> hostname = new HashMap<>();
  
  public static HashMap<String, Integer> height = new HashMap<>();
  
  public static HashMap<String, Integer> width = new HashMap<>();
  
  public static List<Process> processList = new ArrayList<>();
  
  public static HashMap<String, Setupexec> setting = new HashMap<>();
  
  public static HashMap<String, Object> object = new HashMap<>();
  
  public static HashMap<String, String> refdetails = new HashMap<>();
  
  public static Map<String, Map<String, String>> curStepTd = new HashMap<>();
  
  public static Session session = null;
  
  public static Session notificationSession;
  
  public static Session debugSession;
  
  public static Session webInspectpr;
  
  public static HashMap<String, String> Wdaproxy = new HashMap<>();
  
  public static HashMap<String, String> Wdascreen = new HashMap<>();
  
  public static Boolean Status = Boolean.valueOf(false);
  
  public static String[] udid = new String[10];
  
  public static HashMap<String, JsonNode> runTime = new HashMap<>();
  
  public static HashMap<Integer, WebSteps> multiple = new HashMap<>();
  
  public static String PageName = null;
  
  public static UserDetail userDetail = null;
  
  public static Session sendStpesSession = null;
  
  public static JSONObject rcopl = null;
  
  public static String serverIP = null;
  
  public static String testcaseCode = null;
  
  public static WebSteps web = null;
  
  public static Session referencestep = null;
  
  public static Semaphore sem = null;
  
  public static UserDetail user = null;
  
  public static JSONObject DesktopSteps = new JSONObject();
  
  public static Testcase cases = null;
  
  public static Testcase MainFrame = null;
  
  public static JSONObject mainframeSteps = new JSONObject();
  
  public static JSONArray mainframeref = null;
  
  public static Boolean just;
  
  public static OriginalData or = null;
  
  public static HashMap<String, Boolean> apiautowire = new HashMap<>();
  
  public static HashMap<String, String> apirespautowire = new HashMap<>();
  
  public static HashMap<String, String> GrpcPort = new HashMap<>();
  
  public static HashMap<String, String> DeviceID = new HashMap<>();
  
  public static ExecutorService frame = null;
  
  public static HashMap<String, RemoteWebDriver> androiddriver_ce = new HashMap<>();
  
  public static String ResumeStep = null;
  
  public static boolean ReferencePlayback = false;
  
  public static boolean ResumePlayback = false;
  
  public static String PlaybackSessionID = null;
  
  
  
}
