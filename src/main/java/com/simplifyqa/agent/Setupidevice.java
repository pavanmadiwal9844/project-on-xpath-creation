package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import com.sun.jna.platform.win32.Netapi32Util.User;

import net.lingala.zip4j.core.ZipFile;

public class Setupidevice extends HttpServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Setupidevice.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		JSONObject res = new JSONObject();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject reqobj = new JSONObject(jb.toString());
		String message = " unable to create provision folder or unable to install the ipa, failed to verify code signature :";
		Boolean install = true;
		if (reqobj.has("devices") && reqobj.has("projectId") && reqobj.has("customerId")) {
			for (int i = 0; i < reqobj.getJSONArray("devices").length(); i++) {
				if (createIpaFolder(reqobj.getJSONArray("devices").getJSONObject(i).get("provId").toString())) {
					if (installIpa(reqobj.getJSONArray("devices").getJSONObject(i).get("provId").toString(),
							reqobj.getJSONArray("devices").getJSONObject(i).get("id").toString(), reqobj)) {
						install = true;
					} else {
						install = false;
						message += reqobj.getJSONArray("devices").getJSONObject(i).get("id").toString();
					}
				} else {
					install = false;
					message += reqobj.getJSONArray("devices").getJSONObject(i).get("id").toString();
				}

			}
			if (install) {
				res = new JSONObject();
				res.put("success", true).put("message", "Successfully installed the IPA");
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(res);
			} else {
				res = new JSONObject();
				res.put("success", false).put("message", message);
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(res);
			}
		} else {
			res.put("success", false).put("message", "configure data is missing");
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(res);
		}
	}

	protected Boolean createIpaFolder(String provID) {
		try {

			File f1 = new File(Paths.get(UserDir.getAapt().getAbsolutePath()).toString() + File.separator + "ios_ipa");
			if (f1.exists()) {
				File f2 = new File(
						Paths.get(UserDir.getAapt().getAbsolutePath() + File.separator + "ios_ipa").toString()
								+ File.separator + provID);
				if (f2.exists())
					return true;
				else {
					if (f2.mkdir())
						return true;
					else
						throw new Exception("InvalidFolderCreation");
				}

			} else if (f1.mkdir()) {
				File f2 = new File(
						Paths.get(UserDir.getAapt().getAbsolutePath() + File.separator + "ios_ipa").toString()
								+ File.separator + provID);
				if (f2.exists())
					return true;
				else {
					if (f2.mkdir())
						return true;
					else
						throw new Exception("InvalidFolderCreation");
				}

			} else
				throw new Exception("InvalidFolderCreation");

		} catch (Exception e) {
			logger.error("Unable to Create IPA Folder");
			return false;
		}

	}

	protected Boolean installIpa(String provID, String udid, JSONObject req) {
		try {
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("content-type", "application/json");
			headers.put("authorization", req.get("authKey").toString());
			File ipaFile = new File(Paths.get(".") + File.separator + "libs" + File.separator + "ios_ipa"
					+ File.separator + provID + File.separator + "SimplifyqaWDA.ipa");
			if (ipaFile.exists()) {
				ZipFile zipFile = new ZipFile(ipaFile.getAbsolutePath());
				File f2 = new File(
						Paths.get(UserDir.getAapt().getAbsolutePath() + File.separator + "ios_ipa").toString()
								+ File.separator + provID);
				zipFile.extractAll(f2.getAbsolutePath());
				return Idevice.installIPA(udid, f2.getAbsolutePath() + File.separator + "Payload" + File.separator
						+ "WebDriverAgentRunner-Runner.app");
			} else {
				JSONObject reqobj = new JSONObject();
				reqobj.put("projectId", req.get("projectId").toString());
				reqobj.put("customerId", req.get("customerId").toString());
				reqobj.put("provId", provID);
				InputStream in = HttpUtility.getFilePost(req.getString("executionIp") + "/ios/getipa",
						reqobj.toString(), headers);
				FileUtils.copyInputStreamToFile(in, ipaFile);
				ZipFile zipFile = new ZipFile(ipaFile.getAbsolutePath());
				File f2 = new File(
						Paths.get(UserDir.getAapt().getAbsolutePath() + File.separator + "ios_ipa").toString()
								+ File.separator + provID);
				zipFile.extractAll(f2.getAbsolutePath());
				return Idevice.installIPA(udid, f2.getAbsolutePath() + File.separator + "Payload" + File.separator
						+ "WebDriverAgentRunner-Runner.app");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Unable to Install IPA");
			return false;
		}
	}

}
