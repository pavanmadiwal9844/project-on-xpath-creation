package com.simplifyqa.agent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.Service;
import com.simplifyqa.mobile.android.Setautomator2;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;

import dorkbox.notify.Notify;
import dorkbox.notify.Pos;

public class RestartNotification extends HttpServlet {

	/**
	 * @author Srinivas
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.setStatus(HttpStatus.OK_200);
		ReStart();
	}

	public static void ReStart() {
		try {
			Service.a().killDriver();
			Setautomator2.killEmulator();
			Idevice.EndRunnerapp();
			for (String key : ideviceMaganger.devices.keySet()) {
				if (((JSONObject) ideviceMaganger.devices.get(key)).has("simulator"))
					Idevice.shutdownSimulator(key);
			}
			Notify.create()
					.image(ImageIO.read(new File(
							Paths.get(UserDir.getImage().getAbsolutePath(), new String[] { "loader.png" }).toUri())))
					.title("SimplifyQA Agent").text("Restarting Agent...").position(Pos.BOTTOM_RIGHT).darkStyle()
					.hideAfter(3000).show();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
