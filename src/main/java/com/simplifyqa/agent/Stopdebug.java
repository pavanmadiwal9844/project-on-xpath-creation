package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.simplifyqa.customMethod.Commands;
import com.simplifyqa.customMethod.Debugger;

public class Stopdebug extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		try {
			resp.getWriter().println(new JSONObject().put("data", stopDebug()));
		} catch (JSONException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}
	public String res = null;
	public String stopDebug() throws IOException, InterruptedException {
		Process process = null;
		BufferedReader stdInput;
		BufferedReader stdError;
		if (SystemUtils.IS_OS_WINDOWS) {
			String[] command = { "cmd", "/c", "netstat", "-ano", "|", "findStr", "4013" };
			process = Runtime.getRuntime().exec(command);
			stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String s = null;
			

			if (stdInput.readLine() != null) {
				while ((s = stdInput.readLine()) != null) {
					if (Debugger.p != null) {
						if (Debugger.p.isAlive()) {
							Debugger.p.destroyForcibly();
						}
					}
					if (Commands.process != null) {
						if (Commands.process.isAlive()) {
							Commands.process.destroyForcibly();
						}
					}
					System.out.println(s);
					res = s;
					String pid = null;
					if (res.contains("LISTENING")) {
						String a[] = res.split("LISTENING ");
						pid = a[1].trim();
					} else if (res.contains("ESTABLISHED")) {
						String a[] = res.split("ESTABLISHED ");
						pid = a[1].trim();
					}
					String cmd[] = { "cmd", "/c", "taskkill", "/f", "/pid", pid };
					if (pid != null) {
						process = Runtime.getRuntime().exec(cmd);
						stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
						while ((s = stdInput.readLine()) != null) {
							System.out.println(s);
							res = s;
						}
						while ((s = stdError.readLine()) != null) {
							System.out.println(s);
						}
					} else {
						while ((s = stdError.readLine()) != null) {
							System.out.println(s);
							res = s;
						}
					}
				}
			} else {
				res = "Nothing to Stop";
			}

			return res;
		}
		else if(SystemUtils.IS_OS_MAC | SystemUtils.IS_OS_LINUX) {
			ProcessBuilder processs = new ProcessBuilder("lsof", "-ti:4013 x","|", "xargs", "kill");
			processs.start();
			return res = "Killed successfully";
		}
		return res;
	}

}
