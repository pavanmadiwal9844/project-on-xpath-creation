package com.simplifyqa.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.IDevice;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.execution.Startexecution;
import com.simplifyqa.handler.Service;
import com.simplifyqa.mobile.android.DeviceManager;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.OkHttpClient;

public class jenkinsocket {

	static public Socket socket = null;
	static public String socketUdid = null;

	final Logger logger = LoggerFactory.getLogger(jenkinsocket.class);

	public void test() throws URISyntaxException, IOException {
		Properties prop = new Properties();
		Properties prop1 = new Properties();
		FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
		String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "jenkins.properties" }).toString();
		;
		File file = new File(path);
		if (file.exists()) {
			FileInputStream jenkin = new FileInputStream(UserDir.getAapt() + "/jenkins.properties");
			prop1.load(jenkin);
		}
		prop.load(ip);

		String s = prop.getProperty("url");
        IO.Options opts = new IO.Options();
        OkHttpClient client = bypassSSL();
        opts.callFactory = client;
        opts.webSocketFactory = client;
		socket = IO.socket(s,opts);

//		String s = prop.getProperty("url");
//		socket = IO.socket(s);

		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				logger.info("jenkins Socket connected");
				socket.emit("in-jenkins", "hi");
				if (file.exists()) {
					socket.emit("agentonoff", prop1.getProperty("uuid"));
					socketUdid = prop1.getProperty("uuid");
					for (Entry<String, IDevice> map : DeviceManager.devices.entrySet()) {
						socket.emit("mobileconnected",
								new JSONObject().put("deviceId", map.getValue().getSerialNumber()).put("agentId",
										prop1.getProperty("uuid")));
					}
				}
// socket.disconnect();
			}

		}).on("execute-jenkins", new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				try {
					JSONObject req = new JSONObject(args[0].toString());
					JSONObject res = new JSONObject();
					res.put("customerID", req.get("customerId").toString())
							.put("projectID", req.get("projectId").toString()).put("execID", req.get("id").toString())
							.put("authKey", req.getString("authkey")).put("serverIp", prop.getProperty("url"))
							.put("suiteID", req.get("suiteId").toString());
					File file = new java.io.File("exception.log");
					PrintStream ps = new java.io.PrintStream(file);
					String path = UserDir.Webagent().getAbsolutePath();
					StringBuilder command = SystemUtils.IS_OS_WINDOWS
							? new StringBuilder("java -jar \"" + path + File.separator + "SimplifyQA.jar\" ")
							: new StringBuilder("java -jar " + path + File.separator + "SimplifyQA.jar ");
					File fl = new java.io.File(path);
					FileWriter f = new FileWriter(file);
					f.write(path);
					f.close();
					if ((!fl.exists())) {

						logger.info("data", "specified path does not exist , Jar not found!!");
					} else {

						Iterator<String> jsonkey = res.keys();
						while (jsonkey.hasNext()) {
							String key = (String) jsonkey.next();
							command.append("-" + key + " " + res.get(key) + " ");
						}
						final ProcessBuilder process = new ProcessBuilder(Arrays.asList(command.toString().split(" ")));
						process.redirectOutput(ProcessBuilder.Redirect.to(new File("QAWEB.log")));
						process.redirectError(ProcessBuilder.Redirect.to(new File("QAWEB.log")));
						process.directory(new File(path));
						process.start();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}).on("suite-jenkinsExecution", new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				Service.a().destroy();
				try {
// Service.a().killProcess();
// Service.a().killIproxy();
					Service.a().Reset();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				JSONObject resobj = new JSONObject(args[0].toString());
				Startexecution start = new Startexecution();
				try {
					start.Setexecution(resobj);
					start.execute();
				} catch (Exception e) {
// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				logger.info("jenkins Socket disconnected");
			}

		});
		socket.connect();
	}

	public static OkHttpClient bypassSSL() {

		HostnameVerifier myHostnameVerifier = new HostnameVerifier() {

			@Override

			public boolean verify(String hostname, SSLSession session) {

				return true;

			}

		};

		final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			@Override

			public void checkClientTrusted(final X509Certificate[] chain, final String authType) {

			}

			@Override

			public void checkServerTrusted(final X509Certificate[] chain, final String authType) {

			}

			@Override

			public X509Certificate[] getAcceptedIssuers() {

				return new X509Certificate[] {};

			}

		} };

		SSLContext mySSLContext = null;

		try {

			mySSLContext = SSLContext.getInstance("TLS");

			try {

				mySSLContext.init(null, trustAllCerts, null);

			} catch (KeyManagementException e) {

				e.printStackTrace();

			}

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();

		}

		return new OkHttpClient.Builder().hostnameVerifier(myHostnameVerifier)
				.sslSocketFactory(mySSLContext.getSocketFactory(), new X509TrustManager() {

					@Override

					public void checkClientTrusted(final X509Certificate[] chain, final String authType) {

					}

					@Override

					public void checkServerTrusted(final X509Certificate[] chain, final String authType) {

					}

					@Override

					public X509Certificate[] getAcceptedIssuers() {

						return new X509Certificate[] {};

					}

				}).build();

	}
}
