package com.simplifyqa.agent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;

import com.simplifyqa.Utility.UserDir;

import dorkbox.notify.Notify;
import dorkbox.notify.Pos;

public class StartNotification extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.setStatus(HttpStatus.OK_200);
		Start();
	}

	public static void Start() {
		try {
			Notify.create()
					.image(ImageIO.read(new File(
							Paths.get(UserDir.getImage().getAbsolutePath(), new String[] { "loader.png" }).toUri())))
					.title("SimplifyQA Agent").text("Starting Agent...").position(Pos.BOTTOM_RIGHT)
//	      .shake(250, 5)
					.darkStyle().hideAfter(3000).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
