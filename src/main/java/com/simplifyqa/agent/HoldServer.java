package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.method.AndroidMethod;

public class HoldServer extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	 
	 protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException
	    {
	    	resp.setStatus(HttpStatus.OK_200);
	    	resp.addHeader("Access-Control-Allow-Origin", "*");
    		resp.addHeader("Access-Control-Allow-Headers", "*");
    		resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println();
	    }
   protected void doPost(HttpServletRequest req, HttpServletResponse resp)
           throws ServletException, IOException {
	   
   	StringBuffer jb =new StringBuffer();
   	String line = null;
   	try 
   	{
   			BufferedReader reader = req.getReader();
   			while ((line = reader.readLine()) != null)
   				jb.append(line);
    } 
   	catch (Exception e) 	  
   	  {
   		  e.printStackTrace();
   	  }
   	JSONObject resobj=new JSONObject(jb.toString());
   	System.out.println(resobj.toString());
   	Iterator<String> keyItr=resobj.keys();
   	while(keyItr.hasNext()) 
   	{
   		String key=keyItr.next();
   		switch(key) {
   		case "holdEvent":  
   			
				   			JSONObject obj = new JSONObject();
							obj=(JSONObject) resobj.get(key);
							this.x1= obj.getInt("x1");
							this.y1=obj.getInt("y1");
							this.x2= obj.getInt("x2");
							this.y2=obj.getInt("y2");
							Thread.currentThread().setName(obj.get("id").toString());
							AndroidMethod Sqa_driver=new AndroidMethod();
							Sqa_driver.setDevice(obj.get("id").toString());
							Sqa_driver.HoldByAdb(x1, y1, x2, y2);
							resp.setStatus(HttpStatus.OK_200);
							resp.addHeader("Access-Control-Allow-Origin", "*");
				    		resp.addHeader("Access-Control-Allow-Headers", "*");
				    		resp.addHeader("Access-Control-Allow-Methods", "*");
							resp.getWriter().println();
							
   		}
   	}
   }
}