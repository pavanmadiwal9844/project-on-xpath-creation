package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.execution.Startexecution;
import com.simplifyqa.handler.Service;

public class ExecutionStep extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		Service.a().destroy();
//		Service.a().killProcess();
//		Service.a().killIproxy();
		Service.a().Reset();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		GlobalDetail.ResumePlayback=false;
		GlobalDetail.ReferencePlayback=false;
		JSONObject resobj = new JSONObject(jb.toString());
		Startexecution start = new Startexecution();
		try {
			File file = new File(UserDir.getAapt() + "/config.properties");
			if (file.exists()) {
				FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
				Properties prop = new Properties();
				prop.load(ip);
				if (prop.containsKey("serverurl"))
					resobj.put("serverIp", prop.getProperty("serverurl"));
			}
			start.Setexecution(resobj);
			start.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(new JSONObject().put("data", "success"));
	}
}
