package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Status extends HttpServlet{

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static Logger logger = LoggerFactory.getLogger(Status.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(re.toString());
		if (resobj.has("document") && resobj.getString("document").equalsIgnoreCase("completed")) {
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			if (GlobalDetail.rcopl.getString("name").equals("recorder")) {
				resp.getWriter().println(GlobalDetail.rcopl);
			} else if (GlobalDetail.rcopl.getString("name").equals("Playback")) {
				resp.getWriter().println(GlobalDetail.rcopl);
			} else {
				resp.getWriter().println(GlobalDetail.rcopl);
			}
		}
	}


	

}
