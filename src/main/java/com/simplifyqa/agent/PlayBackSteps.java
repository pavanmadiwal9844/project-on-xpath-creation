package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.WebSteps;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.execution.Startexecution;
import com.simplifyqa.manager.StepManager;

public class PlayBackSteps extends HttpServlet {

	/**
	 * author = "bhaskar"
	 */

	private static final long serialVersionUID = 1L;
	public static Logger logger = LoggerFactory.getLogger(CreateStep.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);

			JSONObject jsonobj = new JSONObject(re.toString());
			if(jsonobj.has("StepIndex")) {
				GlobalDetail.ResumeStep = jsonobj.get("StepIndex").toString();
				GlobalDetail.ResumePlayback = true;
				}
			// adding (reference playback for projectwise codeeditor)

			HashMap<String, String> header = new HashMap<String, String>();
			GlobalDetail.ReferencePlayback = true;
			header.put("Content-Type", "application/json");
			header.put("authorization", jsonobj.get("authKey").toString());
			File file = new File(UserDir.getAapt() + "/config.properties");
			if (file.exists()) {
				FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
				Properties prop = new Properties();
				prop.load(ip);
				if (prop.containsKey("serverurl"))
					jsonobj.put("serverIp", prop.getProperty("serverurl"));
			}
			Startexecution start = new Startexecution();
			start.Setexecution(jsonobj);
			start.execute();
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", "success"));
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", "failed"));
		}

	}

}
