package com.simplifyqa.agent;

import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

public class server extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.setStatus(200);
		JSONObject rq = new JSONObject();
		rq.put("success", "sqaAgent");
		resp.getWriter().println(rq.toString());
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		String command = "";
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			JSONArray re;
			JSONObject res;
			String key = keyItr.next();
			String str1;
			switch ((str1 = key).hashCode()) {
			case 1559801053:
				if (!str1.equals("devices"))
					continue;
				re = new JSONArray();
				res = new JSONObject();
				if (DeviceManager.devices.isEmpty() && ideviceMaganger.devices.isEmpty()
						&& DeviceManager.emulators.isEmpty()) {
					res.put("Error", "No devices found!");
					resp.setStatus(200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(res.toString());
					continue;
				}
				if (!DeviceManager.devices.isEmpty()) {
					String size = null;
					for (Map.Entry<String, IDevice> map : (Iterable<Map.Entry<String, IDevice>>) DeviceManager.devices
							.entrySet()) {
						if (!DeviceManager.emulators.containsKey(map.getValue().getAvdName())) {
							if (!((IDevice) map.getValue()).getSerialNumber()
									.equals(((IDevice) map.getValue()).getName())) {
								JSONObject r = (new JSONObject()).put("id",
										((IDevice) map.getValue()).getSerialNumber());
								Thread.currentThread().setName(((IDevice) map.getValue()).getSerialNumber());
								r.put("name", ((IDevice) map.getValue()).getName());
								r.put("type", "android");
								r.put("androidVersion", ((IDevice) map.getValue()).getProperty("ro.build.version.sdk"));
								r.put("wifi", (new AndroidMethod()).wifiStatus());
								r.put("data", (new AndroidMethod()).dataStatus());
								r.put("bluetooth", (new AndroidMethod()).bluetoothStatus());
								r.put("airplanemode", (new AndroidMethod()).airplaneModeStatus());
								command = "wm size";
								CollectingOutputReceiver info = new CollectingOutputReceiver();
								try {
									info = new CollectingOutputReceiver();
									((IDevice) map.getValue()).executeShellCommand(command,
											(IShellOutputReceiver) info);
									size = info.getOutput().split(":")[1].replace("\r\n", "").split("\n")[0];
									GlobalDetail.width.put(((IDevice) map.getValue()).getSerialNumber(),
											Integer.valueOf(Integer.parseInt(size.split("x")[0].trim())));
									GlobalDetail.height.put(((IDevice) map.getValue()).getSerialNumber(),
											Integer.valueOf(Integer.parseInt(size.split("x")[1].trim())));
									r.put("size", info.getOutput().split(":")[1].replace("\r\n", "").split("\n")[0]);
								} catch (TimeoutException | com.android.ddmlib.AdbCommandRejectedException
										| com.android.ddmlib.ShellCommandUnresponsiveException e) {
									e.printStackTrace();
								}
								re.put(r);
							}
						}
					}
				}
				if (!ideviceMaganger.devices.isEmpty())
					for (Map.Entry<String, JSONObject> deivcemap : (Iterable<Map.Entry<String, JSONObject>>) ideviceMaganger.devices
							.entrySet()) {
						JSONObject r = (new JSONObject()).put("id",
								((JSONObject) deivcemap.getValue()).getString("id"));
						r.put("iosVersion", ((JSONObject) deivcemap.getValue()).getString("iosVersion"));
						r.put("name", ((JSONObject) deivcemap.getValue()).getString("name"));
						r.put("type", "ios");
						if (((JSONObject) deivcemap.getValue()).has("simulator"))
							r.put("simulator", ((JSONObject) deivcemap.getValue()).getString("simulator"));
						re.put(r);
					}
				if (!DeviceManager.emulators.isEmpty()) {
					for (Map.Entry<String, Boolean> devicemap : (Iterable<Map.Entry<String, Boolean>>) DeviceManager.emulators
							.entrySet()) {
//						if (devicemap.getValue() == false) {
							JSONObject r = new JSONObject().put("id", devicemap.getKey());
							r.put("name", devicemap.getKey());
							r.put("type", "android");
							r.put("emulator", true);
							re.put(r);
//						}
					}
				}
				try {
					res.put("devices", re);
					resp.setStatus(200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(res.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
