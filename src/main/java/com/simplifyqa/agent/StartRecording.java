package com.simplifyqa.agent;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartRecording extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  private String deviceId;
  
  private static final Logger logger = LoggerFactory.getLogger(Apps.class);
  
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setStatus(200);
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.addHeader("Access-Control-Allow-Headers", "*");
    resp.addHeader("Access-Control-Allow-Methods", "*");
    resp.getWriter().println();
  }
  
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      BufferedReader reader = req.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line); 
    } catch (Exception e) {
      e.printStackTrace();
    } 
    JSONObject resobj = new JSONObject(jb.toString());
    System.out.println(resobj.toString());
    Iterator<String> keyItr = resobj.keys();
    while (keyItr.hasNext()) {
      JSONObject res, obj;
      String key = keyItr.next();
      String str1;
      switch ((str1 = key).hashCode()) {
        case -934908847:
          if (!str1.equals("record"))
            continue; 
          res = new JSONObject();
          obj = new JSONObject();
          obj = (JSONObject)resobj.get(key);
          this.deviceId = obj.get("id").toString();
          Thread.currentThread().setName(this.deviceId);
          if (DeviceManager.devices.isEmpty() && ideviceMaganger.devices.isEmpty()) {
            res.put("Error", "No devices found!");
            resp.setStatus(200);
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Headers", "*");
            resp.addHeader("Access-Control-Allow-Methods", "*");
            resp.getWriter().println(res);
            continue;
          } 
          try {
            GlobalDetail.hostname.clear();
            InitializeDependence.Appiumrun.clear();
            Service.a().destroy();
            Service.a().killProcess();
            Service.a().Reset();
            Thread.sleep(2000L);
            if (obj.getString("type").equals("android")) {
              AndroidMethod Sqa_driver = new AndroidMethod();
              if (GlobalDetail.hostname.isEmpty() && Sqa_driver.SetUiautomor2(this.deviceId, Boolean.valueOf(false))) {
                res.put("data", "Recording");
                res.put("size", (
                    new StringBuilder()).append(GlobalDetail.width.get(this.deviceId)).append("x").append(GlobalDetail.height.get(this.deviceId)).toString());
                GlobalDetail.hostname.put(this.deviceId, Sqa_driver.hostname);
                resp.setStatus(200);
                resp.addHeader("Access-Control-Allow-Origin", "*");
                resp.addHeader("Access-Control-Allow-Headers", "*");
                resp.addHeader("Access-Control-Allow-Methods", "*");
                resp.getWriter().println(res);
                startDeviceManager(this.deviceId, obj.getString("type"));
                continue;
              } 
              if (!GlobalDetail.hostname.isEmpty()) {
                res.put("data", "Recording");
                res.put("size", (
                    new StringBuilder()).append(GlobalDetail.width.get(this.deviceId)).append("x").append(GlobalDetail.height.get(this.deviceId)).toString());
                Sqa_driver.hostname = GlobalDetail.hostname.get(this.deviceId);
                resp.setStatus(200);
                resp.addHeader("Access-Control-Allow-Origin", "*");
                resp.addHeader("Access-Control-Allow-Headers", "*");
                resp.addHeader("Access-Control-Allow-Methods", "*");
                resp.getWriter().println(res);
                continue;
              } 
              res.put("Error", "unable to start Recording");
              resp.setStatus(200);
              resp.addHeader("Access-Control-Allow-Origin", "*");
              resp.addHeader("Access-Control-Allow-Headers", "*");
              resp.addHeader("Access-Control-Allow-Methods", "*");
              resp.getWriter().println(res);
              continue;
            } 
            InitializeDependence.xctestrun.remove(this.deviceId);
            if (Idevice.startSession(this.deviceId)) {
              IosMethod sqaDriver = new IosMethod();
              sqaDriver.setSessionId(GlobalDetail.iosSession.get(this.deviceId));
              sqaDriver.setIdeviceId(this.deviceId);
              JSONObject size = sqaDriver.size();
              GlobalDetail.width.put(this.deviceId, Integer.valueOf(size.getJSONObject("value").getInt("width")));
              GlobalDetail.height.put(this.deviceId, Integer.valueOf(size.getJSONObject("value").getInt("height")));
              res.put("data", "Recording");
              res.put("size", (
                  new StringBuilder()).append(GlobalDetail.width.get(this.deviceId)).append("x").append(GlobalDetail.height.get(this.deviceId)).toString());
              res.put("orientation", sqaDriver.getOrientation());
              resp.setStatus(200);
              resp.addHeader("Access-Control-Allow-Origin", "*");
              resp.addHeader("Access-Control-Allow-Headers", "*");
              resp.addHeader("Access-Control-Allow-Methods", "*");
              resp.getWriter().println(res);
              startDeviceManager(this.deviceId, obj.getString("type"));
              continue;
            } 
            res.put("Error", "unable to start Recording");
            resp.setStatus(200);
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Headers", "*");
            resp.addHeader("Access-Control-Allow-Methods", "*");
            resp.getWriter().println(res);
          } catch (Exception e) {
            e.printStackTrace();
          } 
      } 
    } 
  }
  
  private void startDeviceManager(final String str1, final String str2) throws InterruptedException, ExecutionException {
    (new Thread(new Runnable() {
          public void run() {
            Service.a().b(Events.Intail);
            try {
              Service.a().set(str1);
              Service.a().setType(str2);
              Service.a().run().get();
              StartRecording.logger.info("Agent Started");
            } catch (InterruptedException|ExecutionException e) {
              e.printStackTrace();
            } 
            StartRecording.logger.info("Agent stoped");
          }
        })).start();
  }
}
