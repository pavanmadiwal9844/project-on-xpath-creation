package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.IDevice;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.ideviceMaganger;

public class Apps extends HttpServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Apps.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager.getLogger(Apps.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		System.out.println(resobj.toString());
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			String key = keyItr.next();
			switch (key) {
			case "apps":
				JSONObject res = new JSONObject();
				JSONObject obj = new JSONObject();
				obj = (JSONObject) resobj.get(key);
				try {
					if (DeviceManager.devices.isEmpty() && ideviceMaganger.devices.isEmpty()) {
						res.put("Error", "No devices found!");
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(res);
					} else {
						try {
							Thread.currentThread().setName(obj.get("id").toString());
							if (obj.getString("type").equals("android") || obj.getString("type").equals("emulator")) {
								res.put("data", new AndroidMethod().getPackages(obj.get("id").toString()));
							} else {
								res.put("data", new IosMethod().getPackages(obj.get("id").toString()));
							}
							logger.info("Got all packages");
							resp.setStatus(HttpStatus.OK_200);
							resp.addHeader("Access-Control-Allow-Origin", "*");
							resp.addHeader("Access-Control-Allow-Headers", "*");
							resp.addHeader("Access-Control-Allow-Methods", "*");
							resp.getWriter().println(res);
						} catch (IOException e) {
							logger.error("Unable to get Packages!!!");
							e.printStackTrace();

						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
		}
	}

}
