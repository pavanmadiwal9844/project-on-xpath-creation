package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.Utility.UtilEnum;

public class ProxyServer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Logger logger = LoggerFactory.getLogger(Webexe.class);
	public static Boolean proxyflag = false;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobject = null;
		resobject = new JSONObject(re.toString());
		String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" }).toString();
		File proxyfile = new File(path);
		File file = new File(path);
		FileWriter fileWritter = new FileWriter(path, true);
		BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		bufferWritter.append(resobject.toString());
		Properties p = new Properties();
		p.put("proxyip", resobject.getString("proxyip"));
		p.put("port", resobject.getString("port"));
		p.put("username", resobject.getString("proxyusername"));
		p.put("password", resobject.getString("proxypassword"));
//			p.put("proxyflag", resobject.getBoolean("proxyflag"));
		if (resobject.getBoolean("proxyflag"))
			p.put("proxyflag", "1");
		else
			p.put("proxyflag", "0");
		FileOutputStream outputStrem = new FileOutputStream(path);
		p.store(outputStrem, "This is a sample properties file");
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Content-type", "application/json");
		hash.put("authorization", resobject.getString("authKey"));
		JSONObject respo = new JSONObject().put("validated", Boolean.FALSE);
		respo.put("proxy", "added");
//			JSONObject js = 
		if (resobject.getBoolean("validate")) {
			try {
				SearchColumns searchs = new SearchColumns();
				searchs.getSearchColumns().add(new searchColumnDTO("customerId", "3356",
						Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
				searchs.getSearchColumns().add(new searchColumnDTO("projectId", "1234",
						Boolean.parseBoolean(UtilEnum.FALSE.value()), "integer"));
				searchs.getSearchColumns().add(new searchColumnDTO("disname", "Sqa_desktop",
						Boolean.parseBoolean(UtilEnum.FALSE.value()), "string"));
				searchs.setCollection("object");
				searchs.setLimit(100000);
				searchs.setStartIndex(0);
				JSONObject object = new JSONObject(new ObjectMapper().writeValueAsString(searchs));
				JSONObject response = new JSONObject(
						HttpUtility.SendPost(resobject.getString("serverIp") + "/search", object.toString(), hash));
				respo.put("validated", Boolean.TRUE);
				System.out.println("validated");
			} catch (Exception e) {
				respo.put("validated", Boolean.FALSE);
				e.printStackTrace();
			}
		}
		this.proxyflag = resobject.getBoolean("proxyflag");

		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(respo);
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" }).toString();
		File file = new File(path);
		Properties p = new Properties();
		FileReader reader = new FileReader(path);
		p.load(reader);
		JSONObject json = new JSONObject();
		if (file.exists() && p.getProperty("proxyflag").equals("1"))
			json.put("file", Boolean.TRUE);
		else
			json.put("file", Boolean.FALSE);
		if (!p.isEmpty()) {
			if (p.getProperty("proxyip") != null)
				json.put("proxyip", p.getProperty("proxyip"));
			if (p.getProperty("port") != null)
				json.put("port", p.getProperty("port"));
			if (p.getProperty("username") != null)
				json.put("username", p.getProperty("username"));
			if (p.getProperty("password") != null)
				json.put("password", p.getProperty("password"));
		}
		file.exists();
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		if (file.exists())
			resp.getWriter().println(json);
		else {
			resp.getWriter().println(json);
		}

	}
}
