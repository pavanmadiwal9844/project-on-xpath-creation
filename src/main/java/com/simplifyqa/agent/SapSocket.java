package com.simplifyqa.agent;
import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.method.SapMethod;
@ServerEndpoint(value="/Sap")
public class SapSocket {
	@OnOpen
	public void onOpen(Session session) throws IOException {
		System.out.println("WebSocket opened SAP: " + session.getId());
		 session.setMaxTextMessageBufferSize(4 * 1024 * 1024); // 4MB
	}

	@OnMessage
	public void onMessage(String res, Session session) {
		System.out.println("Message received: " + res);
		JSONObject request = new JSONObject(res);
		GlobalDetail.sapsession = session;
		try {
			if(request.has("sap")) {
				GlobalDetail.sem.release();
			}
			if(request.has("validation")){
				GlobalDetail.sendStpesSession.getAsyncRemote().sendText(request.toString());
			}else {			
				InitializeDependence.sapAction.release(request);
				SapMethod.release = false;
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@OnClose
	public void onClose(CloseReason reason, Session session) throws IOException {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
	}
}
