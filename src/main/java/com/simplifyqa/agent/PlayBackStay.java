package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.manager.StepManager;

public class PlayBackStay extends HttpServlet{
	
	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = -2643081358985941362L;
	public static Logger logger = LoggerFactory.getLogger(CreateStep.class);

	public void doOptions(HttpServletRequest requ, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	public void doPost(HttpServletRequest requ, HttpServletResponse resp) throws IOException {
		StringBuffer str = new StringBuffer();
		String line = null;
		BufferedReader buffer = requ.getReader();
		while ((line = buffer.readLine()) != null) {
			str.append(line);
		}
		JSONObject json = new JSONObject(str.toString());
		StepManager sqadriver = new StepManager();
		logger.info(json.toString());
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		try {
			if (json.has("play_back")) {
				if (json.has("index") && json.getInt("index") == GlobalDetail.web.getIndex()
						&& json.getString("play_back").equals("recived")) {
					GlobalDetail.web.setIndex((GlobalDetail.web.getIndex()) + 1);
					if (GlobalDetail.web.getIndex() < GlobalDetail.web.getArraysteps().size()) {
						logger.info((GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()).toString()));
						resp.getWriter().println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
						System.out.println((GlobalDetail.web.getIndex()));
						System.out.println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
						if (!GlobalDetail.web.getReference()) {
							sqadriver.pass_Fail(json, json.getInt("index"));
						}
					} else {
						logger.info("steps got completed");
						if (!GlobalDetail.web.getReference()) {
							sqadriver.pass_Fail(json, json.getInt("index"));
							sqadriver.FOR("recived", json.getInt("index"));
						}
					}

				} else if (json.has("index") && json.getString("play_back").equals("wait")) {
					logger.info(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()).toString());
					resp.getWriter().println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
					if (json.has("count")) {
						if (json.getInt("count") == 15) {
							if (!GlobalDetail.web.getReference()) {
								sqadriver.pass_Fail(json, GlobalDetail.web.getIndex());
							}
						}
					}
				} else if (!json.has("index") && GlobalDetail.web.getIndex() > 1
						&& GlobalDetail.web.getArraysteps().size() > GlobalDetail.web.getIndex()) {
					logger.info(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()).toString());
					resp.getWriter().println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
					System.out.println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
					System.out.println(GlobalDetail.web.getIndex());


				} else if (!json.has("index") && GlobalDetail.web.getIndex() == 1) {
					logger.info(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()).toString());
					resp.getWriter().println(GlobalDetail.web.getArraysteps().get(GlobalDetail.web.getIndex()));
					if (!GlobalDetail.web.getReference()) {
						sqadriver.pass_Fail(new JSONObject().put("play_back", "recived"),
								GlobalDetail.web.getIndex() - 1);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
