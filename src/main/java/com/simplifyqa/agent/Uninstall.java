package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.method.AndroidMethod;

public class Uninstall extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
    	resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "Origin");
		resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
		resp.addHeader("Access-Control-Allow-Headers", "Content-Type");
		resp.addHeader("Access-Control-Allow-Methods", "POST");
		resp.addHeader("Access-Control-Allow-Methods", "OPTIONS");
		resp.getWriter().println();
    }
 
protected void doPost(HttpServletRequest req, HttpServletResponse resp)
       throws ServletException, IOException {
	StringBuffer jb =new StringBuffer();
	 String line = null;
	  try {
	    BufferedReader reader = req.getReader();
	    while ((line = reader.readLine()) != null)
	      jb.append(line);
	  } catch (Exception e)
	  
	  {
		  e.printStackTrace();
	  }
	JSONObject resobj=new JSONObject(jb.toString());
	System.out.println(resobj.toString());
	Iterator<String> keyItr=resobj.keys();
	while(keyItr.hasNext()) 
	{
		String key=keyItr.next();
		switch(key) {
   		case "uninstall":  	
   							JSONObject obj = new JSONObject();
   							obj=(JSONObject) resobj.get(key);
   							Thread.currentThread().setName(obj.getString("id"));
				   			AndroidMethod Sqa_driver=new AndroidMethod();
							Sqa_driver.setDevice(obj.get("id").toString());
							Sqa_driver.UnInstallApp(obj.getString("package"));	
							resp.setStatus(HttpStatus.OK_200);
							resp.addHeader("Access-Control-Allow-Origin", "*");
							resp.addHeader("Access-Control-Allow-Headers", "Origin");
							resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
							resp.addHeader("Access-Control-Allow-Headers", "Content-Type");
							resp.addHeader("Access-Control-Allow-Methods", "POST");
							resp.addHeader("Access-Control-Allow-Methods", "OPTIONS");
							resp.getWriter().println();
			
		}
	}
}

}