package com.simplifyqa.agent;

import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.mobile.ios.Idevice;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Install extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  private static final Logger logger = LoggerFactory.getLogger(Install.class);
  
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setStatus(200);
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.addHeader("Access-Control-Allow-Headers", "*");
    resp.addHeader("Access-Control-Allow-Methods", "*");
    resp.getWriter().println();
  }
  
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      BufferedReader reader = req.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line); 
    } catch (Exception e) {
      e.printStackTrace();
    } 
    JSONObject resobj = new JSONObject(jb.toString());
    System.out.println(resobj.toString());
    Iterator<String> keyItr = resobj.keys();
    while (keyItr.hasNext()) {
      JSONObject obj;
      IosMethod driverAction;
      String key = keyItr.next();
      String str1;
      switch ((str1 = key).hashCode()) {
        case -672744069:
          if (!str1.equals("Install"))
            continue; 
          obj = new JSONObject();
          obj = (JSONObject)resobj.get(key);
          Thread.currentThread().setName(obj.getString("id"));
          if (obj.getString("type").equalsIgnoreCase("android")) {
            AndroidMethod Sqa_driver = new AndroidMethod();
            Sqa_driver.setDevice(obj.get("id").toString());
            if (Sqa_driver.installapp(obj.getString("path")).booleanValue()) {
              resp.setStatus(200);
              resp.addHeader("Access-Control-Allow-Origin", "*");
              resp.addHeader("Access-Control-Allow-Headers", "*");
              resp.addHeader("Access-Control-Allow-Methods", "*");
              logger.info("Uploaded");
              resp.getWriter().println((new JSONObject()).put("file", "Uploaded"));
              continue;
            } 
            resp.setStatus(200);
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Headers", "*");
            resp.addHeader("Access-Control-Allow-Methods", "*");
            logger.info("Error");
            resp.getWriter().println((new JSONObject()).put("file", "Failed"));
            continue;
          } 
          driverAction = new IosMethod();
          driverAction.setDevice(obj.get("id").toString());
          Idevice.startSession(obj.get("id").toString());
          if (driverAction.installipa(obj.getString("path")).booleanValue()) {
            resp.setStatus(200);
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.addHeader("Access-Control-Allow-Headers", "*");
            resp.addHeader("Access-Control-Allow-Methods", "*");
            logger.info("Uploaded");
            resp.getWriter().println((new JSONObject()).put("file", "Uploaded"));
            continue;
          } 
          resp.setStatus(200);
          resp.addHeader("Access-Control-Allow-Origin", "*");
          resp.addHeader("Access-Control-Allow-Headers", "*");
          resp.addHeader("Access-Control-Allow-Methods", "*");
          logger.info("Error");
          resp.getWriter().println((new JSONObject()).put("file", "Failed"));
      } 
    } 
  }
}
