package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.DTO.UserDetail;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.handler.WebService;
import com.simplifyqa.manager.StepManager;

public class PauseResume extends HttpServlet {

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuffer re = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);

			// pause and resume in recorder (web)
			JSONObject obj = new JSONObject(re.toString());

			if (obj.has("pause")) {
				if (obj.get("pause").equals(true)) {
					if (obj.get("type").equals("web record"))
						InitializeDependence.pause = true;

					if (obj.get("type").equals("mobile record"))
						InitializeDependence.mobile_pause = true;

					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "paused"));

				}

				else {
					if (obj.get("type").equals("web record"))
						InitializeDependence.pause = false;

					if (obj.get("type").equals("mobile record"))
						InitializeDependence.mobile_pause = false;
					resp.setStatus(HttpStatus.OK_200);
					resp.addHeader("Access-Control-Allow-Origin", "*");
					resp.addHeader("Access-Control-Allow-Headers", "*");
					resp.addHeader("Access-Control-Allow-Methods", "*");
					resp.getWriter().println(new JSONObject().put("data", "resume"));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
