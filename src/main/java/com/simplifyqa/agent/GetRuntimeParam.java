package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.Constants;
import com.simplifyqa.manager.MainFrameManager;

public class GetRuntimeParam extends HttpServlet{

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(GetRuntimeParam.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.setContentType("application/json");
		try {
			StringBuffer re = new StringBuffer();
			String line = null;
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			JSONObject resobject = new JSONObject(re.toString());
			logger.info(resobject.toString());
			MainFrameManager sqadriver = new MainFrameManager();
			JSONObject response = sqadriver.getRuntimeParameter(resobject);
			if(response.getJSONObject(Constants.DATA).getInt("totalCount")>0) {
				JSONObject obj = response.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0);
				JSONObject res = new JSONObject().put("param",obj.getString("key")).put("value",obj.getString("value"));
				resp.getWriter().println(res);
			}else {
				JSONObject res = new JSONObject().put("param",resobject.getString("param")).put("value","");
				resp.getWriter().println(res);
			}
			
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
			resp.getWriter().println(new JSONObject().put("success", false).put("error",e.getMessage()));
			
		}
		
		

	}
	
	

}
