package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.execution.Playback;
import com.simplifyqa.handler.Service;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.ios.ideviceMaganger;

public class RefPlay extends HttpServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JSONObject playobj = new JSONObject();

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		JSONObject res = new JSONObject();
		System.out.println(resobj.toString());
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			String key = keyItr.next();
			switch (key) {
			case "playback":
//				Service.a().killProcess();
				Service.a().destroy();
				Service.a().Reset();
				playobj = resobj.getJSONObject(key);
				try {
					if (DeviceManager.devices.isEmpty() && ideviceMaganger.devices.isEmpty()) {
						res.put("Error", "No devices found!");
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(res.toString());
					} else {
						try {
							Playback play = new Playback();

							res.put("data", "success");
							resp.setStatus(HttpStatus.OK_200);
							resp.addHeader("Access-Control-Allow-Origin", "*");
							resp.addHeader("Access-Control-Allow-Headers", "*");
							resp.addHeader("Access-Control-Allow-Methods", "*");
							resp.getWriter().println(res.toString());
							CompletableFuture.runAsync(() -> {
								try {
									Thread.sleep(1000);
									if (play.setup(playobj.get("id").toString(), playobj.getJSONArray("steps"),
											playobj.getBoolean("start"), playobj.getString("type")))
										play.run();

								} catch (InstallException | JSONException | IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (TimeoutException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (AdbCommandRejectedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (ShellCommandUnresponsiveException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								return;
							});
						} catch (IOException e) {
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
			}
		}
	}

}
