package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.ini4j.spi.IniBuilder;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.method.DesktopMethod;
import com.simplifyqa.method.MainframeMethod;
import com.simplifyqa.method.WebMethod;
import com.simplifyqa.agent.AgentStart;

public class StopPlayBack extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(StopPlayBack.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			if (resobj.has("data") && resobj.getString("data").toLowerCase().equals("stop")) {
				System.out.println("API is hit");

				if (resobj.getString("type").toLowerCase().equals("web")) {
					for (Entry<String, WebMethod> map : InitializeDependence.webDriver.entrySet()) {
						map.getValue().deleteSession();
						map.getValue().closeDriver();
						GlobalDetail.runTime.remove(map.getKey());

					}
					GlobalDetail.ReferencePlayback = false;
					GlobalDetail.ResumePlayback=false;
					logger.info("recrding browser : " + InitializeDependence.recording_browser);
					//close firefox driver after recording
					if (InitializeDependence.recording_browser.equals("firefox")) {
						String session = InitializeDependence.firefox_session;
						String port = GlobalDetail.webDriver.get("firefox_rec").getHostAddress();
						try {
							HttpUtility.delete(port + "/session/" + session);
							GlobalDetail.webDriver.get("firefox_rec").closeDriver();
						} catch (Exception e) {
							logger.error(e.toString());
							logger.error("Firefox driver cannot be closed");
						}
					}
					InitializeDependence.reference_playback = false;
					System.out.println("Web Stopped");
					if (resobj.getString("browser").toLowerCase().equals("internetex")) {
						AgentStart.internetExplorerProcess.destroy();
						System.out.println("Internet Explorer Stopped");
					}
				} else if (resobj.getString("type").toLowerCase().equals("mainframe")) {
					if (MainframeMethod.release) {
						InitializeDependence.mfAction.release(new JSONObject().put("execution", "stop"));
					}
					InitializeDependence.mfAction.stopplayback();
					InitializeDependence.mfAction.stopRecorder();
				} else if (resobj.getString("type").toLowerCase().equals("desktop")) {
					InitializeDependence.dtAction.stopplayback();
					InitializeDependence.dtAction.stopRecorder();
				} else if (resobj.getString("type").toLowerCase().equals("sap")) {
					InitializeDependence.sapAction.stopplayback();
					InitializeDependence.sapAction.stopRecorder();
				}
				resp.getWriter().println(new JSONObject().put("data", "success"));
			} else {
				resp.getWriter().println(new JSONObject().put("data", "failed"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.getWriter().println(new JSONObject().put("data", "failed"));
		}

	}

}
