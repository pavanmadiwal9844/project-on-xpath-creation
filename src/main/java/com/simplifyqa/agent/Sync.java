package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.PullImageResultCallback;
import com.github.dockerjava.api.model.AccessMode;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DockerClientBuilder;
import com.simplifyqa.CodeEditor.CodeEditorDebugger;
import com.simplifyqa.Utility.AutoParameterPopulation;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;

public class Sync extends HttpServlet {
	private final static Logger logger = LoggerFactory.getLogger(Sync.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try {

		} catch (Exception e) {
			System.out.println(e);
			JSONObject error = new JSONObject();
			error.put("error", e.toString());
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.setStatus(HttpStatus.OK_200);
			resp.getWriter().print(error);

		}
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();

		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);

			JSONObject res = new JSONObject(re.toString());

			if (AutoParameterPopulation.autoParameterMethod(res.get("customerId").toString(),
					res.get("userId").toString(), res.get("authToken").toString(), res.get("serverIp").toString())) {

				JSONObject apiresponse = new JSONObject();
				logger.info("Sync complete");
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				apiresponse.put("status", 200);
				resp.getWriter().println(apiresponse);

			} else {
//				resp.setStatus(HttpStatus.NOT_FOUND_404);
//				resp.addHeader("Access-Control-Allow-Origin", "*");
//				resp.addHeader("Access-Control-Allow-Headers", "*");
//				resp.addHeader("Access-Control-Allow-Methods", "*");
//				resp.getWriter().println();
				logger.info("error while syncing");
			}

		} catch (Exception e) {
//			resp.setStatus(HttpStatus.NOT_ACCEPTABLE_406);
//			resp.addHeader("Access-Control-Allow-Origin", "*");
//			resp.addHeader("Access-Control-Allow-Headers", "*");
//			resp.addHeader("Access-Control-Allow-Methods", "*");
//			resp.getWriter().println();
			e.printStackTrace();
		}

	}

}
