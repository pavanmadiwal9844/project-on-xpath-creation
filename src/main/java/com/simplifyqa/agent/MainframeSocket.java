package com.simplifyqa.agent;

import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.method.MainframeMethod;

import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;

@ServerEndpoint(value = "/Mainframe")
public class MainframeSocket {

	@OnOpen
	public void onOpen(Session session) throws IOException {
		System.out.println("WebSocket opened Mainframe: " + session.getId());
		 session.setMaxTextMessageBufferSize(4 * 1024 * 1024); // 4MB
	}

	@OnMessage
	public void onMessage(String res, Session session) {
		System.out.println("Message received: " + res);
		GlobalDetail.mainFrSession = session;
		JSONObject request = new JSONObject(res);
		if(request.has("mainframe")) {
			GlobalDetail.sem.release();
		}
		
		if(request.has("validation")){
			GlobalDetail.sendStpesSession.getAsyncRemote().sendText(request.toString());
		}else {
			InitializeDependence.mfAction.release(request);
			MainframeMethod.release = false;
		}
		
		

	}

	@OnClose
	public void onClose(CloseReason reason, Session session) throws IOException {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
	}

}
