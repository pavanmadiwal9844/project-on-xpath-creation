package com.simplifyqa.agent;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.mobile.android.AndroidMirror;
import com.simplifyqa.mobile.android.service;
import com.simplifyqa.mobile.ios.Frame;

@ServerEndpoint(value = "/SqaScreen")
public class Screen {

	private static Screen screen;

	@OnOpen
	public void onOpen(Session session) throws IOException {
		System.out.println("WebSocket opened: " + session.getId());
	}

	@OnMessage
	public void onMessage(String res, Session session) throws IOException, InterruptedException, EncodeException,
			JSONException, TimeoutException, AdbCommandRejectedException, SyncException {
		System.out.println("Message received: " + res);
		JSONObject jsobj = new JSONObject(res);
		if (jsobj.has("selected")) {
			screenStart(jsobj, session);
		}
	}

	@OnClose
	public void onClose(CloseReason reason, Session session) throws IOException {
		System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
		if (GlobalDetail.frame == null) {
			for (Entry<String, Session> map : GlobalDetail.websocket.entrySet()) {
				if (map.getValue().equals(session)) {
					InitializeDependence.aMirror.get(map.getKey()).stopScreenListener();
					InitializeDependence.aMirror.remove(map.getKey());
				}
			}
		}else {
			GlobalDetail.frame.shutdownNow();
		}
	}

	public static Screen screen() {
		if (screen == null)
			screen = new Screen();
		return screen;
	}

	@OnError
	public void onError(Session session, Throwable thr) {
		System.out.println(thr.getCause().getMessage());
	}

	public void screenStart(JSONObject jsobj, Session session)
			throws SyncException, AdbCommandRejectedException, TimeoutException, InterruptedException, IOException {
		JSONObject obj = jsobj.getJSONObject("selected");
		if (GlobalDetail.websocket.containsKey(obj.get("id").toString()))
			GlobalDetail.websocket.remove(obj.get("id").toString());
		GlobalDetail.websocket.put(obj.get("id").toString(), session);
		if (obj.getString("type").equals("android")) {
			if (InitializeDependence.aMirror.containsKey(obj.get("id").toString())) {
//				Service.a().destroy();
//				Service.a().Reset();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
				InitializeDependence.aMirror.get(obj.get("id").toString()).stopScreenListener();
				InitializeDependence.aMirror.remove(obj.get("id").toString());
			}
			InitializeDependence.aMirror.put(obj.get("id").toString(), new AndroidMirror(obj));
			CompletableFuture.runAsync(() -> {
				InitializeDependence.aMirror.get(obj.get("id").toString()).startScreenListener();
				return;
			}).join();
		} else {
			Thread.sleep(1000);
			Frame iosframe = new Frame(obj.get("id").toString());
			ExecutorService frame = service.g().e();
			GlobalDetail.frame = frame;
			frame.submit(() -> {
				iosframe.init();
				return true;
			});
		}
	}
}
