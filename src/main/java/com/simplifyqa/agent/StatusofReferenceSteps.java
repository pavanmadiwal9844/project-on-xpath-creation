package com.simplifyqa.agent;

import java.io.IOException;
import java.net.SocketTimeoutException;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/referenceplayback")
public class StatusofReferenceSteps {

	@OnOpen
	public void onOpen(Session session) throws IOException, SocketTimeoutException {
		System.out.println("WebSocket opened: " + session.getId());
		System.out.println(session.isOpen());

	}

	@OnMessage
	public void OnMessage(String res, Session session) throws IOException, SocketTimeoutException {
		System.out.println(res);
		GlobalDetail.referencestep = session;
	}

	@OnClose
	public void onClose() throws IOException, SocketTimeoutException {
		System.out.println("Closing a WebSocket due to ");
	}

}
