package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.handler.DesktopService;
import com.simplifyqa.manager.DesktopStepManager;

public class SendSteps extends HttpServlet {
	
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(SendSteps.class);

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		try {
			startSerivceManager();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			System.out.println(resobj.toString());
			JSONObject object = new JSONObject();
			object.put("action", resobj.getJSONObject("child").getString("action"));
			object.put("text", resobj.getJSONObject("child").getString("text"));
			for(int i=0;i<resobj.getJSONObject("child").getJSONArray("attributes").length();i++) {
				String str = resobj.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).getString("name");
				if(str.equalsIgnoreCase("xpath")||str.equalsIgnoreCase("id")||str.equalsIgnoreCase("exepath")){
					resobj.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).put("unique", Boolean.TRUE);
					break;
				}
			}
			object.put("attributes", resobj.getJSONObject("child").getJSONArray("attributes"));
			if (!resobj.getJSONObject("child").isNull("value")&&resobj.getJSONObject("child").has("value"))
				object.put("value", resobj.getJSONObject("child").getString("value"));
			System.out.println(object);
			logger.info(object.toString());
			DumpOBject dmpobj = new DumpOBject();
			DesktopStepManager sqaDriver = new DesktopStepManager();
			if ((dmpobj = sqaDriver.DumpObject(dmpobj, object)) != null)
				DesktopService.a().a(dmpobj);

		} catch (Exception e) {
			e.printStackTrace();
		}
//		 new Format().formatData(resobj.getJSONObject("child"), Global.cases);
//		 new Desktoptransformer().transform(resobj.getJSONObject("child"));
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}
	
	
	
	private void startSerivceManager() throws InterruptedException, ExecutionException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					DesktopService.a().Reset();
					DesktopService.a().run().get();
					logger.info("Started handler");

				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				logger.info("stoped handler");
			}
		}).start();

	}


}
