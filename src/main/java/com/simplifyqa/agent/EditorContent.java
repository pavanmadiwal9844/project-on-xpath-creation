package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;

public class EditorContent extends HttpServlet {

	/**
	 * @author Srinivasan
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(EditorContent.class);
	public String time;
	public String jarname;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject resobj = new JSONObject(jb.toString());
		int customerId = resobj.getInt("customerId");
		int userId = resobj.getInt("userId");
		String version = (String) resobj.get("version");
		String serverUrl = (String) resobj.get("serverUrl");
		JSONArray methodcontent = resobj.getJSONArray("str");
		String type = resobj.getString("type");
		String classname = null;
		if (type.equals("android"))
			classname = "CustomMethodsandroid";
		else if (type.equals("ios"))
			classname = "CustomMethodsios";
		else if (type.equals("mainframe"))
			classname = "CustomMethodsmainframe";
		else if (type.equals("web"))
			classname = "CustomMethodsweb";
		String content = methodcontent.getString(0);
		downloadMethods(content, classname + ".java", version, serverUrl, customerId);
		String errorcontent = custommethodCompile(classname + ".java");
		if (!errorcontent.equals("")) {
			resp.setStatus(HttpStatus.PRECONDITION_FAILED_412);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("Error", errorcontent));
		} else {
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("Success", "Saved"));
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String content = null;
		try {
			content = new String(Files.readAllBytes(Paths.get("libs" + File.separator + "External_Jar" + File.separator
					+ "csmethods" + File.separator + "CustomMethods.java")));
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", content));
		} catch (Exception e) {
			resp.setStatus(HttpStatus.OK_200);
			resp.addHeader("Access-Control-Allow-Origin", "*");
			resp.addHeader("Access-Control-Allow-Headers", "*");
			resp.addHeader("Access-Control-Allow-Methods", "*");
			resp.getWriter().println(new JSONObject().put("data", "Not_Found"));
		}
	}

	public void downloadMethods(String content, String classname, String version, String serverUrl, int customerId)
			throws IOException {
		URI path = Paths.get("libs" + File.separator + "External_Jar" + File.separator + "csmethods").toUri();
		BufferedWriter writer = new BufferedWriter(
				new FileWriter(new File(path).getAbsolutePath() + File.separator + classname));
		writer.write(content.toString());
		writer.close();
		File versionfile = new File(
				Paths.get("libs" + File.separator + "External_Jar" + File.separator + "version.txt").toString());
		FileWriter versionwrite = new FileWriter(versionfile);
		versionwrite.write(version);
		versionwrite.close();
		File tiimefile = new File(Paths.get(".") + File.separator + "libs" + File.separator + "External_Jar"
				+ File.separator + "time.txt");
		if (tiimefile.exists()) {
			time = new String(Files.readAllBytes(Paths.get(
					"." + File.separator + "libs" + File.separator + "External_Jar" + File.separator + "time.txt")));
		} else {
			time = "";
		}
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("accept", "application/json");
			if (time.equals("")) {
				time = null;
			}
			JSONObject resjss = HttpUtility.get(serverUrl + "/v1/checkforupdate/" + customerId + "/" + time, null,
					headers);
			if (null != resjss && !resjss.has("error")) {
				JSONArray responst = new JSONArray(resjss.get("jars").toString());
				for (int i = 0; i < responst.length(); i++) {
					JSONObject jsjarname = responst.getJSONObject(i);
					jarname = jsjarname.optString("jarName");
					System.out.println(jarname);
					try {
						InputStream in = HttpUtility
								.getFile(serverUrl + "/v1/downloadjar/" + customerId + "/" + jarname, null, headers);
						int inByte;
						File jarfile = new File(Paths
								.get("libs" + File.separator + "External_Jar" + File.separator + jarname).toString());
						if (jarfile.exists()) {
							jarfile.delete();
							FileOutputStream fout = new FileOutputStream(new File(Paths.get(".") + File.separator
									+ "libs" + File.separator + "External_Jar" + File.separator + jarname));
							while ((inByte = in.read()) != -1)
								fout.write(inByte);
							File timefile = new File(
									Paths.get("libs" + File.separator + "External_Jar" + File.separator + "time.txt")
											.toString());

							if (timefile.exists()) {
								timefile.delete();
								FileWriter write = new FileWriter(timefile);
								write.write(Instant.now().toString());
								write.close();
								in.close();
								fout.close();
							} else {
								FileWriter write = new FileWriter(timefile);
								write.write(Instant.now().toString());
								write.close();
								in.close();
								fout.close();
							}
						} else {
							FileOutputStream fout = new FileOutputStream(new File(
									Paths.get("libs") + File.separator + "External_Jar" + File.separator + jarname));
							while ((inByte = in.read()) != -1)
								fout.write(inByte);
							File timefile = new File(
									Paths.get("libs" + File.separator + "External_Jar" + File.separator + "time.txt")
											.toString());
							FileWriter write = new FileWriter(timefile);
							write.write(Instant.now().toString());
							write.close();
							in.close();
							fout.close();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("No New Jar");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String custommethodCompile(String classname) throws IOException {
		URI path = Paths.get("libs" + File.separator + "External_Jar" + File.separator + "csmethods").toUri();
		File logfile = new File(Paths.get("custommethod.log").toUri());
		if (logfile.exists()) {
			logfile.delete();
		}
		URI agentpath = Paths.get("com.simplifyQA.Agent.jar").toUri();
		String dependencypath = Paths.get("libs").toAbsolutePath() + File.separator + "External_Jar";
		ProcessBuilder process = null;
		if (SystemUtils.IS_OS_WINDOWS) {
			process = new ProcessBuilder("javac", "-g", "-cp",
					new File(agentpath).getAbsolutePath() + ";" + dependencypath + "/*" + ";.",
					new File(path).getAbsolutePath() + File.separator + classname);
		} else if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
			process = new ProcessBuilder("javac", "-g", "-cp",
					new File(agentpath).getAbsolutePath() + ":" + dependencypath + "/*",
					new File(path).getAbsolutePath() + File.separator + classname);
		}
//		process.environment().put("JAVA_HOME", System.getProperty("java.home"));
		process.directory(UserDir.getExternaljar());
		process.redirectOutput(ProcessBuilder.Redirect.to(new File("custommethod.log")));
		process.redirectError(ProcessBuilder.Redirect.to(new File("custommethod.log")));
		process.start();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String errorcontent = "";
		errorcontent = new String(Files.readAllBytes(Paths.get("custommethod.log")));
		return errorcontent;
	}

}