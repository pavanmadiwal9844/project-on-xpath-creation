package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.ObjectInspector.ObjectInspector;
import com.simplifyqa.ObjectInspector.objectservice;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.UserDir;

public class WebObjectInspector extends HttpServlet {

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(WebObjectInspector.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.setContentType("text/javascript");
		resp.setStatus(HttpStatus.OK_200);
		resp.getWriter().println(new String(Files.readAllBytes(Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { "SqaExtension", "objectinspector.js" }))));
	}

	public static void main(String[] args) throws IOException {

//        System.out.println(jqueryText);
	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
//		resp.setContentType("application/json");
		try {
			StringBuffer re = new StringBuffer();
			String line = null;
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			JSONObject resobject = new JSONObject(re.toString());
			try {
//				objectservice.a().destroy();
				resp.getWriter()
						.println(new JSONObject().put("success", new ObjectInspector().startInpector(resobject)));
			} catch (Exception e) {
				// TODO: handle exception
				logger.error("Unable to get the browser options");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
			resp.getWriter().println(new JSONObject().put("success", false).put("error", e.getMessage()));

		}

	}

}
