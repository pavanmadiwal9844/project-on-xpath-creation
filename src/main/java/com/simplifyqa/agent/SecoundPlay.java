package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.UserDetail;
import com.simplifyqa.DTO.WebSteps;
import com.simplifyqa.DTO.executionData;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.*;
import com.simplifyqa.desktoprecorder.DTO.OriginalData;
import com.simplifyqa.desktoprecorder.DTO.Testcase;
import com.simplifyqa.manager.MainFrameManager;
import com.simplifyqa.manager.StepManager;

public class SecoundPlay extends HttpServlet {

	/**
	 * @author bhasakar
	 */
	private static final long serialVersionUID = 937434247318775170L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();
		String line = null;
		BufferedReader reader = req.getReader();
		while ((line = reader.readLine()) != null)
			re.append(line);
		JSONObject jsonobj = new JSONObject(re.toString());
		System.out.println(jsonobj.toString());
		WebSteps web = new WebSteps();
		GlobalDetail.web = web;
		GlobalDetail.web.setReference(false);
		if (jsonobj.has(Constants.SERVERIP)) {
			GlobalDetail.serverIP = jsonobj.getString(Constants.SERVERIP);
		}
		if (GlobalDetail.sendStpesSession != null) {
			GlobalDetail.sendStpesSession.close();
		}
		StepManager step = new StepManager();
		try {
			GlobalDetail.rcopl = new JSONObject().put(Constants.NAME, Constants.PLAYBACK);
			SearchColumns search = new SearchColumns();
			search.getSearchColumns()
					.add(new searchColumnDTO<Integer>(Constants.CUSTOMERID,
							Integer.valueOf(jsonobj.getInt(Constants.CUSTOMERID)),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
			search.getSearchColumns()
					.add(new searchColumnDTO<Integer>(Constants.PROJECTID,
							Integer.valueOf(jsonobj.getInt(Constants.PROJECTID)),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
			search.getSearchColumns()
					.add(new searchColumnDTO<Integer>(Constants.ID,
							Integer.valueOf(jsonobj.getInt(Constants.EXECUTIONID)),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.INTEGER));
			search.getSearchColumns().add(new searchColumnDTO<Boolean>(Constants.DELETED, Boolean.FALSE,
					Boolean.parseBoolean(UtilEnum.FALSE.value()), Constants.BOOLEAN));
			search.setStartIndex(0);
			search.setLimit(100000);
			search.setCollection(Constants.EXECUTION);
			JSONObject json = new JSONObject(new ObjectMapper().writeValueAsString(search));
			JSONObject jso = step.fetchingData(json, jsonobj.getString(Constants.AUTHKEY));
			JSONObject start = new JSONObject();
			GlobalDetail.web.setTestcaseId(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA)
					.getJSONObject(0).getInt(Constants.TESTCASEID));
			GlobalDetail.web.setExecutionId(jsonobj.getInt(Constants.EXECUTIONID));
			UserDetail urdl = new UserDetail();
			urdl.setCustomerId(jsonobj.getInt(Constants.CUSTOMERID));
			urdl.setAuthkey(jsonobj.getString(Constants.AUTHKEY));
			urdl.setProjectId(jsonobj.getInt(Constants.PROJECTID));
			urdl.setTestcaseId(jsonobj.getInt(Constants.TESTCASEID));
			urdl.setCreatedBy(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.CREATEDBY));
			GlobalDetail.userDetail = urdl;
			GlobalDetail.testcaseCode = jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getString(Constants.TESTCASECODE);
			start.put(Constants.TESTCASEID, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA)
					.getJSONObject(0).getInt(Constants.TESTCASEID));
			start.put(Constants.TCSEQ, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.TCSEQ));
			start.put(Constants.TCSEQ, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.VERSION));
			start.put(Constants.TCSEQ, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getString(Constants.TESTCASECODE));
			start.put(Constants.RESULTTC, Constants.INPROGRESS);
			start.put(Constants.TCSEQ, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.CUSTOMERID));
			start.put(Constants.TCSEQ, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.PROJECTID));
			start.put(Constants.TCSEQ, jsonobj.get(Constants.EXECUTIONID));
			start.put(Constants.TCSEQ, 1);
			step.startExecution(start, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getString(Constants.AUTHKEY));
			executionData data = new executionData();
			data.setExecutionId(jsonobj.getLong(Constants.EXECUTIONID));
			data.setProjectId(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getLong(Constants.PROJECTID));
			data.setCustomerId(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getLong(Constants.CUSTOMERID));
			data.setTestcaseId(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getLong(Constants.TESTCASEID));
			data.setTcSeq(jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA).getJSONObject(0)
					.getInt(Constants.TCSEQ));
			JSONObject jss = step.fetchingTestCases(data, jso.getJSONObject(Constants.DATA).getJSONArray(Constants.DATA)
					.getJSONObject(0).getString(Constants.AUTHKEY));
			JSONObject js1 = new JSONObject().put(jsonobj.getString(Constants.BROWSER).toLowerCase(),
					jss.getJSONObject(Constants.DATA));
			System.out.println(js1);
			web.setStatus(true);
			// GlobalDetail.multiple.put(web.getExecutionId(), web);
			if ((js1.keys().next()).equals("mainframe")) {
				System.out.println("Launch the exe");
				GlobalDetail.mainframeref = new JSONArray();
				JSONObject jt = js1.getJSONObject("mainframe");
				System.out.println(GlobalDetail.mainframeSteps);
				GlobalDetail.cases = null;
				GlobalDetail.cases = new Testcase();
				OriginalData or = new OriginalData();
				for (int i = 0; i < jt.getJSONArray(Constants.STEPS).length(); i++) {
					System.out.println(jt.getJSONArray(Constants.STEPS).getJSONObject(i));
					GlobalDetail.mainframeref.put(jt.getJSONArray(Constants.STEPS).getJSONObject(i));
					System.out.println(jt.getJSONArray(Constants.STEPS).getJSONObject(i));
					// TODO currently supporting only one iteration of data need to implement
					Testcase cases = new MainFrameManager().formatData1(
							jt.getJSONArray(Constants.STEPS).getJSONObject(i), GlobalDetail.cases,
							jt.getJSONArray(Constants.TESTDATA).getJSONObject(0));
					or.setData(cases);
				}
				JSONObject js = new JSONObject(new ObjectMapper().writeValueAsString(or));
				GlobalDetail.mainframeSteps = js;
				System.out.println(GlobalDetail.mainframeSteps.toString());
				GlobalDetail.just = false;
				System.out.println(GlobalDetail.mainframeSteps);
				String line1 = null;
				String path1 = Paths
						.get(Constants.LIBS + File.separator + "MainFrame_RP" + File.separator + Constants.PLAYBACK)
						.toAbsolutePath().toString();
				File mfpaly = new File(new File(Paths.get(UserDir.getAapt().getAbsolutePath() + File.separator
						+ "MainFrame_RP" + File.separator + Constants.PLAYBACK).toUri()), "SimplyPlayback.exe");
				Runtime run = Runtime.getRuntime();
				System.out.println(mfpaly.getAbsolutePath());
				Process p = run.exec(new String[] { "cmd", "/c", mfpaly.getAbsolutePath() }, null, new File(path1));
				InputStream stdIn = p.getInputStream();
				InputStreamReader isr = new InputStreamReader(stdIn);
				BufferedReader br = new BufferedReader(isr);
				System.out.println("<OUTPUT>");
				while ((line1 = br.readLine()) != null)
					System.out.println(line1);

			} else {
				step.stepExcecutor(js1, web);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println(new JSONObject().put("type", "localExecution"));

	}

}
