package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.IosMethod;

public class StartApp extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Configure.initAdb();
		JSONObject resobj = new JSONObject(jb.toString());
		System.out.println(resobj.toString());
		Iterator<String> keyItr = resobj.keys();
		while (keyItr.hasNext()) {
			String key = keyItr.next();
			switch (key) {
			case "app":
				JSONObject obj = new JSONObject();
				obj = (JSONObject) resobj.get(key);
				try {
					Thread.sleep(2000);
					if (obj.getString("type").equals("android")) {
						AndroidMethod driverAction = new AndroidMethod();
						Thread.currentThread().setName(obj.get("id").toString());
						driverAction.setDevice(obj.get("id").toString());
//						System.out.println(GlobalDetail.hostname);
						driverAction.hostname = GlobalDetail.hostname.get(obj.get("id").toString());
						driverAction.runapp("io.appium.settings", "io.appium.settings.Settings");
						Thread.sleep(2000);
						if (resobj.getBoolean("start")) {
							if (driverAction.startapp(obj.getString("package"), obj.getString("activity"))) {
								GlobalDetail.sqaDriver.put(obj.get("id").toString(), driverAction.getDriver());
								resp.setStatus(HttpStatus.OK_200);
								JSONObject res = new JSONObject();
								res.put("orientation", driverAction.getOrientation());
								res.put("data", "success");
								resp.addHeader("Access-Control-Allow-Origin", "*");
								resp.addHeader("Access-Control-Allow-Headers", "*");
								resp.addHeader("Access-Control-Allow-Methods", "*");
								resp.getWriter().println(res);
							} else {
								resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
								resp.addHeader("Access-Control-Allow-Origin", "*");
								resp.addHeader("Access-Control-Allow-Headers", "*");
								resp.addHeader("Access-Control-Allow-Methods", "*");
								JSONObject error = new JSONObject();
								error.put("data", "Restart Agent Cannot create new Session");
								resp.getWriter().println(error.toString());
							}
						} else {
							if (driverAction.resetapp(obj.getString("package"), obj.getString("activity"))) {
								GlobalDetail.sqaDriver.put(obj.get("id").toString(), driverAction.getDriver());
								resp.setStatus(HttpStatus.OK_200);
								JSONObject res = new JSONObject();
								res.put("orientation", driverAction.getOrientation());
								res.put("data", "success");
								resp.addHeader("Access-Control-Allow-Origin", "*");
								resp.addHeader("Access-Control-Allow-Headers", "*");
								resp.addHeader("Access-Control-Allow-Methods", "*");
								resp.getWriter().println(res);
							} else {
								resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
								resp.addHeader("Access-Control-Allow-Origin", "*");
								resp.addHeader("Access-Control-Allow-Headers", "*");
								resp.addHeader("Access-Control-Allow-Methods", "*");
								JSONObject error = new JSONObject();
								error.put("data", "Restart Agent Cannot create new Session");
								resp.getWriter().println(error.toString());
							}
						}
					} else {
						IosMethod driverAction = new IosMethod();
						Thread.currentThread().setName(obj.get("id").toString());
						driverAction.setDevice(obj.get("id").toString());
						if (driverAction.launch_app(obj.getString("bundleId"))) {
							GlobalDetail.iosSession.put(obj.get("id").toString(), driverAction.getSessionId());
							JSONObject res = new JSONObject();
							res.put("orientation", driverAction.getOrientation().toLowerCase());
							res.put("data", "success");
							resp.setStatus(HttpStatus.OK_200);
							resp.addHeader("Access-Control-Allow-Origin", "*");
							resp.addHeader("Access-Control-Allow-Headers", "*");
							resp.addHeader("Access-Control-Allow-Methods", "*");
							resp.getWriter().println(res);
						} else {
							resp.setStatus(HttpStatus.FORBIDDEN_403);
							resp.addHeader("Access-Control-Allow-Origin", "*");
							resp.addHeader("Access-Control-Allow-Headers", "*");
							resp.addHeader("Access-Control-Allow-Methods", "*");
							JSONObject error = new JSONObject();
							error.put("data", "Restart Agent Cannot create new Session");
							resp.getWriter().println(error.toString());
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
