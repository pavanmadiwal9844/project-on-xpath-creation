package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.desktoprecorder.DTO.Testcase;
import com.simplifyqa.execution.Startexecution;
import com.simplifyqa.manager.DesktopStepManager;

public class StartPlayBack extends HttpServlet {

	public static org.slf4j.Logger logger = LoggerFactory.getLogger(StartPlayBack.class);

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			JSONObject resobj = new JSONObject(jb.toString());
			if(resobj.has("StepIndex")) {
				GlobalDetail.ResumeStep = resobj.get("StepIndex").toString();
				GlobalDetail.ResumePlayback = true;
				}
			GlobalDetail.ReferencePlayback = true;
			logger.info(resobj.toString());
			File file = new File(UserDir.getAapt() + "/config.properties");
			if (file.exists()) {
				FileInputStream ip = new FileInputStream(UserDir.getAapt() + "/config.properties");
				Properties prop = new Properties();
				prop.load(ip);
				if (prop.containsKey("serverurl"))
					resobj.put("serverIp", prop.getProperty("serverurl"));
			}
			Startexecution start = new Startexecution();
			start.Setexecution(resobj);
			start.execute();
//			new DesktopStepManager().stepExcecutor(resobj);
//			GlobalDetail.cases = null;
//			GlobalDetail.cases = new Testcase();
//			for (int i = 0; i < resobj.getJSONObject("data").getJSONArray("steps").length(); i++) {
//				new DesktopStepManager().formatData(resobj.getJSONObject("data").getJSONArray("steps").getJSONObject(i),
//						GlobalDetail.cases);
//			}

			// System.out.println(resobj.toString());
			startPlayback();
		} catch (Exception e) {
			e.printStackTrace();
		}

		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println(new JSONObject().put("data", "received"));

	}

	public void startPlayback() throws IOException, InterruptedException {
		try {
			String path = Paths.get("libs" + File.separator + "Recorder_exe" + File.separator + "Playback.exe")
					.toAbsolutePath().toString();
			// Runtime run = Runtime.getRuntime();
//			Runtime.getRuntime().exec("cmd /c "+path);
//			@SuppressWarnings("unused")
			// run.exec(path);
//			int ss = pro.waitFor();
//			System.out.println("aadad "+ss);

			ProcessBuilder processBuilder = new ProcessBuilder(path);
			// processBuilder.directory(new File(Paths.get("libs" + File.separator +
			// "Recorder_exe").toAbsolutePath().toString()));
			File fileName = new File(
					Paths.get("libs" + File.separator + "Recorder_exe/out.txt").toAbsolutePath().toString());
			File error = new File(
					Paths.get("libs" + File.separator + "Recorder_exe/out.txt").toAbsolutePath().toString());
			processBuilder.redirectOutput(fileName);
			processBuilder.redirectError(error);
			processBuilder.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
