package com.simplifyqa.agent;

import com.android.dx.gen.Code;
import com.simplifyqa.CodeEditor.CodeEditorDebugger;
import com.simplifyqa.Utility.MethodClassification;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.customMethod.Classnamesfromjar;
import com.simplifyqa.customMethod.Debugger;
import com.simplifyqa.customMethod.DebuggerSocket;
import com.simplifyqa.customMethod.Gradle;
import com.simplifyqa.customMethod.Maven;
import com.simplifyqa.customMethod.Methodnamesfromjar;
import com.simplifyqa.handler.Service;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.android.Setautomator2;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import com.simplifyqa.web.implementation.Automatic_Downloader;
import java.io.File;
import java.io.IOException;
import javax.servlet.Servlet;
import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentStart {
	private static Logger Log = LoggerFactory.getLogger(Idevice.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AgentStart.class);

	public static Process internetExplorerProcess;

	public static void main(String[] args) throws InterruptedException, SecurityException, IOException {
		File file = new File("UserLogs.log");
		if (file.delete()) {
			System.out.println("File deleted successfully");
		} else {
			System.out.println("Failed to delete the file");
		}
		
		AgentStart main = new AgentStart();
		CodeEditorDebugger CodeEditor = new CodeEditorDebugger();
		File logfile = UserDir.getlogfile();
		if (logfile.exists())
			logfile.delete();
		AgentStart.startDeviceManager();
		Service.a().killProcess();
		Service.a().killDriver();
		Service.a().killDebugger();
		Setautomator2.list_emulators();
		
		Log.info("You are executing the Code Editor jar");
		
//		CodeEditor.FileUpdate();
		CodeEditor.Debugger2();
		CodeEditor.filedelete();
//		CodeEditor.method_list();
//		CodeEditor.jarupdate();
//		CodeEditor.socketcheck();
		
		Idevice.EndRunnerapp();
		ideviceMaganger.Idevice();
		checkUpdate();
		if (SystemUtils.IS_OS_MAC)
			try {
				Runtime.getRuntime().exec(new String[] { "chmod", "-R", "777", UserDir.getAapt().getAbsolutePath() });
				Idevice.getSimulators();
			} catch (IOException e) {
				e.printStackTrace();
			}
		jenkinCall();
		MethodClassification.list_Method();
		Server server = new Server();
		ServerConnector connector = new ServerConnector(server);
		String port = System.getProperty("port");
		if (port == null) {
			connector.setPort(4012);
		} else {
			connector.setPort(Integer.parseInt(port));
		}
		server.addConnector((Connector) connector);
		ServletContextHandler context = new ServletContextHandler(1);
		context.setContextPath("/");
		server.setHandler((Handler) context);
		try {
			ServerContainer serverContainer = WebSocketServerContainerInitializer.configureContext(context);
			serverContainer.setDefaultMaxSessionIdleTimeout(604800000L);
			serverContainer.addEndpoint(Screen.class);
			serverContainer.addEndpoint(DeviceStatus.class);
			serverContainer.addEndpoint(RecordSocket.class);
			serverContainer.addEndpoint(DebuggerSocket.class);
			serverContainer.addEndpoint(MainframeSocket.class);
			serverContainer.addEndpoint(StatusofReferenceSteps.class);
			serverContainer.addEndpoint(DesktopSocket.class);
			serverContainer.addEndpoint(SapSocket.class);
			
			context.addServlet(new ServletHolder(new com.simplifyqa.agent.Api()),"/v1/api");	
			context.addServlet(new ServletHolder(new com.simplifyqa.agent.Sync()),"/v1/sync");			
			context.addServlet(new ServletHolder(new com.simplifyqa.agent.CodeEditorLocal()),"/v1/codeditorlocal");
			context.addServlet(new ServletHolder(new com.simplifyqa.agent.PauseResume()),"/v1/pause");
			context.addServlet(new ServletHolder((Servlet) new server()), "/v1/status");
			context.addServlet(new ServletHolder((Servlet) new TapServer()), "/tap");
			context.addServlet(new ServletHolder((Servlet) new SwipeServer()), "/swipe");
			context.addServlet(new ServletHolder((Servlet) new HoldServer()), "/hold");
			context.addServlet(new ServletHolder((Servlet) new Install()), "/install");
			context.addServlet(new ServletHolder((Servlet) new Uninstall()), "/uninstall");
			context.addServlet(new ServletHolder((Servlet) new Apps()), "/apps");
			context.addServlet(new ServletHolder((Servlet) new StartApp()), "/startapp");
			context.addServlet(new ServletHolder((Servlet) new RefPlay()), "/playback");
			context.addServlet(new ServletHolder((Servlet) new KeyBoardEvent()), "/sendKeys");
			context.addServlet(new ServletHolder((Servlet) new ExecutionStep()), "/execution");
			context.addServlet(new ServletHolder((Servlet) new ClearTextEvent()), "/clearKeys");
			context.addServlet(new ServletHolder((Servlet) new MbSettings()), "/networkevent");
			context.addServlet(new ServletHolder((Servlet) new Webexe()), "/status");
			context.addServlet(new ServletHolder((Servlet) new Getxml()), "/parse");
			context.addServlet(new ServletHolder((Servlet) new Orientation()), "/orientation");
			context.addServlet(new ServletHolder((Servlet) new cswgsExecution()), "/cswgsexecution");
			context.addServlet(new ServletHolder((Servlet) new Timer()), "/destroy");
			context.addServlet(new ServletHolder((Servlet) new StartRecording()), "/record");
			context.addServlet(new ServletHolder((Servlet) new CodeEditor()), "/codeeditor");
			context.addServlet(new ServletHolder((Servlet) new EditorContent()), "/editorcontent");
			context.addServlet(new ServletHolder((Servlet) new Classnamesfromjar()), "/classsuggestion");
			context.addServlet(new ServletHolder((Servlet) new Methodnamesfromjar()), "/methodsuggestion");
			context.addServlet(new ServletHolder((Servlet) new StartNotification()), "/startnotification");
			context.addServlet(new ServletHolder((Servlet) new RestartNotification()), "/restartnotification");
			context.addServlet(new ServletHolder((Servlet) new StopNotification()), "/stopnotification");
			context.addServlet(new ServletHolder((Servlet) new Debugger()), "/debug");
			context.addServlet(new ServletHolder((Servlet) new Nextline()), "/step");
			context.addServlet(new ServletHolder((Servlet) new ContinueNextpoint()), "/cont");
			context.addServlet(new ServletHolder((Servlet) new Stopdebug()), "/stopdebugging");
			context.addServlet(new ServletHolder((Servlet) new Maven()), "/maven");
			context.addServlet(new ServletHolder((Servlet) new Gradle()), "/gradle");
			context.addServlet(new ServletHolder((Servlet) new Status()), "/v1/webstatus");
			context.addServlet(new ServletHolder((Servlet) new CreateStep()), "/v1/child");
			context.addServlet(new ServletHolder((Servlet) new Browser()), "/v1/browser");
			context.addServlet(new ServletHolder((Servlet) new PlayBackSteps()), "/v1/playbacksteps");
			context.addServlet(new ServletHolder((Servlet) new PlayBackStay()), "/v1/PlayBackStay");
			context.addServlet(new ServletHolder((Servlet) new SecoundPlay()), "/webext");
			context.addServlet(new ServletHolder((Servlet) new StopPlayBack()), "/stopexecution");
			context.addServlet(new ServletHolder((Servlet) new StartDesktopRecorder()), "/v1/startdesktoprecorder");
			context.addServlet(new ServletHolder((Servlet) new StopDesktopRecorder()), "/stopdesktoprecorder");
			context.addServlet(new ServletHolder((Servlet) new SendSteps()), "/v1/senddesktoprecordersteps");
			context.addServlet(new ServletHolder((Servlet) new GetSteps()), "/v1/getdesktoprecordersteps");
			context.addServlet(new ServletHolder((Servlet) new StartPlayBack()), "/v1/desktopplayback");
			context.addServlet(new ServletHolder((Servlet) new GetMainFrameSteps()), "/v1/sendMainFramerecordersteps");
			context.addServlet(new ServletHolder((Servlet) new MainFramePlayBack()), "/v1/sendMainFrameplaybacksteps");
			context.addServlet(new ServletHolder((Servlet) new MainFrameRecord()), "/v1/MainFrameRecord");
			context.addServlet(new ServletHolder((Servlet) new MainFrameStepsExe()), "/v1/getMainFrameplaybacksteps");
			context.addServlet(new ServletHolder((Servlet) new MainFrameReport()), "/v1/getMainFramereport");
			context.addServlet(new ServletHolder((Servlet) new GetRuntimeParam()), "/v1/getruntimeparam");
			context.addServlet(new ServletHolder((Servlet) new SaveRuntimeParam()), "/v1/saveruntimeparam");
			context.addServlet(new ServletHolder((Servlet) new WebObjectInspector()), "/v1/inspect");
			context.addServlet(new ServletHolder((Servlet) new GettingObjects()), "/v1/getobject");
			context.addServlet(new ServletHolder((Servlet) new ProxyServer()), "/v1/proxy");
			context.addServlet(new ServletHolder((Servlet) new GetIpAddress()), "/v1/registerjenkin");
			context.addServlet(new ServletHolder((Servlet) new Cloudstatus()), "/");
			context.addServlet(new ServletHolder((Servlet) new GetLogs()), "/readLogs/*");
			context.addServlet(new ServletHolder((Servlet) new Setupidevice()), "/setupidevice");
			context.addServlet(new ServletHolder((Servlet) new LaunchEmulator()), "/emulator");
//			context.addServlet(new ServletHolder(new com.simplifyqa.agent.CodeEditorLocal()),"/v1/codeditorlocal");
			server.start();
			server.dump();
			server.join();
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
	}

	public static void startDeviceManager() throws InterruptedException {
		(new Thread(new Runnable() {
			public void run() {
				DeviceManager.getInstance().start();
			}
		})).start();
	}

	private static void checkUpdate() {
		Automatic_Downloader am = new Automatic_Downloader();
		try {
			try {
				if (!am.checkExist("chrome").booleanValue()) {
					Log.info("Updating Chrome Driver!!!");
					am.downloadChromeDriver(am.getLatestVersion(am.getChromeVersion()));
				}
			} catch (Exception e) {
				Log.error("Failed to update chrome driver!!!");
			}
			try {
				if (!am.checkExist("firefox").booleanValue()) {
					Log.info("Updating Firefox Driver!!!");
					am.downloadFirefoxDriver(am.getLatestFirefoxVersion());
				}
			} catch (Exception e) {
				Log.error("Failed to update firefox driver!!!");
			}
			try {
				if (!am.checkExist("edge").booleanValue()) {
					Log.info("Updating Edge Driver!!!");
					am.downloadEdgeDriver(am.getEdgeVersion());
				}
			} catch (Exception e) {
				Log.error("Failed to update edge driver!!!");
			}
		} catch (Exception e) {
			Log.error("Connection Lost!!!");
		}
	}

	private static void jenkinCall() {
		try {
			(new jenkinsocket()).test();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

//	private static void killServer() {
//		try {
//			String cmd = "sudo lsof -i :4012";
//			Runtime run = Runtime.getRuntime();
//			Process pr = run.exec(cmd);
//			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//			String line = "";
//			while ((line=buf.readLine())!=null) {
//				Idevice.killProcess(line.split(" ")[6]);
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
}
