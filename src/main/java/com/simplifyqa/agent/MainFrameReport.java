package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.Constants;
import com.simplifyqa.manager.MainFrameManager;

public class MainFrameReport extends HttpServlet {

	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(MainFrameReport.class);
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		resp.getWriter().println();
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_ORIGIN, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_HEADERS, Constants.STAR);
		resp.addHeader(Constants.ACCESS_CONTROL_ALLOW_METHODS, Constants.STAR);
		try {
			StringBuffer re = new StringBuffer();
			String line = null;
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);
			JSONObject resobject = new JSONObject(re.toString());
			System.out.println(resobject.toString());
			logger.info(resobject.toString());
			MainFrameManager sqadriver = new MainFrameManager();
			if (!GlobalDetail.just) {
				sqadriver.pass_Fail(resobject, resobject.getInt(Constants.INDEX));
					if(!resobject.isNull(Constants.EXECUTION)&&resobject.getString(Constants.EXECUTION).equals("finished")) {
						sqadriver.FOR(resobject, resobject.getInt(Constants.INDEX));
					}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.getWriter().println(new JSONObject().put(Constants.SUCCESS,false).put(Constants.ERROR,e.getMessage()));
		}
		resp.getWriter().println(new JSONObject().put("success",Boolean.TRUE));

	}
}
