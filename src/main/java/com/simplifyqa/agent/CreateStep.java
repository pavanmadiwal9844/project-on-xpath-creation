package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.DTO.DumpOBject;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.handler.WebService;
import com.simplifyqa.manager.StepManager;

public class CreateStep extends HttpServlet {
	/**
	 * @author Bhaskar
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(CreateStep.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		resp.addHeader("Access-Control-Allow-Origin", "*");
//		resp.addHeader("Access-Control-Allow-Headers", "*");
//		resp.addHeader("Access-Control-Allow-Methods", "*");
//		resp.setStatus(HttpStatus.OK_200);
//		resp.getWriter().println();
		

		JSONObject res= new JSONObject();
        res.put("customerId", GlobalDetail.userDetail.getCustomerId().toString());
        res.put("url", GlobalDetail.serverIP + "/search");
        res.put("auth", GlobalDetail.userDetail.getAuthkey());
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Headers", "*");
        resp.addHeader("Access-Control-Allow-Methods", "*");
        resp.setStatus(HttpStatus.OK_200);
        resp.getWriter().println(res);

	}

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer re = new StringBuffer();

		System.out.println("Inside the doPost API");
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				re.append(line);

			// skip recording if pause=true
			if (InitializeDependence.pause) {
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				resp.getWriter().println();
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONObject resobj = new JSONObject(re.toString());
		System.out.println(resobj);
		if (resobj.has("child")) {
			JSONObject object = new JSONObject();

			// for checkbox and radio button not recrding

			if (!resobj.has("action")) {

				for (int i = 0; i < resobj.getJSONObject("child").getJSONArray("attributes").length(); i++) {
					if (resobj.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).get("name")
							.equals("type")) {
						if (resobj.getJSONObject("child").getJSONArray("attributes").getJSONObject(i).get("value")
								.equals("radio")
								|| resobj.getJSONObject("child").getJSONArray("attributes").getJSONObject(i)
										.get("value").equals("checkbox")) {

							resobj.getJSONObject("child").put("action", "Click");

						}
					}

				}

			}
			object.put("action", resobj.getJSONObject("child").getString("action"));
			object.put("text", resobj.getJSONObject("child").getString("text"));
//			if(resobj.getJSONObject("child").has("attributes"))
			object.put("attributes", resobj.getJSONObject("child").getJSONArray("attributes"));

			if (resobj.getJSONObject("child").has("Value")) {
				object.put("value", resobj.getJSONObject("child").getString("Value"));
			}

			if (resobj.getJSONObject("child").has("value") && resobj.getJSONObject("child").get("value") != null) {
				object.put("value", resobj.getJSONObject("child").get("value"));
			}

			logger.info(object.toString());
			DumpOBject dmpobj = new DumpOBject();
			StepManager sqaDriver = new StepManager();
			if ((dmpobj = sqaDriver.DumpObject(dmpobj, object)) != null)
				WebService.a().a(dmpobj);
		} else {
			GlobalDetail.sendStpesSession.getAsyncRemote().sendText(resobj.toString());
		}
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();

	}

}
