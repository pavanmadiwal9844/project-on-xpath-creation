package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Iterator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.simplifyqa.SeleniumFW.ToolDriver;
import com.simplifyqa.Utility.UserDir;

public class cswgsExecution extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Logger logger = LoggerFactory.getLogger(Webexe.class);

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.setStatus(HttpStatus.OK_200);
		JSONObject rq = new JSONObject();
		rq.put("data", "Alive");
		resp.getWriter().println(rq.toString());
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws FileNotFoundException {
		File file = new java.io.File("exception.log");
		PrintStream ps = new java.io.PrintStream(file);
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace(ps);
		}
		JSONObject resobj = new JSONObject(jb.toString());
		String path = UserDir.Webagent().getAbsolutePath();
		logger.debug(path);
		resobj.remove("application");
		resobj.remove("path");
		StringBuilder command = SystemUtils.IS_OS_WINDOWS
				? new StringBuilder("java -jar \"" + path + File.separator + "QMSCO.jar\" ")
				: new StringBuilder("java -jar " + path + File.separator + "QMSCO.jar ");
		try {
			File fl = new java.io.File(path);
			FileWriter f = new FileWriter(file);
			f.write(path);
			f.close();
			if ((!fl.exists())) {
				resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				JSONObject res = new JSONObject();
				res.put("data", "specified path does not exist , Jar not found!!");
				resp.getWriter().println(res);
			} else {

				Iterator<String> jsonkey = resobj.keys();
				while (jsonkey.hasNext()) {
					String key = (String) jsonkey.next();
					command.append("-" + key + " " + resobj.get(key) + " ");
				}
				 fl = new java.io.File(path);
				 f = new FileWriter(file);
				f.write(command.toString());
				f.close();
				final ProcessBuilder process = new ProcessBuilder(Arrays.asList(command.toString().split(" ")));
				process.redirectOutput(ProcessBuilder.Redirect.to(new File("QAWEB.log")));
				process.redirectError(ProcessBuilder.Redirect.to(new File("QAWEB.log")));
				process.directory(new File(path));
				process.start();
				resp.setStatus(HttpStatus.OK_200);
				resp.addHeader("Access-Control-Allow-Origin", "*");
				resp.addHeader("Access-Control-Allow-Headers", "*");
				resp.addHeader("Access-Control-Allow-Methods", "*");
				JSONObject res = new JSONObject();
				res.put("data", "success");
				resp.getWriter().println(res);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
