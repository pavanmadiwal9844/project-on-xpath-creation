package com.simplifyqa.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;

import com.simplifyqa.handler.Events;
import com.simplifyqa.handler.Service;
import com.simplifyqa.handler.Task;

public class MbSettings extends HttpServlet{

	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setStatus(HttpStatus.OK_200);
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "*");
		resp.addHeader("Access-Control-Allow-Methods", "*");
		resp.getWriter().println();
	}
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)

		{
			e.printStackTrace();
		}
			JSONObject resobj = new JSONObject(jb.toString());
			JSONObject obj = new JSONObject();
			CompletableFuture<?> cf = new CompletableFuture<>();
			boolean res= false;
			System.out.println(resobj.toString());
			Iterator<String> keyItr = resobj.keys();
			while (keyItr.hasNext()) {
				String key = keyItr.next();
				switch (key) {
				case "wifi":
					obj = (JSONObject) resobj.get(key);
					cf = new CompletableFuture<>();
					Service.a().a(new Task(Events.wifi, cf, obj));
					res = (Boolean)cf.join();
					if (res) {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Executed"));
					}
					else {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Failed"));
					}
					
					break;
				case "data":
					obj = (JSONObject) resobj.get(key);
					cf = new CompletableFuture<>();
					Service.a().a(new Task(Events.data, cf, obj));
					res = (Boolean)cf.join();
					if (res) {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Executed"));
					}
					else {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Failed"));
					}
					break;
				case "bluetooth":
					obj = (JSONObject) resobj.get(key);
					cf = new CompletableFuture<>();
					Service.a().a(new Task(Events.bluetooth, cf, obj));
					res = (Boolean)cf.join();
					if (res) {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Executed"));
					}
					else {
						resp.setStatus(HttpStatus.OK_200);
						resp.addHeader("Access-Control-Allow-Origin", "*");
						resp.addHeader("Access-Control-Allow-Headers", "*");
						resp.addHeader("Access-Control-Allow-Methods", "*");
						resp.getWriter().println(new JSONObject().put("data", "Failed"));
					}
					break;

				}
			}
		}

	
}
