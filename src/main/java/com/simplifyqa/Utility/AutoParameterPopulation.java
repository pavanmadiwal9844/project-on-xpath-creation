package com.simplifyqa.Utility;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.LoggerFactory;

import com.github.dockerjava.core.DockerClientBuilder;
import com.simplifyqa.CodeEditor.CodeEditorDebugger;
import com.simplifyqa.agent.CreateStep;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.method.*;

import ch.qos.logback.classic.Logger;
import jnr.ffi.annotations.In;

public class AutoParameterPopulation {
	
	

	public static boolean autoParameterMethod(String customerId, String userId,String authToken,String serverIp) {

		try {
			JSONArray method_list = new JSONArray();
			if(VerifyDockerRunning(customerId)) {
				CodeEditorDebugger codeeditor = new CodeEditorDebugger();
				JSONArray CustomMethodsList = new JSONArray();
				CustomMethodsList=codeeditor.method_list(customerId);
				System.out.println(CustomMethodsList.toString());
				
				for(int ce_list=0;ce_list<CustomMethodsList.length();ce_list++) {
					method_list.put(CustomMethodsList.getJSONObject(ce_list));
				}
			}
			
			
			JSONObject sync_payload = new JSONObject();
			List<Sqabind> allannotations = new ArrayList<Sqabind>();
			HashMap<String, String> header = new HashMap<String, String>();
			header.put("Content-Type", "application/json");

			Reflections reflections = new Reflections("com.simplifyqa.method", new MethodAnnotationsScanner());
			Set<Method> webmethods = reflections.getMethodsAnnotatedWith(Sqabind.class);

			for (Method m1 : webmethods) {
				JSONObject current_method = new JSONObject();
				JSONArray params = new JSONArray();
				Sqabind s = m1.getAnnotation(Sqabind.class);

				current_method.put("actionname", s.actionname());
				current_method.put("action_displayname", s.action_displayname());
				current_method.put("action_description", s.action_description());
				current_method.put("paraname_equals_curob", Boolean.parseBoolean(s.paraname_equals_curobj()));
				current_method.put("param_count", s.paramnames().length);
				current_method.put("object_template",s.object_template());

				if (s.paramnames().length != 0) {
					System.out.println(s.toString());
					for (int i = 0; i < s.paramnames().length; i++) {
						JSONObject current_param = new JSONObject();

						current_param.put("type", s.paramtypes()[i]);

						if (s.paramnames().equals("")) {
							current_param.put("name", "");
						} else {
							current_param.put("name", s.paramnames()[i]);
						}
						params.put(current_param);
					}
				}

				current_method.put("params", params);
				method_list.put(current_method);

			}
			
			sync_payload.put("data", method_list);
			sync_payload.put("customerId", Integer.parseInt(customerId));
			sync_payload.put("userId", Integer.parseInt(userId));
			System.out.println(sync_payload.toString());
			System.out.println(method_list.length());
			//to be removed
			header.put("authorization",authToken);
//			header.put("authorization","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjozMzU2LCJjcmVhdGVkQnkiOjU3NzUsInByb2plY3RJZCI6ODg4NywiYXV0b21hdGlvbiI6dHJ1ZSwiYnJvd3Nlck5hbWUiOiJDaHJvbWUiLCJkZXZpY2VzIjpbIkNocm9tZSJdLCJzdGF0dXMiOiJJTlBST0dSRVNTIiwicmVzdWx0RGF0YSI6W10sImxvY2FsZXhlY3V0aW9uIjp0cnVlLCJ2ZXJzaW9uTmFtZSI6InYxLjAiLCJlbnZpcm9ubWVudFR5cGUiOiJnZW5lcmFsIiwidmVyc2lvbiI6MSwidGVzdGNhc2VDb2RlIjoiVEMtMzM1NjI4MTU5IiwidGVzdGNhc2VJZCI6MjgxNTksInRjU2VxIjoxLCJtb2R1bGVJZCI6MjY3NywidGNUeXBlIjoid2ViIiwiZHJ5cnVuIjp0cnVlLCJtYWlsZXJzTGlzdCI6WzYxNjldLCJuYW1lIjoic2RmZ2hqIiwidGVzdGRhdGFJdHIiOltdLCJ0eXBlIjoiZXhlY3V0aW9uIiwiaWF0IjoxNjQ5Njk0ODA1LCJleHAiOjE2NDk3ODEyMDV9.ZBGMmLKSmCV9hgYReu6NC427ZioGMI2EWvlLWfOz4WM");

			JSONObject sync_response = HttpUtility.sendPost(serverIp+"/syncAction", sync_payload.toString(), header);
			if (sync_response.has("success")) {
				if (sync_response.getBoolean("success")) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}

		}

		catch (Exception e) {
			return false;
		}

	}
	
	public static boolean VerifyDockerRunning(String customerId) {
		try {
			com.github.dockerjava.api.DockerClient dockerclient = DockerClientBuilder.getInstance().build();
			List<com.github.dockerjava.api.model.Image> images = dockerclient.listImagesCmd().exec();
			List<com.github.dockerjava.api.model.Container> containers = dockerclient.listContainersCmd().exec();
			
			CodeEditorDebugger codeeditor = new CodeEditorDebugger();
			if(codeeditor.getallprojects()) {
				System.out.println("All Jars Pulled");
			}else {
				System.out.println("Project jars couldn't be downloaded, check if Container running.");
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
	}

}