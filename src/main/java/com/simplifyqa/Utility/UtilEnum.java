package com.simplifyqa.Utility;

public enum UtilEnum {

	BOOLEAN("boolean"), CUSTOMERID("customerId"), INTEGER("integer"), PROJECTID("projectId"), DELETED("deleted"),
	DATA("data"), SKIP("skip"), STAR("*"), TESTCASEID("testcaseId"), EXECUTIONID("executionId"), TCSEQ("tcSeq"),
	VERSION("version"), SUITEID("suiteId"), SUITEIDS("suiteIds"), TESTCASECODE("testcaseCode"), SEARCH("/search"),
	ID("id"), EXECUTION("execution"), FALSE("false"), TRUE("true"), MODULEID("moduleId"),
	EXECUTIONDATA("/v2/executionData"), NAME("name"), IF("IF"), SERVERIP("serverIp"), DEVICEID("deviceId"),
	EXECUTIONTESTCASE("/v1/executionTestcase"), INPROGRESS("Inprogress"), CALL("call"), ITR("itr"), STEP("step"),
	TESTDATA("testdata"), BROWSER("browser"), RESULT("result"), SCREENSHOT("captureScreenshot"),
	EXECUTIONRESULT("/v1/executionresult/step"), FINISHEXECUTION("/v1/finishExecution"),
	ITERATIONSELECTED("iterationSelected"), FUNCTIONSTEPDATA("/v1/functionStepData"),
	MAINFRAMEPLAYBACK("SimplyPlayback.exe"), MAINFRAMERECORDER("SimplyRecord.exe"), MAINFRAMEFOLDER("MainFrame_RP"),
	DESKTOPFOLDER("Desktop_RP"), MAINFRAMERCFOLDER("Record"), MAINFRAMEPLFOLDER("Playback"), MAINFRAME("mainframe"),
	RESULTSTEP("resultStep"), IOS("ios"), ANDROID("android"), WEB("web"), FAILED("Failed"), PASSED("Passed"),
	UPDSTERP("/v1/updaterp"), KEY("key"), IDENTIFIERNAME("identifiername"), IDENTIFIERVALUE("identifiervalue"),
	DESKTOPRECORDFOLDER("Desktop Recorder"), DESKTOPRECORD("SimplifyDesktopRecord.exe"),
	DESKTOPPLAYBACKFOLDER("Desktop Playback"), DESKTOPPLAYBACK("SimplifyDesktopPlayback.exe"),
	DESKTOPINSPECTOR("SimplifyDesktopObjectPicker.exe"),DESKTOPVALIDATION("SimplifyDesktopObjectValidation.exe"),DESKTOPOBJECTPICKERFOLDER("Desktop ObjectPicker"),
	MAINFRAMEOBJECTPICKER("ObjectPicker"), MAINFRAMEINSPECTOR("SimplyOP.exe"),
	SAPFOLDER("Sap_RP"),SAPRECORDFOLDER("Sap Recorder"),SAPRECORD("SimplifySAPGUIRecord.exe"),
	SAPPLAYBACKFOLDER("Sap Playback"), SAPPLAYBACK("SimplifySAPGUIPlayback.exe"),
	 SAPINSPECTOR("SimplifySAPGUIObjectPicker.exe"),SAPVALIDATIONFOLDER("Sap Validate"),SAPINSPECTFOLDER("Sap Inspect"),
	SAPVALIDATION("SimplifySAPValidate.exe"),SAPOBJECTPICKER("Sap ObjectPicker"),
	MAINFRAMEVALIDATION("SimplifyOpValidate.exe"), SCENARIO("/v1/getScenarioExecTcs"), user_prefernce("user_prefernce"),
	userId("userId"), mySQL("mysql"), mySQLDriver("com.mysql.jdbc.Driver"), MYSQLURL("jdbc:mysql://"), Oracle("Oracle"),
	OrDriver("oracle.jdbc.driver.OracleDriver"), SQL("sql"), SQLdriver("com.sql.jdbc.Driver"),
	CHAR_LIST("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), ALPHA_NUMERIC("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"),SAVETCRESULT("/saveresult/testcase"),PROJECT("project"),CUSTOMER("customer");

	private final String value;

	private UtilEnum(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

}
