package com.simplifyqa.Utility;

import java.io.File;
import java.nio.file.Paths;

public class UserDir {

	public static File getLibjpeg() {
		return new File(Paths.get(getScrcpy().getAbsolutePath(), new String[] { "libs" }).toUri());
	}

	public static File getCompress() {
		return new File(Paths.get(getScrcpy().getAbsolutePath(), new String[] { "jniLibs" }).toUri());
	}

	public static File getscrcpyserver() {
		return new File(Paths.get(getScrcpy().getAbsolutePath(), new String[] { "server", "scrcpy-server" }).toUri());
	}

	public static File getScrcpy() {
		return new File(Paths.get(getAapt().getAbsolutePath(), new String[] { "scrcpy" }).toUri());
	}

	public static File getAapt() {
		return new File(Paths.get("libs").toUri());
	}
	
	public static File getLogpath() {
		return new File(Paths.get("log").toUri());
	}
	
	public static File getlogfile() {
		return new File(Paths.get("SqaAgent.log").toUri());
	}
	
	public static File getUserLogs() {
		return new File(Paths.get("UserLogs.log").toUri());
	}
	
	public static File Codeeditor() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "Codeeditor").toUri());
		
	}
	
	public static File CustomMethodSyncing() {
		return new File(Paths.get(Codeeditor() + File.separator + "SyncJars").toUri());
		
	}
	
	public static File getExternaljar() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "External_Jar").toUri());
	}

	public static File getImage() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "images").toUri());
	}

	public static File getdocpath() {
//		System.out.println(new File(Paths.get("doc").toUri()));
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "doc").toUri());
	}

	public static File getadbPath() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "android-sdk").toUri());
	}

	public static File setting() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "apk").toUri());
	}

	public static File driver() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "drivers").toUri());
	}

	public static File Webagent() {
		return new File(Paths.get(getAapt().getAbsolutePath() + File.separator + "QAFile").toUri());
	}
}
