package com.simplifyqa.Utility;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.SSLContext;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.httpclient.params.HttpParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.logger.LoggerClass;

public class HttpUtility {

	final static Logger logger = LoggerFactory.getLogger(HttpUtility.class);
	public static String proxyip = null;
	public static String port = null;
	public static String username = null;
	public static String password = null;
	public static Boolean proxyflag = false;
	public static String udid = null;
	
	private static final org.apache.log4j.Logger APIlogger = org.apache.log4j.LogManager
			.getLogger(HttpUtility.class);

	static LoggerClass APILogger = new LoggerClass();

//storing runtime variable check
	public static String SendPost(String url, String param, HashMap<String, String> header)
			throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		CloseableHttpClient httpClient = getHttpClient(url);
		
		APILogger.customloggAPI(APIlogger, "URL - ", null, url, "info");
		APILogger.customloggAPI(APIlogger, "Method - ", null, "Post", "info");
		APILogger.customloggAPI(APIlogger, "Payload - ", null, param, "info");


		HttpPost httpPost = new HttpPost(url);
		int timeout = 12; // seconds
		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						httpPost.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			;
			// logger.info(param);
			StringEntity params = new StringEntity(param, "UTF-8");
			for (Entry<String, String> map : header.entrySet())
				httpPost.addHeader(map.getKey(), map.getValue());
			httpPost.setEntity(params);
			HttpResponse response = null;
			int check = 0;
			int counter = 0;
			int hardTimeout = 30;
			// System.out.println("You are executing version : 1.0.0");

			do {
				if (InitializeDependence.ios_device) {
					TimerTask task = new TimerTask() {
						@Override
						public void run() {
							if (httpPost != null) {
								httpPost.abort();
							}
						}
					};
					new Timer(true).schedule(task, hardTimeout * 1000);
				}
				counter++;

				response = httpClient.execute(httpPost);
				check = response.getStatusLine().getStatusCode();
				
				APILogger.customloggAPI(APIlogger, "Status Code - ", check, null, "info");


				logger.info("status code :" + check);

				if (check != 200) {
					Thread.sleep(1000);
				}

			} while (check != 200 && counter < 6);

			switch (response.getStatusLine().getStatusCode()) {
			case 200:
				return EntityUtils.toString(response.getEntity(), "UTF-8");
			default:
				logger.error(String.format("Error"));
				logger.error(String.format("%d: %s", response.getStatusLine().getStatusCode(),
						EntityUtils.toString(response.getEntity(), "UTF-8")));
				logger.error(url);
				logger.error(param);
				break;
			}
		} catch (ParseException | IOException | InterruptedException e) {

//			logger.info("firefox :"+ InitializeDependence.firefox_quicklaunch);
			if (!InitializeDependence.firefox_quicklaunch) {

				try {
					Thread.sleep(1000);
					if (InitializeDependence.api_recount < 6) {
//					logger.info("inside api catch block,retrail no." +InitializeDependence.api_recount );
						InitializeDependence.api_recount++;
						SendPost(url, param, header);
					} else {
						e.printStackTrace();
						httpClient.close();
						return "Error";
					}
				} catch (Exception e1) {
					// TODO: handle exception
//					logger.info(e1.toString());
					return "Error";
				}
			}

			else {
				e.printStackTrace();
				httpClient.close();
				return "Error";
			}
		}
		return "Error";
	}

	public static HttpResponse postapi(String url, String m_Method, String param, HashMap<String, String> header)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException,
			IOException {
		JSONObject resp = null;
		HttpResponse response = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);

		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			if (m_Method.equalsIgnoreCase("post")) {
				StringEntity params = new StringEntity(param, "UTF-8");
				for (Entry<String, String> map : header.entrySet())
					post.addHeader(map.getKey(), map.getValue());
				post.setEntity(params);
				response = httpClient.execute(post);

			} else if (m_Method.equalsIgnoreCase("get")) {
				HttpGet getRequest = new HttpGet(url);
				for (Entry<String, String> map : header.entrySet())
					getRequest.addHeader(map.getKey(), map.getValue());
				response = httpClient.execute(getRequest);
			} else if (m_Method.equalsIgnoreCase("put")) {
				StringEntity params = new StringEntity(param, "UTF-8");
				HttpPut put = new HttpPut(url);
				for (Entry<String, String> map : header.entrySet())
					put.addHeader(map.getKey(), map.getValue());
				put.setEntity(params);
				response = httpClient.execute(put);

			} else if (m_Method.equalsIgnoreCase("delete")) {
				HttpDelete deleteRequest = new HttpDelete(url);
				for (Entry<String, String> map : header.entrySet())
					deleteRequest.addHeader(map.getKey(), map.getValue());
				response = httpClient.execute(deleteRequest);
			}

		} catch (Exception e) {
			logger.error("err", e);
			httpClient.close();
			resp = new JSONObject().put("error", "Error");
		}
		return response;
	}

	public static HttpResponse postapi(String url, String m_Method, String param, HashMap<String, String> header,
			String userName, String password) throws KeyManagementException, NoSuchAlgorithmException,
			KeyStoreException, URISyntaxException, IOException {
		JSONObject resp = null;
		HttpResponse response = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);

		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		String authr = userName + ":" + password;
		String encoding = DatatypeConverter.printBase64Binary(authr.getBytes("UTF-8"));
// post.setHeader("Authorization", "Basic " + encoding);
// post.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
		try {
			if (m_Method.equalsIgnoreCase("post")) {
				StringEntity params = new StringEntity(param, "UTF-8");
				for (Entry<String, String> map : header.entrySet())
					post.addHeader(map.getKey(), map.getValue());
				post.setHeader("Authorization", "Basic " + encoding);
				post.setEntity(params);
				response = httpClient.execute(post);

			} else if (m_Method.equalsIgnoreCase("get")) {
				HttpGet getRequest = new HttpGet(url);
				for (Entry<String, String> map : header.entrySet())
					getRequest.addHeader(map.getKey(), map.getValue());
				getRequest.setHeader("Authorization", "Basic " + encoding);
				response = httpClient.execute(getRequest);

			} else if (m_Method.equalsIgnoreCase("put")) {
				StringEntity params = new StringEntity(param, "UTF-8");
				HttpPut put = new HttpPut(url);
				for (Entry<String, String> map : header.entrySet())
					put.addHeader(map.getKey(), map.getValue());
				put.setHeader("Authorization", "Basic " + encoding);
				put.setEntity(params);
				response = httpClient.execute(put);

			} else if (m_Method.equalsIgnoreCase("delete")) {
				HttpDelete deleteRequest = new HttpDelete(url);
				for (Entry<String, String> map : header.entrySet())
					deleteRequest.addHeader(map.getKey(), map.getValue());
				deleteRequest.setHeader("Authorization", "Basic " + encoding);
				response = httpClient.execute(deleteRequest);
			}

		} catch (Exception e) {
			logger.error("err", e);
			httpClient.close();
			resp = new JSONObject().put("error", "Error");
		}
		return response;
	}

//report save
	public static JSONObject sendPost(String url, String param, HashMap<String, String> header)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException,
			IOException {
		
		APILogger.customloggAPI(APIlogger, "URL - ", null, url, "info");
		APILogger.customloggAPI(APIlogger, "Method - ", null, "Post", "info");
		APILogger.customloggAPI(APIlogger, "Payload - ", null, param, "info");

		
		JSONObject resp = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);
		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			// logger.info(param);
			StringEntity params = new StringEntity(param, "UTF-8");
			for (Entry<String, String> map : header.entrySet())
				post.addHeader(map.getKey(), map.getValue());
			post.setEntity(params);

			HttpResponse response = null;
			int counter = 0;
			int check = 0;
			int hardTimeout = 30;

			do {
				counter++;
				if (InitializeDependence.ios_device) {
					TimerTask task = new TimerTask() {
						@Override
						public void run() {
							if (post != null) {
								post.abort();
							}
						}
					};
					new Timer(true).schedule(task, hardTimeout * 1000);
				}
				// System.out.println("\nRequest body : "+param);
				response = httpClient.execute(post);
				// System.out.println("\nResponse : "+response);
				check = response.getStatusLine().getStatusCode();
				// System.out.println("STATUS CODE : "+check);
				APILogger.customloggAPI(APIlogger, "Status Code - ", check, null, "info");

				if (check != 200) {
					Thread.sleep(2000);
				}
			} while (check != 200 && counter < 6);

			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			if (statusLine == 200) {
				resp = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
			} else {
				String s = EntityUtils.toString(response.getEntity(), "UTF-8");
				resp = new JSONObject(s);
			}

		} catch (Exception e) {

			if (InitializeDependence.check_api_count == 5) {
				logger.info("checking point");
			}
			if (InitializeDependence.check_api_count < 6) {

				logger.info(e.toString());
				logger.info("inside api catch block,retrail no." + InitializeDependence.api_recount);
				InitializeDependence.check_api_count++;
				try {
					Thread.sleep(1000);
					sendPost(url, param, header);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					sendPost(url, param, header);
				}
//				SendPost(url, param, header)

			} else {
				e.printStackTrace();
				httpClient.close();
				resp.put("error", "error");
				return resp;
			}
		}
		APILogger.customloggAPI(APIlogger, "URL - ", null, url, "info");
		APILogger.customloggAPI(APIlogger, "Method - ", null, "Post", "info");
		APILogger.customloggAPI(APIlogger, "Payload - ", null, param, "info");

		return resp;

	}

	public static JSONObject getcall(String url) {
		int timeout = 5; // seconds
		RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
				.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
		CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		HttpGet getRequest = new HttpGet(url);
		CloseableHttpResponse response = null;
		try {
			getRequest.addHeader("accept", "application/json");
			try {
				response = httpClient.execute(getRequest);
				HttpEntity httpEntity = response.getEntity();
				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != 200) {
					throw new RuntimeException("Failed with HTTP error code : " + statusCode);
				} else {
					return new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
				}
			} finally {

			}
		} catch (Exception e) {
			return new JSONObject("Error");
		} finally {
			try {
				httpClient.close();
				getRequest.releaseConnection();
				getRequest.releaseConnection();
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String Get(int Localport) throws IOException {
		int timeout = 50; // seconds
		RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
				.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
		CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		try {
			String str1 = String.format("http://localhost:%d/wd/hub/apps", new Object[] { Integer.valueOf(Localport) });
			HttpGet getRequest = new HttpGet(str1);
			if (!(str1.contains("localhost") || str1.contains("127.0.0.1"))) {
				String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
						.toString();
				;
				File file = new File(path);
				if (file.exists()) {
					getProxy();
					if (proxyflag) {
						if (proxyip != null && port != null) {
							if (username != null && password != null) {
								CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
								credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
										new UsernamePasswordCredentials(username, password));
								HttpClientBuilder builder = HttpClients.custom()
										.setDefaultCredentialsProvider(credentialsPovider);
								httpClient = builder.build();
								logger.info(credentialsPovider.toString() + password);
							}
							HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
							RequestConfig config1 = RequestConfig.custom().setProxy(proxy).build();
							getRequest.setConfig(config1);
							logger.info("request is going from proxy server" + config1.toString());
						}
					}
				}
			}
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				throw new RuntimeException("Failed with HTTP error code : " + statusCode);
			} else {
				String out = EntityUtils.toString(httpEntity, "UTF-8");
				logger.info(out);
				return out;
			}
		} catch (Exception e) {
			logger.error("err", e);
		} finally {
// Important: Close the connect
			httpClient.close();
		}
		return null;
	}

	public static JSONObject get(String url, Map<String, String> queryParam, Map<String, String> headers) {
		JSONObject resp = null;
		try {
			CloseableHttpClient httpClient = getHttpClient(url);
			URIBuilder uri = new URIBuilder(url);
			HttpGet get = new HttpGet(uri.build());
			if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
				String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
						.toString();
				;
				File file = new File(path);
				if (file.exists()) {
					getProxy();
					if (proxyflag) {
						if (proxyip != null && port != null) {
							if (username != null && password != null) {
								CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
								credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
										new UsernamePasswordCredentials(username, password));
								HttpClientBuilder builder = HttpClients.custom()
										.setDefaultCredentialsProvider(credentialsPovider);
								httpClient = builder.build();
								logger.info(credentialsPovider.toString() + password);
							}
							HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
							RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
							get.setConfig(config);
							logger.info("request is going from proxy server" + config.toString());
						}
					}
				}
			}
			if (null != queryParam && !queryParam.isEmpty()) {
				queryParam.forEach((key, value) -> {
					uri.addParameter(key, value);
				});
			}

			if (null != headers && !headers.isEmpty()) {
				headers.forEach((k, v) -> {
					get.addHeader(k, v);
				});
			}
			CloseableHttpResponse response = httpClient.execute(get);
			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			HttpEntity entity = response.getEntity();
			resp = new JSONObject(EntityUtils.toString(entity));
		} catch (Exception e) {
			resp = new JSONObject().put("error", e.getMessage());
		}
		return resp;
	}

	public static JSONObject delete(String url) {
		JSONObject resp = null;
		try {
			CloseableHttpClient httpClient = getHttpClient(url);
			URIBuilder uri = new URIBuilder(url);
			HttpDelete del = new HttpDelete(uri.build());
			if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
				String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
						.toString();
				;
				File file = new File(path);
				if (file.exists()) {
					getProxy();
					if (proxyflag) {
						if (proxyip != null && port != null) {
							if (username != null && password != null) {
								CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
								credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
										new UsernamePasswordCredentials(username, password));
								HttpClientBuilder builder = HttpClients.custom()
										.setDefaultCredentialsProvider(credentialsPovider);
								httpClient = builder.build();
								logger.info(credentialsPovider.toString() + password);
							}
							HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
							RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
							del.setConfig(config);
							logger.info("request is going from proxy server" + config.toString());
						}
					}
				}
			}
			int hardTimeout = 5;
			if (InitializeDependence.ios_device) {
				TimerTask task = new TimerTask() {
					@Override
					public void run() {
						if (del != null) {
							del.abort();
						}
					}
				};
				new Timer(true).schedule(task, hardTimeout * 1000);
			}
			CloseableHttpResponse response = httpClient.execute(del);
			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			HttpEntity entity = response.getEntity();
			resp = new JSONObject(EntityUtils.toString(entity));
		} catch (Exception e) {
			resp = new JSONObject().put("error", e.getMessage());
		}
		return resp;

	}

	public static InputStream getFilePost(String url, String param, HashMap<String, String> header)
			throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		InputStream file1 = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);
		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			StringEntity params = new StringEntity(param, "UTF-8");
			for (Entry<String, String> map : header.entrySet())
				post.addHeader(map.getKey(), map.getValue());
			post.setEntity(params);
			HttpResponse response = httpClient.execute(post);
			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			if (statusLine == 200) {
				HttpEntity entity = response.getEntity();
				if (null != entity) {
					file1 = entity.getContent();
				}
			} else {
				file1 = null;
			}

		} catch (Exception e) {
			logger.error("err", e);
			httpClient.close();
			file1 = null;
		}
		return file1;

	}

	public static InputStream getFile(String url, Map<String, String> queryParam, Map<String, String> headers) {
		InputStream file = null;
		try {
			CloseableHttpClient httpClient = getHttpClient(url);
			URIBuilder uri = new URIBuilder(url);
			HttpGet get = new HttpGet(uri.build());
			if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
				String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
						.toString();
				;
				File file1 = new File(path);
				if (file1.exists()) {
					getProxy();
					if (proxyflag) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						if (proxyip != null && port != null) {
							HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
							RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
							get.setConfig(config);
							logger.info("request is going from proxy server" + config.toString());
						}
					}
				}
			}
			if (null != queryParam && !queryParam.isEmpty()) {
				queryParam.forEach((key, value) -> {
					uri.addParameter(key, value);
				});
			}
			if (null != headers && !headers.isEmpty()) {
				headers.forEach((k, v) -> {
					get.addHeader(k, v);
				});
			}
			CloseableHttpResponse response = httpClient.execute(get);
			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				file = entity.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return file;

	}

	public static CloseableHttpClient getHttpClient(String url)

			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

		int timeout = 5; // seconds

		RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)

				.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;

		SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();

		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,

				NoopHostnameVerifier.INSTANCE);

		Registry<ConnectionSocketFactory> socketFactoryRegistry =

				RegistryBuilder.<ConnectionSocketFactory>create()

						.register("https", sslsf)

						.register("http", new PlainConnectionSocketFactory())

						.build();

		BasicHttpClientConnectionManager connectionManager =

				new BasicHttpClientConnectionManager(socketFactoryRegistry);

		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)

				.setConnectionManager(connectionManager).build();

		return httpClient;
	}

	public static void getProxy() throws IOException {
		String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" }).toString();
		Properties p = new Properties();
		FileReader reader = new FileReader(path);
		p.load(reader);
		if (path != null && !p.isEmpty() && p.getProperty("proxyip") != null && p.getProperty("port") != null) {
			proxyip = p.getProperty("proxyip");
			port = p.getProperty("port");
			username = p.getProperty("username");
			password = p.getProperty("password");
			if (p.getProperty("proxyflag").equals("1"))
				proxyflag = true;
			else
				proxyflag = false;
		}

	}

	public static JSONObject sendPostforloader(String url, String param, HashMap<String, String> header)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException,
			IOException {

		JSONObject resp = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);
		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			// logger.info(param);
			StringEntity params = new StringEntity(param, "UTF-8");
			for (Entry<String, String> map : header.entrySet())
				post.addHeader(map.getKey(), map.getValue());
			post.setEntity(params);

			HttpResponse response = null;
			int counter = 0;
			int check = 0;
			int hardTimeout = 30;

//			do {
			counter++;
			if (InitializeDependence.ios_device) {
				TimerTask task = new TimerTask() {
					@Override
					public void run() {
						if (post != null) {
							post.abort();
						}
					}
				};
				new Timer(true).schedule(task, hardTimeout * 1000);
			}
			// System.out.println("\nRequest body : "+param);
			response = httpClient.execute(post);
			// System.out.println("\nResponse : "+response);
			check = response.getStatusLine().getStatusCode();
			// System.out.println("STATUS CODE : "+check);

//				if (check != 200) {
//					Thread.sleep(2000);
//				}
//			} while (check != 200 && counter < 6);

			int statusLine = response.getStatusLine().getStatusCode();
			logger.debug("In get {} {}", url, statusLine);
			if (statusLine == 200) {
				resp = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
			} else {
				String s = EntityUtils.toString(response.getEntity(), "UTF-8");
				resp = new JSONObject(s);
			}

		} catch (Exception e) {

			if (!InitializeDependence.loader_present.equals("true")) {
				if (InitializeDependence.loader_xpath == null) {
					if (InitializeDependence.check_api_count < 3) {

						logger.info(e.toString());
						logger.info("inside api catch block,retrail no." + InitializeDependence.api_recount);
						InitializeDependence.check_api_count++;

//				SendPost(url, param, header)
						sendPost(url, param, header);
					} else {
						e.printStackTrace();
						httpClient.close();
						resp.put("error", "error");
						return resp;
					}
				} else {
					e.printStackTrace();
					httpClient.close();
					resp.put("error", "error");
					return resp;
				}
			}
		}

		return resp;
	}

	public static HttpResponse newpostapiimplementation(String url, String m_Method, String param,
			JSONArray query_params, HashMap<String, String> header) throws KeyManagementException,
			NoSuchAlgorithmException, KeyStoreException, URISyntaxException, IOException {
		JSONObject resp = null;
		HttpResponse response = null;
		CloseableHttpClient httpClient = getHttpClient(url);
		HttpPost post = new HttpPost(url);

		JSONObject param_obj = new JSONObject(param);

		// adding query paramters
		org.apache.http.params.HttpParams parameters = new BasicHttpParams();
		if (!query_params.isEmpty()) {
			for (int param_len = 0; param_len < query_params.length(); param_len++) {
				parameters.setParameter(query_params.getJSONObject(param_len).get("key").toString(),
						query_params.getJSONObject(param_len).get("value").toString());
			}

			post.setParams(parameters);
		}

		if (!(url.contains("localhost") || url.contains("127.0.0.1"))) {
			String path = Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "proxy.properties" })
					.toString();
			;
			File file = new File(path);
			if (file.exists()) {
				getProxy();
				if (proxyflag) {
					if (proxyip != null && port != null) {
						if (username != null && password != null) {
							CredentialsProvider credentialsPovider = new BasicCredentialsProvider();
							credentialsPovider.setCredentials(new AuthScope(proxyip, Integer.parseInt(port)),
									new UsernamePasswordCredentials(username, password));
							HttpClientBuilder builder = HttpClients.custom()
									.setDefaultCredentialsProvider(credentialsPovider);
							httpClient = builder.build();
							logger.info(credentialsPovider.toString() + password);
						}
						HttpHost proxy = new HttpHost(proxyip, Integer.parseInt(port), "http");
						RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
						post.setConfig(config);
						logger.info("request is going from proxy server" + config.toString());
					}
				}
			}
		}
		try {
			if (!param_obj.get("title").toString().equals("form-data")) {
				param= param_obj.getJSONObject("data").toString();
				if (m_Method.equalsIgnoreCase("post")) {
					StringEntity params = new StringEntity(param, "UTF-8");
					for (Entry<String, String> map : header.entrySet())
						post.addHeader(map.getKey(), map.getValue());
					post.setEntity(params);
					response = httpClient.execute(post);

				} else if (m_Method.equalsIgnoreCase("get")) {
					HttpGet getRequest = new HttpGet(url);
					for (Entry<String, String> map : header.entrySet())
						getRequest.addHeader(map.getKey(), map.getValue());
					response = httpClient.execute(getRequest);
				} else if (m_Method.equalsIgnoreCase("put")) {
					StringEntity params = new StringEntity(param, "UTF-8");
					HttpPut put = new HttpPut(url);
					for (Entry<String, String> map : header.entrySet())
						put.addHeader(map.getKey(), map.getValue());
					put.setEntity(params);
					response = httpClient.execute(put);

				} else if (m_Method.equalsIgnoreCase("delete")) {
					HttpDelete deleteRequest = new HttpDelete(url);
					for (Entry<String, String> map : header.entrySet())
						deleteRequest.addHeader(map.getKey(), map.getValue());
					response = httpClient.execute(deleteRequest);
				}
			} else {
//				param= param_obj.getJSONArray("data").get("key").toString();

				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				for (int formdata_length = 0; formdata_length < param_obj.getJSONArray("data")
						.length(); formdata_length++) {

					if (param_obj.getJSONArray("data").getJSONObject(formdata_length).has("active")) {
						if (Boolean.parseBoolean(param_obj.getJSONArray("data").getJSONObject(formdata_length)
								.get("active").toString())) {

							// check for type file or text
							if (param_obj.getJSONArray("data").getJSONObject(formdata_length).get("type").toString()
									.equals("text")) {
								StringBody stringBody1 = new StringBody(param_obj.getJSONArray("data")
										.getJSONObject(formdata_length).get("value").toString(),
										ContentType.MULTIPART_FORM_DATA);
								String formdata_key = param_obj.getJSONArray("data").getJSONObject(formdata_length)
										.get("key").toString();

								builder.addPart(formdata_key, stringBody1);
							}

							else if (param_obj.getJSONArray("data").getJSONObject(formdata_length).get("type")
									.toString().equals("file")) {
								File file = new File(Paths.get(param_obj.getJSONArray("data")
										.getJSONObject(formdata_length).get("value").toString()).toString());
								FileBody formdata_file = new FileBody(file, ContentType.MULTIPART_FORM_DATA);
								String formdata_file_key = param_obj.getJSONArray("data").getJSONObject(formdata_length)
										.get("key").toString();

								builder.addPart(formdata_file_key, formdata_file);
							}

							
						}

					}
				}
				

				HttpEntity entity = builder.build();

				if (m_Method.equalsIgnoreCase("post")) {
//					StringEntity params = new StringEntity(param, "UTF-8");
					for (Entry<String, String> map : header.entrySet())
						post.addHeader(map.getKey(), map.getValue());
					post.setEntity(entity);
					response = httpClient.execute(post);

				} else if (m_Method.equalsIgnoreCase("get")) {
					HttpGet getRequest = new HttpGet(url);
					for (Entry<String, String> map : header.entrySet())
						getRequest.addHeader(map.getKey(), map.getValue());
					response = httpClient.execute(getRequest);
				} else if (m_Method.equalsIgnoreCase("put")) {
//					StringEntity params = new StringEntity(param, "UTF-8");
					HttpPut put = new HttpPut(url);
					for (Entry<String, String> map : header.entrySet())
						put.addHeader(map.getKey(), map.getValue());
					put.setEntity(entity);
					response = httpClient.execute(put);

				} else if (m_Method.equalsIgnoreCase("delete")) {
					HttpDelete deleteRequest = new HttpDelete(url);
					for (Entry<String, String> map : header.entrySet())
						deleteRequest.addHeader(map.getKey(), map.getValue());
					response = httpClient.execute(deleteRequest);
				}

			}

		}

		catch (Exception e) {
			logger.error("err", e);
			httpClient.close();
			resp = new JSONObject().put("error", "Error");
		}
		return response;
	}
}