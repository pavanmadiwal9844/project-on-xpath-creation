package com.simplifyqa.Utility;

public enum ActionClass {

	WBACTION("wbAction"), ANACTION("anAction"), IOACTION("ioAction"), MFACTION("mfAction"), DKACTION("dkAction"), SAACTION("saAction"),
	GNACTION("gnAction"), DBACTION("dbAction"), APIACTION("apiAction"), WBTYPE("web"), MBTYPE("mobile"), APITYPE("api"),
	DBTYPE("db"), GENTYPE("general"), DKTYPE("desktop"), switchtomobile("switchtomobile"),
	switchtodesktop("switchtodesktop"), switchtoweb("switchtoweb");

	private final String value;

	private ActionClass(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
