package com.simplifyqa.Utility;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simplifyqa.DTO.Action;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.execution.Impl.AutomationImpl;
import com.simplifyqa.execution.Interface.ExecutionUtil;
import com.simplifyqa.execution.Interface.Executor;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.ApiMethod;
import com.simplifyqa.method.DesktopMethod;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.method.MainframeMethod;
import com.simplifyqa.method.SapMethod;
import com.simplifyqa.method.WebMethod;
import com.simplifyqa.mobile.android.AndroidMirror;
import com.simplifyqa.sqadrivers.androiddriver;
import com.simplifyqa.sqadrivers.iosdriver;
import com.simplifyqa.sqadrivers.webdriver;

public class InitializeDependence {

	public static MainframeMethod mfAction = new MainframeMethod();
	public static ExecutionUtil serverCall = new AutomationImpl();
	public static HashMap<String, AndroidMethod> androidDriver = new HashMap<String, AndroidMethod>();
	public static HashMap<String, IosMethod> iosDriver = new HashMap<String, IosMethod>();
	public static HashMap<String, WebMethod> webDriver = new HashMap<String, WebMethod>();
	public static GeneralMethod generaldriver = null;
	public static HashMap<String, ApiMethod> apidriver = new HashMap<String, ApiMethod>();
	public static DesktopMethod dtAction = new DesktopMethod();
	public static SapMethod sapAction = new SapMethod();
	
	//code editor drivers
	public static HashMap<String, webdriver> codeeditorwebdriver = new HashMap<String, webdriver>();
	public static HashMap<String, androiddriver> codeeditorandroiddriver = new HashMap<String, androiddriver>();
	public static HashMap<String, iosdriver> codeeditoriosdriver = new HashMap<String, iosdriver>();

	public static Integer seq = -1;
	public static Integer timeout = -1;
	public static JSONObject userdetail = new JSONObject();
	public static JSONObject runtimeJson = new JSONObject();
	public static HashMap<String, ArrayList<String>> actionlist = new HashMap<String, ArrayList<String>>();
	public static HashMap<String, Boolean> xctestrun = new HashMap<String, Boolean>();
	public static HashMap<String, Boolean> Appiumrun = new HashMap<String, Boolean>();
	public static HashMap<String, AndroidMirror> aMirror = new HashMap<String, AndroidMirror>();
	public static String downloadpath = null;
	public static String minwait = null;
	public static String medwait = null;
	public static String maxwait = null;
	public static Integer existTimeout = -1;
	public static boolean ios_device = false;
	public static Integer current_step = 0;
	public static boolean iteration = false;
	public static Integer api_recount = 0;
	public static boolean reference_playback = false;
	public static boolean pause = false;
	public static Integer check = 0;
	public static boolean mobile_pause = false;
	public static String recording_browser = "";
	public static Boolean recording_session = true;
	public static Boolean firefox_quicklaunch=false;
	public static String firefox_session="";
	public static Integer check_api_count=0;
	public static Integer step_pass=0;
	public static Boolean firefox_playback=false;
	public static String jar_version="2.0.9";
	public static String runtime_classname="customclass";
	public static String current_ce_project;
	public static boolean ce_ref_play=false;
	public static String loader_present="false";
	public static String loader_xpath=null;
	public static int loader_waitime=-1;
	public static boolean Click_timeout=false;
	public static String param_description_value=null;
	public static String param_description_name =null;
	public static boolean remove_params_fromreports=false;
	public static JSONObject protected_password=new JSONObject();
	public static boolean suite_new_session=true;
	public static int suite_testcase_sequence=0;
	public static int suite_tescase_count=0;
	public static String suite_session=null;
	public static int global_itr=0;
	public static boolean suite=false;
	public static int funcItr_suite=0;
	public static JsonNode findattr(List<JsonNode> Attributes, String identifierName) {
		try {
			for (int i = 0; i < Attributes.size(); i++)
				if (Attributes.get(i).get(UtilEnum.NAME.value()).asText().toLowerCase()
						.equalsIgnoreCase(identifierName))
					return Attributes.get(i);
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static List<JsonNode> findattrReplace(List<JsonNode> Attributes, String value) {
		try {
			for (int i = 0; i < Attributes.size(); i++)
				if (Attributes.get(i).get("value").asText().toLowerCase().contains("#replace"))
					((ObjectNode) Attributes.get(i)).put("value",
							Attributes.get(i).get("value").asText().replaceAll("#replace", value));
			return Attributes;

		} catch (Exception e) {
			return null;
		}
	}

	public static JSONObject findattrDesktop(List<JsonNode> Attributes, String identifierName) {
		try {
			for (int j = 0; j < Attributes.size(); j++) {
				if (Attributes.get(j).has("unique") && Attributes.get(j).get("unique").asBoolean()) {
					return new JSONObject(Attributes.get(j).toString()).put("unique", Boolean.TRUE);
				}
			}
			for (int i = 0; i < Attributes.size(); i++)
				if (Attributes.get(i).get(UtilEnum.NAME.value()).asText().toLowerCase()
						.equalsIgnoreCase(identifierName))
					return new JSONObject(Attributes.get(i).toString()).put("unique", Boolean.FALSE);
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static String[] getTdArray(Step tcfnStep, JsonNode tcData, Setupexec executionDetail, String udid) {
		String[] td = null;

		try {
			if (executionDetail != null) {
				td = new String[tcfnStep.getParameters().size()];
				if (!GlobalDetail.runTime.isEmpty() && GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						try {
							if ((tcfnStep.getAction().getName().toLowerCase().trim().equals("storeruntime"))) {
								td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							}
							if (tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")
									|| (tcfnStep.getAction().getName().toLowerCase().trim()
											.equals("randomalphanumeric"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("readtextandstore"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("readvalueandstore"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim()
											.equals("randomalphabets")) || (tcfnStep.getAction().getName().toLowerCase().trim()
													.equals("generaterandomstring"))) {
//								System.out.println("it entered the if condition ,initialize dependency");

								throw new Exception();
							}
							td[i] = GlobalDetail.runTime.get(udid.toLowerCase().trim())
									.get(tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
									.asText();
						} catch (Exception e) {
							try {
								td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
//								System.out.println(td[i]);
							} catch (Exception ex) {
								td[i] = "";
							}
						}
					}
				} else {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						try {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						} catch (Exception e) {
							td[i] = "";
						}
					}
				}
			}

			else {
				td = new String[tcfnStep.getParameters().size()];
				if (tcfnStep.getFunctionCode() != null) {

					if (!GlobalDetail.runTime.isEmpty()
							&& GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							try {
								if (tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")
										|| (tcfnStep.getAction().getName().toLowerCase().trim()
												.equals("randomalphanumeric"))
										|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("storeruntime"))
										|| (tcfnStep.getAction().getName().toLowerCase().trim()
												.equals("readtextandstore"))
										|| (tcfnStep.getAction().getName().toLowerCase().trim()
												.equals("readvalueandstore"))
										|| (tcfnStep.getAction().getName().toLowerCase().trim()
												.equals("randomalphabets"))|| (tcfnStep.getAction().getName().toLowerCase().trim()
														.equals("generaterandomstring"))) {

									throw new Exception();
								}
								td[i] = GlobalDetail.runTime.get(udid.toLowerCase().trim())
										.get(tcData.get(tcfnStep.getFunctionCode())
												.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
										.asText();
							} catch (Exception e) {
								try {
									td[i] = tcData.get(tcfnStep.getFunctionCode())
											.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
								} catch (Exception ex) {
									td[i] = "";
								}
							}
						}
					} else {
						for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
							try {
								td[i] = tcData.get(tcfnStep.getFunctionCode())
										.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
							} catch (Exception e) {
								td[i] = "";
							}
						}
					}

				} else if (!GlobalDetail.runTime.isEmpty()
						&& GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						try {
							if (tcfnStep.getAction().getName().toLowerCase().trim().equals("getfromruntime")
									|| (tcfnStep.getAction().getName().toLowerCase().trim()
											.equals("randomalphanumeric"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("storeruntime"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("readtextandstore"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim().equals("readvalueandstore"))
									|| (tcfnStep.getAction().getName().toLowerCase().trim()
											.equals("randomalphabets"))|| (tcfnStep.getAction().getName().toLowerCase().trim()
													.equals("generaterandomstring"))) {
								throw new Exception();
							}
							td[i] = GlobalDetail.runTime.get(udid.toLowerCase().trim())
									.get(tcData.get(tcfnStep.getFunctionCode())
											.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
									.asText();
						} catch (Exception e) {
							try {
								td[i] = GlobalDetail.runTime.get(udid.toLowerCase().trim())
										.get(tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText())
										.asText();
							} catch (Exception ex) {
								try {
									td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
								} catch (Exception xe) {
									td[i] = "";
								}
							}
						}
					}
				} else {
					for (int i = 0; i < tcfnStep.getParameters().size(); i++) {
						try {
							td[i] = tcData.get(tcfnStep.getParameters().get(i).get("name").asText()).asText();
						} catch (Exception e) {
							td[i] = "";
						}
					}
				}
			}
			return td;
		} catch (Exception e) {
			return null;
		}
	}

	public static String getTd(Step tcfnStep, JsonNode parmeter, JsonNode tcData, Setupexec executionDetail,
			String udid) {
		try {
			String td = null;
			if (executionDetail != null) {
				if (!GlobalDetail.runTime.isEmpty() && GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
					try {
						td = GlobalDetail.runTime.get(udid.toLowerCase().trim())
								.get(tcData.get(parmeter.get("name").asText()).asText()).asText();

					} catch (Exception e) {
						td = tcData.get(parmeter.get("name").asText()).asText();
					}
				} else {
//					System.out.println(parmeter.get("name").asText());
					td = tcData.get(parmeter.get("name").asText()).asText();
				}
			}

			else {
				if (tcfnStep.getFunctionCode() != null) {
					if (!GlobalDetail.runTime.isEmpty()
							&& GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
						try {
							td = GlobalDetail.runTime.get(udid.toLowerCase().trim()).get(
									tcData.get(tcfnStep.getFunctionCode()).get(parmeter.get("name").asText()).asText())
									.asText();
						} catch (Exception e) {
							td = tcData.get(tcfnStep.getFunctionCode()).get(parmeter.get("name").asText()).asText();
						}
					} else {
						td = tcData.get(tcfnStep.getFunctionCode()).get(parmeter.get("name").asText()).asText();
					}

				} else if (!GlobalDetail.runTime.isEmpty()
						&& GlobalDetail.runTime.containsKey(udid.toLowerCase().trim())) {
					try {
						td = GlobalDetail.runTime.get(udid.toLowerCase().trim())
								.get(tcData.get(tcfnStep.getFunctionCode()).get(parmeter.get("name").asText()).asText())
								.asText();
					} catch (Exception e) {
						td = tcData.get(parmeter.get("name").asText()).asText();
					}
				} else {
					td = tcData.get(parmeter.get("name").asText()).asText();
				}
			}

			return td;
		} catch (Exception e) {
			return null;
		}
	}

	public static Boolean ifelseeval(Step tcfnstep, JsonNode tcData, Setupexec executionDetail, String udid) {
		try {
			ScriptEngineManager jsM = new ScriptEngineManager();
			ScriptEngine scriptEngine = jsM.getEngineByName("JavaScript");
			String ifstring = "";
			Boolean condition = false;
			ifstring = tcfnstep.getIfstring();

			for (int j = 0; j < tcfnstep.getChildren().size(); j++) {
				String td = getTd(tcfnstep, tcfnstep.getChildren().get(j).getParameter1().get(0), tcData,
						executionDetail, udid);
				td = "\"" + td + "\"";
				ifstring = ifstring.replace(
						tcfnstep.getChildren().get(j).getParameter1().get(0).get(UtilEnum.NAME.value()).asText(), td);
				td = getTd(tcfnstep, tcfnstep.getChildren().get(j).getParameter2().get(0), tcData, executionDetail,
						udid);
				td = "\"" + td + "\"";
				ifstring = ifstring.replace(
						tcfnstep.getChildren().get(j).getParameter2().get(0).get(UtilEnum.NAME.value()).asText(), td);

			}
			ifstring = ifstring.replace(UtilEnum.IF.value(), "").replace(" ", "");
			condition = (Boolean) scriptEngine.eval(ifstring);
			return condition;
		} catch (Exception e) {
			return false;
		}
	}

	public static Boolean ifelseeval(ArrayList<Step> tcifSteps, JsonNode testData, Setupexec execSetupexec,
			String sessionId, int iF, Executor executor, HashMap<String, String> header,int itr) {
		try {
			Action a = new Action();
			Action o = new Action();
			LinkedList<Boolean> check = new LinkedList<>();
			LinkedList<String> operator = new LinkedList<>();
			List<JsonNode> parameters = new ArrayList<JsonNode>();
			boolean iscondtion = false;

			for (int i = 0; i < tcifSteps.get(iF).getChildren().size(); i++) {

				// if children is parameter
				if (tcifSteps.get(iF).getChildren().get(i).getObjectType() != null) {
					if (tcifSteps.get(iF).getChildren().get(i).getObjectType().equals("parameter")) {

						String operation = tcifSteps.get(iF).getChildren().get(i).getOperator();

						a.setName(operation);
						tcifSteps.get(iF).setAction(a);

						parameters.add(tcifSteps.get(iF).getChildren().get(i).getParameter1().get(0));
						parameters.add(tcifSteps.get(iF).getChildren().get(i).getParameter2().get(0));
						
						tcifSteps.get(iF).setParameters(parameters);
						InitializeDependence.current_step = InitializeDependence.current_step+1;
						if (executor.stepExecution(execSetupexec, tcifSteps.get(iF), testData, itr, header, sessionId)) {

							check.add(true);
							operator.add(tcifSteps.get(iF).getChildren().get(i).getCondition());
							parameters.remove(0);
							parameters.remove(0);
						} else {
							check.add(false);
							operator.add(tcifSteps.get(iF).getChildren().get(i).getCondition());
							parameters.remove(0);
							parameters.remove(0);
						}

					}

					// if children is object
					else if (tcifSteps.get(iF).getChildren().get(i).getObjectType().equals("object")) {

						// a.setName(tcifSteps.get(iF).getChildren().get(i).getActionname());
						for (int l = 0; l < tcifSteps.get(iF).getChildren().get(i).getParameters().size(); l++) {
							parameters.add(tcifSteps.get(iF).getChildren().get(i).getParameters().get(l));
						}

						step(tcifSteps.get(iF), i);

						if (executor.stepExecution(execSetupexec, step(tcifSteps.get(iF), itr), testData, i, header,
								sessionId)) {

							check.add(true);
							operator.add(tcifSteps.get(iF).getChildren().get(i).getCondition());

						} else {
							check.add(false);
							operator.add(tcifSteps.get(iF).getChildren().get(i).getCondition());
						}
						for (int l = 0; l < tcifSteps.get(iF).getChildren().get(i).getParameters().size(); l++) {
							parameters.remove(0);
						}

					}

					// iscondition final check
					if (check.size() == 2 && operator.size() == 2) {
						if (operator.get(0).equals("AND")) {
							iscondtion = check.get(0) && check.get(1);

						} else if (operator.get(0).equals("OR")) {
							iscondtion = check.get(0) || check.get(1);
						}
						check.removeFirst();
						operator.removeFirst();

					}

					else {
						iscondtion = check.get(0);
					}

				}

				else {

				}
			}

			return iscondtion;
		}catch(

	Exception e)
	{
		return false;
	}
	}

	public static Step step(Step tcifSteps, int i) {
		Step stepping = new Step();

		stepping.setAction(tcifSteps.getChildren().get(i).getAction());
		stepping.setObject(tcifSteps.getChildren().get(i).getObject());
		stepping.setActionname(tcifSteps.getChildren().get(i).getActionname());
		stepping.setDescription(tcifSteps.getChildren().get(i).getDescription());
		stepping.setParameters(tcifSteps.getChildren().get(i).getParameters());
		stepping.setObject_name(tcifSteps.getChildren().get(i).getObject_name());

		return stepping;

	}

	public static HashMap<String, JFrame> frame = new HashMap<String, JFrame>();

	public static void showPreLoader(String sessionId) {
		if (!frame.containsKey(sessionId))
			frame.put(sessionId, new JFrame());
		frame.get(sessionId).setUndecorated(true);
		frame.get(sessionId).getContentPane().setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		frame.get(sessionId).setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height / 2;
		int width = screenSize.width / 2;
		frame.get(sessionId).setSize(width, height);
		frame.get(sessionId).setLocationRelativeTo(null);
		frame.get(sessionId).setState((int) Float.MAX_VALUE);
		frame.get(sessionId).setFocusable(true);
		ImageIcon loading;
		loading = new ImageIcon(new File((UserDir.getImage() + File.separator + "hzk6C.gif")).getAbsolutePath());
		frame.get(sessionId).add(new JLabel("", loading, JLabel.CENTER));
		frame.get(sessionId).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.get(sessionId).setAlwaysOnTop(true);
		frame.get(sessionId).setVisible(true);

	}

	public static void closePreLoader(String sessionId) {
		if (frame.get(sessionId) != null) {
			frame.get(sessionId).setVisible(false);
			frame.get(sessionId).dispose();
			frame.remove(sessionId);
		}
	}

	public static JSONObject userdetails(JSONObject req, HashMap<String, String> header) {
		try {
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			SearchColumns searchcolumns = new SearchColumns();
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.CUSTOMERID.value(),
							req.getInt(UtilEnum.CUSTOMERID.value()), Boolean.parseBoolean(UtilEnum.FALSE.value()),
							UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.userId.value(), req.getInt(UtilEnum.userId.value()),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchcolumns.setStartIndex(0);
			searchcolumns.setLimit(100000);
			searchcolumns.setCollection(UtilEnum.user_prefernce.value());
			response = new JSONObject(
					HttpUtility.SendPost(req.getString(UtilEnum.SERVERIP.value()) + UtilEnum.SEARCH.value(),
							new ObjectMapper().writeValueAsString(searchcolumns), header));
			return userdetail = response.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
					.getJSONObject(0);
		} catch (Exception e) {
			return null;
		}
	}

}
