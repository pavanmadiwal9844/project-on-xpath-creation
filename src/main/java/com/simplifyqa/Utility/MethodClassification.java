package com.simplifyqa.Utility;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.DB;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.ApiMethod;
import com.simplifyqa.method.DesktopMethod;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.IosMethod;
import com.simplifyqa.method.MainframeMethod;
import com.simplifyqa.method.SapMethod;
import com.simplifyqa.method.WebMethod;

public class MethodClassification {

	public static void list_Method() {
		ArrayList<String> wM = new ArrayList<String>();
		for (Method wm : WebMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.WBACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : AndroidMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.ANACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : IosMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.IOACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : MainframeMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.MFACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : DesktopMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.DKACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : SapMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.SAACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : GeneralMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.GNACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : ApiMethod.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.APIACTION.value(), wM);
		wM = new ArrayList<String>();
		for (Method wm : DB.class.getMethods())
			wM.add(wm.getName());
		InitializeDependence.actionlist.put(ActionClass.DBACTION.value(), wM);

	}

	public static void main(String[] args) {
		ArrayList<String> wM = new ArrayList<String>();
		for (Method wm : GeneralMethod.class.getMethods())
		{
			if(wm.getName().equals("combineruntime")) {
				System.out.println(wm.getParameters()[0].getType());
			}
		}
	}

}
