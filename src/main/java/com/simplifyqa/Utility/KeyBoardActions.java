package com.simplifyqa.Utility;

public enum KeyBoardActions {
	
	ENTER("\uE006"),TAB("\uE004"),ALTLEFT("\uE00A"),ALTRIGHT("\uE052"),CONTROLLEFT("\uE009"),CONTROLRIGHT("\uE051"),OSLEFT("\uE03D"),
	OSRIGHT("\uE053"),SHIFTLEFT("\uE008"),SHIFTRIGHT("\uE050"),SPACE("\uE00D"),DELETE("\uE017"),END("\uE010"),HELP("\uE002"),HOME("\uE011"),
	INSERT("\uE016"),PAGEDOWN("\uE01E"),PAGEUP("\uE01F"),ARROWDOWN("\uE015"),ARROWLEFT("\uE012"),ARROWRIGHT("\uE014"),ARROWUP("\uE013"),
	ESCAPE("\uE00C"),F1("\uE031"),F2("\uE032"),F3("\uE033"),F4("\uE034"),F5("\uE035"),F6("\uE036"),F7("\uE037"),F8("\uE038"),F9("\uE039"),
	F10("\uE03A"),F11("\uE03B"),F12("\uE03C"),NUMPADADD("\uE024"),NUMPADCOMMA("\uE026"),NUMPADDECIMAL("\uE028"),NUMPADDIVIDE("\uE029"),
	NUMPADENTER("\uE007"),NUMPADMULTIPLY("\uE024"),NUMPADSUBTRACT("\uE026"),SHIFT("\uE008"),ALT("\uE00A"),CONTROL("\uE009"),BACKSPACE("\uE003");

	
	private final String value;

	private KeyBoardActions(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
