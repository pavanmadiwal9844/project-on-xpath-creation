package com.simplifyqa.Utility;

public class Constants {
	

	public static final String SERVERIP = "serverIp";
	public static final String NAME = "name";
	public static final String PLAYBACK = "Playback";
	public static final String CUSTOMERID = "customerId";
	public static final String INTEGER = "integer";
	public static final String BOOLEAN = "boolean";
	public static final String PROJECTID = "projectId";
	public static final String ID = "id";
	public static final String DELETED = "deleted";
	public static final String EXECUTION = "execution";
	public static final String DATA = "data";
	public static final String EXECUTIONID = "executionId";
	public static final String TESTCASEID = "testcaseId";
	public static final String TCSEQ = "tcSeq";
	public static final String VERSION = "version";
	public static final String BROWSER = "browser";
	public static final String AUTHKEY = "authKey";
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	public static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
	public static final String STAR = "*";
	public static final String TESTCASECODE = "testcaseCode";
	public static final String RESULTTC = "resultTc";
	public static final String INPROGRESS = "Inprogress";
	public static final String APPLICATIONJSON = "application/json";
	public static final String PARAMNAME = "paramname";
	public static final String PARAMVALUE = "paramvalue";
	public static final String AUTHORIZATION = "authorization";
	public static final String CONTENTTYPE = "Content-type";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String RECEIVED = "received";
	public static final String INDEX = "index";
	public static final String PARAMETERS = "parameters";
	public static final String STEPS = "steps";
	public static final String ACTION = "action";
	public static final String TEXT = "text";
	public static final String ATTRIBUTES = "attributes";
	public static final String CHILD = "child";
	public static final String VALUE = "value";
	public static final String DEFAULTVALUE = "defaultValue";
	public static final String SELECTEDOBJECT = "selectedObject";
	public static final String DISNAME = "disname";
	public static final String CAPTURESCREENSHOT = "captureScreenshot";
	public static final String OBJECT = "object";
	public static final String STRING = "string";
	public static final String PLAY_BACK = "play_back";
	public static final String TESTDATA = "testdata";
	public static final String ITR = "itr";
	public static final String STEPID = "stepId";
	public static final String RESULT = "result";
	public static final String FAILED = "Failed";
	public static final String SCREENSHOT = "screenshot";
	public static final String WAIT = "wait";
	public static final String PASSED = "Passed";
	public static final String PROTECTED = "protected";
	public static final String OBJTEMPLETEID = "objtemplateId";
	public static final String LIBS = "libs";
	public static final String CREATEDBY = "createdBy";
	public static final String SKIP = "skip";
	public static final String GECKO = "https://api.github.com/repos/mozilla/geckodriver/releases/latest";
	public static final String FILES = "https://qa.simplifyqa.com/getfiles";
	
	
	
	
	
	
	
	
	
	



}
