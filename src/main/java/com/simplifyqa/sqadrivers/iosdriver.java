package com.simplifyqa.sqadrivers;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.codoid.products.fillo.Recordset;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.IosMethod;

public class iosdriver {
	public static IosMethod iosmethods= new IosMethod();
	public static GeneralMethod generalMethod = new GeneralMethod();
	
	private Class myClass = null;
	private String classname = "com.android.WebCustom";
	private Editorclassloader customMethod = null;
	
	public static String getxmlDescription() {
		return iosmethods.getxmlDescription();
	}
	

	public static String getxml() {
		return iosmethods.getxml();
	}
	
	public static String getxmlJson() {
		return iosmethods.getxmlJson();
	}	
	public static String getxmlAccessibleSource() {
		return iosmethods.getxmlAccessibleSource();
	}
	
	public static void setSessionId(String sessionid) {
		iosmethods.setSessionId(sessionid);
	}
	
	public static boolean startapp(String ParamString) {
		return iosmethods.startapp(ParamString);
	}
	
	public static boolean isenabled() {
		return iosmethods.isenabled();
	}
	
	public static boolean isdisplayed() {
		return iosmethods.isdisplayed();
	}
	
	public static boolean until(String name, String value) throws InterruptedException {
		return iosmethods.until(name, value);
	}
	
	public static boolean click() {
		return iosmethods.click();
	}
	
	public static String getattrvalue(String value) {
		return iosmethods.getattrvalue(value);
	}
	
	public static Boolean readvalueandstore(String runtimePramaname) {
		return iosmethods.readandstore(runtimePramaname);
	}
	
	public static Boolean readtextandstore(String runtimePramaname) {
		return iosmethods.readtextandstore(runtimePramaname);		
	}
	
	public static Boolean clickifexists() {
		return iosmethods.clickifexists();	
	}
	
	public static boolean clickusingdynamicxpath(String runtimevlau) {
		return iosmethods.clickusingdynamicxpath(runtimevlau);
	}
	
	public static boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		return iosmethods.clickusingdynamicxpath(xpath, runtimevlau);
	}
	
	public static boolean duplicate_element_click(String ParamString) {
		return iosmethods.duplicate_element_click(ParamString);
	}
	
	public static boolean cleartext(String ParamString) {
		return iosmethods.cleartext(ParamString);
	}
	
	public static boolean entertext(String ParamString) {
		return iosmethods.entertext(ParamString);
	}
	
	public static boolean exists() {
		return iosmethods.exists();
	}
	
	public static boolean longpress() {
		return iosmethods.longpress();	
	}

	public static Boolean readandstore(String runtimePramaname) {
		return iosmethods.readandstore(runtimePramaname);
	}
	
	public static boolean Longpress() {
		return iosmethods.Longpress();
	}
	
	public static boolean exists(String identifierName, String value) {
		return iosmethods.exists(identifierName, value);
	}
	
	public static boolean notexists() {
		return iosmethods.notexists();
	}
	
	public static boolean validatetext(String ParamString) {
		return iosmethods.validatetext(ParamString);
	}
	
	public  static boolean validatepartialtext(String ParamString) {
		return iosmethods.validatepartialtext(ParamString);
	}
	
	public static boolean validateattr(String str1, String str2) {
		return iosmethods.validateattr(str1, str2);
	}
	
	public static boolean emulatorevent(String value) {
		return iosmethods.emulatorevent(value);
	}
	
	public static boolean mbscreenrotation(String ParamString) {
		return iosmethods.mbscreenrotation(ParamString);
	}
	
	public static Boolean closeApp() {
		return iosmethods.closeApp();
	}
	
	public static boolean launch_app(String ParamString) {
		return iosmethods.launch_app(ParamString);
	}
	
	public static boolean setOrientation(String orientation) {
		return iosmethods.setOrientation(orientation);
	}
	
	public static String getOrientation() {
		return iosmethods.getOrientation();
	}
	
	public static boolean Home() {
		return iosmethods.Home();
	}
	
	public static boolean Tap(int x, int y) {
		return iosmethods.Tap(x, y);
	}
	
	public static String Screenshot() {
		return iosmethods.Screenshot();
	}
	
	public static boolean FindElement(String name, String value) {
		return FindElement(name, value);
	}
	
	public static boolean FindElements(String name, String value) throws Exception {
		return FindElements(name, value);
	}
	
	public static JSONArray FnElements(String name, String value) {
		return iosmethods.FnElements(name, value);
	}
	
	public static boolean validateText(String value) {
		return iosmethods.validateText(value);
	}
	
	public static boolean validatepatialText(String value) {
		return iosmethods.validatepartialtext(value);
	}
	
	public static Boolean swipeevent() {
		return iosmethods.swipeevent();
	}
	
	public static boolean swipe(int startX, int startY, int endX, int endY, int timeout) {
		return swipe(startX, startY, endX, endY, timeout);
	}
	
	public static JSONObject size() {
		return iosmethods.size();
	}
	
	public static boolean Tap() {
		return iosmethods.Tap();
	}
	
	public static boolean EnterText(String value) {
		return iosmethods.EnterText(value);
	}
	
	public static JSONArray getPackages(String deviceId) {
		return iosmethods.getPackages(deviceId);
	}
	
	public static void setDevice(String deviceID) {
		iosmethods.setDevice(deviceID);
	}
	
	public static boolean cleartext() {
		return iosmethods.cleartext();
	}
	
	public static boolean startSession(String deivceId) {
		return iosmethods.startSession(deivceId);
	}
	
	public static int width() {
	return width();
	}
	
	public static int height() {
		return iosmethods.height();
	}
	
	public static Boolean uniqueElement(com.simplifyqa.DTO.Object obj) {
		return iosmethods.uniqueElement(obj);
		
	}
	
	public static Boolean parallelsearch(com.simplifyqa.DTO.Object obj, ArrayList<Future<Boolean>> taskList, String objecttype)
			throws InterruptedException {
		return iosmethods.parallelsearch(obj, taskList, objecttype);
	}
	
	 public static Boolean killtask(ArrayList<Future<Boolean>> taskList) throws
	 InterruptedException, ExecutionException, TimeoutException {
		 return iosmethods.killtask(taskList);
	 }
	
	
	public static Boolean scrolldown() {
		return iosmethods.scrolldown();
	}
	
	public static Boolean scrolldownrandom() {
		return iosmethods.scrolldownrandom();
	}
	
	public static Boolean scrolldown(int xCoornidates) {
		return iosmethods.scrolldown(xCoornidates);
	}
	
	public static Boolean scrollup() {
		return iosmethods.scrollup();
	}
	
	public static Boolean scrollup(int xCoordinates) {
		return iosmethods.scrollup();
	}
	
	public static Boolean scrolluprandom() {
		return iosmethods.scrolluprandom();
	}
	
	public static boolean scrollTillElementVertical(String identifierName, String value, boolean direction, int xCoordinates) {
		return iosmethods.scrollTillElementVertical(identifierName, value, direction, xCoordinates);
	}
	
	public static Boolean scrolltillelementverticalup() {
		return iosmethods.scrolltillelementverticalup();
	}
	
	public static Boolean scrolltillelementverticaldown() {
		return iosmethods.scrolltillelementverticaldown();
	}
	
	public static Boolean scrollLeft() {
		return iosmethods.scrollLeft();
	}
	
	public static Boolean scrollright() {
		return iosmethods.scrollright();
	}
	
	public static Boolean scrollleft() {
		return iosmethods.scrollleft();
	}
	
	public static Boolean scrollLeft(int Ycoordinate) {
		return iosmethods.scrollLeft();
	}
	
	
	public static Boolean scrollRight(int Ycoordinate) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return iosmethods.scrollRight(Ycoordinate);	
	}
	
	public boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction,
			int Ycoordinate) {
		return iosmethods.scrollTillElementHorizontal(identifierName, value, direction);		
	}
	
	public static boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction) {
		return iosmethods.scrollTillElementHorizontal(identifierName, value, direction);
	}
	
	public static Boolean scrolltillelementhorizontalright() {
		return iosmethods.scrolltillelementhorizontalright();
	}
	
	public static Boolean scrolltillelementhorizontalleft() {
		return iosmethods.scrolltillelementhorizontalleft();
	}
	
	public static JSONObject getbounds() {
		return iosmethods.getbounds();
	}
	
	public static Boolean tapbycoordinate() throws NumberFormatException, JSONException, Exception {
		return iosmethods.tapbycoordinate();
	}
	
	public static Boolean scrolltillelementfound(String value) {
		return iosmethods.scrolltillelementfound(value);	
	}
	
	
	public static Boolean click_using_ocr() {
		return iosmethods.click_using_ocr();
	}
	
	public static Boolean entertext_using_ocr(String value) {
		return iosmethods.entertext_using_ocr(value);
	}
	
	public static boolean dataincrease(String ele1, String ele2) {
		return iosmethods.dataincrease(ele1, ele2);
	}
	
	public static boolean datadecrease(String ele1, String ele2) {
		return iosmethods.datadecrease(ele1, ele2);
	}
	
	public static boolean datesequence(String xpath, String no, String month) {
		return iosmethods.datesequence(xpath, no, month);
	}
	
	public static boolean elementpresent() {
		return iosmethods.elementpresent();
	}
	
	public static boolean elementnotpresent() {
		return iosmethods.elementnotpresent();
	}
	
	public static boolean elementvisible() {
		return iosmethods.elementvisible();
	}
	
	public static boolean elementnotvisible() {
		return iosmethods.elementnotvisible();
	}
	
	public static boolean elementenabled() {
		return iosmethods.elementenabled();
	}
	
	public static boolean elementdisabled() {
		return iosmethods.elementdisabled();
	}
	
	public static boolean elementmatchesvalue(String expectedValue) {
		return iosmethods.elementmatchesvalue(expectedValue);
	}
	
	public static boolean elementnotmatchesvalue(String expectedValue) {
		return iosmethods.elementnotmatchesvalue(expectedValue);
	}
	
	public static boolean elementlessthanorequalstovalue(String expectedValue) {
		return iosmethods.elementlessthanorequalstovalue(expectedValue);
	}
	
	public static boolean elementlessthanvalue(String expectedValue) {
		return iosmethods.elementlessthanvalue(expectedValue);
	}
	
	public static boolean elementgreaterthanorequalstovalue(String expectedValue) {
		return iosmethods.elementgreaterthanorequalstovalue(expectedValue);	
	}
	
	public static boolean elementgreaterthanvalue(String expectedValue) {
		return iosmethods.elementgreaterthanvalue(expectedValue);
	}
	
	public static boolean elementnotequalstovalue(String expectedValue) {
		return iosmethods.elementnotequalstovalue(expectedValue);
	}
	
	public static boolean elementequalstovalue(String expectedValue) {
		return iosmethods.elementequalstovalue(expectedValue);
	}
	
	public static boolean elementnotcontainsvalue(String expectedValue) {
		return iosmethods.elementnotcontainsvalue(expectedValue);
	}
	
	public static boolean elementcontainsvalue(String expectedValue) {
		return iosmethods.elementcontainsvalue(expectedValue);
	}

	public static String getElementText() {
		return iosmethods.getElementText();
	}
	
	
	public Boolean installipa(String filepath) {
		return iosmethods.installipa(filepath);
	}
		
	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header) {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			return false;
		}
	}	
	
	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//General Methods
	public static boolean randomalphanumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphanumeric(count, prefix, suffix, parameter);
	}
	
	public static Boolean getfromruntime(String identifiername) {
		return generalMethod.getfromruntime(identifiername);
	}
	
	public static boolean storeruntime(String paraname, String paravalue) {
		return generalMethod.storeruntime(paraname, paravalue);
	}
	
	public static Boolean hold(String timeOut) {
		return generalMethod.hold(timeOut);
	}
	
	public static boolean minwait() {
		return generalMethod.minwait();
	}
	
	public static boolean medwait() {
		return generalMethod.medwait();
	}
	
	public static boolean maxwait() {
		return generalMethod.minwait();
	}
	
	public static boolean randomnumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomnumeric(count, prefix, suffix, parameter);
	}
	
	public static boolean combineruntime(String... param1) {
		return generalMethod.combineruntime(param1);
	}
	
	public static String readjson(String param1, String key, String index) {
		return generalMethod.readjson(param1, key, index);
	}
	
	public static boolean connecttodb(String param, String url, String username, String password) {
		return generalMethod.connecttodb(param, url, username, password);
	}
	
	public static Boolean closeConnection() {
		return generalMethod.closeConnection();
	}
	
	public static Boolean executequery(String req, String suffix) {
		return generalMethod.executequery(req, suffix);
	}
	
	public static Boolean executequery(String req) {
		return generalMethod.executequery(req);
	}
	
	public static boolean storedbtoruntime(String req, String suffix) {
		return generalMethod.storedbtoruntime(req, suffix);
	}
	
	public static Map<String, String> flattenJSON(String json) {
		return generalMethod.flattenJSON(json);
	}
	
	public static Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore) {
		return generalMethod.storejsonkey(JsonRuntimeParam, key, runtimeToStore);
	}

	public static boolean randomalphabets(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphabets(count, prefix, suffix, parameter);
	}
	
	public static String getRandomNumber() {
		return generalMethod.getRandomNumber();
	}
	
	public static boolean generaterandomstring(String runtimeParam, String prefix, String strLenght) {
		return generalMethod.generaterandomstring(runtimeParam, prefix, strLenght);
	}
	
	public static boolean validateparameter(String paramName, String value) {
		return generalMethod.validateparameter(paramName, value);
	}
	
	public static boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore) {
		return generalMethod.replaceinjsonandstore(baseString, replaceWith, replacevalue, runtimeStore);
	}
	
	public static boolean replaceandstore(String param1, String param2, String param3, String param4) {
		return generalMethod.replaceandstore(param1, param2, param3, param4);
	}
	
	public static boolean splitandgetvalue(String param, String delimt, String index, String param1) {
		return generalMethod.splitandgetvalue(param, delimt, index, param1);
	}
	
	public static boolean comparejsondoc(String path1, String path2) {
		return generalMethod.comparejsondoc(path1, path2);
	}
	
	public static boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam) {
		return generalMethod.replacestring(srcStr, replWith, replTo, runTimeParam);
	}
	
	public static boolean storeintojson(String Json, String key, String runtime) {
		return generalMethod.storeintojson(Json, key, runtime);
	}
	
	public static boolean storefromjson(String Json, String key, String runtime) {
		return generalMethod.storefromjson(Json, key, runtime);
	}
	
	public static boolean validatefromjson(String Json, String key, String ExpectedValue) {
		return generalMethod.validatefromjson(Json, key, ExpectedValue);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);

	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	
	public static boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapiresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String formdatatype,
			String formdata, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, formdatatype, formdata, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean generateexcelid(String filepath, String columnname) {
		return generalMethod.generateexcelid(filepath, columnname);
	}
	
	public static boolean generateexcelstring(String filepath, String columnname) {
		return generalMethod.generateexcelstring(filepath, columnname);
	}
	
	public static boolean validatefromsoap(String Json, String ExpectedValue) {
		return generalMethod.validatefromsoap(Json, ExpectedValue);
	}
	
	public static boolean existsusingsikuli(String imgpath) {
		return generalMethod.existsusingsikuli(imgpath);
	}
	
	public static boolean clickusingsikuli(String param) {
		return generalMethod.clickusingsikuli(param);
	}
	
	public static boolean entertextusingsikuli(String imgpath, String param) {
		return generalMethod.entertextusingsikuli(imgpath, param);
	}
	
	public static boolean storetdasmap(Map<String, String> tdMap) {
		return generalMethod.storetdasmap(tdMap);
	}
	
	public static JSONObject getruntimevariableJSon(String identifiername) {
		return generalMethod.getruntimevariableJSon(identifiername);
	}
	
	public static boolean deleteruntime(String param) {
		return generalMethod.deleteruntime(param);
	}
	
	public static String readFile(String FilePath) {
		return generalMethod.readFile(FilePath);
	}
	
	public static boolean readfile(String FilePath, String runtimeparam) {
		return generalMethod.readfile(FilePath, runtimeparam);
	}
	
	public static boolean writetofile(String filepath, String data) {
		return generalMethod.writetofile(filepath, data);
	}
	
	public static Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval) {
		return generalMethod.readfromexcel(excelfilepath, sheetname, rowname, rowval);
	}
	
	public static boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam) {
		return generalMethod.updatejsonusingexcel(excelfilepath, jsonfilepath, sheetname, rowname, rowval, runtimparam);
	}
	
	public static String getruntimevalueifexist(String identifiername) {
		return generalMethod.getruntimevalueifexist(identifiername);
	}
	
	public static boolean verifyadditionofvalues(String... str) {
		return generalMethod.verifyadditionofvalues(str);
	}
	
	public static boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam) {
		return generalMethod.generatecustomdateandtime(UDate, DateFormat, Time, Meri, runtimeparam);
	}
	
	public static boolean getisodateandtime(String UDate, String Time, String runtimeparam) {
		return generalMethod.getisodateandtime(UDate, Time, runtimeparam);
	}
	
	public static boolean getdate(String UDate, String DateFormat, String runtimeparam) {
		return generalMethod.getdate(UDate, DateFormat, runtimeparam);
	}
	
	public static boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
		return generalMethod.gettime(Time, TimeFormat, Meridiem, runtimeparam);
	}
	
	public static boolean checkiffileexists(String filePathString) throws Exception {
		return generalMethod.checkiffileexists(filePathString);
	}
	
	public static String convertxmltojson(String xmlfile) {
		return generalMethod.convertxmltojson(xmlfile);
	}
	
	public static boolean validatefromxml(String xmlfile, String key, String expectedvalue) {
		return generalMethod.validatefromxml(xmlfile, key, expectedvalue);
	}
	
	public static boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
		return generalMethod.storexmlvalue(xmlfile, key, runtimeparam);
	}
	
	public static boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam) {
		return generalMethod.excelgetcelldata(excelfilepath, sheetname, cellreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,

			String runtimeparam) {
		return generalMethod.excelgetcoldatausingcolref(excelfilepath, sheetname, colreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam) {
		return generalMethod.excelgetcoldatausinghdr(excelfilepath, sheetname, hdrname, runtimparam);
	}
	
	public static boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdatausinghdr(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdata(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusingcolref(excelfilepath, sheetname, colreference, replacevalue);
	}
	
	public static boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusinghdr(excelfilepath, sheetname, hdrname, replacevalue);
	}
	
	public static boolean excelupdatecellvalwrtrowval(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowval(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue)
			throws IOException {
		return generalMethod.csvupdatecelldata(fileToUpdate, row, col, replacevalue);
	}
	
	public static boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException {
		return generalMethod.csvgetcelldata(fileToUpdate, row, col, runtimeparam);
	}
	
	public static boolean additionofvalues(String... param1) {
		return generalMethod.additionofvalues(param1);
	}
	
	public static boolean subtractionofvalues(String... param1) {
		return generalMethod.subtractionofvalues(param1);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate, Ignorechar);
	}
	
	public static boolean concatandstore(String... param1) {
		return generalMethod.concatandstore(param1);
	}
	
	public static boolean excelupdatecellvalwrtrowvalakpk(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowvalakpk(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean simulatekeyboardwk() throws InterruptedException, AWTException {
		return generalMethod.simulatekeyboardwk();
	}
	public static boolean getkeyjson(String Json, String key, String runtime) {
		return generalMethod.getkeyjson(Json, key, runtime);
	}
	
	public static boolean getprocessutilization(String processToFind) {
		return generalMethod.getprocessutilization(processToFind);
	}
	
	public static boolean multiplejsonkey(String response,String key1,String key2,String key3,String identifiername,String key4) {
		return generalMethod.multiplejsonkey(response, key1, key2, key3, identifiername, key4);
	}
	
	public static boolean verifyapiresponse(String... param1) {
		return generalMethod.verifyapiresponse(param1);
	}
		
}
