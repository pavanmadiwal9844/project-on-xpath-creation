package com.simplifyqa.sqadrivers;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Map;

import org.json.JSONObject;
import org.python.antlr.PythonParser.return_stmt_return;

import com.codoid.products.fillo.Recordset;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.WebMethod;

public class generaldriver {
	public static GeneralMethod generalMethod = new GeneralMethod();
	
	
	public static boolean randomalphanumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphanumeric(count, prefix, suffix, parameter);
	}
	
	public static Boolean getfromruntime(String identifiername) {
		return generalMethod.getfromruntime(identifiername);
	}
	
	public static boolean storeruntime(String paraname, String paravalue) {
		return generalMethod.storeruntime(paraname, paravalue);
	}
	
	public static Boolean hold(String timeOut) {
		return generalMethod.hold(timeOut);
	}
	
	public static boolean minwait() {
		return generalMethod.minwait();
	}
	
	public static boolean medwait() {
		return generalMethod.medwait();
	}
	
	public static boolean maxwait() {
		return generalMethod.minwait();
	}
	
	public static boolean randomnumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomnumeric(count, prefix, suffix, parameter);
	}
	
	public static boolean combineruntime(String... param1) {
		return generalMethod.combineruntime(param1);
	}
	
	public static String readjson(String param1, String key, String index) {
		return generalMethod.readjson(param1, key, index);
	}
	
	public static boolean connecttodb(String param, String url, String username, String password) {
		return generalMethod.connecttodb(param, url, username, password);
	}
	
	public static Boolean closeConnection() {
		return generalMethod.closeConnection();
	}
	
	public static Boolean executequery(String req, String suffix) {
		return generalMethod.executequery(req, suffix);
	}
	
	public static Boolean executequery(String req) {
		return generalMethod.executequery(req);
	}
	
	public static boolean storedbtoruntime(String req, String suffix) {
		return generalMethod.storedbtoruntime(req, suffix);
	}
	
	public static Map<String, String> flattenJSON(String json) {
		return generalMethod.flattenJSON(json);
	}
	
	public static Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore) {
		return generalMethod.storejsonkey(JsonRuntimeParam, key, runtimeToStore);
	}

	public static boolean randomalphabets(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphabets(count, prefix, suffix, parameter);
	}
	
	public static String getRandomNumber() {
		return generalMethod.getRandomNumber();
	}
	
	public static boolean generaterandomstring(String runtimeParam, String prefix, String strLenght) {
		return generalMethod.generaterandomstring(runtimeParam, prefix, strLenght);
	}
	
	public static boolean validateparameter(String paramName, String value) {
		return generalMethod.validateparameter(paramName, value);
	}
	
	public static boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore) {
		return generalMethod.replaceinjsonandstore(baseString, replaceWith, replacevalue, runtimeStore);
	}
	
	public static boolean replaceandstore(String param1, String param2, String param3, String param4) {
		return generalMethod.replaceandstore(param1, param2, param3, param4);
	}
	
	public static boolean splitandgetvalue(String param, String delimt, String index, String param1) {
		return generalMethod.splitandgetvalue(param, delimt, index, param1);
	}
	
	public static boolean comparejsondoc(String path1, String path2) {
		return generalMethod.comparejsondoc(path1, path2);
	}
	
	public static boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam) {
		return generalMethod.replacestring(srcStr, replWith, replTo, runTimeParam);
	}
	
	public static boolean storeintojson(String Json, String key, String runtime) {
		return generalMethod.storeintojson(Json, key, runtime);
	}
	
	public static boolean storefromjson(String Json, String key, String runtime) {
		return generalMethod.storefromjson(Json, key, runtime);
	}
	
	public static boolean validatefromjson(String Json, String key, String ExpectedValue) {
		return generalMethod.validatefromjson(Json, key, ExpectedValue);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);

	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	
	public static boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapiresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String formdatatype,
			String formdata, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, formdatatype, formdata, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean generateexcelid(String filepath, String columnname) {
		return generalMethod.generateexcelid(filepath, columnname);
	}
	
	public static boolean generateexcelstring(String filepath, String columnname) {
		return generalMethod.generateexcelstring(filepath, columnname);
	}
	
	public static boolean validatefromsoap(String Json, String ExpectedValue) {
		return generalMethod.validatefromsoap(Json, ExpectedValue);
	}
	
	public static boolean existsusingsikuli(String imgpath) {
		return generalMethod.existsusingsikuli(imgpath);
	}
	
	public static boolean clickusingsikuli(String param) {
		return generalMethod.clickusingsikuli(param);
	}
	
	public static boolean entertextusingsikuli(String imgpath, String param) {
		return generalMethod.entertextusingsikuli(imgpath, param);
	}
	
	public static boolean storetdasmap(Map<String, String> tdMap) {
		return generalMethod.storetdasmap(tdMap);
	}
	
	public static JSONObject getruntimevariableJSon(String identifiername) {
		return generalMethod.getruntimevariableJSon(identifiername);
	}
	
	public static boolean deleteruntime(String param) {
		return generalMethod.deleteruntime(param);
	}
	
	public static String readFile(String FilePath) {
		return generalMethod.readFile(FilePath);
	}
	
	public static boolean readfile(String FilePath, String runtimeparam) {
		return generalMethod.readfile(FilePath, runtimeparam);
	}
	
	public static boolean writetofile(String filepath, String data) {
		return generalMethod.writetofile(filepath, data);
	}
	
	public static Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval) {
		return generalMethod.readfromexcel(excelfilepath, sheetname, rowname, rowval);
	}
	
	public static boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam) {
		return generalMethod.updatejsonusingexcel(excelfilepath, jsonfilepath, sheetname, rowname, rowval, runtimparam);
	}
	
	public static String getruntimevalueifexist(String identifiername) {
		return generalMethod.getruntimevalueifexist(identifiername);
	}
	
	public static boolean verifyadditionofvalues(String... str) {
		return generalMethod.verifyadditionofvalues(str);
	}
	
	public static boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam) {
		return generalMethod.generatecustomdateandtime(UDate, DateFormat, Time, Meri, runtimeparam);
	}
	
	public static boolean getisodateandtime(String UDate, String Time, String runtimeparam) {
		return generalMethod.getisodateandtime(UDate, Time, runtimeparam);
	}
	
	public static boolean getdate(String UDate, String DateFormat, String runtimeparam) {
		return generalMethod.getdate(UDate, DateFormat, runtimeparam);
	}
	
	public static boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
		return generalMethod.gettime(Time, TimeFormat, Meridiem, runtimeparam);
	}
	
	public static boolean checkiffileexists(String filePathString) throws Exception {
		return generalMethod.checkiffileexists(filePathString);
	}
	
	public static String convertxmltojson(String xmlfile) {
		return generalMethod.convertxmltojson(xmlfile);
	}
	
	public static boolean validatefromxml(String xmlfile, String key, String expectedvalue) {
		return generalMethod.validatefromxml(xmlfile, key, expectedvalue);
	}
	
	public static boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
		return generalMethod.storexmlvalue(xmlfile, key, runtimeparam);
	}
	
	public static boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam) {
		return generalMethod.excelgetcelldata(excelfilepath, sheetname, cellreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,

			String runtimeparam) {
		return generalMethod.excelgetcoldatausingcolref(excelfilepath, sheetname, colreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam) {
		return generalMethod.excelgetcoldatausinghdr(excelfilepath, sheetname, hdrname, runtimparam);
	}
	
	public static boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdatausinghdr(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdata(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusingcolref(excelfilepath, sheetname, colreference, replacevalue);
	}
	
	public static boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusinghdr(excelfilepath, sheetname, hdrname, replacevalue);
	}
	
	public static boolean excelupdatecellvalwrtrowval(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowval(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue)
			throws IOException {
		return generalMethod.csvupdatecelldata(fileToUpdate, row, col, replacevalue);
	}
	
	public static boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException {
		return generalMethod.csvgetcelldata(fileToUpdate, row, col, runtimeparam);
	}
	
	public static boolean additionofvalues(String... param1) {
		return generalMethod.additionofvalues(param1);
	}
	
	public static boolean subtractionofvalues(String... param1) {
		return generalMethod.subtractionofvalues(param1);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate, Ignorechar);
	}
	
	public static boolean concatandstore(String... param1) {
		return generalMethod.concatandstore(param1);
	}
	
	public static boolean excelupdatecellvalwrtrowvalakpk(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowvalakpk(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean simulatekeyboardwk() throws InterruptedException, AWTException {
		return generalMethod.simulatekeyboardwk();
	}
	public static boolean getkeyjson(String Json, String key, String runtime) {
		return generalMethod.getkeyjson(Json, key, runtime);
	}
	
	public static boolean getprocessutilization(String processToFind) {
		return generalMethod.getprocessutilization(processToFind);
	}
	
	public static boolean multiplejsonkey(String response,String key1,String key2,String key3,String identifiername,String key4) {
		return generalMethod.multiplejsonkey(response, key1, key2, key3, identifiername, key4);
	}
	
	public static boolean verifyapiresponse(String... param1) {
		return generalMethod.verifyapiresponse(param1);
	}
		
		
	
	
	}
