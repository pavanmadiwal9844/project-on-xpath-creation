package com.simplifyqa.sqadrivers;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

// import org.hamcrest.core.CombinableMatcher;
import org.json.JSONArray;
import org.json.JSONObject;
// import org.springframework.instrument.classloading.weblogic.WebLogicLoadTimeWeaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codoid.products.fillo.Recordset;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Function;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.driver.WebDriver;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.method.WebMethod;
import com.simplifyqa.method.Interface.Generalmethod;
import com.simplifyqa.method.Interface.Webmethod;

public class webdriver{

	public static WebMethod webmethods = new WebMethod();
	public static GeneralMethod generalMethod = new GeneralMethod();

	private static webdriver wbdiver = null;
	public WebDriver driver;
	public static Class myClass = null;
	private String classname = "com.android.WebCustom";
	public static Editorclassloader customMethod = null;
	public String brname = null;
	public Setupexec curExecution = null;
	public HashMap<String, String> header = new HashMap<String, String>();
	public static Integer currentSeq = -1;
	public static final Logger logger = LoggerFactory.getLogger(webdriver.class);
	public static final org.apache.log4j.Logger userlogs = org.apache.log4j.LogManager
			.getLogger(webdriver.class);
	
	
	public static WebDriver getwebDriver() {
		return webmethods.getDriver();
	}
	
	public static Object getCurrentObject() {
		webmethods.getDriver();
		return webmethods.curObject;
	}
	
	public static String getbrowser() {
		webmethods.getDriver();
		return webmethods.brname;
		
	}
	
	public static boolean getattrandstore(String a, String b) {
		return webmethods.getattrandstore(a, b);
	}

	public static boolean WebCapabilities(String browserName, String platformName) {
		webmethods.getDriver();
		return webmethods.webCapabilities(browserName, platformName);
	}

	public static boolean DeleteSession() {
		return webmethods.deleteSession();
	}

	public static boolean closeDriver() {
		return webmethods.closeDriver();
	}

	public static JSONObject getTimeout() {
		return webmethods.getTimeout();
	}

	public static boolean setTimeout(long implicittimeout, long pageloadtimeout, long scripttimeout) {
		return webmethods.setTimeout(implicittimeout, pageloadtimeout, scripttimeout);
	}

	public static Boolean launchUrl(String url) {
		return webmethods.launchUrl(url);
	}

	public static String geturl() {
		return webmethods.geturl();
	}

	public static boolean back() {
		return webmethods.back();
	}

	public static boolean forward() {
		return webmethods.forward();
	}

	public static boolean refresh() {
		return webmethods.refresh();
	}

	public static String getTitle() {
		return webmethods.getTitle();
	}

	public static String getWindowhandle() {
		return webmethods.getWindowhandle();
	}

	public static boolean closeWindow() {
		return webmethods.closeWindow();
	}

	public static boolean switchtoWindow(String handle) {
		return webmethods.switchtoWindow(handle);
	}

	public static JSONArray getWindowhandles() {
		return webmethods.getWindowhandles();
	}

// @Override
	public static boolean switchtoframe(String id) {
		return webmethods.switchtoframe(id);
	}

// @Override
	public static boolean switchtoparentframe() {
		return webmethods.switchtoparentframe();
	}

	public static JSONObject getWindowrect() {
		return webmethods.getWindowrect();
	}

	public static boolean setWindowrect(int height, int width, int x, int y) {
		return webmethods.setWindowrect(height, width, x, y);
	}

	public static boolean maximizeScreen() {
		return webmethods.maximizeScreen();
	}

	public static boolean minimizeScreen() {
		return webmethods.minimizeScreen();
	}

	public static boolean fullScreen() {
		return webmethods.fullScreen();
	}

	public static JSONObject getActiveelement() {
		return webmethods.getActiveelement();
	}

	public static String findElement(String using, String value) {
		return webmethods.findElement(using, value);
	}

	public static JSONArray findElements(String using, String value) {
		return webmethods.findElements(using, value);
	}

	public static String findelementfromElement(String using, String value, String elementId) {
		return webmethods.findelementfromElement(using, value, elementId);
	}

	public static JSONObject findelementsfromElement(String using, String value, String elementId) {
		return webmethods.findelementsfromElement(using, value, elementId);
	}

	public static boolean isElementselected(String elementid) {
		return webmethods.isElementselected(elementid);
	}

	public static String getElementattribute(String elementid, String attribute) {
		return webmethods.getElementattribute(elementid, attribute);
	}

	public static String getElementproperty(String elementid, String property) {
		return webmethods.getElementproperty(elementid, property);
	}

	public static String getElementcssvalue(String elementid, String css) {
		return webmethods.getElementcssvalue(elementid, css);
	}

	public static String getElementtext(String elementid) {
		return webmethods.getElementtext(elementid);
	}

	public static String getElementtagname(String elementid) {
		return webmethods.getElementtagname(elementid);
	}

	public static JSONObject getElementrect(String elementid) {
		return webmethods.getElementrect(elementid);
	}

	public static boolean isElementenabled(String elementid) {
		return webmethods.isElementenabled(elementid);
	}

	public static String elementClick(String elementId) {
		return webmethods.elementClick(elementId);
	}

	public static boolean elementClear(String elementId) {
		return webmethods.elementClear(elementId);
	}

	public static boolean elementSendkeys(String elementId, String valuetoEnter) {
		return webmethods.elementSendkeys(elementId, valuetoEnter);
	}

	public static String getPagesource() {
		return webmethods.getPagesource();
	}

	public static boolean executeScript(String script) {
		return webmethods.executeScript(script);
	}

	public static JSONObject executeScript2(String script) {
		return webmethods.executeScript2(script);
	}

	public static boolean executeAsyncscript(String script) {
		return webmethods.executeAsyncscript(script);
	}

	public static JSONArray getallCookies() {
		return webmethods.getallCookies();
	}

	public static JSONObject getnamedCookies(String cookieName) {
		return webmethods.getnamedCookies(cookieName);
	}

	public static boolean addCookie(String name, String value, String path, String domain, boolean secure,
			boolean httpOnly, int expiry) {
		return webmethods.addCookie(name, value, path, domain, secure, httpOnly, expiry);
	}

	public static boolean deleteCookie(String cookieName) {
		return webmethods.deleteCookie(cookieName);
	}

	public static boolean deleteAllcookies() {
		return webmethods.deleteAllcookies();
	}

	public static boolean performAction() {
		return webmethods.performAction();
	}

	public static boolean releaseAction() {
		return webmethods.releaseAction();
	}

	public static boolean dismissAlert() {
		return webmethods.dismissAlert();
	}

	public static boolean acceptAlert() {
		return webmethods.acceptAlert();
	}

	public static String getAlerttext() {
		return webmethods.getAlerttext();
	}

	public static boolean sendAlerttext(String valuetoEnter) {
		return webmethods.sendAlerttext(valuetoEnter);
	}

	public static String takeScreenshot() {
		return webmethods.takeScreenshot();
	}

	public static String takeElementscreenshot(String elementId) {
		return webmethods.takeElementscreenshot(elementId);
	}

	public static boolean click() {
		return webmethods.click();

	}

	public static boolean validateandclick(String valuetovalidate, String clickxpathvalue, String maxtimeout) {
		return webmethods.validateandclick(valuetovalidate, clickxpathvalue, maxtimeout);

	}

	public static boolean validateandclick(String identifiername, String value, String valuetovalidate,
			String clickxpathvalue, String maxtimeout) {
		return webmethods.validateandclick(identifiername, value, valuetovalidate, clickxpathvalue, maxtimeout);
	}

	public static boolean click(String runtimeval) {
		return webmethods.click(runtimeval);
	}

	public static boolean entertext(String valuetoenter) {
		return webmethods.entertext(valuetoenter);
	}

	public static boolean contenttext(String valuetoEnter) {
		return webmethods.contenttext(valuetoEnter);
	}

	public static boolean keyboardaction(String valuetoenter) {
		return webmethods.keyboardaction(valuetoenter);
	}

	public static boolean cleartext() {
		return webmethods.cleartext();
	}

	public static boolean entertexttab(String valuetoenter) {
		return webmethods.entertexttab(valuetoenter);
	}

	public static boolean entertextandenter(String valuetoenter) {
		return webmethods.entertextandenter(valuetoenter);
	}

	public static boolean validatetext(String valuetovalidate) {
		return webmethods.validatetext(valuetovalidate);
	}

	public static boolean validatetext(String replacevalue, String valuetovalidate) {
		return webmethods.validatetext(replacevalue, valuetovalidate);
	}

	public static boolean validatevalue(String valuetovalidate) {
		return webmethods.validatevalue(valuetovalidate);
	}

	public static boolean exists() {
		return webmethods.exists();
	}

	public static boolean exists(String paramvalue) {
		return webmethods.exists(paramvalue);
	}

	public static boolean dynamicexists(String valuetovalidate) {
		return webmethods.dynamicexists(valuetovalidate);
	}

	public static boolean notexists() {
		return webmethods.notexists();
	}

	public static boolean clickifexist() {
		return webmethods.clickifexist();
	}

	public static String escape(String toEscape) {
		return webmethods.escape(toEscape);
	}

	public static boolean validateconfirmationtext(String valuetovalidate) {
		return webmethods.validateconfirmationtext(valuetovalidate);
	}

	public static boolean validatealerttext(String valuetovalidate) {
		return webmethods.validatealerttext(valuetovalidate);
	}

	public static boolean validateonprompt(String valuetovalidate) {
		return webmethods.validateonprompt(valuetovalidate);
	}

	public static boolean alertaccept() {
		return webmethods.alertaccept();
	}

	public static boolean chooseokonconfirmation() {
		return webmethods.chooseokonconfirmation();
	}

	public static boolean choosecancelonconfirmation() {
		return webmethods.choosecancelonconfirmation();
	}

	public static boolean choosecancelonprompt() {
		return webmethods.choosecancelonprompt();
	}

	public static boolean answeronprompt(String valuetoEnter) {
		return webmethods.answeronprompt(valuetoEnter);
	}

	public static boolean selectitemfromdropdown(String selectedvalue) {
		return webmethods.selectitemfromdropdown(selectedvalue);
	}

	public static boolean justOneMethod(String identifiername, String value) {
		return webmethods.justOneMethod(identifiername, value);
	}

	public static boolean selectByVisibleText(String text) {
		return webmethods.selectByVisibleText(text);
	}

	public static boolean scrollintoview() {
		return webmethods.scrollintoview();
	}

	public static boolean checkifnotchecked() {
		return webmethods.checkifnotchecked();
	}

	public static boolean uncheckifchecked() {
		return webmethods.uncheckifchecked();
	}

	public static boolean validatecheck() {
		return webmethods.validatecheck();
	}

	public static boolean enter() {
		return webmethods.enter();
	}

	public static boolean tab() {
		return webmethods.tab();
	}

	public static boolean switchtotab(String tabindex) {
		return webmethods.switchtotab(tabindex);
	}

	public static boolean switchtoparenttab() {
		return webmethods.switchtoparenttab();
	}

	public static Boolean checkTab(Object obj, JSONArray tabs) {
		return webmethods.checkTab(obj, tabs);
	}

	public static Boolean checkFrame(Object obj) {
		return webmethods.checkFrame(obj);
	}

	public static boolean draganddropobject(String targetxpathvalue) {
		return webmethods.draganddropobject(targetxpathvalue);
	}

	public static boolean readtextandstore(String Paraname) {
		return webmethods.readtextandstore(Paraname);
	}

	public static boolean getattrandstore(String identifiername, String value, String fieldName, String Paraname) {
		return webmethods.getattrandstore(identifiername, value, fieldName, Paraname);
	}

	public static boolean readvalueandstore(String Paraname) {
		return webmethods.readvalueandstore(Paraname);
	}

	public static boolean readtextandstore(String identifiername, String value, String Paraname) {
		return webmethods.readtextandstore(identifiername, value, Paraname);
	}

	public static boolean readvalueandstore(String identifiername, String value, String Paraname) {
		return webmethods.readvalueandstore(identifiername, value, Paraname);
	}

	public static boolean dragandDrop(String identifiername, String value, String targetxpathvalue) {
		return webmethods.dragandDrop(identifiername, value, targetxpathvalue);
	}

	public static JSONArray waituntil(String identifiername, String value) {
		return webmethods.waituntil(identifiername, value);
	}

	public static JSONObject uniqueElement(Object obj) {
		return webmethods.uniqueElement(obj);
	}

	public static JSONObject parallelsearch(Object obj, ArrayList<Future<JSONObject>> taskList, String objecttype)
			throws InterruptedException {
		return webmethods.parallelsearch(obj, taskList, objecttype);
	}

	public static JSONObject killtask(ArrayList<Future<JSONObject>> taskList)
			throws InterruptedException, ExecutionException, TimeoutException {
		return webmethods.killtask(taskList);
	}

	public static JSONObject uniqueIdvalue(Object obj, String[] idname, Integer i) {
		return webmethods.uniqueIdvalue(obj, idname, i);
	}
	
	public static JsonNode findattr(List<JsonNode> Attributes, String identifierName) {
		return webmethods.findattr(Attributes, identifierName);
	}
	
	public static JSONObject fnElements(String identifiername, String value, String objecttype) {
		return webmethods.fnElements(identifiername, value, objecttype);
	}
	
	public static int fnElements(String identifiername, String value) {
		return webmethods.fnElements(identifiername, value);
	}
		
	public static JSONObject fnElementsnotenabled(String identifiername, String value, String objecttype) {
		return webmethods.fnElementsnotenabled(identifiername, value, objecttype);
	}
	
	public static boolean enter(String identifiername, String value) {
		return webmethods.enter(identifiername, value);
	}
	
	public static boolean tab(String identifiername, String value) {
		return webmethods.tab(identifiername, value);
	}
	
	public static boolean uploadfile(String path) throws InterruptedException, AWTException {
		return webmethods.uploadfile(path);
	}
	
	public static boolean entertext(String identifiername, String value, String valuetoenter) {
		return webmethods.entertext(identifiername, value, valuetoenter);
	}
	
	public static boolean keyboardaction(String identifiername, String value, String valuetoenter) {
		return webmethods.keyboardaction(identifiername, value, valuetoenter);
	}

	public static boolean arrowup() {
		return webmethods.arrowup();
	}
		
	public static boolean arrowdown() {
		return webmethods.arrowdown();
	}
	
	public static boolean enterfilepath(String value) {
		return webmethods.enterfilepath(value);
	}
		
	public static boolean launchapplication(String url) {
		return webmethods.launchapplication(url);
	}
	
	public static boolean clickifexist(String identifiername, String value) {
		return webmethods.clickifexist(identifiername, value);
	}
	
	public static boolean mouseoveranelement() {
		return webmethods.mouseoveranelement();
	}
	
	public static boolean cleartext(String identifiername, String value) {
		return webmethods.cleartext(identifiername, value);
	}
	
	public static boolean checkifnotchecked(String identifiername, String value) {
		return webmethods.checkifnotchecked(identifiername, value);
	}
	
	public static boolean uncheckifchecked(String identifiername, String value) {
		return webmethods.uncheckifchecked(identifiername, value);
	}
	
	public static boolean waitforalert() {
		return webmethods.waitforalert();
	}
	
	public static boolean exists(String identifiername, String value) {
		return webmethods.exists(identifiername, value);
	}
	
	public static boolean notexists(String identifiername, String value) {
		return webmethods.notexists(identifiername, value);
	}
	
	public static boolean validatetext(String identifiername, String value, String valuetovalidate) {
		return webmethods.validatetext(identifiername, value,valuetovalidate);
	}
	
	public static boolean validatevalue(String identifiername, String value, String valuetovalidate) {
		return webmethods.validatevalue(identifiername, value, valuetovalidate);
	}
	
	public static boolean validatecheck(String identifiername, String value) {
		return webmethods.validatecheck(identifiername, value);
	}
	
	public static boolean scrolltoview() {
		return webmethods.scrolltoview();
	}
	
	public static boolean entertextandenter(String identifiername, String value, String valuetoenter) {
		return webmethods.entertextandenter(identifiername, value, valuetoenter);
	}
	
	public static boolean entertextandtab(String identifiername, String value, String valuetoenter) {
		return webmethods.entertextandtab(identifiername, value, valuetoenter);
	}
	
	public static boolean fileuploadusingrobot() {
		return webmethods.fileuploadusingrobot();
	}
	
	public static boolean settimeout(long implicit, long pageout, long script) {
		return webmethods.settimeout(implicit, pageout, script);
	}
	
	public static boolean clickusingjs() {
		return webmethods.clickusingjs();
	}
	
	public static boolean clickusingjss() {
		return webmethods.clickusingjss();
	}
	
	public static boolean clickusingjs(String runtimevalue) {
		return webmethods.clickusingjs(runtimevalue);
	}
	
	public static boolean validatepartialtext(String identifiername, String value, String valuetovalidate) {
		return webmethods.validatepartialtext(identifiername, value, valuetovalidate);
	}
	
	public static boolean validatepartialtext(String valuetovalidate) {
		return webmethods.validatepartialtext(valuetovalidate);
	}
	
	public static String getUrl() {
		return webmethods.getUrl();
	}
	
	public static boolean iselementDisplayed(String elementId) {
		return webmethods.iselementDisplayed(elementId);
	}
	
	public static String fullpageScreenshot() {
		return webmethods.fullpageScreenshot();
	}
	
	public static boolean entertextbyjs(String identifiername, String value, String valuetoenter) {
		return webmethods.entertextbyjs(identifiername, value, valuetoenter);
	}
	
	public static boolean entertextbyjs(String valuetoenter) {
		return webmethods.entertextbyjs(valuetoenter);
	}
	
	public static boolean clickusingjs(String identifiername, String value) {
		return webmethods.clickusingjs(identifiername, value);
	}
	
	public static boolean selectitemfromdropdownbyxpath(String selectedvalue) {
		return webmethods.selectitemfromdropdownbyxpath(selectedvalue);
	}
	
	public static boolean dbclick() {
		return webmethods.dbclick();
	}
	
	public static boolean mousemove() {
		return webmethods.mousemove();
	}
	
	public static boolean dbclick(String identifiername, String value) {
		return webmethods.dbclick(identifiername, value);
	}
	
	public static boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		return webmethods.clickusingdynamicxpath(xpath, runtimevlau);
	}
	
	public static boolean clickusingdynamicxpath(String runtimevlau) {
		return webmethods.clickusingdynamicxpath(runtimevlau);
	}
	
	public static boolean clickusingjswithdynamicxpath(String xpath, String runtimevlau) {
		return webmethods.clickusingjswithdynamicxpath(xpath, runtimevlau);
	}
	
	public static boolean clickusingjswithdynamicxpath(String runtimevlau) {
		return webmethods.clickusingjswithdynamicxpath(runtimevlau);
	}
	
	public static boolean readinstringandstore(String identifiername, String value, String Paraname, String startString,
			String endString) {
		return webmethods.readinstringandstore(identifiername,value,Paraname,startString,endString);
	}
	
	public static boolean readinstringandstore(String Paraname, String startString, String endString) {
		return webmethods.readinstringandstore(Paraname, startString, endString);
	}
	
	public static boolean doWaitPreparation() {
		return webmethods.doWaitPreparation();
	}
	
	public static boolean closed() {
		return webmethods.closed();
	}
	
	public static boolean closewindow() {
		return webmethods.closewindow();
	}
	
	public static boolean back(Object obj) {
		return webmethods.back(obj);
	}
	
	public static boolean forward(Object obj) {
		return webmethods.forward(obj);
	}
	
	public static boolean refresh(Object obj) {
		return webmethods.refresh(obj);
	}
	
	public static boolean waituntilelementexist() {
		return webmethods.waituntilelementexist();
	}
	
	public static boolean waituntilelementexist(String identifiername, String value) {
		return webmethods.waituntilelementexist(identifiername, value);
	}
	
	public static boolean closewindow(Object obj) {
		return webmethods.closewindow(obj);
	}
	
	public static boolean switchtowindow() {
		return webmethods.switchtowindow();
	}
	
	public static boolean switchtoframe() {
		return webmethods.switchtoframe();
	}
	
	public static boolean switchtowindow(String vlaue) {
		return webmethods.switchtowindow(vlaue);
	}
	
	public static boolean switchtoparent() {
		return webmethods.switchtoparent();
	}
	
	public static Boolean getfilename(String param) {
		return webmethods.getfilename(param);
	}
	
	public static Boolean jsexecutor(String js) {
		return webmethods.jsexecutor(js);
	}
	
	public static boolean closetab(String tab) {
		return webmethods.closetab(tab);
	}
	
	public static boolean opennewtabwithurl(String m_strURL) throws Exception {
		return webmethods.opennewtabwithurl(m_strURL);
	}
	
	public static boolean gettablerowcount(String identifiername, String value, String runtimeparam) {
		return webmethods.gettablerowcount(identifiername, value, runtimeparam);
	}
	
	public static boolean gettablerowcount(String runtimeparam) {
		return webmethods.gettablerowcount(runtimeparam);
	}
	
	public static boolean clickusingmouse() {
		return webmethods.clickusingmouse();
	}
	
	public static boolean clickusingmouse(String xpath) {
		return webmethods.clickusingmouse(xpath);
	}
	
	public static boolean selectitemfromcustomdropdown(String xpath2) {
		return webmethods.selectitemfromcustomdropdown(xpath2);
	}
	
	public static boolean selectitemfromcustomdropdownxpath(String xpath2, String dynamicxpath) {
		return webmethods.selectitemfromcustomdropdownxpath(xpath2, dynamicxpath);
	}
	
	public static boolean datepicker(String Date) {
		return webmethods.datepicker(Date);
	}
	
	public static boolean entertextusingrobo(String valuetoenter) throws InterruptedException, AWTException {
		return webmethods.entertextusingrobo(valuetoenter);
	}
	
	public static boolean validatetime(String valuetovalidate) {
		return webmethods.validatetime(valuetovalidate);
	}
	
	public static boolean validatetime(String identifiername, String value, String valuetovalidate) {
		return webmethods.validatetime(identifiername, value, valuetovalidate);
	}
	

	public static boolean elementpresent() {
		return webmethods.elementpresent();
	}
	
	public static boolean elementnotpresent() {
		return webmethods.elementnotpresent();
	}
	
	public static boolean elementvisible() {
		return webmethods.elementvisible();
	}
	
	public static boolean elementnotvisible() {
		return webmethods.elementnotvisible();
		
	}
	
	public static boolean elementcontainstext(String valuecheck) {
		return webmethods.elementcontainstext(valuecheck);
	}
	
	public static boolean elementcontainsvalue(String valuecheck) {
		return webmethods.elementcontainsvalue(valuecheck);
	}
	
	public static boolean elementnotcontainstext(String valuecheck) {
		return webmethods.elementnotcontainstext(valuecheck);
	}
	
	public static boolean elementnotcontainsvalue(String valuecheck) {
		return webmethods.elementnotcontainsvalue(valuecheck);
	}
	
	public static boolean elementisclickable() {
		return webmethods.elementisclickable();
	}
	
	public static boolean elementnotclickable() {
		return webmethods.elementnotclickable();
	}
	
	public static boolean elementmatchestext(String valuecheck) {
		return webmethods.elementmatchestext(valuecheck);
	}
	
	public static boolean elementmatchesvalue(String valuecheck) {
		return webmethods.elementmatchesvalue(valuecheck);
	}
	
	public static boolean elementnotmatchesvalue(String valuecheck) {
		return webmethods.elementnotmatchesvalue(valuecheck);
	}
	
	public static boolean elementnotmatchestext(String valuecheck) {
		return webmethods.elementnotmatchestext(valuecheck);
	}
	
	
	public static boolean elementisenabled() {
		return webmethods.elementisenabled();
		
	}
	
	public static boolean elementdisabled() {
		return webmethods.elementdisabled();
	}
	
	public static boolean elementchecked() {
		return webmethods.elementchecked();
	}
	
	public static boolean elementnotchecked() {
		return webmethods.elementnotchecked();
	}
	
	public static boolean elementequaltotext(String value1) {
		return webmethods.elementequaltotext(value1);
	}
	
	public static boolean elementnotequalstotext(String value1) {
		return webmethods.elementnotequalstotext(value1);
	}
	
	public static boolean elementnotequaltovalue(String value1) {
		return webmethods.elementnotequaltovalue(value1);
	}
	
	public static boolean elementlesserthantext(String value1) {
		return webmethods.	elementlesserthantext(value1);
	}
	
	public static boolean elementlesserthanvalue(String value1) {
		return webmethods.elementlesserthanvalue(value1);
	}
	
	public static boolean elementlesserthanequaltotext(String value1) {
		return webmethods.elementlesserthanequaltotext(value1);
	}
	
	public static boolean elementlesserthanequaltovalue(String value1) {
		return webmethods.elementlesserthanequaltovalue(value1);
	}
	
	public static boolean elementgreaterthantext(String value1) {
		return webmethods.elementgreaterthantext(value1);
	}
	
	public static boolean elementgreaterthanvalue(String value1) {
		return webmethods.elementgreaterthanvalue(value1);
	}
	
	public static boolean elementgreaterthanequaltotext(String value1) {
		return webmethods.elementgreaterthanequaltotext(value1);
	}
	
	public static boolean elementgreaterthanequaltovalue(String value1) {
		return webmethods.elementgreaterthanequaltovalue(value1);
	}
	
	public static String getElementId() {
		return webmethods.getElementId();
	}
	
	public static boolean daetpickerthetaedge(String Date) {
		return webmethods.daetpickerthetaedge(Date);
	}
	
	public static boolean datepickerakpk(String Date) {
		return webmethods.datepickerakpk(Date);
	}
	
	public static String getattribute(String identifiername, String value, String fieldName) {
		return webmethods.getattribute(identifiername, value, fieldName);
	}
	
	public static boolean getattributeandstore(String fieldName, String Paraname) {
		return webmethods.getattributeandstore(fieldName, Paraname);
	}
	
	public static boolean click(String identifiername, String value) {
		return webmethods.click(identifiername, value);
	}
	
	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
// this.customMethod=null;
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass(InitializeDependence.runtime_classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			//added to load all capabilities for custom methods
			webmethods.setDriver(null);
			webmethods.getDriver();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header,Class[] objectype) {
		try {
			if (setcustomClass(executiondetail, header)) {
				System.out.println("Inside webdrive Call method");
				if (value == (null))
					return customMethod.ExecustomMethodwithproperdatatype(identifierName, value, InitializeDependence.runtime_classname,objectype);
				else {
					return customMethod.ExecustomMethodwithproperdatatype(identifierName, value, InitializeDependence.runtime_classname,objectype);
				}
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//General methods
	
	public static boolean randomalphanumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphanumeric(count, prefix, suffix, parameter);
	}
	
	public static Boolean getfromruntime(String identifiername) {
		return generalMethod.getfromruntime(identifiername);
	}
	
	public static boolean storeruntime(String paraname, String paravalue) {
		return generalMethod.storeruntime(paraname, paravalue);
	}
	
	public static Boolean hold(String timeOut) {
		return generalMethod.hold(timeOut);
	}
	
	public static boolean minwait() {
		return generalMethod.minwait();
	}
	
	public static boolean medwait() {
		return generalMethod.medwait();
	}
	
	public static boolean maxwait() {
		return generalMethod.minwait();
	}
	
	public static boolean randomnumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomnumeric(count, prefix, suffix, parameter);
	}
	
	public static boolean combineruntime(String... param1) {
		return generalMethod.combineruntime(param1);
	}
	
	public static String readjson(String param1, String key, String index) {
		return generalMethod.readjson(param1, key, index);
	}
	
	public static boolean connecttodb(String param, String url, String username, String password) {
		return generalMethod.connecttodb(param, url, username, password);
	}
	
	public static Boolean closeConnection() {
		return generalMethod.closeConnection();
	}
	
	public static Boolean executequery(String req, String suffix) {
		return generalMethod.executequery(req, suffix);
	}
	
	public static Boolean executequery(String req) {
		return generalMethod.executequery(req);
	}
	
	public static boolean storedbtoruntime(String req, String suffix) {
		return generalMethod.storedbtoruntime(req, suffix);
	}
	
	public static Map<String, String> flattenJSON(String json) {
		return generalMethod.flattenJSON(json);
	}
	
	public static Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore) {
		return generalMethod.storejsonkey(JsonRuntimeParam, key, runtimeToStore);
	}

	public static boolean randomalphabets(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphabets(count, prefix, suffix, parameter);
	}
	
	public static String getRandomNumber() {
		return generalMethod.getRandomNumber();
	}
	
	public static boolean generaterandomstring(String runtimeParam, String prefix, String strLenght) {
		return generalMethod.generaterandomstring(runtimeParam, prefix, strLenght);
	}
	
	public static boolean validateparameter(String paramName, String value) {
		return generalMethod.validateparameter(paramName, value);
	}
	
	public static boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore) {
		return generalMethod.replaceinjsonandstore(baseString, replaceWith, replacevalue, runtimeStore);
	}
	
	public static boolean replaceandstore(String param1, String param2, String param3, String param4) {
		return generalMethod.replaceandstore(param1, param2, param3, param4);
	}
	
	public static boolean splitandgetvalue(String param, String delimt, String index, String param1) {
		return generalMethod.splitandgetvalue(param, delimt, index, param1);
	}
	
	public static boolean comparejsondoc(String path1, String path2) {
		return generalMethod.comparejsondoc(path1, path2);
	}
	
	public static boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam) {
		return generalMethod.replacestring(srcStr, replWith, replTo, runTimeParam);
	}
	
	public static boolean storeintojson(String Json, String key, String runtime) {
		return generalMethod.storeintojson(Json, key, runtime);
	}
	
	public static boolean storefromjson(String Json, String key, String runtime) {
		return generalMethod.storefromjson(Json, key, runtime);
	}
	
	public static boolean validatefromjson(String Json, String key, String ExpectedValue) {
		return generalMethod.validatefromjson(Json, key, ExpectedValue);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);

	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	
	public static boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapiresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String formdatatype,
			String formdata, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, formdatatype, formdata, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean generateexcelid(String filepath, String columnname) {
		return generalMethod.generateexcelid(filepath, columnname);
	}
	
	public static boolean generateexcelstring(String filepath, String columnname) {
		return generalMethod.generateexcelstring(filepath, columnname);
	}
	
	public static boolean validatefromsoap(String Json, String ExpectedValue) {
		return generalMethod.validatefromsoap(Json, ExpectedValue);
	}
	
	public static boolean existsusingsikuli(String imgpath) {
		return generalMethod.existsusingsikuli(imgpath);
	}
	
	public static boolean clickusingsikuli(String param) {
		return generalMethod.clickusingsikuli(param);
	}
	
	public static boolean entertextusingsikuli(String imgpath, String param) {
		return generalMethod.entertextusingsikuli(imgpath, param);
	}
	
	public static boolean storetdasmap(Map<String, String> tdMap) {
		return generalMethod.storetdasmap(tdMap);
	}
	
	public static JSONObject getruntimevariableJSon(String identifiername) {
		return generalMethod.getruntimevariableJSon(identifiername);
	}
	
	public static boolean deleteruntime(String param) {
		return generalMethod.deleteruntime(param);
	}
	
	public static String readFile(String FilePath) {
		return generalMethod.readFile(FilePath);
	}
	
	public static boolean readfile(String FilePath, String runtimeparam) {
		return generalMethod.readfile(FilePath, runtimeparam);
	}
	
	public static boolean writetofile(String filepath, String data) {
		return generalMethod.writetofile(filepath, data);
	}
	
	public static Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval) {
		return generalMethod.readfromexcel(excelfilepath, sheetname, rowname, rowval);
	}
	
	public static boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam) {
		return generalMethod.updatejsonusingexcel(excelfilepath, jsonfilepath, sheetname, rowname, rowval, runtimparam);
	}
	
	public static String getruntimevalueifexist(String identifiername) {
		return generalMethod.getruntimevalueifexist(identifiername);
	}
	
	public static boolean verifyadditionofvalues(String... str) {
		return generalMethod.verifyadditionofvalues(str);
	}
	
	public static boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam) {
		return generalMethod.generatecustomdateandtime(UDate, DateFormat, Time, Meri, runtimeparam);
	}
	
	public static boolean getisodateandtime(String UDate, String Time, String runtimeparam) {
		return generalMethod.getisodateandtime(UDate, Time, runtimeparam);
	}
	
	public static boolean getdate(String UDate, String DateFormat, String runtimeparam) {
		return generalMethod.getdate(UDate, DateFormat, runtimeparam);
	}
	
	public static boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
		return generalMethod.gettime(Time, TimeFormat, Meridiem, runtimeparam);
	}
	
	public static boolean checkiffileexists(String filePathString) throws Exception {
		return generalMethod.checkiffileexists(filePathString);
	}
	
	public static String convertxmltojson(String xmlfile) {
		return generalMethod.convertxmltojson(xmlfile);
	}
	
	public static boolean validatefromxml(String xmlfile, String key, String expectedvalue) {
		return generalMethod.validatefromxml(xmlfile, key, expectedvalue);
	}
	
	public static boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
		return generalMethod.storexmlvalue(xmlfile, key, runtimeparam);
	}
	
	public static boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam) {
		return generalMethod.excelgetcelldata(excelfilepath, sheetname, cellreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,

			String runtimeparam) {
		return generalMethod.excelgetcoldatausingcolref(excelfilepath, sheetname, colreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam) {
		return generalMethod.excelgetcoldatausinghdr(excelfilepath, sheetname, hdrname, runtimparam);
	}
	
	public static boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdatausinghdr(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdata(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusingcolref(excelfilepath, sheetname, colreference, replacevalue);
	}
	
	public static boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusinghdr(excelfilepath, sheetname, hdrname, replacevalue);
	}
	
	public static boolean excelupdatecellvalwrtrowval(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowval(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue)
			throws IOException {
		return generalMethod.csvupdatecelldata(fileToUpdate, row, col, replacevalue);
	}
	
	public static boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException {
		return generalMethod.csvgetcelldata(fileToUpdate, row, col, runtimeparam);
	}
	
	public static boolean additionofvalues(String... param1) {
		return generalMethod.additionofvalues(param1);
	}
	
	public static boolean subtractionofvalues(String... param1) {
		return generalMethod.subtractionofvalues(param1);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate, Ignorechar);
	}
	
	public static boolean concatandstore(String... param1) {
		return generalMethod.concatandstore(param1);
	}
	
	public static boolean excelupdatecellvalwrtrowvalakpk(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowvalakpk(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean simulatekeyboardwk() throws InterruptedException, AWTException {
		return generalMethod.simulatekeyboardwk();
	}
	public static boolean getkeyjson(String Json, String key, String runtime) {
		return generalMethod.getkeyjson(Json, key, runtime);
	}
	
	public static boolean getprocessutilization(String processToFind) {
		return generalMethod.getprocessutilization(processToFind);
	}
	
	public static boolean multiplejsonkey(String response,String key1,String key2,String key3,String identifiername,String key4) {
		return generalMethod.multiplejsonkey(response, key1, key2, key3, identifiername, key4);
	}
	
	public static boolean verifyapiresponse(String... param1) {
		return generalMethod.verifyapiresponse(param1);
	}
	
	public static boolean launchapplication(String url,String browsername) {
		return webmethods.launchapplicationwithspecificbrowser(url, browsername);
	}
	
	public static boolean storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey, String index,
			String runtimeparam) {
		return generalMethod.storearrayjsonkey(basejson, refkey, refvalue, actualkey, index, runtimeparam);
	}
	
	public static String storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey) {
		return generalMethod.storearrayjsonkey(basejson, refkey, refvalue, actualkey);
	}
	
	public boolean storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey,
			String runtimeparam) {
		return generalMethod.storearrayjsonkey(basejson, refkey, refvalue, actualkey, runtimeparam);
	}
	
	public boolean microsoftreademails(String emailid, String password, String subject, String runtimeparam) {
		return generalMethod.microsoftreademails(emailid, password, subject, runtimeparam);
	}
	
	public boolean getmail(String servername, String portno, String username, String password, String subject,
			String runtimparam) {
		return generalMethod.getmail(servername, portno, username, password, subject, runtimparam);
	}
	
	public static boolean InitializeWebBrowser(String url, String browsername) {
		return webmethods.InitializeWebBrowser(url, browsername);
	}
		
	
	
}