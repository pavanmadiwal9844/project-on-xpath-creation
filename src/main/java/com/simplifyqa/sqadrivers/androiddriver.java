package com.simplifyqa.sqadrivers;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.codoid.products.fillo.Recordset;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.StartApp;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.method.AndroidMethod;
import com.simplifyqa.method.GeneralMethod;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class androiddriver {
	public static AndroidMethod androidmethods = new AndroidMethod();
	public static GeneralMethod generalMethod = new GeneralMethod();
	
	public static Class myClass = null;
	private String classname = "com.android.WebCustom";
	public static Editorclassloader customMethod = null;
	
	public static Object getCurrentObject() {
		androidmethods.getDriver();
		return androidmethods.curobject;
	}
	
	public static RemoteWebDriver getAndroidDriver() {
		return androidmethods.getDriver();
	} 
	public static Boolean startapp(String Package, String Activity) {
		return androidmethods.startapp(Package, Activity);
	}

	public static Boolean resetapp(String Package, String Activity) {
		return androidmethods.resetapp(Package, Activity);
	}

	public static AndroidElement getElement(String objectName) {
		return androidmethods.getElement(objectName);
	}

	public static AndroidElement getElementbyobjRep(String objectName, String identifierName) {
		return androidmethods.getElementbyobjRep(objectName, identifierName);
	}

	public static AndroidElement getElement(String identifierName, String value) {
		return androidmethods.getElement(identifierName, value);
	}

	public static List<WebElement> getElements(String identifierName, String value) {
		return androidmethods.getElements(identifierName, value);
	}

	public static Boolean clickbycoordinate(String x, String y) {
		return androidmethods.clickbycoordinate(x, y);
	}

	public static Boolean exists() {
		return androidmethods.exists();
	}

	public static boolean existsusindynamicxpath(String dynamicvalue) {
		return androidmethods.existsusindynamicxpath(dynamicvalue);
	}

	public static boolean notexists(String dynamicvalue) {
		return androidmethods.notexists(dynamicvalue);
	}

	public static Boolean notexists() {
		return androidmethods.notexists();
	}

	public static Boolean entertext(String value) {
		return androidmethods.entertext(value);
	}

	public static Boolean entertext(String value, String value2) {
		return androidmethods.entertext(value, value2);
	}

	public static Boolean entertextusingdynamicxpath(String value, String dynamicvalue) {
		return androidmethods.entertextusingdynamicxpath(value, dynamicvalue);
	}

	public static Boolean duplicate_element_click() {
		return androidmethods.duplicate_element_click();
	}

	public static Boolean validatetext(String value) {
		return androidmethods.validatetext(value);
	}

	public static Boolean validatepartialtext(String value) {
		return androidmethods.validatepartialtext(value);
	}

	public static Boolean click() {
		return androidmethods.click();
	}

	public static Boolean findelementandclickbyadb() {
		return androidmethods.findelementandclickbyadb();
	}

	public static Boolean findelementandclickbyadb(String identifierName, String value) {
		return androidmethods.findelementandclickbyadb(identifierName, value);
	}

	public static Boolean Tap(Object object, int timeout) {
		return androidmethods.Tap(object, timeout);
	}

	public static Boolean click(Object object, int timeout) {
		return androidmethods.click(object, timeout);
	}

	public static Boolean readvalueandstore(String runtimePramaname) {
		return androidmethods.readvalueandstore(runtimePramaname);
	}

	public static Boolean readtextandstore(String runtimePramaname) {
		return androidmethods.readtextandstore(runtimePramaname);
	}

	public static Boolean clickifexists() {
		return androidmethods.clickifexists();
	}

	public static Boolean Tap(AndroidElement Element) {
		return androidmethods.Tap(Element);
	}

	public static Boolean Clear(AndroidElement Element) {
		return androidmethods.Clear(Element);
	}

	public static Boolean Click(AndroidElement Element) {
		return androidmethods.Click(Element);
	}

	public static Boolean Clear(String identifierName, String value) {
		return androidmethods.Clear(identifierName, value);
	}

	public static Boolean Tap(String identifierName, String value) {
		return androidmethods.Tap(identifierName, value);
	}

	public static boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		return androidmethods.clickusingdynamicxpath(xpath, runtimevlau);
	}

	public static boolean clickusingdynamicxpath(String runtimevlau) {
		return androidmethods.clickusingdynamicxpath(runtimevlau);
	}

	public static Boolean Click(String identifierName, String value) {
		return androidmethods.Click(identifierName, value);
	}

	public static Boolean Tap(int x, int y) {
		return androidmethods.Tap(x, y);
	}

	public static Boolean TapByAdb(int x, int y) {
		return androidmethods.TapByAdb(x, y);
	}

	public static Boolean swipe(int startX, int startY, int endX, int endY, int timeout) {
		return androidmethods.swipe(startX, startY, endX, endY, timeout);
	}

	public static Boolean SwipeByPercent(double x1Percent, double y1Percent, double x2Percent, double y2Percent) {
		return androidmethods.SwipeByPercent(x1Percent, y1Percent, x2Percent, y2Percent);
	}

	public static void swipeByElements(AndroidElement startElement, AndroidElement endElement) {
		androidmethods.swipeByElements(startElement, endElement);
	}

	public static Boolean scrollDown()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollDown();
	}

	public static Boolean scrolldown(int xCoornidates)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrolldown(xCoornidates);
	}

	public static Boolean scrollup(int xCoordinates)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollup(xCoordinates);
	}

	public static Boolean scrollUp()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollUp();
	}

	public static Boolean scrolltillelementverticalup() {
		return androidmethods.scrolltillelementverticalup();
	}

	public static Boolean scrolltillelementverticaldown() {
		return androidmethods.scrolltillelementverticaldown();
	}

	public static Boolean scrolltillelementhorizontalright() {
		return androidmethods.scrolltillelementhorizontalright();
	}

	public static Boolean scrolltillelementhorizontalleft() {
		return androidmethods.scrolltillelementhorizontalleft();
	}

	public static Boolean scrolltillelementverticalup(String param) {
		return androidmethods.scrolltillelementverticalup(param);
	}

	public static Boolean scrolltillelementverticaldown(String param) {
		return androidmethods.scrolltillelementverticaldown(param);
	}

	public static Boolean scrolltillelementhorizontalright(String param) {
		return androidmethods.scrolltillelementhorizontalright(param);
	}

	public static Boolean scrolltillelementhorizontalleft(String param) {
		return androidmethods.scrolltillelementhorizontalleft(param);
	}

	public static Boolean scrollTillElementVertical(Object object, boolean direction) {
		return androidmethods.scrollTillElementVertical(object, direction);
	}

	public static boolean scrollTillElementVertical(String identifierName, String value, boolean direction) {
		return androidmethods.scrollTillElementVertical(identifierName, value, direction);
	}

	public static boolean scrollTillElementVertical(String identifierName, String value, boolean direction,
			int xCoordinates) {
		return androidmethods.scrollTillElementVertical(identifierName, value, direction, xCoordinates);
	}

	public static Boolean scrollLeft()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollLeft();
	}

	public static Boolean scrollLeft(int Ycoordinate)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollLeft(Ycoordinate);
	}

	public static Boolean scrollRight()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollRight();
	}

	public static Boolean scrollRight(int Ycoordinate)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.scrollRight(Ycoordinate);
	}

	public static Boolean scrollTillElementHorizontal(Object object, boolean direction) {
		return androidmethods.scrollTillElementHorizontal(object, direction);
	}

	public static String getElementText(String identifierName, String value) {
		return androidmethods.getElementText(identifierName, value);
	}

	public static boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction) {
		return androidmethods.scrollTillElementHorizontal(identifierName, value, direction);
	}

	public static boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction,
			int Ycoordinate) {
		return androidmethods.scrollTillElementHorizontal(identifierName, value, direction, Ycoordinate);
	}

	public static Boolean swipeevent() {
		return androidmethods.swipeevent();
	}

	public static Boolean scrollright() {
		return androidmethods.scrollright();
	}

	public static Boolean scrollleft() {
		return androidmethods.scrollleft();
	}

	public static Boolean scrolldown() {
		return androidmethods.scrolldown();
	}

	public static Boolean scrollup() {
		return androidmethods.scrollup();
	}

	public static Boolean swipeByAdb(int startX, int startY, int endX, int endY, int timeout) {
		return androidmethods.swipeByAdb(startX, startY, endX, endY, timeout);
	}

	public static Boolean holdelement(int startX, int startY, int endX, int endY) {
		return androidmethods.holdelement(startX, startY, endX, endY);
	}

	public static Boolean HoldByPercent(double x1Percent, double y1Percent, double x2Percent, double y2Percent) {
		return androidmethods.HoldByPercent(x1Percent, y1Percent, x2Percent, y2Percent);
	}

	public static Boolean HoldByElements(AndroidElement startElement, AndroidElement endElement) {
		return androidmethods.HoldByElements(startElement, endElement);
	}

	public static Boolean HoldByAdb(int startX, int startY, int endX, int endY) {
		return androidmethods.HoldByAdb(startX, startY, endX, endY);
	}

	public static Boolean LongPress(AndroidElement Element) {
		return androidmethods.LongPress(Element);
	}

	public static Boolean LongPress(Object object) {
		return androidmethods.LongPress(object);
	}

	public static Boolean LongPress(String identifierName, String value) {
		return androidmethods.LongPress(identifierName, value);
	}

	public static Boolean LongPress(int startX, int startY) {
		return androidmethods.LongPress(startX, startY);
	}

	public static String getXml(RemoteWebDriver sqaDriver) {
		return androidmethods.getXml(sqaDriver);
	}

	public static String getXml() {
		return androidmethods.getXml();
	}

	public static Dimension getsize()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		return androidmethods.getsize();
	}

	public static int width() {
		return androidmethods.width();
	}

	public static int height() {
		return androidmethods.height();
	}

	public static AndroidDriver<WebElement> getSaqAndroidDriver() {
		return androidmethods.getSaqAndroidDriver();
	}

	public static Boolean runapp(String Package, String Activity) {
		return androidmethods.runapp(Package, Activity);
	}

	public static Boolean installapp(String s) {
		return androidmethods.installapp(s);
	}

	public static Boolean UnInstallApp(String Package) {
		return androidmethods.UnInstallApp(Package);
	}

	public static JSONArray getPackages(String deviceId) {
		return androidmethods.getPackages(deviceId);
	}

	public static void setDevice(String deviceID) {
		androidmethods.setDevice(deviceID);
	}

	public static boolean SetUiautomor2(String deviceId, Boolean playback) throws InstallException, IOException,
			TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException {
		return androidmethods.SetUiautomor2(deviceId, playback);
	}

	public static Boolean ClearkeysByAdb(String value) {
		return androidmethods.ClearkeysByAdb(value);
	}

	public static Boolean ClearkeysByAdb() {
		return androidmethods.ClearkeysByAdb();
	}

	public static boolean cleartext(String identifiername, String value) {
		return androidmethods.cleartext(identifiername, value);
	}

	public static Boolean ClearText(String identifierName, String value) {
		return androidmethods.ClearText(identifierName, value);
	}

	public static Boolean sendkeysByOther(Object object, String str) {
		return androidmethods.sendkeysByOther(object, str);
	}

	public static Boolean SendkeysByAdb(String value) {
		return androidmethods.SendkeysByAdb(value);
	}

	public static Boolean enter() {
		return androidmethods.enter();
	}

	public static Boolean sendkeysWithAdb(String identifierName, String value, String str) {
		return androidmethods.sendkeysWithAdb(identifierName, value, str);
	}

	public static Boolean sendkeysByOther(String str) {
		return androidmethods.sendkeysByOther(str);
	}

	public static Boolean Sendkeys(Object object, String str) {
		return androidmethods.Sendkeys(object, str);
	}

	public static Boolean Sendkeys(String identifierName, String value, String str) {
		return androidmethods.Sendkeys(identifierName, value, str);
	}

	public static Boolean emulatorevent(String value) {
		return androidmethods.emulatorevent(value);
	}

	public static Boolean EmulatorEvent() {
		return androidmethods.EmulatorEvent();
	}

	public static Boolean validatetext(Object object, String str) {
		return androidmethods.validatetext(object, str);
	}

	public static Boolean validatepartialtext(Object object, String str) {
		return androidmethods.validatepartialtext(object, str);
	}

	public static Boolean validatetext(String identifierName, String Value, String Text) {
		return androidmethods.validatetext(identifierName, Value, Text);
	}

	public static Boolean validatepartialtext(String identifierName, String Value, String Text) {
		return androidmethods.validatepartialtext(identifierName, Value, Text);
	}

	public static Boolean validateattr(String str1, String str2) {
		return androidmethods.validateattr(str1, str2);
	}

	public static Boolean validateattr(Object object, String str1, String str2) {
		return androidmethods.validateattr(object, str1, str2);
	}

	public static Boolean validateattr(String identifierName, String Value, String text, String text1) {
		return androidmethods.validateattr(identifierName, Value, text, text1);
	}

	public static Boolean exists(Object object) {
		return androidmethods.exists(object);
	}

	public static Boolean exists(String identifierName, String value) {
		return androidmethods.exists(identifierName, value);
	}

	public static Boolean notexists(String identifierName, String value) {
		return androidmethods.notexists(identifierName, value);
	}

	public static boolean wifiStatus() {
		return androidmethods.wifiStatus();
	}

	public static boolean dataStatus() {
		return androidmethods.dataStatus();
	}

	public static boolean bluetoothStatus() {
		return androidmethods.bluetoothStatus();
	}

	public static boolean airplaneModeStatus() {
		return androidmethods.airplaneModeStatus();
	}

	public static Boolean mbwifitoggle(String value) {
		return androidmethods.mbwifitoggle(value);
	}

	public static boolean wifiToggle(boolean flag) {
		return androidmethods.wifiToggle(flag);
	}

	public static Boolean mbdatatoggle(String value) {
		return androidmethods.mbdatatoggle(value);
	}

	public static boolean dataToggle(boolean flag) {
		return androidmethods.dataToggle(flag);
	}

	public static Boolean mbbluetoothtoggle(String value) {
		return androidmethods.mbbluetoothtoggle(value);
	}

	public static boolean bluetoothToggle(boolean flag) {
		return androidmethods.bluetoothToggle(flag);
	}

	public static String getOrientation() {
		return androidmethods.getOrientation();
	}

	public static Boolean mbscreenrotation(String orientation) {
		return androidmethods.mbscreenrotation(orientation);
	}

	public static JSONObject uniqueElement(Object obj) {
		return androidmethods.uniqueElement(obj);
	}

	public static JSONObject parallelsearch(Object obj, ArrayList<Future<JSONObject>> taskList)
			throws InterruptedException {
		return androidmethods.parallelsearch(obj, taskList);
	}

	public static JSONObject killtask(ArrayList<Future<JSONObject>> taskList)
			throws InterruptedException, ExecutionException, TimeoutException {
		return androidmethods.killtask(taskList);
	}

	public static JSONObject fnElements(String identifiername, String value) {
		return androidmethods.fnElements(identifiername, value);
	}

	public static Boolean click_using_ocr() {
		return androidmethods.click_using_ocr();
	}

	public static Boolean entertext_using_ocr(String value) {
		return androidmethods.entertext_using_ocr(value);
	}

	public static Boolean keyboard_search() {
		return androidmethods.keyboard_search();
	}

	public static Boolean clickmultielementbydynamicxpath(String... params) {
		return androidmethods.clickmultielementbydynamicxpath(params);
	}

	public static boolean selectmutipleitemsfromdropdown(String xpath, String values) {
		return androidmethods.selectmutipleitemsfromdropdown(xpath, values);
	}

	public static boolean scrolltillelementexist(String xpath, String values) {
		return androidmethods.scrolltillelementexist(xpath, values);
	}

	public static boolean scrolltilltime() {
		return androidmethods.scrolltilltime();
	}

	public static Boolean readandstore(String runtimePramaname) {
		return androidmethods.readandstore(runtimePramaname);
	}

	public static boolean dataincrease(String ele1, String ele2) {
		return androidmethods.dataincrease(ele1, ele2);
	}

	public static boolean datadecrease(String ele1, String ele2) {
		return androidmethods.datadecrease(ele1, ele2);
	}

	public static boolean datesequence(String xpath, String no) {
		return androidmethods.datesequence(xpath, no);
	}

	public static boolean scrollandverifyelementnotexist(String value) {
		return androidmethods.scrollandverifyelementnotexist(value);
	}

	public static boolean elementpresent() {
		return androidmethods.elementpresent();
	}

	public static boolean elementnotpresent() {
		return androidmethods.elementnotpresent();
	}

	public static boolean elementvisible() {
		return androidmethods.elementvisible();
	}

	public static boolean elementnotvisible() {
		return androidmethods.elementnotvisible();
	}

	public static boolean elementdisabled() {
		return androidmethods.elementdisabled();
	}

	public static boolean elementenabled() {
		return androidmethods.elementenabled();
	}

	public static boolean elementchecked() {
		return androidmethods.elementchecked();
	}

	public static boolean elementnotchecked() {
		return androidmethods.elementnotchecked();
	}

	public static boolean elementcontainsvalue(String value) {
		return androidmethods.elementcontainsvalue(value);
	}

	public static boolean elementnotcontainsvalue(String value) {
		return androidmethods.elementnotcontainsvalue(value);
	}

	public static boolean elementequalstovalue(String value) {
		return androidmethods.elementequalstovalue(value);
	}

	public static boolean elementnotequalstovalue(String value) {
		return androidmethods.elementnotequalstovalue(value);
	}

	public static boolean elementgreaterthanvalue(String value) {
		return androidmethods.elementgreaterthanvalue(value);
	}

	public static boolean elementgreaterthanorequalstovalue(String value) {
		return androidmethods.elementgreaterthanorequalstovalue(value);
	}

	public static boolean elementlessthanvalue(String value) {
		return androidmethods.elementlessthanvalue(value);
	}

	public static boolean elementlessthanorequalstovalue(String value) {
		return androidmethods.elementlessthanorequalstovalue(value);
	}

	public static boolean elementmatchesvalue(String expectedValue) {
		return androidmethods.elementmatchesvalue(expectedValue);
	}

	public static boolean elementnotmatchesvalue(String expectedValue) {
		return androidmethods.elementnotmatchesvalue(expectedValue);
	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass(InitializeDependence.runtime_classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header,Class[] objectype)
			throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			InstantiationException, NoSuchMethodException, SecurityException {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null)) {
					return customMethod.ExecustomMethodwithproperdatatype(identifierName, value, InitializeDependence.runtime_classname,objectype);
				}
				else {
					return customMethod.ExecustomMethodwithproperdatatype(identifierName, value, InitializeDependence.runtime_classname,objectype);

				}
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	//General methods
	public static boolean randomalphanumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphanumeric(count, prefix, suffix, parameter);
	}
	
	public static Boolean getfromruntime(String identifiername) {
		return generalMethod.getfromruntime(identifiername);
	}
	
	public static boolean storeruntime(String paraname, String paravalue) {
		return generalMethod.storeruntime(paraname, paravalue);
	}
	
	public static Boolean hold(String timeOut) {
		return generalMethod.hold(timeOut);
	}
	
	public static boolean minwait() {
		return generalMethod.minwait();
	}
	
	public static boolean medwait() {
		return generalMethod.medwait();
	}
	
	public static boolean maxwait() {
		return generalMethod.minwait();
	}
	
	public static boolean randomnumeric(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomnumeric(count, prefix, suffix, parameter);
	}
	
	public static boolean combineruntime(String... param1) {
		return generalMethod.combineruntime(param1);
	}
	
	public static String readjson(String param1, String key, String index) {
		return generalMethod.readjson(param1, key, index);
	}
	
	public static boolean connecttodb(String param, String url, String username, String password) {
		return generalMethod.connecttodb(param, url, username, password);
	}
	
	public static Boolean closeConnection() {
		return generalMethod.closeConnection();
	}
	
	public static Boolean executequery(String req, String suffix) {
		return generalMethod.executequery(req, suffix);
	}
	
	public static Boolean executequery(String req) {
		return generalMethod.executequery(req);
	}
	
	public static boolean storedbtoruntime(String req, String suffix) {
		return generalMethod.storedbtoruntime(req, suffix);
	}
	
	public static Map<String, String> flattenJSON(String json) {
		return generalMethod.flattenJSON(json);
	}
	
	public static Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore) {
		return generalMethod.storejsonkey(JsonRuntimeParam, key, runtimeToStore);
	}

	public static boolean randomalphabets(String count, String prefix, String suffix, String parameter) {
		return generalMethod.randomalphabets(count, prefix, suffix, parameter);
	}
	
	public static String getRandomNumber() {
		return generalMethod.getRandomNumber();
	}
	
	public static boolean generaterandomstring(String runtimeParam, String prefix, String strLenght) {
		return generalMethod.generaterandomstring(runtimeParam, prefix, strLenght);
	}
	
	public static boolean validateparameter(String paramName, String value) {
		return generalMethod.validateparameter(paramName, value);
	}
	
	public static boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore) {
		return generalMethod.replaceinjsonandstore(baseString, replaceWith, replacevalue, runtimeStore);
	}
	
	public static boolean replaceandstore(String param1, String param2, String param3, String param4) {
		return generalMethod.replaceandstore(param1, param2, param3, param4);
	}
	
	public static boolean splitandgetvalue(String param, String delimt, String index, String param1) {
		return generalMethod.splitandgetvalue(param, delimt, index, param1);
	}
	
	public static boolean comparejsondoc(String path1, String path2) {
		return generalMethod.comparejsondoc(path1, path2);
	}
	
	public static boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam) {
		return generalMethod.replacestring(srcStr, replWith, replTo, runTimeParam);
	}
	
	public static boolean storeintojson(String Json, String key, String runtime) {
		return generalMethod.storeintojson(Json, key, runtime);
	}
	
	public static boolean storefromjson(String Json, String key, String runtime) {
		return generalMethod.storefromjson(Json, key, runtime);
	}
	
	public static boolean validatefromjson(String Json, String key, String ExpectedValue) {
		return generalMethod.validatefromjson(Json, key, ExpectedValue);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);

	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	
	public static boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		return generalMethod.restapiresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String formdatatype,
			String formdata, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		return generalMethod.restapi(m_baseURI, m_Method, m_Header, m_Parameters, formdatatype, formdata, responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	public static boolean generateexcelid(String filepath, String columnname) {
		return generalMethod.generateexcelid(filepath, columnname);
	}
	
	public static boolean generateexcelstring(String filepath, String columnname) {
		return generalMethod.generateexcelstring(filepath, columnname);
	}
	
	public static boolean validatefromsoap(String Json, String ExpectedValue) {
		return generalMethod.validatefromsoap(Json, ExpectedValue);
	}
	
	public static boolean existsusingsikuli(String imgpath) {
		return generalMethod.existsusingsikuli(imgpath);
	}
	
	public static boolean clickusingsikuli(String param) {
		return generalMethod.clickusingsikuli(param);
	}
	
	public static boolean entertextusingsikuli(String imgpath, String param) {
		return generalMethod.entertextusingsikuli(imgpath, param);
	}
	
	public static boolean storetdasmap(Map<String, String> tdMap) {
		return generalMethod.storetdasmap(tdMap);
	}
	
	public static JSONObject getruntimevariableJSon(String identifiername) {
		return generalMethod.getruntimevariableJSon(identifiername);
	}
	
	public static boolean deleteruntime(String param) {
		return generalMethod.deleteruntime(param);
	}
	
	public static String readFile(String FilePath) {
		return generalMethod.readFile(FilePath);
	}
	
	public static boolean readfile(String FilePath, String runtimeparam) {
		return generalMethod.readfile(FilePath, runtimeparam);
	}
	
	public static boolean writetofile(String filepath, String data) {
		return generalMethod.writetofile(filepath, data);
	}
	
	public static Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval) {
		return generalMethod.readfromexcel(excelfilepath, sheetname, rowname, rowval);
	}
	
	public static boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam) {
		return generalMethod.updatejsonusingexcel(excelfilepath, jsonfilepath, sheetname, rowname, rowval, runtimparam);
	}
	
	public static String getruntimevalueifexist(String identifiername) {
		return generalMethod.getruntimevalueifexist(identifiername);
	}
	
	public static boolean verifyadditionofvalues(String... str) {
		return generalMethod.verifyadditionofvalues(str);
	}
	
	public static boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam) {
		return generalMethod.generatecustomdateandtime(UDate, DateFormat, Time, Meri, runtimeparam);
	}
	
	public static boolean getisodateandtime(String UDate, String Time, String runtimeparam) {
		return generalMethod.getisodateandtime(UDate, Time, runtimeparam);
	}
	
	public static boolean getdate(String UDate, String DateFormat, String runtimeparam) {
		return generalMethod.getdate(UDate, DateFormat, runtimeparam);
	}
	
	public static boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
		return generalMethod.gettime(Time, TimeFormat, Meridiem, runtimeparam);
	}
	
	public static boolean checkiffileexists(String filePathString) throws Exception {
		return generalMethod.checkiffileexists(filePathString);
	}
	
	public static String convertxmltojson(String xmlfile) {
		return generalMethod.convertxmltojson(xmlfile);
	}
	
	public static boolean validatefromxml(String xmlfile, String key, String expectedvalue) {
		return generalMethod.validatefromxml(xmlfile, key, expectedvalue);
	}
	
	public static boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
		return generalMethod.storexmlvalue(xmlfile, key, runtimeparam);
	}
	
	public static boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam) {
		return generalMethod.excelgetcelldata(excelfilepath, sheetname, cellreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,

			String runtimeparam) {
		return generalMethod.excelgetcoldatausingcolref(excelfilepath, sheetname, colreference, runtimeparam);
	}
	
	public static boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam) {
		return generalMethod.excelgetcoldatausinghdr(excelfilepath, sheetname, hdrname, runtimparam);
	}
	
	public static boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdatausinghdr(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam) {
		return generalMethod.excelgetsheetdata(excelfilepath, sheetname, runtimeparam);
	}
	
	public static boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusingcolref(excelfilepath, sheetname, colreference, replacevalue);
	}
	
	public static boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname,

			String replacevalue) {
		return generalMethod.excelreplacevalueincolusinghdr(excelfilepath, sheetname, hdrname, replacevalue);
	}
	
	public static boolean excelupdatecellvalwrtrowval(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowval(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue)
			throws IOException {
		return generalMethod.csvupdatecelldata(fileToUpdate, row, col, replacevalue);
	}
	
	public static boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException {
		return generalMethod.csvgetcelldata(fileToUpdate, row, col, runtimeparam);
	}
	
	public static boolean additionofvalues(String... param1) {
		return generalMethod.additionofvalues(param1);
	}
	
	public static boolean subtractionofvalues(String... param1) {
		return generalMethod.subtractionofvalues(param1);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate);
	}
	
	public static boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar) {
		return generalMethod.comparepartialstring(Basestring, valuetovalidate, Ignorechar);
	}
	
	public static boolean concatandstore(String... param1) {
		return generalMethod.concatandstore(param1);
	}
	
	public static boolean excelupdatecellvalwrtrowvalakpk(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		return generalMethod.excelupdatecellvalwrtrowvalakpk(excelfilepath, sheetname, refhdrname, cellval, hdrname, replacevalue);
	}
	
	public static boolean simulatekeyboardwk() throws InterruptedException, AWTException {
		return generalMethod.simulatekeyboardwk();
	}
	public static boolean getkeyjson(String Json, String key, String runtime) {
		return generalMethod.getkeyjson(Json, key, runtime);
	}
	
	public static boolean getprocessutilization(String processToFind) {
		return generalMethod.getprocessutilization(processToFind);
	}
	
	public static boolean multiplejsonkey(String response,String key1,String key2,String key3,String identifiername,String key4) {
		return generalMethod.multiplejsonkey(response, key1, key2, key3, identifiername, key4);
	}
	
	public static boolean verifyapiresponse(String... param1) {
		return generalMethod.verifyapiresponse(param1);
	}
	
	public static boolean StartApplication(String Package, String Activity,String serialno) {
		return androidmethods.startapp(Package, Activity, serialno);
		
	}
	
	public static boolean InitializeAndroidDriver(String Package, String Activity) {
		return androidmethods.initializeandroiddriver(Package, Activity);
	}

}
