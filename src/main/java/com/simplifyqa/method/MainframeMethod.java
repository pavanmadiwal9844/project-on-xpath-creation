package com.simplifyqa.method;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GetMainFrameSteps;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.execution.Impl.AutomationImpl;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mainframe.Impl.MainframeImpl;
import com.simplifyqa.mainframe.Interface.MainframeInterface;

public class MainframeMethod implements com.simplifyqa.method.Interface.MainframeMethod {
	private static final Logger logger = LoggerFactory.getLogger(MainframeMethod.class);
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(MainframeMethod.class);
	LoggerClass logerOBJ = new LoggerClass();

	public static Semaphore semCon;
	public static Semaphore semProd;
	private MainframeInterface mainframe = new MainframeImpl();
	private Process recorderProcess;
	private Process playbackProcess;
	private Setupexec curExecuiton;
	private Boolean captureScreenshot = false;
	private HashMap<String, String> header;
	private JSONObject status;
	private String[] attrName = { "label", "position" };
	private Integer i = -1;
	private Editorclassloader customMethod = null;
	@SuppressWarnings("rawtypes")
	private Class myClass = null;
	private String classname = "CustomMethodsmainframe";
	public static Boolean release = false;

	private String getSimplifyRecorderExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.MAINFRAMEFOLDER.value(),
				UtilEnum.MAINFRAMERCFOLDER.value(), UtilEnum.MAINFRAMERECORDER.value() }).toString();
	}

	private String getSimplifyPlaybackExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.MAINFRAMEFOLDER.value(),
				UtilEnum.MAINFRAMEPLFOLDER.value(), UtilEnum.MAINFRAMEPLAYBACK.value() }).toString();
	}

	private String getSimplifyRecorderpath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.MAINFRAMEFOLDER.value(), UtilEnum.MAINFRAMERCFOLDER.value() })
				.toString();
	}

	private String getSimplifyPlaypath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.MAINFRAMEFOLDER.value(), UtilEnum.MAINFRAMEPLFOLDER.value() })
				.toString();
	}

	public Boolean startInspector() {
		try {
			recorderProcess = mainframe.startRecorderExe(getSimplifyInspectorExepath(), getSimplifyInspecotpath());
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public Boolean startValidation() {
		try {
			recorderProcess = mainframe.startRecorderExe(getSimplifyvalidationpath(), getSimplifyInspecotpath());
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	private String getSimplifyInspectorExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.MAINFRAMEFOLDER.value(),
				UtilEnum.MAINFRAMEOBJECTPICKER.value(), UtilEnum.MAINFRAMEINSPECTOR.value() }).toString();
	}

	private String getSimplifyvalidationpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.MAINFRAMEFOLDER.value(),
				UtilEnum.MAINFRAMEOBJECTPICKER.value(), UtilEnum.MAINFRAMEVALIDATION.value() }).toString();
	}

	private String getSimplifyInspecotpath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.MAINFRAMEFOLDER.value(), UtilEnum.MAINFRAMEOBJECTPICKER.value() })
				.toString();
	}

	public Boolean startRecorder() {
		try {
			recorderProcess = mainframe.startRecorderExe(getSimplifyRecorderExepath(), getSimplifyRecorderpath());
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public Boolean startPlayback() {
		try {
			if (recorderProcess != null) {
				recorderProcess.destroyForcibly();
			}
			playbackProcess = mainframe.startRecorderExe(getSimplifyPlaybackExepath(), getSimplifyPlaypath());
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public Boolean stopRecorder() throws IOException {
//		Runtime.getRuntime().exec("taskkill /F /IM PCSWS.exe");
		if (GetMainFrameSteps.exename != null)
			Runtime.getRuntime().exec("taskkill /F /IM " + GetMainFrameSteps.exename);
		return mainframe.stopRecorderExe(recorderProcess);
	}

	public Boolean stopplayback() {
		return mainframe.stopPlaybackExe(playbackProcess);
	}

	public void setDetail(Setupexec exedetail, HashMap<String, String> header, Integer itr) {
		this.curExecuiton = exedetail;
		this.header = header;
	}

	public JSONObject launchApp() {
		try {
			semProd.acquire();
			startPlayback();
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			return status;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	public void release(JSONObject res) {
		try {
			status = res;
			semCon.release();
			Thread.sleep(200);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			// TODO: handle exception
		}
	}

	public JSONObject typetext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurTypetext(steObject.getAttributes(), tcData, i);
	}

	public JSONObject launchapplication(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		launchApp();
		return launchapplication(steObject.getAttributes(), tcData, i);
	}

	private JSONObject launchapplication(List<JsonNode> attributes, String tcData, Integer i) {
		try {
//			if (i > attrName.length)
//				return null;
			JSONObject res = null;
//			JsonNode attr = InitializeDependence.findattr(attributes, attrName[i]);
//			if (attr != null) {
			if ((res = launchapplication(null, null, tcData)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())) {
				return res;
			} else {
				return res;
			}
//			} else {
//				return launchapplication(attributes, tcData, ++i);
//			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	private JSONObject resCurTypetext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = typetext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurTypetext(attributes, tcData, ++i);
				}
			} else {
				return resCurTypetext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject keyboardaction(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return keyboardaction(steObject.getAttributes(), tcData, i);
	}

	private JSONObject keyboardaction(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
//			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);

			if ((res = keyboardaction(null, null, tcData)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())) {
				return res;
			} else {
				return res;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject validatetext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatetext(steObject.getAttributes(), tcData, i);
	}

	private JSONObject validatetext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = validatetext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatetext(attributes, tcData, ++i);
				}
			} else {
				return validatetext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject waitfortext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return waitfortext(steObject.getAttributes(), tcData, i);
	}

	private JSONObject waitfortext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = waitfortext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return waitfortext(attributes, tcData, ++i);
				}
			} else {
				return waitfortext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject validatepartialtext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatepartialtext(steObject.getAttributes(), tcData, i);
	}

	private JSONObject validatepartialtext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = validatepartialtext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatepartialtext(attributes, tcData, ++i);
				}
			} else {
				return validatepartialtext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject click(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}

	private JSONObject resCurclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = click(attr.getString("name").toLowerCase(), attr.getString("value"))).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()) || attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurclick(attributes, ++i);
				}
			} else
				return resCurclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject deletetext(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return deletetext(steObject.getAttributes(), i);
	}

	private JSONObject deletetext(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = deletetext(attr.getString("name").toLowerCase(), attr.getString("value")))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return deletetext(attributes, ++i);
				}
			} else
				return deletetext(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject findonscreen(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return findonscreen(steObject.getAttributes(), tcData, i);
	}

	private JSONObject findonscreen(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = findonscreen(attr.getString("name").toLowerCase(), attr.getString("value"), tcData))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return findonscreen(attributes, tcData, ++i);
				}
			} else {
				return findonscreen(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject readtextandstore(Object steObject, String parametername) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		JSONObject res = readtextandstore(steObject.getAttributes(), i);
		readtextandstore(res, parametername);
		return res;
	}

	public boolean readtextandstore(JSONObject json, String Paraname) {
		try {
			header = new HashMap<String, String>();
			JSONObject re = new JSONObject();
			String innertext = null;
			if (curExecuiton == null) {
				curExecuiton = new Setupexec();
				curExecuiton.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecuiton.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecuiton.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecuiton.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecuiton.getAuthKey());
			}
			re.put("paramname", Paraname);
			re.put("paramvalue", json.getString("testdata"));
			InitializeDependence.serverCall.postruntimeParameter(curExecuiton, re, header);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public String getruntimevariable(String identifiername) {
		JSONObject res;
		try {
			if (curExecuiton == null || header.isEmpty()) {
				header = new HashMap<String, String>();
				curExecuiton = new Setupexec();
				curExecuiton.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecuiton.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecuiton.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecuiton.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecuiton.getAuthKey());
			}
			res = InitializeDependence.serverCall.getruntimeParameter(curExecuiton, identifiername, header);
			if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
				logger.info("Executed  getRuntime :{}", res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("value"));
				return res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0)
						.getString("value");
			} else
				logger.info("Failed to  getRuntime ");
			return null;
		} catch (Exception e) {
			logger.info("Failed to  getRuntime ");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Failed to getRuntime: ", null, e.toString(), "error");
			return null;
		}
	}

	private JSONObject readtextandstore(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = readtextandstore(attr.getString("name").toLowerCase(), attr.getString("value")))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return readtextandstore(attributes, ++i);
				}
			} else
				return readtextandstore(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject findelementbyposition(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return findelementbyposition(steObject.getAttributes(), i);
	}

	private JSONObject findelementbyposition(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = findelementbyposition(attr.getString("name").toLowerCase(), attr.getString("value")))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return findelementbyposition(attributes, ++i);
				}
			} else
				return findelementbyposition(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject doubleclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurdoubleclick(steObject.getAttributes(), i);
	}

	private JSONObject resCurdoubleclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = doubleclick(attr.getString("name").toLowerCase(), attr.getString("value")))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurdoubleclick(attributes, ++i);
				}
			} else
				return resCurdoubleclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	public JSONObject findelementbylabel(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return findelementbylabel(steObject.getAttributes(), i);
	}

	private JSONObject findelementbylabel(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			if (attr != null) {
				if ((res = findelementbylabel(attr.getString("name").toLowerCase(), attr.getString("value")))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return findelementbylabel(attributes, ++i);
				}
			} else
				return findelementbylabel(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Type the text", action_displayname = "typetext", actionname = "typetext", paraname_equals_curobj = "false", paramnames = { "identifierName","value","str" }, paramtypes = { "string","string","string" })
	public JSONObject typetext(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "typetext");
	}

	public JSONObject launchapplication(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "launchapplication");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Click the element", action_displayname = "click", actionname = "click", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject click(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "click");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Double Click the element", action_displayname = "doubleclick", actionname = "doubleclick", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject doubleclick(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "doubleclick");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Keyboard action", action_displayname = "keyboardaction", actionname = "keyboardaction", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject keyboardaction(String identifierName, String value, String str) {

		return postwithTD(identifierName, value, str, "keyboardaction");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Validate the element text", action_displayname = "validatetext", actionname = "validatetext", paraname_equals_curobj = "false", paramnames = { "identifierName","value","str" }, paramtypes = { "string","string","string" })
	public JSONObject validatetext(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "validatetext");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Validate the element partial text", action_displayname = "validatepartialtext", actionname = "validatepartialtext", paraname_equals_curobj = "false", paramnames = { "identifierName","value","str" }, paramtypes = { "string","string","string" })
	public JSONObject validatepartialtext(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "validatepartialtext");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Delete the text.", action_displayname = "deletetext", actionname = "deletetext", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject deletetext(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "deletetext");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "find element on the screen", action_displayname = "findonscreen", actionname = "findonscreen", paraname_equals_curobj = "false", paramnames = { "identifierName","value","str" }, paramtypes = { "string","string","string" })
	public JSONObject findonscreen(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "findonscreen");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Read text and store", action_displayname = "readtextandstore", actionname = "readtextandstore", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject readtextandstore(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "readtextandstore");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Find element by position", action_displayname = "findelementbyposition", actionname = "findelementbyposition", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject findelementbyposition(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "findelementbyposition");
	}

	@Override
	public JSONObject getfromruntime(String identifierName) {
		try {
			return InitializeDependence.serverCall.getruntimeParameter(curExecuiton, identifierName, header);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	
	private JSONObject postwithoutTD(String identifierName, String value, String action) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			System.out.println("bhaskar" + req.toString());
			GlobalDetail.mainFrSession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			release = true;
			return status;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	private JSONObject postwithTD(String identifierName, String value, String str, String action) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			req.put("tcData", str);
			System.out.println("bhaskar" + req.toString());
			GlobalDetail.mainFrSession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			release = true;
			semCon.acquire();
			return status;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Wait for the text", action_displayname = "waitfortext", actionname = "waitfortext", paraname_equals_curobj = "false", paramnames = { "identifierName","value","str" }, paramtypes = { "string","string","string" })
	public JSONObject waitfortext(String identifierName, String value, String str) {
		return postwithTD(identifierName, value, str, "waitfortext");
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Find the element by labelt", action_displayname = "findelementbylabel", actionname = "findelementbylabel", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject findelementbylabel(String identifierName, String value) {
		return postwithoutTD(identifierName, value, "findelementbylabel");
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header) {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Type Text", action_displayname = "typetext", actionname = "typetext", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject typetext(Object object, String value) {
		return resCurTypetext(object.getAttributes(), value, 0);
	}

	@Override
	public JSONObject click(Object object) {
		return resCurclick(object.getAttributes(), 0);
	}

	@Override
	public JSONObject doubleclick(Object object) {
		return resCurdoubleclick(object.getAttributes(), 0);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Keyboard action", action_displayname = "keyboardaction", actionname = "keyboardaction", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject keyboardaction(Object object, String value) {
		return keyboardaction(object.getAttributes(), value, 0);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Wait for the text", action_displayname = "waitfortext", actionname = "waitfortext", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject waitfortext(Object object, String value) {
		return waitfortext(object.getAttributes(), value, 0);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Validate the element text", action_displayname = "validatetext", actionname = "validatetext", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject validatetext(Object object, String value) {
		return validatetext(object.getAttributes(), value, 0);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Validate the partial text of element", action_displayname = "validatepartialtext", actionname = "validatepartialtext", paraname_equals_curobj = "false", paramnames = { "identifierName","value" }, paramtypes = { "string","string" })
	public JSONObject validatepartialtext(Object object, String value) {
		return validatepartialtext(object.getAttributes(), value, 0);
	}

	@Override
	public JSONObject deletetext(Object object) {
		return deletetext(object.getAttributes(), 0);
	}

	@Override
	public JSONObject findonscreen(Object object, String value) {
		return findonscreen(object.getAttributes(), value, 0);
	}

	@Override
	public JSONObject readtextandstore(Object object) {
		return readtextandstore(object.getAttributes(), 0);
	}

	@Override
	public JSONObject findelementbyposition(Object object) {
		return findelementbyposition(object.getAttributes(), 0);
	}

	@Override
	public JSONObject findelementbylabel(Object object) {
		return findelementbylabel(object.getAttributes(), 0);
	}

	@Override
	public JSONObject getfromruntime(Object object) {
		return null;
	}

	@Override
	public boolean wait(Integer timeout) {
		try {
			Thread.sleep(timeout);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

}
