package com.simplifyqa.method;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lwjgl.system.CallbackI.I;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.Capabilities;
import com.simplifyqa.DTO.FindElement;
import com.simplifyqa.DTO.FirstMatch;
import com.simplifyqa.DTO.IosStart;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Options;
import com.simplifyqa.DTO.Sendkey;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.iosActs;
import com.simplifyqa.DTO.iosact;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.grpc.grpc;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mobile.ios.Frame;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.mobile.ios.PackageDetail;
import com.simplifyqa.mobile.ios.Source;
import com.simplifyqa.mobile.ios.ideviceMaganger;
import com.simplifyqa.web.implementation.executor;

import io.appium.java_client.android.AndroidElement;

public class IosMethod {

	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	LoggerClass logerOBJ = new LoggerClass();

	private String sessionId;
	private Integer count = 0;
	private String currentEleement;
	private String ideviceId;
	private Editorclassloader customMethod = null;
	@SuppressWarnings("rawtypes")
	private Class myClass = null;
	private String classname = "CustomMethodsios";
	public static Integer currentSeq = -1;
	public Object curObject = null;
	private JsonNode attribute = null, attribute2 = null;
	private String[] ididame = { "xpath", "XpathByName", "XpathByLabel", "XpathByValue" };
	private static final Logger logger = LoggerFactory.getLogger(IosMethod.class);
	private String exepectedValue = null;

	public String getExepectedValue() {
		return exepectedValue;
	}

	public void setExepectedValue(String exepectedValue) {
		this.exepectedValue = exepectedValue;
	}

	public String getCurrentEleement() {
		return currentEleement;
	}

	public Setupexec curExecution = null;
	public HashMap<String, String> header = new HashMap<String, String>();

	public void setCurrentEleement(String currentEleement) {
		this.currentEleement = currentEleement;
	}

	public String getIdeviceId() {
		return ideviceId;
	}

	public void setIdeviceId(String ideviceId) {
		this.ideviceId = ideviceId;
		HttpUtility.udid = ideviceId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public IosMethod() {
// TODO Auto-generated constructor stub
	}

	public IosMethod(Setupexec curExecution, Integer currentSeq, HashMap<String, String> header) {
		this.curExecution = curExecution;
		this.currentSeq = currentSeq;
		this.header = header;
	}

	public String getxmlDescription() {
		try {
			@SuppressWarnings({ "resource" })
			Source convert = new Source();
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/source?format=description");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return convert.a(res);
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getxmlDescription();
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
				return null;
			}
		}
	}

	public String getxml() {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/source");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			String value = new StringBuilder(
					res.getString("value").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<hierarchy>"))
							.append("</hierarchy>" + "").toString();
			count = 0;
			return value;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getxml();
			} else {
				count = 0;
				return null;
			}
		}
	}

	public String getxmlJson() {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/source?format=json");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return res.getString("value");
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getxmlJson();
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
				return null;
			}
		}
	}

	public String getxmlAccessibleSource() {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/wda/accessibleSource");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return res.getString("value");
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getxmlAccessibleSource();
			} else {
				logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
				count = 0;
				return null;
			}
		}
	}

	public void setSessionId(String sessionid) {
		this.sessionId = sessionid;
	}

	private String createImageFromByte(byte[] binaryData) {
		try {
			Thread.sleep(1000);
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(Base64.getEncoder().encodeToString(binaryData));
			return sb.toString();
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append((InitializeDependence.iosDriver.get(sessionId)).Screenshot());
			return sb.toString();

		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "startapp", actionname = "startapp", paraname_equals_curobj = "false", paramnames = { "ParamString" }, paramtypes = { "String" })
	public boolean startapp(String ParamString) {
		try {
			Thread.sleep(2000);
			new Frame(ideviceId).init();
			if (launch_app(ParamString)) {
				if (size == null)
					size = size();
				if (GlobalDetail.websocket.containsKey(ideviceId)) {
					Thread.sleep(3000);
					GlobalDetail.websocket.get(ideviceId).getBasicRemote()
							.sendText(new JSONObject().put("size", size.getJSONObject("value").getInt("width") + "x"
									+ size.getJSONObject("value").getInt("height")).toString());
				}
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "isenabled", actionname = "isenabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean isenabled() {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/enabled");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			if (res.optJSONObject("value") == null) {
				count = 0;
				return res.getBoolean("value");
			} else
				return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "isdisplayed", actionname = "isdisplayed", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean isdisplayed() {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/displayed");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return res.getBoolean("value");
		} catch (Exception e) {
			return false;
		}
	}

	private Integer maxtime = 12;
	private Integer timeout = 0;
	private String value = "";
// private Integer failtimeout = 65;

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "until", actionname = "until", paraname_equals_curobj = "false", paramnames = { "name","value" }, paramtypes = { "String","String" })
	public boolean until(String name, String value) throws InterruptedException {
		Boolean found = false;
		try {
			while ((timeout++ < maxtime)) {
				Thread.sleep(500);
				if (FindElements(name, value) && isdisplayed() && isenabled()) {
					found = true;
					break;
				}
			}
			timeout = 0;
			if (!found)
				return false;
			else {
				logger.info("Element found:{}", this.value = value);
				logerOBJ.customlogg(userlogger, "Object Property :", null, name, "info");
				logerOBJ.customlogg(userlogger, "Object Property Value :", null, value, "info");
				return true;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action waits if the element not found, returns true if element found", action_displayname = "untilnocheck", actionname = "untilnocheck", paraname_equals_curobj = "false", paramnames = { "name","value" }, paramtypes = { "String","String" })
	public boolean untilnocheck(String name, String value)throws InterruptedException{
		Boolean found = false;
		try {
			while ((timeout++ < maxtime)) {
				Thread.sleep(500);
				if (FindElements(name, value)){
					found = true;
					break;
				}
			}
			timeout = 0;
			if (!found)
				return false;
			else {
				logger.info("Element found:{}", this.value = value);
				logerOBJ.customlogg(userlogger, "Object Property :", null, name, "info");
				logerOBJ.customlogg(userlogger, "Object Property Value :", null, value, "info");
				return true;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
		
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a click", action_displayname = "click", actionname = "click", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public boolean click() {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curObject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curObject.getAttributes(), "height");
			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				}
				Boolean status = req.getBoolean("status");
// Boolean text_present = req.getBoolean("text present");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					int x1 = Integer.parseInt(req.getJSONObject("center").get("x").toString());
					int y1 = Integer.parseInt(req.getJSONObject("center").get("y").toString());

					if (width_recorded == null || height_recorded == null) {
						return Tap(x + 20, y + 20);

					}

					else {
						return Tap(x1, y1);
					}

				}

				else {
					return false;
				}
			} else {
				if (uniqueElement(curObject)) {
					logger.info("Executed Click on :{} ", value);
					return Tap();
				} else {
					throw new Exception("Unable to click");
				}

			}
		} catch (Exception e) {

			logger.info("Failed to Execute Click on :{} ,No Element Found", value);
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action  reads the attribute value and returns it as a String", action_displayname = "getattrvalue", actionname = "getattrvalue", paraname_equals_curobj = "false", paramnames = { "value" }, paramtypes = { "String" })
	public String getattrvalue(String value) {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/attribute/" + value);
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			if (res.has("value")) {
				return res.getString("value");
			} else {
				return null;
			}
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getattrvalue(value);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
				return null;
			}
		}
	}
	
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action  reads value and stores it to runtime parameter", action_displayname = "readvalueandstore", actionname = "readvalueandstore", paraname_equals_curobj = "false", paramnames = { "runtimePramaname" }, paramtypes = { "String" })
	public Boolean readvalueandstore(String runtimePramaname) {
		try {

			GeneralMethod general = new GeneralMethod();
			general.deleteruntime(runtimePramaname);
			GlobalDetail.runTime.remove(runtimePramaname);

			JSONObject re = new JSONObject();
			if (uniqueElement(curObject)) {
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
				if (jsonObject.has("createdBy")) {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

				}
				String attrvalue = null;
				if ((attrvalue = getattrvalue("value")) != null) {
					re.put("paramvalue", attrvalue);
					InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
					logger.info("Value: " + re.get("paramvalue") + "stored into :" + runtimePramaname);

					return true;
				} else {
					logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
					throw new Exception("Unable to click");
				}

			} else {
				logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
				throw new Exception("Unable to click");
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action  reads text and stores it to runtime parameter", action_displayname = "readtextandstore", actionname = "readtextandstore", paraname_equals_curobj = "false", paramnames = { "runtimePramaname" }, paramtypes = { "String" })
	public Boolean readtextandstore(String runtimePramaname) {
		try {

			GeneralMethod general = new GeneralMethod();
			general.deleteruntime(runtimePramaname);
			GlobalDetail.runTime.remove(runtimePramaname);
			JSONObject re = new JSONObject();
			if (uniqueElement(curObject)) {
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
				if (jsonObject.has("createdBy")) {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

				}
				String attrvalue = null;
				if ((attrvalue = getattrvalue("value")) != null) {
					re.put("paramvalue", attrvalue);
					InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
					logger.info("Text: " + re.get("paramvalue") + "stored into :" + runtimePramaname);
					return true;
				} else {
					logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
					throw new Exception("Unable to click");
				}

			} else {
				logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
				throw new Exception("Unable to click");
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a click if the element exists", action_displayname = "clickifexists", actionname = "clickifexists", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public Boolean clickifexists() {
		try {
			maxtime = 5;
			if (exists()) {
				return Tap();
			} else {
				return true;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a click using dynamic xpath", action_displayname = "clickusingdynamicxpath", actionname = "clickusingdynamicxpath", paraname_equals_curobj = "true",  paramnames = { "" }, paramtypes = { "String" })
	public boolean clickusingdynamicxpath(String runtimevlau) {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			maxtime = 12;
			if (until("xpath", attribute.get("xpath").asText().replaceAll("#replace", runtimevlau))) {
				logger.info("Executed Click on :{} ",
						attribute.get("value").asText().replaceAll("#replace", runtimevlau));
				return Tap();
			} else {
				logger.info("Failed to Execute Click on :{} ,No Element Found",
						attribute.get("xpath").asText().replaceAll("#replace", runtimevlau));
				throw new Exception("Unable to click");
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a click using dynamic xpath", action_displayname = "clickusingdynamicxpath", actionname = "clickusingdynamicxpath", paraname_equals_curobj = "true", paramnames = { "","" }, paramtypes = { "String","String" })
	public boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		try {
			maxtime = 12;
			if (until("xpath", xpath.replaceAll("#replace", runtimevlau))) {
				logger.info("Executed Click on :{} ", xpath.replaceAll("#replace", runtimevlau));
				return Tap();
			} else {
				logger.info("Failed to Execute Click on :{} ,No Element Found",
						xpath.replaceAll("#replace", runtimevlau));
				throw new Exception("Unable to click");
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "duplicate_element_click", action_displayname = "duplicate_element_click", actionname = "duplicate_element_click", paraname_equals_curobj = "false", paramnames = { "ParamString" }, paramtypes = { "String" })
	public boolean duplicate_element_click(String ParamString) {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));

				req.put("image2", attribute.get("value").asText());

				req.put("width", size().getJSONObject("value").getInt("width"));

				req.put("height", size().getJSONObject("value").getInt("height"));
				req.put("text", "");
				req.put("rt_top", "needed");
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				Boolean status = req.getBoolean("status");
// Boolean text_present = req.getBoolean("text present");

				if (status) {

// int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
// int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					int x = req.getInt("rt_tp_x");
					int y = req.getInt("rt_tp_y");
					return Tap(x - 20, y - 20);
				}

				else {
					return false;
				}

			} else {

				if (uniqueElement(curObject)) {
					return Tap();
				} else {
					attribute = InitializeDependence.findattr(curObject.getAttributes(), "x");
					attribute2 = InitializeDependence.findattr(curObject.getAttributes(), "y");
					int mWidth = -1, mHieght = -1, x = -1, y = -1;
					mWidth = width();
					mHieght = height();
					x = (int) ((Math.round(attribute.get("value").asDouble()) * mWidth) / 100);
					y = (int) ((Math.round(attribute2.get("value").asDouble()) * mHieght) / 100);
					if (attribute != null && attribute2 != null) {
						return Tap(x, y);
					} else
						return false;
				}

			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action clears the text", action_displayname = "cleartext", actionname = "cleartext", paraname_equals_curobj = "false", paramnames = { "ParamString" }, paramtypes = { "String" })
	public boolean cleartext(String ParamString) {
		try {
			if (uniqueElement(curObject)) {
				return cleartext();
			} else
				throw new Exception("Unable to cleartext");
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			logger.error(e.getMessage());
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action enters the given text", action_displayname = "entertext", actionname = "entertext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertext(String ParamString) {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				Boolean status = req.getBoolean("status");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());
// int x = req.getInt("rt_tp_x");
// int y = req.getInt("rt_tp_y");
					Tap(x + 20, y + 20);
					return EnterText(ParamString);
				} else
					throw new Exception("Unable to Entertext");

			} else {

				if (uniqueElement(curObject)) {
					return EnterText(ParamString);
				} else
					throw new Exception("Unable to Entertext");
			}
		} catch (Exception e) {

			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the object exists", action_displayname = "exists", actionname = "exists", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean exists() {
		try {
			if (uniqueElement(curObject))
				return true;
			else
				throw new Exception("Exception exists");

		} catch (Exception e) {
			logger.info("unable to check element: {} exists", value);
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action validates whether the object exists or not", action_displayname = "validateexist", actionname = "validateexist", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean validateexist() {
		try {
			if (uniqueElement2(curObject))
				return true;
			else
				throw new Exception("Exception exists");

		} catch (Exception e) {
			logger.info("unable to check element: {} exists", value);
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}


	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a long press", action_displayname = "longpress", actionname = "longpress", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean longpress() {
		try {
			if (uniqueElement(curObject)) {

				HttpUtility.SendPost(
						"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId
								+ "/wda/element/" + currentEleement + "/touchAndHold",
						new JSONObject().put("duration", "2.5").toString(), new HashMap<String, String>());
				count = 0;
				return true;
			} else
				throw new Exception("Exception exists");

		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return longpress();
			} else {
				count = 0;
				logger.info("unable to perform tounch and hold on element: {} ", value);
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to perform tounch and hold on element: ",
						null, e.toString(), "error");
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action reads the element value and stores it to runtime parameter", action_displayname = "readandstore", actionname = "readandstore", paraname_equals_curobj = "false", paramnames = { "runtimePramaname" }, paramtypes = { "String" })
	public Boolean readandstore(String runtimePramaname) {
		try {

			GeneralMethod general = new GeneralMethod();
			general.deleteruntime(runtimePramaname);
			GlobalDetail.runTime.remove(runtimePramaname);
			String value = null;

			JSONObject re = new JSONObject();
			for (JsonNode attr : curObject.getAttributes()) {
				if (attr.has("name")) {
					if (attr.get("name").toString().contains("xpath")) {
						value = attr.get("value").asText();
						break;
					}

				}
			}
			if (FindElements("xpath", value)) {
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
				if (jsonObject.has("createdBy")) {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

				}
				String attrvalue = null;
				if ((attrvalue = getattrvalue("value")) != null) {
					re.put("paramvalue", attrvalue);
					InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
					logger.info("Value: " + re.get("paramvalue") + "stored into :" + runtimePramaname);

					return true;
				} else {
					logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
					throw new Exception("Unable to click");
				}

			} else {
				logger.info("Failed to Execute  readvalueandstore on :{} ,No Element Found");
				throw new Exception("Unable to click");
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a long press action", action_displayname = "Longpress", actionname = "Longpress", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean Longpress() {
		try {
			HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId
							+ "/wda/element/" + currentEleement + "/touchAndHold",
					new JSONObject().put("duration", "2.5").toString(), new HashMap<String, String>());
			count = 0;
			return true;

		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return Longpress();
			} else {
				count = 0;
				logger.info("unable to perform tounch and hold on element: {} ", value);
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to perform tounch and hold on element: ",
						null, e.toString(), "error");
				return false;
			}
		}
	}

	public boolean exists(String identifierName, String value) {
		try {
			return until(identifierName, value);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the element not exists", action_displayname = "notexists", actionname = "notexists", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean notexists() {
		try {
			if (uniqueElement(curObject))
				throw new Exception("Exception NotExists");
			else
				return true;

		} catch (Exception e) {
			logger.info("unable to check element: {} NOt Exists", value);
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to check element: NOt Exists ", null,
					e.toString(), "error");
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action validates text", action_displayname = "validatetext", actionname = "validatetext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatetext(String ParamString) {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curObject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curObject.getAttributes(), "height");

			if (attribute != null) {
				System.out.println(createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", ParamString);
				req.put("rt_top", "");
				req.put("device", "xr");

				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				}
				int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
				int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

				Boolean status = req.getBoolean("status");
				Boolean text_present = req.getBoolean("text present");

				if (status) {
					if (text_present) {
						return true;
					} else {
						throw new Exception("Exception ValidatText");
					}

				}

				else {
					throw new Exception("Exception ValidatText");
				}

			}

			else {
				if (uniqueElement(curObject)) {
					return validateText(ParamString);
				} else
					throw new Exception("Exception ValidatText");
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action validates text partially", action_displayname = "validatepartialtext", actionname = "validatepartialtext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatepartialtext(String ParamString) {
		try {
			if (uniqueElement(curObject)) {
				return validatepatialText(ParamString);
			} else
				throw new Exception("Exception ValidatPartialText");
		} catch (Exception e) {
			logger.info("unable to  ValidatPartialText : {} at {}", ParamString, value);
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to ValidatPartialText:", null, e.toString(),
					"error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "validateattr", action_displayname = "validateattr", actionname = "validateattr", paraname_equals_curobj = "false", paramnames = { "str1","str2" }, paramtypes = { "String","String" })
	public boolean validateattr(String str1, String str2) {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			if (attribute != null) {
				if (until(attribute.get("name").asText(), attribute.get("value").asText())) {
					return validateText(str2);
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to validateattr:", null, e.toString(), "error");
			return false;
		}
	}

	public boolean emulatorevent(String value) {
		if (value.equals("ioshome"))
			return Home();
		else {
			logger.error("Unable execute home button");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable execute home button", null, "", "error");
			return false;
		}
	}

	public boolean mbscreenrotation(String ParamString) {
		return setOrientation(ParamString);
	}

	public Boolean closeApp() {
		try {
			JSONObject res = HttpUtility
					.delete("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId);
			sessionId = res.getString("sessionId");
			GlobalDetail.iosSession.put(Thread.currentThread().getName(), sessionId);
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return closeApp();
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to closeApp :", null, e.toString(),
						"error");
				return false;
			}
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action launches the given application", action_displayname = "launch_app", actionname = "launch_app", paraname_equals_curobj = "false", paramnames = { "ParamString" }, paramtypes = { "String" })
	public boolean launch_app(String ParamString) {
		try {
			InitializeDependence.ios_device = true;
			FirstMatch Match = new FirstMatch();
			IosStart start = new IosStart();
			Capabilities capabilities = new Capabilities();
			List<FirstMatch> firstMatch = new ArrayList<FirstMatch>();
			Match.setBundleId(ParamString);
			Match.setEventloopIdleDelaySec(0);
			Match.setMaxTypingFrequency(60);
			Match.setShouldUseSingletonTestManager(true);
			Match.setShouldWaitForQuiescence(false);
			Match.setArguments(new ArrayList<String>());
			Match.setEnvironment(new JSONObject());
			Match.setShouldUseTestManagerForVisibilityDetection(false);
			firstMatch.add(Match);
			capabilities.setFirstMatch(firstMatch);
			start.setCapabilities(capabilities);
			String resp = HttpUtility.SendPost("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session",
					new ObjectMapper().writeValueAsString(start), new HashMap<String, String>());
			JSONObject res = new JSONObject(resp);
			sessionId = res.getString("sessionId");
			GlobalDetail.iosSession.put(Thread.currentThread().getName(), sessionId);
			if (res.has("value") && res.getJSONObject("value").has("error")) {
				logger.info(res.getJSONObject("value").getString("message"));
				throw new Exception("Appliction not found");
			}
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return launch_app(ParamString);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to launch_app :", null, e.toString(),
						"error");
				return false;
			}
		}

	}

	public boolean setOrientation(String orientation) {
		try {

			HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId
							+ "/orientation",
					new JSONObject().put("orientation", orientation.toUpperCase()).toString(),
					new HashMap<String, String>());
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return setOrientation(orientation);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to setOrientation :", null, e.toString(),
						"error");
				return false;
			}
		}
	}

	public String getOrientation() {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/orientation");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return res.getString("value");
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getOrientation();
			} else {
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to getOrientation :", null, e.toString(),
						"error");
				count = 0;
				return null;
			}
		}
	}

	public boolean Home() {
		try {

			HttpUtility.SendPost("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/wda/homescreen", "",
					new HashMap<String, String>());
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return Home();
			} else {
				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
				count = 0;
				return false;
			}
		}
	}

	public boolean Tap(int x, int y) {
		try {

			iosact Action = new iosact();
			iosActs Actions = new iosActs();
			Options options = new Options();
			List<iosact> Act = new ArrayList<iosact>();
			options.setX(x);
			options.setY(y);
			Action.setAction("tap");
			Action.setOptions(options);
			Act.add(Action);
			Actions.setActions(Act);
			System.out.println(new ObjectMapper().writeValueAsString(Actions));
			String response=HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId
							+ "/wda/touch/perform",
					new ObjectMapper().writeValueAsString(Actions), new HashMap<String, String>());
			System.out.println(response);
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return Tap(x, y);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to tap :", null, e.toString(), "error");
				return false;
			}
		}
	}

	public String Screenshot() {

		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/screenshot");
			getRequest.addHeader("accept", "application/json");
			int hardTimeout = 10;
			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					if (getRequest != null) {
						getRequest.abort();
					}
				}
			};
			new Timer(true).schedule(task, hardTimeout * 1000);
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			return new JSONObject(EntityUtils.toString(httpEntity, "UTF-8")).getString("value");
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to ScreenShot :", null, e.toString(), "error");
			return null;
		}
	}

	public boolean FindElement(String name, String value) {

		try {
			FindElement element = new FindElement();
			element.setUsing(name);
			element.setValue(value.replace("\\", "\""));
			JSONObject res = HttpUtility.sendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/element",
					new ObjectMapper().writeValueAsString(element), new HashMap<String, String>());
			if (!res.getJSONObject("value").has("error")) {
				currentEleement = res.getJSONObject("value").getString("ELEMENT");
				count = 0;
				return true;
			} else
				return false;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return FindElement(name, value);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to FindElement :", null, e.toString(),
						"error");
				return false;
			}
		}
	}

	public boolean FindElements(String name, String value) throws Exception {

		try {
			FindElement element = new FindElement();
			element.setUsing(name);
			element.setValue(value.replace("\\", "\""));
			JSONObject res = HttpUtility.sendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/elements",
					new ObjectMapper().writeValueAsString(element), new HashMap<String, String>());
			count = 0;
			if (res.get("value") instanceof JSONObject && res.getJSONObject("value").has("error"))
				return false;
			else if (res.get("value") instanceof JSONArray && res.getJSONArray("value").length() == 1) {
				currentEleement = res.getJSONArray("value").getJSONObject(0).getString("ELEMENT");
				return true;
			} else
				return false;
		} catch (

		Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return FindElements(name, value);
			} else {
				count = 0;
				timeout = 12;
				throw new Exception("failed");
			}

		}
	}

	public JSONArray FnElements(String name, String value) {

		try {
			FindElement element = new FindElement();
			element.setUsing(name);
			element.setValue(value.replace("\\", "\""));
			JSONObject res = HttpUtility.sendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/elements",
					new ObjectMapper().writeValueAsString(element), new HashMap<String, String>());
			if (res.get("value") instanceof JSONObject && res.getJSONObject("value").has("error"))
				throw new Exception("Unable to find Element");
			else if (res.get("value") instanceof JSONArray) {
				count = 0;
				return res.getJSONArray("value");
			} else {
				count = 0;
				return new JSONArray();
			}
		} catch (

		Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return FnElements(name, value);
			} else {
				count = 0;
				return new JSONArray();
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action validates text", action_displayname = "validateText", actionname = "validateText", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validateText(String value) {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/attribute/label");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			if (res.has("value")) {
				setExepectedValue(res.getString("value"));
				if (res.getString("value").equals(value))
					return true;
				else
					return false;
			} else {
				return false;
			}
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return validateText(value);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to FindElements :", null, e.toString(),
						"error");
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action validates text partially", action_displayname = "validatepatialText", actionname = "validatepatialText", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatepatialText(String value) {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/attribute/label");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			if (res.has("value")) {
				setExepectedValue(res.getString("value"));
				if (res.getString("value").contains(value))
					return true;
				else
					return false;
			} else {
				return false;
			}
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return validatepatialText(value);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to validatepatialText :", null,
						e.toString(), "error");
				return false;
			}
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action swipes to the object", action_displayname = "swipeevent", actionname = "swipeevent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean swipeevent() {
		try {
			int Mobile_width = -1, Mobile_height = -1, startX = -1, startY = -1, endX = -1, endY = -1, to = 1000;
			Mobile_width = width();

			Mobile_height = height();

			for (int i = 0; i < curObject.getAttributes().size(); i++) {
				if (curObject.getAttributes().get(i).get("name").asText().equals("x1"))
					startX = (int) ((Double.parseDouble(curObject.getAttributes().get(i).get("value").asText())
							* Mobile_width) / 100);
				if (curObject.getAttributes().get(i).get("name").asText().equals("y1"))
					startY = (int) (Math.round(
							Double.parseDouble(curObject.getAttributes().get(i).get("value").asText()) * Mobile_height)
							/ 100);
				if (curObject.getAttributes().get(i).get("name").asText().equals("x2"))
					endX = (int) (Math.round(
							Double.parseDouble(curObject.getAttributes().get(i).get("value").asText()) * Mobile_width)
							/ 100);
				if (curObject.getAttributes().get(i).get("name").asText().equals("y2"))
					endY = (int) (Math.round(
							Double.parseDouble(curObject.getAttributes().get(i).get("value").asText()) * Mobile_height)
							/ 100);
				if (curObject.getAttributes().get(i).get("name").asText().equals("timeout"))
					to = (int) Math.round(Double.parseDouble(curObject.getAttributes().get(i).get("value").asText()));

			}
			if (Mobile_height != -1 && Mobile_width != -1 && startX != -1 && startY != -1 && endX != -1 && endY != -1)
				return swipe(startX, startY, endX, endY, to);
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to swipeevent :", null, e.toString(), "error");
			return false;
		}
	}

	public boolean swipe(int startX, int startY, int endX, int endY, int timeout) {
		try {

			iosActs Actions = new iosActs();
			List<iosact> Act = new ArrayList<iosact>();
			Act.add(new iosact("press", new Options(startX, startY)));
			Act.add(new iosact("wait", new Options(timeout)));
			Act.add(new iosact("moveTo", new Options(endX, endY)));
			Act.add(new iosact("release", new Options()));
			Actions.setActions(Act);
			System.out.println(new ObjectMapper().writeValueAsString(Actions));
			HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId
							+ "/wda/touch/perform",
					new ObjectMapper().writeValueAsString(Actions), new HashMap<String, String>());
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return swipe(startX, startY, endX, endY, timeout);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to swipe :", null, e.toString(), "error");
				return false;
			}
		}
	}

	public JSONObject size() {
		try {
			@SuppressWarnings({ "resource", "deprecation" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/window/size");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			size = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return size;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return size();
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed:", null, e.toString(), "error");
				return null;
			}
		}
	}

	public boolean Tap() {
		try {

			JSONObject res = new JSONObject(HttpUtility.sendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/element/"
							+ currentEleement + "/click",
					new JSONObject().put("id", currentEleement).toString(), new HashMap<String, String>()));
			count = 0;
			System.out.println(res);
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return Tap();
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to Tap :", null, e.toString(), "error");
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action enters text", action_displayname = "EnterText", actionname = "EnterText", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean EnterText(String value) {
		try {
			cleartext();
			Sendkey keys = new Sendkey();
			List<String> val = new ArrayList<String>();
			keys.setText(value);
			for (String x : value.split(""))
				val.add(x);
			keys.setValue(val);
			String resp = HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/element/"
							+ currentEleement + "/value",
					new ObjectMapper().writeValueAsString(keys), new HashMap<String, String>());
			JSONObject res = new JSONObject(resp);
			System.out.println(res);
			count = 0;
			return true;
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return EnterText(value);
			} else {
				count = 0;
				logerOBJ.customlogg(userlogger, "Step Result Failed: unable to EnterText :", null, e.toString(),
						"error");
				return false;
			}
		}
	}

	public JSONArray getPackages(String deviceId) {
		try {
			System.out.println(deviceId);
			setDevice(deviceId);
			return new PackageDetail(deviceId).init();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to getPackages :", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	public void setDevice(String deviceID) {
		this.ideviceId = deviceID;
		Thread.currentThread().setName(deviceID);
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action clears text", action_displayname = "cleartext", actionname = "cleartext", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean cleartext() {
		try {
			String resp = HttpUtility.SendPost(
					"http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/" + sessionId + "/element/"
							+ currentEleement + "/clear",
					new ObjectMapper().writeValueAsString(""), new HashMap<String, String>());
			JSONObject res = new JSONObject(resp);
			System.out.println(res);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean startSession(String deivceId) {
		return Idevice.startSession(deivceId);
	}

	private JSONObject size = null;

	public int width() {
		try {
			if (size != null)
				return size.getJSONObject("value").getInt("width");
			else {
				size = size();
				return size.getJSONObject("value").getInt("width");
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return -1;
		}
	}

	public int height() {
		try {
			if (size != null)
				return size.getJSONObject("value").getInt("height");
			else {
				size = size();
				return size.getJSONObject("value").getInt("height");
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed ", null, e.toString(), "error");
			e.printStackTrace();
			return -1;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header) {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to CallMethod :", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: unable to setcustomClass :", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}
	}

	private Integer maxCount = 90000;

	public Boolean uniqueElement(Object obj) {
		try {
			ArrayList<Future<Boolean>> taskList = new ArrayList<>();
			Boolean flag = false;
			String objecttype = null;
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					objecttype = "unique";
					flag = true;
					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						return until("xpath", attr.get("value").asText());

				}
			}
			Boolean s = false;
			if (!flag) {
				objecttype = "notunique";
				maxtime = 1;
				Instant start = Instant.now();
				executor.g().restart();
				parallelsearch(obj, taskList, objecttype);
				s = killtask(taskList);
				while (s == false) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
					if (timeelapsed < maxCount) {
						taskList.clear();
						executor.g().parallelobject.shutdownNow();
						executor.g().restart();
						parallelsearch(obj, taskList, objecttype);
						s = killtask(taskList);
					} else {
						return false;
					}
				}
			}
			maxtime = 12;
			return s;
		} catch (Exception e) {
			maxtime = 12;
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}
	public boolean uniqueElement2(Object obj) {
		try {
			ArrayList<Future<Boolean>> taskList = new ArrayList<>();
			Boolean flag = false;
			String objecttype = null;
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					objecttype = "unique";
					flag = true;
					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						return untilnocheck("xpath", attr.get("value").asText());

				}
			}
			Boolean s = false;
			if (!flag) {
				objecttype = "notunique";
				maxtime = 1;
				Instant start = Instant.now();
				executor.g().restart();
				parallelsearch(obj, taskList, objecttype);
				s = killtask(taskList);
				while (s == false) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
					if (timeelapsed < maxCount) {
						taskList.clear();
						executor.g().parallelobject.shutdownNow();
						executor.g().restart();
						parallelsearch(obj, taskList, objecttype);
						s = killtask(taskList);
					} else {
						return false;
					}
				}
			}
			maxtime = 12;
			return s;
		} catch (Exception e) {
			maxtime = 12;
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean parallelsearch(Object obj, ArrayList<Future<Boolean>> taskList, String objecttype)
			throws InterruptedException {
		taskList.removeAll(taskList);
		for (String idname : ididame) {
			JsonNode attr = InitializeDependence.findattr(obj.getAttributes(), idname);
			if (attr != null) {
				if (attr.get("name").asText().toLowerCase().equals("xpath")) {
					taskList.add((Future<Boolean>) executor.g().parallelobject().submit(() -> {
						return until("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbyname")) {
					taskList.add((Future<Boolean>) executor.g().parallelobject().submit(() -> {
						return until("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbylabel")) {
					taskList.add((Future<Boolean>) executor.g().parallelobject().submit(() -> {
						return until("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbyvalue")) {
					taskList.add((Future<Boolean>) executor.g().parallelobject().submit(() -> {
						return until("xpath", attr.get("value").asText());
					}));
				}
			}
		}
		return null;
	}

	public Boolean killtask(ArrayList<Future<Boolean>> taskList)
			throws InterruptedException, ExecutionException, TimeoutException {
		Boolean s = false;
		while (!executor.g().parallelobject().isShutdown() && taskList.size() != 0) {
			for (int i = 0; i < taskList.size(); i++) {
				if (!taskList.get(i).isDone()) {
					continue;
				} else {
					s = taskList.get(i).get();
					if (s) {
						executor.g().parallelobject().shutdownNow();
						taskList.removeAll(taskList);
						break;
					} else {
						taskList.remove(i);
					}
				}
			}
// break;
		}
		return s;
	}
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls down randomly", action_displayname = "scrolldown", actionname = "scrolldown", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolldown() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "x");
			if (attribute != null) {
				return scrolldown((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrolldownrandom();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls down randomly", action_displayname = "scrolldownrandom", actionname = "scrolldownrandom", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolldownrandom() {
		int Startpoint = (int) (size.getJSONObject("value").getInt("height") * 0.5);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("height") * 0.2);
		try {
			return swipe((int) (size.getJSONObject("value").getInt("width") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("width") * 0.5), scrollEnd, 2000);
		} catch (Exception e) {
			return swipe((int) (size.getJSONObject("value").getInt("width") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("width") * 0.5), scrollEnd, 2000);
		}

	}

	@SuppressWarnings("rawtypes")
	public Boolean scrolldown(int xCoornidates) {
		int Startpoint = (int) (size.getJSONObject("value").getInt("height") * 0.5);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("height") * 0.2);
		xCoornidates = (int) ((xCoornidates * size.getJSONObject("value").getInt("width")) / 100);
		try {
			return swipe(xCoornidates, Startpoint, xCoornidates, scrollEnd, 2000);
		} catch (Exception e) {
			return swipe(xCoornidates, Startpoint, xCoornidates, scrollEnd, 2000);
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls up randomly", action_displayname = "scrollup", actionname = "scrollup", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrollup() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "x");
			if (attribute != null) {
				return scrollup((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrolluprandom();

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollup(int xCoordinates) {
		int Startpoint = (int) (size.getJSONObject("value").getInt("height") * 0.2);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("height") * 0.5);
		xCoordinates = (int) ((xCoordinates * size.getJSONObject("value").getInt("width")) / 100);
		try {
			return swipe(xCoordinates, Startpoint, xCoordinates, scrollEnd, 2000);
		} catch (Exception e) {
			return swipe(xCoordinates, Startpoint, xCoordinates, scrollEnd, 2000);
		}

	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls up randomly and finds the element", action_displayname = "scrolluprandom", actionname = "scrolluprandom", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolluprandom() {
		int Startpoint = (int) (size.getJSONObject("value").getInt("height") * 0.2);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("height") * 0.5);
		try {
			return swipe((int) (size.getJSONObject("value").getInt("width") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("width") * 0.5), scrollEnd, 2000);
		} catch (Exception e) {
			return swipe((int) (size.getJSONObject("value").getInt("width") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("width") * 0.5), scrollEnd, 2000);
		}

	}

	public boolean scrollTillElementVertical(String identifierName, String value, boolean direction, int xCoordinates) {
		try {
			maxtime = 5;
// GlobalDetail.maXTimeout = 1;
			JSONArray Element = FnElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.length() == 0) {
				count++;
				if (direction)
					scrolldown(xCoordinates);
				else
					scrollup(xCoordinates);
				Element = FnElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.length() > 0 && Element.length() == 1) {
// GlobalDetail.maXTimeout = 90;
				maxtime = 12;
				return true;
			} else
				return false;
		} catch (Exception e) {
			return scrollTillElementVertical(identifierName, value, direction, xCoordinates);
		}

	}

	public boolean scrollTillElementVertical(String identifierName, String value, boolean direction) {
		try {
			maxtime = 5;
			JSONArray Element = FnElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.length() == 0) {
				count++;
				if (direction)
					scrolldown();
				else
					scrollup();
				Element = FnElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.length() > 0 && Element.length() == 1) {
				maxtime = 12;
				return true;
			} else
				return false;
		} catch (Exception e) {
			return scrollTillElementVertical(identifierName, value, direction);
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls vertically up and finds the element", action_displayname = "scrolltillelementverticalup", actionname = "scrolltillelementverticalup", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementverticalup() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curObject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), false,
						(int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(),
						false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}
	
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls vertically down and finds the element", action_displayname = "scrolltillelementverticaldown", actionname = "scrolltillelementverticaldown", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementverticaldown() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curObject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), true,
						(int) Math.round(attribute2.get("value").asDouble()));

			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), true);
			} else
				return false;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls to the left", action_displayname = "scrollLeft", actionname = "scrollLeft", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrollLeft() {
		int Startpoint = (int) (size.getJSONObject("value").getInt("width") * 0.5);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("width") * 0.2);
		try {
			return swipe((int) (size.getJSONObject("value").getInt("height") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("height") * 0.5), scrollEnd, 2000);
		} catch (Exception e) {
			return swipe((int) (size.getJSONObject("value").getInt("height") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("height") * 0.5), scrollEnd, 2000);
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls to the right", action_displayname = "scrollright", actionname = "scrollright", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrollright() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "y");
			if (attribute != null) {
				return scrollRight((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollRight();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrollleft() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "x");
			if (attribute != null) {
				return scrollLeft((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollLeft();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollLeft(int Ycoordinate) {
		int Startpoint = (int) (size.getJSONObject("value").getInt("width") * 0.5);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("width") * 0.2);
		Ycoordinate = (int) ((Ycoordinate * size.getJSONObject("value").getInt("height")) / 100);
		try {
			return swipe(Ycoordinate, Startpoint, Ycoordinate, scrollEnd, 2000);
		} catch (Exception e) {
			return swipe(Startpoint, Ycoordinate, scrollEnd, Ycoordinate, 2000);
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollRight() {
		int Startpoint = (int) (size.getJSONObject("value").getInt("width") * 0.2);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("width") * 0.5);
		try {
			return swipe((int) (size.getJSONObject("value").getInt("height") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("height") * 0.5), scrollEnd, 2000);
		} catch (Exception e) {
			return swipe((int) (size.getJSONObject("value").getInt("height") * 0.5), Startpoint,
					(int) (size.getJSONObject("value").getInt("height") * 0.5), scrollEnd, 2000);
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollRight(int Ycoordinate)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		int Startpoint = (int) (size.getJSONObject("value").getInt("width") * 0.2);
		int scrollEnd = (int) (size.getJSONObject("value").getInt("width") * 0.5);
		Ycoordinate = (int) ((Ycoordinate * size.getJSONObject("value").getInt("height")) / 100);
		try {
			return swipe(Ycoordinate, Startpoint, Ycoordinate, scrollEnd, 2000);
		} catch (Exception e) {
			return swipe(Startpoint, Ycoordinate, scrollEnd, Ycoordinate, 2000);
		}
	}

	public boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction,
			int Ycoordinate) {
		try {
			maxtime = 5;
// GlobalDetail.maXTimeout = 1;
			JSONArray Element = FnElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.length() == 0) {
				count++;
				if (direction)
					scrollLeft(Ycoordinate);
				else
					scrollRight(Ycoordinate);
				Element = FnElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.length() > 0 && Element.length() == 1) {
				maxtime = 12;
// GlobalDetail.maXTimeout = 90;
				return true;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			e.printStackTrace();
			return scrollTillElementHorizontal(identifierName, value, direction, Ycoordinate);
		}
	}

	public boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction) {
		try {
// GlobalDetail.maXTimeout = 1;
			maxtime = 5;
			JSONArray Element = FnElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.length() == 0) {
				count++;
				if (direction)
					scrollLeft();
				else
					scrollRight();
				Element = FnElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.length() > 0 && Element.length() == 1) {
// GlobalDetail.maXTimeout = 90;
				maxtime = 12;
				return true;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			e.printStackTrace();
			return scrollTillElementHorizontal(identifierName, value, direction);
		}

	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls horizontally to right and finds the element", action_displayname = "scrolltillelementhorizontalright", actionname = "scrolltillelementhorizontalright", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementhorizontalright() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curObject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						false, (int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls horizontally to left and finds the element", action_displayname = "scrolltillelementhorizontalleft", actionname = "scrolltillelementhorizontalleft", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementhorizontalleft() {
		try {
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curObject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						true, (int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						true);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	public JSONObject getbounds() {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/attribute/rect");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			return res.getJSONObject("value");
		} catch (Exception e) {
			if (count < 3) {
				count++;
				if (!ideviceMaganger.devices.get(ideviceId).has("simulator")) {
					GlobalDetail.Wdaproxy.remove(ideviceId);
					GlobalDetail.Wdascreen.remove(ideviceId);
					InitializeDependence.xctestrun.remove(ideviceId);
				}
				Idevice.startSession(ideviceId);
				sessionId = GlobalDetail.iosSession.get(ideviceId);
				return getbounds();
			} else {
				count = 0;
				return null;
			}
		}
	}

	public Boolean tapbycoordinate() throws NumberFormatException, JSONException, Exception {
		try {
			JsonNode start_bound;
			JSONObject res;
			String bound;
			int x, y;
			Thread.sleep(1000);
			attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			if (FindElements("xpath", attribute.get("value").asText())) {
				res = getbounds();
				x = res.getInt("x") + (res.getInt("width") / 2);
				y = res.getInt("y") + (res.getInt("height") / 2);

			} else {
				start_bound = InitializeDependence.findattr(curObject.getAttributes(), "bounds");
				bound = start_bound.get("value").toString().replace("][", ",");
				bound = bound.replace("[", "").replace("]", "");
				bound = bound.replace("\"", "");
				String[] bounds = bound.split(",");
				x = (Integer.parseInt(bounds[0]) + Integer.parseInt(bounds[2])) / 2;
				y = (Integer.parseInt(bounds[1]) + Integer.parseInt(bounds[3])) / 2;
			}
			return Tap(x, y);
		} catch (InterruptedException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	@Sqabind(object_template="Sqa Mobile",action_description = "This action scrolls and finds the element", action_displayname = "scrolltillelementfound", actionname = "scrolltillelementfound", paraname_equals_curobj = "false", paramnames = { "value" }, paramtypes = { "String" })
	public Boolean scrolltillelementfound(String value) {
		JsonNode start_bound;
		String[] bounds=null;
		JSONObject bound;
		try {
		attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
		if(FindElements("xpath",attribute.get("value").asText())) {
			bound=getbounds();
			String[] tmp = {String.valueOf(bound.getInt("x")),String.valueOf(bound.getInt("y")),String.valueOf(bound.getInt("x")+bound.getInt("width")),String.valueOf(bound.getInt("y")+bound.getInt("height"))};
			bounds=tmp;
		}else {
			start_bound = InitializeDependence.findattr(curObject.getAttributes(), "bounds");
			bounds= start_bound.get("value").toString().replace("][", ",").replace("[", "").replace("]", "").replace("\"", "").split(",");
		}
		int startpoint = (Integer.parseInt(bounds[1])+Integer.parseInt(bounds[3]))/2;
		int scrollEnd = startpoint - 50;
		Boolean found = false;
			while ((timeout++ < maxtime)) {
				Thread.sleep(1000);
				if (FindElements("xpath", value) && isdisplayed()) {
					found = true;
					break;
				} else {
					int xcoordinate = (Integer.parseInt(bounds[0])+Integer.parseInt(bounds[2]))/2;
//					xcoordinate = (int) ((xcoordinate * size.getJSONObject("value").getInt("width")) / 100);
					swipe(xcoordinate, startpoint, xcoordinate, scrollEnd, 2000);
				}
			}
			timeout = 0;
			if (!found)
				return false;
			else {
				logger.info("Element found:{}", this.value = value);
				return true;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action performs a click using ocr", action_displayname = "click_using_ocr", actionname = "click_using_ocr", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean click_using_ocr() {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curObject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curObject.getAttributes(), "height");
			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
// req.put("device_recorded",);
				req = HttpUtility.sendPost("http://172.104.183.14:32770/image_rec_forms", req.toString(), headers);

				Boolean status = req.getBoolean("status");
// Boolean text_present = req.getBoolean("text present");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					return Tap(x, y);
				}

				else {
					return false;
				}

			} else {
				if (uniqueElement(curObject)) {
					logger.info("Executed Click on :{} ", value);
					return Tap();
				} else {
					logger.info("Failed to Execute Click on :{} ,No Element Found", value);
					throw new Exception("Unable to click");
				}
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action enters text using ocr", action_displayname = "entertext_using_ocr", actionname = "entertext_using_ocr", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public Boolean entertext_using_ocr(String value) {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curObject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curObject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curObject.getAttributes(), "height");

			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(ideviceId)));
				req.put("image2", attribute.get("value").asText());
				req.put("width", width());
				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/image_rec_forms", req.toString(), headers);
				}
				Boolean status = req.getBoolean("status");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());
// int x = req.getInt("rt_tp_x");
// int y = req.getInt("rt_tp_y");

					int x_cen = Integer.parseInt(req.getJSONObject("center").get("x").toString());
					int y_cen = Integer.parseInt(req.getJSONObject("center").get("y").toString());

					if (width_recorded == null || height_recorded == null) {
						Tap(x + 20, y + 20);
					}

					else {
						Tap(x_cen, y_cen);
						Thread.sleep(2000);
					}
					return EnterText(value);
				} else {
					return false;
				}

			} else {
				if (uniqueElement(curObject)) {
					return EnterText(value);
				} else
					throw new Exception("Unable to Entertext");
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : ", null, e.toString(), "error");
			return false;
		}

	}


	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the given second parameter is greater than the first parameter", action_displayname = "dataincrease", actionname = "dataincrease", paraname_equals_curobj = "false", paramnames = { "ele1","ele2" }, paramtypes = { "String","String" })
	public boolean dataincrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 > item1) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the given second parameter is lesser than the first parameter", action_displayname = "datadecrease", actionname = "datadecrease", paraname_equals_curobj = "false", paramnames = { "ele1","ele2" }, paramtypes = { "String","String" })
	public boolean datadecrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 < item1) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "datesequence", actionname = "datesequence", paraname_equals_curobj = "false", paramnames = { "xpath","no","month" }, paramtypes = { "String","String","String" })
	public boolean datesequence(String xpath, String no, String month) {

		try {
			boolean flag = false;
			AndroidElement element = null;
			String xpath1;
			String xpath2;
			String date1 = "", date2 = "";

			for (int i = 0; i < Integer.parseInt(no) - 1; i++) {

				getxml();

				if (i == 0) {
					xpath1 = "(//XCUIElementTypeStaticText[contains(@label,'Oct')])[1]";
					xpath2 = "(//XCUIElementTypeStaticText[contains(@label,'Oct')])[2]";
				} else {
					xpath1 = "(//XCUIElementTypeStaticText[contains(@label,'Oct')])[2]";
					xpath2 = "(//XCUIElementTypeStaticText[contains(@label,'Oct')])[3]";
				}

				if (FindElement("xpath", xpath1)) {
					date1 = getattrvalue("label");
				}

				if (FindElement("xpath", xpath2)) {
					date2 = getattrvalue("label");
				}

				date1.replace(",", "");
				date2.replace(",", "");
				int check = 0;

				String[] date_1 = date1.split(" ");
				String[] date_2 = date2.split(" ");

				if (date_1[3].equals("PM")) {

					String[] splitter = date_1[2].split(":");
					check = 12 + Integer.parseInt(splitter[0]);
					System.out.println(check);
					date_1[2] = String.valueOf(check).concat(":").concat(splitter[1]);
					date1 = date_1[0].concat(" ".concat(date_1[1]).concat(" ").concat(date_1[2]));
				}

				else {
					date1 = date_1[0].concat(" ".concat(date_1[1]).concat(" ").concat(date_1[2]));
				}

				if (date_2[3].equals("PM")) {

					String[] splitter = date_2[2].split(":");
					check = 12 + Integer.parseInt(splitter[0]);
					System.out.println(check);
					date_2[2] = String.valueOf(check).concat(":").concat(splitter[1]);
					date2 = date_2[0].concat(" ".concat(date_2[1]).concat(" ").concat(date_2[2]));
				} else {
					date2 = date_2[0].concat(" ".concat(date_2[1]).concat(" ").concat(date_2[2]));
				}

				if ((date1.contains(month) && date2.contains(month))
						|| (date1.contains(month) && date2.contains(month))) {

					if ((Float.parseFloat(date_1[2].replace(":", ".")) > Float.parseFloat(date_2[2].replace(":", ".")))

							|| (Float.parseFloat(date_1[2].replace(":", ".")) == Float
									.parseFloat(date_2[2].replace(":", ".")))) {
						flag = true;

					}

				}

				else if (date1.contains("Today") && date2.contains("Yesterday")) {
					flag = true;
				}

				else {
					flag = false;
				}

//				element = FindElement("xpath",xpath2);

				JSONObject res = getbounds();
				int xcoordinate = res.getInt("x") + (res.getInt("width") / 2);
				int startpoint = res.getInt("y") + (res.getInt("height") / 2);

//				int startpoint = Integer.parseInt(getattrvalue("y"));
//				int xcoordinate = Integer.parseInt(getattrvalue("x"));

				int scrollEnd = startpoint;

				swipe(xcoordinate, startpoint + 150, xcoordinate, scrollEnd, 2000);
				Thread.sleep(3000);
			}

			return flag;
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is present", action_displayname = "elementpresent", actionname = "elementpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementpresent() {
		if (uniqueElement(curObject) != null) {
			return true;
		} else
			return false;
	}
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is not present", action_displayname = "elementnotpresent", actionname = "elementnotpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotpresent() {
		if (uniqueElement(curObject) != null) {
			return false;
		} else
			return true;
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is displayed", action_displayname = "elementvisible", actionname = "elementvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementvisible() {
		if (isdisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is not displayed", action_displayname = "elementnotvisible", actionname = "elementnotvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotvisible() {
		if (!isdisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is enabled", action_displayname = "elementenabled", actionname = "elementenabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementenabled() {
		if (isenabled()) {
			return true;
		} else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is disabled", action_displayname = "elementdisabled", actionname = "elementdisabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementdisabled() {
		if (!isenabled()) {
			return true;
		} else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element matches the given value", action_displayname = "elementmatchesvalue", actionname = "elementmatchesvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementmatchesvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				Pattern p = Pattern.compile(expectedValue, Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(getElementText());
				boolean match = m.find();
				if (match) {
					return true;
				} else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element not matches the given value", action_displayname = "elementnotmatchesvalue", actionname = "elementnotmatchesvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementnotmatchesvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				Pattern p = Pattern.compile(expectedValue, Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(getElementText());
				boolean match = m.find();
				if (match) {
					return false;
				} else {
					return true;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is lesser than or equal to the given value", action_displayname = "elementlessthanorequalstovalue", actionname = "elementlessthanorequalstovalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementlessthanorequalstovalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (Integer.parseInt(getElementText()) <= Integer.parseInt(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is lessert than the given value", action_displayname = "elementlessthanvalue", actionname = "elementlessthanvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementlessthanvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (Integer.parseInt(getElementText()) < Integer.parseInt(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is greater than or equal to the given value", action_displayname = "elementgreaterthanorequalstovalue", actionname = "elementgreaterthanorequalstovalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementgreaterthanorequalstovalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (Integer.parseInt(getElementText()) >= Integer.parseInt(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is greater than the given value", action_displayname = "elementgreaterthanvalue", actionname = "elementgreaterthanvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementgreaterthanvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (Integer.parseInt(getElementText()) > Integer.parseInt(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is not equal to the given value", action_displayname = "elementnotequalstovalue", actionname = "elementnotequalstovalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementnotequalstovalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (getElementText().equals(expectedValue)) {
					return false;
				}

				else {
					return true;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element is equal to the given value", action_displayname = "elementequalstovalue", actionname = "elementequalstovalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementequalstovalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (getElementText().equals(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element not contains the given value", action_displayname = "elementnotcontainsvalue", actionname = "elementnotcontainsvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementnotcontainsvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (getElementText().contains(expectedValue)) {
					return false;
				}

				else {
					return true;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action checks whether the IOS element contains the given value", action_displayname = "elementcontainsvalue", actionname = "elementcontainsvalue", paraname_equals_curobj = "false", paramnames = { "expectedValue" }, paramtypes = { "String" })
	public boolean elementcontainsvalue(String expectedValue) {
		try {
			if (uniqueElement(curObject) != null) {
				if (getElementText().contains(expectedValue)) {
					return true;
				}

				else {
					return false;
				}
			}

			else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public String getElementText() {
		try {
			@SuppressWarnings({ "deprecation", "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("http://localhost:" + GlobalDetail.Wdaproxy.get(ideviceId) + "/session/"
					+ sessionId + "/element/" + currentEleement + "/attribute/label");
			getRequest.addHeader("accept", "application/json");
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			count = 0;
			if (res.has("value")) {
				return res.getString("value").toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean installipa(String filepath) {
		try {
			if (((JSONObject) ideviceMaganger.devices.get(this.ideviceId)).has("simulator")) {
				if (grpc.installApp(Integer.parseInt((String) GlobalDetail.GrpcPort.get(this.ideviceId)), filepath)
						.booleanValue())
					return Boolean.valueOf(true);
				return Boolean.valueOf(false);
			}
			if (Idevice.installIPA(this.ideviceId, filepath))
				return Boolean.valueOf(true);
			return Boolean.valueOf(false);
		} catch (IOException e) {
			return Boolean.valueOf(false);
		}
	}
}
