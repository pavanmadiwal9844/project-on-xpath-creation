package com.simplifyqa.method;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.StreamSupport;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.desktop.impl.DesktopImpl;
import com.simplifyqa.execution.Impl.AutomationImpl;
import com.simplifyqa.execution.Impl.DesktopExecutionImpl;
import com.simplifyqa.logger.LoggerClass;

public class DesktopMethod2 implements com.simplifyqa.method.Interface.DesktopMethod {
	
	final static Logger logger = LoggerFactory.getLogger(DesktopMethod2.class);
	private static final  org.apache.log4j.Logger userlogger =  org.apache.log4j.LogManager.getLogger(DesktopMethod2.class);
	LoggerClass logerOBJ = new LoggerClass();
	
	public static Semaphore semCon;
	public static Semaphore semProd;
	private DesktopImpl desktop = new DesktopImpl();
	private Process recorderProcess;
	private Process playbackProcess;
	private Setupexec curExecuiton;
	private Boolean captureScreenshot = false;
	private HashMap<String, String> header;
	private JSONObject status;
	private String[] attrName = { "xpath", "tagName" };
	private String attrlaunch = "ExePath";
	private Integer i = -1;
	private Editorclassloader customMethod = null;
	@SuppressWarnings("rawtypes")
	private Class myClass = null;
	private String classname = "CustomMethodsmainframe";
	public static String launchprocess;

	@Override
	public JSONObject entertext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "entertext", exename);
	}
	
	public JSONObject keyboardentertext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "keyboard entertext", exename);
	}

	@Override
	public JSONObject click(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "click", exename);
	}
	
	public JSONObject mouseclick(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "mouse click", exename);
	}
	
	public JSONObject expand(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "expand", exename);
	}
	public JSONObject select(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "select", exename);
	}
	public JSONObject toggle(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "toggle", exename);
	}
	public JSONObject check(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "check", exename);
	}
	
	public JSONObject waitforelement(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "waitforelement", exename);
	}
	
	@Override
	public JSONObject rightclick(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "rightclick", exename);
	}
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "", action_displayname = "stopplayback", actionname = "stopplayback", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean stopplayback() {
		String str = splitPath(launchprocess);
		desktop.stopPlaybackExe(str);
		return desktop.stopPlaybackExe(playbackProcess);
	}
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "", action_displayname = "splitPath", actionname = "splitPath", paraname_equals_curobj = "false", paramnames = { "pathString"}, paramtypes = { "string" })
	public String splitPath(String pathString) {
	      Path path = Paths.get(pathString);
	      String[] str = StreamSupport.stream(path.spliterator(), false).map(Path::toString)
	                          .toArray(String[]::new);
	      return str[str.length-1];
	  }
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "", action_displayname = "stopreccorder", actionname = "stopreccorder", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean stopreccorder() {
		if(recorderProcess!=null) {
			return desktop.stopRecorderExe(recorderProcess);
		}
		return false;
	}
	
//	public JSONObject launchapplication(String identifierName, String value,String str) {
//		return postwithTD(identifierName, value,str, "launchapplication");
//
//	}

	@Override
	public JSONObject doubleclick(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "doubleclick", exename);
	}

	@Override
	public JSONObject keyboardaction(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "keyboardaction", exename);
	}

	@Override
	public JSONObject waitfortext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "waitfortext", exename);
	}

	@Override
	public JSONObject validatetext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "validatetext", exename);
	}

	@Override
	public JSONObject validatepartialtext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "validatepartialtext", exename);
	}

	@Override
	public JSONObject deletetext(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "deletetext", exename);
	}

	@Override
	public JSONObject findonscreen(String identifierName, String value, String str, String exename) {
		return postwithTD(identifierName, value, str, "findonscreen", exename);
	}

	@Override
	public JSONObject readtextandstore(String identifierName, String value, String exename) {
		return postwithoutTD(identifierName, value, "readtextandstore", exename);
	}

	/*@Override
	 *
	 * public JSONObject getfromruntime(String asText) { // TODO Auto-generated
	 * method stub return null; }
	 */
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "", action_displayname = "stopRecorder", actionname = "stopRecorder", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean stopRecorder() {
		/*String str = splitPath(launchprocess);
		desktop.stopPlaybackExe(str);*/
		return desktop.stopRecorderExe(recorderProcess);
	}

	@Override
	public void setDetail(Setupexec executionDetail, HashMap<String, String> header, Integer itr) {
		// TODO Auto-generated method stub

	}
	

	@Sqabind(object_template="Sqa_Desktop",action_description = "launchApp", action_displayname = "launchApp", actionname = "launchApp", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public JSONObject launchApp() {
		try {
			semProd.acquire();
			startPlayback();
			semProd.release();
			semCon.acquire();
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			logerOBJ.customlogg(userlogger, "Step Result Failed: LaunchApp : ", null, e.getMessage(), "error");
			logger.info(e.getMessage());
			return null;
		}
	}

	@Sqabind(object_template="Sqa_Desktop",action_description = "startPlayback", action_displayname = "startPlayback", actionname = "startPlayback", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean startPlayback() {
		try {
			if (recorderProcess != null) {
				recorderProcess.destroyForcibly();
			}
			playbackProcess = desktop.startPlaybackExe(getSimplifyPlaybackExepath(), getSimplifyPlaypath());
			return true;
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: StartPlayback : ", null, e.getMessage(), "error");
			e.printStackTrace();
			return false;
		}
	}
	
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "startRecorder", action_displayname = "startRecorder", actionname = "startRecorder", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean startRecorder() {
		try {
			if (recorderProcess != null) {
				recorderProcess.destroyForcibly();
			}
			recorderProcess = desktop.startRecorderExe(getSimplifyRecordExepath(), getSimplifyRecordpath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: StartRecorder : ", null, e.getMessage(), "error");
			return false;
		}
	}
	
	private String getSimplifyRecordExepath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] {UtilEnum.DESKTOPFOLDER.value(), UtilEnum.DESKTOPRECORDFOLDER.value(), UtilEnum.DESKTOPRECORD.value() })
				.toString();
	}
	

	private String getSimplifyPlaybackExepath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] {UtilEnum.DESKTOPFOLDER.value(), UtilEnum.DESKTOPPLAYBACKFOLDER.value(), UtilEnum.DESKTOPPLAYBACK.value() })
				.toString();
	}
	
	private String getSimplifyRecordpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] {UtilEnum.DESKTOPFOLDER.value(), UtilEnum.DESKTOPRECORDFOLDER.value() })
				.toString();
	}

	private String getSimplifyPlaypath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] {UtilEnum.DESKTOPFOLDER.value(),  UtilEnum.DESKTOPPLAYBACKFOLDER.value() })
				.toString();
	}

	
	public Boolean startInspector() {
		try {
			recorderProcess = desktop.startRecorderExe(getSimplifyInspectorExepath(), getSimplifyInspecotpath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa_Desktop",action_description = "", action_displayname = "startValidation", actionname = "startValidation", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean startValidation() {
		try {
			recorderProcess = desktop.startRecorderExe(getSimplifyvalidationpath(), getSimplifyValidatepath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private String getSimplifyInspectorExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.DESKTOPFOLDER.value(),
				UtilEnum.DESKTOPOBJECTPICKERFOLDER.value(), UtilEnum.DESKTOPINSPECTOR.value() }).toString();
	}
	
	private String getSimplifyvalidationpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.DESKTOPFOLDER.value(),
				UtilEnum.DESKTOPOBJECTPICKERFOLDER.value(), UtilEnum.DESKTOPVALIDATION.value() }).toString();
	}

	private String getSimplifyInspecotpath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.DESKTOPFOLDER.value(), UtilEnum.DESKTOPOBJECTPICKERFOLDER.value() })
				.toString();
	}
	private String getSimplifyValidatepath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.DESKTOPFOLDER.value(),  UtilEnum.DESKTOPOBJECTPICKERFOLDER.value() })
				.toString();
	}
	
	public void release(JSONObject res) {
		try {
			status = res;
			semCon.release();
			Thread.sleep(200);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.getMessage(), "error");
		}
	}

	public JSONObject click(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	
	public JSONObject mouseclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurMouseclick(steObject.getAttributes(), i);
	}
	
	public JSONObject expand(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	public JSONObject check(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	public JSONObject toggle(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	public JSONObject select(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	
	public JSONObject waitforelement(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return waitforelement(steObject.getAttributes(), i);
	}
	
	public JSONObject rightclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resRightclick(steObject.getAttributes(), i);
	}
	
	public JSONObject validatetext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatetext(steObject.getAttributes(), tcData, i);
	}
	
//	public JSONObject launchapplication(Object steObject, Boolean captureScreenshot) {
//		i = 0;
//		launchApp();
//		this.captureScreenshot = captureScreenshot;
//		return launchapplication(steObject.getAttributes(), i);
//	}
	
	public JSONObject keyboardaction(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return keyboardaction(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject readtextandstore(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return readtextandstore(steObject.getAttributes(), i);
	}
	
	public JSONObject deletetext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return deletetext(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject waitfortext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return waitfortext(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject findonscreen(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return findonscreen(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject validatepartialtext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatepartialtext(steObject.getAttributes(), tcData, i);
	}
	
	private JSONObject findonscreen(List<JsonNode> attributes, String tcData, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = findonscreen(attr.getString("name"), attr.getString("value"), tcData,exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return findonscreen(attributes, tcData, ++i);
			}
		} else
			return findonscreen(attributes,tcData, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: FindOnScreen: ", null, e.toString(), "error");
			return null;
		}

	}
	
	private String exename(List<JsonNode> Attributes, String identifierName) {
		for (int j = 0; j < Attributes.size(); j++) {
			if (Attributes.get(i).get(UtilEnum.NAME.value()).asText().equalsIgnoreCase(identifierName)) {
				return Attributes.get(i).get("value").asText();
			}
		}
		return null;
	}
	
	private JSONObject validatetext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes,"exename");
			if (attr != null) {
				if ((res = validatetext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData, exename))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatetext(attributes, tcData, ++i);
				}
			} else {
				return validatetext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: ValidateText : ", null, e.toString(), "error");
			return null;
		}

	}

	

	private JSONObject waitfortext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = waitfortext(attr.getString("name"), attr.getString("value"), tcData, exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return waitfortext(attributes, tcData, ++i);
			}
		} else
			return waitfortext(attributes, tcData, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: WaitForText :", null, e.toString(), "error");
			return null;
		}

	}
	
	private JSONObject deletetext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = deletetext(attr.getString("name"), attr.getString("value"), tcData, exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return deletetext(attributes, tcData,  ++i);
			}
		} else
			return deletetext(attributes, tcData,  ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: DeleteText : ", null, e.toString(), "error");
			return null;
		}

	}
	
	private JSONObject readtextandstore(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = readtextandstore(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return readtextandstore(attributes, ++i);
			}
		} else
			return readtextandstore(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ReadTextAndStore : ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject keyboardaction(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			if(i>1) {
				return null;
			}
			JSONObject res = null;
//			JsonNode attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
//			if (attr != null) {
			String exename = exename(attributes,"exename");
				if ((res = keyboardaction(null,null, tcData, exename)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					return keyboardaction(attributes, tcData, ++i);
				}
//			} else
//				return keyboardaction(attributes, tcData, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: KeyboardAction : ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	

	private JSONObject waitforelement(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = waitforelement(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return waitforelement(attributes, ++i);
			}
		} else
			return waitforelement(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: WaitForElement : ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurclick(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = click(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return resCurclick(attributes, ++i);
			}
		} else
			return resCurclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurMouseclick(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = mouseclick(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return resCurclick(attributes, ++i);
			}
		} else
			return resCurclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resRightclick(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = rightclick(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return resRightclick(attributes, ++i);
			}
		} else
			return resRightclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject validatepartialtext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes,"exename");
			if (attr != null) {
				if ((res = validatepartialtext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData, exename))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatepartialtext(attributes, tcData, ++i);
				}
			} else {
				return validatepartialtext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ValidatePartialText: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject postwithoutTD(String identifierName, String value, String action,String exename) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			req.put("exename", exename);
			System.out.println(req.toString());
			GlobalDetail.desktopsession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			System.out.println("status"+status.toString());
			return status;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: PostWithOutTD : ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}
	}
	
	private JSONObject postwithTD(String identifierName, String value, String str, String action, String exename) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			req.put("tcData", str);
			req.put("exename", exename);
			GlobalDetail.desktopsession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			System.out.println("status"+status.toString());
			return status;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: PostWithTD : ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
			
		}
	}
	
	
	public JSONObject entertext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurEnterText(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject keyboardentertext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurKeyboardEnterText(steObject.getAttributes(), tcData, i);
	}
	
	public JSONObject launchapplication(Object steObject, String tcData, Boolean captureScreenshot,String object_name) throws InterruptedException {
		i = 0;
		launchApp();
		this.captureScreenshot = captureScreenshot;
		return launchapplication(steObject.getAttributes(), tcData, i,object_name);
	}
	
	public JSONObject doubleclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurdoubleclick(steObject.getAttributes(), i);
	}
	
	private JSONObject resCurdoubleclick(List<JsonNode> attributes, Integer i) {
		try {
		if (i > attrName.length)
			return null;
		JSONObject res = null;
		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
		String exename = exename(attributes,"exename");
		if (attr != null) {
			if ((res = doubleclick(attr.getString("name"), attr.getString("value"), exename)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
				return res;
			} else {
				if(i>=attrName.length-1) {
					return res;
				}
				return resCurdoubleclick(attributes, ++i);
			}
		} else
			return resCurdoubleclick(attributes, ++i);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurEnterText(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes,"exename");
			if (attr != null) {
				if ((res = entertext(attr.getString("name"), attr.getString("value"), tcData, exename)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					return res;
				}
			} else {
				return resCurEnterText(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurKeyboardEnterText(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes,"exename");
			if (attr != null) {
				if ((res = keyboardentertext(attr.getString("name"), attr.getString("value"), tcData, exename)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					return res;
				}
			} else {
				return resCurKeyboardEnterText(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject launchapplication(List<JsonNode> attributes, String tcData, Integer i,String object_name) throws InterruptedException {
//		try {
//		if (i > attrName.length)
//			return null;
//		JSONObject res = null;
//		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrlaunch);
//		String exename = exename(attributes,"exename");
//		if (attr != null) {
//			if ((res = launchapplication("xpath", attr.getString("value"), tcData, exename)).getString("play_back").toLowerCase()
//					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
//				return res;
//			} else {
//				if(i>=attrName.length-1) {
//					return res;
//				}
//				return res;
//			}
//		} else
//			return res;
//		} catch (Exception e) {
//			return null;
//		}
//
		launchprocess = tcData;
			try {
				tcData = tcData.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]", "");
				Process launch = Runtime.getRuntime().exec(new String[] {"cmd","/c",Paths.get(tcData).toAbsolutePath().toString()});
				Thread.sleep(2000);
				return new JSONObject().put("play_back", "passed").put("tcData", tcData).put("captureScreenshot", "");
			} catch (IOException e) {
				return new JSONObject().put("play_back", "failed").put("tcData", tcData).put("captureScreenshot", "");
			}
	}

	@Override
	public JSONObject launchApplication() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean wait(Integer timeout) {
		try {
			Thread.sleep(timeout);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}
	
	public boolean hold(Integer timeout) {
		try {
			Thread.sleep(timeout);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public JSONObject launchapplication(String identifierName, String value) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
