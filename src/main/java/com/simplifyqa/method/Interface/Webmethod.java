package com.simplifyqa.method.Interface;

import java.util.HashMap;
import java.util.Map;

import com.codoid.products.fillo.Recordset;
import com.simplifyqa.DTO.Object;

public interface Webmethod {

	public boolean click(String identifiername, String value);

	public boolean clickusingjs(String identifiername, String value);

	public boolean entertext(String identifiername, String value, String valuetoenter);

	public boolean entertextbyjs(String identifiername, String value, String valuetoenter);

	public boolean arrowup();

	public boolean arrowdown();

	public boolean enterfilepath(String value);

	public boolean clickusingjs();

	public boolean settimeout(long script, long pageout, long implicit);

	public boolean clickifexist(String identifiername, String value);

	public boolean mouseoveranelement();

	public boolean cleartext(String identifiername, String value);

	public boolean selectitemfromdropdown(String selectedvalue);

	public boolean selectitemfromdropdownbyxpath(String selectedvalue);

	public boolean checkifnotchecked(String identifiername, String value);

	public boolean uncheckifchecked(String identifiername, String value);

	public boolean waitforalert();

	public boolean exists(String identifiername, String value);

	public boolean validatetext(String identifiername, String value, String valuetovalidate);

	public boolean validatepartialtext(String identifiername, String value, String valuetovalidate);

	public boolean validatecheck(String identifiername, String value);

	public boolean scrolltoview();

	public boolean entertextandenter(String identifiername, String value, String valuetoenter);

	public boolean enter(String identifiername, String value);

	public boolean scrolltobottom();

	public boolean scrolltotop();

	public boolean mouseoverandclick();

	public boolean entertextandtab(String identifiername, String value, String valuetoenter);

	public boolean fileuploadusingrobot();

	public boolean waituntilelementpresent();

	public boolean selectcheckbox();

	public boolean swipetoelement();

	public boolean click();

	public boolean entertext(String value);

	public boolean entertextbyjs(String value);

	public boolean entertexttab(String valuetoenter);

	public boolean validatetext(String valuetovalidate);

	public boolean validatepartialtext(String valuetovalidate);

	public boolean exists();

	public boolean clickifexist();

	public boolean enter();

	public boolean checkifnotchecked();

	public boolean uncheckifchecked();

	public boolean validateconfirmationtext(String valuetovalidate);

	public boolean chooseokonconfirmation();

	public boolean choosecancelonconfirmation();

	public boolean validatealerttext(String valuetovalidate);

	public boolean alertaccept();

	public boolean validateonprompt(String valuetovalidate);

	public boolean answeronprompt(String valuetoEnter);

	public boolean choosecancelonprompt();

	public boolean contenttext(String valuetoEnter);

	public boolean notexists(String identifiername, String value);

	public boolean notexists();

	public boolean validatecheck();
	
	public boolean draganddropobject(String targetxpathvalue);

	public boolean dragandDrop(String identifiername, String value, String targetxpathvalue);

	public boolean scrollintoview();

	public boolean readtextandstore(String identifiername, String value, String Paraname);

	public boolean readtextandstore(String Paraname);

	public String getUrl();

	public boolean iselementDisplayed(String elementId);

	public boolean dbclick();

	public boolean dbclick(String identifiername, String value);

	public boolean clickusingdynamicxpath(String xpath, String runtimevlau);

	public boolean clickusingjswithdynamicxpath(String xpath, String runtimevlau);

	public boolean exists(String paramvalue);

	public boolean click(String runtimeval);

	public boolean clickusingjs(String runtimevalue);

	public boolean readinstringandstore(String identifiername, String value, String Paraname, String startString,
			String endString);

	public boolean readinstringandstore(String Paraname, String startString, String endString);

	public boolean validateandclick(String valuetovalidate, String clickxpathvalue, String maxtimeout);

	public boolean back(Object obj);

	public boolean forward(Object obj);

	public boolean refresh(Object obj);

	public boolean closewindow(Object obj);

	public boolean switchtowindow();

	public boolean switchtowindow(String vlaue);

	public boolean dynamicexists(String valuetovalidate);

	public boolean entertextandenter(String valuetoenter);

	public boolean switchtoparentframe();

	public boolean switchtoframe(String frameindex);

	public boolean switchtotab(String tabindex);

	public boolean switchtoparenttab();

	public boolean switchtoframe();

	public boolean switchtoparent();

	public boolean launchapplication(String url);

	public boolean clickusingjss();

	public Boolean getfilename(String param);

	public Boolean jsexecutor(String js);

	public boolean closetab(String tab);

	public boolean opennewtabwithurl(String m_strURL) throws Exception;

	public boolean gettablerowcount(String identifiername, String value, String runtimeparam);

	public boolean validatetext(String replacevalue, String valuetovalidate);

}
