package com.simplifyqa.method.Interface;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Object;

public interface AndroidMethod {

	public Boolean launchapp(Object stepObject, JsonNode tcData);
	public Boolean click(Object stepObject);
	public Boolean entertext(Object stepObject, JsonNode tcData);
	
	
}
