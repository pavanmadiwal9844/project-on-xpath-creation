package com.simplifyqa.method.Interface;

import javax.xml.soap.SOAPMessage;

public interface Apimethod {

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime);

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime);

	public boolean callsoapwebservice(String soapEndpointUrl, String soapAction, String myNamespace,
			String myNamespaceURI, String runtimename);

	String soapMessageToString(SOAPMessage message);
}
