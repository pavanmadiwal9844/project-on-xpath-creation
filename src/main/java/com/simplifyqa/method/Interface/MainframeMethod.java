package com.simplifyqa.method.Interface;

import org.json.JSONObject;

import com.simplifyqa.DTO.Object;

public interface MainframeMethod {

//	public Boolean launchapp() ;
	public JSONObject typetext(String identifierName, String value, String str);

	public JSONObject click(String identifierName, String value);

	public JSONObject doubleclick(String identifierName, String value);

	public JSONObject keyboardaction(String identifierName, String value, String str);

	public JSONObject waitfortext(String identifierName, String value, String str);

	public JSONObject validatetext(String identifierName, String value, String str);

	public JSONObject validatepartialtext(String identifierName, String value, String str);

	public JSONObject deletetext(String identifierName, String value);

	public JSONObject findonscreen(String identifierName, String value, String str);

	public JSONObject readtextandstore(String identifierName, String value);

	public JSONObject findelementbyposition(String identifierName, String value);

	public JSONObject findelementbylabel(String identifierName, String value);

	public JSONObject getfromruntime(String identifierName);

	public JSONObject typetext(Object object, String value);

	public JSONObject click(Object object);

	public JSONObject doubleclick(Object object);

	public JSONObject keyboardaction(Object object, String value);

	public JSONObject waitfortext(Object object, String value);

	public JSONObject validatetext(Object object, String value);

	public JSONObject validatepartialtext(Object object, String value);

	public JSONObject deletetext(Object object);

	public JSONObject findonscreen(Object object, String value);

	public JSONObject readtextandstore(Object object);

	public JSONObject findelementbyposition(Object object);

	public JSONObject findelementbylabel(Object object);

	public JSONObject getfromruntime(Object object);

	public boolean wait(Integer timeout);

}
