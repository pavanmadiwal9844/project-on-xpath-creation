package com.simplifyqa.method.Interface;

import java.util.HashMap;

import org.json.JSONObject;

import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;

public interface SapMethod {
	public JSONObject entertext(String identifierName, String value, String str, String exename,String type);

	public JSONObject click(String identifierName, String value, String exename,String type);
	
	public JSONObject keyboardentertext(String identifierName, String value, String str, String exename,String type);
	
	public JSONObject mouseclick(String identifierName, String value, String exename,String type);


	public JSONObject doubleclick(String identifierName, String value, String exename,String type);

	public JSONObject keyboardaction(String identifierName, String value, String str, String exename,String type);

	public JSONObject waitfortext(String identifierName, String value, String str, String exename,String type);

	public JSONObject validatetext(String identifierName, String value, String str, String exename,String type);

	public JSONObject validatepartialtext(String identifierName, String value, String str, String exename,String type);

	public JSONObject deletetext(String identifierName, String value, String exename,String type);

	public JSONObject findonscreen(String identifierName, String value, String str, String exename,String type);

	public JSONObject readtextandstore(String identifierName, String value, String exename,String type);
	
	public JSONObject getfromruntime(String asText);
	
	public void setDetail(Setupexec executionDetail, HashMap<String, String> header, Integer itr);
	
	public JSONObject launchApplication();

	JSONObject launchapplication(String identifierName, String value);

	JSONObject rightclick(String identifierName, String value, String exename,String type);

	boolean wait(Integer timeout);
}
