package com.simplifyqa.method.Interface;

import java.util.HashMap;

import org.json.JSONObject;

import com.simplifyqa.DTO.Setupexec;

public interface DesktopMethod {
	
	public JSONObject entertext(String identifierName, String value, String str, String exename);
	
	public JSONObject keyboardentertext(String identifierName, String value, String str, String exename);

	public JSONObject click(String identifierName, String value, String exename);
	
	public JSONObject mouseclick(String identifierName, String value, String exename);

	public JSONObject doubleclick(String identifierName, String value, String exename);

	public JSONObject keyboardaction(String identifierName, String value, String str, String exename);

	public JSONObject waitfortext(String identifierName, String value, String str, String exename);

	public JSONObject validatetext(String identifierName, String value, String str, String exename);

	public JSONObject validatepartialtext(String identifierName, String value, String str, String exename);

	public JSONObject deletetext(String identifierName, String value, String str, String exename);

	public JSONObject findonscreen(String identifierName, String value, String str, String exename);

	public JSONObject readtextandstore(String identifierName, String value, String exename);
	
	//public JSONObject getfromruntime(String asText);
	
	public JSONObject expand(String identifierName, String value, String exename);
	
	public JSONObject select(String identifierName, String value, String exename) ;
	
	public JSONObject toggle(String identifierName, String value, String exename) ;
	
	public JSONObject check(String identifierName, String value, String exename) ;
	
	public void setDetail(Setupexec executionDetail, HashMap<String, String> header, Integer itr);
	
	public JSONObject launchApplication();

	JSONObject launchapplication(String identifierName, String value);

	JSONObject rightclick(String identifierName, String value, String exename);

	boolean wait(Integer timeout);
	
	boolean hold(Integer timeout);



}
