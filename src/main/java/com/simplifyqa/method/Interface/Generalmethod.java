package com.simplifyqa.method.Interface;

import java.io.IOException;

import com.codoid.products.fillo.Recordset;

public interface Generalmethod {

	public Boolean getfromruntime(String identifiername);

	public boolean storeruntime(String paraname, String paravalue);

	public Boolean hold(String timeOut);

	public boolean randomalphanumeric(String count, String prefix, String suffix, String parameter);

	public boolean randomnumeric(String count, String prefix, String suffix, String parameter);

	public boolean combineruntime(String... param1);

	public String readjson(String param1, String key, String index);

	public Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore);

	public String getRandomNumber();

	public boolean generaterandomstring(String runtimeParam, String prefix, String strLenght);

	public boolean validateparameter(String paramName, String value);

	public boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore);

	public boolean replaceandstore(String param1, String param2, String param3, String param4);

	public boolean splitandgetvalue(String param, String delimt, String index, String param1);

	public boolean concatandstore(String... param1);

	public boolean comparejsondoc(String path1, String path2);

	public boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam);

	public boolean storefromjson(String Json, String key, String runtime);

	public boolean validatefromjson(String Json, String key, String ExpectedValue);

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime);

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime);

	public boolean generateexcelid(String filepath, String columnname);

	public boolean generateexcelstring(String filepath, String columnname);

	public boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam);

	public Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval);

	public String getruntimevalueifexist(String identifiername);

	public boolean verifyadditionofvalues(String... str);

	public boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam);

	public String convertxmltojson(String xmlfile);

	public boolean validatefromxml(String xmlfile, String key, String expectedvalue);

	public boolean storexmlvalue(String xmlfile, String key, String runtimeparam);

	public boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam);

	public boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,
			String runtimeparam);

	public boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam);

	public boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam);

	public boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam);

	public boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,
			String replacevalue);

	public boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname, String replacevalue);

	public boolean getdate(String UDate, String DateFormat, String runtimeparam);

	public boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam);

	public boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue) throws IOException;

	public boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException;

	public boolean minwait();

	public boolean medwait();

	public boolean maxwait();

	public boolean comparepartialstring(String Basestring, String valuetovalidate);

	public boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar);

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String formdatatype, String formdata, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime);
	
	public boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime);

}
