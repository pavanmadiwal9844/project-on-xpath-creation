package com.simplifyqa.method;

import static java.util.stream.Collectors.joining;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.swing.JFrame;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.SystemUtils;
import org.apache.tools.ant.property.GetProperty;
import org.ini4j.Ini;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.ElementClickInterceptedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.KeyBoardActions;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.driver.WebDriver;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.Interface.Generalmethod;
import com.simplifyqa.method.Interface.Webmethod;
import com.simplifyqa.method.GeneralMethod;
import com.simplifyqa.web.DTO.Webcapabilities;
import com.simplifyqa.web.implementation.executor;

public class WebMethod implements Webmethod {
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	LoggerClass logerOBJ = new LoggerClass();

	public WebDriver driver = null;
	public Webcapabilities capabilities;
	public String[] ididame = { "css", "xpathIdRelative", "xpathLink", "xpathPosition", "xpathInnerText",
			"xpathTextContent", "xpathAttributes", "xpath", "class", "cssIndex", "id", "name", "xpathClass",
			"xpathText", "xpathName", "XpathNoattr", "linkText" };
	public String framelocation = null;
	public String exepectedValue = null;
	public String Actualvalue = null;
	private static final Logger logger = LoggerFactory.getLogger(WebMethod.class);
	private Editorclassloader customMethod = null;
	@SuppressWarnings("rawtypes")
	private Class myClass = null;
	private String classname = "CustomMethodsweb";
	public Setupexec curExecution = null;
	public String itemselected = null;

	public String getItemselected() {
		return itemselected;
	}

	public void setItemselected(String itemselected) {
		this.itemselected = itemselected;
	}

	public String getActualvalue() {
		return Actualvalue;
	}

	public void setActualvalue(String actualvalue) {
		this.Actualvalue = actualvalue;
	}

	public HashMap<String, String> header = new HashMap<String, String>();
	public static Integer currentSeq = -1;
	private JFrame frame = null;
	public Object curObject = null;
	public String brname = null;
	private boolean stWebExe = false;
	public String browser = null;
	private Map<String, String> tdMap = new HashMap<String, String>();
	private static java.sql.Connection con = null;
	Integer maxtime = 90000;

	public Map<String, String> getTdMap() {
		if (!tdMap.isEmpty())
			return tdMap;
		else {
			if (!GlobalDetail.curStepTd.isEmpty()) {
				if (GlobalDetail.curStepTd.containsKey(Thread.currentThread().getName()))
					return GlobalDetail.curStepTd.get(Thread.currentThread().getName());
				else
					return new HashMap<String, String>();
			} else
				return new HashMap<String, String>();
		}
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public boolean isStWebExe() {
		return stWebExe;
	}

	public void setStWebExe(boolean stWebExe) {
		this.stWebExe = stWebExe;
	}

	public String getExepectedValue() {
		return exepectedValue;
	}

	public void setExepectedValue(String exepectedValue) {
		this.exepectedValue = exepectedValue;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		
	}

	public boolean webCapabilities(String browserName, String platformName) {
		try {
			InitializeDependence.webDriver.get(Thread.currentThread().getName()).browser = browserName;
			browser = browserName;
			capabilities = new Webcapabilities();
			JSONObject alwaysmatch, cap, firstMatch;
			JSONArray firstMatchArray = new JSONArray();
			cap = new JSONObject();
			alwaysmatch = new JSONObject();
			firstMatch = new JSONObject();

			switch (browserName.toLowerCase().trim()) {
			case "chrome":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");
				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
					firstMatch.put("goog:chromeOptions", new JSONObject()
							.put("args", new JSONArray().put("start-maximized").put("--ignore-certificate-errors").put("--window-size=1920,1200")).put("prefs", preferences));
				} else {
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args", new JSONArray().put("start-maximized").put("--ignore-certificate-errors").put("--window-size=1920,1200")));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "chrome-incognito":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");
				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args", new JSONArray().put("start-maximized").put("--incognito").put("--ignore-certificate-errors").put("--window-size=1920,1200"))
									.put("prefs", preferences));
				} else {
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args", new JSONArray().put("start-maximized").put("--incognito").put("--ignore-certificate-errors").put("--window-size=1920,1200")));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "chrome-headless":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");
				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args",
									new JSONArray().put("start-maximized").put("--headless").put("--disable-gpu").put("--ignore-certificate-errors").put("--window-size=1920,1200"))
									.put("prefs", preferences));
				} else {
					firstMatch.put("goog:chromeOptions", new JSONObject().put("args",
							new JSONArray().put("start-maximized").put("--headless").put("--disable-gpu").put("--ignore-certificate-errors").put("--window-size=1920,1200")));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "firefox":
				alwaysmatch.put("browserName", "firefox");
				if (SystemUtils.IS_OS_WINDOWS) {
					firstMatch.put("moz:firefoxOptions",
							new JSONObject().put("binary", "C:\\Program Files\\Mozilla Firefox\\firefox.exe"));
				} else {
					firstMatch.put("moz:firefoxOptions",
							new JSONObject().put("binary", "/Applications/Firefox.app/Contents/MacOS/firefox-bin"));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);
				break;
			case "ie":
				alwaysmatch.put("browserName", "internet explorer");
				alwaysmatch.put("pageLoadStrategy", "eager");
				alwaysmatch.put("timeouts",
						new JSONObject().put("implicit", 0).put("pageLoad", 1000).put("script", 1000));
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);
				cap.put("ignoreProtectedModeSettings", true);
				cap.put("nativeEvents", true);

				cap.put("javascriptEnabled", true);
				break;
			case "edge":
				cap.put("browserName", "edge");
				cap.put("platformName", "windows");
				break;
			case "safari":
				alwaysmatch.put("browserName", "Safari");
				alwaysmatch.put("platformName", "Mac");
				cap.put("alwaysMatch", alwaysmatch);

			default:
				break;
			}
			capabilities.setCapabilities(cap);
			driver = new WebDriver(capabilities);
			if (browserName.equals("edge"))
				GlobalDetail.webDriver.put(Thread.currentThread().getName(), driver);
			else
				GlobalDetail.webDriver.put(Thread.currentThread().getName(), driver);

			InitializeDependence.webDriver.get(Thread.currentThread().getName()).capabilities = capabilities;
			InitializeDependence.webDriver.get(Thread.currentThread().getName()).driver = driver;

			logger.info(capabilities.toString());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// public WebDriver getDriver() {
	// if (driver != null)
	// return driver;
	// else
	// return GlobalDetail.webDriver.get(Thread.currentThread().getName());
	// }

	public WebDriver getDriver() {
		if (driver != null)
			return driver;
		else {
			System.out.println(Thread.currentThread().getName());
			driver = InitializeDependence.webDriver.get(Thread.currentThread().getName()).driver;
			browser = InitializeDependence.webDriver.get(Thread.currentThread().getName()).browser;
			brname = InitializeDependence.webDriver.get(Thread.currentThread().getName()).brname;
			curObject = InitializeDependence.webDriver.get(Thread.currentThread().getName()).curObject;
			curExecution = InitializeDependence.webDriver.get(Thread.currentThread().getName()).curExecution;
			framelocation = InitializeDependence.webDriver.get(Thread.currentThread().getName()).framelocation;
			capabilities = InitializeDependence.webDriver.get(Thread.currentThread().getName()).capabilities;
			
			

			return InitializeDependence.webDriver.get(Thread.currentThread().getName()).driver;
		}
//			return GlobalDetail.webDriver.get(Thread.currentThread().getName());
	}

	public boolean deleteSession() {
		try {
			return getDriver().getWbDriver().deleteSession();
		} catch (Exception e) {
			return true;// TODO: handle exception
		}
	}

	public boolean closeDriver() {
		try {
			return getDriver().closeDriver();
		} catch (Exception e) {
			return true;
		}
	}

	public JSONObject getTimeout() {
		return getDriver().getActionManger().getTimeout();
	}

	public boolean setTimeout(long implicittimeout, long pageloadtimeout, long scripttimeout) {
		return getDriver().getActionManger().setTimeout(scripttimeout, pageloadtimeout, implicittimeout);
	}

	public Boolean launchUrl(String url) {
		return getDriver().getActionManger().launchUrl(url);
	}

	public String geturl() {
		return getDriver().getActionManger().getUrl();
	}

	public boolean back() {
		return getDriver().getActionManger().back();
	}

	public boolean forward() {
		return getDriver().getActionManger().forward();
	}

	public boolean refresh() {
		return getDriver().getActionManger().refresh();
	}

	public String getTitle() {
		return getDriver().getActionManger().getTitle();
	}

	public String getWindowhandle() {
		return getDriver().getActionManger().getWindowhandle();
	}

	public boolean closeWindow() {
		return getDriver().getActionManger().closeWindow();
	}

	public boolean switchtoWindow(String handle) {
		return getDriver().getActionManger().switchtoWindow(handle);
	}

	public JSONArray getWindowhandles() {
		return getDriver().getActionManger().getWindowhandles();
	}

	@Override
	public boolean switchtoframe(String id) {
		return getDriver().getActionManger().switchtoFrame(Integer.parseInt(id));
	}

	@Override
	public boolean switchtoparentframe() {
		return getDriver().getActionManger().switchtoParentframe();
	}

	public JSONObject getWindowrect() {
		return getDriver().getActionManger().getWindowrect();
	}

	public boolean setWindowrect(int height, int width, int x, int y) {
		return getDriver().getActionManger().setWindowrect(height, width, x, y);
	}

	public boolean maximizeScreen() {
		return getDriver().getActionManger().maximizeScreen();
	}

	public boolean minimizeScreen() {
		return getDriver().getActionManger().minimizeScreen();
	}

	public boolean fullScreen() {
		return getDriver().getActionManger().fullsScreen();
	}

	public JSONObject getActiveelement() {
		return getDriver().getActionManger().getActiveelement();
	}

	public String findElement(String using, String value) {
		return getDriver().getActionManger().findElement(using, value);
	}

	public JSONArray findElements(String using, String value) {
		return getDriver().getActionManger().findElements(using, value);
	}

	public String findelementfromElement(String using, String value, String elementId) {
		return getDriver().getActionManger().findelementfromElement(using, value, elementId);
	}

	public JSONObject findelementsfromElement(String using, String value, String elementId) {
		return getDriver().getActionManger().findelementsfromElement(using, value, elementId);
	}

	public boolean isElementselected(String elementid) {
		return getDriver().getActionManger().isElementselected(elementid);
	}

	public String getElementattribute(String elementid, String attribute) {
		return getDriver().getActionManger().getElementattribute(elementid, attribute);
	}

	public String getElementproperty(String elementid, String property) {
		return getDriver().getActionManger().getElementproperty(elementid, property);
	}

	public String getElementcssvalue(String elementid, String css) {
		return getDriver().getActionManger().getElementcssvalue(elementid, css);
	}

	public String getElementtext(String elementid) {
		return getDriver().getActionManger().getElementtext(elementid);
	}

	public String getElementtagname(String elementid) {
		return getDriver().getActionManger().getElementtagname(elementid);
	}

	public JSONObject getElementrect(String elementid) {
		return getDriver().getActionManger().getElementrect(elementid);
	}

	public boolean isElementenabled(String elementid) {
		return getDriver().getActionManger().isElementenabled(elementid);
	}

	public String elementClick(String elementId) {
		return getDriver().getActionManger().elementClick(elementId);
	}

	public boolean elementClear(String elementId) {
		return getDriver().getActionManger().elementClear(elementId);
	}

	public boolean elementSendkeys(String elementId, String valuetoEnter) {
		return getDriver().getActionManger().elementSendkeys(elementId, valuetoEnter);
	}

	public String getPagesource() {
		return getDriver().getActionManger().getPagesource();
	}

	public boolean executeScript(String script) {
		return getDriver().getActionManger().executeScript(script);
	}

	public JSONObject executeScript2(String script) {
		return getDriver().getActionManger().executeScript2(script);
	}

	public boolean executeAsyncscript(String script) {
		return getDriver().getActionManger().executeAsyncscript(script);
	}

	public JSONArray getallCookies() {
		return getDriver().getActionManger().getallCookies();
	}

	public JSONObject getnamedCookies(String cookieName) {
		return getDriver().getActionManger().getnamedCookies(cookieName);
	}

	public boolean addCookie(String name, String value, String path, String domain, boolean secure, boolean httpOnly,
			int expiry) {
		return getDriver().getActionManger().addCookie(name, value, path, domain, secure, httpOnly, expiry);
	}

	public boolean deleteCookie(String cookieName) {
		return getDriver().getActionManger().deleteCookie(cookieName);
	}

	public boolean deleteAllcookies() {
		return getDriver().getActionManger().deleteAllcookies();
	}

	public boolean performAction() {
		return getDriver().getActionManger().performAction();
	}

	public boolean releaseAction() {
		return getDriver().getActionManger().releaseAction();
	}

	public boolean dismissAlert() {
		return getDriver().getActionManger().dismissAlert();
	}

	public boolean acceptAlert() {
		return getDriver().getActionManger().acceptAlert();
	}

	public String getAlerttext() {
		return getDriver().getActionManger().getAlerttext();
	}

	public boolean sendAlerttext(String valuetoEnter) {
		return getDriver().getActionManger().sendAlerttext(valuetoEnter);
	}

	public String takeScreenshot() {
		return getDriver().getActionManger().takeScreenshot();
	}

	public String takeElementscreenshot(String elementId) {
		return getDriver().getActionManger().takeElementscreenshot(elementId);
	}

	public String getElementppt(String elementid, String attribute) {
		return getDriver().getActionManger().getElementppt(elementid, attribute);
	}

	public String findElementforloader(String using, String value) {
		return getDriver().getActionManger().findElementforloader(using, value);
	}

	@Sqabind(object_template="sqa_record",action_description = "This is for clicking on web elements", action_displayname = "Click", actionname = "click", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public boolean click() {
		try {
			JSONArray tabs = new JSONArray();
//			Thread.sleep(2000);
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;

				if ((idnamevalue = uniqueElement(curObject)) != null) {
					System.out.println(idnamevalue);
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					return click(identifiername, value);
				} else {
					logerOBJ.customlogg(userlogger, "Step Result Failed: Click cannot be performed, Element not found.",
							null, "", "error");
					return false;
				}
			} else
				return false;
		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action first validates whether the web element is there or not and then clicks if it is there", action_displayname = "validateandclick", actionname = "validateandclick", paraname_equals_curobj = "true", paramnames = { "","","" }, paramtypes = { "String","String","String" })
	public boolean validateandclick(String valuetovalidate, String clickxpathvalue, String maxtimeout) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					return validateandclick(identifiername, value, valuetovalidate, clickxpathvalue, maxtimeout);
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean validateandclick(String identifiername, String value, String valuetovalidate, String clickxpathvalue,
			String maxtimeout) {
		int maxtime = Integer.parseInt(maxtimeout);
		int timetaken = 0;
		while (!validatetext(identifiername, value, valuetovalidate)) {
			if (timetaken <= maxtime) {
				try {
					Thread.sleep(2000);
					timetaken = timetaken + 2000;
					click(identifiername, clickxpathvalue);
				} catch (InterruptedException e) {
					e.printStackTrace();
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action clicks on a web element using the runtime value", action_displayname = "click",actionname = "click", paraname_equals_curobj = "true", paramnames = {""}, paramtypes = {"string"})
	public boolean click(String runtimeval) {
		try {

			if (browser.equals("firefox")) {
				Thread.sleep(100);
			}
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), runtimeval));
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {

					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					value = value.replaceAll("#replace", runtimeval);
					return click(identifiername, value);
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is to enter text into the web element", action_displayname = "entertext", actionname ="entertext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertext(String valuetoenter) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			JSONObject idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return entertext(identifiername, value, valuetoenter);
		} else {
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "inputs text using javascript", action_displayname = "contenttext", actionname = "contenttext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean contenttext(String valuetoEnter) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			JSONObject idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			if (identifiername.toLowerCase().trim().contains("css")) {
				return executeScript("var e=document.querySelector(" + escape(value)
						+ "); e.scrollIntoView(); e.innerText=" + valuetoEnter + ";");
			} else if (identifiername.toLowerCase().trim().contains("xpath")) {
				return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
						+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView();r[0].innerText="
						+ escape(valuetoEnter) + ";r[0].dispatchEvent(new Event('input')),1000;");
			} else if (identifiername.toLowerCase().trim().equals("id")) {
				return executeScript("var e=document.getElementById(" + escape(value)
						+ "); e.scrollIntoView();e.innerText=" + valuetoEnter.toString() + ";");
			} else if (identifiername.toLowerCase().trim().equals("name")) {
				return executeScript(escape("var e =document.getElementsByName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();e[0].innerText=" + valuetoEnter.toString()
						+ ": e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else if (identifiername.toLowerCase().trim().equals("class")) {
				return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();e[0].innerText=" + valuetoEnter.toString()
						+ ": e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This is for keyboard related actions, used for pressing a key on the keyboard.", action_displayname = "keyboardaction", actionname = "keyboardaction", paraname_equals_curobj = "false", paramnames = { "valuetoenter" }, paramtypes = { "String" })
	public boolean keyboardaction(String valuetoenter) {
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return keyboardaction(identifiername, value, valuetoenter);
	}

	@Sqabind(object_template="sqa_record",action_description = "This is for clearing the text in the textbox", action_displayname = "cleartext", actionname = "cleartext", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public boolean cleartext() {
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return cleartext(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is for entering text into a web element and then perform a 'Tab' button press", action_displayname = "entertexttab", actionname = "entertexttab", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertexttab(String valuetoenter) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return entertextandtab(identifiername, value, valuetoenter);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is for entering text into a web element and then a 'Enter' button press", action_displayname = "entertextandenter", actionname = "entertextandenter", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertextandenter(String valuetoenter) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return entertextandenter(identifiername, value, valuetoenter);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is for validating the given text", action_displayname = "validatetext", actionname = "validatetext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatetext(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);

		System.out.println("idnamevalue found :" + idnamevalue);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return validatetext(identifiername, value, valuetovalidate);
	}

	@Override

	public boolean validatetext(String replacevalue, String valuetovalidate) {

		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), replacevalue));
		if (tabs.length() > 1)
			checkTab(curObject, tabs);

		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		value = value.replaceAll("#replace", replacevalue);
		return validatetext(identifiername, value, valuetovalidate);

	}

	@Sqabind(object_template="sqa_record",action_description = "validates the value of a web element. In other words checks whether the value of a web element is equal to the given value.", action_displayname = "validatevalue", actionname = "validatevalue", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatevalue(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return validatetext(identifiername, value, valuetovalidate);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This checks whether a web element exists or not", action_displayname = "exists", actionname = "exists", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public boolean exists() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		if (idnamevalue == null) {
			return false;
		} else {
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return exists(identifiername, value);
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This checks whether a web element exists or not based on the given value of the locator", action_displayname = "exists", actionname = "exists", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean exists(String paramvalue) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), paramvalue));
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		value = value.replaceAll("#replace", paramvalue);
		return exists(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This checks whether a web element exists or not based on the dynamic value of the locator", action_displayname = "dynamicexists", actionname = "dynamicexists", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean dynamicexists(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), valuetovalidate));
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		value = value.replace("#Replace", valuetovalidate);
		return exists(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This checks whether a web element not exists.", action_displayname = "notexists", actionname = "notexists", paraname_equals_curobj = "true", paramnames = {}, paramtypes = {})
	public boolean notexists() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement2(curObject);
		if (idnamevalue == null) {
			return true;
		}
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return notexists(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action checks if the web element exists, if exists it clicks on the web element.", action_displayname = "clickifexist", actionname = "clickifexist", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickifexist() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		InitializeDependence.Click_timeout = true;
		JSONObject idnamevalue = uniqueElement2(curObject);
		InitializeDependence.Click_timeout = false;
		if (idnamevalue == null) {
			return true;
		}
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return clickifexist(identifiername, value);
	}

	public static String escape(String toEscape) {
		if (toEscape.contains("\"") && toEscape.contains("'")) {
			boolean quoteIsLast = false;
			if (toEscape.lastIndexOf("\"") == toEscape.length() - 1) {
				quoteIsLast = true;
			}
			String[] substringsWithoutQuotes = toEscape.split("\"");

			StringBuilder quoted = new StringBuilder("concat(");
			for (int i = 0; i < substringsWithoutQuotes.length; i++) {
				quoted.append("\"").append(substringsWithoutQuotes[i]).append("\"");
				quoted.append(
						((i == substringsWithoutQuotes.length - 1) ? (quoteIsLast ? ", '\"')" : ")") : ", '\"', "));
			}
			return quoted.toString();
		}
		if (toEscape.contains("\"")) {
			return String.format("'%s'", toEscape);
		}

		return String.format("\"%s\"", toEscape);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action validates the text of the alert using the given text", action_displayname = "validateconfirmationtext", actionname = "validateconfirmationtext", paraname_equals_curobj = "false", paramnames = { "valuetovalidate" }, paramtypes = { "String" })
	public boolean validateconfirmationtext(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		String alerttext = getAlerttext();
		if (alerttext.equals(valuetovalidate))
			return true;
		else
			return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This checks whether the alert text contains the given text.", action_displayname = "validatealerttext", actionname = "validatealerttext", paraname_equals_curobj = "false", paramnames = { "valuetovalidate" }, paramtypes = { "String" })
	public boolean validatealerttext(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		String alerttext = getAlerttext();

		if (alerttext.contains(valuetovalidate))
			return true;
		else
			return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action validates the text of the prompt using the given text", action_displayname = "validateonprompt", actionname = "validateonprompt", paraname_equals_curobj = "false", paramnames = { "valuetovalidate" }, paramtypes = { "String" })
	public boolean validateonprompt(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		String alerttext = getAlerttext();
		if (alerttext.equals(valuetovalidate))
			return true;
		else
			return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action accepts the alert", action_displayname = "alertaccept", actionname = "alertaccept", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean alertaccept() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		return acceptAlert();
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action chooses OK on an alert", action_displayname = "chooseokonconfirmation", actionname = "chooseokonconfirmation", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean chooseokonconfirmation() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		return acceptAlert();
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action chooses Cancel on an alert", action_displayname = "choosecancelonconfirmation", actionname = "choosecancelonconfirmation", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean choosecancelonconfirmation() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		return dismissAlert();
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action chooses Cancel on a prompt", action_displayname = "choosecancelonprompt", actionname = "choosecancelonprompt", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean choosecancelonprompt() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		return dismissAlert();
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action answers when a prompt appears", action_displayname = "answeronprompt", actionname = "answeronprompt", paraname_equals_curobj = "false", paramnames = { "valuetoEnter" }, paramtypes = { "String" })
	public boolean answeronprompt(String valuetoEnter) {
		boolean res = false;
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		res = sendAlerttext(valuetoEnter);
		if (res)
			return acceptAlert();
		else
			return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action selects the given value of a dropdown web element", action_displayname = "selectitemfromdropdown", actionname = "selectitemfromdropdown", paraname_equals_curobj = "false", paramnames = { "selectedvalue" }, paramtypes = { "String" })
	public boolean selectitemfromdropdown(String selectedvalue) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			String value = ".//option[contains(., " + escape(selectedvalue) + ")]";
			if (!justOneMethod("xpath", value)) {
				value = ".//option[normalize-space(.) =" + escape(selectedvalue) + "]";
				if (!justOneMethod("xpath", value)) {
					JSONObject idnamevalue;
					if ((idnamevalue = uniqueElement(curObject)) != null) {
						value = idnamevalue.getString("value") + "/option[normalize-space(.) =" + escape(selectedvalue)
								+ "]";
					}
					if (!justOneMethod("xpath", value)) {
						JsonNode at = findattr(curObject.getAttributes(), "id");
						if (at != null) {
							value = "//select[@id=" + escape(at.get("value").asText()) + "]/option[normalize-space(.) ="
									+ escape(selectedvalue) + "]";
						} else {
							at = findattr(curObject.getAttributes(), "name");
							if (at != null)
								value = "//select[@name=" + escape(at.get("value").asText())
										+ "]/option[normalize-space(.) =" + escape(selectedvalue) + "]";
						}
					}
				}
				if (!justOneMethod("xpath", value))
					value = "//*[@value=" + escape(selectedvalue) + "]";
			}

			return clickifexist("xpath", value);
		} else {
			return false;
		}
	}

	public boolean justOneMethod(String identifiername, String value) {
		settimeout(5000, 300000, 3600000);
		JSONArray elements = findElements(identifiername, value);
		if (elements.length() == 1) {
			return true;
		}

		return false;
	}

	@Sqabind(object_template="sqa_record",action_description = "This action selects an element by the text visible on it.", action_displayname = "selectByVisibleText", actionname = "selectByVisibleText", paraname_equals_curobj = "false", paramnames = { "text" }, paramtypes = { "String" })
	public boolean selectByVisibleText(String text) {
		try {
			JSONObject elements = findElements("xpath", escape(".//option[normalize-space(.) = " + escape(text) + "]"))
					.getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					elementClick(elementId);
				}
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action scrolls into the viewport where the web element is present", action_displayname = "scrollintoview", actionname = "scrollintoview", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean scrollintoview() {
		try {
			Thread.sleep(2000);
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return scrollIntoview(identifiername, value);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	private boolean scrollIntoview(String identifiername, String value) {
		try {
			if (identifiername.toLowerCase().trim().contains("css")) {
				return executeScript("var e=document.querySelector(" + escape(value) + "); e.scrollIntoView();");
			} else if (identifiername.toLowerCase().trim().contains("xpath")) {
				return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
						+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView();");
			} else if (identifiername.toLowerCase().trim().equals("id")) {
				return executeScript("var e=document.getElementById(" + escape(value) + "); e.scrollIntoView();");
			} else if (identifiername.toLowerCase().trim().equals("name")) {
				return executeScript(escape("var e =document.getElementsByName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();"));
			} else if (identifiername.toLowerCase().trim().equals("class")) {
				return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();"));
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action checks the checkbox if it is not checked already.", action_displayname = "checkifnotchecked", actionname = "checkifnotchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean checkifnotchecked() {
		try {
			Thread.sleep(7000);
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1)
				checkTab(curObject, tabs);
			JSONObject idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return checkifnotchecked(identifiername, value);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action unchecks the checkbox if it is checked already.", action_displayname = "uncheckifchecked",  actionname = "uncheckifchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean uncheckifchecked() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return uncheckifchecked(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action validates whether a checkbox is checked or not", action_displayname = "validatecheck", actionname = "validatecheck", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean validatecheck() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		String objval = null;
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		JSONObject elements = findElements(identifiername, value).getJSONObject(0);
		Iterator<String> iterator = elements.keys();
		String key = null;
		if (elements.length() == 1) {
			while (iterator.hasNext()) {
				key = iterator.next();
			}
			String elementId = elements.getString(key);
			if (elementId != null) {
				for (JsonNode attr : curObject.getAttributes()) {
					if (attr.get("name").asText().toLowerCase().trim().equals("checked")) {
						objval = attr.get("value").asText();
						break;
					}
				}
				if (getElementproperty(elementId, "checked") != null) {
					if (objval.trim().equals(getElementproperty(elementId, "checked").trim()))
						return true;
					else
						return false;
				} else
					return false;
			}
		}
		return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action peforms an Enter key press on the keyboard", action_displayname = "enter", actionname = "enter", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean enter() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return enter(identifiername, value);
	}

	@Sqabind(object_template="sqa_record",action_description = "This action peforms a Tab key press on the keyboard", action_displayname = "tab",actionname = "tab", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean tab() {
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return tab(identifiername, value);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action navigates to the browser tab with the specified index", action_displayname = "switchtotab", actionname = "switchtotab", paraname_equals_curobj = "false", paramnames = { "tabindex" }, paramtypes = { "String" })
	public boolean switchtotab(String tabindex) {
		JSONArray windows = getWindowhandles();
		String handle = windows.optString(Integer.parseInt(tabindex));
		return switchtoWindow(handle);
	}

	@Override
	public boolean switchtoparenttab() {
		JSONArray windows = getWindowhandles();
		String handle = windows.optString(0);
		return switchtoWindow(handle);
	}

	public Boolean checkTab(Object obj, JSONArray tabs) {
		try {
//			doWaitPreparation();
			JsonNode attribute = null;
			String tabhandle = null;
			attribute = findattr(obj.getAttributes(), "tab");
			if (attribute != null) {
				int tabid = attribute.get("value").asInt();
				if (tabs != null && tabs.length() > 1) {
					String currentwindow = getWindowhandle();
					tabhandle = tabs.optString(tabid);
					if (!currentwindow.equals(tabhandle)) {
						switchtoWindow(tabhandle);
						maximizeScreen();
					}
				}
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean checkFrame(Object obj) {
		try {

			Boolean found = false;
			int j = 0;
			JsonNode attribute = null;
			attribute = findattr(obj.getAttributes(), "framelocation");
			if (attribute != null) {
				switchtoparentframe();
				framelocation = null;
				if (framelocation == null) {
					// if(!browser.equals("safari")) {
					framelocation = attribute.get("value").asText();
					if (browser.equals("firefox") && framelocation.contains(":")
							|| browser.equals("safari") && framelocation.contains(":")) {
						framelocation = "parent:0";
					}
					if (framelocation.contains(":")) {
						String[] frLct = framelocation.split(":");
						for (int i = 1; i < frLct.length; i++) {
							while (!(found) && j++ < 200) {
								found = switchtoframe(frLct[i].trim());
								Thread.sleep(100);
							}
						}
					}

				} else if (!framelocation.equalsIgnoreCase(attribute.get("value").asText())) {
					String[] froldlct = framelocation.split(":");
					String[] frnewlct = attribute.get("value").asText().split(":");
					if (froldlct.length > frnewlct.length) {
						switchtoparentframe();
					} else if (froldlct.length < frnewlct.length) {
						for (int i = froldlct.length; i < frnewlct.length; i++) {
							switchtoframe(frnewlct[i].trim());
						}
					}
					framelocation = attribute.get("value").asText();
				}
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action drags and drops a web element to the given target element ", action_displayname = "draganddropobject", actionname = "draganddropobject",paraname_equals_curobj = "false",  paramnames = { "targetxpathvalue" }, paramtypes = { "String" })
	public boolean draganddropobject(String targetxpathvalue) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return dragandDrop(identifiername, value, targetxpathvalue);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action reads the text of the object and stores the value to runtime parameter", action_displayname = "readtextandstore", actionname = "readtextandstore", paraname_equals_curobj = "false", paramnames = { "Paraname" }, paramtypes = { "String" })
	public boolean readtextandstore(String Paraname) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return readtextandstore(identifiername, value, Paraname);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

//	public boolean getattrandstore(String fieldName, String Paraname) {
//		try {
//			JSONArray tabs = new JSONArray();
//			Boolean flag = false;
//			String objecttype = null;
//			JSONObject idnamevalue=null;
//			tabs = getWindowhandles();
//			if (tabs.length() > 1) {
//				checkTab(curObject, tabs);
//			}
//			if (checkFrame(curObject)) {
//				flag=true;
//				objecttype="unique";
//				for (JsonNode attr : curObject.getAttributes()) {
//					if (attr.has("unique") && attr.get("unique").asBoolean()) {
//						if (attr.get("name").asText().toLowerCase().contains("xpath"))
//							 idnamevalue=fnElementsnotenabled("xpath", attr.get("value").asText(), objecttype);
//						else if (attr.get("name").asText().toLowerCase().contains("css"))
//							idnamevalue=fnElementsnotenabled("css selector", attr.get("value").asText(), objecttype);
//						else if (attr.get("name").asText().toLowerCase().equals("linktext"))
//							idnamevalue=fnElementsnotenabled("link text", attr.get("value").asText(), objecttype);
//						else if (attr.get("name").asText().toLowerCase().equals("class"))
//							idnamevalue=fnElementsnotenabled("class", Stream.of(attr.get("value").asText().split("\\s+"))
//									.map(str -> "." + str).collect(joining(" ")), objecttype);
//						else if (attr.get("name").asText().toLowerCase().equals("id"))
//							idnamevalue=fnElementsnotenabled("css selector", Stream.of(attr.get("value").asText().split("\\s+"))
//									.map(str -> "#" + str).collect(joining(" ")), objecttype);
//						else
//							idnamevalue=fnElements("name",
//									String.format("*[name='%s']", attr.get("value").asText().replace("'", "\\'")),
//									objecttype);
//					}
//
//				}
//				
//				if(idnamevalue!=null) {
//				String identifiername = idnamevalue.getString("identifiername");
//				String value = idnamevalue.getString("value");
//				return getattrandstore(identifiername, value, fieldName, Paraname);
//				}
//				
//				else {
//					return false;
//				}
//			} else {
//				return false;
//			}
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
//			e.printStackTrace();
//			return false;
//		}
//
//	}

	@Sqabind(object_template="sqa_record",action_description = "This action gets the value of the attribute and stores it to runtime parameter", action_displayname = "getattrandstore", actionname = "getattrandstore", paraname_equals_curobj = "false", paramnames = { "fieldName","Paraname" }, paramtypes = { "String","String" })
	public boolean getattrandstore(String fieldName, String Paraname) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return getattrandstore(identifiername, value, fieldName, Paraname);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}

	}

	public boolean getattrandstore(String identifiername, String value, String fieldName, String Paraname) {
		try {
			JSONObject re = new JSONObject();
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				if ((innertext = getElementproperty(elementId, fieldName)) == null) {
					innertext = getElementproperty(elementId, value);
				}
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				Base64 base64Url = new Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
//				re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());
				if (jsonObject.has("createdBy")) {

					re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", Paraname + "_" + jsonObject.get("updatedBy").toString());

				}
				re.put("paramvalue", innertext);
				InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
				return true;

			}
			return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action reads the value of the object and stores the value to runtime parameter", action_displayname = "readvalueandstore", actionname = "readvalueandstore", paraname_equals_curobj = "false", paramnames = { "Paraname" }, paramtypes = { "String" })
	public boolean readvalueandstore(String Paraname) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return readvalueandstore(identifiername, value, Paraname);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean readtextandstore(String identifiername, String value, String Paraname) {
		try {
			
			System.out.println("\nInside read text and store");
			System.out.println("paraname :" + Paraname);
			JSONObject re = new JSONObject();
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			System.out.println("element found inside readtextandstore :" + elements);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				if ((innertext = getElementproperty(elementId, "innerText")) == null) {
					innertext = getElementproperty(elementId, value);
				}
				if (curExecution == null) {
					header = new HashMap<String, String>();
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}

				System.out.println(" ");
				System.out.println("currentexecution :" + curExecution);
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				Base64 base64Url = new Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
//				re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());
				if (jsonObject.has("createdBy")) {

					System.out.println("inside created block");
					re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", Paraname + "_" + jsonObject.get("updatedBy").toString());

				}
				re.put("paramvalue", innertext);
				System.out.println(re);
				InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
				return true;

			}
			return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean readvalueandstore(String identifiername, String value, String Paraname) {
		try {
			JSONObject re = new JSONObject();
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				if ((innertext = getElementproperty(elementId, "value")) == null) {
					innertext = getElementproperty(elementId, value);
				}
				if (curExecution == null) {
					header = new HashMap<String, String>();
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				Base64 base64Url = new Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
//				re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());
				if (jsonObject.has("createdBy")) {

					re.put("paramname", Paraname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", Paraname + "_" + jsonObject.get("updatedBy").toString());

				}
				re.put("paramvalue", innertext);
				InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
				return true;

			}
			return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean dragandDrop(String identifiername, String value, String targetxpathvalue) {
		try {
			String[] desVal = targetxpathvalue.split(":=");
			if (identifiername.contains("css") && desVal[0].contains("css")) {
				return executeScript(
						"function v(e,t){function n(e){var t=new CustomEvent(\"CustomEvent\");return t.initCustomEvent(e,!0,!0,null),t.dataTransfer={data:{},setData:function(e,t){this.data[e]=t},getData:function(e){return this.data[e]}},t}function r(e,t,n){return e.dispatchEvent?e.dispatchEvent(n):e.fireEvent?e.fireEvent(\"on\"+t,n):void 0}var o=n(\"dragstart\");r(e,\"dragstart\",o);var a=n(\"drop\");a.dataTransfer=o.dataTransfer,r(t,\"drop\",a);var u=n(\"dragend\");u.dataTransfer=o.dataTransfer,r(e,\"dragend\",u)}v(document.querySelector("
								+ escape(value) + "),document.querySelector(" + escape(desVal[1]) + "));");
			} else if (identifiername.contains("xpath") && desVal[0].contains("css")) {
				return executeScript(
						"function v(e,t){function n(e){var t=new CustomEvent(\"CustomEvent\");return t.initCustomEvent(e,!0,!0,null),t.dataTransfer={data:{},setData:function(e,t){this.data[e]=t},getData:function(e){return this.data[e]}},t}function r(e,t,n){return e.dispatchEvent?e.dispatchEvent(n):e.fireEvent?e.fireEvent(\"on\"+t,n):void 0}var o=n(\"dragstart\");r(e,\"dragstart\",o);var a=n(\"drop\");a.dataTransfer=o.dataTransfer,r(t,\"drop\",a);var u=n(\"dragend\");u.dataTransfer=o.dataTransfer,r(e,\"dragend\",u)}v(document.evaluate("
								+ escape(value)
								+ ",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue,document.querySelector("
								+ escape(desVal[1]) + "));");
			} else if (identifiername.contains("css") && desVal[0].contains("xpath")) {
				return executeScript(
						"function v(e,t){function n(e){var t=new CustomEvent(\"CustomEvent\");return t.initCustomEvent(e,!0,!0,null),t.dataTransfer={data:{},setData:function(e,t){this.data[e]=t},getData:function(e){return this.data[e]}},t}function r(e,t,n){return e.dispatchEvent?e.dispatchEvent(n):e.fireEvent?e.fireEvent(\"on\"+t,n):void 0}var o=n(\"dragstart\");r(e,\"dragstart\",o);var a=n(\"drop\");a.dataTransfer=o.dataTransfer,r(t,\"drop\",a);var u=n(\"dragend\");u.dataTransfer=o.dataTransfer,r(e,\"dragend\",u)}v(document.querySelector("
								+ escape(value) + "),document.evaluate(" + escape(desVal[1])
								+ ",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue);");
			} else
				return executeScript(
						"function v(e,t){function n(e){var t=new CustomEvent(\"CustomEvent\");return t.initCustomEvent(e,!0,!0,null),t.dataTransfer={data:{},setData:function(e,t){this.data[e]=t},getData:function(e){return this.data[e]}},t}function r(e,t,n){return e.dispatchEvent?e.dispatchEvent(n):e.fireEvent?e.fireEvent(\"on\"+t,n):void 0}var o=n(\"dragstart\");r(e,\"dragstart\",o);var a=n(\"drop\");a.dataTransfer=o.dataTransfer,r(t,\"drop\",a);var u=n(\"dragend\");u.dataTransfer=o.dataTransfer,r(e,\"dragend\",u)}v(document.evaluate("
								+ escape(value)
								+ ",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue,document.evaluate("
								+ escape(desVal[1])
								+ ",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue);");
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed To Execute : ", null, "", "error");
			return false;
		}
	}

	public JSONArray waituntil(String identifiername, String value) {
		try {
			Integer timeout = 1;
			JSONArray res = null;
			return res = findElements(identifiername, value);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject uniqueElement2(Object obj) {
		try {
			Boolean flag = false;
			String objecttype = null;
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {

					objecttype = "unique";
					flag = true;
					logerOBJ.customlogg(userlogger, "Object Property :", null, attr.get("name").toString(), "info");
					logerOBJ.customlogg(userlogger, "Object Property Value :", null, attr.get("value").toString(),
							"info");

					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "xpath").put("value",
									attr.get("value").asText());
						else
							return null;

					else if (attr.get("name").asText().toLowerCase().contains("css"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "css").put("value",
									attr.get("value").asText());
						else
							return null;
					else if (attr.get("name").asText().toLowerCase().equals("linktext"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "linktest").put("value",
									attr.get("value").asText());
						else
							return null;
					else if (attr.get("name").asText().toLowerCase().equals("class"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "class").put("value",
									attr.get("value").asText());
						else
							return null;

					else if (attr.get("name").asText().toLowerCase().equals("id"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "id").put("value",
									attr.get("value").asText());
						else
							return null;
					else if (findElement("xpath", attr.get("value").asText()) != null)
						return (new JSONObject()).put("identifiername", "xpath").put("value",
								attr.get("value").asText());
					else
						return null;
				}

			}
			if (!flag) {
				for (JsonNode attr : obj.getAttributes()) {
					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						if (findElement("xpath", attr.get("value").asText()) != null)
							return (new JSONObject()).put("identifiername", "xpath").put("value",
									attr.get("value").asText());
						else
							return null;
				}

			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			logerOBJ.customlogg(userlogger, "Step Result Failed : Due to ", null, e.toString(), "error");
			return null;
		}
	}

	public JSONObject uniqueElement(Object obj) {
		try {
			ArrayList<Future<JSONObject>> taskList = new ArrayList<>();
			Boolean flag = false;
			String objecttype = null;
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {

					objecttype = "unique";
					flag = true;
					logerOBJ.customlogg(userlogger, "Object Property :", null, attr.get("name").toString(), "info");
					logerOBJ.customlogg(userlogger, "Object Property Value :", null, attr.get("value").toString(),
							"info");

					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						return fnElements("xpath", attr.get("value").asText(), objecttype);
					else if (attr.get("name").asText().toLowerCase().contains("css"))
						return fnElements("css selector", attr.get("value").asText(), objecttype);
					else if (attr.get("name").asText().toLowerCase().equals("linktext"))
						return fnElements("link text", attr.get("value").asText(), objecttype);
					else if (attr.get("name").asText().toLowerCase().equals("class"))
						return fnElements("class", Stream.of(attr.get("value").asText().split("\\s+"))
								.map(str -> "." + str).collect(joining(" ")), objecttype);
					else if (attr.get("name").asText().toLowerCase().equals("id"))
						return fnElements("css selector", Stream.of(attr.get("value").asText().split("\\s+"))
								.collect(joining(" ")), objecttype);
					else
						return fnElements("name",
								String.format("*[name='%s']", attr.get("value").asText().replace("'", "\\'")),
								objecttype);
				}
			}
			JSONObject s = null;
			if (!flag) {

				objecttype = "notunique";
				Instant start = Instant.now();
				executor.g().restart();
				parallelsearch(obj, taskList, objecttype);
				s = killtask(taskList);
				while (s == null) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
					if (timeelapsed < 90000) {
						taskList.clear();
						executor.g().parallelobject.shutdownNow();
						executor.g().restart();
						parallelsearch(obj, taskList, objecttype);
						s = killtask(taskList);
					} else {
						maxtime = 90000;
						logerOBJ.customlogg(userlogger, "Step Result Failed : Due to Element not Found.", null, "",
								"error");
						return null;
					}
				}

				maxtime = 90000;
			}
			logerOBJ.customlogg(userlogger, "Object Property :", null, s.get("identifiername").toString(), "info");
			logerOBJ.customlogg(userlogger, "Object Property Value :", null, s.get("value").toString(), "info");
			return s;
		} catch (Exception e) {
			e.printStackTrace();
			logerOBJ.customlogg(userlogger, "Step Result Failed : Due to ", null, e.toString(), "error");
			return null;
		}
	}

	public JSONObject parallelsearch(Object obj, ArrayList<Future<JSONObject>> taskList, String objecttype)
			throws InterruptedException {
		taskList.removeAll(taskList);
		for (String idname : ididame) {
			JsonNode attr = InitializeDependence.findattr(obj.getAttributes(), idname);
			if (attr != null) {
				if (attr.get("name").asText().toLowerCase().contains("css")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("css selector", attr.get("value").asText(), objecttype);
					}));
				} else if (attr.get("name").asText().toLowerCase().contains("xpath")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText(), objecttype);
					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathidrelative")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathattributes")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathlink")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathtextcontent")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathposition")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathinnertext")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
//				} else if (attr.get("name").asText().toLowerCase().equals("xpathtextcontent")) {
//					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
//						return fnElements("xpath", attr.get("value").asText(), objecttype);
//					}));
				} else if (attr.get("name").asText().toLowerCase().equals("linktext")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("link text", attr.get("value").asText(), objecttype);
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("class")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("class", Stream.of(attr.get("value").asText().split("\\s+"))
								.map(str -> "." + str).collect(joining(" ")), objecttype);
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("id")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("css selector", Stream.of(attr.get("value").asText().split("\\s+"))
								.map(str -> "#" + str).collect(joining(" ")), objecttype);
					}));
				}
			}
		}
		return null;

	}

	public JSONObject killtask(ArrayList<Future<JSONObject>> taskList)
			throws InterruptedException, ExecutionException, TimeoutException {
		JSONObject s = null;
		while (!executor.g().parallelobject().isShutdown() && taskList.size() != 0) {
			for (int i = 0; i < taskList.size(); i++) {
				if (!taskList.get(i).isDone()) {
					continue;
				} else {
					s = taskList.get(i).get();
					if (s != null) {
						executor.g().parallelobject().shutdownNow();
						taskList.removeAll(taskList);
						break;
					} else {
						taskList.remove(i);
					}
				}
			}
		}
		return s;
	}

	public JSONObject uniqueIdvalue(Object obj, String[] idname, Integer i) {
		JsonNode attribute = null;
		String identifiername = null;
		String value = null;
		JSONArray result = null;
		JSONObject idnamevalue = new JSONObject();
		try {
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						return idnamevalue.put("identifiername", "xpath").put("value", attr.get("value").asText());
					else if (attr.get("name").asText().toLowerCase().contains("css"))
						return idnamevalue.put("identifiername", "css selector").put("value",
								attr.get("value").asText());
					else if (attr.get("name").asText().toLowerCase().equals("linktext"))
						return idnamevalue.put("identifiername", "link text").put("value", attr.get("value").asText());
					else if (attr.get("name").asText().toLowerCase().equals("class"))
						return idnamevalue.put("identifiername", "class").put("value", attr.get("value").asText());
					else if (attr.get("name").asText().toLowerCase().equals("id"))
						return idnamevalue.put("identifiername", "id").put("value", attr.get("value").asText());
					else
						return idnamevalue.put("identifiername", attr.get("name").asText()).put("value",
								attr.get("value").asText());
				}
			}

			if (i < idname.length) {
				attribute = findattr(obj.getAttributes(), idname[i]);
				if (attribute != null) {
					identifiername = attribute.get("name").asText();
					value = attribute.get("value").asText();
					if (identifiername.toLowerCase().contains("css"))
						identifiername = "css selector";
					if (identifiername.toLowerCase().contains("xpath"))
						identifiername = "xpath";
					result = waituntil(identifiername, value);
					if (result != null && result.length() == 1) {
						idnamevalue.put("identifiername", identifiername);
						idnamevalue.put("value", value);
					} else {
						++i;
						idnamevalue = uniqueIdvalue(obj, idname, i);
					}
				} else {
					++i;
					idnamevalue = uniqueIdvalue(obj, idname, i);
				}
			} else {
				logger.error("No unique value for id,name,class,xpath");
				logerOBJ.customlogg(userlogger, "No unique value for id, name,class,xpath.. ", null, "", "error");
				idnamevalue = null;
			}
			return idnamevalue;
		} catch (Exception e) {
			e.printStackTrace();
			return uniqueIdvalue(obj, idname, ++i);
		}
	}

	public JsonNode findattr(List<JsonNode> Attributes, String identifierName) {
		try {
			for (int i = 0; i < Attributes.size(); i++) {
				if (Attributes.get(i).get("name").asText().equalsIgnoreCase(identifierName)) {
					return Attributes.get(i);
				}
			}
			return null;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	String clickelementId = null;

	public JSONObject fnElements(String identifiername, String value, String objecttype) {
		try {

			int count = 0;
			if (InitializeDependence.loader_present.equals("true")) {
				if (InitializeDependence.loader_xpath != null) {
					settimeout(10, 10, 10);
					String array_length = findElementforloader("xpath", InitializeDependence.loader_xpath);

					logger.info("loader xpath found....");
					boolean present = true;
					if (array_length != null) {

						if (isElementenabled(array_length) && iselementDisplayed(array_length)) {
//						while (present && count < 900) {
//							logger.info("count : "+ count);
//							
//							if (waituntilelementnotexist("xpath", InitializeDependence.loader_xpath)) {
//								logger.info("waited for element");
//								present = true;
//							} else {
//								logger.info("element yet not loaded");
//								logger.info("count :" + count);
//								present = false;
//
//							}
//							count++;
//						}

							int maxtimee = 0;

							if (InitializeDependence.loader_waitime == -1) {
								maxtimee = 90000;
							} else {
								maxtimee = InitializeDependence.loader_waitime;
							}
							Instant start = Instant.now();
							while (present) {
								Instant stop = Instant.now();
								long timeelapsed = java.time.Duration.between(start, stop).toMillis();

								if (timeelapsed < maxtimee) {
									if (waituntilelementnotexist("xpath", InitializeDependence.loader_xpath)) {
										logger.info("waited for element");
										present = true;
									} else {
										logger.info("element yet not loaded");
										logger.info("count :" + count);
										present = false;

									}

								} else {
									logger.info("waited for lement to go off for time: "
											+ InitializeDependence.loader_waitime);
									return null;
								}
							}

						}

					} else {
						logger.info("xpath of loader is wrong");

					}
				} else {
					logger.info("loader xpath not provided in userconfig");
				}
			}

			if (objecttype.equals("notunique")) {
				settimeout(1000, 300000, 3600000);
			} else {
				settimeout(30000L, 300000, 3600000);
			}

			JSONArray eleArray = new JSONArray();
			String elementId = null;
			System.out.println("before element check");
			if ((eleArray = findElements(identifiername, value)) != null) {
				if (eleArray.length() == 1) {
					System.out.println("element found");
					Iterator<String> iterator = eleArray.getJSONObject(0).keys();
					String key = null;
					while (iterator.hasNext())
						key = iterator.next();
					elementId = eleArray.getJSONObject(0).getString(key);
					Integer i = Integer.valueOf(0);
					boolean ena = false, dis = true;
					int counter = 0;
					for (i = Integer.valueOf(i.intValue() + 1); (!ena || !dis) && i.intValue() < 900;) {
						counter++;
						System.out.println(" ");
						System.out.println("counter :" + counter);
						if (value.contains("/..") || this.browser.equals("safari")) {
							ena = isElementenabled(elementId);
							continue;
						}
						ena = isElementenabled(elementId);
					}
					if (elementId != null && ena && dis)
						return (new JSONObject()).put("identifiername", identifiername).put("value", value);
					return null;
				}
				return null;
			}
			return null;
		} catch (Exception e) {
			this.logerOBJ.customlogg(userlogger, "Exception : ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	public int fnElements(String identifiername, String value) {
		try {
			JSONArray eleArray = new JSONArray();
			// String elementId = null;
			eleArray = findElements(identifiername, value);
			return eleArray.length();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : Due to ", null, e.toString(), "error");
			e.printStackTrace();

		}
		return 0;
	}

	public JSONObject fnElementsnotenabled(String identifiername, String value, String objecttype) {
		try {

			if (objecttype.equals("notunique")) {
				settimeout(1000, 300000, 3600000);
			} else {
				settimeout(30000, 300000, 3600000);
			}

			JSONArray eleArray = new JSONArray();
			String elementId = null;
			System.out.println("before element check");
			if ((eleArray = findElements(identifiername, value)) != null) {
				if (eleArray.length() == 1) {
					System.out.println("element found");
					Iterator<String> iterator = eleArray.getJSONObject(0).keys();
					String key = null;
					while (iterator.hasNext()) {
						key = iterator.next();
					}
					elementId = eleArray.getJSONObject(0).getString(key);
					Integer i = 0;
//					boolean ena = false, dis = true;
					int counter = 0;

					if (elementId != null) {
						return new JSONObject().put("identifiername", identifiername).put("value", value);
					} else
						return null;
				} else
					return null;
			} else {
				return null;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Exception : ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean click(String identifiername, String value) {
		JSONArray elementsarr = null;
		try {

			System.out.println("inside click\n");
//			Thread.sleep(1000);
			if (InitializeDependence.timeout == -1) {
				settimeout(90000, 300000, 3600000);
			} else
				settimeout(InitializeDependence.timeout, 300000, 3600000);

			elementsarr = findElements(identifiername, value);
			JSONObject elements = null;

			System.out.println("elemnt arr :" + elementsarr);

			if (elementsarr.length() != 0 && elementsarr.length() <= 1)
				elements = elementsarr.getJSONObject(0);
			else {
				throw new Exception("click fail");
			}
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				clickelementId = elements.getString(key);
				Integer i = 0;
				boolean ena = false, dis = true;
				while (!(ena && dis) && i++ < 900) {
					System.out.println("i : " + i);
					System.out.println("inside click while loop");
					ena = isElementenabled(clickelementId);
					System.out.println("ena : " + ena);
//					dis = iselementDisplayed(clickelementId);
					if (browser.equals("safari") && i == 5) {
						dis = true;
					}
					Thread.sleep(100);
				}
				if (clickelementId != null && ena && dis) {
					logger.info("Executed Click on :{} ", value);
					String res = elementClick(clickelementId);
					if (res.equals("true")) {
						Thread.sleep(1000);
						return true;
					} else if (res.equals("not clickable at point")) {
						System.out.println("trying to click by js");
						return clickusingjs(identifiername, value);
					} else {
						return false;
					}
				} else {
					logger.info("Failed to Execute Click on :{} ,No Element Found,Trying by Js", value);
					throw new Exception("Executing with Js");
				}
			} else {
				logger.info("Failed to Execute Click on :{} ,No Element Found,Trying by Js", value);
				throw new Exception("Executing with Js");
			}

		} catch (ElementClickInterceptedException ec) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String res = elementClick(clickelementId);
			if (res.equals("true")) {
				return true;
			} else if (res.equals("not clickable at point")) {
				return clickusingjs(identifiername, value);
			} else {
				return false;
			}
		} catch (Exception e) {

			int timeout = 0;
			if (browser.equals("safari")) {
				try {
					Thread.sleep(100);
					timeout = timeout + 100;
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (timeout <= 2000) {
					return click(identifiername, value);
				} else {
					return false;
				}
			} else {
				if (identifiername.toLowerCase().trim().contains("css")) {
					return executeScript("var e=document.querySelector(" + escape(value)
							+ "); e.scrollIntoView(); setTimeout(()=>{e.click()},2000) ");
				} else if (identifiername.toLowerCase().trim().contains("xpath")) {
					return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
							+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView(); setTimeout(()=>{r[0].click()},2000) ");
				} else if (identifiername.toLowerCase().trim().equals("id")) {
					return executeScript("var e=document.getElementById(" + escape(value)
							+ "); e.scrollIntoView(); setTimeout(()=>{e.click()},2000) ");
				} else if (identifiername.toLowerCase().trim().equals("name")) {
					return executeScript(escape("var e =document.getElementsByName(" + escape(value)
							+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{e[0].click()},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
				} else if (identifiername.toLowerCase().trim().equals("class")) {
					return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
							+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{e[0].click()},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
				} else {
					logerOBJ.customlogg(userlogger, "Step Result Failed : Due to ", null, e.toString(), "error");
					e.printStackTrace();
					logger.info("Failed to Execute Click on :{} ", value);
					logerOBJ.customlogg(userlogger, "Failed to Execute Click on :{} ", null, value, "error");
					return false;
				}
			}
		}
	}

	@Override
	public boolean enter(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				Integer i = 0;
				boolean ena = false, dis = false;
				while (!(dis && ena) && i++ < 900) {
					ena = isElementenabled(elementId);
					dis = iselementDisplayed(elementId);
					Thread.sleep(100);
					if (browser.equals("safari") && i == 5) {
						dis = true;
					}
				}
				if (elementId != null) {
					if (dis && ena) {
						// if (browser.equals("safari")) {
						String enterkey = KeyBoardActions.ENTER.value();
						Thread.sleep(500);
						res = elementSendkeys(elementId, enterkey);
						logger.info("Executed Enter on :{}", value);
						Thread.sleep(5000);
						// } else {
						// Thread.sleep(500);
						// res = true;

						// res = elementSendkeys(elementId, "\n");
						// logger.info("Executed Enter on :{}", value);

						// logger.info("Executed Enter on :{}", value);
						Thread.sleep(5000);
						// }

					} else {
						res = false;
						logger.info("Step Failed to Execute Enter on :{} , element not found", value);
						logerOBJ.customlogg(userlogger, "Failed to Execute Enter on :{} , element not found ", null,
								value, "error");

						Thread.sleep(5000);
					}

				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute Enter on :{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed to Execute Enter on : ", null, value, "error");
			return false;
		}
	}

	public boolean tab(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					String enterkey = KeyBoardActions.TAB.value();
					res = elementSendkeys(elementId, enterkey);
					logger.info("Executed Tab on :{}", value);
					logerOBJ.customlogg(userlogger, "Step Failed to Execute Tab on : ", null, value, "error");
				} else {
					res = false;
					logger.info("Failed to Execute Tab on :{} , element not found", value);
					logerOBJ.customlogg(userlogger, "Step Failed to Execute Tab on : ,element not found ", null, value,
							"error");
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute Tab on :{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed to Execute Tab on : ", null, value, "error");

			return false;
		}
	}

//	public boolean uploadfile(String path) throws InterruptedException, AWTException {
//
//		boolean bStatus = false;
////		Thread.sleep(1000);
//		try {
//			if (clickwhenisenablaedandisdisplayed()) {
//				StringSelection ss = new StringSelection(path);
//
//				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
//
//				Thread.sleep(1000);
//
//				Robot robot = new Robot();
//
//				String OS = System.getProperty("os.name");
//				if (OS.contains("Windows")) {
//
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
//
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_V);
//					Thread.sleep(1000);
//
//					robot.keyRelease(java.awt.event.KeyEvent.VK_V);
//
//					Thread.sleep(1000);
//
//					robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
//
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(1000);
//
//					System.out.println("Finishied printing");
//				} else if (OS.toLowerCase().contains("mac")) {
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_META);
//
//					Thread.sleep(1000);
//
//					robot.keyPress(java.awt.event.KeyEvent.VK_SHIFT);
//					Thread.sleep(1000);
//					robot.keyPress(java.awt.event.KeyEvent.VK_G);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_SHIFT);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_G);
//					Thread.sleep(1000);
//					robot.keyPress(java.awt.event.KeyEvent.VK_META);
//					Thread.sleep(1000);
//					robot.keyPress(java.awt.event.KeyEvent.VK_V);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_V);
//					Thread.sleep(2000);
//					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(3000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(4000);
//					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
//					Thread.sleep(1000);
//					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
//				}
//
//				bStatus = true;
//				Thread.sleep(1000);
//			} else {
//				bStatus = false;
//				Thread.sleep(1000);
//			}
//
//		} catch (Exception e) {
//
//			System.out.println("expection is " + e);
//			logerOBJ.customlogg(userlogger, "Step Failed to Execute UploadFile : ", null, e.toString(), "error");
//
//			bStatus = false;
//
//		}
//		Thread.sleep(1000);
//		return bStatus;
//
//	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action uploads a given file to the object", action_displayname = "uploadfile", actionname = "uploadfile", paraname_equals_curobj = "false", paramnames = { "path" }, paramtypes = { "String" })
	public boolean uploadfile(String path) throws InterruptedException, AWTException {

		boolean bStatus = false;
//		Thread.sleep(1000);
		try {
			if (click()) {
				StringSelection ss = new StringSelection(path);

				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

				Thread.sleep(1000);

				Robot robot = new Robot();
				String OS = System.getProperty("os.name");
				if (OS.contains("Windows")) {

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(1000);

					robot.keyRelease(java.awt.event.KeyEvent.VK_V);

					Thread.sleep(1000);

					robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);

					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
				} else if (OS.toLowerCase().contains("mac")) {
					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_META);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_SHIFT);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_G);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_SHIFT);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_G);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(2000);
					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(3000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(4000);
					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
				}

				bStatus = true;
			} else {
				bStatus = false;
			}

		} catch (Exception e) {

			System.out.println("expection is " + e);
			logerOBJ.customlogg(userlogger, "Step Failed to Execute UploadFile : ", null, e.toString(), "error");

			bStatus = false;

		}

		return bStatus;

	}



	@Override
	public boolean entertext(String identifiername, String value, String valuetoenter) {
		try {
			valuetoenter = valuetoenter.replaceAll("#SPACE#", " ");
			if (valuetoenter.isEmpty())
				return true;
			JSONArray elearr = findElements(identifiername, value);
			if (elearr.isEmpty()) {
				Thread.sleep(10000);
				elearr = findElements(identifiername, value);
			}
			JSONObject elements = null;
			if (elearr.length() != 0 && elearr.length() >= 1) {
				elements = elearr.getJSONObject(0);
			} else {
				throw new Exception("entertext failed");
			}
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				Integer i = 0;
				boolean ena = false, dis = false;
				if (getElementproperty(elementId, "type").equals("file")) {
					while (!(ena) && i++ < 900) {
						ena = isElementenabled(elementId);
						Thread.sleep(100);
					}
					if (elementId != null && ena) {
						res = elementSendkeys(elementId, valuetoenter);
						if (res) {
							logger.info("Executed EnterText on :{} with value :{}", value, valuetoenter);
						} else
							throw new Exception("Executing with Js");
					} else {
						logger.info("Failed to Execute EnterText on :{} ,No Element Found,Trying by Js", value);
						// logerOBJ.customlogg(userlogger, "Step Failed: EnterText on: ", null, value,
						// "error");
						throw new Exception("Executing with Js");
					}
				} else if (!getElementproperty(elementId, "type").equals("hidden")) {
					if (!browser.equals("safari")) {
						while (!(dis && ena) && i++ < 900) {
							ena = isElementenabled(elementId);
							dis = iselementDisplayed(elementId);
							Thread.sleep(100);
						}
					} else {
						while (!(ena) && i++ < 900) {
							ena = isElementenabled(elementId);
							dis = iselementDisplayed(elementId);
							Thread.sleep(100);
						}
					}

					if (browser.equals("safari")) {
						if (elementId != null && ena && !dis) {
							res = elementSendkeys(elementId, valuetoenter);
						}
					} else {
						if (elementId != null && dis && ena) {
							elementClick(elementId);
							elementClear(elementId);
							res = elementSendkeys(elementId, valuetoenter);
							if (res) {
								logger.info("Executed EnterText on :{} with value :{}", value, valuetoenter);
							} else
								throw new Exception("Executing with Js");
						} else {
							logger.info("Failed to Execute EnterText on :{} ,No Element Found,Trying by Js", value);
							throw new Exception("Executing with Js");
						}
					}

				}
			}
			return res;
		} catch (Exception e) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			if (!browser.equals("safari")) {
				if (identifiername.toLowerCase().trim().contains("css")) {
					return executeScript("var e=document.querySelector(" + escape(value)
							+ "); e.scrollIntoView();  setTimeout(()=>{e.value=" + escape(valuetoenter)
							+ ";e.dispatchEvent(new Event('input'))},1000)");
				} else if (identifiername.toLowerCase().trim().contains("xpath")) {
					return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
							+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView();setTimeout(()=>{r[0].value="
							+ escape(valuetoenter) + ";r[0].dispatchEvent(new Event('input'))},1000);");
				} else if (identifiername.toLowerCase().trim().equals("id")) {
					return executeScript("var e=document.getElementById(" + escape(value)
							+ "); e.scrollIntoView(); setTimeout(()=>{e.value=" + escape(valuetoenter) + "},1000)");
				} else if (identifiername.toLowerCase().trim().equals("name")) {
					return executeScript(escape("var e =document.getElementsByName(" + escape(value)
							+ ");  setTimeout(()=>{e[0].value=" + escape(valuetoenter)
							+ "},1000): e.length>1?  \"More than one element found.\": \"No element found.\""));
				} else if (identifiername.toLowerCase().trim().equals("class")) {
					return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
							+ "); throw e.length==1?e[0].scrollIntoView(); setTimeout(()=>{e.value="
							+ escape(valuetoenter)
							+ "},1000): e.length>1?  \"More than one element found.\": \"No element found.\""));
				} else {
					e.printStackTrace();
					logger.info("Failed to Execute EnterText on :{}", value);
					logerOBJ.customlogg(userlogger, "Step Failed: Element EnterText on : ", null, value, "error");
					return false;
				}
			} else {
				return false;
			}
		}

	}

//	r[0].scrollIntoView();
	public boolean keyboardaction(String identifiername, String value, String valuetoenter) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (valuetoenter.equalsIgnoreCase("backspace")) {
					if (elementId != null) {
						int c = 10;
						for (int i = 0; i <= c; i++) {
							res = elementSendkeys(elementId, KeyBoardActions.BACKSPACE.value());
						}

					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F1")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F1.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F2")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F2.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F3")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F3.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F4")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F4.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F5")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F5.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F6")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F6.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F7")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F7.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F8")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F8.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F9")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F9.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F10")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F10.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F11")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F11.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("F12")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.F12.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("Enter")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.ENTER.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else if (valuetoenter.equalsIgnoreCase("Tab")) {
					if (elementId != null) {
						res = elementSendkeys(elementId, KeyBoardActions.TAB.value());
					} else {
						res = false;
						logger.error("Element not found");
						logerOBJ.customlogg(userlogger, "Step Failed: Element not found ", null, null, "error");
					}
				} else {
					Scanner scan = new Scanner(valuetoenter);
					scan.useDelimiter("!@");
					String combination = "";
					while (scan.hasNext()) {
						String word = scan.next();
						if (word.equalsIgnoreCase("alt")) {
							combination = combination + "/" + KeyBoardActions.ALT.value();
						} else if (word.equalsIgnoreCase("tab")) {
							combination = combination + "/" + KeyBoardActions.TAB.value();
						} else if (word.equalsIgnoreCase("shift")) {
							combination = combination + "/" + KeyBoardActions.SHIFT.value();
						} else if (word.equalsIgnoreCase("cntrl")) {
							combination = combination + "/" + KeyBoardActions.CONTROL.value();
						} else {
							combination = combination + "/" + word;
						}
					}
					if (combination.charAt(0) == '/') {
						combination = combination.substring(1);
					}
					res = elementSendkeys(elementId, combination);
				}
			}
			return res;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed to Execute KeyboardAction : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean arrowup() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean arrowdown() {
		return false;
	}

	@Override
	public boolean enterfilepath(String value) {
		return false;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Launches the given URL", action_displayname = "launchapplication", actionname = "launchapplication", paraname_equals_curobj = "false", paramnames = { "url" }, paramtypes = { "String" })
	public boolean launchapplication(String url) {
		try {
			
			 if (InitializeDependence.suite) {
			        if (!InitializeDependence.suite_new_session && InitializeDependence.suite_testcase_sequence == 0) {
			          if (InitializeDependence.global_itr != 1)
			            return launchUrl(url).booleanValue(); 
			          if (InitializeDependence.funcItr_suite != 1 && InitializeDependence.funcItr_suite != 0)
			            return launchUrl(url).booleanValue(); 
			        } 
			        if (!InitializeDependence.suite_new_session && InitializeDependence.suite_testcase_sequence != 0)
			          return launchUrl(url).booleanValue(); 
			      } 
			if (driver != null) {
				deleteSession();
				closeDriver();
				driver = null;
			}

			else {
				getDriver();
			}
			boolean val = false;
			String platform;
			if (SystemUtils.IS_OS_WINDOWS)
				platform = "windows";
			else if (SystemUtils.IS_OS_MAC)
				platform = "mac";
			else
				platform = "linux";
			val = webCapabilities(brname, platform);
			if (brname.equals("edge"))
				maximizeScreen();
			if (val == true) {
				if (InitializeDependence.timeout == -1)
					val = settimeout(90000, 300000, 3600000);
				else
					val = settimeout(InitializeDependence.timeout, 300000, 3600000);
				if (val) {
					maximizeScreen();
					launchUrl(url);
					logger.info("Url Launched :{}", url);
				} else {
					logger.info("setting timeout failed");
					logerOBJ.customlogg(userlogger, "Step Failed: timeout ", null, "", "error");
				}
			}
			return val;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed  : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean clickifexist(String identifiername, String value) {
		try {

			if (InitializeDependence.existTimeout == -1) {
				settimeout(10000, 300000, 3600000);
			} else
				settimeout(InitializeDependence.existTimeout, 300000, 3600000);

			boolean res = false;
			boolean click = false;
			res = exists(identifiername, value);
			if (res == true) {
				click = click(identifiername, value);
			} else {
				click = true;
			}
			logger.info("Executed ClickIfExist on :{}", value);
			if (InitializeDependence.timeout == -1)
				settimeout(90000, 300000, 3600000);
			else
				settimeout(InitializeDependence.timeout, 300000, 3600000);
			return click;
		} catch (Exception e) {
			if (InitializeDependence.timeout == -1)
				settimeout(90000, 300000, 3600000);
			else
				settimeout(InitializeDependence.timeout, 300000, 3600000);
			logger.info("Executed ClickIfExist on :{}", value);
			return true;
		}
	}

	@Override
	public boolean mouseoveranelement() {
		return false;
	}

	@Override
	public boolean cleartext(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				elementClick(elementId);
				if (elementId != null) {
					String texttoclear = getElementproperty(elementId, "value");
					for (int i = 0; i <= texttoclear.length(); i++) {
						res = elementSendkeys(elementId, KeyBoardActions.BACKSPACE.value());
						res = elementSendkeys(elementId, KeyBoardActions.DELETE.value());
					}
					logger.info("Executed ClearText on :{}", value);
				} else {
					logger.info("Failed to Executed ClearText on :{}", value);
					logerOBJ.customlogg(userlogger, "Step Failed To Execute ClearText : ", null, value, "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Failed to Executed ClearText on :{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed To Execute ClearText : ", null, e.toString(), "error");

			return false;
		}
	}

	@Override
	public boolean checkifnotchecked(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {

					boolean response = getElementpropertyboolean(elementId, "checked");
					Thread.sleep(2000);
					logerOBJ.customlogg(userlogger, "Checkbox response :", null, value, "error");
					if (response == false) {
//						Thread.sleep(1000);
						elementClick(elementId);
						logger.info("Executed CheckIfNotChecked on :{}", value);
						res = true;
					} else {
						logger.info("CheckBox or RadioBox is already checked!!!", value);
						logerOBJ.customlogg(userlogger, "CheckBox or RadioBox is already checked!!!", null, value,
								"error");
						res = true;
					}
				} else {
					logger.info("Failed to Executed CheckIfNotChecked on :{}", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to CheckIfNotChecked :", null, value, "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Executed CheckIfNotChecked on :{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to CheckIfNotChecked :", null, value, "error");
			return false;
		}
	}

	public boolean getElementpropertyboolean(String elementid, String property) {
		return getDriver().getActionManger().getElementpropertyboolean(elementid, property);
	}

	@Override
	public boolean uncheckifchecked(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					boolean response = Boolean.parseBoolean(getElementproperty(elementId, "checked"));
					if (response == true) {
						elementClick(elementId);
						logger.info("Executed UnCheckIfChecked on :{}", value);
						res = true;
					} else {
						logger.info("Failed to uncheck  on radio or checkbox with :{} Because Already unchecked",
								value);
						logerOBJ.customlogg(userlogger, "Step Failed: to UnCheckIfChecked on :", null, value, "error");

						res = false;
					}
				} else {
					logger.info("Failed to Executed UnCheckIfChecked on :{}", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to UnCheckIfChecked on :", null, value, "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Executed UnCheckIfChecked on :{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to UnCheckIfChecked on :", null, value, "error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action waits for the alert to appear", action_displayname = "waitforalert", actionname ="waitforalert", paraname_equals_curobj = "false",  paramnames = {}, paramtypes = {})
	public boolean waitforalert() {
		try {
			boolean result = false;
			String resp = getAlerttext();
			while (resp == "no such alert") {
				resp = getAlerttext();
			}
			if (resp != "no such alert") {
				result = true;
			}
			return result;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean exists(String identifiername, String value) {
		try {
			settimeout(10000, 300000, 3600000);
			Boolean res = false;
			JSONArray elementsarr = findElements(identifiername, value);
			JSONObject elements = new JSONObject();
			int count = 0;
			if (elementsarr.length() != 0 && elementsarr.length() <= 1) {
				elements = elementsarr.getJSONObject(0);
			} else {
				while (elementsarr.length() == 0) {
					count++;
					if (count == 3) {
						break;
					} else {
						elementsarr = findElements(identifiername, value);
						if (elementsarr.length() != 0) {
							elements = elementsarr.getJSONObject(0);
						}
					}
				}
			}
			String key = null;
			if (elements.length() == 1) {
				Iterator<String> iterator = elements.keys();
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				clickelementId = elements.getString(key);
				Integer i = 0;
				boolean ena = false, dis = false;
				while (!(ena && dis) && i++ < 900) {
					ena = isElementenabled(clickelementId);
					dis = iselementDisplayed(clickelementId);
					if (browser.equals("safari") && i == 5) {
						dis = true;
					}
					Thread.sleep(100);
				}

				if (ena && dis) {
					logger.info("Element Exist  with :{} ", value);
					res = true;
				} else {
					logger.info("Failed to Find Element   with :{} ", value);
					res = false;
				}
			} else {
				res = false;
			}
			return res;

		} catch (Exception e) {
			if (browser.equals("safari")) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return exists(identifiername, value);
			}
			logger.info("Failed to Find Element   with :{} ", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to Find Element :", null, value, "error");
			return false;
		}
	}

	@Override
	public boolean notexists(String identifiername, String value) {
		try {

			if (InitializeDependence.existTimeout == -1) {
				settimeout(10000, 300000, 3600000);
			} else
				settimeout(InitializeDependence.existTimeout, 300000, 3600000);
			JSONArray felement = findElements(identifiername, value);
			if (felement != null) {
				if (felement.length() > 0) {
					logger.info(" Failed to Execute notExist  with :{} ", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute notExist :", null, value, "error");
					if (InitializeDependence.timeout == -1)
						settimeout(90000, 300000, 3600000);
					else
						settimeout(InitializeDependence.timeout, 300000, 3600000);
					return false;
				} else {
					logger.info("Execute notExist  with :{} ", value);
					if (InitializeDependence.timeout == -1)
						settimeout(90000, 300000, 3600000);
					else
						settimeout(InitializeDependence.timeout, 300000, 3600000);
					return true;
				}
			} else {
				logger.info("Execute notExist  with :{} ", value);
				if (InitializeDependence.timeout == -1)
					settimeout(90000, 300000, 3600000);
				else
					settimeout(InitializeDependence.timeout, 300000, 3600000);
				return true;
			}
		} catch (Exception e) {
			logger.info(" Failed to Execute notExist  with :{} ", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute notExist :", null, e.toString(), "error");

			return true;
		}
	}

	@Override
	public boolean validatetext(String identifiername, String value, String valuetovalidate) {
		try {

			System.out.println("inisde validate text");
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			System.out.println("\nelements : " + elements);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			Boolean dis = false, ena = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
//				scrollIntoview(identifiername, value);
				if (elementId != null) {

					System.out.println("inside loop");

					ena = isElementenabled(elementId);
					dis = iselementDisplayed(elementId);

					System.out.println("ena :" + ena);
					System.out.println("dis :" + dis);
					String text = getElementproperty(elementId, "innerText").replace("\n", "");
					System.out.println("\ntext : " + text);
					this.setExepectedValue(text);
					this.setActualvalue(valuetovalidate);

//					text = text.trim();

					if (text.equals(valuetovalidate)) {
						logger.info("Executed  validateText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						res = true;
					} else {
						logger.info("Failed to Execute  validateText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText actualValue :", null,
								text, "error");
						res = false;
					}
				} else {
					logger.info("Failed to Execute  validateText Element not FoundF");
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText Element not Found ", null, "",
							"error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute  validateText");
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText :", null, e.toString(), "error");
			return false;
		}
	}

	public boolean validatevalue(String identifiername, String value, String valuetovalidate) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
//				scrollIntoview(identifiername, value);
				if (elementId != null) {
					String text = getElementproperty(elementId, "value").replace("\n", "");
					this.setExepectedValue(text);
					if (text.equals(valuetovalidate)) {
						logger.info("Executed  validateText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						res = true;
					} else {
						logger.info("Failed to Execute  validateText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText :", null, text, "error");
						res = false;
					}
				} else {
					logger.info("Failed to Execute  validateText Element not FoundF");
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText Element Not Found", null, "",
							"error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute  validateText");
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateText :", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean validatecheck(String identifiername, String value) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					String text = getElementproperty(elementId, "type");
					if (text.equals("checkbox")) {
						logger.info("Executed validateCheck with:{}", value);
						res = true;
					} else {
						logger.info("Failed to Execute validateCheck with:{}", value);
						logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateCheck :", null, value,
								"error");

						res = false;
					}
				} else {
					logger.info("Failed to Execute validateCheck with:{} Element not found", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateCheck :", null, value, "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Failed to Execute validateCheck with:{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute validateCheck :", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean scrolltoview() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean entertextandenter(String identifiername, String value, String valuetoenter) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}

				String elementId = elements.getString(key);
				if (elementId != null) {
					String text = valuetoenter + "\uE007";

					res = elementSendkeys(elementId, text);
					logger.info("Executed EnterTextandEnter with:{}", value);
				} else {

					logger.info("Failed to  Execute EnterTextandEnter with:{} Element not Found", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute EnterTextandEnter :", null, "", "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to  Execute EnterTextandEnter with:{}", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute EnterTextandEnter :", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean scrolltobottom() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolltotop() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseoverandclick() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean entertextandtab(String identifiername, String value, String valuetoenter) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					String text = valuetoenter + "\uE004";
					res = elementSendkeys(elementId, text);
					logger.info("Executed EnterTextandTab with:{}", value);
				} else {
					logger.info("Failed to  Execute EnterTextandTab with:{} Element not Found", value);
					logerOBJ.customlogg(userlogger, "Step Failed: to Execute EnterTextandTab, Element not Found:", null,
							"", "error");
					res = false;
				}
			}
			return true;
		} catch (Exception e) {
			logger.info("Failed to  Execute EnterTextandTab with:{} ", value);
			logerOBJ.customlogg(userlogger, "Step Failed: to Execute EnterTextandTab :", null, e.toString(), "error");

			return false;
		}
	}

	@Override
	public boolean fileuploadusingrobot() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean waituntilelementpresent() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean selectcheckbox() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean swipetoelement() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean settimeout(long implicit, long pageout, long script) {
		return setTimeout(implicit, pageout, script);
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header) {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
//			this.customMethod=null;
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass(classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action performs a click on the object using javascript", action_displayname = "clickusingjs", actionname = "clickusingjs", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickusingjs() {
		try {
			// Thread.sleep(3000);
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					if (browser.equals("safari") && !value.contains("/..")) {
						return click(identifiername, value);
					} else {
						return clickusingjs(identifiername, value);
					}
				} else
					return false;
			} else
				return false;

		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action performs a click on the object using javascript", action_displayname = "clickusingjss", actionname = "clickusingjss", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickusingjss() {
		try {
			boolean res = false;
			Thread.sleep(1000);
			settimeout(5000, 300000, 3600000);
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
//				res = clickusingjs(idnamevalue.getString("identifiername"), idnamevalue.getString("value"));
				if ((idnamevalue = uniqueElement(curObject)) != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					JSONArray elementsarr = findElements(identifiername, value);
					JSONObject elements = null;
					int count = 0;
					if (elementsarr.length() != 0 && elementsarr.length() <= 1) {
						elements = elementsarr.getJSONObject(0);
					} else {
						while (elementsarr.length() == 0) {
							count++;
							if (count <= 3) {
								elementsarr = findElements(identifiername, value);
								elements = elementsarr.getJSONObject(0);
							} else {
								throw new Exception("Element not found");
							}
						}
					}
					Iterator<String> iterator = elements.keys();
					String key = null;
					if (elements.length() == 1) {
						while (iterator.hasNext()) {
							key = iterator.next();
						}
						clickelementId = elements.getString(key);
						Integer i = 0;
						boolean ena = false, dis = false;
						while (!(ena && dis) && i++ < 900) {
							ena = isElementenabled(clickelementId);
							dis = iselementDisplayed(clickelementId);
							if (browser.equals("safari") && i == 5) {
								dis = true;
							}
							Thread.sleep(100);
						}
						if (clickelementId != null && ena && dis) {
							logger.info("Executed Click on :{} ", value);
							res = clickusingjs(identifiername, value);
						} else {
							logger.info("Failed to Execute Click on :{} ,No Element Found,Trying by Js", value);
							logerOBJ.customlogg(userlogger, "Step Failed Execute to click : ", null, "", "error");
						}
					}
				}
			}
			return res;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed to click : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action performs a dynamic click on the object using javascript", action_displayname = "clickusingjs", actionname = "clickusingjs", paraname_equals_curobj = "false", paramnames = { "runtimevalue" }, paramtypes = { "String" })
	public boolean clickusingjs(String runtimevalue) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					value = value.replaceAll("#replace", runtimevalue);
					return clickusingjs(identifiername, value);
				} else
					return false;
			} else
				return false;
		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean validatepartialtext(String identifiername, String value, String valuetovalidate) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
					// String text = getElementtext(elementId).replace("\n", "");
					String text = getElementproperty(elementId, "innerText").replace("\n", "");
					this.setExepectedValue(text);
					this.setActualvalue(valuetovalidate);
					if (text.contains(valuetovalidate)) {
						logger.info("Executed  validatePartialText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						return true;
					} else {
						logger.info("Failed to Execute  validatePartialText actualValue :{} with expectedValue:{} ",
								text, valuetovalidate);
						logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText ", null, "",
								"error");
						res = false;
					}
				} else {
					logger.info("Failed to Execute  validatePartialText Because Element not Found");
					logerOBJ.customlogg(userlogger,
							"Step Failed Execute to validatePartialText Because Element not Found : ", null, "",
							"error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute  validatePartialText ");
			logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action partially validates the text of the object with the given text", action_displayname = "validatepartialtext", actionname = "validatepartialtext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatepartialtext(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return validatepartialtext(identifiername, value, valuetovalidate);
	}

	@Override
	public String getUrl() {
		try {
			return geturl();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean iselementDisplayed(String elementId) {
		return getDriver().getActionManger().isElementDisplayed(elementId);
	}

	public String fullpageScreenshot() {
		try {
			String html2canvasJs = new String(Files.readAllBytes(
					Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "SqaExtension", "html2canvas.js" })));
			String generateScreenshotJS = "var img=\"\";\n" + "\n" + "function take_screenshot()\n" + "{\n"
					+ "  return html2canvas(document.body)\n" + "  .then(canvas => {\n"
					+ "      img = canvas.toDataURL()     \n" + " console.log(img);\n" + "  });\n" + "\n"
					+ "}take_screenshot(); function getimg(){return img } setTimeout(()=>{getimg},1000);";
			executeScript(html2canvasJs + " " + generateScreenshotJS);
			return null;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean entertextbyjs(String identifiername, String value, String valuetoenter) {
		try {
			if (identifiername.toLowerCase().trim().contains("css")) {
				return executeScript("var e=document.querySelector(" + escape(value) + "); e.scrollIntoView();e.value="
						+ escape(valuetoenter) + ";e.dispatchEvent(new Event('input')),1000");
			} else if (identifiername.toLowerCase().trim().contains("xpath")) {
				return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
						+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView();r[0].value="
						+ escape(valuetoenter) + ";r[0].dispatchEvent(new Event('input')),1000");
			} else if (identifiername.toLowerCase().trim().equals("id")) {
				return executeScript("var e=document.getElementById(" + escape(value)
						+ "); e.scrollIntoView(); setTimeout(()=>{e.value=" + escape(valuetoenter) + "},2000)");
			} else if (identifiername.toLowerCase().trim().equals("name")) {
				return executeScript(escape("var e =document.getElementsByName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView(); setTimeout(()=>{e[0].value="
						+ escape(valuetoenter)
						+ "},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else if (identifiername.toLowerCase().trim().equals("class")) {
				return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView(); setTimeout(()=>{e.value=" + escape(valuetoenter)
						+ "},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else {
				logger.info("Failed to Execute EnterText on :{}", value);
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action enters the given text using javascript", action_displayname = "entertextbyjs", actionname = "entertextbyjs", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertextbyjs(String valuetoenter) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			JSONObject idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return entertextbyjs(identifiername, value, valuetoenter);
		} else {
			return false;
		}
	}

	@Override
	public boolean clickusingjs(String identifiername, String value) {
		if (identifiername.toLowerCase().trim().contains("css")) {
			return executeScript("var e=document.querySelector(" + escape(value)
					+ "); e.scrollIntoView(); setTimeout(()=>{e.click()},2000) ");
		} else if (identifiername.toLowerCase().trim().contains("xpath")) {
			return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
					+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView(); setTimeout(()=>{r[0].click()},2000);dispatchEvent(new Event('click')) ");
		} else if (identifiername.toLowerCase().trim().equals("id")) {
			return executeScript("var e=document.getElementById(" + escape(value)
					+ "); e.scrollIntoView(); setTimeout(()=>{e.click()},2000) ");
		} else if (identifiername.toLowerCase().trim().equals("name")) {
			return executeScript(escape("var e =document.getElementsByName(" + escape(value)
					+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{e[0].click()},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
		} else if (identifiername.toLowerCase().trim().equals("class")) {
			return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
					+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{e[0].click()},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
		} else {
			logger.info("Failed to Execute Click on :{} ", value);
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action selects an item from the dropdown using its xpath", action_displayname = "selectitemfromdropdownbyxpath", actionname = "selectitemfromdropdownbyxpath", paraname_equals_curobj = "false", paramnames = { "selectedvalue" }, paramtypes = { "String" })
	public boolean selectitemfromdropdownbyxpath(String selectedvalue) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			return click("xpath", selectedvalue);
		} else {
			return false;
		}
	}
	// select[@id="searchSystemUser_userType"]/option[normalize-space(.) ="All"]

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action performs a double click", action_displayname = "dbclick", actionname = "dbclick", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean dbclick() {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					return dbclick(identifiername, value);
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to dbclick : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action moves the mouse cursor", action_displayname = "mousemove", actionname = "mousemove", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean mousemove() {
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		String script = null;
		if (identifiername.equalsIgnoreCase("id"))
			script = "var evObj = document.createEvent(\"MouseEvents\");evObj.initEvent(\"mouseover\",true, false); document.getElementById("
					+ escape(value) + ").dispatchEvent(evObj)";
		else if (identifiername.equalsIgnoreCase("class"))
			script = "var evObj = document.createEvent(\"MouseEvents\");evObj.initEvent(\"mouseover\",true, false); document.getElementsByClassName("
					+ escape(value) + ")[0].dispatchEvent(evObj)";
		return executeScript(script);
//		return  executeScript(escape("var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); document.getElementsByClassName('"+value+"')[0].dispatchEvent(evObj)"));

	}

	@Override
	public boolean dbclick(String identifiername, String value) {
		try {

			if (identifiername.toLowerCase().trim().contains("css")) {
				return executeScript("var e=document.querySelector(" + escape(value)
						+ "); e.scrollIntoView(); setTimeout(()=>{var r=document.createEvent(\"MouseEvents\");r.initEvent(\"dblclick\",!0,!0),e.dispatchEvent(r)},2000) ");
			} else if (identifiername.toLowerCase().trim().contains("xpath")) {
				return executeScript("for(var n=[],r=[],o=(n=document.evaluate(" + escape(value)
						+ ",document,null,XPathResult.ANY_TYPE,null)).iterateNext();o;)r.push(o),o=n.iterateNext();if(1!=r.length)throw r.length>1?\"More than one element found.\":\"No element found.\";r[0].scrollIntoView(); setTimeout(()=>{var rk=document.createEvent(\"MouseEvents\");rk.initEvent(\"dblclick\",!0,!0),r[0].dispatchEvent(rk)},2000) ");
			} else if (identifiername.toLowerCase().trim().equals("id")) {
				return executeScript("var e=document.getElementById(" + escape(value)
						+ "); e.scrollIntoView(); setTimeout(()=>{var rk=document.createEvent(\"MouseEvents\");rk.initEvent(\"dblclick\",!0,!0),e.dispatchEvent(rk)},2000) ");
			} else if (identifiername.toLowerCase().trim().equals("name")) {
				return executeScript(escape("var e =document.getElementsByName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{var rk=document.createEvent(\"MouseEvents\");rk.initEvent(\"dblclick\",!0,!0),e[0].dispatchEvent(rk)},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else if (identifiername.toLowerCase().trim().equals("class")) {
				return executeScript(escape("var e =document.getElementsByClassName(" + escape(value)
						+ "); throw e.length==1?e[0].scrollIntoView();setTimeout(()=>{var rk=document.createEvent(\"MouseEvents\");rk.initEvent(\"dblclick\",!0,!0),e[0].dispatchEvent(rk)},2000): e.length>1?  \"More than one element found.\": \"No element found.\""));
			} else {
				logger.info("Failed to Execute Click on :{} ", value);
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to dbclick : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		try {
			return click("xpath", xpath.replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingdynamicxpath : ", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action performs a dynamic click using dynamic xpath", action_displayname = "clickusingdynamicxpath", actionname = "clickusingdynamicxpath", paraname_equals_curobj = "false", paramnames = { "runtimevlau" }, paramtypes = { "String" })
	public boolean clickusingdynamicxpath(String runtimevlau) {
		try {
			JsonNode attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			return click("xpath", attribute.get("value").asText().replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingdynamicxpath : ", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean clickusingjswithdynamicxpath(String xpath, String runtimevlau) {
		try {
			return clickusingjs("xpath", xpath.replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingjswithdynamicxpath : ", null,
					e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action performs a dynamic click by dynamic xpath using javascript", action_displayname = "clickusingjswithdynamicxpath", actionname = "clickusingjswithdynamicxpath", paraname_equals_curobj = "false", paramnames = { "runtimevlau" }, paramtypes = { "String" })
	public boolean clickusingjswithdynamicxpath(String runtimevlau) {
		try {
			JsonNode attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");
			return clickusingjs("xpath", attribute.get("value").asText().replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingjswithdynamicxpath : ", null,
					e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean readinstringandstore(String identifiername, String value, String Paraname, String startString,
			String endString) {
		try {
			JSONObject re = new JSONObject();
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				int startIndex = -1, endIndex = -1;
				if ((innertext = getElementproperty(elementId, "innerText")) == null) {
					innertext = getElementproperty(elementId, value);
				}
				startIndex = innertext.indexOf(startString);
				endIndex = innertext.indexOf(endString);
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				if (startIndex < endIndex) {
					re.put("paramname", Paraname);
					re.put("paramvalue", innertext.substring(startIndex, endIndex));
					InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
					logger.info("Store runTime parameter Done");

					return true;
				} else {
					logger.info("Store runTime parameter failed");
					logerOBJ.customlogg(userlogger, "Step Failed: Execute to Store runTime parameter : ", null, "",
							"error");

					return false;
				}

			}
			return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed: Execute to readinstringandstore : ", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action reads a substring from given indices and stores it to runtime parameter", action_displayname = "readinstringandstore", actionname = "readinstringandstore", paraname_equals_curobj = "false", paramnames = { "Paraname","startString","endString" }, paramtypes = { "String","String","String" })
	public boolean readinstringandstore(String Paraname, String startString, String endString) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue = uniqueElement(curObject);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");
				return readinstringandstore(identifiername, value, Paraname, startString, endString);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to readinstringandstore : ", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean doWaitPreparation() {
		try {
			executeScript2("window.eval('function setNewPageValue(e) {window.new_page = true;};\\\r\n"
					+ "                window.addEventListener(\"beforeunload\", setNewPageValue, false);\\\r\n"
					+ "                if (window.XMLHttpRequest) {if (!window.origXMLHttpRequest || !window.ajax_obj) {\\\r\n"
					+ "                window.ajax_obj = []; window.origXMLHttpRequest = window.XMLHttpRequest;\\\r\n"
					+ "                window.XMLHttpRequest = function() { var xhr = new window.origXMLHttpRequest();\\\r\n"
					+ "                window.ajax_obj.push(xhr); return xhr;}}} function setDOMModifiedTime() {\\\r\n"
					+ "                window.domModifiedTime = Date.now();}var _win = window.document.body;\\\r\n"
					+ "                _win.addEventListener(\"DOMNodeInserted\", setDOMModifiedTime, false);\\\r\n"
					+ "                _win.addEventListener(\"DOMNodeInsertedIntoDocument\", setDOMModifiedTime, false);\\\r\n"
					+ "                _win.addEventListener(\"DOMNodeRemoved\", setDOMModifiedTime, false);\\\r\n"
					+ "                _win.addEventListener(\"DOMNodeRemovedFromDocument\", setDOMModifiedTime, false);\\\r\n"
					+ "                _win.addEventListener(\"DOMSubtreeModified\", setDOMModifiedTime, false);');\r\n"
					+ "    window.sideex_new_page = window.eval('(function() {return window.new_page;}())');\r\n"
					+ "    var expression = 'if(window.document.readyState==\"complete\"){return true;}else{return false;}';\r\n"
					+ "    window.sideex_page_done = window.eval('(function() {' + expression + '}())');\r\n"
					+ "    var expression = 'if (window.ajax_obj) { if (window.ajax_obj.length == 0) {return true;} else {\\\r\n"
					+ "        for (var index in window.ajax_obj) {\\\r\n"
					+ "        if (window.ajax_obj[index].readyState !== 4 &&\\\r\n"
					+ "        window.ajax_obj[index].readyState !== undefined &&\\\r\n"
					+ "        window.ajax_obj[index].readyState !== 0) {return false;}}return true;}}\\\r\n"
					+ "        else {if (window.origXMLHttpRequest) {window.origXMLHttpRequest = \"\";}return true;}';\r\n"
					+ "    window.sideex_ajax_done = window.eval('(function() {' + expression + '}())');\r\n"
					+ "    window.sideex_dom_time = window.eval('(function() {return window.domModifiedTime;}())');");
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return false;
		}
	}

	public boolean closed() {
		try {
			deleteSession();
			closeDriver();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public boolean closewindow() {
		try {
			closeWindow();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean back(Object obj) {
		try {
			return back();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean forward(Object obj) {
		try {
			return forward();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean refresh(Object obj) {
		try {
			return refresh();
		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action waits until the element exists", action_displayname = "waituntilelementexist", actionname = "waituntilelementexist", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean waituntilelementexist() {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1)
				checkTab(curObject, tabs);
			JSONObject idnamevalue = uniqueElement(curObject);
			if (idnamevalue == null)
				return true;

			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return waituntilelementexist(identifiername, value);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to waituntilelementexist : ", null, e.toString(),
					"error");
			return false;
		}
	}

	public boolean waituntilelementexist(String identifiername, String value) {
		try {
			Integer maxtime = 10000;
			settimeout(5000, 300000, 3600000);
			if (InitializeDependence.existTimeout != -1)
				maxtime = InitializeDependence.existTimeout;
			Integer i = 0;
			boolean ena = true;
			JSONArray elements = findElements(identifiername, value);
			Instant start = Instant.now();

			if (!elements.isEmpty()) {
				while ((ena)) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
//					elements = findElements(identifiername, value);

					if (timeelapsed < maxtime) {
						String key = null;
						JSONObject elemen = elements.getJSONObject(0);
						Iterator<String> iterator = elemen.keys();
						while (iterator.hasNext()) {
							key = iterator.next();
						}
						clickelementId = elemen.getString(key);
						System.out.println(isElementenabled(clickelementId));
						System.out.println(iselementDisplayed(clickelementId));
						if (!elements.isEmpty() && isElementenabled(clickelementId)
								&& iselementDisplayed(clickelementId)) {
							logger.info("wait for obeject Not Exist  with :{} ", value);
							ena = true;
						} else {
							logger.info(" Element not Exist   with :{} ", value);
							logerOBJ.customlogg(userlogger, "Step Failed: Element not Exist ", null, "", "error");
							ena = false;
						}
						Thread.sleep(200);
					} else {
						if (ena) {
							return true;
						} else {
							return false;
						}
					}
				}
			} else
				return true;
			settimeout(90000, 300000, 3600000);
			if (!ena)
				return true;
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to waituntilelementexist : ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Override
	public boolean closewindow(Object obj) {
		boolean res = false;
		try {
			JsonNode attribute = null;
			attribute = findattr(obj.getAttributes(), "tab");
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (attribute != null) {
				int tabid = attribute.get("value").asInt();
				switchtoWindow(tabs.get(tabid).toString());
			}
			res = closeWindow();
			tabs = getWindowhandles();
			if (tabs.length() == 1) {
				res = switchtoWindow(tabs.get(0).toString());
			}
			return res;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to closewindow : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action navigates to the broswer tab where the current object is present", action_displayname = "switchtowindow", actionname = "switchtowindow", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean switchtowindow() {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		JsonNode attribute = null;
		String tabhandle = null;
		attribute = findattr(curObject.getAttributes(), "tab");
		if (attribute != null) {
			int tabid = attribute.get("value").asInt();
			tabhandle = tabs.optString(tabid);
			return switchtoWindow(tabhandle);
		} else {
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action navigates to the viewport frame where the current object is present", action_displayname = "switchtoframe", actionname = "switchtoframe", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean switchtoframe() {
		try {

			logger.info("--------------------SWITCH TO FRAME-------------------");
			if (browser.equals("firefox") || browser.equals("safari")) {
				return true;
			}
			Boolean iframe = false;
			for (JsonNode attr : curObject.getAttributes()) {
				logger.info("inside for loop");
				if (attr.get("name").asText().toLowerCase().contains("xpath")) {
					logger.info("inside for loop if condition");
					if (attr.get("value").asText().contains("//iframe")) {
						iframe = true;
					}
				}
			}
			JSONArray totaliframe = null;
			logger.info("before findelements");
			if (iframe)
				totaliframe = findElements("xpath", "//iframe");
			else
				totaliframe = findElements("xpath", "//frame");

			logger.info("after find elements");
			JSONObject idnamevalue;
			JSONObject frame = null;
			int index = -1;

			Thread.sleep(2000);

			logger.info("going to check unique elements");
			if ((idnamevalue = uniqueElement(curObject)) != null) {
				logger.info("inside uniquelements if condition");
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");

				logger.info("identifiername :" + identifiername);
				logger.info("value : " + value);

				frame = findElements(identifiername, value).getJSONObject(0);
				logger.info("frame : " + frame);
				for (int i = 0; i < totaliframe.length(); i++) {
					if (frame.toString().equals(totaliframe.getJSONObject(i).toString())) {
						index = i;
						break;
					}
				}

				logger.info("frame of elment : " + index);
			}
			if (index == -1)
				return false;
			else
				return switchtoframe(String.valueOf(index));

		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action navigates to the given broswer tab", action_displayname = "switchtowindow", actionname = "switchtowindow", paraname_equals_curobj = "false", paramnames = { "vlaue" }, paramtypes = { "String" })
	public boolean switchtowindow(String vlaue) {
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			String tabhandle = null;
			int tabid = Integer.parseInt(vlaue);
			tabhandle = tabs.optString(tabid);
			return switchtoWindow(tabhandle);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to switchtowindow : ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action navigates to the parent frame of the current frame", action_displayname = "switchtoparent", actionname = "switchtoparent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean switchtoparent() {
		try {
			return switchtoparentframe();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to switchtoparent : ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the fileanme from the browsers download list and stores it to runtime parameter", action_displayname = "getfilename", actionname = "getfilename", paraname_equals_curobj = "false", paramnames = { "param" }, paramtypes = { "String" })
	public Boolean getfilename(String param) {
		try {
			JSONObject res;
			if (executeScript("window.open()")) {
				JSONArray tabs = new JSONArray();
				tabs = getWindowhandles();
				if (tabs.length() > 1) {
					switchtoWindow(tabs.getString(tabs.length() - 1));
					launchUrl("chrome://downloads");
					res = executeScript2(
							"return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div#content #file-link').text");
					return InitializeDependence.generaldriver.storeruntime(param,
							InitializeDependence.downloadpath + File.separator + res.get("value").toString());
				} else
					return false;
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to getfilename : ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public Boolean jsexecutor(String js) {
		try {
			JSONObject res = executeScript2(js);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action closes the given tab", action_displayname = "closetab", actionname = "closetab", paraname_equals_curobj = "false", paramnames = { "tab" }, paramtypes = { "String" })
	public boolean closetab(String tab) {
		boolean res = false;
		try {
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			int tabid = Integer.parseInt(tab);
			switchtoWindow(tabs.get(tabid).toString());
			res = closeWindow();
			tabs = getWindowhandles();
			if (tabs.length() == 1) {
				res = switchtoWindow(tabs.get(0).toString());
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action opens a new browser tab with the given url ", action_displayname = "opennewtabwithurl", actionname = "opennewtabwithurl", paraname_equals_curobj = "false", paramnames = { "m_strURL" }, paramtypes = { "String" })
	public boolean opennewtabwithurl(String m_strURL) throws Exception {
		try {
			if (executeScript("window.open(" + escape(m_strURL) + ",'_blank')")) {
				Thread.sleep(2000);
				JSONArray tabs = new JSONArray();
				tabs = getWindowhandles();
				if (tabs.length() > 1) {
					switchtoWindow(tabs.getString(tabs.length() - 1));
					return true;
				} else
					return false;
			} else
				return false;
		}

		catch (Exception e)

		{
			logerOBJ.customlogg(userlogger, "Step Failed Execute to opennewtabwithurl : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean gettablerowcount(String identifiername, String value, String runtimeparam) {
		try {
			int idnamevalue;
			idnamevalue = fnElements(identifiername, value);
			InitializeDependence.generaldriver.storeruntime(runtimeparam, Integer.toString(idnamevalue));
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the row count of a table to runtime parameter", action_displayname = "gettablerowcount", actionname = "gettablerowcount", paraname_equals_curobj = "false", paramnames = { "runtimeparam" }, paramtypes = { "String" })
	public boolean gettablerowcount(String runtimeparam) {
		try {
			int idnamevalue = -1;
			String objecttype;
			Boolean flag;
			List<JsonNode> attrname = curObject.getAttributes();

			for (JsonNode attr : curObject.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					objecttype = "unique";

					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						idnamevalue = fnElements("xpath", attr.get("value").asText());
				}
				InitializeDependence.generaldriver.storeruntime(runtimeparam, Integer.toString(idnamevalue));

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action performs a mouse click", action_displayname = "clickusingmouse", actionname = "clickusingmouse", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickusingmouse() {
		try {
			String value;
			String identifiername;
			System.out.println(curObject);

			JSONObject idnamevalue;
			String element;
			idnamevalue = uniqueElement(curObject);
			identifiername = idnamevalue.getString("identifiername");
			value = escape(idnamevalue.getString("value"));
			System.out.println(identifiername);
			System.out.println(value);
			if (identifiername.toLowerCase().trim().contains("xpath")) {

				try {
					System.out.println(executeScript2(

							"function getElementByXpath(path) {\r\n"
									+ "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;\r\n"
									+ "}\r\n" + "function sleep(msec) {\r\n"
									+ "    var start = new Date().getTime();\r\n"
									+ "    for (var i = 0; i < 1e7; i++) {\r\n"
									+ "        if ((new Date().getTime() - start) > msec) {\r\n"
									+ "            break;\r\n" + "        }\r\n" + "    }\r\n" + "}\r\n"
									+ "function fireMouseEvents(query, eventNames) {\r\n"
									+ "    // var element = getElementByXpath(query);\r\n"
									+ "    if (getElementByXpath(query) && eventNames && eventNames.length) {\r\n"
									+ "        sleep(4000);\r\n" + "        for (var index in eventNames) {\r\n"
									+ "            var eventName = eventNames[index]; if (getElementByXpath(query).fireEvent) {\r\n"
									+ "                getElementByXpath(query).fireEvent('on' + eventName);\r\n"
									+ "                return true;\r\n" + "            }\r\n"
									+ "            else {\r\n"
									+ "                var eventObject = document.createEvent('MouseEvents'); eventObject.initEvent(eventName, true, false);\r\n"
									+ "                // element = getElementByXpath(query);\r\n"
									+ "                getElementByXpath(query).dispatchEvent(eventObject);\r\n"
									+ "                return true;\r\n" + "            }\r\n" + "        }\r\n"
									+ "    }\r\n" + "}\r\n" + "fireMouseEvents(" + value + ",[\"mousedown\"])"));

					return true;
				} catch (Exception e) {
					logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingmouse : ", null, e.toString(),
							"error");
					return false;
				}
			}

			else {
				System.out.println("mouse click failed");
				return false;
			}

		}

		catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingmouse : ", null, e.toString(), "error");
			return false;
		}

	}

//	public boolean clickusingmouse(String xpath) {
//		try {
////			xpath = xpath.replace("\"", "'");
//			JSONObject element1 = null;
//			String demo = xpath.replace("\"", "");
//			System.out.println("inside clickusingmouse before if statement");
//			if ((element1 = fnElements("xpath", demo, "unique")) != null) {
//				Thread.sleep(1000);
//				executeScript2(
//						"function getElementByXpath(e){return document.evaluate(e,document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue}function sleep(e){for(var t=(new Date).getTime(),n=0;n<1e7&&!((new Date).getTime()-t>e);n++);}function fireMouseEvents(e,t){if(getElementByXpath(e)&&t&&t.length)for(var n in t){var a=t[n];if(getElementByXpath(e).fireEvent)return getElementByXpath(e).fireEvent(\"on\"+a),!0;var r=document.createEvent(\"MouseEvents\");return r.initEvent(a,!0,!1),getElementByXpath(e).dispatchEvent(r),!0}}fireMouseEvents("
//								+ xpath + ",['mousedown'])");
//				// fireMouseEvents("+escape(xpath)+",['mousedown'])
//				Thread.sleep(2000);
//				System.out.println("inside clickusingmouse inside if statement,script got executed");
//				return true;
//			} else {
//				return false;
//			}
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingmouse : ", null, e.toString(), "error");
//			return false;
//		}
//
//	}

//	
	@Sqabind(object_template="sqa_record",action_description = "This action performs a mouse click on the given object", action_displayname = "clickusingmouse", actionname = "clickusingmouse", paraname_equals_curobj = "false", paramnames = { "xpath" }, paramtypes = { "String" })
	public boolean clickusingmouse(String xpath) {
		try {
//			xpath = xpath.replace("\"", "'");
			JSONObject element1 = null;
			String demo = xpath.replace("\"", "");
			System.out.println("inside clickusingmouse before if statement");
			if ((element1 = fnElements("xpath", demo, "unique")) != null) {
				JSONObject obj = executeScript2(
						"function getElementByXpath(e){return document.evaluate(e,document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue}function sleep(e){for(var t=(new Date).getTime(),n=0;n<1e7&&!((new Date).getTime()-t>e);n++);}function fireMouseEvents(e,t){if(getElementByXpath(e)&&t&&t.length)for(var n in t){var a=t[n];if(getElementByXpath(e).fireEvent)return getElementByXpath(e).fireEvent(\"on\"+a),!0;var r=document.createEvent(\"MouseEvents\");return r.initEvent(a,!0,!1),getElementByXpath(e).dispatchEvent(r),!0}}fireMouseEvents("
								+ xpath + ",['mousedown'])");
				// fireMouseEvents("+escape(xpath)+",['mousedown'])
				System.out.println(obj.toString());
				System.out.println("inside clickusingmouse inside if statement,script got executed");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to clickusingmouse : ", null, e.toString(), "error");
			return false;
		}

	}

	public boolean selectitemfromcustomdropdownxpath(String xpath2, String dynamicxpath) {
		try {
			JSONObject element1 = null;
			JSONObject element2 = null;

			JsonNode attribute = InitializeDependence.findattr(curObject.getAttributes(), "xpath");

//			xpath2 = escape(xpath2);
			if ((element1 = uniqueElement(curObject)) != null) {
				do {
					Thread.sleep(100);
					String ele = escape(element1.getString("value"));
//					ele.replace('\'','"');
					clickusingmouse(escape(element1.getString("value")));
					System.out.println("outside select if statement");

					// taking time in this step
					if ((element2 = fnElements("xpath", xpath2.replaceAll("#replace", dynamicxpath),
							"unique")) != null) {
						System.out.println("inside select if statement,click will happen now");
						System.out.println(xpath2.replaceAll("#replace", dynamicxpath));
						// click("xpath", attribute.get("value").asText().replaceAll("#replace",
						// dynamicxpath));
						click("xpath", xpath2.replaceAll("#replace", dynamicxpath));
						break;
					} else
						continue;
				} while (element2 == null);
			}

			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to selectitemfromcustomdropdownxpath : ", null,
					e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action selects the given item from the custom dropdown", action_displayname = "selectitemfromcustomdropdown", actionname = "selectitemfromcustomdropdown", paraname_equals_curobj = "false", paramnames = { "xpath2" }, paramtypes = { "String" })
	public boolean selectitemfromcustomdropdown(String xpath2) {

		try {

			String create_xpath = "//*[@id='menu-']/div[3]/ul";

			int itemno = 0;

			String original_xpath;

			JSONObject element1 = null;

			JSONObject element2 = null;

//            xpath2 = escape(xpath2);

			if ((element1 = uniqueElement(curObject)) != null) {

				do {

					Thread.sleep(100);

					String ele = escape(element1.getString("value"));

//                    ele.replace('\'','"');
//					Thread.sleep(2000);
					clickusingmouse(escape(element1.getString("value")));
//					clickusingmouse();
					clickwhenisenablaedandisdisplayed();

//					if ((element2 = fnElements("xpath", xpath2, "unique")) != null) {

					JSONObject dropdown_list = executeScript2(
							"function getElementByXpath(e){return document.evaluate(e,document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue};function list_a(){var e = getElementByXpath(\"//*[@id='menu-']/div[3]/ul\");return e.innerText;};return list_a();");

					String[] list_drop = dropdown_list.get("value").toString().split("\n");

					for (int i = 0; i < list_drop.length; i++) {

						if (list_drop[i].equals(xpath2)) {

							itemno = i + 1;

						}

						else {

							continue;

						}

					}

					if (itemno == 0) {

						logger.info("item not present in the drop down");

						return false;

					}

					else {

						original_xpath = create_xpath + "/li[" + itemno + "]";

					}

					logger.info("original xpath" + original_xpath);
//						Thread.sleep(1500);
					click("xpath", original_xpath);

					Thread.sleep(2000);

					break;

//					} else
//
//						continue;

				} while (element2 == null);

			}

			return true;

		} catch (Exception e) {

			return false;

		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action picks the given date", action_displayname = "datepicker", actionname = "datepicker", paraname_equals_curobj = "false", paramnames = { "Date" }, paramtypes = { "String" })
	public boolean datepicker(String Date) {

		try {
			String Udate = Date.split("/")[1];
			String Umonth = Date.split("/")[0];
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern("MMMM");
			JSONObject idnamevalue;
			idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");

			Date actdate = new Date(Date);
			DateTime dtOrg = new DateTime(actdate);
			Umonth = dtOrg.toString(DateTimeFormat.forPattern("MMMM"));
			String Uyear = Date.split("/")[2];
			click();
			JSONObject elements = findElements("xpath",
					"//div[contains(@class,'MuiPickersCalendarHeader-transitionContainer')]/p[contains(@class,'MuiTypography-root')]")
							.getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				int count = 0;
				do {
					int flag;
					JSONArray elemnt_list;

					try {
						Thread.sleep(2000);

						if ((innertext = getElementproperty(elementId, "innerText")) == null && (elements = fnElements(
								"xpath",
								"//div[contains(@class,'MuiPickersCalendarHeader-transitionContainer')]/p[contains(@class,'MuiTypography-root')]",
								"unique")) != null) {

							elemnt_list = findElements("xpath",
									"//div[contains(@class,'MuiPickersCalendarHeader-transitionContainer')]/p[contains(@class,'MuiTypography-root')]");

							if (elemnt_list.length() != 0 && elemnt_list.length() <= 1)
								elements = elemnt_list.getJSONObject(0);
							else {
								throw new Exception("click fail");
							}
							iterator = elements.keys();
							key = null;
							if (elements.length() == 1) {
								while (iterator.hasNext()) {
									key = iterator.next();
								}
								clickelementId = elements.getString(key);
								if (elemnt_list.length() == 1
										&& (innertext = getElementproperty(elements.getString(key),
												"innerText")) != null)
									System.out.println(innertext);
								else {
									innertext = getElementproperty(elements.getString(key), "textContent");
									System.out.println(innertext);
								}

							}
						}
					} catch (Exception e) {
						logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(),
								"error");
						System.out.println(e);
					}
					if (innertext.contains(Umonth)) {
						flag = 1;
						break;
					} else {

						JSONArray elementarr = null;
						String calEle;
						elementarr = findElements("xpath",
								"//button[contains(@class,'MuiPickersCalendarHeader-iconButton')][1]");

						JSONObject elementcal = null;

						if (elementarr.length() != 0 && elementarr.length() <= 1) {
							elementcal = elementarr.getJSONObject(0);

						}

						else {
							System.out.println("month change element not found");
						}

						Iterator<String> iterator2 = elementcal.keys();
						String key2 = null;
						if (elementcal.length() == 1) {
							while (iterator2.hasNext()) {
								key2 = iterator2.next();
							}
							calEle = elementcal.getString(key2);
							boolean elem = isElementenabled(calEle);
							if (elem && count == 0) {

								click("xpath", "//button[contains(@class,'MuiPickersCalendarHeader-iconButton')][1]");

							}

							else

							{
								click("xpath", "//button[contains(@class,'MuiPickersCalendarHeader-iconButton')][2]");
								count++;
							}

						}

					}
				} while (!innertext.contains(Umonth));

				// date
				click("xpath", "(//div[contains(@class,'MuiPickersCalendar-transitionContainer')]//p[text()='" + Udate
						+ "'])[last()]");

				click(identifiername, value);

				Thread.sleep(1000);

				// year (toolbar)
				click("xpath", "//button[contains(@class,'MuiPickersToolbarButton-toolbarBtn')][1]");

				// year (choose)
				clickusingjs("xpath",
						"//div[contains(@class,'MuiPickersYearSelection-container')]/div[text()='" + Uyear + "']");
				click();
				Thread.sleep(2000);
			}
			return true;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(), "error");

			System.out.println(e);
			return false;
		}

	}

//	public boolean entertextusingrobo(String valuetoenter) throws InterruptedException, AWTException {
//
//		try {
//			Thread.sleep(1000);
//			if (click()) {
//				Thread.sleep(2000);
//
//				Robot robot = new Robot();
//				for (char c : valuetoenter.toCharArray()) {
//					int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
//					if (KeyEvent.CHAR_UNDEFINED == keyCode) {
//						throw new RuntimeException("Key code not found for character '" + c + "'");
//					}
//					robot.keyPress(keyCode);
//					robot.delay(100);
//					robot.keyRelease(keyCode);
//					robot.delay(100);
//				}
//
//			} else
//
//				return false;
//
//		}
//
//		catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Failed Execute to entertextusingrobo : ", null, e.toString(),
//					"error");
//			e.printStackTrace();
//
//			return false;
//
//		}
//
//		return true;
//
//	}
	@Sqabind(object_template="sqa_record",action_description = "This action enters the given text using Robot class", action_displayname = "entertextusingrobo", actionname = "entertextusingrobo", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean entertextusingrobo(String valuetoenter) throws InterruptedException, AWTException {
		try {
			Thread.sleep(1000);
			if (click()) {
				Thread.sleep(2000);
				Robot robot = new Robot();
				for (char c : valuetoenter.toCharArray()) {
					int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
					if (keyCode == 512) {
						robot.keyPress(KeyEvent.VK_SHIFT);
						robot.keyPress(KeyEvent.VK_2);
						robot.keyRelease(KeyEvent.VK_SHIFT);
						robot.keyRelease(KeyEvent.VK_2);
					} else {
						if (KeyEvent.CHAR_UNDEFINED == keyCode) {
							throw new RuntimeException("Key code not found for character '" + c + "'");
						}
						robot.keyPress(keyCode);
						robot.delay(100);
						robot.keyRelease(keyCode);
						robot.delay(100);
					}
				}
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to entertextusingrobo : ", null, e.toString(),
					"error");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Sqabind(object_template="sqa_record",action_description = "This action validates the given time", action_displayname = "validatetime", actionname = "validatetime", paraname_equals_curobj = "false", paramnames = { "valuetovalidate" }, paramtypes = { "String" })
	public boolean validatetime(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return validatetime(identifiername, value, valuetovalidate);

	}

//	public static void main(String[] args) {
//		WebMethod obj = new WebMethod();
//		obj.validatetime("","","12/8/2021 01:48 PM");
//	}
	public boolean validatetime(String identifiername, String value, String valuetovalidate) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;

//			Uptime
			String time[];
			int day;
			int min, hr, hr2, min2;
			time = valuetovalidate.split(" ");

			String final_time = time[1];
			String day1 = time[0];
			String day2 = time[0];
			String time2[];

			time2 = final_time.split(":");
			hr = Integer.parseInt(time2[0]);
			min = Integer.parseInt(time2[1]);

			hr2 = Integer.parseInt(time2[0]);
			min2 = Integer.parseInt(time2[1]);

			String hour = "0";
			String minute = "0";
			String updated_time = "";
			String day_up = "";

			if (min == 59) {
				if (hr < 12) {
					hr++;
					min = 0;

				}

				else if (hr == 12) {
					hr = 1;
					min = 0;
				}
			}

			else {
				min++;
			}

			System.out.println(time[2]);
			if (hr == 12 && Integer.parseInt(time2[0]) == 11) {
				if (time[2].equals("PM")) {
					time[2] = "AM";
					day = Integer.parseInt(day1);
					day--;

					if (Integer.toString(day).length() == 1) {

						day_up = "0".concat(Integer.toString(day));

					}

				}

				else {
					day_up = day1;
					time[2] = "PM";
				}
			}

			else {
				day_up = day1;
			}

			String hou = Integer.toString(hr);
			String minu = Integer.toString(min);

			if (Integer.toString(hr).length() == 1 || Integer.toString(min).length() == 1) {

				if (Integer.toString(hr).length() == 1) {
					hou = hour.concat(Integer.toString(hr));
					updated_time = hou.concat(":" + minu);

				}

				if (Integer.toString(min).length() == 1) {

					minu = minute.concat(Integer.toString(min));
					updated_time = hou.concat(":" + minu);

				}

				System.out.println(updated_time);

			}

			else {

				updated_time = Integer.toString(hr).concat(":" + Integer.toString(min));
			}

			String up_time = day_up.concat(" " + updated_time.concat(" " + time[2]));

			// downtime
			hour = "0";
			minute = "0";
			updated_time = "";
			if (min2 == 0) {

				if (hr2 <= 12 && hr2 != 1) {
					hr2--;
					min2 = 59;

				}

				else if (hr2 == 1) {
					min2 = 59;
					hr2 = 12;
				}

			} else {
				min2--;
			}

			if ((hr2 == 11 && Integer.parseInt(time2[0]) == 12)) {
				if (time[2].equals("PM")) {
					time[2] = "AM";

				}

				else {
					time[2] = "PM";
				}
			}

			String hou1 = Integer.toString(hr2);
			String minu1 = Integer.toString(min2);

			if (Integer.toString(hr2).length() == 1 || Integer.toString(min2).length() == 1) {

				if (Integer.toString(hr2).length() == 1) {
					hou1 = hour.concat(Integer.toString(hr2));
					updated_time = hou1.concat(":" + minu1);

				}

				if (Integer.toString(min2).length() == 1) {

					minu1 = minute.concat(Integer.toString(min2));
					updated_time = hou1.concat(":" + minu1);

				}

				System.out.println(updated_time);

			}

			else {

				updated_time = Integer.toString(hr2).concat(":" + Integer.toString(min2));
			}

			String down_time = time[0].concat(" " + updated_time.concat(" " + time[2]));

			System.out.println("up_time :" + up_time);
			System.out.println("down_time :" + down_time);

			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {
//					 String text = getElementtext(elementId).replace("\n", "");
					String text = getElementproperty(elementId, "innerText").replace("\n", "");
					this.setExepectedValue(text);
					if (text.contains(valuetovalidate) || text.contains(up_time) || text.contains(down_time)) {
						logger.info("Executed  validatePartialText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						return true;
					} else {
						logger.info("Failed to Execute  validatePartialText actualValue :{} with expectedValue:{} ",
								text, valuetovalidate);
						logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, "",
								"error");
						res = false;
					}
				} else {
					logger.info("Failed to Execute  validatePartialText Because Element not Found");
					logerOBJ.customlogg(userlogger, "Step Failed: Element not found. ", null, "", "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute  validatePartialText ");
			logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, e.toString(),
					"error");
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action validates the given time partially", action_displayname = "validatepartialtime", actionname = "validatepartialtime", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "String" })
	public boolean validatepartialtime(String valuetovalidate) {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		String identifiername = idnamevalue.getString("identifiername");
		String value = idnamevalue.getString("value");
		return validatepartialtime(identifiername, value, valuetovalidate);

	}

	// 11:26 AM
	public boolean validatepartialtime(String identifiername, String value, String valuetovalidate) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			Boolean res = false;

//			Uptime
			String time[];
			int day;
			int min, hr, hr2, min2;
			time = valuetovalidate.split(" ");

			String final_time = time[0];
//			String day1 = time[0];
//			String day2 = time[0];
			String time2[];

			time2 = final_time.split(":");
			hr = Integer.parseInt(time2[0]);
			min = Integer.parseInt(time2[1]);

			hr2 = Integer.parseInt(time2[0]);
			min2 = Integer.parseInt(time2[1]);

			String hour = "0";
			String minute = "0";
			String updated_time = "";
			String day_up = "";

			if (min == 59) {
				if (hr < 12) {
					hr++;
					min = 0;

				}

				else if (hr == 12) {
					hr = 1;
					min = 0;
				}
			}

			else {
				min++;
			}

			if (hr == 12 && Integer.parseInt(time2[0]) == 11) {
				if (time[1].equals("PM")) {
					time[1] = "AM";

				}

				else {

					time[1] = "PM";
				}
			}

			String hou = Integer.toString(hr);
			String minu = Integer.toString(min);

			if (Integer.toString(hr).length() == 1 || Integer.toString(min).length() == 1) {

				if (Integer.toString(hr).length() == 1) {
					hou = hour.concat(Integer.toString(hr));
					updated_time = hou.concat(":" + minu);

				}

				if (Integer.toString(min).length() == 1) {

					minu = minute.concat(Integer.toString(min));
					updated_time = hou.concat(":" + minu);

				}

				System.out.println(updated_time);

			}

			else {

				updated_time = Integer.toString(hr).concat(":" + Integer.toString(min));
			}

			String up_time = updated_time.concat(" " + time[1]);

			// downtime
			hour = "0";
			minute = "0";
			updated_time = "";
			if (min2 == 0) {

				if (hr2 <= 12 && hr2 != 1) {
					hr2--;
					min2 = 59;

				}

				else if (hr2 == 1) {
					min2 = 59;
					hr2 = 12;
				}

			} else {
				min2--;
			}

			if ((hr2 == 11 && Integer.parseInt(time2[0]) == 12)) {
				if (time[1].equals("PM")) {
					time[1] = "AM";

				}

				else {
					time[1] = "PM";
				}
			}

			String hou1 = Integer.toString(hr2);
			String minu1 = Integer.toString(min2);

			if (Integer.toString(hr2).length() == 1 || Integer.toString(min2).length() == 1) {

				if (Integer.toString(hr2).length() == 1) {
					hou1 = hour.concat(Integer.toString(hr2));
					updated_time = hou1.concat(":" + minu1);

				}

				if (Integer.toString(min2).length() == 1) {

					minu1 = minute.concat(Integer.toString(min2));
					updated_time = hou1.concat(":" + minu1);

				}

				System.out.println(updated_time);

			}

			else {

				updated_time = Integer.toString(hr2).concat(":" + Integer.toString(min2));
			}

			String down_time = updated_time.concat(" " + time[1]);

			System.out.println("up_time :" + up_time);
			System.out.println("down_time :" + down_time);

			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				if (elementId != null) {

					String text = getElementproperty(elementId, "innerText").replace("\n", "");
					this.setExepectedValue(text);
					if (text.contains(valuetovalidate) || text.contains(up_time) || text.contains(down_time)) {
						logger.info("Executed  validatePartialText actualValue :{} with expectedValue:{} ", text,
								valuetovalidate);
						return true;
					} else {
						logger.info("Failed to Execute  validatePartialText actualValue :{} with expectedValue:{} ",
								text, valuetovalidate);
						logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, "",
								"error");
						res = false;
					}
				} else {
					logger.info("Failed to Execute  validatePartialText Because Element not Found");
					logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, "", "error");
					res = false;
				}
			}
			return res;
		} catch (Exception e) {
			logger.info("Failed to Execute  validatePartialText ");
			logerOBJ.customlogg(userlogger, "Step Failed Execute to validatePartialText : ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is present or not", action_displayname = "elementpresent", actionname = "elementpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementpresent() {

		JSONObject idnamevalue;

		if ((idnamevalue = uniqueElement(curObject)) != null) {
			System.out.println(idnamevalue);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return true;
		} else {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Elemnent not present", null, "", "error");
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is not present", action_displayname = "elementnotpresent", actionname = "elementnotpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotpresent() {
		JSONObject idnamevalue;

		if ((idnamevalue = uniqueElement(curObject)) != null) {
			System.out.println(idnamevalue);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			return false;
		} else {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Elemnent not present", null, "", "error");
			return true;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is displayed or not", action_displayname = "elementvisible", actionname = "elementvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementvisible() {
		try {
			clickelementId = getElementId();

			if (iselementDisplayed(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visible ", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	public boolean elementvisible(String xpath) {
		try {
			// clickelementId = getElementId();

			if (iselementDisplayed(xpath)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visible ", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is not displayed", action_displayname = "elementnotvisible", actionname = "elementnotvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotvisible() {
		try {

			clickelementId = getElementId();

			if (iselementDisplayed(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visi ", null, "", "error");
				return false;
			}

			else {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element contains the given text", action_displayname = "elementcontainstext", actionname = "elementcontainstext", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementcontainstext(String valuecheck) {

		try {

			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "innerText").contains(valuecheck)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element contains the given value", action_displayname = "elementcontainsvalue", actionname = "elementcontainsvalue", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementcontainsvalue(String valuecheck) {

		try {

			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "value").contains(valuecheck)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element not contains the given text", action_displayname = "elementnotcontainstext", actionname = "elementnotcontainstext", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementnotcontainstext(String valuecheck) {

		try {
			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "innerText").contains(valuecheck)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return false;
			}

			else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element not contains the given value", action_displayname = "elementnotcontainsvalue", actionname = "elementnotcontainsvalue", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementnotcontainsvalue(String valuecheck) {

		try {
			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "value").contains(valuecheck)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return false;
			}

			else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is a clickable element or not", action_displayname = "elementisclickable", actionname = "elementisclickable", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementisclickable() {

		try {
			clickelementId = getElementId();

			if (getElementcssvalue(clickelementId, "cursor").equals("pointer")) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element is not a clickable element", action_displayname = "elementnotclickable", actionname = "elementnotclickable", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotclickable() {
		try {

			clickelementId = getElementId();

			if (getElementcssvalue(clickelementId, "cursor").equals("pointer")) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return false;
			}

			else {
				return true;
			}

		} catch (Exception e) {
			return false;
		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action checks whether element text matches the given text", action_displayname = "elementmatchestext", actionname = "elementmatchestext", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementmatchestext(String valuecheck) {
		try {

			clickelementId = getElementId();
			String element_text = getElementproperty(clickelementId, "innerText");
			Pattern p = Pattern.compile(valuecheck, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(element_text);
			boolean match = m.find();

			if (match) {
				return true;
			}

			else
				return false;

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether element value matches the given value", action_displayname = "elementmatchesvalue", actionname = "elementmatchesvalue", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementmatchesvalue(String valuecheck) {
		try {

			clickelementId = getElementId();
			String element_text = getElementproperty(clickelementId, "value");
			Pattern p = Pattern.compile(valuecheck, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(element_text);
			boolean match = m.find();

			if (match) {
				return true;
			}

			else
				return false;

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether element value not matches with the given value", action_displayname = "elementnotmatchesvalue", actionname = "elementnotmatchesvalue", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementnotmatchesvalue(String valuecheck) {
		try {

			clickelementId = getElementId();
			String element_text = getElementproperty(clickelementId, "value");
			Pattern p = Pattern.compile(valuecheck, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(element_text);
			boolean match = m.find();

			if (match) {
				return false;
			}

			else
				return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether element text not matches with the given text", action_displayname = "elementnotmatchestext", actionname ="elementnotmatchestext", paraname_equals_curobj = "false", paramnames = { "valuecheck" }, paramtypes = { "String" })
	public boolean elementnotmatchestext(String valuecheck) {
		try {

			clickelementId = getElementId();
			String element_text = getElementproperty(clickelementId, "text");
			Pattern p = Pattern.compile(valuecheck, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(element_text);
			boolean match = m.find();

			if (match) {
				return false;
			}

			else
				return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether an element is enabled or not", action_displayname = "elementisenabled", actionname = "elementisenabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementisenabled() {
		try {
			clickelementId = getElementId();
			if (isElementenabled(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visi ", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether an element is displayed or not", action_displayname = "elementdisabled", actionname = "elementdisabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementdisabled() {
		try {
			clickelementId = getElementId();
			if (!isElementenabled(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visi ", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether a checkbox is checked or not", action_displayname = "elementchecked", actionname = "elementchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementchecked() {
		try {
			clickelementId = getElementId();

			if (isElementselected(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visi ", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether a checkbox is not checked", action_displayname = "elementnotchecked", actionname ="elementnotchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotchecked() {
		try {
			clickelementId = getElementId();
			if (!isElementselected(clickelementId)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Elemnent visi ", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is equal to the given text", action_displayname = "elementequaltotext", actionname = "elementequaltotext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementequaltotext(String value1) {
		try {

			clickelementId = getElementId();

			String a = getElementproperty(clickelementId, "innertext");
			if (getElementproperty(clickelementId, "innerText").equals(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is equal to the given value", action_displayname = "elementequaltovalue", actionname = "elementequaltovalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementequaltovalue(String value1) {
		try {

			clickelementId = getElementId();

			String a = getElementproperty(clickelementId, "value");
			if (getElementproperty(clickelementId, "value").equals(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is not equal to the given text", action_displayname = "elementnotequalstotext", actionname = "elementnotequalstotext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementnotequalstotext(String value1) {
		try {

			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "innerText").equals(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return false;
			}

			else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is not equal to the given value", action_displayname = "elementnotequaltovalue", actionname = "elementnotequaltovalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementnotequaltovalue(String value1) {
		try {

			clickelementId = getElementId();

			if (getElementproperty(clickelementId, "value").equals(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return false;
			}

			else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is lesser than the given text", action_displayname = "elementlesserthantext", actionname = "elementlesserthantext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementlesserthantext(String value1) {
		try {

			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "innerText")) < Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			logger.info(e.toString());
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is lesser than the given value", action_displayname = "elementlesserthanvalue", actionname = "elementlesserthanvalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementlesserthanvalue(String value1) {
		try {

			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "value")) < Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			logger.info(e.toString());
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is lesser than or equal to the given text", action_displayname = "elementlesserthanequaltotext", actionname = "elementlesserthanequaltotext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementlesserthanequaltotext(String value1) {
		try {

			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "innerText")) <= Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is lesser than or equal to the given value", action_displayname = "elementlesserthanequaltovalue", actionname = "elementlesserthanequaltovalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementlesserthanequaltovalue(String value1) {
		try {

			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "value")) <= Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is greater than the given text", action_displayname = "elementgreaterthantext", actionname = "elementgreaterthantext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementgreaterthantext(String value1) {
		try {
			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "innerText")) > Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is greater than the given value", action_displayname = "elementgreaterthanvalue", actionname = "elementgreaterthanvalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementgreaterthanvalue(String value1) {
		try {
			clickelementId = getElementId();

			if (Integer.parseInt(getElementproperty(clickelementId, "value")) > Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element text is greater than or equal to the given text", action_displayname = "elementgreaterthanequaltotext", actionname = "elementgreaterthanequaltotext", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementgreaterthanequaltotext(String value1) {
		try {
			clickelementId = getElementId();
			if (Integer.parseInt(getElementproperty(clickelementId, "innerText")) >= Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the element value is greater than or equal to the given value", action_displayname = "elementgreaterthanequaltovalue", actionname ="elementgreaterthanequaltovalue", paraname_equals_curobj = "false", paramnames = { "value1" }, paramtypes = { "String" })
	public boolean elementgreaterthanequaltovalue(String value1) {
		try {
			clickelementId = getElementId();
			if (Integer.parseInt(getElementproperty(clickelementId, "value")) >= Integer.parseInt(value1)) {
				logerOBJ.customlogg(userlogger, "Step Passed: Value present in the elemnt", null, "", "error");
				return true;
			}

			else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

	public String getElementId() {

		try {
			JSONArray elementsarr;

			JSONObject idnamevalue;

			if ((idnamevalue = uniqueElement(curObject)) != null) {
				System.out.println(idnamevalue);
				String identifiername = idnamevalue.getString("identifiername");
				String value = idnamevalue.getString("value");

				elementsarr = findElements(identifiername, value);
				JSONObject elements = null;

				System.out.println("elemnt arr :" + elementsarr);

				if (elementsarr.length() != 0 && elementsarr.length() <= 1)
					elements = elementsarr.getJSONObject(0);
				else {
// 					throw new Exception("click fail");
					return "error";

				}
				Iterator<String> iterator = elements.keys();
				String key = null;
				if (elements.length() == 1) {
					while (iterator.hasNext()) {
						key = iterator.next();
					}
					clickelementId = elements.getString(key);

				}

			}
			return clickelementId;
		} catch (Exception e) {
			return "error";
		}
	}

	public String getElementId(String identifier, String value) {

		try {
			JSONArray elementsarr;

			JSONObject idnamevalue;

			elementsarr = findElements(identifier, value);
			JSONObject elements = null;

			System.out.println("elemnt arr :" + elementsarr);

			if (elementsarr.length() != 0 && elementsarr.length() <= 1)
				elements = elementsarr.getJSONObject(0);
			else {
// 					throw new Exception("click fail");
				return "error";

			}
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				clickelementId = elements.getString(key);

			}
			return clickelementId;
		} catch (Exception e) {
			return "error";
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "Selects date from calender", action_displayname = "daetpickerthetaedge", actionname = "daetpickerthetaedge", paraname_equals_curobj = "false", paramnames = { "Date" }, paramtypes = { "String" })
	public boolean daetpickerthetaedge(String Date) {

		try {
			String Udate = Date.split("/")[1];
			String Umonth = Date.split("/")[0];
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern("MMM");
			JSONObject idnamevalue;
			idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");

			Date actdate = new Date(Date);
			DateTime dtOrg = new DateTime(actdate);
			String Tmonth = dtOrg.toString(date);
			String Uyear = Date.split("/")[2];
			click();

			// calender xpath (mon yr)
			JSONObject elements = findElements("xpath",
					"//div[contains(@style,'display: block')]//span[@class='calendar-text']").getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				int count = 0;
				String month = null;
				do {
					int flag;
					JSONArray elemnt_list;

					try {
						Thread.sleep(2000);

						if ((innertext = getElementproperty(elementId, "innerText")) == null
								&& (elements = fnElements("xpath",
										"//div[contains(@style,'display: block')]//span[@class='calendar-text']",
										"unique")) != null) {

							elemnt_list = findElements("xpath",
									"//div[contains(@style,'display: block')]//span[@class='calendar-text']");

							if (elemnt_list.length() != 0 && elemnt_list.length() <= 1)
								elements = elemnt_list.getJSONObject(0);
							else {
								throw new Exception("click fail");
							}
							iterator = elements.keys();
							key = null;
							if (elements.length() == 1) {
								while (iterator.hasNext()) {
									key = iterator.next();
								}
								clickelementId = elements.getString(key);
								if (elemnt_list.length() == 1
										&& (innertext = getElementproperty(elements.getString(key),
												"innerText")) != null)
									System.out.println(innertext);
								else {
									innertext = getElementproperty(elements.getString(key), "textContent");
									System.out.println(innertext);
								}

							}
						}
					} catch (Exception e) {
						logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(),
								"error");
						System.out.println(e);
					}
					if (innertext.contains(Tmonth)) {
						flag = 1;
						break;
					} else {
						month = innertext.split(" ")[0];
						String year = innertext.split(" ")[1];

						// check for year
						while (!year.equals(Uyear)) {
							if (Integer.parseInt(Uyear) > Integer.parseInt(year)) {
								clickusingjs("xpath",
										"//div[contains(@style,'display: block')]//div[@class='calendar-nav calendar-nextyear']");
								Thread.sleep(1000);
							} else if (Integer.parseInt(Uyear) < Integer.parseInt(year)) {
								clickusingjs("xpath",
										"//div[contains(@style,'display: block')]//div[@class='calendar-nav calendar-prevyear']");
								Thread.sleep(1000);
							} else
								break;
							year = getElementproperty(elementId, "innerText").split(" ")[0];

						}

						// check for month
						SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
						String dt = Tmonth + " " + Uyear;
						Date firstDate = sdf.parse(dt);
						Date secondDate = sdf.parse(innertext);
						while (!month.equals(Tmonth)) {
							if (firstDate.compareTo(secondDate) > 0) {
								clickusingjs("xpath",
										"//div[contains(@style,'display: block')]//div[@class='calendar-nav calendar-nextmonth']");
								Thread.sleep(2000);
							} else {
								clickusingjs("xpath",
										"//div[contains(@style,'display: block')]//div[@class='calendar-nav calendar-prevmonth']");
								Thread.sleep(2000);
								// Thread.sleep(2000);
							}
							month = getElementproperty(elementId, "innerText").split(" ")[0];

						}

					}
				} while (!month.contains(Tmonth));

				// date
				click("xpath", "//div[contains(@style,'display: block')]//td[text()='" + Udate
						+ "'  and not(contains(@class,'other-month'))]");
			}
			return true;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(), "error");

			System.out.println(e);
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "Selects date from calender", action_displayname = "datepickerakpk", actionname = "datepickerakpk", paraname_equals_curobj = "false", paramnames = { "Date" }, paramtypes = { "String" })
	public boolean datepickerakpk(String Date) {
		try {
			String Udate = Date.split("/")[1];
			String Umonth = Date.split("/")[0];
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern("MMMM");
			JSONObject idnamevalue;
			idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			Date actdate = new Date(Date);
			DateTime dtOrg = new DateTime(actdate);
			String Tmonth = dtOrg.toString(date);
			String Uyear = Date.split("/")[2];
			click();
			// calender xpath (mon yr)

			JSONObject elements = findElements("xpath", "(//span[@class='flatpickr-day '])[1]").getJSONObject(0);
			String attribute = getattribute("xpath", "(//span[@class='flatpickr-day '])[1]", "aria-label");
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				int count = 0;
				// String month = null;
				String Fmonth = attribute.split(" ")[0];
				String Fyear = attribute.split(" ")[attribute.split(" ").length - 1];
				do {
					if (Fmonth.contains(Tmonth)) {
						int flag = 1;
						break;
					} else {
						// check for year
						while (!Fyear.equals(Uyear)) {
							if (Integer.parseInt(Uyear) > Integer.parseInt(Fyear)) {
								click("xpath",
										"(//span[contains(@class,'flatpickr-prev-month')]/..//span[@class='arrowUp'])[1]");
								Thread.sleep(1000);
							} else if (Integer.parseInt(Uyear) < Integer.parseInt(Fyear)) {
								click("xpath",
										"(//span[contains(@class,'flatpickr-prev-month')]/..//span[@class='arrowDown'])[1]");
								Thread.sleep(1000);
							} else
								break;
							Fyear = getattribute("xpath", "(//span[@class='flatpickr-day '])[1]", "aria-label")
									.split(" ")[attribute.split(" ").length - 1];
						}
						// check for month
						SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
						String dt = Tmonth + " " + Uyear;
						String dt1 = Fmonth + " " + Fyear;
						Date firstDate = sdf.parse(dt);
						Date secondDate = sdf.parse(dt1);
						while (!Fmonth.equals(Tmonth)) {
							if (firstDate.compareTo(secondDate) > 0) {
								click("xpath",
										"(//span[contains(@class,'flatpickr-prev-month')]/../span[@class='flatpickr-next-month']//*[name()='svg'])[1]");
								Thread.sleep(2000);
							} else {
								click("xpath",
										"(//span[contains(@class,'flatpickr-prev-month')]/../span[@class='flatpickr-prev-month']//*[name()='svg'])[1]");
								Thread.sleep(2000);
								// Thread.sleep(2000);
							}
							Fmonth = getattribute("xpath", "(//span[@class='flatpickr-day '])[1]", "aria-label")
									.split(" ")[0];
						}
					}
				} while (!Fmonth.contains(Tmonth));
				// date
				click("xpath", "//div[contains(@class,'open')]//span[text()='" + Integer.parseInt(Udate)
						+ "' and not(contains(@class,'nextMonthDay') and contains(@class,'nextMonthDay'))]");
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(), "error");
			System.out.println(e);
			return false;
		}
	}

	public String getattribute(String identifiername, String value, String fieldName) {
		try {
			JSONObject elements = findElements(identifiername, value).getJSONObject(0);
			Iterator<String> iterator = elements.keys();
			String key = null;
			if (elements.length() == 1) {
				while (iterator.hasNext()) {
					key = iterator.next();
				}
				String elementId = elements.getString(key);
				String innertext = null;
				if ((innertext = getElementppt(elementId, fieldName)) == null) {
					innertext = getElementppt(elementId, fieldName);
				}
				return innertext;

			}
			return null;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return null;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action gets the value of the given attribute name and stores it to runtime parameter", action_displayname = "getattributeandstore", actionname = "getattributeandstore", paraname_equals_curobj = "false", paramnames = { "fieldName","Paraname" }, paramtypes = { "String","String" })
	public boolean getattributeandstore(String fieldName, String Paraname) {
		try {
			JSONArray tabs = new JSONArray();
			Boolean flag = Boolean.valueOf(false);
			String objecttype = null;
			JSONObject idnamevalue = null;
			tabs = getWindowhandles();
			if (tabs.length() > 1)
				checkTab(this.curObject, tabs);
			if (checkFrame(this.curObject).booleanValue()) {
				flag = Boolean.valueOf(true);
				objecttype = "unique";
				for (JsonNode attr : this.curObject.getAttributes()) {
					if (attr.has("unique") && attr.get("unique").asBoolean()) {
						if (attr.get("name").asText().toLowerCase().contains("xpath")) {
							idnamevalue = fnElementsnotenabled("xpath", attr.get("value").asText(), objecttype);
							break;
						}
						if (attr.get("name").asText().toLowerCase().contains("css")) {
							idnamevalue = fnElementsnotenabled("css selector", attr.get("value").asText(), objecttype);
							break;
						}
						if (attr.get("name").asText().toLowerCase().equals("linktext")) {
							idnamevalue = fnElementsnotenabled("link text", attr.get("value").asText(), objecttype);
							break;
						} else if (attr.get("name").asText().toLowerCase().equals("class")) {
							idnamevalue = fnElements("class", Stream.of(attr.get("value").asText().split("\\s+"))
									.map(str -> "." + str).collect(joining(" ")), objecttype);
							break;
						} else if (attr.get("name").asText().toLowerCase().equals("id")) {
							idnamevalue = fnElements("css selector", Stream.of(attr.get("value").asText().split("\\s+"))
									.map(str -> "#" + str).collect(joining(" ")), objecttype);
							break;
						} else {
							idnamevalue = fnElements("name",
									String.format("*[name='%s']", attr.get("value").asText().replace("'", "\\'")),
									objecttype);
						}
					}
				}
				if (idnamevalue != null) {
					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					JSONObject re = new JSONObject();
					JSONObject elements = findElements(identifiername, value).getJSONObject(0);
					Iterator<String> iterator = elements.keys();
					String key = null;
					if (elements.length() == 1) {
						while (iterator.hasNext())
							key = iterator.next();
						String elementId = elements.getString(key);
						if (this.curExecution == null) {
							this.curExecution = new Setupexec();
							this.curExecution.setAuthKey((String) GlobalDetail.refdetails.get("authkey"));
							this.curExecution.setCustomerID(Integer
									.valueOf(Integer.parseInt((String) GlobalDetail.refdetails.get("customerId"))));
							this.curExecution.setProjectID(Integer
									.valueOf(Integer.parseInt((String) GlobalDetail.refdetails.get("projectId"))));
							this.curExecution.setServerIp((String) GlobalDetail.refdetails.get("executionIp"));
							this.header.put("content-type", "application/json");
							this.header.put("authorization", this.curExecution.getAuthKey());
						}
						String token = this.curExecution.getAuthKey();
						String[] split = token.split("\\.");
						String base64EncodedBody = split[1];
						Base64 base64Url = new Base64(true);
						JSONObject jsonObject = null;
						jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
						if (jsonObject.has("createdBy")) {
							re.put("paramname",
									String.valueOf(Paraname) + "_" + jsonObject.get("createdBy").toString());
						} else {
							re.put("paramname",
									String.valueOf(Paraname) + "_" + jsonObject.get("updatedBy").toString());
						}
						if (isElementenabled(elementId)) {
							re.put("paramvalue", "enabled");
							InitializeDependence.serverCall.postruntimeParameter(this.curExecution, re, this.header);
							return true;
						}
						re.put("paramvalue", "disabled");
						InitializeDependence.serverCall.postruntimeParameter(this.curExecution, re, this.header);
						return true;
					}
					return false;
				}
				return false;
			}
			return false;
		} catch (Exception e) {
			this.logerOBJ.customlogg(userlogger, "Step Failed Execute : ", null, "", "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action selects an item from the dropdown using keyboard", action_displayname = "dropdownselectusingkeyboard", actionname = "dropdownselectusingkeyboard", paraname_equals_curobj = "false", paramnames = { "value" }, paramtypes = { "String" })
	public boolean dropdownselectusingkeyboard(String value) {
		try {
			usingmousepointer();
			click();
			click();
			String id = null;

			for (JsonNode attr : this.curObject.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					if (attr.get("name").asText().toLowerCase().contains("id")) {
						id = attr.get("value").asText();
					}
				}
			}
			Robot robot = new Robot();
			for (int i = 0; i < 25; i++) {

				System.out.println("countttttttt :" + i);
				robot.keyPress(KeyEvent.VK_DOWN);
				Thread.sleep(2000);

				JSONObject city = executeScript2("var e=document.getElementById(" + "'" + id + "'"
						+ ");return e.options[e.selectedIndex].text;");

				logger.info(city.toString());
				this.setItemselected(value);

				if (value.equals(city.get("value"))) {
					robot.keyPress(KeyEvent.VK_ENTER);

					break;
				}

			}
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the given second value is greater than the first value", action_displayname = "dataincrease", actionname ="dataincrease", paraname_equals_curobj = "false", paramnames = { "ele1","ele2" }, paramtypes = { "String","String" })
	public boolean dataincrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 > item1) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the given second value is lesser than the first value", action_displayname = "datadecrease", actionname = "datadecrease", paraname_equals_curobj = "false", paramnames = { "ele1","ele2" }, paramtypes = { "String","String" })
	public boolean datadecrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 < item1) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action performs a mouse click", action_displayname = "clickusingmousepointer", actionname = "clickusingmousepointer", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickusingmousepointer() {
		try {
			Thread.sleep(1000);
			JSONObject WINDOWS_SIZE = getWindowrect();
			String elementId = getElementId();
			JSONObject object = getElementrect(elementId);
			int mask = Integer.parseInt(String.valueOf(InputEvent.BUTTON1_MASK));
			Robot robot = new Robot();
			robot.mouseMove(0, 0);
//		         Thread.sleep(5000);
//		          robot.mouseMove(594+10,373+115);

			System.out.println("x : " + object.getInt("x"));
			System.out.println("y : " + object.getInt("y"));
			System.out.println("width : " + object.getInt("width"));
//			System.out.println("height : " + object.getInt("height"));

			if (object.getInt("y") + 135 < WINDOWS_SIZE.getInt("height")) {
				robot.mouseMove(object.getInt("x") + 30, object.getInt("y") + 132);
			} else {
				robot.mouseMove(object.getInt("x") + 30, 132 - 560 + object.getInt("y"));
			}

			System.out.println(object.getInt("x") + 30 + "," + object.getInt("y") + 132);
			Thread.sleep(1000);
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

//		         Thread.sleep(5000);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean usingmousepointer() {
		try {

			JSONObject WINDOWS_SIZE = getWindowrect();
			String elementId = getElementId();
			JSONObject object = getElementrect(elementId);
			int mask = Integer.parseInt(String.valueOf(InputEvent.BUTTON1_MASK));
			Robot robot = new Robot();
			robot.mouseMove(0, 0);
//		         Thread.sleep(5000);
//		          robot.mouseMove(594+10,373+115);

			System.out.println("x : " + object.getInt("x"));
			System.out.println("y : " + object.getInt("y"));
			System.out.println("width : " + object.getInt("width"));
//			System.out.println("height : " + object.getInt("height"));

			if (object.getInt("y") + 135 < WINDOWS_SIZE.getInt("height")) {
				robot.mouseMove(object.getInt("x") + 30, object.getInt("y") + 132);
			} else {
				robot.mouseMove(object.getInt("x") + 30, 132 - 560 + object.getInt("y"));
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action enters text using mouse pointer", action_displayname = "entertextusingmousepointer", actionname = "entertextusingmousepointer", paraname_equals_curobj = "false", paramnames = { "value" }, paramtypes = { "String" })
	public boolean entertextusingmousepointer(String value) {
		try {
			clickusingmousepointer();
			entertextusingrobo(value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action launches the given URL with the given capabilities", action_displayname = "launchapplication", actionname = "launchapplication", paraname_equals_curobj = "false", paramnames = { "url","extracapabilities" }, paramtypes = { "String","String" })
	public boolean launchapplication(String url, String extracapabilities) {
		try {
			if (driver != null) {
				deleteSession();
				closeDriver();
				driver = null;
			}
			boolean val = false;
			String platform;
			if (SystemUtils.IS_OS_WINDOWS)
				platform = "windows";
			else if (SystemUtils.IS_OS_MAC)
				platform = "mac";
			else
				platform = "linux";
			val = webCapabilities(brname, platform, extracapabilities);
			if (brname.equals("edge"))
				maximizeScreen();
			if (val == true) {
				if (InitializeDependence.timeout == -1)
					val = settimeout(90000, 300000, 3600000);
				else
					val = settimeout(InitializeDependence.timeout, 300000, 3600000);
				if (val) {
					maximizeScreen();
					launchUrl(url);
					logger.info("Url Launched :{}", url);
				} else {
					logger.info("setting timeout failed");
					logerOBJ.customlogg(userlogger, "Step Failed: timeout ", null, "", "error");
				}
			}
			return val;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed  : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean webCapabilities(String browserName, String platformName, String extrajson) {
		try {
			browser = browserName;
			capabilities = new Webcapabilities();
			JSONObject alwaysmatch, cap, firstMatch;
			JSONArray firstMatchArray = new JSONArray();
			cap = new JSONObject();
			alwaysmatch = new JSONObject();
			firstMatch = new JSONObject();

			// String capname=null;
			JSONObject appendJson = new JSONObject();

			if (extrajson.equals("allow-location")) {

				appendJson.put("profile.default_content_setting_values.geolocation", 1);
			} else if (extrajson.equals("block-location")) {
				appendJson.put("profile.default_content_setting_values.geolocation", 2);
			}

			// if(extrajson)

			switch (browserName.toLowerCase().trim()) {
			case "chrome":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");

				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
//appending user capabilities.
					for (String key : appendJson.keySet()) {
						// based on you key types
						String keyStr = (String) key;
						String keyvalue = (String) appendJson.get(keyStr);
						preferences.put(keyStr, keyvalue);
					} // appending user capabilities...
					firstMatch.put("goog:chromeOptions", new JSONObject()
							.put("args", new JSONArray().put("start-maximized").put("--ignore-certificate-errors").put("--window-size=1920,1200")).put("prefs", preferences));

				} else {
					// appending user capabilities.
					JSONObject preferences = new JSONObject();
					preferences = appendJson;
					firstMatch.put("goog:chromeOptions", new JSONObject()
							.put("args", new JSONArray().put("start-maximized").put("--ignore-certificate-errors").put("--window-size=1920,1200")).put("prefs", preferences));

				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "chrome-incognito":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");
				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
					// appending user capabilities..
					for (String key : appendJson.keySet()) {
						// based on you key types
						String keyStr = (String) key;
						String keyvalue = (String) appendJson.get(keyStr);
						preferences.put(keyStr, keyvalue);
					}
					// appending user capabilities..
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args", new JSONArray().put("start-maximized").put("--incognito").put("--ignore-certificate-errors").put("--window-size=1920,1200"))
									.put("prefs", preferences));
				} else {
					// appending user capabilities.
					JSONObject preferences = new JSONObject();
					for (String key : appendJson.keySet()) {
						// based on you key types
						String keyStr = (String) key;
						String keyvalue = (String) appendJson.get(keyStr);
						preferences.put(keyStr, keyvalue);
					} // appending user capabilities...
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args", new JSONArray().put("start-maximized").put("--incognito").put("--ignore-certificate-errors").put("--window-size=1920,1200")));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "chrome-headless":
				alwaysmatch.put("browserName", "chrome");
				alwaysmatch.put("pageLoadStrategy", "normal");
				if (InitializeDependence.downloadpath != null) {
					JSONObject preferences = new JSONObject();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", InitializeDependence.downloadpath);
					firstMatch.put("goog:chromeOptions",
							new JSONObject().put("args",
									new JSONArray().put("start-maximized").put("--headless").put("--disable-gpu").put("--ignore-certificate-errors").put("--window-size=1920,1200"))
									.put("prefs", preferences));
				} else {
					firstMatch.put("goog:chromeOptions", new JSONObject().put("args",
							new JSONArray().put("start-maximized").put("--headless").put("--disable-gpu").put("--ignore-certificate-errors").put("--window-size=1920,1200")));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);

				break;
			case "firefox":
				alwaysmatch.put("browserName", "firefox");
				if (SystemUtils.IS_OS_WINDOWS) {
					firstMatch.put("moz:firefoxOptions", new JSONObject()
							.put("binary", "C:\\Program Files\\Mozilla Firefox\\firefox.exe").put("prefs", appendJson));
				} else {
					firstMatch.put("moz:firefoxOptions",
							new JSONObject().put("binary", "/Applications/Firefox.app/Contents/MacOS/firefox-bin")
									.put("prefs", appendJson));
				}
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}

				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);
				break;
			case "ie":
				alwaysmatch.put("browserName", "internet explorer");
				alwaysmatch.put("pageLoadStrategy", "eager");
				alwaysmatch.put("timeouts",
						new JSONObject().put("implicit", 0).put("pageLoad", 1000).put("script", 1000));
				if (platformName.equals("windows")) {
					firstMatch.put("platformName", "windows");
				} else {
					firstMatch.put("platformName", platformName);
				}
				firstMatchArray.put(firstMatch);
				cap.put("alwaysMatch", alwaysmatch);
				cap.put("firstMatch", firstMatchArray);
				cap.put("ignoreProtectedModeSettings", true);
				cap.put("nativeEvents", true);

				cap.put("javascriptEnabled", true);
				break;
			case "edge":
				cap.put("browserName", "edge");
				cap.put("platformName", "windows");
				break;
			case "safari":
				alwaysmatch.put("browserName", "Safari");
				alwaysmatch.put("platformName", "Mac");
				cap.put("alwaysMatch", alwaysmatch);

			default:
				break;
			}
			capabilities.setCapabilities(cap);
			driver = new WebDriver(capabilities);
			if (browserName.equals("edge"))
				GlobalDetail.webDriver.put(Thread.currentThread().getName(), driver);
			else
				GlobalDetail.webDriver.put(Thread.currentThread().getName(), driver);
			logger.info(capabilities.toString());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action launches the given URL in the specified browser", action_displayname = "launchapplicationwithspecificbrowser", actionname = "launchapplicationwithspecificbrowser", paraname_equals_curobj = "false", paramnames = { "url","browsername" }, paramtypes = { "String","String" })
	public boolean launchapplicationwithspecificbrowser(String url, String browsername) {
		try {

			System.out.println(Thread.currentThread().getName());
			InitializeDependence.webDriver.get(Thread.currentThread().getName()).brname = browsername;
			InitializeDependence.webDriver.get(Thread.currentThread().getName()).browser = browsername;
			if (driver != null) {
				deleteSession();
				closeDriver();
				driver = null;
			}
			boolean val = false;
			String platform;
			if (SystemUtils.IS_OS_WINDOWS)
				platform = "windows";
			else if (SystemUtils.IS_OS_MAC)
				platform = "mac";
			else
				platform = "linux";
			val = webCapabilities(browsername, platform);
			if (browsername.equals("edge"))
				maximizeScreen();
			if (val == true) {
				if (InitializeDependence.timeout == -1)
					val = settimeout(90000, 300000, 3600000);
				else
					val = settimeout(InitializeDependence.timeout, 300000, 3600000);
				if (val) {
					maximizeScreen();
					launchUrl(url);
					logger.info("Url Launched :{}", url);
				} else {
					logger.info("setting timeout failed");
					logerOBJ.customlogg(userlogger, "Step Failed: timeout ", null, "", "error");
				}
			}
			return val;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed  : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean InitializeWebBrowser(String url, String browsername) {

		try {

			System.out.println(Thread.currentThread().getName());

			InitializeDependence.webDriver.get(Thread.currentThread().getName()).brname = browsername;
			InitializeDependence.webDriver.get(Thread.currentThread().getName()).browser = browsername;
			if (driver != null) {
				deleteSession();
				closeDriver();
				driver = null;
			}
			boolean val = false;
			String platform;
			if (SystemUtils.IS_OS_WINDOWS)
				platform = "windows";
			else if (SystemUtils.IS_OS_MAC)
				platform = "mac";
			else
				platform = "linux";
			val = webCapabilities(browsername, platform);
			if (browsername.equals("edge"))
				maximizeScreen();
			if (val == true) {
				if (InitializeDependence.timeout == -1)
					val = settimeout(90000, 300000, 3600000);
				else
					val = settimeout(InitializeDependence.timeout, 300000, 3600000);
				if (val) {
					maximizeScreen();
					launchUrl(url);
					logger.info("Url Launched :{}", url);
				} else {
					logger.info("setting timeout failed");
					logerOBJ.customlogg(userlogger, "Step Failed: timeout ", null, "", "error");
				}
			}
			return val;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed  : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean dynamicexistcheck(String paramvalue) {

		String value_xpath = "";

		JSONArray tabs = new JSONArray();

		tabs = getWindowhandles();

		if (tabs.length() > 1)

			checkTab(curObject, tabs);
		for (JsonNode attr : curObject.getAttributes()) {

			if (attr.get("name").asText().toLowerCase().equals("xpath")) {

				value_xpath = attr.get("value").asText();

			}

		}

		value_xpath = value_xpath.replaceAll("#replace", paramvalue);

//	        curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), paramvalue));

//	        JSONObject idnamevalue = uniqueElement(curObject);

//	        String identifiername = idnamevalue.getString("identifiername");

//	        String value = idnamevalue.getString("value");

		JSONArray eleArray = new JSONArray();
		eleArray = findElements("xpath", value_xpath);

		if (eleArray.length() == 1) {

			logger.info("Element found");
			return true;

		} else {
			logger.info("Element not found with xpath : " + value_xpath);
			return false;
		}

	}
	
	
	@Sqabind(object_template="sqa_record",action_description = "scroll", action_displayname = "scroll", actionname = "scroll", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean scroll() {

		return executeScript("window.scrollBy(0,400)");

	}

	@Sqabind(object_template="sqa_record",action_description = "scrollup", action_displayname = "scrollup", actionname = "scrollup", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean scrollup() {

		return executeScript("window.scrollBy(0,-300)");

	}
	
	@Sqabind(object_template="sqa_record",action_description = "clickwhenisenablaedandisdisplayed", action_displayname = "clickwhenisenablaedandisdisplayed", actionname = "clickwhenisenablaedandisdisplayed", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean clickwhenisenablaedandisdisplayed() {
		try {
			String identifiername;
			String value;

			if (checkFrame(curObject)) {
				JSONObject idnamevalue;

				if ((idnamevalue = uniqueElement(curObject)) != null) {
					System.out.println(idnamevalue);
					identifiername = idnamevalue.getString("identifiername");
					value = idnamevalue.getString("value");
					// return click(identifiername, value);
					System.out.println(identifiername);
					System.out.println(value);

					String elementId = getElementId(identifiername, value);

					int flag = 1;
					boolean check = elementvisible(elementId);

					if (check == true) {
						flag++;
						JSONObject object = getElementrect(elementId);
						int x = object.getInt("x");
						int y = object.getInt("y");
						y = y - 100;

						executeScript("window.scrollTo(" + x + "," + y + ")");
						Thread.sleep(1000);
						click(identifiername, value);
						Thread.sleep(1000);
						return true;
					} else {
//					executeScript("window.scrollTo(0,0)");
//					
//					while(flag==1)
//					{
//						check =elementvisible(elementId);
//						if(check==true)
//						{
//							flag++;
//							click(identifiername,value);
//							return true;
//							
//						}
//						else
//						{
//							
//							executeScript("window.scrollBy(0,300)");
//							
//						}
//						
//					}
//					return false;
						return false;
					}

				}

			} else {
				logerOBJ.customlogg(userlogger, "Step Result Failed: Click cannot be performed, Element not found.",
						null, "", "error");
				return false;
			}
			return false;

		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}

	public boolean movetoelelement(String identifiername, String value) throws InterruptedException {

		String elementId = getElementId(identifiername, value);

		int flag = 1;
		boolean check = elementvisible(elementId);

		if (check == true) {
			flag++;
			JSONObject object = getElementrect(elementId);
			int x = object.getInt("x");
			int y = object.getInt("y");
			y = y - 100;

			executeScript("window.scrollTo(" + x + "," + y + ")");
			Thread.sleep(1000);
			return true;

		} else
			return false;

	}
	
	@Sqabind(object_template="sqa_record",action_description = "entertextelement", action_displayname = "entertextelement", actionname = "entertextelement", paraname_equals_curobj = "false", paramnames = {"valuetoenter"}, paramtypes = {"String"})
	public boolean entertextelement(String valuetoenter) throws InterruptedException {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		if (checkFrame(curObject)) {
			JSONObject idnamevalue = uniqueElement(curObject);
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");

			// new code

			if (movetoelelement(identifiername, value)) {
				return entertext(identifiername, value, valuetoenter);
			} else
				return false;
			// new code...

		} else {
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "clickgotoelement", action_displayname = "clickgotoelement", actionname = "clickgotoelement", paraname_equals_curobj = "false", paramnames = {"runtimeval"}, paramtypes = {"String"})
	public boolean clickgotoelement(String runtimeval) {
		try {

			if (browser.equals("firefox")) {
				Thread.sleep(100);
			}
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), runtimeval));
			if (tabs.length() > 1) {
				checkTab(curObject, tabs);
			}
			if (checkFrame(curObject)) {
				JSONObject idnamevalue;
				if ((idnamevalue = uniqueElement(curObject)) != null) {

					String identifiername = idnamevalue.getString("identifiername");
					String value = idnamevalue.getString("value");
					value = value.replaceAll("#replace", runtimeval);

					movetoelelement(identifiername, value);

					return click(identifiername, value);
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}
	
	@Sqabind(object_template="sqa_record",action_description = "clickgotoelement", action_displayname = "clickgotoelement", actionname = "clickgotoelement", paraname_equals_curobj = "false", paramnames = {"paramval"}, paramtypes = {"String"})
	public boolean dynamicexistchecktoelement(String paramvalue) throws InterruptedException {

		String value_xpath = "";

		JSONArray tabs = new JSONArray();

		tabs = getWindowhandles();

		if (tabs.length() > 1)

			checkTab(curObject, tabs);
		for (JsonNode attr : curObject.getAttributes()) {

			if (attr.get("name").asText().toLowerCase().equals("xpath")) {

				value_xpath = attr.get("value").asText();

			}

		}

		value_xpath = value_xpath.replaceAll("#replace", paramvalue);

//	        curObject.setAttributes(InitializeDependence.findattrReplace(curObject.getAttributes(), paramvalue));

//	        JSONObject idnamevalue = uniqueElement(curObject);

//	        String identifiername = idnamevalue.getString("identifiername");

//	        String value = idnamevalue.getString("value");

		JSONArray eleArray = new JSONArray();
		eleArray = findElements("xpath", value_xpath);

		if (eleArray.length() == 1) {

			logger.info("Element found");
			movetoelelement("xpath", value_xpath);
			return true;

		} else {
			logger.info("Element not found with xpath : " + value_xpath);
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "existstoelement", action_displayname = "existstoelement", actionname = "existstoelement", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean existstoelement() throws InterruptedException {
		JSONArray tabs = new JSONArray();
		tabs = getWindowhandles();
		if (tabs.length() > 1)
			checkTab(curObject, tabs);
		JSONObject idnamevalue = uniqueElement(curObject);
		if (idnamevalue == null) {
			return false;
		} else {
			String identifiername = idnamevalue.getString("identifiername");
			String value = idnamevalue.getString("value");
			movetoelelement(identifiername, value);
			return exists(identifiername, value);
		}
	}

	
	@Sqabind(object_template="sqa_record",action_description = "waituntilelementnotexist", action_displayname = "waituntilelementnotexist", actionname = "waituntilelementnotexist", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})

	public boolean waituntilelementnotexist() {
		try {
			String value = null;
			JSONArray tabs = new JSONArray();
			tabs = getWindowhandles();
			if (tabs.length() > 1)
				checkTab(this.curObject, tabs);
			for (JsonNode attr : this.curObject.getAttributes()) {
				if (attr.get("name").asText().toLowerCase().equals("xpath"))
					value = attr.get("value").asText();
			}
			return waituntilelementnotexist("xpath", value);
		} catch (Exception e) {
			this.logerOBJ.customlogg(userlogger, "Step Failed Execute to waituntilelementexist : ", null, e.toString(),
					"error");
			return false;
		}
	}

	public boolean waituntilelementnotexist(String identifiername, String value) {
		try {
			Integer maxtime = Integer.valueOf(20000);
			settimeout(5000, 300000, 3600000);
			if (InitializeDependence.existTimeout.intValue() != -1)
				maxtime = InitializeDependence.existTimeout;
			Integer i = Integer.valueOf(0);
			boolean ena = true;
			JSONArray elements = findElements(identifiername, value);
			Instant start = Instant.now();
			if (elements.isEmpty()) {
				while (!ena) {
					Instant stop = Instant.now();
					long timeelapsed = Duration.between(start, stop).toMillis();
					if (timeelapsed < maxtime.intValue()) {
						String key = null;
						JSONObject elemen = elements.getJSONObject(0);
						Iterator<String> iterator = elemen.keys();
						while (iterator.hasNext())
							key = iterator.next();
						this.clickelementId = elemen.getString(key);
						System.out.println(isElementenabled(this.clickelementId));
						System.out.println(iselementDisplayed(this.clickelementId));
						if (elements.isEmpty() && !isElementenabled(this.clickelementId)
								&& !iselementDisplayed(this.clickelementId)) {
							logger.info("wait for obeject Not Exist  with :{} ", value);
							ena = false;
						} else {
							logger.info(" Element not Exist   with :{} ", value);
							this.logerOBJ.customlogg(userlogger, "Step Failed: Element not Exist ", null, "", "error");
							ena = true;
						}
						Thread.sleep(200L);
						continue;
					}
					if (ena)
						return true;
					return false;
				}
			} else {
				return true;
			}
			settimeout(90000, 300000, 3600000);
			if (!ena)
				return true;
			return false;
		} catch (Exception e) {
			this.logerOBJ.customlogg(userlogger, "Step Failed Execute to waituntilelementexist : ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "scrollleftakpk", action_displayname = "scrollleftakpk", actionname = "scrollleftakpk", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean scrollleftakpk() {

		try {

			boolean ena = false;

			String elementId = null;

			while (!ena) {

				jsexecutor("function getElementByXpath(path) {\n"

						+ "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;\n"

						+ "}\n"

						+ "var e = getElementByXpath(\"(//h3[text()='In Active FI']/..//div[@class='ngViewport ng-scope'])[1]\");\n"

						+ "e.scrollLeft+=100000;");

				int idnamevalue = 0;

				String objecttype;

				Boolean flag;

				List<JsonNode> attrname = curObject.getAttributes();

				for (JsonNode attr : curObject.getAttributes()) {

					if (attr.get("name").asText().equalsIgnoreCase("xpath")) {

						idnamevalue = fnElements("xpath", attr.get("value").asText());

						if (idnamevalue == 0) {

							ena = false;

							// continue;

						} else if (idnamevalue == 1) {

							ena = true;

						}

					}

				}

			}

			return true;

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		}

	}

	public boolean waitforelementexist(String identifiername, String value) {
		try {
			Integer maxtime = 100;
			settimeout(5000, 300000, 3600000);
//			if (InitializeDependence.existTimeout != -1)
//				maxtime = InitializeDependence.existTimeout;
			Integer i = 0;
			boolean ena = true;
			JSONArray elements = findElements(identifiername, value);
			Instant start = Instant.now();

			if (!elements.isEmpty()) {
				while ((ena)) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
//					elements = findElements(identifiername, value);

					if (timeelapsed < maxtime) {
						String key = null;
						JSONObject elemen = elements.getJSONObject(0);
						Iterator<String> iterator = elemen.keys();
						while (iterator.hasNext()) {
							key = iterator.next();
						}
						clickelementId = elemen.getString(key);
						System.out.println(isElementenabled(clickelementId));
						System.out.println(iselementDisplayed(clickelementId));
						if (!elements.isEmpty() && isElementenabled(clickelementId)
								&& iselementDisplayed(clickelementId)) {
							logger.info("wait for obeject Not Exist  with :{} ", value);
							ena = true;
						} else {
							logger.info(" Element not Exist   with :{} ", value);
							logerOBJ.customlogg(userlogger, "Step Failed: Element not Exist ", null, "", "error");
							ena = false;
						}
						Thread.sleep(200);
					} else {
						if (ena) {
							return true;
						} else {
							return false;
						}
					}
				}
			} else
				return false;
			settimeout(90000, 300000, 3600000);
			if (!ena)
				return true;
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Failed Execute to waituntilelementexist : ", null, e.toString(),
					"error");
			return false;
		}
	}

//	public boolean dropdownvaluevalidation(String valuetoenter) throws InterruptedException {
//		boolean bStatus = false;
//		try {
//			JSONArray tabs = new JSONArray();
//			tabs = getWindowhandles();
//			if (tabs.length() > 1)
//				checkTab(curObject, tabs);
//			if (checkFrame(curObject)) {
//				JSONObject idnamevalue = uniqueElement(curObject);
//				String identifiername = idnamevalue.getString("identifiername");
//				String value = idnamevalue.getString("value");
//				clickwhenisenablaedandisdisplayed(identifiername, value);
//				Thread.sleep(1000);
//				JSONObject jb = executeScript2(
//						"function getElementByXpath(e){return document.evaluate(e,document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue};function list_a(){var e = getElementByXpath(\"//*[@id='menu-']/div[3]/ul\");return e.innerText;};return list_a();");
//				// String xpath = "//li[text()='" + valuetoenter + "']";
//				String[] droplist = jb.get("value").toString().split("\n");
//				List<String> list1 = new ArrayList<>();
//				int len = droplist.length;
//				for (int j = 0; j <= len; j++) {
//					String s = droplist[j];
//					list1.add(s);
//				}
//				String testdata[] = valuetoenter.split(",");
//				List<String> list2 = Arrays.asList(testdata);
//				int testdata_len = list2.size();
//				for (int i = 0; i < testdata_len; i++) {
//					String f = list2.get(i);
//					if (!list1.contains(f)) {
//						bStatus = false;
//						break;
//					}
//					bStatus = true;
//				}
//			}
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(), "error");
//			System.out.println(e);
//			bStatus = false;
//		}
//		return bStatus;
//	}
//
//	public boolean clickwhenisenablaedandisdisplayed(String identifiername, String value) {
//		try {
//			if (checkFrame(curObject)) {
//				JSONObject idnamevalue;
//
//				if ((idnamevalue = uniqueElement(curObject)) != null) {
//					System.out.println(idnamevalue);
//					identifiername = idnamevalue.getString("identifiername");
//					value = idnamevalue.getString("value");
//					// return click(identifiername, value);
//					System.out.println(identifiername);
//					System.out.println(value);
//
//					String elementId = getElementId(identifiername, value);
//
//					int flag = 1;
//					boolean check = elementvisible(elementId);
//
//					if (check == true) {
//						flag++;
//						JSONObject object = getElementrect(elementId);
//						int x = object.getInt("x");
//						int y = object.getInt("y");
//						y = y - 100;
//
//						executeScript("window.scrollTo(" + x + "," + y + ")");
//						Thread.sleep(1000);
//						click(identifiername, value);
//						Thread.sleep(1000);
//						return true;
//					} else {
//						// executeScript("window.scrollTo(0,0)");
//						//
//						// while(flag==1)
//						// {
//						// check =elementvisible(elementId);
//						// if(check==true)
//						// {
//						// flag++;
//						// click(identifiername,value);
//						// return true;
//						//
//						// }
//						// else
//						// {
//						//
//						// executeScript("window.scrollBy(0,300)");
//						//
//						// }
//						//
//						// }
//						// return false;
//						return false;
//					}
//
//				}
//
//			} else {
//				logerOBJ.customlogg(userlogger, "Step Result Failed: Click cannot be performed, Element not found.",
//						null, "", "error");
//				return false;
//			}
//			return false;
//
//		} catch (Exception e) {
//			return false;
//			// TODO: handle exception
//		}
//	}

	@Sqabind(object_template="sqa_record",action_description = "datepickerprocurement", action_displayname = "datepickerprocurement", actionname = "datepickerprocurement", paraname_equals_curobj = "false", paramnames = { "Date" }, paramtypes = { "String" })
	public boolean datepickerprocurement(String Date) {

		try {

			String Udate = Date.split("/")[1];

			String Umonth = Date.split("/")[0];

			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern("MMMM");

			JSONObject idnamevalue;

			idnamevalue = uniqueElement(curObject);

			String identifiername = idnamevalue.getString("identifiername");

			String value = idnamevalue.getString("value");

			Date actdate = new Date(Date);

			DateTime dtOrg = new DateTime(actdate);

			String Tmonth = dtOrg.toString(date);

			String Uyear = Date.split("/")[2];

			click();

			// calender xpath (mon yr)

			JSONObject elements = findElements("xpath", "//a[@class='k-link k-nav-fast']").getJSONObject(0);

			Iterator<String> iterator = elements.keys();

			String key = null;

			if (elements.length() == 1) {

				while (iterator.hasNext()) {

					key = iterator.next();

				}

			}

			String elementId = elements.getString(key);

			String attribute = getElementproperty(elementId, "innerText");

			// String month = null;

			String Fmonth = attribute.split(" ")[0];

			String Fyear = attribute.split(" ")[attribute.split(" ").length - 1];

			do {

				if (Fmonth.contains(Tmonth)) {

					int flag = 1;

					break;

				} else {

					// check for month

					SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);

					String dt = Tmonth + " " + Uyear;

					String dt1 = Fmonth + " " + Fyear;

					Date firstDate = sdf.parse(dt);

					Date secondDate = sdf.parse(dt1);

					while (!Fmonth.equals(Tmonth)) {

						if (firstDate.compareTo(secondDate) > 0) {

							click("xpath", "//span[@class='k-icon k-i-arrow-60-right']");

							Thread.sleep(2000);

						} else {

							click("xpath", "//span[@class='k-icon k-i-arrow-60-left']");

							Thread.sleep(2000);

							// Thread.sleep(2000);

						}

						Fmonth = getElementproperty(elementId, "innerText").split(" ")[0];

					}

				}

			} while (!Fmonth.contains(Tmonth));

			// date

			click("xpath", "//div[@aria-hidden='false']//td[not(contains(@class,'k-other-month'))]//a[text()='" + Udate

					+ "']");

			return true;

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Failed Execute to datepicker : ", null, e.toString(), "error");

			System.out.println(e);

			return false;

		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "uploadfilerobo", action_displayname = "uploadfilerobo", actionname = "uploadfilerobo", paraname_equals_curobj = "false", paramnames = { "FilePath" }, paramtypes = { "String" })
	public boolean uploadfilerobo(String path) throws InterruptedException, AWTException {

		boolean bStatus = false;
//		Thread.sleep(1000);
		try {
			if (clickusingmousepointer()) {

				StringSelection ss = new StringSelection(path);

				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

				Thread.sleep(1000);

				Robot robot = new Robot();

				String OS = System.getProperty("os.name");
				if (OS.contains("Windows")) {

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(1000);

					robot.keyRelease(java.awt.event.KeyEvent.VK_V);

					Thread.sleep(1000);

					robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(500);
					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(1500);

					System.out.println("Finishied printing");
				} else if (OS.toLowerCase().contains("mac")) {
					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_META);

					Thread.sleep(1000);

					robot.keyPress(java.awt.event.KeyEvent.VK_SHIFT);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_G);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_SHIFT);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_G);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyPress(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_META);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_V);
					Thread.sleep(2000);
					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(3000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(4000);
					robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
					Thread.sleep(1000);
					robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
				}

				bStatus = true;
				Thread.sleep(1000);
			} else {
				bStatus = false;
				Thread.sleep(1000);
			}

		} catch (Exception e) {

			System.out.println("expection is " + e);
			logerOBJ.customlogg(userlogger, "Step Failed to Execute UploadFile : ", null, e.toString(), "error");

			bStatus = false;

		}
		Thread.sleep(1000);
		return bStatus;

	}
	
	
}
