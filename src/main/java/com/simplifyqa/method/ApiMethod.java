package com.simplifyqa.method;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.DTO.SearchColumns;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.DTO.searchColumnDTO;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.execution.Impl.AutomationImpl;
import com.simplifyqa.execution.Impl.DesktopExecutionImpl;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.Interface.Apimethod;
import com.simplifyqa.mobile.ios.xml;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.MultipartBody.Builder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiMethod implements Apimethod {

	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationImpl.class);
	LoggerClass logerOBJ = new LoggerClass();

	private GeneralMethod gnMethod = null;
	private static final Logger logger = LoggerFactory.getLogger(ApiMethod.class);
	public Setupexec curExecution = null;
	public HashMap<String, String> header = new HashMap<String, String>();
	private HashMap<String, String> rpParam = null;
	public Integer currentSeq = -1;
	private Class myClass = null;
	private Editorclassloader customMethod = null;
	private String classname = "CustomMethodsdb";
	public Step curStep = null;

	public ApiMethod() {
		// TODO Auto-generated constructor stub
	}

	public ApiMethod(Setupexec curExecution, HashMap<String, String> header, Integer currentSeq) {
		this.curExecution = curExecution;
		this.header = header;
		this.currentSeq = currentSeq;
	}

	private void setGeneralMethodObj() {
		gnMethod = new GeneralMethod(this.curExecution, this.header, this.currentSeq);
	}

//	public void setRespbyapi(String setrespbyapi) {
//
//		GlobalDetail.apiSeq = currentSeq;
//		GlobalDetail.apiRes = setrespbyapi;
//	}
//
//	public void setIsapi(boolean isapi) {
//		GlobalDetail.isApi = isapi;
//	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header)
			throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			InstantiationException, NoSuchMethodException, SecurityException {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
//							ExecustomMethod(identifierName, value,classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logger.error("Unable to call Custom Method !");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable To call Custom Method!! : ", null, e.toString(),
					"error");
			return false;
		}

	}

	public boolean storexmlresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {

		boolean bStatus = false;

		boolean bFlag = false;

		HttpResponse response = null;

//		setIsapi(true);

		try {
			JSONObject Headerjson = new JSONObject(m_Header);

			m_Body = m_Body.replace("\\\"", "\"");

			logger.info("Request URL=" + m_baseURI);

			if (m_baseURI.contains("{")) {

				bFlag = true;

			}

			Iterator<String> keys = Headerjson.keys();

			HashMap<String, String> hdr = new HashMap<String, String>();

			while (keys.hasNext()) {

				String headerKey = keys.next();

				String headerValue = Headerjson.get(headerKey).toString();

				hdr.put(headerKey, headerValue);

			}

			response = HttpUtility.postapi(m_baseURI, m_Method, m_Body, hdr);

			String responseData = EntityUtils.toString(response.getEntity(), "UTF-8");

			bStatus = true;
//			JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
//			String responseData = responseJson.toString();
//			System.out.println(responseData);
			int responsecode = response.getStatusLine().getStatusCode();

//			setRespbyapi(responseData);
			curStep.setResponseData(responseData);
			curStep.setIsApi(true);

			ObjectMapper mapper = new ObjectMapper();
			setGeneralMethodObj();

			if (response != null) {
				System.out.println("Response code=" + Integer.toString(responsecode));
				System.out.println("Actual Response Body=" + responseData);
				setGeneralMethodObj();

				gnMethod.storeruntime(responseBodyRuntime, responseData);
				gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));
				gnMethod.storeruntime(responseMsgRuntime, response.getStatusLine().toString());
			} else {
			}
			boolean apiResponse = true;
			boolean appApiToReport = true;

			bStatus = true;
		} catch (Exception e) {
			logger.error("Exception Thrown", e);
			logerOBJ.customlogg(userlogger, "Step Result Failed: FindOnScreen: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

		return bStatus;

	}

	@Override
	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {

		boolean bStatus = false;

		boolean bFlag = false;

		HttpResponse response = null;

//		setIsapi(true);

		try {
			JSONObject Headerjson = new JSONObject(m_Header);

			m_Body = m_Body.replace("\\\"", "\"");

			logger.info("Request URL=" + m_baseURI);

			if (m_baseURI.contains("{")) {

				bFlag = true;

			}

			Iterator<String> keys = Headerjson.keys();

			HashMap<String, String> hdr = new HashMap<String, String>();

			while (keys.hasNext()) {

				String headerKey = keys.next();

				String headerValue = Headerjson.get(headerKey).toString();

				hdr.put(headerKey, headerValue);

			}

			response = HttpUtility.postapi(m_baseURI, m_Method, m_Body, hdr);

			bStatus = true;
			JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
			String responseData = responseJson.toString();
			System.out.println(responseData);
			int responsecode = response.getStatusLine().getStatusCode();

//			setRespbyapi(responseData);
			curStep.setResponseData(responseData);
			curStep.setIsApi(true);

// Read response into global variable

// Validate response based on the arguments passed
			ObjectMapper mapper = new ObjectMapper();
			setGeneralMethodObj();

			if (response != null) {
				System.out.println("Response code=" + Integer.toString(responsecode));
				System.out.println("Actual Response Body=" + responseData);
				setGeneralMethodObj();
				String respjson = StringEscapeUtils.escapeJava(responseData).replaceAll("\\n", "").replaceAll("\\t", "")
						.replaceAll("\\r", "").replaceAll("\\f", "");
				if (respjson.startsWith("\""))
					respjson = respjson.substring(1);
				if (respjson.endsWith("\""))
					respjson = respjson.substring(0, respjson.length() - 1);
				System.out.println(respjson);
				gnMethod.storeruntime(responseBodyRuntime, respjson);
				gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));
				gnMethod.storeruntime(responseMsgRuntime, response.getStatusLine().toString());
			} else {
			}
			boolean apiResponse = true;
			boolean appApiToReport = true;

			bStatus = true;
		} catch (Exception e) {
			logger.error("Exception Thrown", e);
			logerOBJ.customlogg(userlogger, "Step Result Failed: FindOnScreen: ", null, e.toString(), "error");
			e.printStackTrace();
			if (storexmlresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime,
					responseCodeRuntime, responseMsgRuntime)) {
				return true;
			}

			else {
				return false;
			}
		}
		return bStatus;
	}

	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,

			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,

			String responseMsgRuntime) {

		boolean bStatus = false;

		boolean bFlag = false;

		HttpResponse response = null;

		JSONObject Headerjson = new JSONObject(m_Header);

		setGeneralMethodObj();

		System.out.println(m_baseURI);

		try {

			m_Body = m_Body.replace("\\\"", "\"");

			logger.info("Request URL=" + m_baseURI);

			if (m_baseURI.contains("{")) {

				bFlag = true;

			}

			Iterator<String> keys = Headerjson.keys();

			HashMap<String, String> hdr = new HashMap<String, String>();

			while (keys.hasNext()) {

				String headerKey = keys.next();

				String headerValue = Headerjson.get(headerKey).toString();

				hdr.put(headerKey, headerValue);

			}

			// JSONObject responseJson = null;

			String responseData = null;

			// ObjectMapper mapper = new ObjectMapper();

			response = HttpUtility.postapi(m_baseURI, m_Method, m_Body, hdr, userName, password);

			if (m_Header.contains("application/xml")) {

				responseData = EntityUtils.toString(response.getEntity());

				gnMethod.storeruntime(responseBodyRuntime, responseData.toString());

			} else if (m_Header.contains("application/json")) {

				JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));

				responseData = responseJson.toString();

				gnMethod.storeruntime(responseBodyRuntime, responseData);

			}

			bStatus = true;

			System.out.println(responseData);

			int responsecode = response.getStatusLine().getStatusCode();

			// setRespbyapi(responseData);

			curStep.setResponseData(responseData);

			curStep.setIsApi(true);

			// Read response into global variable

			// Validate response based on the arguments passed

			if (response != null) {

				System.out.println("Response code=" + Integer.toString(responsecode));

				System.out.println("Actual Response Body=" + responseData);

				gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));

				gnMethod.storeruntime(responseMsgRuntime, response.getStatusLine().toString());

			} else {

			}

			boolean apiResponse = true;

			boolean appApiToReport = true;

			bStatus = true;

		} catch (Exception e) {

			logger.error("Exception Thrown", e);

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

		}

		return bStatus;

	}

	public boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		boolean bStatus = false;
		boolean bFlag = false;
		HttpResponse response = null;
		JSONObject Headerjson = new JSONObject(m_Header);

		System.out.println(m_baseURI);
		try {
			m_Body = m_Body.replace("\\\"", "\"");

			logger.info("Request URL=" + m_baseURI);

			if (m_baseURI.contains("{")) {

				bFlag = true;

			}

			Iterator<String> keys = Headerjson.keys();

			HashMap<String, String> hdr = new HashMap<String, String>();

			while (keys.hasNext()) {

				String headerKey = keys.next();

				String headerValue = Headerjson.get(headerKey).toString();

				hdr.put(headerKey, headerValue);

			}

			response = HttpUtility.postapi(m_baseURI, m_Method, m_Body, hdr, userName, password);

			bStatus = true;
			JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));
			String responseData = responseJson.toString();
			System.out.println(responseData);
			int responsecode = response.getStatusLine().getStatusCode();

//			setRespbyapi(responseData);
			curStep.setResponseData(responseData);
			curStep.setIsApi(true);
			// Read response into global variable

			// Validate response based on the arguments passed
			ObjectMapper mapper = new ObjectMapper();
			setGeneralMethodObj();
//			System.out.println(mapper.writeValueAsString(responseData).replace("\\", "").replace("\"{", "{")
//					.replace(":{\"", ":\"{\"").replace("}\"", "}").replace("]}", "]}\""));

			if (response != null) {
				System.out.println("Response code=" + Integer.toString(responsecode));
				System.out.println("Actual Response Body=" + responseData);
				gnMethod.storeruntime(responseBodyRuntime,
						mapper.writeValueAsString(responseData).replace("\\", "").replace("\"{", "{")
								.replace(":{\"", ":\"{\"").replace("}\"", "}").replace("]}", "]}\"").replace("\"{", "{")
								.replace("}\"", "}"));
				gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));
				gnMethod.storeruntime(responseMsgRuntime, response.getStatusLine().toString());
			} else {
			}
			boolean apiResponse = true;
			boolean appApiToReport = true;

			bStatus = true;
		} catch (Exception e) {
			logger.error("Exception Thrown", e);
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
		}
		return bStatus;
	}

	

	public boolean restapiforformdata(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String formdatatype, String reqbody, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		boolean bStatus = false;
		boolean bFlag = false;
		Response response = null;

		String headerKey = null;
		String headerValue = null;

		System.out.println(m_baseURI);
		try {
			reqbody = reqbody.replace("\\\"", "\"");
			logger.info("Request URL=" + m_baseURI);
			if (m_baseURI.contains("{")) {
				bFlag = true;
			}
			if (!m_Header.equals("")) {
				JSONObject Headerjson = new JSONObject(m_Header);
				Iterator<String> keys = Headerjson.keys();
				HashMap<String, String> hdr = new HashMap<String, String>();

				while (keys.hasNext()) {
					headerKey = keys.next();
					headerValue = Headerjson.get(headerKey).toString();
					hdr.put(headerKey, headerValue);
				}
			} else {
				headerKey = "";
				headerValue = "";
			}
			okhttp3.HttpUrl.Builder url = HttpUrl.parse(m_baseURI).newBuilder();
			if (!m_Parameters.equals("")) {
				JSONObject Paramsjson = new JSONObject(m_Parameters);

				Iterator<String> paramskeys = Paramsjson.keys();
				HashMap<String, String> params = new HashMap<String, String>();
				String paramskey = null;
				String paramsvalue = null;
				while (paramskeys.hasNext()) {
					paramskey = paramskeys.next();
					paramsvalue = Paramsjson.get(paramskey).toString();
					url = url.addQueryParameter(paramskey, paramsvalue);

//				params.put(paramskey, paramsvalue);
				}
			} else {

			}
			HttpUrl finalurl = url.build();

			OkHttpClient client = new OkHttpClient().newBuilder().build();
			MediaType mediaType = MediaType.parse("text/plain");
			RequestBody body = null;

			if (!reqbody.equals("")) {
				JSONObject filejson = new JSONObject(reqbody);
				Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
				if (formdatatype.equalsIgnoreCase("file")) {
					Iterator<String> filekeys = filejson.keys();
					// HashMap<String, String> file = new HashMap<String, String>();
					while (filekeys.hasNext()) {
						String fileKey = filekeys.next();
						String fileValue = filejson.get(fileKey).toString();
						File file = new File(fileValue);
						body = builder
								.addFormDataPart(fileKey, "file", RequestBody
										.create(MediaType.parse("application/octet-stream"), new File(fileValue)))
								.build();
					}
				}
				if (formdatatype.contains("text")) {
					JSONObject textjson = new JSONObject(reqbody);
					Iterator<String> textkeys = textjson.keys();

					while (textkeys.hasNext()) {
						String textKey = textkeys.next();
						String textValue = textjson.get(textKey).toString();
						body = builder.addFormDataPart(textKey, textValue).build();
					}
				}
			} else {

			}
			Request request = new Request.Builder().url(finalurl).method(m_Method, body)
					.addHeader(headerKey, headerValue).build();
			response = client.newCall(request).execute();
			int responsecode = response.code();
			String responseData = response.body().string();
			System.out.println(responseData);
			curStep.setResponseData(responseData);
			curStep.setIsApi(true);
//			 Read response into global variable
//			 Validate response based on the arguments passed
			ObjectMapper mapper = new ObjectMapper();
			setGeneralMethodObj();
			if (response != null) {
				System.out.println("Response code=" + Integer.toString(responsecode));
				System.out.println("Actual Response Body=" + responseData);
				gnMethod.storeruntime(responseBodyRuntime, responseData);
				gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));
				gnMethod.storeruntime(responseMsgRuntime, response.message().toString());
			} else {
			}
			boolean apiResponse = true;
			boolean appApiToReport = true;

			bStatus = true;
		} catch (Exception e) {
			logger.error("Exception Thrown", e);
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
		}
		return bStatus;
	}

	public boolean getexecutiondata(String customerId, String projectId, String executionId, String serverIp,
			String username, String password, String collectionname) {
		try {
			if (curExecution == null) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			JSONObject response;
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			SearchColumns searchcolumns = new SearchColumns();
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.CUSTOMERID.value(), Integer.parseInt(customerId),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.PROJECTID.value(), Integer.parseInt(projectId),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns()
					.add(new searchColumnDTO<Integer>(UtilEnum.ID.value(), Integer.parseInt(executionId),
							Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.INTEGER.value()));
			searchcolumns.getSearchColumns().add(new searchColumnDTO<Boolean>(UtilEnum.DELETED.value(), Boolean.FALSE,
					Boolean.parseBoolean(UtilEnum.FALSE.value()), UtilEnum.BOOLEAN.value()));
			searchcolumns.setStartIndex(0);
			searchcolumns.setLimit(100000);
			searchcolumns.setCollection(UtilEnum.EXECUTION.value());
			response = new JSONObject(HttpUtility.SendPost(serverIp + UtilEnum.SEARCH.value(),
					new ObjectMapper().writeValueAsString(searchcolumns), header));
//			setRespbyapi(response.toString());
			curStep.setResponseData(response.toString());
			curStep.setIsApi(true);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	private void createSoapEnvelope(SOAPMessage soapMessage, String myNamespace, String myNamespaceURI)
			throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("CelsiusToFahrenheit", myNamespace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Celsius", myNamespace);
		soapBodyElem1.addTextNode("100");
	}

	@Override
	public boolean callsoapwebservice(String soapEndpointUrl, String soapAction, String myNamespace,
			String myNamespaceURI, String runtimename) {
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, myNamespace, myNamespaceURI),
					soapEndpointUrl);

			// Print the SOAP Response
			System.out.println("Response SOAP Message:");
			System.out.println(soapMessageToString(soapResponse));
//			setRespbyapi(soapMessageToString(soapResponse));
			curStep.setResponseData(soapMessageToString(soapResponse));
			curStep.setIsApi(true);
			setGeneralMethodObj();
			gnMethod.storeruntime(runtimename, soapMessageToString(soapResponse));
			soapConnection.close();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;

		}
	}

	@Override
	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
						logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, ioe.toString(), "error");
					}
				}
			}
		}
		return result;
	}

	private SOAPMessage createSOAPRequest(String soapAction, String myNamespace, String myNamespaceURI)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage, myNamespace, myNamespaceURI);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);

		soapMessage.saveChanges();

		/* Print the request message, just for debugging purposes */
		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;
	}

	public boolean rawapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,

			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,

			String responseMsgRuntime) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
			URISyntaxException, IOException {
		try {
			boolean bStatus = false;

			boolean bFlag = false;

			HttpResponse response = null;

			JSONObject Headerjson = new JSONObject(m_Header);

			setGeneralMethodObj();

			System.out.println(m_baseURI);

			try {

				m_Body = m_Body.replace("\\\"", "\"");

				logger.info("Request URL=" + m_baseURI);

				if (m_baseURI.contains("{")) {

					bFlag = true;

				}

				Iterator<String> keys = Headerjson.keys();

				HashMap<String, String> hdr = new HashMap<String, String>();

				while (keys.hasNext()) {

					String headerKey = keys.next();

					String headerValue = Headerjson.get(headerKey).toString();

					hdr.put(headerKey, headerValue);

				}

				// JSONObject responseJson = null;

				String responseData = null;

				// ObjectMapper mapper = new ObjectMapper();

				response = HttpUtility.postapi(m_baseURI, m_Method, m_Body, hdr, userName, password);

				if (m_Header.contains("application/xml")) {

					responseData = EntityUtils.toString(response.getEntity());

					gnMethod.storeruntime(responseBodyRuntime, responseData.toString());

				} else if (m_Header.contains("application/json")) {

					JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));

					responseData = responseJson.toString();

					gnMethod.storeruntime(responseBodyRuntime, responseData);

				} else if (m_Header.contains("text/plain")) {

					// JSONObject responseJson = new
					// JSONObject(EntityUtils.toString(response.getEntity(), "UTF-8"));

					responseData = EntityUtils.toString(response.getEntity());
					System.out.println(responseData);
					// String result = EntityUtils.toString(response.getEntity());
					// System.out.println(result);

					gnMethod.storeruntime(responseBodyRuntime, responseData);

				}

				bStatus = true;

				System.out.println(responseData);

				int responsecode = response.getStatusLine().getStatusCode();

				// setRespbyapi(responseData);

				curStep.setResponseData(responseData);

				curStep.setIsApi(true);

				// Read response into global variable

				// Validate response based on the arguments passed

				if (response != null) {

					System.out.println("Response code=" + Integer.toString(responsecode));

					System.out.println("Actual Response Body=" + responseData);

					gnMethod.storeruntime(responseCodeRuntime, String.valueOf(responsecode));

					gnMethod.storeruntime(responseMsgRuntime, response.getStatusLine().toString());

				} else {

				}

				boolean apiResponse = true;

				boolean appApiToReport = true;

				bStatus = true;

			} catch (Exception e) {

				logger.error("Exception Thrown", e);

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

			}

			return bStatus;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

}
