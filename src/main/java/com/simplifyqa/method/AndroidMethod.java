package com.simplifyqa.method;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.SystemUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.mobile.android.DeviceManager;
import com.simplifyqa.mobile.android.PackageDetail;
import com.simplifyqa.mobile.android.Setautomator2;
import com.simplifyqa.mobile.ios.Idevice;
import com.simplifyqa.web.implementation.executor;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;

@SuppressWarnings("deprecation")
public class AndroidMethod {
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(AutomationExecuiton.class);

	LoggerClass logerOBJ = new LoggerClass();
	public RemoteWebDriver driver;
	private String classname = "CustomMethodsandroid";
	private static final Logger logger = LoggerFactory.getLogger(AndroidMethod.class);
	private Editorclassloader customMethod = null;
	public Setupexec curExecution = null;
	public static Integer currentSeq = -1;
	public HashMap<String, String> header = new HashMap<String, String>();
	public Object curobject;
	private String exepectedValue = null;
	private String Actualvalue = null;

	public String getActualvalue() {
		return Actualvalue;
	}

	public void setActualvalue(String actualvalue) {
		Actualvalue = actualvalue;
	}

	public String getAdbpath() {
		if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX)
			return new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb").getAbsolutePath();
		else
			return new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe").getAbsolutePath();
	}

	private JsonNode attribute = null, attribute2 = null;
	private String[] ididame = { "xpath", "xpathbytext", "xpathbycontent-desc", "xpathbyclass", "xpathvyname",
			"xpathbylabel", "xpathbyvalue", "resource-id" };

	public String getExepectedValue() {
		return exepectedValue;
	}

	public void setExepectedValue(String exepectedValue) {
		this.exepectedValue = exepectedValue;
	}

	@SuppressWarnings("rawtypes")
	private Class myClass = null;

//	public RemoteWebDriver getDriver() {
//		return GlobalDetail.sqaDriver.get(Thread.currentThread().getName());
//	}

	public RemoteWebDriver getDriver() {
		device = getDevice();
		driver = GlobalDetail.sqaDriver.get(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getName());
		for (Entry<String, AndroidMethod> map : InitializeDependence.androidDriver.entrySet()) {
			if (driver == map.getValue().driver) {
				curExecution = map.getValue().curExecution;
				curobject = map.getValue().curobject;
			}
		}
//	curobject=InitializeDependence.androidDriver.get(Thread.currentThread().getName()).curobject;
		return GlobalDetail.sqaDriver.get(Thread.currentThread().getName());

	}

	public IDevice getDevice() {
		return DeviceManager.devices.get(Thread.currentThread().getName());
	}

	public void setDriver(RemoteWebDriver driver) {
		this.driver = driver;
	}

	public IDevice device;
	public String hostname = null;
	private Integer count = 0;

	public AndroidMethod() {
	}

	public AndroidMethod(Setupexec curExecution, Integer currentSeq, HashMap<String, String> header) {
		this.curExecution = curExecution;
		this.currentSeq = currentSeq;
		this.header = header;
	}

	@SuppressWarnings({ "rawtypes" })
	@Sqabind(object_template="Sqa Mobile",action_description = "This action start the application.", action_displayname = "startapp", actionname = "startapp",paraname_equals_curobj = "false", paramnames = { "Package","Activity" }, paramtypes = { "String","String" })
	public Boolean startapp(String Package, String Activity) {
		try {
			if (count == 10) {
				count = 0;
				return false;
			}
			count++;
			ServerSocket serverSocket;
			String port = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
			serverSocket.close();
			DesiredCapabilities capabilitiesApp = new DesiredCapabilities();
			capabilitiesApp.setCapability(CapabilityType.BROWSER_NAME, "");
			capabilitiesApp.setCapability("skipDeviceInitialization", true);
			capabilitiesApp.setCapability("skipServerInstallation", true);
			capabilitiesApp.setCapability("skipServerLaunch", true);
			capabilitiesApp.setCapability("deviceName", getDevice().getName());
			capabilitiesApp.setCapability("udid", getDevice().getSerialNumber());
			capabilitiesApp.setCapability("systemPort", port);
			capabilitiesApp.setCapability("platformName", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability("platform", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability(MobileCapabilityType.PLATFORM_VERSION,
					getDevice().getProperty("ro.build.version.release"));
			capabilitiesApp.setCapability("newCommandTimeout", 2000000);
			capabilitiesApp.setCapability("autoWebView", true);
			capabilitiesApp.setCapability("ensureWebviewsHavePages", true);
			capabilitiesApp.setCapability("enableWebviewDetailsCollection", true);
			capabilitiesApp.setCapability("appPackage", Package);
			capabilitiesApp.setCapability("appActivity", Activity);
			capabilitiesApp.setCapability("noReset", true);
			capabilitiesApp.setCapability("automationName", "uiautomator2");
			capabilitiesApp.setCapability("noSign", true);
			capabilitiesApp.setCapability("settings[allowInvisibleElements]", true);
			capabilitiesApp.setCapability("settings[shouldUseCompactResponses]", false);

			capabilitiesApp.setCapability("nativeWebScreenshot", true);
			logger.info(capabilitiesApp.toString());
			logger.info("Application {} {} started", Package, Activity);
			driver = (RemoteWebDriver) new AndroidDriver(
					new URL(GlobalDetail.hostname.get(getDevice().getSerialNumber())), capabilitiesApp);
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			GlobalDetail.sqaDriver.put(getDevice().getSerialNumber(), driver);
//			GlobalDetail.hostname = hostname;
			return true;
		} catch (Exception e) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//			e.printStackTrace();
			if (startapp(Package, Activity))
				return true;
			else
				return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "This action reset the application, before launching", action_displayname = "resetapp", actionname = "resetapp", paraname_equals_curobj = "false", paramnames = { "Package","Activity" }, paramtypes = { "String","String" })
	public Boolean resetapp(String Package, String Activity) {
		try {
			if (count == 10) {
				count = 0;
				return false;
			}
			count++;
			ServerSocket serverSocket;
			String port = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
			serverSocket.close();
			DesiredCapabilities capabilitiesApp = new DesiredCapabilities();
			capabilitiesApp.setCapability(CapabilityType.BROWSER_NAME, "");
			capabilitiesApp.setCapability("skipDeviceInitialization", true);
			capabilitiesApp.setCapability("skipServerInstallation", true);
			capabilitiesApp.setCapability("skipServerLaunch", true);
			capabilitiesApp.setCapability("deviceName", getDevice().getName());
			capabilitiesApp.setCapability("udid", getDevice().getSerialNumber());
			capabilitiesApp.setCapability("systemPort", port);
			capabilitiesApp.setCapability("platformName", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability("platform", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability(MobileCapabilityType.PLATFORM_VERSION,
					getDevice().getProperty("ro.build.version.release"));
			capabilitiesApp.setCapability("newCommandTimeout", 2000000);
			capabilitiesApp.setCapability("autoWebView", true);
			capabilitiesApp.setCapability("ensureWebviewsHavePages", true);
			capabilitiesApp.setCapability("enableWebviewDetailsCollection", true);
			capabilitiesApp.setCapability("appPackage", Package);
			capabilitiesApp.setCapability("appActivity", Activity);
			capabilitiesApp.setCapability("noReset", false);
			capabilitiesApp.setCapability("automationName", "uiautomator2");
			capabilitiesApp.setCapability("noSign", true);
			capabilitiesApp.setCapability("settings[allowInvisibleElements]", true);
			capabilitiesApp.setCapability("settings[shouldUseCompactResponses]", false);
			logger.info(capabilitiesApp.toString());
			logger.info("Application {} {} reseted", Package, Activity);
			driver = (RemoteWebDriver) new AndroidDriver(
					new URL(GlobalDetail.hostname.get(getDevice().getSerialNumber())), capabilitiesApp);
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			GlobalDetail.sqaDriver.put(getDevice().getSerialNumber(), driver);
//			GlobalDetail.hostname = hostname;
			return true;
		} catch (Exception e) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
//				e1.printStackTrace();
			}
//			e.printStackTrace();
			if (resetapp(Package, Activity))
				return true;
			else {
				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
				return false;
			}
		}
	}

	public AndroidElement getElement(String objectName) {
		try {
			JSONObject resp = null;
			ObjectMapper mapper = new ObjectMapper();
			Object object = new Object();
			JsonNode Attribute = null;
			@SuppressWarnings("unused")
			String searchColumns = "{searchColumns:[" + " \"column\": \"customerId\", \"value\":"
					+ GlobalDetail.setting.get(Thread.currentThread().getName()).getCustomerID()
					+ ", \"regEx\": false, \"type\": \"integer\" },"
					+ " { \"column\": \"deleted\", \"value\": false, \"regEx\": false, \"type\": \"boolean\" },"
					+ "{ \"column\": \"projectId\", \"value\": "
					+ GlobalDetail.setting.get(Thread.currentThread().getName()).getProjectID()
					+ ", \"regEx\": false, \"type\": \"integer\" }," + "{ \"column\": \"disname\", \"value\": "
					+ objectName + ", \"ignorecase\": true, \"type\": \"string\" }" + "]"
					+ " \"startIndex\": 0, \"limit\": 100000,\r\n" + "        \"collection\": \"object\"" + "}";
			@SuppressWarnings({ "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					GlobalDetail.setting.get(Thread.currentThread().getName()).getServerIp() + "/search");
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("authorization",
					GlobalDetail.setting.get(Thread.currentThread().getName()).getAuthKey());
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				throw new RuntimeException("Failed with HTTP error code : " + statusCode);
			} else {
				resp = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
				logger.info(resp.toString());
			}
			if (resp.isEmpty())
				return null;
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			object = mapper.readValue(resp.getJSONObject("data").toString(), Object.class);
			Attribute = InitializeDependence.findattr(object.getAttributes(), "resource-id");
			if (Attribute != null)
				return getElement(Attribute.get("name").asText(), Attribute.get("value").asText());
			else {
				Attribute = InitializeDependence.findattr(object.getAttributes(), "xpath");
				if (Attribute != null)
					return getElement(Attribute.get("name").asText(), Attribute.get("value").asText());
				else
					return null;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unused")
	public AndroidElement getElementbyobjRep(String objectName, String identifierName) {
		try {
			JSONObject resp = null;
			ObjectMapper mapper = new ObjectMapper();
			Object object = new Object();
			JsonNode Attribute = null;
			String searchColumns = "{searchColumns:[" + " \"column\": \"customerId\", \"value\":"
					+ GlobalDetail.setting.get(Thread.currentThread().getName()).getCustomerID()
					+ ", \"regEx\": false, \"type\": \"integer\" },"
					+ " { \"column\": \"deleted\", \"value\": false, \"regEx\": false, \"type\": \"boolean\" },"
					+ "{ \"column\": \"projectId\", \"value\": "
					+ GlobalDetail.setting.get(Thread.currentThread().getName()).getProjectID()
					+ ", \"regEx\": false, \"type\": \"integer\" }," + "{ \"column\": \"disname\", \"value\": "
					+ objectName + ", \"ignorecase\": true, \"type\": \"string\" }" + "]"
					+ " \"startIndex\": 0, \"limit\": 100000,\r\n" + "        \"collection\": \"object\"" + "}";
			@SuppressWarnings({ "resource" })
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(
					GlobalDetail.setting.get(Thread.currentThread().getName()).getServerIp() + "/search");
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("authorization",
					GlobalDetail.setting.get(Thread.currentThread().getName()).getAuthKey());
			HttpResponse response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				throw new RuntimeException("Failed with HTTP error code : " + statusCode);
			} else {
				resp = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
				logger.info(resp.toString());
			}
			if (resp.isEmpty())
				return null;
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			object = mapper.readValue(resp.getJSONObject("data").toString(), Object.class);
			Attribute = InitializeDependence.findattr(object.getAttributes(), identifierName);
			if (Attribute != null)
				return getElement(Attribute.get("name").asText(), Attribute.get("value").asText());
			else
				return null;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}

	}

	public AndroidElement getElement(String identifierName, String value) {
		try {
			Thread.currentThread().setName(device.getSerialNumber());
			AndroidElement element = null;
			switch (identifierName.contains("xpath") ? "xpath" : identifierName) {
			case "xpath":
				element = (AndroidElement) getDriver().findElement(By.xpath(value));
				break;

			case "resource-id":
				element = (AndroidElement) getDriver().findElement(By.id(value));
				break;
			}
			return (element.isDisplayed() && element.isEnabled()) ? element : null;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	public List<WebElement> getElements(String identifierName, String value) {
		try {
			Thread.currentThread().setName(device.getSerialNumber());
			List<WebElement> elements = null;
			switch (identifierName.contains("xpath") ? "xpath" : identifierName) {
			case "xpath":
				elements = getDriver().findElements(By.xpath(value));
				break;
			case "resource-id":
				elements = getDriver().findElements(By.id(value));
				break;
			}
			return (!elements.isEmpty()) ? elements : new ArrayList<WebElement>();
		} catch (Exception e) {
			return new ArrayList<WebElement>();
		}

	}

	private String createImageFromByte(byte[] binaryData) {
		try {
			Thread.sleep(1000);
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(Base64.getEncoder().encodeToString(binaryData));
//			sb.append(getDriver().getScreenshotAs(OutputType.FILE));
			return sb.toString();
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(getDriver().getScreenshotAs(OutputType.BASE64));

			return sb.toString();

		}
	}

	private String createImageFromByteforocr(byte[] binaryData) {
		try {
			Thread.sleep(1000);
			StringBuilder sb = new StringBuilder();
//			sb.append("data:image/png;base64,");
//			sb.append(Base64.getEncoder().encodeToString(binaryData));
			sb.append(getDriver().getScreenshotAs(OutputType.FILE));
			return sb.toString();
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append("data:image/png;base64,");
			sb.append(getDriver().getScreenshotAs(OutputType.BASE64));

			return sb.toString();

		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "This action reset the application, before launching", action_displayname = "resetapp", actionname = "resetapp", paraname_equals_curobj = "false", paramnames = { "Package","Activity" }, paramtypes = { "String","String" })
	public Boolean clickbycoordinate(String x, String y) {
		return (TapByAdb(Integer.parseInt(x), Integer.parseInt(y)));
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Verify the object Exists", action_displayname = "exists", actionname = "exists", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean exists() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return exists(attribute.get("name").asText(), attribute.get("value").asText());
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Verify the object Exists using dynamic xpath.", action_displayname = "existsusindynamicxpath", actionname = "existsusindynamicxpath", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public boolean existsusindynamicxpath(String dynamicvalue) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return exists(attribute.get("name").asText(),
						attribute.get("value").asText().replaceAll("#replace", dynamicvalue));
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Verify the object not Exists", action_displayname = "notexists", actionname = "notexists", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public boolean notexists(String dynamicvalue) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return notexists(attribute.get("name").asText(),
						attribute.get("value").asText().replaceAll("#replace", dynamicvalue));
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Verify the object not Exists", action_displayname = "notexists", actionname = "notexists", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean notexists() {

		if (uniqueElement(curobject) == null) {
			return true;
		}

		else {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return exists(attribute.get("name").asText(), attribute.get("value").asText());
			else
				return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Enter the text.", action_displayname = "entertext", actionname = "entertext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public Boolean entertext(String value) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "needed");
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);

				Boolean status = req.getBoolean("status");

				if (status) {
					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					// int l = Integer.parseInt(req.getJSONObject("center").get("y").toString());
					Tap(x + 70, y - 500);
					return sendkeysByOther(value);

				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null)
					return Sendkeys(attribute.get("name").asText(), attribute.get("value").asText(), value);
				else {
					return sendkeysByOther(value);
				}
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean entertext(String value, String value2) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "needed");
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);

				Boolean status = req.getBoolean("status");

				if (status) {
					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					// int l = Integer.parseInt(req.getJSONObject("center").get("y").toString());
					Tap(x + 70, y - 500);
					return sendkeysByOther(value);

				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null)
					return Sendkeys(attribute.get("name").asText(), attribute.get("value").asText(),
							value.concat(value2));
				else {
					return sendkeysByOther(value.concat(value2));
				}
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Enter the text using dynamic xpath", action_displayname = "entertextusingdynamicxpath", actionname = "entertextusingdynamicxpath", paraname_equals_curobj = "true", paramnames = { "", "" }, paramtypes = { "string", "string" })
	public Boolean entertextusingdynamicxpath(String value, String dynamicvalue) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "needed");
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);

				Boolean status = req.getBoolean("status");

				if (status) {
					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					// int l = Integer.parseInt(req.getJSONObject("center").get("y").toString());
					Tap(x + 70, y - 500);
					return sendkeysByOther(value);

				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null)
					return Sendkeys(attribute.get("name").asText(),
							attribute.get("value").asText().replaceAll("#replace", dynamicvalue), value);
				else {
					return sendkeysByOther(value);
				}
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "dupilicate element should click", action_displayname = "duplicate_element_click", actionname = "duplicate_element_click", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean duplicate_element_click() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", "");

				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
				int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());
				return Tap(x + 20, y + 20);

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null) {
					if (Tap(attribute.get("name").asText(), attribute.get("value").asText()))
						return true;
					else {
						attribute = InitializeDependence.findattr(curobject.getAttributes(), "x");
						attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
						int mWidth = -1, mHieght = -1, x = -1, y = -1;
						mWidth = width();
						mHieght = height();
						x = (int) ((Math.round(attribute.get("value").asDouble()) * mWidth) / 100);
						y = (int) ((Math.round(attribute2.get("value").asDouble()) * mHieght) / 100);
						if (attribute != null && attribute2 != null) {
							return TapByAdb(x, y);
						} else
							return false;
					}
				} else {
					attribute = InitializeDependence.findattr(curobject.getAttributes(), "x");
					attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
					int mWidth = -1, mHieght = -1, x = -1, y = -1;
					mWidth = width();
					mHieght = height();
					x = (int) ((Math.round(attribute.get("value").asDouble()) * mWidth) / 100);
					y = (int) ((Math.round(attribute2.get("value").asDouble()) * mHieght) / 100);
					if (attribute != null && attribute2 != null) {
						return TapByAdb(x, y);
					} else
						return false;
				}
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public static boolean sendformdata(String url, String image_path, String image2, int width, int height,
			String text) {
		try {
			CloseableHttpClient httpClient = HttpUtility.getHttpClient(url);
			System.out.println(Paths.get("com.simplifyQA.Agent.jar"));
			BasicFileAttributes attr = Files.readAttributes(Paths.get(image_path), BasicFileAttributes.class);

			FileTime creationTime = attr.lastModifiedTime();

			File jar = new File(Paths.get(image_path).toString());

			File file = new File(Paths.get(image_path).toString());
			HttpPost post = new HttpPost(url);
			FileBody fileBody = new FileBody(file, ContentType.create("multipart/form-data"));
			StringBody stringBody1 = new StringBody(image2, ContentType.MULTIPART_FORM_DATA);
			StringBody stringBody2 = new StringBody(String.valueOf(width), ContentType.MULTIPART_FORM_DATA);
			StringBody stringBody3 = new StringBody(String.valueOf(height), ContentType.MULTIPART_FORM_DATA);
			StringBody stringBody4 = new StringBody(text, ContentType.MULTIPART_FORM_DATA);

			//
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("image1", fileBody);
			builder.addPart("image2", stringBody1);
			builder.addPart("width", stringBody2);
			builder.addPart("height", stringBody3);
			builder.addPart("text", stringBody4);

			HttpEntity entity = builder.build();
			//
			post.setEntity(entity);
			org.apache.http.HttpResponse response = httpClient.execute(post);

			String result = EntityUtils.toString(response.getEntity());
			JSONObject res = new JSONObject(result);

			File file_image = new File(image_path);

			if (res.has("text1")) {
				String original_ocr = res.get("text1").toString();
				logger.info(original_ocr);

				if (original_ocr.contains(text)) {
					logger.info(text + " found in OCR.");
					try {
						file_image.delete();
						return true;
					} catch (Exception e) {
						return true;
						// TODO: handle exception
					}

				}

				else {
					String dublicate_text = text.replace(" ", "");
					if (original_ocr.contains(dublicate_text)) {
						try {
							file_image.delete();
							return true;
						} catch (Exception e) {
							return true;
							// TODO: handle exception
						}
					} else {
						logger.info("object not found during OCR.");
						try {
							file_image.delete();
							return false;
						} catch (Exception e) {
							return false;
							// TODO: handle exception
						}
					}
				}
			}

			else {
				if (res.has("text2")) {
					String crop_ocr = res.get("text2").toString();

					if (crop_ocr.contains(text)) {
						logger.info(text + " found in OCR.");
						try {
							file_image.delete();
							return true;
						} catch (Exception e) {
							return true;
							// TODO: handle exception
						}

					} else {
						try {
							file_image.delete();
							return false;
						} catch (Exception e) {
							return false;
							// TODO: handle exception
						}
					}

				}

				else {
					try {
						file_image.delete();
						return false;
					} catch (Exception e) {
						return false;
						// TODO: handle exception
					}
				}
			}

		}

		catch (Exception e) {
			System.out.println(e);
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Validate text with the parameter value", action_displayname = "validatetext", actionname = "validatetext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public Boolean validatetext(String value) {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");
			if (attribute != null) {
				System.out.println(createImageFromByteforocr(GlobalDetail.screenshot.get(device.getSerialNumber())));
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));

				req.put("image2", attribute.get("value").asText());

				req.put("width", width());

				req.put("height", height());
				req.put("text", value);
				req.put("rt_top", "");
				req.put("device", "xr");

				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				System.out.println(req);

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					if (sendformdata("http://172.104.183.14:32770/validate_text",
							createImageFromByteforocr(GlobalDetail.screenshot.get(device.getSerialNumber())),
							attribute.get("value").asText(), width_recorded.get("value").asInt(),
							height_recorded.get("value").asInt(), value)) {
						return true;
					}

					else {
						return false;
					}

				}

				int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
				int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

				Boolean status = req.getBoolean("status");
				Boolean text_present = req.getBoolean("text present");

				if (status) {
					if (text_present) {
						return true;
					} else {
						return false;
					}

				}

				else {
					return false;
				}
			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null)
					return validatetext(attribute.get("name").asText(), attribute.get("value").asText(), value);
				else
					return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Validate partial text with the parameter value", action_displayname = "validatepartialtext", actionname = "validatepartialtext", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public Boolean validatepartialtext(String value) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return validatepartialtext(attribute.get("name").asText(), attribute.get("value").asText(), value);
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Click an element", action_displayname = "click", actionname = "click", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean click() {
		try {

			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");

			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));
				req.put("image2", attribute.get("value").asText());
				req.put("width", width());
				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				}
				Boolean status = req.getBoolean("status");
				// Boolean text_present = req.getBoolean("text present");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					int x1 = Integer.parseInt(req.getJSONObject("center").get("x").toString());
					int y1 = Integer.parseInt(req.getJSONObject("center").get("y").toString());

					if (width_recorded == null || height_recorded == null) {
						return Tap(x + 20, y + 20);

					}

					else {
						return TapByAdb(x1, y1);
					}

				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null) {

					getElement(attribute.get("name").asText(), attribute.get("value").asText());
					if (Tap(attribute.get("name").asText(), attribute.get("value").asText())) {
						logger.info("Executed Click on :{} ", attribute.get("value").asText());
						return true;
					} else {
						logger.info("Failed to Execute Click on :{} ,No Element Found",
								attribute.get("value").asText());
						throw new Exception("Unable to click");
					}
				} else {
					logger.info("No Unique attribute in Object");
					throw new Exception("No Unique found");
				}

			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean clickandenter(String value) {
		try {

			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");

			if (attribute != null) {
				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));
				req.put("image2", attribute.get("value").asText());
				req.put("width", width());
				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
				}
				Boolean status = req.getBoolean("status");
				// Boolean text_present = req.getBoolean("text present");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					int x1 = Integer.parseInt(req.getJSONObject("center").get("x").toString());
					int y1 = Integer.parseInt(req.getJSONObject("center").get("y").toString());

					if (width_recorded == null || height_recorded == null) {
						return Tap(x + 20, y + 20);

					}

					else {
						TapByAdb(x1, y1);
						Thread.sleep(2000);
						return SendkeysByAdb(value);
					}

				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null) {

					getElement(attribute.get("name").asText(), attribute.get("value").asText());
					if (Tap(attribute.get("name").asText(), attribute.get("value").asText())) {
						logger.info("Executed Click on :{} ", attribute.get("value").asText());
						return true;
					} else {
						logger.info("Failed to Execute Click on :{} ,No Element Found",
								attribute.get("value").asText());
						throw new Exception("Unable to click");
					}
				} else {
					logger.info("No Unique attribute in Object");
					throw new Exception("No Unique found");
				}

			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "find the element and click by adb", action_displayname = "findelementandclickbyadb", actionname = "findelementandclickbyadb", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean findelementandclickbyadb() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null) {
				return findelementandclickbyadb(attribute.get("name").asText(), attribute.get("value").asText());
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean findelementandclickbyadb(String identifierName, String value) {
		try {
			List<WebElement> elements;
			if ((elements = (getElements(identifierName, value.replace("\\", "\"")))).size() == 1) {

				if (elements.get(0).isEnabled()) {
					TapByAdb(((AndroidElement) elements.get(0)).getCenter().getX(),
							((AndroidElement) elements.get(0)).getCenter().getY());
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (StaleElementReferenceException e) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			return Tap(identifierName, value);
		} catch (org.openqa.selenium.TimeoutException e) {
			logger.error(" Exception in Tap Element not found " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Tap : Element not found: ", null,
					e.toString(), "error");
			return false;
		} catch (Exception e) {
			logger.error(" Exception in Tap " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Tap :  ", null, e.toString(), "error");
			return false;
		}

	}

	public Boolean Tap(Object object, int timeout) {
		try {
			JSONObject idnamevalue = null;
			if ((idnamevalue = uniqueElement(curobject)) != null) {
				return Tap(idnamevalue.getString("name"), idnamevalue.getString("value"));
			} else {
				return false;
			}

		} catch (

		Exception e) {
			return false;
		}
	}

	public Boolean click(Object object, int timeout) {
		try {
			JSONObject idnamevalue = null;
			if ((idnamevalue = uniqueElement(curobject)) != null) {
				return Click(idnamevalue.getString("name"), idnamevalue.getString("value"));
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Read value and store in runtime parameter", action_displayname = "readvalueandstore", actionname = "readvalueandstore", paraname_equals_curobj = "false", paramnames = { "runtimePramaname" }, paramtypes = { "string" })
	public Boolean readvalueandstore(String runtimePramaname) {
		try {
			JSONObject idnamevalue = uniqueElement(curobject);
			JSONObject re = new JSONObject();
			AndroidElement Element = getElement(idnamevalue.getString("name"), idnamevalue.getString("value"));
			if (curExecution == null) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
			if (jsonObject.has("createdBy")) {

				re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

			} else {

				re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

			}
			re.put("paramvalue", Element.getAttribute("text"));
			InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
			logger.info("Value: " + re.get("paramvalue") + "stored into :" + runtimePramaname);
			return true;

		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Read text and store in runtime parameter", action_displayname = "readtextandstore", actionname = "readtextandstore", paraname_equals_curobj = "false", paramnames = { "runtimePramaname" }, paramtypes = { "string" })
	public Boolean readtextandstore(String runtimePramaname) {
		try {
			JSONObject idnamevalue = uniqueElement(curobject);
			JSONObject re = new JSONObject();
			AndroidElement Element = getElement(idnamevalue.getString("name"), idnamevalue.getString("value"));
			if (curExecution == null) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
			if (jsonObject.has("createdBy")) {

				re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

			} else {

				re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

			}
			re.put("paramvalue", Element.getAttribute("text"));
			InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
			logger.info("Text: " + re.get("paramvalue") + "stored into :" + runtimePramaname);
			return true;

		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Click if element exists", action_displayname = "clickifexists", actionname = "clickifexists", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean clickifexists() {
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			maxCount = 5000;
			JSONObject idnamevalue = null;
			if ((idnamevalue = uniqueElement(curobject)) != null) {
				if (exists(idnamevalue.getString("name"), idnamevalue.getString("value"))) {
					maxCount = 90000;
					driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
					return Tap(idnamevalue.getString("name"), idnamevalue.getString("value"));
				} else
					return true;
			} else {
				maxCount = 90000;
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
				return true;
			}
		} catch (Exception e) {
			maxCount = 90000;
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean Tap(AndroidElement Element) {
		try {
			new TouchAction((PerformsTouchActions) getDriver())
					.tap((TapOptions) TapOptions.tapOptions().withElement(ElementOption.element((Element)))).perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean Clear(AndroidElement Element) {
		try {
			Element.clear();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean Click(AndroidElement Element) {
		try {
			Element.click();
			return true;
		} catch (Exception e) {
			return Tap(Element);
		}
	}

	public Boolean Clear(String identifierName, String value) {
		try {
			List<WebElement> elements;
			if ((elements = (getElements(identifierName, value.replace("\\", "\"")))).size() == 1) {
				elements.get(0).clear();
				return true;
			} else {
				return false;
			}
		} catch (org.openqa.selenium.TimeoutException e) {
			logger.error(" Exception in Clear Element not found " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Clear Element not found: ", null,
					e.toString(), "error");
			return false;
		} catch (Exception e) {
			logger.error(" Exception in Clear " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Clear: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean Tap(String identifierName, String value) {
		try {
			List<WebElement> elements;
			if ((elements = (getElements(identifierName, value.replace("\\", "\"")))).size() == 1) {

				if (elements.get(0).isEnabled()) {
					new TouchAction((PerformsTouchActions) getDriver()).tap(
							(TapOptions) TapOptions.tapOptions().withElement(ElementOption.element((elements.get(0)))))
							.perform();
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (StaleElementReferenceException e) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			return Tap(identifierName, value);
		} catch (org.openqa.selenium.TimeoutException e) {
			logger.error(" Exception in Tap Element not found " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Tap Element not found: ", null,
					e.toString(), "error");
			return false;
		} catch (Exception e) {
			logger.error(" Exception in Tap " + identifierName + " " + value, e);
			logerOBJ.customlogg(userlogger, "Step Result Failed : Exception in Tap: ", null, e.toString(), "error");

			return false;
		}
	}

	
	public boolean clickusingdynamicxpath(String xpath, String runtimevlau) {
		try {
			return Tap("xpath", xpath.replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Click element using dynamic xpath", action_displayname = "clickusingdynamicxpath", actionname = "clickusingdynamicxpath", paraname_equals_curobj = "true", paramnames = { "" }, paramtypes = { "string" })
	public boolean clickusingdynamicxpath(String runtimevlau) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			return Tap("xpath", attribute.get("value").asText().replaceAll("#replace", runtimevlau));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public Boolean Click(String identifierName, String value) {
		try {
			List<WebElement> elements;
			if ((elements = (getElements(identifierName, value.replace("\\", "\"")))).size() == 1) {
				elements.get(0).click();
				elements.get(0).click();
				return true;
			} else {
				return false;
			}
		} catch (org.openqa.selenium.TimeoutException e) {
			return Tap(identifierName, value);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Click : ", null, e.toString(), "error");
			return Tap(identifierName, value);
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean Tap(int x, int y) {
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point(x, y))
					.waitAction(WaitOptions.waitOptions(Duration.ofMillis(250))).release().perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Tap: ", null, e.toString(), "error");
			return TapByAdb(x, y);
		}

	}

	public Boolean TapByAdb(int x, int y) {
		try {
			CollectingOutputReceiver tap1 = new CollectingOutputReceiver();
			String tap = "input tap " + x + " " + y;
			getDevice().executeShellCommand(tap, tap1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: TapByAdb: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean swipe(int startX, int startY, int endX, int endY, int timeout) {
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
					.waitAction(waitOptions(ofMillis(timeout))).moveTo(point(endX, endY)).release().perform();
			return true;
		} catch (Exception e) {
			return swipeByAdb(startX, startY, endX, endY, timeout);
		}

	}

	@SuppressWarnings("rawtypes")
	public Boolean SwipeByPercent(double x1Percent, double y1Percent, double x2Percent, double y2Percent) {
		try {
			int Mobile_width = width();
			int Mobile_height = height();
			int startX = (int) ((x1Percent * Mobile_width) / 100);
			int startY = (int) ((y1Percent * Mobile_height) / 100);
			int endX = (int) ((x2Percent * Mobile_width) / 100);
			int endY = (int) ((y2Percent * Mobile_height) / 100);

			new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
					.waitAction(waitOptions(ofMillis(500))).moveTo(point(endX, endY)).release().perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: SwipeByPercent: ", null, e.toString(), "error");
			return false;
		}

	}

	@SuppressWarnings("rawtypes")
	public void swipeByElements(AndroidElement startElement, AndroidElement endElement) {
		int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
		int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

		int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
		int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);

		new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
				.waitAction(waitOptions(ofMillis(1000))).moveTo(point(endX, endY)).release().perform();
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollDown()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getHeight() * 0.5);
		int scrollEnd = (int) (dimensions.getHeight() * 0.2);
		try {
			new TouchAction((PerformsTouchActions) getDriver())
					.press(point((int) (dimensions.getWidth() * 0.5), Startpoint))
					.waitAction(waitOptions(ofMillis(2000)))
					.moveTo(point((int) (dimensions.getWidth() * 0.5), scrollEnd)).release().perform();
			return true;
		} catch (Exception e) {
			return swipe((int) (dimensions.getWidth() * 0.5), Startpoint, (int) (dimensions.getWidth() * 0.5),
					scrollEnd, 2000);
		}

	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "Scroll down to x-coordinates.", action_displayname = "xCoornidates", actionname = "scrolldown", paraname_equals_curobj = "false", paramnames = { "xCoornidates" }, paramtypes = { "int" })
	public Boolean scrolldown(int xCoornidates)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getHeight() * 0.5);
		int scrollEnd = (int) (dimensions.getHeight() * 0.2);
		xCoornidates = (int) ((xCoornidates * dimensions.getWidth()) / 100);
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point((int) (xCoornidates), Startpoint))
					.waitAction(waitOptions(ofMillis(2000))).moveTo(point((int) (xCoornidates), scrollEnd)).release()
					.perform();
			return true;
		} catch (Exception e) {
			return swipe(xCoornidates, Startpoint, xCoornidates, scrollEnd, 2000);
		}

	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "Scroll up to x-coordinates.", action_displayname = "xCoornidates", actionname = "scrollup", paraname_equals_curobj = "false", paramnames = { "xCoornidates" }, paramtypes = { "int" })
	public Boolean scrollup(int xCoordinates)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getHeight() * 0.2);
		int scrollEnd = (int) (dimensions.getHeight() * 0.5);
		xCoordinates = (int) ((xCoordinates * dimensions.getWidth()) / 100);
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point((int) (xCoordinates), Startpoint))
					.waitAction(waitOptions(ofMillis(2000))).moveTo(point((int) (xCoordinates), scrollEnd)).release()
					.perform();
			return true;
		} catch (Exception e) {
			return swipe(xCoordinates, Startpoint, xCoordinates, scrollEnd, 2000);
		}

	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollUp()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getHeight() * 0.2);
		int scrollEnd = (int) (dimensions.getHeight() * 0.5);
		try {
			new TouchAction((PerformsTouchActions) getDriver())
					.press(point((int) (dimensions.getWidth() * 0.5), Startpoint))
					.waitAction(waitOptions(ofMillis(2000)))
					.moveTo(point((int) (dimensions.getWidth() * 0.5), scrollEnd)).release().perform();
			return true;
		} catch (Exception e) {
			return swipe((int) (dimensions.getWidth() * 0.5), Startpoint, (int) (dimensions.getWidth() * 0.5),
					scrollEnd, 2000);
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "scroll till element vertical up", action_displayname = "scrolltillelementverticalup", actionname = "scrolltillelementverticalup", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementverticalup() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), false,
						(int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(),
						false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "scroll till element vertical down", action_displayname = "scrolltillelementverticaldown", actionname = "scrolltillelementverticaldown", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementverticaldown() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), true,
						(int) Math.round(attribute2.get("value").asDouble()));

			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(), attribute.get("value").asText(), true);
			} else
				return false;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Scroll till element horizontal right", action_displayname = "scrolltillelementhorizontalright", actionname = "scrolltillelementhorizontalright", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementhorizontalright() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						false, (int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Scroll till element horizontal left", action_displayname = "scrolltillelementhorizontalleft", actionname = "scrolltillelementhorizontalleft", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrolltillelementhorizontalleft() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						true, (int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(), attribute.get("value").asText(),
						true);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrolltillelementverticalup(String param) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), false,
						(int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrolltillelementverticaldown(String param) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), true,
						(int) Math.round(attribute2.get("value").asDouble()));

			} else if (attribute2 != null) {
				return scrollTillElementVertical(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), true);
			} else
				return false;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrolltillelementhorizontalright(String param) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), false,
						(int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), false);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrolltillelementhorizontalleft(String param) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "xpath");
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "y");
			if (attribute != null && attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), true,
						(int) Math.round(attribute2.get("value").asDouble()));
			} else if (attribute2 != null) {
				return scrollTillElementHorizontal(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#Replace", param), true);
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrollTillElementVertical(Object object, boolean direction) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), "xpath");
			if (Attribute != null) {
				return scrollTillElementVertical(Attribute.get("name").asText(), Attribute.get("value").asText(),
						direction);
			} else {
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public boolean scrollTillElementVertical(String identifierName, String value, boolean direction) {
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			List<WebElement> Element = getElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.size() == 0) {
				count++;
				if (direction)
					scrollDown();
				else
					scrollUp();
				Element = getElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.size() > 0 && Element.size() == 1) {
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
				return true;
			} else
				return false;
		} catch (Exception e) {
			return scrollTillElementVertical(identifierName, value, direction);
		}

	}

	public boolean scrollTillElementVertical(String identifierName, String value, boolean direction, int xCoordinates) {
		try {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
//			GlobalDetail.maXTimeout = 1;
			List<WebElement> Element = getElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.size() == 0) {
				count++;
				if (direction)
					scrolldown(xCoordinates);
				else
					scrollup(xCoordinates);
				Element = getElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.size() > 0 && Element.size() == 1) {
//				GlobalDetail.maXTimeout = 90;
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
				return true;
			} else
				return false;
		} catch (Exception e) {
			return scrollTillElementVertical(identifierName, value, direction, xCoordinates);
		}

	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "scroll to Left", action_displayname = "scrollLeft", actionname = "scrollLeft", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrollLeft()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getWidth() * 0.5);
		int scrollEnd = (int) (dimensions.getWidth() * 0.2);
		try {
			new TouchAction((PerformsTouchActions) getDriver())
					.press(point(Startpoint, (int) (dimensions.getHeight() * 0.5)))
					.waitAction(waitOptions(ofMillis(2000)))
					.moveTo(point(scrollEnd, (int) (dimensions.getHeight() * 0.5))).release().perform();
			return true;
		} catch (Exception e) {
			return swipe(Startpoint, (int) (dimensions.getHeight() * 0.5), scrollEnd,
					(int) (dimensions.getHeight() * 0.5), 2000);
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollLeft(int Ycoordinate)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getWidth() * 0.5);
		int scrollEnd = (int) (dimensions.getWidth() * 0.2);
		Ycoordinate = (int) ((Ycoordinate * dimensions.getHeight()) / 100);
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point(Startpoint, Ycoordinate))
					.waitAction(waitOptions(ofMillis(2000))).moveTo(point(scrollEnd, Ycoordinate)).release().perform();
			return true;
		} catch (Exception e) {
			return swipe(Startpoint, Ycoordinate, scrollEnd, Ycoordinate, 2000);
		}
	}

	@SuppressWarnings("rawtypes")
	@Sqabind(object_template="Sqa Mobile",action_description = "scroll to Right", action_displayname = "scrollRight", actionname = "scrollRight", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean scrollRight()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getWidth() * 0.2);
		int scrollEnd = (int) (dimensions.getWidth() * 0.5);
		try {
			new TouchAction((PerformsTouchActions) getDriver())
					.press(point(Startpoint, (int) (dimensions.getHeight() * 0.5)))
					.waitAction(waitOptions(ofMillis(2000)))
					.moveTo(point(scrollEnd, (int) (dimensions.getHeight() * 0.5))).release().perform();
			return true;
		} catch (Exception e) {
			return swipe(Startpoint, (int) (dimensions.getHeight() * 0.5), scrollEnd,
					(int) (dimensions.getHeight() * 0.5), 2000);
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean scrollRight(int Ycoordinate)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		Dimension dimensions = getsize();
		int Startpoint = (int) (dimensions.getWidth() * 0.2);
		int scrollEnd = (int) (dimensions.getWidth() * 0.5);
		Ycoordinate = (int) ((Ycoordinate * dimensions.getHeight()) / 100);
		try {
			new TouchAction((PerformsTouchActions) getDriver()).press(point(Startpoint, Ycoordinate))
					.waitAction(waitOptions(ofMillis(2000))).moveTo(point(scrollEnd, Ycoordinate)).release().perform();
			return true;
		} catch (Exception e) {
			return swipe(Startpoint, Ycoordinate, scrollEnd, Ycoordinate, 2000);
		}
	}

	public Boolean scrollTillElementHorizontal(Object object, boolean direction) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), "xpath");
			if (Attribute != null) {
				return scrollTillElementHorizontal(Attribute.get("name").asText(), Attribute.get("value").asText(),
						direction);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public String getElementText(String identifierName, String value) {
		try {
			List<WebElement> elements;
			if ((elements = (getElements(identifierName, value.replace("\\", "\"")))).size() == 1) {
				System.out.println(elements.get(0).getText());
				return elements.get(0).getText();
			} else {
				return null;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	public boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction) {
		try {
//			GlobalDetail.maXTimeout = 1;
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			List<WebElement> Element = getElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.size() == 0) {
				count++;
				if (direction)
					scrollLeft();
				else
					scrollRight();
				Element = getElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.size() > 0 && Element.size() == 1) {
//				GlobalDetail.maXTimeout = 90;
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return scrollTillElementHorizontal(identifierName, value, direction);
		}

	}

	public boolean scrollTillElementHorizontal(String identifierName, String value, boolean direction,
			int Ycoordinate) {
		try {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
//			GlobalDetail.maXTimeout = 1;
			List<WebElement> Element = getElements(identifierName, value.replace("\\", "\""));
			int count = 0;
			while (Element.size() == 0) {
				count++;
				if (direction)
					scrollLeft(Ycoordinate);
				else
					scrollRight(Ycoordinate);
				Element = getElements(identifierName, value.replace("\\", "\""));
				if (count == 500) {
					break;
				}
			}
			if (Element.size() > 0 && Element.size() == 1) {
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
//				GlobalDetail.maXTimeout = 90;
				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return scrollTillElementHorizontal(identifierName, value, direction, Ycoordinate);
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Swipe to the event", action_displayname = "swipeevent", actionname = "swipeevent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean swipeevent() {
		try {
			int Mobile_width = -1, Mobile_height = -1, startX = -1, startY = -1, endX = -1, endY = -1, to = 1000;
			Mobile_width = width();

			Mobile_height = height();

			for (int i = 0; i < curobject.getAttributes().size(); i++) {
				if (curobject.getAttributes().get(i).get("name").asText().equals("x1"))
					startX = (int) ((Double.parseDouble(curobject.getAttributes().get(i).get("value").asText())
							* Mobile_width) / 100);
				if (curobject.getAttributes().get(i).get("name").asText().equals("y1"))
					startY = (int) (Math.round(
							Double.parseDouble(curobject.getAttributes().get(i).get("value").asText()) * Mobile_height)
							/ 100);
				if (curobject.getAttributes().get(i).get("name").asText().equals("x2"))
					endX = (int) (Math.round(
							Double.parseDouble(curobject.getAttributes().get(i).get("value").asText()) * Mobile_width)
							/ 100);
				if (curobject.getAttributes().get(i).get("name").asText().equals("y2"))
					endY = (int) (Math.round(
							Double.parseDouble(curobject.getAttributes().get(i).get("value").asText()) * Mobile_height)
							/ 100);
				if (curobject.getAttributes().get(i).get("name").asText().equals("timeout"))
					to = (int) Math.round(Double.parseDouble(curobject.getAttributes().get(i).get("value").asText()));

			}
			if (Mobile_height != -1 && Mobile_width != -1 && startX != -1 && startY != -1 && endX != -1 && endY != -1)
				return swipe(startX, startY, endX, endY, to);
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrollright() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "y");
			if (attribute != null) {
				return scrollRight((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollRight();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrollleft() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null) {
				return scrollLeft((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollLeft();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrolldown() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null) {
				return scrolldown((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollDown();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean scrollup() {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(), "x");
			if (attribute != null) {
				return scrollup((int) Math.round(attribute.get("value").asDouble()));
			} else
				return scrollUp();

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean swipeByAdb(int startX, int startY, int endX, int endY, int timeout) {
		try {
			CollectingOutputReceiver tap1 = new CollectingOutputReceiver();
			String swipeCordinate = "input swipe " + startX + " " + startY + " " + endX + " " + endY + " " + timeout;
			getDevice().executeShellCommand(swipeCordinate, tap1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean holdelement(int startX, int startY, int endX, int endY) {
		try {

			new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
					.waitAction(waitOptions(Duration.ofMillis(1000))).moveTo(point(endX, endY)).release().perform();

			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@SuppressWarnings("rawtypes")
	public Boolean HoldByPercent(double x1Percent, double y1Percent, double x2Percent, double y2Percent) {
		try {
			int Mobile_width = width();
			int Mobile_height = height();
			int startX = (int) ((x1Percent * Mobile_width) / 100);
			int startY = (int) ((y1Percent * Mobile_height) / 100);
			int endX = (int) ((x2Percent * Mobile_width) / 100);
			int endY = (int) ((y2Percent * Mobile_height) / 100);

			new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
					.waitAction(waitOptions(ofMillis(1000))).moveTo(point(endX, endY)).release().perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@SuppressWarnings("rawtypes")
	public Boolean HoldByElements(AndroidElement startElement, AndroidElement endElement) {

		try {
			int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
			int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

			int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
			int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);

			new TouchAction((PerformsTouchActions) getDriver()).press(point(startX, startY))
					.waitAction(waitOptions(ofMillis(1000))).moveTo(point(endX, endY)).release().perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

		}
		return false;
	}

	public Boolean HoldByAdb(int startX, int startY, int endX, int endY) {
		try {
			CollectingOutputReceiver tap1 = new CollectingOutputReceiver();
			String holdCordinate = "input draganddrop " + startX + " " + startY + " " + endX + " " + endY;
			getDevice().executeShellCommand(holdCordinate, tap1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean LongPress(AndroidElement Element) {
		try {
			new TouchAction((PerformsTouchActions) getDriver()).tap(tapOptions().withElement(element(Element)))
					.waitAction(waitOptions(Duration.ofMillis(2000))).perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return false;
		}

	}

	public Boolean LongPress(Object object) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (Attribute != null) {
				return LongPress(Attribute.get("name").asText(), Attribute.get("value").asText());
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean LongPress(String identifierName, String value) {
		try {
			AndroidElement Element = getElement(identifierName, value);
			new TouchAction((PerformsTouchActions) getDriver()).tap(tapOptions().withElement(element(Element)))
					.waitAction(waitOptions(Duration.ofMillis(2000))).perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "LongPress", actionname = "LongPress", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean LongPress(int startX, int startY) {
		try {
			CollectingOutputReceiver tap1 = new CollectingOutputReceiver();
			String holdCordinate = "input draganddrop " + startX + " " + startY + " " + startX + " " + startY;
			getDevice().executeShellCommand(holdCordinate, tap1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public String getXml(RemoteWebDriver sqaDriver) {
		try {
			String xmlString = sqaDriver.getPageSource();
			return xmlString;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return null;
		}
	}

	public String getXml() {
		try {
			File adbpath = null;
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			Process p1;
			p1 = Runtime.getRuntime()
					.exec(new String[] { adbpath.getAbsolutePath(), "exec-out", "uiautomator", "dump", "/dev/tty" });
//			p1.waitFor();
			InputStream stdIn = p1.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			BufferedReader br = new BufferedReader(isr);
			StringBuilder xmlString = new StringBuilder();
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				xmlString.append(line);

			return xmlString.toString().replace("UI hierchary dumped to: /dev/tty", "");

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return null;
		}
	}

	public Dimension getsize()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		try {

			return new Dimension(GlobalDetail.width.get(getDevice().getSerialNumber()),
					GlobalDetail.height.get(getDevice().getSerialNumber()));
		} catch (Exception e) {
			String size = null;
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			getDevice().executeShellCommand("wm size", info);
			size = info.getOutput().split(":")[1].replace("\r\n", "").split("\n")[0];
			GlobalDetail.width.put(getDevice().getSerialNumber(), Integer.parseInt(size.split("x")[0].trim()));
			GlobalDetail.height.put(getDevice().getSerialNumber(), Integer.parseInt(size.split("x")[1].trim()));

			return new Dimension(GlobalDetail.width.get(getDevice().getSerialNumber()),
					GlobalDetail.height.get(getDevice().getSerialNumber()));
		}
	}

	public int width() {
		try {
			Dimension size = getsize();
			return size.width;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return -1;
		}

	}

	public int height() {
		try {
			Dimension size = getsize();
			return size.height;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return -1;
		}

	}

	@SuppressWarnings("unchecked")
	public AndroidDriver<WebElement> getSaqAndroidDriver() {
		return (AndroidDriver<WebElement>) getDriver();
	}

	public Boolean runapp(String Package, String Activity) {
		try {
			CollectingOutputReceiver started = new CollectingOutputReceiver();
			String start = "am start -n " + Package + "/" + Activity;
			getDevice().executeShellCommand(start, started);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean installapp(String s) {
		try {
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { getAdbpath(), "-s", getDevice().getSerialNumber(), "install", s });
			JSONObject list = null;
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.contains("No such file or directory"))
					throw new Exception("No Such Directory");
				else if (line.contains("failed"))
					throw new Exception("Failed to install");
			}
			killTask("com.example.nettyhttpserver");
//			getDevice().installPackage(s, false);
			logger.info("apk uploaded");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error in uploading apk");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Error in uploading apk: ", null, e.toString(),
					"error");
			return false;
		}
	}

	public boolean killTask(String bundleid) {
		try {
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { getAdbpath(), "shell", "am", "force-stop", bundleid });
			JSONObject list = null;
			builder.redirectErrorStream(true);
			Process process = builder.start();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Boolean UnInstallApp(String Package) {
		try {
			getDevice().uninstallPackage(Package);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public JSONArray getPackages(String deviceId) {
		try {
			setDevice(deviceId);
			return new PackageDetail(getDevice()).init();
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}
	}

	public void setDevice(String deviceID) {

		device = DeviceManager.devices.get(deviceID);
		Thread.currentThread().setName(deviceID);
	}

	public boolean SetUiautomor2(String deviceId, Boolean playback) throws InstallException, IOException,
			TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException {
		setDevice(deviceId);
		hostname = null;
		if (!InitializeDependence.Appiumrun.containsKey(deviceId)) {
			if ((hostname = new Setautomator2().init(getDevice(), playback)) != null) {
				InitializeDependence.Appiumrun.put(deviceId, true);
				GlobalDetail.hostname.put(deviceId, hostname);
				return true;
			} else
				return false;
		} else {
			hostname = GlobalDetail.hostname.get(deviceId);
			return true;
		}
	}

	public Boolean ClearkeysByAdb(String value) {
		char[] c = value.toCharArray();
		try {
			CollectingOutputReceiver clear1 = new CollectingOutputReceiver();
			for (int i = 0; i <= c.length; i++) {
				String clear = "input keyevent 67";
				getDevice().executeShellCommand(clear, clear1);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "", action_displayname = "ClearkeysByAdb", actionname = "ClearkeysByAdb", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean ClearkeysByAdb() {
		try {
			CollectingOutputReceiver clear1 = new CollectingOutputReceiver();
			getDevice().executeShellCommand("input keyevent KEYCODE_MOVE_END", clear1);
			getDevice().executeShellCommand(" input keyevent --longpress $(printf 'KEYCODE_DEL %.0s' {1..250})",
					clear1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	public boolean cleartext(String identifiername, String value) {

		try {
			ClearText(identifiername, value);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public Boolean ClearText(String identifierName, String value) {
		try {
			WebElement webElement = getElement(identifierName, value.replace("\\", "\""));
			String valuetoclear = webElement.getText();
			return ClearkeysByAdb(valuetoclear);
//			if (webElement.isDisplayed()) {
//				webElement.clear();
//				return true;
//			} else
//				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ClearText : ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	public Boolean sendkeysByOther(Object object, String str) {
		try {
			JsonNode Attribute = null;

			Attribute = InitializeDependence.findattr(object.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (Attribute != null) {
				return sendkeysByOther(str);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: sendKeysByOther ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean SendkeysByAdb(String value) {
		try {
			CollectingOutputReceiver send1 = new CollectingOutputReceiver();
			String send = "input text  '" + value + "'";
			getDevice().executeShellCommand(send, send1);
			// device.executeShellCommand("input keyevent 66", send1);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "enter", action_displayname = "enter", actionname = "enter", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean enter() {
		try {
			CollectingOutputReceiver send1 = new CollectingOutputReceiver();
			getDevice().executeShellCommand("input keyevent 66", send1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	public Boolean sendkeysWithAdb(String identifierName, String value, String str) {
		try {
			AndroidElement Element = getElement(identifierName, value.replace("\\", "\""));
			Element.click();
			Clear(Element);
			CollectingOutputReceiver send1 = new CollectingOutputReceiver();
			String send = "input text '" + str + "'";
			getDevice().executeShellCommand(send, send1);
			logger.info(send1 + "EnterText Done");
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;

		}
	}

	public Boolean sendkeysByOther(String str) {
		try {
			new Actions(getDriver()).sendKeys(str).build().perform();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean Sendkeys(Object object, String str) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), uniqueElement(object).getString("name"));
			if (Attribute != null) {
				return Sendkeys(Attribute.get("name").asText(), Attribute.get("value").asText(), str);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public Boolean Sendkeys(String identifierName, String value, String str) {
		try {
			AndroidElement webElement = getElement(identifierName, value.replace("\\", "\""));
//			new TouchAction((PerformsTouchActions) getDriver())
//					.tap((TapOptions) TapOptions.tapOptions().withElement(ElementOption.element((webElement))))
//					.perform();
			if (webElement.isDisplayed()) {
				webElement.click();
				webElement.clear();
				webElement.sendKeys(str);
				return true;
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return sendkeysByOther(str);
		}

	}

	public Boolean emulatorevent(String value) {
		try {
			CollectingOutputReceiver direct1 = new CollectingOutputReceiver();
			String text = "input keyevent  '" + value + "'";
			getDevice().executeShellCommand(text, direct1);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean EmulatorEvent() {
		try {
			((AppiumDriver) getDriver()).navigate().back();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validatetext(Object object, String str) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), uniqueElement(object).getString("name"));
			if (Attribute != null) {
				return validatetext(Attribute.get("name").asText(), Attribute.get("value").asText(), str);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validatepartialtext(Object object, String str) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), uniqueElement(object).getString("name"));
			if (Attribute != null) {
				return validatepartialtext(Attribute.get("name").asText(), Attribute.get("value").asText(), str);
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validatetext(String identifierName, String Value, String Text) {
		try {
			AndroidElement Element = getElement(identifierName, Value);
			setExepectedValue(Element.getAttribute("text"));
			setActualvalue(Text);
			if (Element.getAttribute("text").equals(Text))
				return true;
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validatepartialtext(String identifierName, String Value, String Text) {
		try {
			AndroidElement Element = getElement(identifierName, Value);
			setExepectedValue(Element.getAttribute("text"));
			setActualvalue(Text);
			if (Element.getAttribute("text").contains(Text))
				return true;
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "validate attribute", action_displayname = "validateattr", actionname = "validateattr", paraname_equals_curobj = "false", paramnames = { "str1", "str2" }, paramtypes = { "string", "string" })
	public Boolean validateattr(String str1, String str2) {
		try {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null)
				return validateattr(attribute.get("name").asText(), attribute.get("value").asText(), str1, str2);
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validateattr(Object object, String str1, String str2) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), uniqueElement(object).getString("name"));
			if (Attribute != null) {
				return validateattr(Attribute.get("name").asText(), Attribute.get("value").asText(), str1, str2);
			} else {

				logerOBJ.customlogg(userlogger, "Step Result Failed: validateattr : error ", null, "", "error");
				return false;
			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean validateattr(String identifierName, String Value, String text, String text1) {
		try {
			AndroidElement Element = getElement(identifierName, Value);
			if (Element.getAttribute(text1).equals(text)) {
				logger.info("Attributes {} exists with value {}", text, text1);
				return true;
			}

			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean exists(Object object) {
		try {
			JsonNode Attribute = null;
			Attribute = InitializeDependence.findattr(object.getAttributes(), uniqueElement(object).getString("name"));
			if (Attribute != null) {
				return exists(Attribute.get("name").asText(), Attribute.get("value").asText());
			} else {
				return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean exists(String identifierName, String value) {
		try {
			@SuppressWarnings("unused")
			AndroidElement Element = getElement(identifierName, value.replace("\\", "\""));
			if (Element == null)
				return false;
			else
				return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public Boolean notexists(String identifierName, String value) {
		try {
			@SuppressWarnings("unused")
			AndroidElement Element = getElement(identifierName, value.replace("\\", "\""));

			return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return true;
		}
	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header)
			throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			InstantiationException, NoSuchMethodException, SecurityException {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
//							ExecustomMethod(identifierName, value,classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logger.error("Unable to call Custom Method !");
			logerOBJ.customlogg(userlogger, "Step Result Failed : Unable to call Custom Method ! : ", null,
					e.toString(), "error");
			return false;
		}

	}

	public boolean wifiStatus() {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			this.getDevice().executeShellCommand("settings get global wifi_on", info);
			if (Integer.parseInt(info.getOutput().trim()) == 1)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Uanable to get Wifi status !");
			logerOBJ.customlogg(userlogger, "Step Result Failed : Unable to see wifiStatus! : ", null, e.toString(),
					"error");
			return false;
		}
	}

	public boolean dataStatus() {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			this.getDevice().executeShellCommand("settings get global mobile_data", info);
			if (Integer.parseInt(info.getOutput().trim()) == 1)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Uanable to get Mobile data status ! ");

			return false;
		}
	}

	public boolean bluetoothStatus() {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			this.getDevice().executeShellCommand("settings get global bluetooth_on", info);
			if (Integer.parseInt(info.getOutput().trim()) == 1)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Uanable to get Bluetooth status ! ");

			return false;
		}
	}

	public boolean airplaneModeStatus() {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			this.getDevice().executeShellCommand("settings get global airplane_mode_on", info);
			if (Integer.parseInt(info.getOutput().trim()) == 1)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error("Uanable to get airplane Mode status ! ");
			return false;
		}
	}

	public Boolean mbwifitoggle(String value) {
		try {
			return wifiToggle(Boolean.parseBoolean(value));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public boolean wifiToggle(boolean flag) {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			if (flag) {
				this.getDevice().executeShellCommand("am broadcast -a io.appium.settings.wifi --es setstatus enable",
						info);
				return true;
			} else {
				this.getDevice().executeShellCommand("am broadcast -a io.appium.settings.wifi --es setstatus disable",
						info);
				return true;
			}
		} catch (Exception e) {
			logger.error("Unable to toggle wifi");

			return false;
		}
	}

	public Boolean mbdatatoggle(String value) {
		try {
			return dataToggle(Boolean.parseBoolean(value));
		} catch (Exception e) {

			return false;
		}
	}

	public boolean dataToggle(boolean flag) {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			if (flag) {
				this.getDevice().executeShellCommand(" svc data enable", info);
				return true;
			} else {
				this.getDevice().executeShellCommand(" svc data disable", info);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Unable to toggle data");

			return false;
		}
	}

	public Boolean mbbluetoothtoggle(String value) {
		try {
			return bluetoothToggle(Boolean.parseBoolean(value));
		} catch (Exception e) {

			return false;
		}
	}

	public boolean bluetoothToggle(boolean flag) {
		try {
			CollectingOutputReceiver info = new CollectingOutputReceiver();
			if (flag) {
				this.getDevice().executeShellCommand(
						"am broadcast -a io.appium.settings.bluetooth --es setstatus enable", info);
				return true;
			} else {
				this.getDevice().executeShellCommand(
						" am broadcast -a io.appium.settings.bluetooth --es setstatus disable", info);
				return true;
			}
		} catch (Exception e) {
			logger.error("Unable to toggle bluetooth");

			return false;
		}
	}

	public String getOrientation() {
		try {
			ScreenOrientation screen = ((AppiumDriver) getDriver()).getOrientation();
			return screen.value();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Boolean mbscreenrotation(String orientation) {
		try {
			if (orientation.equals(ScreenOrientation.PORTRAIT.value())) {
				((AppiumDriver) getDriver()).rotate(ScreenOrientation.PORTRAIT);
				return true;
			} else {
				((AppiumDriver) getDriver()).rotate(ScreenOrientation.LANDSCAPE);
				return true;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	private Integer maxCount = 90000;

	public JSONObject uniqueElement(Object obj) {
		try {
			ArrayList<Future<JSONObject>> taskList = new ArrayList<>();
			Boolean flag = false;
			for (JsonNode attr : obj.getAttributes()) {
				if (attr.has("unique") && attr.get("unique").asBoolean()) {
					flag = true;
					logerOBJ.customlogg(userlogger, "Object Property :", null, attr.get("name").toString(), "info");
					logerOBJ.customlogg(userlogger, "Object Property Value :", null, attr.get("value").toString(),
							"info");
					if (attr.get("name").asText().toLowerCase().contains("xpath"))
						return fnElements(attr.get("name").asText(), attr.get("value").asText());
					else if (attr.get("name").asText().toLowerCase().contains("resource-id")) {
						return fnElements(attr.get("name").asText(), attr.get("value").asText());
					}

				}
			}
			JSONObject s = null;
			if (!flag) {
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Instant start = Instant.now();
				executor.g().restart();
				parallelsearch(obj, taskList);
				s = killtask(taskList);
				while (s == null) {
					Instant stop = Instant.now();
					long timeelapsed = java.time.Duration.between(start, stop).toMillis();
					if (timeelapsed < maxCount) {
						taskList.clear();
						executor.g().parallelobject.shutdownNow();
						executor.g().restart();
						parallelsearch(obj, taskList);
						s = killtask(taskList);
					} else {
						logerOBJ.customlogg(userlogger, "Step Result Failed : Due to Element not Found.", null, "",
								"error");
						return null;
					}
				}
			}
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			logerOBJ.customlogg(userlogger, "Object Property :", null, s.get("name").toString(), "info");
			logerOBJ.customlogg(userlogger, "Object Property Value :", null, s.get("value").toString(), "info");
			return s;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed : Due to Element not Found.", null, "", "error");
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject parallelsearch(Object obj, ArrayList<Future<JSONObject>> taskList) throws InterruptedException {
		taskList.removeAll(taskList);
		for (String idname : ididame) {
			JsonNode attr = InitializeDependence.findattr(obj.getAttributes(), idname);
			if (attr != null) {
				if (attr.get("name").asText().toLowerCase().equals("xpath")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbytext")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbycontent-desc")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbyclass")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("xpathbyname")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("xpath", attr.get("value").asText());
					}));
				} else if (attr.get("name").asText().toLowerCase().equals("resource-id")) {
					taskList.add((Future<JSONObject>) executor.g().parallelobject().submit(() -> {
						return fnElements("resource-id", attr.get("value").asText());
					}));
				}
			}
		}
		return null;
	}

	public JSONObject killtask(ArrayList<Future<JSONObject>> taskList)
			throws InterruptedException, ExecutionException, TimeoutException {
		JSONObject s = null;
		while (!executor.g().parallelobject().isShutdown() && taskList.size() != 0) {
			for (int i = 0; i < taskList.size(); i++) {
				if (!taskList.get(i).isDone()) {
					continue;
				} else {
					s = taskList.get(i).get();
					if (s != null) {
						executor.g().parallelobject().shutdownNow();
						taskList.removeAll(taskList);
						break;
					} else {
						taskList.remove(i);
					}
				}
			}
		}
		return s;
	}

	public JSONObject fnElements(String identifiername, String value) {
		try {
			List<WebElement> elements = null;
			if ((elements = (getElements(identifiername.contains("xpath") ? "xpath" : identifiername,
					value.replace("\\", "\"")))).size() == 1) {
				Integer i = 0;
				boolean ena = false, dis = false;
				while (!(ena && dis) && i++ < 900) {
					ena = elements.get(0).isEnabled();
					dis = elements.get(0).isDisplayed();
				}
				return new JSONObject().put("name", identifiername).put("value", value);
			} else {
				return null;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "click_using_ocr", action_displayname = "click_using_ocr", actionname = "click_using_ocr", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean click_using_ocr() {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));
				req.put("image2", attribute.get("value").asText());
				req.put("width", width());
				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				// req.put("device_recorded",);
				req = HttpUtility.sendPost("http://172.104.183.14:32770/image_rec_forms", req.toString(), headers);

				Boolean status = req.getBoolean("status");
				// Boolean text_present = req.getBoolean("text present");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

					return Tap(x, y);
				}

				else {
					return false;
				}

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null) {
					if (Tap(attribute.get("name").asText(), attribute.get("value").asText()))
						return true;
					else
						return false;
				} else
					return false;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "entertext_using_ocr", action_displayname = "entertext_using_ocr", actionname = "entertext_using_ocr", paraname_equals_curobj = "true", paramnames = {"value"}, paramtypes = {"String"})
	public Boolean entertext_using_ocr(String value) {
		try {
			JsonNode width_recorded = null;
			JsonNode height_recorded = null;

			attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
			width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
			height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");

			if (attribute != null) {

				JSONObject req = new JSONObject();
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));
				req.put("image2", attribute.get("value").asText());
				req.put("width", width());
				req.put("height", height());
				req.put("text", "");
				req.put("rt_top", "");
				req.put("device", "xr");
				if (width_recorded == null) {
					req.put("device_recorded_width", "null");
				} else {
					req.put("device_recorded_width", width_recorded.get("value").asInt());
				}

				if (height_recorded == null) {
					req.put("device_recorded_height", "null");
				}

				else {
					req.put("device_recorded_height", height_recorded.get("value").asInt());
				}

				if (width_recorded == null || height_recorded == null) {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

				}

				else {
					req = HttpUtility.sendPost("http://172.104.183.14:32770/image_rec_forms", req.toString(), headers);
				}
				Boolean status = req.getBoolean("status");

				if (status) {

					int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
					int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());
					int x_cen = Integer.parseInt(req.getJSONObject("center").get("x").toString());
					int y_cen = Integer.parseInt(req.getJSONObject("center").get("y").toString());

					if (width_recorded == null || height_recorded == null) {
						Tap(x + 20, y + 20);
					}

					else {
						Tap(x_cen, y_cen);
						Thread.sleep(2000);

					}
					return sendkeysByOther(value);
				} else
					return false;

			} else {
				attribute = InitializeDependence.findattr(curobject.getAttributes(),
						uniqueElement(curobject).getString("name"));
				if (attribute != null)
					return Sendkeys(attribute.get("name").asText(), attribute.get("value").asText(), value);
				else {
					return sendkeysByOther(value);
				}
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "keyboard_search", action_displayname = "keyboard_search", actionname = "keyboard_search", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean keyboard_search() {
		try {
			Thread.sleep(3000);
			getDriver().executeScript("mobile: performEditorAction", ImmutableMap.of("action", "search"));
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "click multi element by dynamic xpath", action_displayname = "clickmultielementbydynamicxpath", actionname = "clickmultielementbydynamicxpath",paraname_equals_curobj = "false", paramnames = { "params" }, paramtypes = { "string" })
	public Boolean clickmultielementbydynamicxpath(String... params) {
		try {
			for (String param : params) {
				if (!clickusingdynamicxpath(param)) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			logger.info(e.getMessage());
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "select mutiple items from dropdown", action_displayname = "selectmutipleitemsfromdropdown", actionname = "selectmutipleitemsfromdropdown",paraname_equals_curobj = "false", paramnames = { "xpath", "values" }, paramtypes = { "string", "string" })
	public boolean selectmutipleitemsfromdropdown(String xpath, String values) {

		try {

			String[] data = values.split(",");
			AndroidElement element2 = null;
//			int attribute =(int)48.333333;
			attribute2 = InitializeDependence.findattr(curobject.getAttributes(), "x");

			for (int i = 0; i < data.length; i++) {
				getXml();
				int startpoint = -1;
				String xpath_made = xpath.replaceAll("#replace", data[i]);
				AndroidElement element = null;

				element = (AndroidElement) getDriver().findElement(By.xpath(xpath_made));
				boolean flag = true;
				if (element2 != null) {
					startpoint = element2.getLocation().getY();
				} else {
					startpoint = element.getLocation().getY();
				}
				do {

					if (element.isDisplayed()) {
						Tap(element);
						flag = false;

					}

					else {

						int scrollEnd = startpoint - 200;
						int xcoordinate = (int) Math.round(attribute.get("value").asDouble());
						xcoordinate = (int) ((xcoordinate * width()) / 100);
						swipe(xcoordinate, startpoint, xcoordinate, scrollEnd, 2000);

					}

				} while (flag);

				element2 = element;
				flag = true;

			}
			return true;

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "scroll till element exist", action_displayname = "scrolltillelementexist", actionname = "scrolltillelementexist",paraname_equals_curobj = "false", paramnames = { "xpath", "values" }, paramtypes = { "string", "string" })
	public boolean scrolltillelementexist(String xpath, String values) {

		try {

			String[] data = values.split(",");
			AndroidElement element2 = null;
			AndroidElement element = null;
			Boolean check = true;
			for (int i = 0; i < data.length; i++) {
				int startpoint = -1;
				if (check)
					xpath = "//android.widget.TextView[@text ='" + data[i] + "']";

				else {
					check = false;
					i = -1;
				}

				element = (AndroidElement) getDriver().findElement(By.xpath(xpath));
//				String xpath_made = xpath.replaceAll("#replace", data[i]);
				boolean flag = true;
				if (element2 != null) {
					startpoint = element2.getLocation().getY();
				} else {
					startpoint = element.getLocation().getY();
				}
				do {

					if (element.isDisplayed()) {
//						Tap(element);
						flag = false;

					}

					else {

						int scrollEnd = startpoint - 200;
						int xcoordinate = (int) Math.round(attribute.get("value").asDouble());
						xcoordinate = (int) ((xcoordinate * width()) / 100);
						swipe(xcoordinate, startpoint, xcoordinate, scrollEnd, 2000);

					}

				} while (flag);

				element2 = element;
				flag = true;

			}
			return true;

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "scroll till time", action_displayname = "scrolltilltime", actionname = "scrolltilltime",paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean scrolltilltime() {

		try {
			for (int i = 0; i < 6; i++) {
				scrolldown();
				Thread.sleep(1000);

			}
			return true;
		}

		catch (Exception e) {
			System.out.println(e);
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "read and store", action_displayname = "readandstore", actionname = "readandstore",paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean readandstore(String runtimePramaname) {
		try {
			JSONObject idnamevalue = uniqueElement(curobject);

			if (idnamevalue != null) {
				JSONObject re = new JSONObject();
				AndroidElement Element = getElement(idnamevalue.getString("name"), idnamevalue.getString("value"));
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
				if (jsonObject.has("createdBy")) {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

				}
				re.put("paramvalue", Element.getAttribute("text"));
				InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
				logger.info("Text: " + re.get("paramvalue") + "stored into :" + runtimePramaname);
				return true;

			}

			else {
				JSONObject re = new JSONObject();
				// AndroidElement Element = getElement(idnamevalue.getString("name"),
				// idnamevalue.getString("value"));
				if (curExecution == null) {
					curExecution = new Setupexec();
					curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
					curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
					curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
					curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
					header.put("content-type", "application/json");
					header.put("authorization", curExecution.getAuthKey());
				}
				String token = curExecution.getAuthKey();
				String[] split = token.split("\\.");
				String base64EncodedBody = split[1];
				org.apache.commons.codec.binary.Base64 base64Url = new org.apache.commons.codec.binary.Base64(true);
				JSONObject jsonObject = null;
				jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
				if (jsonObject.has("createdBy")) {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("createdBy").toString());

				} else {

					re.put("paramname", runtimePramaname + "_" + jsonObject.get("updatedBy").toString());

				}
				// re.put("paramvalue", Element.getAttribute("text"));
				re.put("paramvalue", "0");
				InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
				logger.info("Text: " + re.get("paramvalue") + "stored into :" + runtimePramaname);
				return true;

			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "data increase", action_displayname = "dataincrease", actionname = "dataincrease",paraname_equals_curobj = "false", paramnames = { "ele1", "ele2" }, paramtypes = { "string", "string"})
	public boolean dataincrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 > item1) {
			return true;
		}

		else {
			return false;
		}
	}

	
	@Sqabind(object_template="Sqa Mobile",action_description = "data decrease", action_displayname = "datadecrease", actionname = "datadecrease",paraname_equals_curobj = "false", paramnames = { "ele1", "ele2" }, paramtypes = { "string", "string" })
	public boolean datadecrease(String ele1, String ele2) {
		int item1 = Integer.parseInt(ele1);
		int item2 = Integer.parseInt(ele2);

		if (item2 < item1) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "date sequence", action_displayname = "datesequence", actionname = "datesequence",paraname_equals_curobj = "false", paramnames = { "xpath", "no" }, paramtypes = { "string", "string" })
	public boolean datesequence(String xpath, String no) {

		try {
			boolean flag = false;
			AndroidElement element = null;
			String xpath1;
			String xpath2;

			for (int i = 0; i < Integer.parseInt(no) - 1; i++) {

				getXml();

				if (i == 0) {
					xpath1 = "(//android.widget.TextView[contains(@text,'Today at')])[1]";
					xpath2 = "(//android.widget.TextView[contains(@text,'Today at')])[2]";
				} else {
					xpath1 = "(//android.widget.TextView[contains(@text,'Today at')])[2]";
					xpath2 = "(//android.widget.TextView[contains(@text,'Today at')])[3]";
				}

				AndroidElement Element1 = getElement("xpath", xpath1);
				AndroidElement Element2 = getElement("xpath", xpath2);

				String date1 = Element1.getAttribute("text");
				String date2 = Element2.getAttribute("text");
				int check = 0;

				String[] date_1 = date1.split(" ");
				String[] date_2 = date2.split(" ");

				if (date_1[3].equals("pm")) {

					String[] splitter = date_1[2].split(":");
					check = 12 + Integer.parseInt(splitter[0]);
					System.out.println(check);
					date_1[2] = String.valueOf(check).concat(":").concat(splitter[1]);
					date1 = date_1[0].concat(" ".concat(date_1[1]).concat(" ").concat(date_1[2]));
				}

				else {
					date1 = date_1[0].concat(" ".concat(date_1[1]).concat(" ").concat(date_1[2]));
				}

				if (date_2[3].equals("pm")) {

					String[] splitter = date_2[2].split(":");
					check = 12 + Integer.parseInt(splitter[0]);
					System.out.println(check);
					date_2[2] = String.valueOf(check).concat(":").concat(splitter[1]);
					date2 = date_2[0].concat(" ".concat(date_2[1]).concat(" ").concat(date_2[2]));
				} else {
					date2 = date_2[0].concat(" ".concat(date_2[1]).concat(" ").concat(date_2[2]));
				}

				if ((date1.contains("Today") && date2.contains("Today"))
						|| (date1.contains("Yesterday") && date2.contains("Yesterday"))) {

					if ((Float.parseFloat(date_1[2].replace(":", ".")) > Float.parseFloat(date_2[2].replace(":", ".")))

							|| (Float.parseFloat(date_1[2].replace(":", ".")) == Float
									.parseFloat(date_2[2].replace(":", ".")))) {
						flag = true;

					}

				}

				else if (date1.contains("Today") && date2.contains("Yesterday")) {
					flag = true;
				}

				else {
					flag = false;
				}

				element = (AndroidElement) getDriver().findElement(By.xpath(xpath2));

				int startpoint = element.getLocation().getY();
				int xcoordinate = element.getLocation().getX();

				int scrollEnd = startpoint - 400;

				swipe(xcoordinate, startpoint + 150, xcoordinate, scrollEnd, 2000);
				Thread.sleep(3000);
			}

			return flag;
		} catch (Exception e) {
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "scroll and verify element not exist", action_displayname = "scrollandverifyelementnotexist", actionname = "scrollandverifyelementnotexist", paraname_equals_curobj = "false", paramnames = { "value" }, paramtypes = { "string" })
	public boolean scrollandverifyelementnotexist(String value) {

		try {

			Boolean flag = false;

			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

			for (JsonNode attr : curobject.getAttributes()) {

				if (attr.has("unique") && attr.get("unique").asBoolean()) {

					attribute = attr;

					break;

				}

			}

			for (int i = 1; i <= 25; i++) {

				if (exists(attribute.get("name").asText(),
						attribute.get("value").asText().replace("#replace", value))) {

					flag = true;

					break;

				} else

					scrollDown();

			}

			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);

			if (flag)

				return false;

			else

				return true;

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		}

	}

	// if else methods
	
	@Sqabind(object_template="Sqa Mobile",action_description = "elementpresent", action_displayname = "elementpresent", actionname = "elementpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementpresent() {
		getXml();

		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return false;
		} else {
			return true;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element not present", action_displayname = "elementnotpresent", actionname = "elementnotpresent", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotpresent() {
		AndroidElement element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return true;
		} else {
			return false;

		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element visible", action_displayname = "elementvisible", actionname = "elementvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementvisible() {

		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		List<WebElement> elements = null;

		if (attribute == null) {
			return false;
		} else {
			elements = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (elements.get(0).isDisplayed()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element not visible", action_displayname = "elementnotvisible", actionname = "elementnotvisible", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotvisible() {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return true;
		} else {
			element = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (!element.get(0).isDisplayed()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element disabled", action_displayname = "elementdisabled", actionname = "elementdisabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementdisabled() {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return true;
		} else {
			element = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (!element.get(0).isEnabled()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element enabled", action_displayname = "elementenabled", actionname = "elementenabled", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementenabled() {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return false;
		} else {
			element = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (element.get(0).isEnabled()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element checked", action_displayname = "elementchecked", actionname = "elementchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementchecked() {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return false;
		} else {
			element = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (element.get(0).isSelected()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "element not checked", action_displayname = "elementnotchecked", actionname = "elementnotchecked", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean elementnotchecked() {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		if (attribute == null) {
			return false;
		} else {
			element = getElements(attribute.get("name").asText(), attribute.get("value").asText());
			if (!element.get(0).isSelected()) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element contains the value.", action_displayname = "elementcontainsvalue", actionname = "elementcontainsvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementcontainsvalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));
		if (Element.getAttribute("text").equals(value)) {
			return true;
		}

		else {
			return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element not contains the value.", action_displayname = "elementnotcontainsvalue", actionname = "elementnotcontainsvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementnotcontainsvalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));
		if (Element.getAttribute("text").equals(value)) {
			return false;
		}

		else {
			return true;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is equal to the parameter value.", action_displayname = "elementequalstovalue", actionname = "elementequalstovalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementequalstovalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Element.getAttribute("text").equals(value)) {
			return true;
		}

		else {
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is not equal to the parameter value.", action_displayname = "elementnotequalstovalue", actionname = "elementnotequalstovalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementnotequalstovalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Element.getAttribute("text").equals(value)) {
			return false;
		}

		else {
			return true;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is greater then parameter value.", action_displayname = "elementgreaterthanvalue", actionname = "elementgreaterthanvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementgreaterthanvalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Integer.parseInt(Element.getAttribute("text")) > Integer.parseInt(value)) {
			return true;
		}

		else {
			return true;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is greater then or equal to the parameter value.", action_displayname = "elementgreaterthanorequalstovalue", actionname = "elementgreaterthanorequalstovalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementgreaterthanorequalstovalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Integer.parseInt(Element.getAttribute("text")) >= Integer.parseInt(value)) {
			return true;
		}

		else {
			return true;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is less then parameter value.", action_displayname = "elementlessthanvalue", actionname = "elementlessthanvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementlessthanvalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Integer.parseInt(Element.getAttribute("text")) < Integer.parseInt(value)) {
			return true;
		}

		else {
			return true;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is less then or equal to the parameter value.", action_displayname = "elementlessthanorequalstovalue", actionname = "elementlessthanorequalstovalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementlessthanorequalstovalue(String value) {
		List<WebElement> element = null;
		attribute = InitializeDependence.findattr(curobject.getAttributes(),
				uniqueElement(curobject).getString("name"));

		AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());
		setExepectedValue(Element.getAttribute("text"));

		if (Integer.parseInt(Element.getAttribute("text")) <= Integer.parseInt(value)) {
			return true;
		}

		else {
			return true;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value matches to the parameter value.", action_displayname = "elementmatchesvalue", actionname = "elementmatchesvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementmatchesvalue(String expectedValue) {
		try {
			List<WebElement> element = null;
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));

			AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());

			Pattern p = Pattern.compile(expectedValue, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(Element.getAttribute("text"));
			boolean match = m.find();
			if (match) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value not matches to the parameter value.", action_displayname = "elementnotmatchesvalue", actionname = "elementnotmatchesvalue", paraname_equals_curobj = "false", paramnames = { "Valuetocheck" }, paramtypes = { "string" })
	public boolean elementnotmatchesvalue(String expectedValue) {
		try {
			List<WebElement> element = null;
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));

			AndroidElement Element = getElement(attribute.get("name").asText(), attribute.get("value").asText());

			Pattern p = Pattern.compile(expectedValue, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(Element.getAttribute("text"));
			boolean match = m.find();
			if (match) {
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Start application using serial number.", action_displayname = "startapp", actionname = "startapp", paraname_equals_curobj = "false", paramnames = { "package", "activity", "serialno" }, paramtypes = { "string","string","string" })
	public Boolean startapp(String Package, String Activity, String serialno) {
		try {
			if (count == 10) {
				count = 0;
				return false;
			}

			Boolean devicepresent = false;

			String deviceslist = getdeviceList();
			String[] devicess = deviceslist.split("\t");

			for (int i = 0; i < devicess.length; i++) {
				if (devicess[i].trim().equals(serialno)) {
					devicepresent = true;
					continue;
				}
			}

			if (devicepresent) {
				count++;
				ServerSocket serverSocket;
				String port = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
				serverSocket.close();
				DesiredCapabilities capabilitiesApp = new DesiredCapabilities();
				capabilitiesApp.setCapability(CapabilityType.BROWSER_NAME, "");
				capabilitiesApp.setCapability("skipDeviceInitialization", true);
				capabilitiesApp.setCapability("skipServerInstallation", true);
				capabilitiesApp.setCapability("skipServerLaunch", true);
				capabilitiesApp.setCapability("deviceName", getDevice().getName());
				capabilitiesApp.setCapability("udid", getDevice().getSerialNumber());
				capabilitiesApp.setCapability("systemPort", port);
				capabilitiesApp.setCapability("platformName", org.openqa.selenium.Platform.ANDROID);
				capabilitiesApp.setCapability("platform", org.openqa.selenium.Platform.ANDROID);
				capabilitiesApp.setCapability(MobileCapabilityType.PLATFORM_VERSION,
						getDevice().getProperty("ro.build.version.release"));
				capabilitiesApp.setCapability("newCommandTimeout", 2000000);
				capabilitiesApp.setCapability("autoWebView", true);
				capabilitiesApp.setCapability("ensureWebviewsHavePages", true);
				capabilitiesApp.setCapability("enableWebviewDetailsCollection", true);
				capabilitiesApp.setCapability("appPackage", Package);
				capabilitiesApp.setCapability("appActivity", Activity);
				capabilitiesApp.setCapability("noReset", true);
				capabilitiesApp.setCapability("automationName", "uiautomator2");
				capabilitiesApp.setCapability("noSign", true);
				capabilitiesApp.setCapability("settings[allowInvisibleElements]", true);
				capabilitiesApp.setCapability("settings[shouldUseCompactResponses]", false);
				logger.info(capabilitiesApp.toString());
				logger.info("Application {} {} started", Package, Activity);
				driver = (RemoteWebDriver) new AndroidDriver(new URL(GlobalDetail.hostname.get(serialno)),
						capabilitiesApp);
				driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
				GlobalDetail.sqaDriver.put(serialno, driver);
//			GlobalDetail.hostname = hostname;
				return true;

			} else {
				logger.info("Device not connected.");
				return false;
			}
		} catch (Exception e) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//			e.printStackTrace();
			if (startapp(Package, Activity))
				return true;
			else
				return false;
		}
	}

	public String getdeviceList() {
		try {
			File adbpath = null;
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			Process p1;
			p1 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "devices" });
//			p1.waitFor();
			InputStream stdIn = p1.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			BufferedReader br = new BufferedReader(isr);
			StringBuilder xmlString = new StringBuilder();
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				xmlString.append(line);

			return xmlString.toString().replace("List of devices attached", "").replace("device", "");

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return null;
		}
	}

	public Boolean initializeandroiddriver(String Package, String Activity) {
		try {
			if (count == 10) {
				count = 0;
				return false;
			}
			count++;
			ServerSocket serverSocket;
			String port = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
			serverSocket.close();
			DesiredCapabilities capabilitiesApp = new DesiredCapabilities();
			capabilitiesApp.setCapability(CapabilityType.BROWSER_NAME, "");
			capabilitiesApp.setCapability("skipDeviceInitialization", true);
			capabilitiesApp.setCapability("skipServerInstallation", true);
			capabilitiesApp.setCapability("skipServerLaunch", true);
			capabilitiesApp.setCapability("deviceName", getDevice().getName());
			capabilitiesApp.setCapability("udid", getDevice().getSerialNumber());
			capabilitiesApp.setCapability("systemPort", port);
			capabilitiesApp.setCapability("platformName", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability("platform", org.openqa.selenium.Platform.ANDROID);
			capabilitiesApp.setCapability(MobileCapabilityType.PLATFORM_VERSION,
					getDevice().getProperty("ro.build.version.release"));
			capabilitiesApp.setCapability("newCommandTimeout", 2000000);
			capabilitiesApp.setCapability("autoWebView", true);
			capabilitiesApp.setCapability("ensureWebviewsHavePages", true);
			capabilitiesApp.setCapability("enableWebviewDetailsCollection", true);
			capabilitiesApp.setCapability("appPackage", Package);
			capabilitiesApp.setCapability("appActivity", Activity);
			capabilitiesApp.setCapability("noReset", true);
			capabilitiesApp.setCapability("automationName", "uiautomator2");
			capabilitiesApp.setCapability("noSign", true);
			capabilitiesApp.setCapability("settings[allowInvisibleElements]", true);
			capabilitiesApp.setCapability("settings[shouldUseCompactResponses]", false);

			capabilitiesApp.setCapability("nativeWebScreenshot", true);
			logger.info(capabilitiesApp.toString());
			logger.info("Application {} {} started", Package, Activity);
			driver = (RemoteWebDriver) new AndroidDriver(
					new URL(GlobalDetail.hostname.get(getDevice().getSerialNumber())), capabilitiesApp);
			driver.manage().timeouts().implicitlyWait(GlobalDetail.maXTimeout, TimeUnit.SECONDS);
			GlobalDetail.androiddriver_ce.put(getDevice().getSerialNumber(), driver);
//			GlobalDetail.hostname = hostname;
			return true;
		} catch (Exception e) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//			e.printStackTrace();
			if (startapp(Package, Activity))
				return true;
			else
				return false;
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Short scroll", action_displayname = "shortscrollpage", actionname = "shortscrollpage", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean shortscrollpage() {
		try {
			
			

			File adbpath = null;
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			Process p1;
			String a = "100 200 0 0";
			p1 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "shell", "input", "touchscreen",
					"swipe", String.valueOf(100), String.valueOf(300), String.valueOf(0), String.valueOf(0) });

			p1.waitFor();
			InputStream stdIn = p1.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			BufferedReader br = new BufferedReader(isr);
			StringBuilder xmlString = new StringBuilder();
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				xmlString.append(line);
			
			Thread.sleep(2000);
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}

	@Sqabind(object_template="Sqa Mobile",action_description = "Long Scroll", action_displayname = "longscrollpage", actionname = "longscrollpage", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean longscrollpage() {
		try {

			File adbpath = null;
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			Process p1;
			String a = "100 200 0 0";
			p1 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "shell", "input", "touchscreen",
					"swipe", String.valueOf(0), String.valueOf(1300), String.valueOf(0), String.valueOf(0) });

			p1.waitFor();
			InputStream stdIn = p1.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			BufferedReader br = new BufferedReader(isr);
			StringBuilder xmlString = new StringBuilder();
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				xmlString.append(line);
			Thread.sleep(2000);
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}

	public boolean launchurl(String url) {
		try {
			driver.navigate().to(url);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean tab() {
		try {
		AndroidMethod me = new AndroidMethod();
		Thread.sleep(1000);
		me.getDriver().getKeyboard().pressKey(Keys.TAB);
		
//		me.getDriver().getKeyboard().pressKey(Keys.TAB);
		return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
	}
	
	public boolean keyboardentertext(String value) {
		try {
		AndroidMethod me = new AndroidMethod();
		me.getDriver().getKeyboard().sendKeys(value);
		return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public Boolean cleartextusingadb(int length) {
		try {
			File adbpath = null;
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			
			Process p2;
			p2 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "shell","input","keyevent","123" });
			Thread.sleep(1000);
			
			AndroidMethod me = new AndroidMethod();
			
//			
			for(int i=0;i<25;i++) {
				System.out.print(length);
				System.out.println("i : "+i);
				Thread.sleep(600);
				Process p1;
			p1 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "shell","input","keyevent","67" });
			
			}
			return true;
			
		}
		catch (Exception e) {
			// TODO: handle exception
			logger.info("could not delete text");
			logger.error(e.toString());
			return false;
		}
	}
	//image process method
	public Boolean cleartextandenter(String value) {
		try {
		JsonNode width_recorded = null;
		JsonNode height_recorded = null;

		attribute = InitializeDependence.findattr(curobject.getAttributes(), "cropimg");
		width_recorded = InitializeDependence.findattr(curobject.getAttributes(), "width");
		height_recorded = InitializeDependence.findattr(curobject.getAttributes(), "height");

		if (attribute != null) {
			JSONObject req = new JSONObject();
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			req.put("image1", createImageFromByte(GlobalDetail.screenshot.get(device.getSerialNumber())));
			req.put("image2", attribute.get("value").asText());
			req.put("width", width());
			req.put("height", height());
			req.put("text", "");
			req.put("rt_top", "");
			req.put("device", "xr");
			if (width_recorded == null) {
				req.put("device_recorded_width", "null");
			} else {
				req.put("device_recorded_width", width_recorded.get("value").asInt());
			}

			if (height_recorded == null) {
				req.put("device_recorded_height", "null");
			}

			else {
				req.put("device_recorded_height", height_recorded.get("value").asInt());
			}

			if (width_recorded == null || height_recorded == null) {
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img_old", req.toString(), headers);

			}

			else {
				req = HttpUtility.sendPost("http://172.104.183.14:32770/img", req.toString(), headers);
			}
			Boolean status = req.getBoolean("status");
			// Boolean text_present = req.getBoolean("text present");

			if (status) {

				int x = Integer.parseInt(req.getJSONObject("top_left").get("x").toString());
				int y = Integer.parseInt(req.getJSONObject("top_left").get("y").toString());

				int x1 = Integer.parseInt(req.getJSONObject("center").get("x").toString());
				int y1 = Integer.parseInt(req.getJSONObject("center").get("y").toString());

				if (width_recorded == null || height_recorded == null) {
					return Tap(x + 20, y + 20);

				}

				else {
					
					if(cleartextusingadb(value.length())) {					
					Thread.sleep(2000);
					return SendkeysByAdb(value);
					}else {
						return false;
					}
				}

			}

			else {
				return false;
			}

		} else {
			attribute = InitializeDependence.findattr(curobject.getAttributes(),
					uniqueElement(curobject).getString("name"));
			if (attribute != null) {

				getElement(attribute.get("name").asText(), attribute.get("value").asText());
				if (Tap(attribute.get("name").asText(), attribute.get("value").asText())) {
					logger.info("Executed Click on :{} ", attribute.get("value").asText());
					return true;
				} else {
					logger.info("Failed to Execute Click on :{} ,No Element Found",
							attribute.get("value").asText());
					throw new Exception("Unable to click");
				}
			} else {
				logger.info("No Unique attribute in Object");
				throw new Exception("No Unique found");
			}

		}

	} catch (Exception e) {
		logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
		return false;
	}
	}
	
	@Sqabind(object_template="Sqa Mobile",action_description = "Check that element value is less then or equal to the parameter value.", action_displayname = "keyboardsearch_bycoordinate", actionname = "keyboardsearch_bycoordinate", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean keyboardsearch_bycoordinate() {
		int w=width()-100;
		int h=height()-220;
		Tap(w,h);
		return true;
	}

}
