package com.simplifyqa.method;

import java.awt.AWTException;

//import com.github.underscore.lodash.U;
import java.awt.Robot;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Executors;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bouncycastle.tsp.GenTimeAccuracy;
import org.bson.BsonDocument;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.github.javafaker.Faker;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.util.JSON;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.DTO.Step;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.customMethod.Sqabind;
import com.simplifyqa.execution.AutomationExecuiton;
import com.simplifyqa.logger.LoggerClass;
import com.simplifyqa.method.Interface.Generalmethod;
import com.sun.mail.gimap.GmailFolder;
import com.sun.mail.gimap.GmailStore;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.EmailMessageSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;

//import groovy.json.internal.JsonFastParser;

import com.simplifyqa.FileOperations.*;

public class GeneralMethod extends ApiMethod implements Generalmethod {
	private static final org.apache.log4j.Logger userlogger = org.apache.log4j.LogManager
			.getLogger(GeneralMethod.class);
	LoggerClass logerOBJ = new LoggerClass();
	private final Logger logger = LoggerFactory.getLogger(GeneralMethod.class);
	private static Connection dbconnection = null;
	public static Setupexec curExecution = null;
	public HashMap<String, String> header = new HashMap<String, String>();
	public static Integer currentSeq = -1;
	private Class myClass = null;
	private Editorclassloader customMethod = null;
	private String classname = "CustomMethodsdb";
	private Boolean dbrun = false;
	public String exepectedValue = null;
	public static Step curStep = null;
	private String runtimevalue = null;
	private String jsonkey = null;
	private String jsonvalue = null;
	private String MemoryUtilization = null;
	private String keynotfound = null;
	public static int response_count = 0;
	private static ExchangeService service;
	public MongoClient mongoClient = null;
	boolean key_found = false;

	public String getKeynotfound() {
		return keynotfound;
	}

	public void setKeynotfound(String keynotfound) {
		this.keynotfound = keynotfound;
	}

	public String getMemoryUtilization() {
		return MemoryUtilization;
	}

	public void setMemoryUtilization(String memoryUtilization) {
		MemoryUtilization = memoryUtilization;
	}

	public String getJsonkey() {
		return jsonkey;
	}

	public void setJsonkey(String jsonkey) {
		this.jsonkey = jsonkey;
	}

	public String getJsonvalue() {
		return jsonvalue;
	}

	public void setJsonvalue(String jsonvalue) {
		this.jsonvalue = jsonvalue;
	}

	private HashMap<String, String> rpParam = null;

	public String getExepectedValue() {
		return exepectedValue;
	}

	public void setExepectedValue(String exepectedValue) {
		this.exepectedValue = exepectedValue;
	}

	public GeneralMethod() {

	}

	public Boolean getDbrun() {
		return dbrun;
	}

	public void setDbrun(Boolean dbrun) {
		this.dbrun = dbrun;
	}

	public GeneralMethod(Setupexec curExecution, HashMap<String, String> header, Integer currentSeq) {
		this.curExecution = curExecution;
		this.header = header;
		this.currentSeq = currentSeq;
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action generates a random alphanumeric value and stores it to runtime parameter", action_displayname = "randomalphanumeric", actionname = "randomalphanumeric", paraname_equals_curobj = "false", paramnames = { "count","prefix","suffix","parameter" }, paramtypes = { "String","String","String","String" })
	public boolean randomalphanumeric(String count, String prefix, String suffix, String parameter) {
		StringBuilder builder = new StringBuilder();
		int cn = Integer.parseInt(count);
		while (cn-- != 0) {
			int character = (int) (Math.random() * UtilEnum.ALPHA_NUMERIC.value().length());
			builder.append(UtilEnum.ALPHA_NUMERIC.value().charAt(character));
		}
		return storeruntime(parameter, prefix + builder.toString() + suffix);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action is used to get value from database", action_displayname = "getfromruntime", actionname = "getfromruntime", paraname_equals_curobj = "true", paramnames = {"identifiername"}, paramtypes = {"String"})
	public Boolean getfromruntime(String identifiername) {
		JSONObject res;
		runtimevalue = null;
		
		logger.info("getfromruntime :" + identifiername);
		try {
			if (curExecution == null || header.isEmpty()) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}

			System.out.println("curexe :" + curExecution);
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			Base64 base64Url = new Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
			if (jsonObject.has("createdBy")) {

				res = InitializeDependence.serverCall.getruntimeParameter(curExecution,
						identifiername + "_" + jsonObject.get("createdBy").toString(), header);

			} else {
				res = InitializeDependence.serverCall.getruntimeParameter(curExecution,
						identifiername + "_" + jsonObject.get("updatedBy").toString(), header);

			}
			curExecution = null;
			if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
				logger.info("Executed  getRuntime :{}", res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("value"));
				runtimevalue = res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value())
						.getJSONObject(0).getString("value");
				this.exepectedValue = runtimevalue;
				return stashRuntime(identifiername, runtimevalue);
			} else
				logger.info("Failed to  getRuntime ");
			throw new Exception("Failed to getRuntime ");
		} catch (

		Exception e) {
			e.printStackTrace();
			runtimevalue = null;
			return false;
		}

	}

	private Boolean stashRuntime(String runtimeParamName, String runtimeValue) {
		try {
			if (curExecution != null) {
				if (runtimeValue != null) {
					if (GlobalDetail.runTime.containsKey(Thread.currentThread().getName().toLowerCase().trim())) {
						((ObjectNode) GlobalDetail.runTime.get(Thread.currentThread().getName().toLowerCase().trim()))
								.put(runtimeParamName, runtimeValue);
						return true;
					} else {
						GlobalDetail.runTime.put(Thread.currentThread().getName().toLowerCase().trim(),
								new ObjectMapper()
										.readTree(new JSONObject().put(runtimeParamName, runtimeValue).toString()));
						return true;
					}
				} else {
					return false;
				}
			} else {
				if (runtimeValue != null) {
					if (curStep.getFunctionCode() != null) {
						if (GlobalDetail.runTime.containsKey(Thread.currentThread().getName().toLowerCase().trim())) {
							((ObjectNode) GlobalDetail.runTime
									.get(Thread.currentThread().getName().toLowerCase().trim())).put(runtimeParamName,
											runtimeValue);
							return true;
						} else {
							GlobalDetail.runTime.put(Thread.currentThread().getName().toLowerCase().trim(),
									new ObjectMapper()
											.readTree(new JSONObject().put(runtimeParamName, runtimeValue).toString()));
							return true;
						}
					} else {
						if (GlobalDetail.runTime.containsKey(Thread.currentThread().getName().toLowerCase().trim())) {
							((ObjectNode) GlobalDetail.runTime
									.get(Thread.currentThread().getName().toLowerCase().trim())).put(runtimeParamName,
											runtimeValue);
							return true;
						} else {
							GlobalDetail.runTime.put(Thread.currentThread().getName().toLowerCase().trim(),
									new ObjectMapper()
											.readTree(new JSONObject().put(runtimeParamName, runtimeValue).toString()));
							return true;
						}
					}
				} else {
					return false;
				}

			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Override
	public boolean storeruntime(String paraname, String paravalue) {
		try {
			logger.info(curStep.getAction().getName());

			logger.info("paraname before deleting value :" + paraname);
			deleteruntime(paraname);

			JSONObject re = new JSONObject();
			if (curExecution == null) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			Base64 base64Url = new Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));

			if (jsonObject.has("createdBy")) {

				re.put("paramname", paraname + "_" + jsonObject.get("createdBy").toString());

			} else {

				re.put("paramname", paraname + "_" + jsonObject.get("updatedBy").toString());

			}
			re.put("paramvalue", paravalue);
			logger.info("store to runtime :{}", re);
			InitializeDependence.serverCall.postruntimeParameter(curExecution, re, header);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Failed To Store In DataBase: ", null, e.toString(),
					"error");
			e.printStackTrace();
			logger.info("Faild to store in database");
			storeruntime(paraname, paravalue);
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action halts the execution for a specified amount of time", action_displayname = "hold", actionname = "hold", paraname_equals_curobj = "false", paramnames = { "timeout" }, paramtypes = { "String" })
	public Boolean hold(String timeOut) {
		try {
			logger.info("INSIDE HOLD");
			Thread.sleep(Integer.parseInt(timeOut));
			logger.info("WAIT COMPLETED returning true");
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: WaitCompleted returing false: ", null, e.toString(),
					"error");
			logger.info("WAIT COMPLETED returning false");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action halts the execution for 1 second", action_displayname = "minwait", actionname = "minwait", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean minwait() {
		try {
			if (AutomationExecuiton.minwait) {
				Thread.sleep(Integer.parseInt(InitializeDependence.minwait));
			} else {
				Thread.sleep(1000);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action halts the execution for 3 second", action_displayname = "medwait", actionname = "medwait", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean medwait() {
		try {
			if (AutomationExecuiton.medwait) {
				Thread.sleep(Integer.parseInt(InitializeDependence.medwait));
			} else {
				Thread.sleep(3000);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action halts the execution for 5 second", action_displayname = "maxwait", actionname = "maxwait", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean maxwait() {
		try {
			if (AutomationExecuiton.maxwait) {
				Thread.sleep(Integer.parseInt(InitializeDependence.maxwait));
			} else {
				Thread.sleep(5000);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action generates a random numeric value and stores it to runtime parameter", action_displayname = "randomnumeric", actionname = "randomnumeric", paraname_equals_curobj = "false", paramnames = { "count","prefix","suffix","parameter" }, paramtypes = { "String","String","String","String" })
	public boolean randomnumeric(String count, String prefix, String suffix, String parameter) {
		try {
			int min = (int) Math.pow(10, Integer.parseInt(count) - 1);
			int max = (int) Math.pow(10, Integer.parseInt(count));
			Random random = new Random();
			return storeruntime(parameter, prefix + Integer.toString(random.nextInt(max - min) + min) + suffix);
		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to genrate and store in SQA runtime: ", null,
					e.toString(), "error");
			logger.info("Unable to genrate and store in SQA runtime.");
			return false;
		}
	}

	//array
	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action combines runtime variables", action_displayname = "combineruntime", actionname = "combineruntime", paraname_equals_curobj = "false", paramnames = { "count","prefix","suffix","parameter" }, paramtypes = { "String","String","String","String" })
	public boolean combineruntime(String... param1) {
		try {

			String[] parma2 = new String[param1.length - 1];

			for (int i = 0; i < param1.length - 1; i++) {

				parma2[i] = param1[i];

			}

			String finalvalue = String.join("!##", parma2);

			return storeruntime(param1[param1.length - 1], finalvalue);

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unabel to combineruntime value: ", null, e.toString(),
					"error");

			logger.info("Unabel to combineruntime value");
			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action reads the json value of a given json key ", action_displayname = "readjson", actionname = "readjson", paraname_equals_curobj = "false", paramnames = { "param1","key","index" }, paramtypes = { "String","String","String" })
	public String readjson(String param1, String key, String index) {
		try {

			return ((JSONArray) InitializeDependence.runtimeJson.get(param1)).getJSONObject(Integer.parseInt(index))

					.get(key).toString();

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to readJson: ", null, e.toString(), "error");
			logger.info("Unable to readJson.");
			return null;

		}
	}

//	public boolean connecttodb(String param, String url, String username, String password) {
//		try {
//
//			if (dbconnection == null) {
//
//				if (param.equals(UtilEnum.mySQL.value())) {
//
//					Class.forName(UtilEnum.mySQLDriver.value());
//
//					dbconnection = DriverManager.getConnection(UtilEnum.MYSQLURL + url, username, password);
//
//					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);
//
//				} else if (param.contentEquals(UtilEnum.Oracle.value())) {
//
//					Class.forName(UtilEnum.OrDriver.value());
//
//					dbconnection = DriverManager.getConnection(url, username, password);
//
//					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);
//
//				} else if (param.equals(UtilEnum.SQL.value())) {
//
//					Class.forName(UtilEnum.SQLdriver.value());
//
//					dbconnection = DriverManager.getConnection(url, username, password);
//
//					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);
//
//				}
//
//			}
//			logger.info("successfully connected to Database.");
//
//			return true;
//
//		} catch (Exception e) {
//			logger.info("Unable to connect DataBase.");
//			return false;
//
//		}
//
//	}

	@Sqabind(object_template="sqa_record",action_description = "This action is used for DB connection ", action_displayname = "connecttodb", actionname = "connecttodb", paraname_equals_curobj = "false", paramnames = { "param","url","username","password" }, paramtypes = { "String","String","String","String" })
	public boolean connecttodb(String param, String url, String username, String password) {

		try {

			if (dbconnection == null) {

				if (param.equalsIgnoreCase("mysql")) {

					Class.forName("com.mysql.jdbc.Driver");

					dbconnection = DriverManager.getConnection("jdbc:mysql://" + url, username, password);

					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);

				} else if (param.equalsIgnoreCase("Oracle")) {

					Class.forName("oracle.jdbc.driver.OracleDriver");

					dbconnection = DriverManager.getConnection(url, username, password);

					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);

				} else if (param.equalsIgnoreCase("sqlserver")) {

					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

					dbconnection = DriverManager.getConnection("jdbc:sqlserver://" + url, username, password);

//                        con = DriverManager.getConnection("jdbc:sqlserver://"+url+";databaseName=TestDB", username, password);

					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);

				} else if (param.equalsIgnoreCase("db2")) {

					Class.forName("com.ibm.db2.jcc.DB2Driver");

					dbconnection = DriverManager.getConnection("jdbc:db2://" + url, username, password);

//                    con = DriverManager.getConnection("jdbc:sqlserver://"+url+";databaseName=TestDB", username, password);

					dbconnection.setNetworkTimeout(Executors.newFixedThreadPool(1), 900000);

				}

			}

			return true;

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return false;

		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action is used for close DB connection ", action_displayname = "closeConnection", actionname = "closeConnection", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean closeConnection() {
		try {
			dbconnection.close();
			logger.info("DB connection close successfully");
			return true;

		} catch (SQLException e) {
			logger.info("Unable to close Connecton.");
			return false;

		}
	}

	public Boolean executequery(String req, String suffix) {
		try {

			Statement sMent = dbconnection.createStatement();

			sMent.executeQuery(req);

			storedbtoruntime(req, suffix);

			return true;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to executeqery for DB: ", null, e.toString(),
					"error");
			logger.info("Unable to executeqery for DB.");
			return false;

		}
	}

	public Boolean executequery(String req) {
		try {

			Statement sMent = dbconnection.createStatement();

			sMent.executeUpdate(req);

			return true;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to executeqery for DB: ", null, e.toString(),
					"error");
			logger.info("Unable to executeqery for DB.");
			return false;

		}

	}

//	public Boolean storedbtoruntime(String req, String suffix) {
//		try {
//
//			Statement sMent = dbconnection.createStatement();
//
//			ResultSet res2 = sMent.executeQuery(req);
//
//			ResultSetMetaData rsmd = res2.getMetaData();
//
//			int columnCount = rsmd.getColumnCount();
//
//			while (res2.next()) {
//
//				for (int i = 0; i < columnCount; i++) {
//					storeruntime(suffix + "_" + rsmd.getColumnName(i + 1), res2.getString(rsmd.getColumnName(i + 1)));
//				}
//
//			}
//
//			return true;
//
//		} catch (Exception e) {
//			logger.info("Unable to store db value to SQA Runtime.");
//			return false;
//
//		}
//	}
	
	@Sqabind(object_template="sqa_record",action_description = "storedbtoruntime", action_displayname = "storedbtoruntime", actionname = "storedbtoruntime", paraname_equals_curobj = "false", paramnames = { "req","suffix" }, paramtypes = { "String","String" })
	public boolean storedbtoruntime(String req, String suffix) {

		try {

			Statement sMent = dbconnection.createStatement();

			ResultSet res2 = sMent.executeQuery(req);

			ResultSetMetaData rsmd = res2.getMetaData();

			int columnCount = rsmd.getColumnCount();

			JSONArray dbarr = new JSONArray();

			JSONObject data = new JSONObject();

			while (res2.next()) {
				JSONObject dbdata = new JSONObject();

				for (int i = 0; i < columnCount; i++) {

					dbdata.put(rsmd.getColumnName(i + 1), res2.getString(rsmd.getColumnName(i + 1)));
				}
				dbarr.put(dbdata);
				data.put("data", dbarr);

			}
			return storeruntime(suffix, data.toString().replace("\"", "\\\""));

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			return false;

		}

	}

	public boolean setcustomClass(Setupexec executiondetail, HashMap<String, String> header) {
		try {
			if (this.myClass == null && this.customMethod == null) {
				this.customMethod = new Editorclassloader(Editorclassloader.class.getClassLoader());
				this.myClass = this.customMethod.loadclass("csmethods." + classname, executiondetail, header);
			}
			this.customMethod.loadClass(myClass);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean CallMethod(String identifierName, String[] value, Setupexec executiondetail,
			HashMap<String, String> header)
			throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			InstantiationException, NoSuchMethodException, SecurityException {
		try {
			if (setcustomClass(executiondetail, header)) {
				if (value == (null))
					return customMethod.ExecustomMethod(identifierName, value, classname);
//							ExecustomMethod(identifierName, value,classname);
				else {
					return customMethod.ExecustomMethod(identifierName, value, classname);
				}
			} else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to call Custom Method: ", null, e.toString(),
					"error");
			logger.error("Unable to call Custom Method !");
			return false;
		}

	}

	public Map<String, String> flattenJSON(String json) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			addKeys("", new ObjectMapper().readTree(json), map);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	private void addKeys(String currentPath, JsonNode jsonNode, Map<String, String> map) {
		if (jsonNode.isObject()) {
			ObjectNode objectNode = (ObjectNode) jsonNode;
			Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
			String pathPrefix = currentPath.isEmpty() ? "" : currentPath + "/";

			while (iter.hasNext()) {
				Map.Entry<String, JsonNode> entry = iter.next();
				addKeys(pathPrefix + entry.getKey(), entry.getValue(), map);
			}
		} else if (jsonNode.isArray()) {
			ArrayNode arrayNode = (ArrayNode) jsonNode;
			for (int i = 0; i < arrayNode.size(); i++) {
				addKeys(currentPath + "[" + i + "]", arrayNode.get(i), map);
			}
		} else if (jsonNode.isValueNode()) {
			ValueNode valueNode = (ValueNode) jsonNode;
			map.put(currentPath, valueNode.asText());
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action finds the value of a given json key and stores it to runtime parameter", action_displayname = "storejsonkey", actionname = "storejsonkey", paraname_equals_curobj = "false", paramnames = { "JsonRuntimeParam","key","runtimeToStore"}, paramtypes = { "String","String","String" })
	public Boolean storejsonkey(String JsonRuntimeParam, String key, String runtimeToStore) {
		try {
			Map<String, String> jsonStringify = flattenJSON(StringEscapeUtils.unescapeJava(JsonRuntimeParam));
			String dataFromJsonResp = jsonStringify.get(key);
			if (dataFromJsonResp != null) {
				logger.info("StoreJsonKey key found :{} with value:{}", key, dataFromJsonResp);

				this.setJsonkey(key);
				this.setJsonvalue(dataFromJsonResp);
				return storeruntime(runtimeToStore, dataFromJsonResp);
			} else {
				logger.info("StoreJsonKey key not found :{}", key);
				return false;
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	private final String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmenopqrstuvwxyz";
	
	@Sqabind(object_template="sqa_record",action_description = "This action generates a random alphabetical string and stores it to runtime parameter", action_displayname = "randomalphabets", actionname = "randomalphabets", paraname_equals_curobj = "false", paramnames = { "count","prefix","suffix","parameter" }, paramtypes = { "String","String","String","String" })
	public boolean randomalphabets(String count, String prefix, String suffix, String parameter) {
		StringBuilder builder = new StringBuilder();
		int cn = Integer.parseInt(count);
		for (int i = 0; i < cn; i++) {
			builder.append(ALPHA_STRING.charAt((int) (Math.random() * 10 % ALPHA_STRING.length())));
		}

		deleteruntime(prefix + builder.toString() + suffix);
		GlobalDetail.runTime.remove(prefix + builder.toString() + suffix);
		return storeruntime(parameter, prefix + builder.toString() + suffix);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action returns a random number as a String", action_displayname = "getRandomNumber", actionname = "getRandomNumber", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public String getRandomNumber() {
		try {
			int randomInt = 0;
			Random randomGenerator = new Random();
			randomInt = randomGenerator.nextInt(UtilEnum.CHAR_LIST.value().length());
			if (randomInt - 1 == -1) {
				return String.valueOf(randomInt);
			} else {
				return String.valueOf(randomInt - 1);
			}
		} catch (Exception e) {
			return String.valueOf(-1);
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action generates a random string and stores it to runtime parameter", action_displayname = "generaterandomstring", actionname = "generaterandomstring", paraname_equals_curobj = "false", paramnames = { "runtimeParam","prefix","strLenght" }, paramtypes = { "String","String","String" })
	public boolean generaterandomstring(String runtimeParam, String prefix, String strLenght) {
		try {
			StringBuffer randStr = new StringBuffer();
			// randStr.append(prefix + "_");
			randStr.append(prefix);
			for (int i = 0; i < Integer.parseInt(strLenght); i++) {
				int number = Integer.parseInt(getRandomNumber());
				char ch = UtilEnum.CHAR_LIST.value().charAt(number);
				randStr.append(ch);
			}
			return storeruntime(runtimeParam, randStr.toString());

		} catch (Exception e) {
			logger.info("Unable to genrate randomstring.");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to genrate randomstring: ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the given first parameter is equal to the second parameter", action_displayname = "validateparameter", actionname = "validateparameter", paraname_equals_curobj = "false", paramnames = { "paramName","value" }, paramtypes = { "String","String" })
	public boolean validateparameter(String paramName, String value) {
		try {
			if (paramName.equalsIgnoreCase(value)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.info("Unable to validate Parameter.");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to validate Parameter: ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action replaces the value of the given json key and stores it to runtime parameter ", action_displayname = "replaceinjsonandstore", actionname = "replaceinjsonandstore", paraname_equals_curobj = "false", paramnames = { "baseString","replaceWith","replacevalue","runtimeStore" }, paramtypes = { "String","String","String","String" })
	public boolean replaceinjsonandstore(String baseString, String replaceWith, String replacevalue,
			String runtimeStore) {
		try {
			String value = "";
//
//			if (getfromruntime(replacevalue))
//				value = runtimevalue;
//			else
//				throw new Exception("Error in getfromruntime");
			if (new File(baseString).isFile()) {
				baseString = new String(Files.readAllBytes(Paths.get(baseString)), StandardCharsets.UTF_8)
						.replaceAll("\r\n", "").replaceAll("\t", "");
				if (!value.isEmpty())
					replacevalue = value;
			} else {
				if (!value.isEmpty())
					replacevalue = value;
			}
			String replacedString = null;
			String[] replace1 = replaceWith.split("!##");
			String[] replace2 = replacevalue.split("!##");
			for (int i = 0; i < replace1.length; i++) {

				replacedString = baseString.replaceAll(replace1[i], replace2[i]);
			}
			ObjectMapper mapper = new ObjectMapper();
			return storeruntime(runtimeStore,
					mapper.writeValueAsString(replacedString).replace("\"{", "{").replace("}\"", "}"));

		} catch (Exception e) {
			logger.info("Unable to replace in json and store to runtime.");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to replace in json and store to runtime: ",
					null, e.toString(), "error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action replaces the string text to the given text and stores it to runtime ", action_displayname = "replaceandstore", actionname = "replaceandstore", paraname_equals_curobj = "false", paramnames = { "param1","param2","param3","param4" }, paramtypes = { "String","String","String","String" })
	public boolean replaceandstore(String param1, String param2, String param3, String param4) {
		try {
			String value = "";
			if (param3.toLowerCase().equals("#nothing#"))
				param3 = param3.toLowerCase().replaceAll("#nothing#", "");
			try {
				File jsonbody = new File(param1);
				if (jsonbody.isFile()) {
					String fileContent = readFile(param1).replaceAll("\r\n", "").replaceAll("\t", "");
					param1 = fileContent;
					if (value.isEmpty()) {
						param3 = param3;
					} else {
						param3 = value;
					}
				}

				else {
					if (value.isEmpty()) {
						param3 = param3;
					} else {
						param3 = value;
					}

				}
			} catch (Exception e) {
				param3 = param3;
			}

			String[] replace1 = param2.split("!##");
			String[] replace2 = param3.split("!##");
			for (int i = 0; i < replace1.length; i++) {
				param1 = param1.replaceAll(replace1[i], replace2[i]);

			}
			return storeruntime(param4, param1);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action splits the given string and stores the value of the given index after splitting to runtime parameter", action_displayname = "splitandgetvalue", actionname = "splitandgetvalue", paraname_equals_curobj = "false", paramnames = { "param","delimt","index","param1" }, paramtypes = { "String","String","String","String" })
	public boolean splitandgetvalue(String param, String delimt, String index, String param1) {
		try {
			delimt = delimt.replaceAll("#SPACE#", " ");
			return storeruntime(param1, param.split(delimt)[Integer.parseInt(index)]);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to split and get value: ", null, e.toString(),
					"error");
			logger.info("Unable to split and get value.");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "Compare 2 JSON documents", action_displayname = "comparejsondoc", actionname = "comparejsondoc", paraname_equals_curobj = "false", paramnames = { "path1","path2" }, paramtypes = { "String","String" })
	public boolean comparejsondoc(String path1, String path2) {
		try {
			JSONObject result = new JSONObject();
			JSONArray ref1 = new JSONArray();
			JSONArray ref2 = new JSONArray();
			JSONArray ref3 = new JSONArray();
			File jsonfile1 = new File(path1);
			File jsonfile2 = new File(path2);
			@SuppressWarnings("deprecation")
			JsonParser leftJson = new JsonFactory().createJsonParser(jsonfile1);
			@SuppressWarnings("deprecation")
			JsonParser rightJson = new JsonFactory().createJsonParser(jsonfile2);
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<Map<String, java.lang.Object>> type = new TypeReference<Map<String, java.lang.Object>>() {
			};
			Map<String, java.lang.Object> leftMap = mapper.readValue(leftJson, type);
			Map<String, java.lang.Object> rightMap = mapper.readValue(rightJson, type);
			MapDifference<String, java.lang.Object> difference = Maps.difference(leftMap, rightMap);
			difference.entriesOnlyOnLeft().forEach((key, value) -> ref1.put(new JSONObject().put(key, value)));
			result.put(jsonfile1.getName(), ref1);
			difference.entriesOnlyOnRight().forEach((key, value) -> ref2.put(new JSONObject().put(key, value)));
			result.put(jsonfile2.getName(), ref2);
			difference.entriesDiffering().forEach((key, value) -> ref3.put(new JSONObject().put(key, value)));
			result.put("difference", ref3);
			curStep.setResponseData(result.toString(4));
			curStep.setIsApi(true);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action replaces the substring of a string with the given string and stores the resulting string to runtime parameter", action_displayname = "replacestring", actionname = "replacestring", paraname_equals_curobj = "false", paramnames = { "srcStr","replWith","replTo","runTimeParam" }, paramtypes = { "String","String","String","String" })
	public boolean replacestring(String srcStr, String replWith, String replTo, String runTimeParam) {
		try {
			srcStr = srcStr.replace(replWith, replTo);
			storeruntime(runTimeParam, srcStr);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action finds the value of the given json key and stores it to runtime parameter", action_displayname = "storeintojson", actionname = "storeintojson", paraname_equals_curobj = "false", paramnames = { "Json","key","runtime" }, paramtypes = { "String","String","String" })
	public boolean storeintojson(String Json, String key, String runtime) {
		try {

//			Json={"path":"/deliverit/openapi/order/update/default/FordQ","message":"{"reasons":[{"description":"orders[0].orderNbr must not be null","reasonCode":"1401"}]}","error":"BAD_REQUEST","timestamp":"2021-11-22T06:50:47.473+0000","status":400};
			String[] ab = Json.split(",");

//		Json.replace("]}\"","]}").replace("}","}\"").replace(":\"{\"",":{\"").replace("{", "\"{");
			for (int i = 0; i < ab.length; i++) {
				JSONObject objjj = new JSONObject(ab[i]);
			}
			JSONObject object = new JSONObject(Json);
			if (object.has(key)) {
				this.rpParam = new HashMap<String, String>();
				this.rpParam.put(key, object.get(key).toString());
				logger.info("StoreFromJson key found:{} with value:{}", key, object.get(key).toString());
				return storeruntime(runtime, object.get(key).toString());
			}

			else {
				logger.info("StoreFromJson key not found:{}", key);
				logerOBJ.customlogg(userlogger, "Step Result Failed: StoreFromJson key not found.", null, "", "error");
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Unable to Parse String to JSON");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to Parse String to JSON: ", null, e.toString(),
					"error");
			return false;
		}
	}


	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the value of the given json key and stores it to runtime parameter", action_displayname = "storefromjson", actionname = "storefromjson", paraname_equals_curobj = "false", paramnames = { "Json","key","runtime" }, paramtypes = { "String","String","String" })
	public boolean storefromjson(String Json, String key, String runtime) {
		try {
			Json.replace("\\", "");
//			Json.replace("\n", "\\n");

			Map<String, String> jsonStringify = flattenJSON(StringEscapeUtils.unescapeJava(Json));

			String dataFromJsonResp = jsonStringify.get(key);
			if (dataFromJsonResp != null) {
				this.rpParam = new HashMap<String, String>();
				this.rpParam.put(key, dataFromJsonResp);
				logger.info("StoreFromJson key found:{} with value:{}", key, dataFromJsonResp);
				return storeruntime(runtime, dataFromJsonResp);
			} else {
				logger.info("StoreFromJson key not found:{}", key);
				logerOBJ.customlogg(userlogger, "Step Result Failed: StoreFromJson key not found.", null, "", "error");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Unable to Parse String to JSON");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to Parse String to JSON: ", null, e.toString(),
					"error");
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action validates whether the given key has the given expected value in a json", action_displayname = "validatefromjson", actionname = "validatefromjson", paraname_equals_curobj = "false", paramnames = { "Json","key","ExpectedValue" }, paramtypes = { "String","String","String" })
	public boolean validatefromjson(String Json, String key, String ExpectedValue) {
		try {
			if (Json.startsWith("\"["))
				Json = Json.substring(2, Json.length() - 1);
			else if (Json.startsWith("["))
				Json = Json.substring(1, Json.length() - 1);
			Map<String, String> jsonStringify = flattenJSON(StringEscapeUtils.unescapeJava(Json));
			String dataFromJsonResp = jsonStringify.get(key);
			if (dataFromJsonResp != null && dataFromJsonResp.trim().contains(ExpectedValue)) {
				logger.info("ValidateFromJson key found :{} with value:{}", key, dataFromJsonResp);
				return true;
			} else {
				logger.info("ValidateFromJson key not  found :{}", key);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Unable to Parse String to JSON :{}", e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to Parse String to JSON : ", null, e.toString(),
					"error");

			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "restapi", actionname = "restapi", paraname_equals_curobj = "false", paramnames = {"baseURL", "Method Type","Header","Parameters","Body","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime" }, paramtypes = { "String","String","String","String","String","String","String","String" })
	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, responseBodyRuntime,
				responseCodeRuntime, responseMsgRuntime);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "restapi", actionname = "restapi", paraname_equals_curobj = "false", paramnames = {"baseURL","Method","Header","Parameters","Body","userName","password","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String","String","String" })
	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}
	
	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "restapiresponse", actionname = "restapiresponse", paraname_equals_curobj = "false", paramnames = {"baseURL","Method","Header","Parameters","Body","userName","password","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String","String","String" })
	public boolean restapiresponse(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapiresponse(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "restapi", actionname = "restapi", paraname_equals_curobj = "false", paramnames = {"baseURL","Method", "Header","Parameters","formdatatype","formdata","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String","String" })
	public boolean restapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String formdatatype,
			String formdata, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapiforformdata(m_baseURI, m_Method, m_Header, m_Parameters, formdatatype, formdata,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}

	
	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "msgetaccesstoken", actionname = "msgetaccesstoken", paraname_equals_curobj = "false", paramnames = {"baseURI","Method","Header","Parameters","Body","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String" })
	public boolean msgetaccesstoken(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapiforformdata(m_baseURI, m_Method, m_Header, m_Parameters, "text", m_Body,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}

	@Sqabind(object_template="sqa_record",action_description = "This is restapi action ", action_displayname = "msgraphreademail", actionname = "msgraphreademail", paraname_equals_curobj = "false", paramnames = {"baseURI","Method","Header","Parameters","Body","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String" })
	public boolean msgraphreademail(String m_baseURI, String m_Method, String m_Header, String m_Parameters,
			String m_Body, String responseBodyRuntime, String responseCodeRuntime, String responseMsgRuntime) {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.restapiforformdata(m_baseURI, m_Method, m_Header, m_Parameters, "text", m_Body,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "generateexcelid", action_displayname = "generateexcelid", actionname = "generateexcelid", paraname_equals_curobj = "false", paramnames = { "filepath","columnname" }, paramtypes = { "String","String" })
	public boolean generateexcelid(String filepath, String columnname) {
		Random rand = new Random();
		int low = 3000;
		int high = 4000;
		int empid_1 = rand.nextInt(high - low) + low;
		int empid_2 = rand.nextInt(high - low) + low;
		int empid_3 = rand.nextInt(high - low) + low;
		int empid_4 = rand.nextInt(high - low) + low;

		FileInputStream file = null;

		XSSFWorkbook workbook = null;
		try {
			file = new FileInputStream(new File(filepath));
			workbook = new XSSFWorkbook(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFRow header = sheet.getRow(0);
		int noOfColumns = sheet.getRow(0).getLastCellNum();
		int columnnumber = 0;
		for (int i = 0; i < noOfColumns; i++) {
			XSSFCell header_cell = header.getCell(i);
			if (header_cell.getStringCellValue().trim().equalsIgnoreCase(columnname)) {
				columnnumber = i;
				break;
			}
		}
		Cell cell = null;
		cell = sheet.getRow(1).getCell(columnnumber);
		cell.setCellValue(empid_1);
		cell = sheet.getRow(2).getCell(columnnumber);
		cell.setCellValue(empid_2);
		cell = sheet.getRow(3).getCell(columnnumber);
		cell.setCellValue(empid_3);
		cell = sheet.getRow(4).getCell(columnnumber);
		cell.setCellValue(empid_4);
		try {
			file.close();
			FileOutputStream outFile = new FileOutputStream(new File(filepath));
			workbook.write(outFile);
			outFile.close();
			storeruntime("emp1id", Integer.toString(empid_1));
			storeruntime("emp2id", Integer.toString(empid_2));
			storeruntime("emp3id", Integer.toString(empid_3));
			storeruntime("emp4id", Integer.toString(empid_4));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "generateexcelstring", action_displayname = "generateexcelstring", actionname = "generateexcelstring", paraname_equals_curobj = "false", paramnames = { "filepath","columnname" }, paramtypes = { "String","String" })
	public boolean generateexcelstring(String filepath, String columnname) {
		Faker f = new Faker();
		String emp1name = f.name().firstName();
		String emp2name = f.name().firstName();
		String emp3name = f.name().firstName();
		String emp4name = f.name().firstName();
		FileInputStream file = null;
		XSSFWorkbook workbook = null;
		try {
			file = new FileInputStream(new File(filepath));
			workbook = new XSSFWorkbook(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFRow header = sheet.getRow(0);
		int noOfColumns = sheet.getRow(0).getLastCellNum();
		int columnnumber = 0;
		for (int i = 0; i < noOfColumns; i++) {
			XSSFCell header_cell = header.getCell(i);
			if (header_cell.getStringCellValue().trim().equalsIgnoreCase(columnname)) {
				columnnumber = i;
				break;
			}
		}
		Cell cell = null;
		cell = sheet.getRow(1).getCell(columnnumber);
		cell.setCellValue(emp1name);
		cell = sheet.getRow(2).getCell(columnnumber);
		cell.setCellValue(emp2name);
		cell = sheet.getRow(3).getCell(columnnumber);

		cell.setCellValue(emp3name);
		cell = sheet.getRow(4).getCell(columnnumber);

		cell.setCellValue(emp4name);
		try {
			file.close();
			FileOutputStream outFile = new FileOutputStream(new File(filepath));
			workbook.write(outFile);
			outFile.close();
			storeruntime("emp1name", emp1name);
			storeruntime("emp2name", emp2name);
			storeruntime("emp3name", emp3name);
			storeruntime("emp4name", emp4name);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}
	
	@Sqabind(object_template="sqa_record",action_description = "This action validates whether the given soap value contains the given string", action_displayname = "validatefromsoap", actionname = "validatefromsoap", paraname_equals_curobj = "false", paramnames = { "Json","ExpectedValue" }, paramtypes = { "String","String" })
	public boolean validatefromsoap(String Json, String ExpectedValue) {
		if (ExpectedValue.trim().contains(Json)) {
			return true;
		} else {
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "existsusingsikuli", action_displayname = "existsusingsikuli", actionname = "existsusingsikuli", paraname_equals_curobj = "false", paramnames = { "imgpath" }, paramtypes = { "String" })
	public boolean existsusingsikuli(String imgpath) {
		Screen s = new Screen();
		try {
			Pattern img = new Pattern(imgpath);
			s.exists(img);
			// s.exists(img);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
		return true;
	}

	@Sqabind(object_template="sqa_record",action_description = "", action_displayname = "clickusingsikuli", actionname = "clickusingsikuli", paraname_equals_curobj = "false", paramnames = { "param" }, paramtypes = { "String" })
	public boolean clickusingsikuli(String param) {
		Screen s = new Screen();
		try {
			Pattern img = new Pattern(param);
			s.wait(img, 150);
			s.find(img);
			s.click(img);
		} catch (FindFailed e) {
			return false;
		}
		return true;
	}

	@Sqabind(object_template="sqa_record",action_description = "entertextusingsikuli", action_displayname = "entertextusingsikuli", actionname = "entertextusingsikuli", paraname_equals_curobj = "false", paramnames = { "imgpath","param" }, paramtypes = { "String","String" })
	public boolean entertextusingsikuli(String imgpath, String param) {
		Screen s = new Screen();
		try {
			Pattern img = new Pattern(imgpath);
			s.wait(img, 100);
			// Thread.sleep(1000);
			s.find(img);
			s.click(img);
			s.type(param);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
		return true;
	}

	public boolean storetdasmap(Map<String, String> tdMap) {
		try {
			GlobalDetail.curStepTd.put(Thread.currentThread().getName(), tdMap);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	public HashMap<String, String> getRpParam() {
		return rpParam;
	}

	public JSONObject getruntimevariableJSon(String identifiername) {
		JSONObject res;
		try {
			if (curExecution == null || header.isEmpty()) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			Base64 base64Url = new Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
			res = InitializeDependence.serverCall.getruntimeParameter(curExecution,
					identifiername + "_" + jsonObject.get("createdBy").toString(), header);
			if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
				logger.info("Executed  getRuntime :{}", res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("value"));
				return res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0);
			} else
				logger.info("Failed to  getRuntime ");
			return null;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: Failed to getRuntime: ", null, e.toString(), "error");
			logger.info("Failed to  getRuntime ");
			return null;
		}
	}

	public boolean deleteruntime(String param) {
		try {
			if (curExecution == null || header.isEmpty()) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			
			JSONObject req = getruntimevariableJSon(param);
			JSONObject res = InitializeDependence.serverCall.deleteruntime(curExecution, req, header);
			if (res.getBoolean("success"))
				return true;
			else
				return true;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return true;
		}
	}

	public String readFile(String FilePath) {
		String everything = "";
		String str;

		try {
			File file = new File(FilePath);
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((str = br.readLine()) != null) {
				everything = everything + str;
			}
			// everything = new String(Files.readAllLines(Paths.get(FilePath)));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;
			// e.printStackTrace();
		}
		return everything;

	}

	@Sqabind(object_template="sqa_record",action_description = "This action reads the file from the given filepath and stores it to runtime parameter", action_displayname = "readfile", actionname = "readfile", paraname_equals_curobj = "false", paramnames = { "FilePath","runtimeparam" }, paramtypes = { "String","String" })
	public boolean readfile(String FilePath, String runtimeparam) {

		String everything = "";

		String str;

		try {

			File file = new File(FilePath);

			BufferedReader br = new BufferedReader(new FileReader(file));

			while ((str = br.readLine()) != null) {

				everything = everything + str;

			}

			return storeruntime(runtimeparam, everything);

			// everything = new String(Files.readAllLines(Paths.get(FilePath)));

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

			// e.printStackTrace();

		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action writes the given data to the specified file", action_displayname = "writetofile", actionname = "writetofile", paraname_equals_curobj = "false", paramnames = { "filepath","data" }, paramtypes = { "String","String" })
	public boolean writetofile(String filepath, String data) {
		try {
			FileOutputStream outputStream = new FileOutputStream(filepath);
			byte[] strToBytes = data.getBytes();
			outputStream.write(strToBytes);
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public Recordset readfromexcel(String excelfilepath, String sheetname, String rowname, String rowval) {
		try {
			HashMap<String, String> exceldata = new HashMap<String, String>();
			Fillo fillo = new Fillo();
			com.codoid.products.fillo.Connection connection = fillo.getConnection(excelfilepath);
			String strQuery = "Select * from " + sheetname + " where " + rowname + "='" + rowval + "'";
			Recordset recordset = connection.executeQuery(strQuery);

			return recordset;

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return null;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "updatejsonusingexcel", action_displayname = "updatejsonusingexcel", actionname = "updatejsonusingexcel", paraname_equals_curobj = "false", paramnames = { "excelfilepath","jsonfilepath","sheetname","rowname","rowval","runtimparam" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean updatejsonusingexcel(String excelfilepath, String jsonfilepath, String sheetname, String rowname,
			String rowval, String runtimparam) {
		try {
			Recordset recordset = readfromexcel(excelfilepath, sheetname, rowname, rowval);
			String json = readFile(jsonfilepath);
			if (json == null) {
				json = jsonfilepath;
			}
			while (recordset.next()) {
				ArrayList<String> dataColl = recordset.getFieldNames();
				Iterator<String> dataIterator = dataColl.iterator();
				int flag = 0;
				while (dataIterator.hasNext()) {
					if (flag == 0) {
						flag++;
						dataIterator.next();
						continue;
					}
					for (int i = 0; i <= dataColl.size() - 2; i++) {
						String data = dataIterator.next();
						String dataVal = recordset.getField(data);
						System.out.println(dataVal);
						String arrdata[] = dataVal.split(",");
						for (int j = 0; j < arrdata.length; j++) {
							json = json.replaceAll(data + "\\[" + j + "\\]", arrdata[j]);
						}

					}

				}
//				
			}
			return storeruntime(runtimparam, json);

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}

	}

	@Override
	public String getruntimevalueifexist(String identifiername) {
		JSONObject res;
		try {
			if (curExecution == null || header.isEmpty()) {
				curExecution = new Setupexec();
				curExecution.setAuthKey(GlobalDetail.refdetails.get("authkey"));
				curExecution.setCustomerID(Integer.parseInt(GlobalDetail.refdetails.get("customerId")));
				curExecution.setProjectID(Integer.parseInt(GlobalDetail.refdetails.get("projectId")));
				curExecution.setServerIp(GlobalDetail.refdetails.get("executionIp"));
				header.put("content-type", "application/json");
				header.put("authorization", curExecution.getAuthKey());
			}
			String token = curExecution.getAuthKey();
			String[] split = token.split("\\.");
			String base64EncodedBody = split[1];
			Base64 base64Url = new Base64(true);
			JSONObject jsonObject = null;
			jsonObject = new JSONObject(new String(base64Url.decode(base64EncodedBody)));
			res = InitializeDependence.serverCall.getruntimeParameter(curExecution,
					identifiername + "_" + jsonObject.get("createdBy").toString(), header);
			if (res.getJSONObject(UtilEnum.DATA.value()).getInt("totalCount") != 0) {
				logger.info("Executed  getRuntime :{}", res.getJSONObject(UtilEnum.DATA.value())
						.getJSONArray(UtilEnum.DATA.value()).getJSONObject(0).getString("value"));
				return res.getJSONObject(UtilEnum.DATA.value()).getJSONArray(UtilEnum.DATA.value()).getJSONObject(0)
						.getString("value");
			} else {
				logger.info("Runtime variable does not exist ");
				logerOBJ.customlogg(userlogger, "Step Result Failed: Runtime variable does not exist.", null, "",
						"error");
			}
			return null;
		} catch (Exception e) {
			logger.info("Failed to  getRuntime ");
			logerOBJ.customlogg(userlogger, "Step Result Failed: Failed to getRuntime: ", null, e.toString(), "error");
			return null;
		}
	}

	@Override
	public boolean verifyadditionofvalues(String... str) {
		String[] numbStr = new String[str.length - 1];
		int sum = 0;
		for (int i = 0; i < str.length - 1; i++)
			numbStr[i] = str[i];

		for (String temp : numbStr) {
			sum += Integer.parseInt(temp);
		}
		String comparevalue = str[str.length - 1];
		if (String.valueOf(sum).equals(comparevalue))
			return true;
		else
			return false;

	}

	@Sqabind(object_template="sqa_record",action_description = "This action generates a custom date and time and stores it to runtime parameter", action_displayname = "generatecustomdateandtime", actionname = "generatecustomdateandtime", paraname_equals_curobj = "false", paramnames = { "UDate","DateFormat","Time","Meri","runtimeparam" }, paramtypes = { "String","String","String","String","String" })
	public boolean generatecustomdateandtime(String UDate, String DateFormat, String Time, String Meri,
			String runtimeparam) {
		try {
			String[] dt, time;
			String finaldt = null;
			String fntime = null;
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern(DateFormat);

			if (UDate.contains("current date") && Time.contains("current time")) {
				Date actdate = new Date();
				if (UDate.contains("+")) {
					dt = UDate.split("\\+");
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.plusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else if (UDate.contains("-")) {
					dt = UDate.split("\\-");
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.minusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else {
					DateTime dtOrg = new DateTime(actdate);

					finaldt = dtOrg.toString(date).split(" ")[0];

				}

				if (Time.contains("+")) {
					time = Time.split("\\+");
					DateTime tmOrg = new DateTime(actdate);
					DateTime tmPlusOne = tmOrg.plusHours(Integer.parseInt(time[1]));
					fntime = tmPlusOne.toString(date).split(" ")[1];

				} else if (Time.contains("-")) {
					time = Time.split("\\-");
					DateTime tmOrg = new DateTime(actdate);
					DateTime tmPlusOne = tmOrg.minusHours(Integer.parseInt(time[1]));
					fntime = tmPlusOne.toString(date).split(" ")[1];

				} else {
					DateTime tmOrg = new DateTime(actdate);

					fntime = tmOrg.toString(date).split(" ")[1];

				}
				storeruntime(runtimeparam, finaldt + " " + fntime + " " + Meri);
			} else {

				if (UDate.contains("+")) {
					dt = UDate.split("\\+");
					Date actdate = new Date(dt[0]);
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.plusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else if (UDate.contains("-")) {
					dt = UDate.split("\\-");
					Date actdate = new Date(dt[0]);
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.minusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				}

				if (Time.contains("+")) {
					time = Time.split("\\+");
					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
							.plusHours(Integer.parseInt(time[1]));

					fntime = tm.toString(date).split(" ")[1];

				} else if (Time.contains("-")) {
					time = Time.split("\\-");
					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
							.minusHours(Integer.parseInt(time[1]));
					fntime = tm.toString(date).split(" ")[1];

				}
				storeruntime(runtimeparam, finaldt + " " + fntime + " " + Meri);
			}
			return true;

		} catch (Exception e) {
			logger.info(e.getMessage());
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action gets isodateandtime and stores it to runtime parameter ", action_displayname = "getisodateandtime", actionname = "getisodateandtime", paraname_equals_curobj = "false", paramnames = { "UDate","Time","runtimeparam" }, paramtypes = { "String","String","String" })
	public boolean getisodateandtime(String UDate, String Time, String runtimeparam) {

		try {
			String[] dt, time;
			String finaldt = null;
			String fntime = null;
			if (UDate.contains("ISO")) {
				if (UDate.contains("+")) {
					dt = UDate.split("\\+");
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.plusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					if (Time.contains("+")) {
						time = Time.split("\\+");
						dtPlusOne = dtPlusOne.plusHours(Integer.parseInt(time[1]));
						finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					} else if (Time.contains("-")) {
						time = Time.split("\\-");
						dtPlusOne = dtPlusOne.minusHours(Integer.parseInt(time[1]));
						finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					}
				} else if (UDate.contains("-")) {
					dt = UDate.split("\\-");
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.minusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					if (Time.contains("+")) {
						time = Time.split("\\+");
						dtPlusOne = dtPlusOne.plusHours(Integer.parseInt(time[1]));
						finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					} else if (Time.contains("-")) {
						time = Time.split("\\-");
						dtPlusOne = dtPlusOne.minusHours(Integer.parseInt(time[1]));
						finaldt = dtPlusOne.toString().split("\\.")[0].toString();
					}
				} else {
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					finaldt = dtOrg.toString().split("\\.")[0].toString();

				}

			}
			return storeruntime(runtimeparam, finaldt);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the date and stores it to runtime parameter", action_displayname = "getdate", actionname = "getdate", paraname_equals_curobj = "false", paramnames = { "UDate","DateFormat","runtimeparam" }, paramtypes = { "String","String","String" })
	public boolean getdate(String UDate, String DateFormat, String runtimeparam) {
		try {
			String[] dt;
			String finaldt = null;
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern(DateFormat);

			if (UDate.contains("today")) {
				if (UDate.contains("+")) {
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					dt = UDate.split("\\+");
					DateTime dtPlusOne = dtOrg.plusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else if (UDate.contains("-")) {
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					dt = UDate.split("\\-");
					DateTime dtPlusOne = dtOrg.minusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else {
					Date actdate = new Date();
					DateTime dtOrg = new DateTime(actdate);
					finaldt = dtOrg.toString(date);
				}
			} else {
				if (UDate.contains("+")) {
					dt = UDate.split("\\+");
					Date actdate = new Date(dt[0]);
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.plusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				} else if (UDate.contains("-")) {
					dt = UDate.split("\\-");
					Date actdate = new Date(dt[0]);
					DateTime dtOrg = new DateTime(actdate);
					DateTime dtPlusOne = dtOrg.minusDays(Integer.parseInt(dt[1]));
					finaldt = dtPlusOne.toString(date).split(" ")[0];

				}

				else if (DateFormat.equals("dd mm yyyy") || DateFormat.equals("dd mmm yyyy")
						|| DateFormat.equals("DD MM YYYY") || DateFormat.equals("DD MMM YYYY")) {

					String d = UDate.substring(0, 2);
					String m = null;
					m = UDate.substring(3, 5);
					String y = UDate.substring(6, 10);
					String mname = null;
					switch (m) {
					case "01":
						mname = "Jan";
						break;

					case "02":
						mname = "Feb";

					case "03":
						mname = "Mar";
						break;

					case "04":
						mname = "Apr";
						break;

					case "05":
						mname = "May";
						break;

					case "06":
						mname = "Jun";
						break;

					case "07":
						mname = "Jul";
						break;

					case "08":
						mname = "Aug";
						break;

					case "09":
						mname = "Sep";
						break;

					case "10":
						mname = "Oct";
						break;

					case "11":
						mname = "Nov";
						break;

					case "12":
						mname = "Dec";
						break;
					default:
						System.out.println("Not Found");

					}

					String space = " ";
					finaldt = d + space + mname + space + y;

				} else {
					Date actdate = new Date(UDate);
					DateTime dtOrg = new DateTime(actdate);
					finaldt = dtOrg.toString(date);
				}

			}
			return storeruntime(runtimeparam, finaldt);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

//	@Override
//	public boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
//		try {
//
//			System.out.println("runtimeparam before executing :" + runtimeparam);
//			String[] time;
//			String fntime = null;
//			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern(TimeFormat);
//			Date actdate = new Date();
//			DateTime tmOrg = new DateTime(actdate);
//
//			if (Time.contains("now")) {
//				if (Time.contains("+")) {
//					time = Time.split("\\+");
//					DateTime tmPlusOne = tmOrg.plusHours(Integer.parseInt(time[1]));
//					fntime = tmPlusOne.toString(date);
//				} else if (Time.contains("-")) {
//					time = Time.split("\\-");
//					DateTime tmPlusOne = tmOrg.minusHours(Integer.parseInt(time[1]));
//					fntime = tmPlusOne.toString(date);
//				} else {
//					fntime = tmOrg.toString(date);
//				}
//			} else {
//				if (Time.contains("+")) {
//					time = Time.split("\\+");
//					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
//							.plusHours(Integer.parseInt(time[1]));
//					fntime = tm.toString(date);
//				} else if (Time.contains("-")) {
//					time = Time.split("\\-");
//					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
//							.minusHours(Integer.parseInt(time[1]));
//					fntime = tm.toString(date);
//				} else {
//					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(Time);
//					fntime = tm.toString(date);
//				}
//
//			}
//			System.out.println("runtimeparam from gettime :" + runtimeparam + " " + fntime);
//			return storeruntime(runtimeparam, fntime);
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
//			e.printStackTrace();
//			return false;
//		}
//	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the time and stores it to runtime", action_displayname = "gettime", actionname = "gettime", paraname_equals_curobj = "false", paramnames = { "Time","TimeFormat","Meridiem","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean gettime(String Time, String TimeFormat, String Meridiem, String runtimeparam) {
		try {

			System.out.println("runtimeparam before executing :" + runtimeparam);
			String[] time;
			String fntime = null;
			org.joda.time.format.DateTimeFormatter date = DateTimeFormat.forPattern(TimeFormat);
			Date actdate = new Date();
			DateTime tmOrg = new DateTime(actdate);

			if (Time.contains("now")) {
				if (Time.contains("+")) {
					time = Time.split("\\+");
					DateTime tmPlusOne = tmOrg.plusHours(Integer.parseInt(time[1]));
					fntime = tmPlusOne.toString(date);
				} else if (Time.contains("-")) {
					time = Time.split("\\-");
					DateTime tmPlusOne = tmOrg.minusHours(Integer.parseInt(time[1]));
					fntime = tmPlusOne.toString(date);
				} else {
					fntime = tmOrg.toString(date);
				}
			} else {
				if (Time.contains("+")) {
					time = Time.split("\\+");
					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
							.plusHours(Integer.parseInt(time[1]));
					fntime = tm.toString(date);
				} else if (Time.contains("-")) {
					time = Time.split("\\-");
					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(time[0])
							.minusHours(Integer.parseInt(time[1]));
					fntime = tm.toString(date);
				} else {
					org.joda.time.LocalTime tm = new org.joda.time.LocalTime(Time);
					fntime = tm.toString(date);
				}

			}
			System.out.println("runtimeparam from gettime :" + runtimeparam + " " + fntime);
			return storeruntime(runtimeparam, fntime);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the given file exists or not", action_displayname = "checkiffileexists", actionname = "checkiffileexists", paraname_equals_curobj = "false", paramnames = { "filePathString" }, paramtypes = { "String" })
	public boolean checkiffileexists(String filePathString) throws Exception {

		try {

			File f = new File(filePathString);
			if (f.isFile())
				return true;
			else
				return false;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
//	public String convertxmltojson(String xmlfile) {
//		try {
//			String TEST_XML_STRING = readFile(xmlfile);
//			JSONObject xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);
//			String jsonPrettyPrintString = xmlJSONObj.toString();
//			return jsonPrettyPrintString;
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
//			e.printStackTrace();
//			return null;
//		}
//
//	}

	@Sqabind(object_template="sqa_record",action_description = "This action converts the given xml to json", action_displayname = "convertxmltojson", actionname = "convertxmltojson", paraname_equals_curobj = "false", paramnames = { "xmlfile" }, paramtypes = { "String" })
	public String convertxmltojson(String xmlfile) {
		try {
			File ff = new File(xmlfile);
			if (ff.isFile()) {
				String TEST_XML_STRING = readFile(xmlfile);
				JSONObject xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);
				String jsonPrettyPrintString = xmlJSONObj.toString();

				return jsonPrettyPrintString;
			} else {
				JSONObject xmlJSONObj = XML.toJSONObject(xmlfile);
				String jsonPrettyPrintString = xmlJSONObj.toString();

				return jsonPrettyPrintString;
			}

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return null;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action checks whether the given key has the expected value in the xml file", action_displayname = "validatefromxml", actionname = "validatefromxml", paraname_equals_curobj = "false", paramnames = { "xmlfile","key","expectedvalue" }, paramtypes = { "String","String","String" })
	public boolean validatefromxml(String xmlfile, String key, String expectedvalue) {
		try {
			String json = convertxmltojson(xmlfile);
			return validatefromjson(json, key, expectedvalue);

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

//	@Override
//	public boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
//		try {
//			String json = convertxmltojson(xmlfile);
//			return storejsonkey(json, key, runtimeparam);
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
//			e.printStackTrace();
//			return false;
//		}
//	}

	@Sqabind(object_template="sqa_record",action_description = "This action finds the xml value of a given key and stores it to runtime parameter", action_displayname = "storexmlvalue", actionname = "storexmlvalue", paraname_equals_curobj = "false", paramnames = { "xmlfile","key","runtimeparam" }, paramtypes = { "String","String","String" })
	public boolean storexmlvalue(String xmlfile, String key, String runtimeparam) {
		try {
			xmlfile = xmlfile.replaceAll("\\n", "");
			String json = convertxmltojson(xmlfile);
			return storejsonkey(json, key, runtimeparam);
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the data of a cell in excel sheet and stores it to runtime parameter", action_displayname = "excelgetcelldata", actionname = "excelgetcelldata", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","cellreference","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean excelgetcelldata(String excelfilepath, String sheetname, String cellreference, String runtimeparam) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			try {

				sheet = workbook.getSheet(sheetname);

				Cell cellvalue = null;

				CellReference ref = new CellReference(cellreference);

				Row row = sheet.getRow(ref.getRow());

				if (row != null) {

					cellvalue = row.getCell(ref.getCol());

				}

				return storeruntime(runtimeparam, cellvalue.toString());

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override

	@Sqabind(object_template="sqa_record",action_description = "This action gets the data of a column using column reference in excel sheet and stores it to runtime parameter", action_displayname = "excelgetcoldatausingcolref", actionname = "excelgetcoldatausingcolref", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","colreference","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean excelgetcoldatausingcolref(String excelfilepath, String sheetname, String colreference,

			String runtimeparam) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			try {

				sheet = workbook.getSheet(sheetname);

				int colindex = CellReference.convertColStringToIndex(colreference);

				int starRow = sheet.getFirstRowNum();

				int endRow = sheet.getLastRowNum();

				JSONObject jsn = new JSONObject();

				DataFormatter formatter = new DataFormatter();

				JSONArray CellValue = new JSONArray();

				Iterator<Row> cellIterator = sheet.iterator();

				while (cellIterator.hasNext()) {

					for (int i = starRow; i <= endRow; i++) {

						Row row = sheet.getRow(i);

						Cell column = row.getCell(colindex);

						// CellValue.put(column.getStringCellValue().trim());

						CellValue.put(formatter.formatCellValue(column));

					}

					jsn.put(colreference, CellValue);

					break;

				}

				return storeruntime(runtimeparam, jsn.toString());

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the data of a column using header in excel sheet and stores it to runtime parameter", action_displayname = "excelgetcoldatausinghdr", actionname = "excelgetcoldatausinghdr", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","hdrname","runtimparam" }, paramtypes = { "String","String","String","String" })
	public boolean excelgetcoldatausinghdr(String excelfilepath, String sheetname, String hdrname, String runtimparam) {

		try {

			Sheet sheet = null;

			Workbook workbook = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			int colnum = -1;

			try {

				sheet = workbook.getSheet(sheetname);

				Row row = sheet.getRow(0);

				for (int i = 0; i < row.getLastCellNum(); i++) {

					if (row.getCell(i).getStringCellValue().equals(hdrname.trim())) {

						colnum = i;

						break;

					}

				}

				int starRow = sheet.getFirstRowNum();

				int endRow = sheet.getLastRowNum();

				JSONObject jsn = new JSONObject();

				JSONArray CellValue = new JSONArray();

				DataFormatter formatter = new DataFormatter();

				Iterator<Row> cellIterator = sheet.iterator();

				while (cellIterator.hasNext()) {

					for (int i = starRow + 1; i <= endRow; i++) {

						row = sheet.getRow(i);

						Cell column = row.getCell(colnum);

						CellValue.put(formatter.formatCellValue(column));

					}

					jsn.put(hdrname, CellValue);

					break;

				}

				return storeruntime(runtimparam, jsn.toString());

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the data of a sheet using header in excel sheet and stores it to runtime parameter", action_displayname = "excelgetsheetdatausinghdr", actionname = "excelgetsheetdatausinghdr", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","runtimeparam" }, paramtypes = { "String","String","String" })
	public boolean excelgetsheetdatausinghdr(String excelfilepath, String sheetname, String runtimeparam) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			try {

				sheet = workbook.getSheet(sheetname);

				Iterator<Row> sheetIterator = sheet.iterator();

				ArrayList<String> columnNames = new ArrayList<String>();

				JsonArray sheetArray = new JsonArray();

				DataFormatter formatter = new DataFormatter();

				// Row row = sheet.getRow(0);

				while (sheetIterator.hasNext()) {

					Row currentRow = sheetIterator.next();

					JsonObject jsonObject = new JsonObject();

					if (currentRow.getRowNum() != 0) {

						for (int j = 0; j < columnNames.size(); j++) {

							if (currentRow.getCell(j) != null) {

								if (currentRow.getCell(j).getCellTypeEnum() == CellType.STRING) {

									jsonObject.addProperty(columnNames.get(j),

											currentRow.getCell(j).getStringCellValue());

								} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.NUMERIC) {

									jsonObject.addProperty(columnNames.get(j),

											currentRow.getCell(j).getNumericCellValue());

								} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BOOLEAN) {

									jsonObject.addProperty(columnNames.get(j),

											currentRow.getCell(j).getBooleanCellValue());

								} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BLANK) {

									jsonObject.addProperty(columnNames.get(j), "");

								}

							} else {

								jsonObject.addProperty(columnNames.get(j), "");

							}

						}

						sheetArray.add(jsonObject);

					} else {

						// store column names

						for (int k = 0; k < currentRow.getPhysicalNumberOfCells(); k++) {

							columnNames.add(formatter.formatCellValue(currentRow.getCell(k)));

						}

					}

				}

				return storeruntime(runtimeparam, sheetArray.toString());

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the data of a sheet in excel and stores it to runtime parameter", action_displayname = "excelgetsheetdata", actionname = "excelgetsheetdata", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","runtimeparam" }, paramtypes = { "String","String","String" })
	public boolean excelgetsheetdata(String excelfilepath, String sheetname, String runtimeparam) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			try {

				sheet = workbook.getSheet(sheetname);

				Iterator<Row> sheetIterator = sheet.iterator();

				JsonObject sheetArray = new JsonObject();

				// Row row = sheet.getRow(0);

				int index = 0;

				while (sheetIterator.hasNext()) {

					JsonArray sheetrow = new JsonArray();

					Row currentRow = sheetIterator.next();

					for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++) {

						if (currentRow.getCell(j) != null) {

							if (currentRow.getCell(j).getCellTypeEnum() == CellType.STRING) {

								sheetrow.add(currentRow.getCell(j).getStringCellValue());

							} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.NUMERIC) {

								sheetrow.add(currentRow.getCell(j).getNumericCellValue());

							} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BOOLEAN) {

								sheetrow.add(currentRow.getCell(j).getBooleanCellValue());

							} else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BLANK) {

								sheetrow.add("");

							}

						} else {

							sheetrow.add("");

						}

					}

					sheetArray.add("Row " + Integer.toString(index), sheetrow);

					index++;

				}

				return storeruntime(runtimeparam, sheetArray.toString());

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action replaces the column value using column reference in excel", action_displayname = "excelreplacevalueincolusingcolref", actionname = "excelreplacevalueincolusingcolref", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","colreference","replacevalue" }, paramtypes = { "String","String","String","String" })
	public boolean excelreplacevalueincolusingcolref(String excelfilepath, String sheetname, String colreference,

			String replacevalue) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			try {

				sheet = workbook.getSheet(sheetname);

				int colindex = CellReference.convertColStringToIndex(colreference);

				int starRow = sheet.getFirstRowNum();

				int endRow = sheet.getLastRowNum();

				for (int i = starRow; i <= endRow; i++) {

					Row row = sheet.getRow(i);

					Cell column = row.getCell(colindex);

					column.setCellValue(replacevalue);

				}

				FileOperations.writetoexcel(excelfilepath, workbook);

				return true;

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action replaces the column value using header in excel", action_displayname = "excelreplacevalueincolusinghdr", actionname = "excelreplacevalueincolusinghdr", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","hdrname","replacevalue" }, paramtypes = { "String","String","String","String" })
	public boolean excelreplacevalueincolusinghdr(String excelfilepath, String sheetname, String hdrname,

			String replacevalue) {

		try {

			Workbook workbook = null;

			Sheet sheet = null;

			if (excelfilepath.contains(".xlsx")) {

				workbook = FileOperations.openexcel(excelfilepath);

			} else if (excelfilepath.contains(".xls")) {

				workbook = FileOperations.openexcel(excelfilepath);

			}

			sheet = workbook.getSheet(sheetname);

			int colnum = -1;

			DataFormatter formatter = new DataFormatter();

			try {

				sheet = workbook.getSheet(sheetname);

				Row row = sheet.getRow(0);

				for (int i = 0; i < row.getLastCellNum(); i++) {

					if (formatter.formatCellValue(row.getCell(i)).equals(hdrname.trim())) {

						colnum = i;

						break;

					}

				}

				int starRow = sheet.getFirstRowNum();

				int endRow = sheet.getLastRowNum();

				for (int i = starRow + 1; i <= endRow; i++) {

					row = sheet.getRow(i);

					Cell column = row.getCell(colnum);

					column.setCellValue(replacevalue);

				}

				FileOperations.writetoexcel(excelfilepath, workbook);

				return true;

			} catch (Exception e) {

				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

				e.printStackTrace();

				return false;

			}

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");

			e.printStackTrace();

			return false;

		}

	}

	@SuppressWarnings("unlikely-arg-type")
	@Sqabind(object_template="sqa_record",action_description = "This action updates the value of a cell in excel sheet", action_displayname = "excelupdatecellvalwrtrowval", actionname = "excelupdatecellvalwrtrowval", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","refhdrname","cellval","hdrname","replacevalue" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean excelupdatecellvalwrtrowval(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		try {
			Workbook workbook = null;
			Sheet sheet = null;
			if (excelfilepath.contains(".xlsx")) {
				workbook = FileOperations.openexcel(excelfilepath);
			} else if (excelfilepath.contains(".xls")) {
				workbook = FileOperations.openexcel(excelfilepath);
			}
			sheet = workbook.getSheet(sheetname);
			int colnum = -1;
			DataFormatter formatter = new DataFormatter();
			try {
				Row row = sheet.getRow(0);
				for (int i = 0; i < row.getLastCellNum(); i++) {
					if (formatter.formatCellValue(row.getCell(i)).equals(refhdrname.trim())) {
						colnum = i;
						break;
					}
				}
				int starRow = sheet.getFirstRowNum();
				int endRow = sheet.getLastRowNum();
				for (int i = starRow + 1; i <= endRow; i++) {
					row = sheet.getRow(i);
					System.out.println(row.getCell(colnum));
					if (row.getCell(colnum).toString().equals(cellval)) {
						int rownum = row.getRowNum();
						row = sheet.getRow(0);
						for (i = 0; i < row.getLastCellNum(); i++) {
							if (formatter.formatCellValue(row.getCell(i)).equals(hdrname.trim())) {
								colnum = i;
								break;
							}
						}
						row = sheet.getRow(rownum);
						// for (i = 0; i < row.getLastCellNum(); i++) {
						if (sheet.getRow(rownum) == null)
							sheet.createRow(rownum);
						try {
							System.out.println(row.getCell(colnum));
							if (row.getCell(colnum) == null) {
								row.createCell(colnum);
								row.getCell(colnum).setCellValue(replacevalue);
								break;
							} else {
								row.getCell(colnum).setCellValue(replacevalue);
								break;
							}
						} catch (Exception e) {
							row.getCell(colnum).setCellValue(" ");
							row.getCell(colnum).setCellValue(replacevalue);
							e.printStackTrace();
						}
					}
				}

				FileOperations.writetoexcel(excelfilepath, workbook);
				return true;
			} catch (Exception e) {
				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action updates the value of a cell in csv file", action_displayname = "csvupdatecelldata", actionname = "csvupdatecelldata", paraname_equals_curobj = "false", paramnames = { "fileToUpdate","row","col","replacevalue" }, paramtypes = { "String","String","String","String" })
	public boolean csvupdatecelldata(String fileToUpdate, String row, String col, String replacevalue)
			throws IOException {
		try {
			File inputFile = new File(fileToUpdate);
			CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
			List<String[]> csvBody = reader.readAll();
			csvBody.get(Integer.parseInt(row))[Integer.parseInt(col)] = replacevalue;
			reader.close();
			CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
			writer.writeAll(csvBody);
			writer.flush();
			writer.close();
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action gets the value of a cell of csv file and stores it to runtime parameter", action_displayname = "csvgetcelldata", actionname = "csvgetcelldata", paraname_equals_curobj = "false", paramnames = { "fileToUpdate","row","col","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean csvgetcelldata(String fileToUpdate, String row, String col, String runtimeparam) throws IOException {
		try {
			File inputFile = new File(fileToUpdate);
			CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
			List<String[]> csvBody = reader.readAll();
			String cellvalue = csvBody.get(Integer.parseInt(row))[Integer.parseInt(col)];
			reader.close();
			return storeruntime(runtimeparam, cellvalue);

		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	public boolean additionofvalues(String... param1) {
		try {
			String[] parma2 = new String[param1.length - 1];
			int finalvalue = 0;
			for (int i = 0; i < param1.length - 1; i++) {
				parma2[i] = param1[i];
				finalvalue = finalvalue + Integer.parseInt(parma2[i]);
			}
			return storeruntime(param1[param1.length - 1], Integer.toString(finalvalue));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}

	}

	public boolean subtractionofvalues(String... param1) {
		try {
			int finalvalue = Integer.parseInt(param1[0]);
			for (int i = 1; i < param1.length - 1; i++) {
				finalvalue = finalvalue - Integer.parseInt(param1[i]);
			}
			return storeruntime(param1[param1.length - 1], Integer.toString(finalvalue));
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action compares two given strings partially", action_displayname = "comparepartialstring", actionname = "comparepartialstring", paraname_equals_curobj = "false", paramnames = { "Basestring","valuetovalidate" }, paramtypes = { "String","String" })
	public boolean comparepartialstring(String Basestring, String valuetovalidate) {
		try {
			if (Basestring.toLowerCase().contains(valuetovalidate.toLowerCase())) {
				logger.info("Executed  validatePartialText actualValue :{} with expectedValue:{} ", Basestring,
						valuetovalidate);
			} else {
				logger.info("Failed to Execute  validatePartialText actualValue :{} with expectedValue:{} ", Basestring,
						valuetovalidate);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Sqabind(object_template="sqa_record",action_description = "This action compares two given strings partially", action_displayname = "comparepartialstring", actionname = "comparepartialstring", paraname_equals_curobj = "false", paramnames = { "Basestring","valuetovalidate","Ignorechar" }, paramtypes = { "String","String","String" })
	public boolean comparepartialstring(String Basestring, String valuetovalidate, String Ignorechar) {
		try {
			Basestring = Basestring.replaceAll(Ignorechar, "");
			if (Basestring.toLowerCase().contains(valuetovalidate.toLowerCase())) {
				logger.info("Executed  validatePartialText actualValue :{} with expectedValue:{} ", Basestring,
						valuetovalidate);
			} else {
				logger.info("Failed to Execute  validatePartialText actualValue :{} with expectedValue:{} ", Basestring,
						valuetovalidate);
			}
			return true;
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}

//	public boolean concatandstore(String... param1) {
//		try {
//			int len = param1.length;
//			if (param1[len - 2].equals("#SPACE#"))
//				param1[len - 2] = " ";
//			String[] param2 = new String[param1.length - 2];
//			String finalvalue = null;
//			for (int i = 0; i < param1.length - 2; i++) {
//				param2[i] = param1[i];
//			}
//			finalvalue = String.join(param1[len - 2], param2);
//			System.out.println("concatinate and store :" + param1[len - 1] + " " + finalvalue);
//			return storeruntime(param1[len - 1], finalvalue);
//		} catch (Exception e) {
//			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to combineruntime value: ", null, e.toString(),
//					"error");
//			logger.info("Unable to combineruntime value");
//			return false;
//		}
//	}

	public boolean concatandstore(String... param1) {

		try {

			int len = param1.length;

			String finalvalue = null;

			if (len == 3) {

				finalvalue = param1[len - 3] + param1[len - 2];

			} else {

				if (param1[len - 2].equals("#SPACE#"))

					param1[len - 2] = " ";

				String[] param2 = new String[param1.length - 2];

				for (int i = 0; i < param1.length - 2; i++) {

					param2[i] = param1[i];

				}

				finalvalue = String.join(param1[len - 2], param2);

				System.out.println("concatinate and store :" + param1[len - 1] + " " + finalvalue);

			}

			return storeruntime(param1[len - 1], finalvalue);

		} catch (Exception e) {

			logerOBJ.customlogg(userlogger, "Step Result Failed: Unable to combineruntime value: ", null, e.toString(),

					"error");

			logger.info("Unable to combineruntime value");

			return false;

		}

	}

	@SuppressWarnings("unlikely-arg-type")
	@Sqabind(object_template="sqa_record",action_description = "This action updates the call value in an excel sheet", action_displayname = "excelupdatecellvalwrtrowvalakpk", actionname = "excelupdatecellvalwrtrowvalakpk", paraname_equals_curobj = "false", paramnames = { "excelfilepath","sheetname","refhdrname","cellval","hdrname","replacevalue" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean excelupdatecellvalwrtrowvalakpk(String excelfilepath, String sheetname, String refhdrname,
			String cellval, String hdrname, String replacevalue) {
		try {
			Workbook workbook = null;
			Sheet sheet = null;
			if (excelfilepath.contains(".xlsx")) {
				workbook = FileOperations.openexcel(excelfilepath);
			} else if (excelfilepath.contains(".xls")) {
				workbook = FileOperations.openexcel(excelfilepath);
			}
			for (int i = 0; i <= workbook.getNumberOfSheets(); i++) {
				if (workbook.getSheetName(i).contains(sheetname))
					sheet = workbook.getSheetAt(i);
				break;
			}
			int colnum = -1;
			DataFormatter formatter = new DataFormatter();
			try {
//				for (int i = 0; i < row.getLastCellNum(); i++) {
//					if (formatter.formatCellValue(row.getCell(i)).equals(refhdrname.trim())) {
//						colnum = i;
//						break;
//					}
//				}
				int starRow = sheet.getFirstRowNum();
				int endRow = sheet.getLastRowNum();
				boolean valmatch = false;
				for (int i = starRow + 1; i <= endRow; i++) {
					Row row = sheet.getRow(0);
					for (int j = 0; j < row.getLastCellNum(); j++) {
						if (formatter.formatCellValue(row.getCell(j)).equals(refhdrname.trim())) {
							colnum = j;
							break;
						}
					}
					row = sheet.getRow(i);
					System.out.println(row.getCell(colnum));
					if (row.getCell(colnum).getCellTypeEnum() == CellType.NUMERIC) {
						int cellvalue = (int) row.getCell(colnum).getNumericCellValue();
						if (cellvalue == Integer.parseInt(cellval))
							valmatch = true;
						else
							valmatch = false;
					} else if (row.getCell(colnum).getCellTypeEnum() == CellType.STRING) {
						String celvalue = row.getCell(colnum).getStringCellValue();
						if (celvalue.toString().equals(cellval))
							valmatch = true;
						else
							valmatch = false;
					} else {
						valmatch = false;
					}

					if (valmatch) {
						int rownum = row.getRowNum();
						row = sheet.getRow(0);
						for (int k = 0; k < row.getLastCellNum(); k++) {
							if (formatter.formatCellValue(row.getCell(k)).equals(hdrname.trim())) {
								colnum = k;
								break;
							}
						}
						row = sheet.getRow(rownum);
						if (sheet.getRow(rownum) == null)
							sheet.createRow(rownum);
						try {
							if (row.getCell(colnum) == null) {
								row.createCell(colnum);
								row.getCell(colnum).setCellValue(replacevalue);
								// break;
							} else {
								row.getCell(colnum).setCellValue(replacevalue);
								// break;
							}
						} catch (Exception e) {
							row.getCell(colnum).setCellValue(" ");
							row.getCell(colnum).setCellValue(replacevalue);
							e.printStackTrace();
						}
					}
				}
				FileOperations.writetoexcel(excelfilepath, workbook);
				return true;
			} catch (Exception e) {
				logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			logerOBJ.customlogg(userlogger, "Step Result Failed: ", null, e.toString(), "error");
			e.printStackTrace();
			return false;
		}
	}
	@Sqabind(object_template="sqa_record",action_description = "simulatekeyboardwk", action_displayname = "simulatekeyboardwk", actionname = "simulatekeyboardwk", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public boolean simulatekeyboardwk() throws InterruptedException, AWTException {

		boolean bStatus = false;

		// Thread.sleep(1000);

		try {

			Robot robot = new Robot();

			String OS = System.getProperty("os.name");

			if (OS.toLowerCase().contains("windows")) {

				Thread.sleep(1000);

				robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);

				Thread.sleep(1000);

				robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);

				Thread.sleep(1000);

				robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);

				Thread.sleep(1000);

				robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);

				Thread.sleep(1000);

			} else if (OS.toLowerCase().contains("mac")) {

				Thread.sleep(10000);

				robot.keyPress(java.awt.event.KeyEvent.VK_META);

				Thread.sleep(1000);

				robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);

				Thread.sleep(1000);

				robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);

				Thread.sleep(1000);

				robot.keyRelease(java.awt.event.KeyEvent.VK_META);

				Thread.sleep(1000);

			}

			bStatus = true;

		} catch (Exception e) {

			System.out.println("expection is " + e);

			logerOBJ.customlogg(userlogger, "Step Failed to Execute UploadFile : ", null, e.toString(), "error");

			bStatus = false;

		}

		return bStatus;

	}

	// methods for if else condition
	public boolean eq(String param1, String param2) {
		if (param1.equals(param2)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean ne(String param1, String param2) {
		if (param1.equals(param2)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean lte(String param1, String param2) {
		if (Integer.parseInt(param1) <= Integer.parseInt(param2)) {
			return true;
		} else
			return false;
	}

	public boolean gte(String param1, String param2) {
		if (Integer.parseInt(param1) >= Integer.parseInt(param2)) {
			return true;
		} else
			return false;
	}

	public boolean gt(String param1, String param2) {
		if (Integer.parseInt(param1) > Integer.parseInt(param2)) {
			return true;
		} else
			return false;
	}

	public boolean lt(String param1, String param2) {
		if (Integer.parseInt(param1) < Integer.parseInt(param2)) {
			return true;
		} else
			return false;
	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of given json key to the runtime parameter", action_displayname = "getkeyjson", actionname = "getkeyjson", paraname_equals_curobj = "false", paramnames = { "Json","key","runtime" }, paramtypes = { "String","String","String" })
	public boolean getkeyjson(String Json, String key, String runtime) {
		try {
			Json.replace("\\", "");
			JSONObject obj = new JSONObject(Json);
			if (obj.has(key)) {
				return storeruntime(runtime, obj.get(key).toString());
			} else {
				logger.info("key not found");
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action finds out the memory utilization of the given process", action_displayname = "getprocessutilization", actionname = "getprocessutilization", paraname_equals_curobj = "false", paramnames = { "processToFind" }, paramtypes = { "String" })
	public boolean getprocessutilization(String processToFind) {
		int i = 0;
		String memUtil = null;
		// processToFind="epiplex.exe";
		try {
			String line;
			int startIndex = 0;
			Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
			// System.out.println(p);
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				// System.out.println(line);
				if (line.trim().equals("")) {
					continue;
				}
				if (i == 0) {
					startIndex = line.indexOf("   Mem Usage");
				}
				if (line.trim().startsWith(processToFind)) {
					memUtil = line.substring(startIndex, line.length()).trim();
					System.out.println(memUtil.trim()); // <-- Parse data here

					this.setMemoryUtilization(memUtil.trim());

					break;
				}

				i++;
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		return true;

	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of a json key in the nested json object to runtime parameter", action_displayname = "multiplejsonkey", actionname = "multiplejsonkey", paraname_equals_curobj = "false", paramnames = { "response","key1","key2","key3","identifiername","key4" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean multiplejsonkey(String response, String key1, String key2, String key3, String identifiername,
			String key4) {

		try {
			JSONObject obj = new JSONObject();
			response = response.replace("\\", "");
			JSONObject res = new JSONObject(response);

			if (response_count == 0) {
				if (res.getJSONArray(key3).getJSONObject(0).has(key1)) {
					// for json array
					if (key2 != "") {
						for (int i = 0; i < res.getJSONArray(key3).getJSONObject(0).getJSONArray(key1).length(); i++) {
							if (res.getJSONArray(key3).getJSONObject(0).getJSONArray(key1).getJSONObject(i).has(key2)) {
								obj.put(String.valueOf(i), res.getJSONArray(key3).getJSONObject(0).getJSONArray(key1)
										.getJSONObject(i).get(key2));
							}
						}
					}

					// for json object
					response_count = 0;
				}
			}

			else if (res.getJSONObject(key3).getJSONArray(key4).getJSONObject(0).has(key1) && response_count != 0) {
				// for json array
				if (key2 != "") {
					for (int i = 0; i < res.getJSONObject(key3).getJSONArray(key4).getJSONObject(0).getJSONArray(key1)
							.length(); i++) {
						if (res.getJSONObject(key3).getJSONArray(key4).getJSONObject(0).getJSONArray(key1)
								.getJSONObject(i).has(key2)) {
							obj.put(String.valueOf(i), res.getJSONObject(key3).getJSONArray(key4).getJSONObject(0)
									.getJSONArray(key1).getJSONObject(i).get(key2));
						}
					}
				}

				// for json object
				response_count = 0;
			}
			storeruntime(identifiername, obj.toString());
			return true;
		} catch (Exception e) {
			if (response_count == 0) {
				response_count++;
				return multiplejsonkey(response, key1, key2, key3, identifiername, key4);
			}

			else {
				return false;
			}

		}

	}

	@Sqabind(object_template="sqa_record",action_description = "This action replaces the value of a json key in the nested json object and store it to runtime parameter", action_displayname = "replacejsonkey", actionname = "replacejsonkey", paraname_equals_curobj = "false", paramnames = { "response","key1","value","identifiername","key2","key3" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean replacejsonkey(String response, String key1, String value, String identifiername, String key2,
			String key3) {
		try {

			JSONObject obj = new JSONObject();
			response = response.replace("\\", "");
			JSONObject res = new JSONObject(response);
			int i = 1;

			if (res.has(key2)) {

				if (!key3.equals("")) {
					JSONObject ob = new JSONObject(res.getJSONArray(key2).getJSONObject(0));
					if (res.getJSONArray(key2).getJSONObject(0).getJSONObject(key3).has(key1)) {
						res.getJSONArray(key2).getJSONObject(0).getJSONObject(key3).remove(key1);
						res.getJSONArray(key2).getJSONObject(0).getJSONObject(key3).put(key1,
								Double.parseDouble(value));

//				obj.put(String.valueOf(i), res.getJSONArray("orders").getJSONObject(0).getDouble(key1));
					}
					i = i + 1;
				}

				else {
					JSONObject ob = new JSONObject(res.getJSONArray(key2).getJSONObject(0));
					System.out.println(ob);

					if (res.getJSONArray(key2).getJSONObject(0).has(key1)) {
						res.getJSONArray(key2).getJSONObject(0).remove(key1);
						res.getJSONArray(key2).getJSONObject(0).put(key1, Double.parseDouble(value));
					}
				}
			}
			storeruntime(identifiername, res.toString());
			return true;

		} catch (Exception e) {
			try {

				if (response_count == 0) {

				}

			} catch (Exception e2) {
				// TODO: handle exception
			}
			// TODO: handle exception
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action appends the value of a json key in the nested json object and store it to runtime parameter", action_displayname = "appendjsonobject", actionname = "appendjsonobject", paraname_equals_curobj = "false", paramnames = { "origionalString","arraykey","appendString","runtimename","stop" }, paramtypes = { "String","String","String","String","String" })
	public boolean appendjsonobject(String origionalString, String arraykey, String appendString, String runtimename,
			String stop) {
		// JSONObject json = new JSONObject(string);
		JSONObject origionalJson = new JSONObject(origionalString);
		JSONObject append = new JSONObject(appendString);
		JSONArray data = new JSONArray();
		// JSONObject stops = new JSONObject(stop);

		if (origionalJson.has(arraykey)) {
			if (origionalJson.getJSONArray(arraykey).getJSONObject(0).has(stop)) {
				origionalJson.getJSONArray(arraykey).getJSONObject(0).getJSONArray(stop).put(append);
				System.out.println(origionalJson);
				return storeruntime(runtimename, origionalJson.toString());

			} else {
				System.out.println("key not found");

				return false;
			}

		}
		return false;

	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of a json key in the nested json object to runtime parameter", action_displayname = "getjsonkey", actionname = "getjsonkey", paraname_equals_curobj = "false", paramnames = { "response","key1","key2","key3","identifiername" }, paramtypes = { "String","String","String","String","String" })
	public boolean getjsonkey(String response, String key1, String key2, String key3, String identifiername) {

		try {
			JSONObject obj = new JSONObject();
			response = response.replace("\\", "");
			JSONObject res = new JSONObject(response);

			System.out.println(res);
			if (res.getJSONObject(key1).getJSONArray(key3).getJSONObject(0).has(key2)) {
				// for json array

				for (int i = 0; i < res.getJSONObject(key1).getJSONArray(key3).length(); i++) {
					if (res.getJSONObject(key1).getJSONArray(key3).getJSONObject(i).has(key2)) {
						obj.put(String.valueOf(i),
								res.getJSONObject(key1).getJSONArray(key3).getJSONObject(i).get(key2));
					}
				}

				storeruntime(identifiername, obj.toString());
				return true;

			}
			return false;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public boolean verifyapiresponse(String... param1) {
		JSONObject response1 = new JSONObject(param1[0]);
		// JSONObject response2 = new JSONObject(param1[1]);

		String[] obj = param1[1].split("!##");
		JSONObject res = new JSONObject();
		int count;

		if (response1.length() == obj.length) {
			for (int i = 0; i < obj.length; i++) {
				count = 0;
				for (int j = 0; j < response1.length(); j++) {
					if (response1.getString(String.valueOf(j)).contains(obj[i])) {
						continue;
					} else {

						count++;
					}
				}
				if (count >= response1.length()) {
					return false;
				}
			}
		}

		else if (obj.length < response1.length()) {
			int validate = 0;
			for (int i = 0; i < obj.length; i++) {
				for (int j = 0; j < response1.length(); j++) {
					if (response1.getString(String.valueOf(j)).contains(obj[i])) {
						validate++;
					}

					else {
						continue;
					}
				}

			}

			if (validate == obj.length) {
				return true;
			}

			else {
				return false;
			}
		}
		return true;
	}

	@Sqabind(object_template="sqa_record",action_description = "This action is for rawapi type", action_displayname = "rawapi", actionname = "rawapi", paraname_equals_curobj = "false", paramnames = {"m_baseURI","m_Method","m_Header","Parameters","Body","userName","password","responseBodyRuntime","responseCodeRuntime","responseMsgRuntime"}, paramtypes = { "String","String","String","String","String","String","String","String","String","String" })
	public boolean rawapi(String m_baseURI, String m_Method, String m_Header, String m_Parameters, String m_Body,
			String userName, String password, String responseBodyRuntime, String responseCodeRuntime,
			String responseMsgRuntime) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
			URISyntaxException, IOException {
		super.curExecution = curExecution;
		super.currentSeq = currentSeq;
		super.header = header;
		super.curStep = curStep;
		return super.rawapi(m_baseURI, m_Method, m_Header, m_Parameters, m_Body, userName, password,
				responseBodyRuntime, responseCodeRuntime, responseMsgRuntime);
	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of a json key in the nested json array to runtime parameter", action_displayname = "storearrayjsonkey", actionname = "storearrayjsonkey", paraname_equals_curobj = "false", paramnames = { "basejson","refkey","refvalue","actualkey","index","runtimeparam" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey, String index,
			String runtimeparam) {
		try {
			int occurance = 0;
			JSONArray jsonArray = new JSONArray(basejson);
			String actualvalue = null;
			boolean found = false;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.optJSONObject(i);
				Iterator<String> iterator = object.keys();
				while (iterator.hasNext()) {
					String currentKey = iterator.next();
					if (currentKey.equals(refkey)) {
						if (Integer.parseInt(index) == occurance) {
							if (object.get(refkey).equals(refvalue)) {
								actualvalue = object.get(actualkey).toString();
								found = true;
								break;
							}
						} else {
							occurance++;
						}
					}
				}
				if (found) {
					break;
				}
			}
			return storeruntime(runtimeparam, actualvalue);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of a json key in the nested json array to runtime parameter", action_displayname = "storearrayjsonkey", actionname = "storearrayjsonkey", paraname_equals_curobj = "false", paramnames = { "basejson","refkey","refvalue","actualkey","runtimeparam" }, paramtypes = { "String","String","String","String","String" })
	public boolean storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey,
			String runtimeparam) {
		try {
			Map<String, String> jsonStringify = flattenJSON(basejson);
			String actualvalue = null;
			boolean found = false;
			if (jsonStringify.get(refkey).equals(refvalue)) {
				actualvalue = jsonStringify.get(actualkey).toString();
				found = true;
				// break;
			}
			return storeruntime(runtimeparam, StringUtils.chomp(actualvalue));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action stores the value of a json key in the nested json array to runtime parameter", action_displayname = "storearrayjsonkey", actionname = "storearrayjsonkey", paraname_equals_curobj = "false", paramnames = { "basejson","refkey","refvalue","actualkey" }, paramtypes = { "String","String","String","String" })
	public static String storearrayjsonkey(String basejson, String refkey, String refvalue, String actualkey) {
		String runtimeparam = null;
		try {
			JSONArray jsonArray = new JSONArray(basejson);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.optJSONObject(i);
				Iterator<String> iterator = object.keys();
				while (iterator.hasNext()) {
					String currentKey = iterator.next();
					if (currentKey.equals(refkey)) {
						if (object.get(refkey).toString().contains(refvalue)) {
							runtimeparam = (java.lang.String) object.get(actualkey).toString();
							break;
						}
					}
				}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return runtimeparam;
	}

	@Sqabind(object_template="sqa_record",action_description = "This action gets the mail and stores it to runtime parameter", action_displayname = "getmailfrommailtrap", actionname = "getmailfrommailtrap", paraname_equals_curobj = "false", paramnames = { "authkey","inboxname","subject","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean getmailfrommailtrap(String authkey, String inboxname, String subject, String runtimeparam) {
		try {
			authkey = authkey.replace("\\\"", "\"");
			JSONObject Headerjson = new JSONObject(authkey);
			Iterator<String> keys = Headerjson.keys();
			HashMap<String, String> hdr = new HashMap<String, String>();
			while (keys.hasNext()) {
				String headerKey = keys.next();
				String headerValue = Headerjson.get(headerKey).toString();
				hdr.put(headerKey, headerValue);
			}

			// API to get the inboxes
			HttpResponse rawinboxresp = HttpUtility.postapi("https://mailtrap.io/api/v1/inboxes", "GET", "", hdr);
			String inboxresp = new BasicResponseHandler().handleResponse(rawinboxresp);
			String inboxid = storearrayjsonkey(inboxresp.toString(), "name", inboxname, "id");

			// API to get the messages
			HttpResponse rawmsgresp = HttpUtility.postapi("https://mailtrap.io/api/v1/inboxes/" + inboxid + "/messages",
					"GET", "", hdr);
			String messageresp = new BasicResponseHandler().handleResponse(rawmsgresp);
			String messageid = storearrayjsonkey(messageresp.toString(), "subject", subject, "id");

			// API to get the messagebody
			HttpResponse rawmessagebody = HttpUtility.postapi(
					"https://mailtrap.io/api/v1/inboxes/" + inboxid + "/messages/" + messageid + "/body.txt", "GET", "",
					hdr);
			String messagebody = new BasicResponseHandler().handleResponse(rawmessagebody);
			return storeruntime(runtimeparam, messagebody.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "This action gets the mail and stores it to runtime parameter", action_displayname = "getmail", actionname = "getmail", paraname_equals_curobj = "false", paramnames = { "servername","portno","username","password","subject","runtimparam" }, paramtypes = { "String","String","String","String","String","String" })
	public boolean getmail(String servername, String portno, String username, String password, String subject,
			String runtimparam) {
		try {
			Properties props = new Properties();
			// props.put("mail.imaps.tls", "true");
			Session session = Session.getDefaultInstance(props);
			Store store = session.getStore("imaps");
			// System.setProperty("mail.imap.ssl.protocols", "TLSv1.2");
			store.connect(servername, Integer.parseInt(portno), username, password);
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			String content = null;
			Calendar cal = Calendar.getInstance();
			ReceivedDateTerm term = new ReceivedDateTerm(ComparisonTerm.EQ, new Date(cal.getTimeInMillis()));
			Message[] messages = inbox.search(term);
			// Message[] messagew = inbox.getMessages();

			// Sort messages from recent to oldest
			Arrays.sort(messages, (m1, m2) -> {
				try {
					return m2.getSentDate().compareTo(m1.getSentDate());
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			});
			for (Message message : messages) {
				if (message.getSubject().contains(subject)) {
					content = getTextFromMessage(message);
					break;
					// System.out.println(content);
				}
			}
			return storeruntime(runtimparam, content);
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public boolean getspammail(String username, String password, String subject, String runtimparam) {
		try {
			String content = null;
			Properties properties = System.getProperties();
			properties.setProperty("mail.store.protocol", "gimaps");
			Session emailSession = Session.getDefaultInstance(properties, null);
			GmailStore store = (GmailStore) emailSession.getStore("gimap");
			store.connect("imap.gmail.com", username, password);
			GmailFolder inbox = (GmailFolder) store.getFolder("[Gmail]/Spam");
			inbox.open(Folder.READ_ONLY);
			Calendar cal = Calendar.getInstance();
			ReceivedDateTerm term = new ReceivedDateTerm(ComparisonTerm.EQ, new Date(cal.getTimeInMillis()));
			Message[] messages = inbox.search(term);
			// Message[] messagew = inbox.getMessages();

			// Sort messages from recent to oldest
			Arrays.sort(messages, (m1, m2) -> {
				try {
					return m2.getSentDate().compareTo(m1.getSentDate());
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			});
			for (Message message : messages) {
				if (message.getSubject().contains(subject)) {
					content = getTextFromMessage(message);
					break;
					// System.out.println(content);
				}
			}
			return storeruntime(runtimparam, content);
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		MimeMultipart mimeMultipart = null;
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		} else {
			mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	public static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		String html;
		BodyPart bodyPart = mimeMultipart.getBodyPart(0);
		if (bodyPart.isMimeType("text/plain")) {
			result = (String) bodyPart.getContent();
		} else if (bodyPart.isMimeType("text/html")) {
			html = (String) bodyPart.getContent();
			result = org.jsoup.Jsoup.parse(html).text();
		} else if (bodyPart.getContent() instanceof MimeMultipart) {
			result = getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
		}
		return result;

	}

	@Sqabind(object_template="sqa_record",action_description = "This action reads the micosoft outlook emails", action_displayname = "microsoftreademails", actionname = "microsoftreademails", paraname_equals_curobj = "false", paramnames = { "emailid","password","subject","runtimeparam" }, paramtypes = { "String","String","String","String" })
	public boolean microsoftreademails(String emailid, String password, String subject, String runtimeparam) {
		List<Map> msgDataList = new ArrayList<>();
		try {
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
			service.setUrl(new URI("https://outlook.office365.com/EWS/exchange.asmx"));
			ExchangeCredentials credentials = new WebCredentials(emailid, password);
			service.setCredentials(credentials);

			try {
				service.setTraceEnabled(true);
				SearchFilter sf = new SearchFilter.SearchFilterCollection(LogicalOperator.And,
						new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false),
						new SearchFilter.ContainsSubstring(EmailMessageSchema.Subject, subject));

				System.out.println("|---------------------> service = {}" + service);
				microsoft.exchange.webservices.data.core.service.folder.Folder folder = microsoft.exchange.webservices.data.core.service.folder.Folder
						.bind(service, WellKnownFolderName.Inbox);
				FindItemsResults<Item> results = service.findItems(folder.getId(), sf, new ItemView(1));
				int i = 1;
				boolean status = false;
				for (Item item : results) {
					Map messageData = readEmailItem(item.getId());
					status = storeruntime(runtimeparam, messageData.get("emailBody").toString());
//					System.out.println("|---------------------> service = {}" + (i++) + ":");
//					System.out.println("|---------------------> service = {}" + messageData.get("subject").toString());
////					System.out
////							.println("|---------------------> service = {}" + messageData.get("senderName").toString());
//
//					System.out
//							.println("|---------------------> service = {}" + messageData.get("emailBody").toString());
				}
				return status;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public Map readEmailItem(ItemId itemId) {
		Map messageData = new HashMap();
		try {

			PropertySet itempropertyset = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject,
					EmailMessageSchema.Body);
			itempropertyset.setRequestedBodyType(BodyType.Text);

			Item itm = Item.bind(service, itemId, itempropertyset);

//            Item itm = Item.bind(service, itemId, PropertySet.FirstClassProperties);
			EmailMessage emailMessage = EmailMessage.bind(service, itm.getId());
			messageData.put("emailItemId", emailMessage.getId().toString());
			messageData.put("subject", emailMessage.getSubject());
			messageData.put("fromAddress", emailMessage.getFrom().getAddress());
			messageData.put("senderName", emailMessage.getSender().getName());
			Date dateTimeCreated = emailMessage.getDateTimeCreated();
			messageData.put("SendDate", dateTimeCreated.toString());
			Date dateTimeRecieved = emailMessage.getDateTimeReceived();
			messageData.put("RecievedDate", dateTimeRecieved.toString());
			messageData.put("Size", emailMessage.getSize() + "");
			messageData.put("emailBody", itm.getBody());

			// .getBody().toString());
			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return messageData;
	}

	@Sqabind(object_template="sqa_record",action_description = "replacexmlkey", action_displayname = "replacexmlkey", actionname = "replacexmlkey", paraname_equals_curobj = "false", paramnames = { "xml","key1","key2","key3","key4","key5","add","key6"}, paramtypes = { "String","String","String","String","String","String","String","String" })

	public boolean replacexmlkey(String xml, String key1, String key2, String key3, String key4, String key5,
			String whatToadd, String key6) {

		try {
			Document doc = convertStringToXMLDocument(xml);
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Node deliveritload = doc.getFirstChild();
			String key4duplicate = "ns0:" + key4;
			Node LoadHeaderchilds = doc.getElementsByTagName(key4duplicate).item(0);

			NodeList list = LoadHeaderchilds.getChildNodes();
			String key5duplicate = "ns0:" + key5;
			for (int temp = 0; temp < list.getLength(); temp++) {
				Node node = list.item(temp);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;
					if (key5duplicate.equals(eElement.getNodeName())) {

						System.out.println("Before" + eElement.getTextContent());
						eElement.setTextContent(whatToadd);
						System.out.println("After" + eElement.getTextContent());

					}
				}
			}

			String updatedxml = getStringFromDocument(doc);
			System.out.println("Xml after updation---------" + updatedxml);
			storeruntime(key6, updatedxml);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	private Document convertStringToXMLDocument(String xmlString) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getStringFromDocument(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String convertjsontoxml(String json, String root) throws JSONException {
		try {

			JSONObject jsonObject = new JSONObject(json);

			String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>\n<" + root + ">" + XML.toString(jsonObject)
					+ "</" + root + ">";

			return xml;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}

	}

	private boolean keyExists(JSONObject object, String searchedKey) {
		boolean exists = object.has(searchedKey);
		if (!exists) {
			Iterator<?> keys = object.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (object.get(key) instanceof JSONObject) {
					exists = keyExists((JSONObject) object.get(key), searchedKey);
				}
			}
		}
		return exists;
	}

	
	@Sqabind(object_template="sqa_record",action_description = "connecttomongodb", action_displayname = "connecttomongodb", actionname = "connecttomongodb", paraname_equals_curobj = "false", paramnames = {"dburl","portno","dbname","username","password"}, paramtypes = { "String","String","String","String","String" })
	public boolean connecttomongodb(String dburl, String portno, String dbname, String username, String password) {
		try {

			char[] passwrdtochar = password.toCharArray();

			String string = "mongodb://" + username + ":" + password + "@" + dburl + ":" + portno + "/" + dbname;

			mongoClient = MongoClients.create(string);
//				mongoClient.getDatabase("e2epie_drm");

			System.out.println("started");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Sqabind(object_template="sqa_record",action_description = "readfrommongodb", action_displayname = "readfrommongodb", actionname = "readfrommongodb", paraname_equals_curobj = "false", paramnames = {"dbname","collectionname","filterquery","runtimeparam"}, paramtypes = { "String","String","String","String" })
	public boolean readfrommongodb(String dbname, String collectionname, String filterquery, String runtimeparam) {
		try {

			// MongoClient database=
			// connecttomongodb(dburl,portno,dbname,username,password);

			if (mongoClient == null) {
				return false;
			} else {
				MongoDatabase db = mongoClient.getDatabase(dbname);

				BasicDBObject allQuery = new BasicDBObject();
				BasicDBObject fields = new BasicDBObject();
				fields.put("name", 1);

				MongoCollection<org.bson.Document> col = db.getCollection(collectionname);

				System.out.println(db.getCollection(collectionname).countDocuments());

//			BasicDBObject query = new BasicDBObject(); 
//		    BasicDBObject fields = new BasicDBObject("ObjectId","4f693d40e4b04cde19f17205");
//			BsonDocument doc= BsonDocument.parse(json);

//			(org.bson.Document) collectionname.find(new Document("_id",""));
				FindIterable<org.bson.Document> reference = col.find();
				MongoCursor<org.bson.Document> cursor = reference.iterator();

				List<String> resultlist = new ArrayList<String>();

				String res;
				while (cursor.hasNext()) {
					resultlist.add(cursor.next().toJson());
				}
				System.out.println("finished");

				System.out.println(resultlist.toString());
				JSONObject main_ = new JSONObject();
				JSONArray obj = new JSONArray(resultlist.toString());
				for (int i = 0; i < obj.length(); i++) {
					String getid = obj.getJSONObject(i).get("_id").toString();
					JSONObject new_id = new JSONObject(getid);
					if (new_id.get("$oid").equals(filterquery)) {
						main_ = obj.getJSONObject(i);
					}

				}
				return storeruntime(runtimeparam, main_.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Sqabind(object_template="sqa_record",action_description = "closeconnectionmongodb", action_displayname = "closeconnectionmongodb", actionname = "closeconnectionmongodb", paraname_equals_curobj = "false", paramnames = {}, paramtypes = {})
	public Boolean closeconnectionmongodb() {

		mongoClient.close();
		logger.info("DB connection close successfully");
		return true;

	}

	@Sqabind(object_template="sqa_record",action_description = "storemultiplekeys", action_displayname = "storemultiplekeys", actionname = "storemultiplekeys", paraname_equals_curobj = "false", paramnames = {"Json","keys","runtim1"}, paramtypes = {"String","String","String"})
	public boolean storemultiplekeys(String Json, String keys, String runtim1) {

		try {
			Json.replace("\\", "");
			JSONObject obj = new JSONObject(Json);
			String[] key_array = keys.split(",");
			String[] runtime = runtim1.split(",");

			if (key_array.length == runtime.length) {
				for (int i = 0; i < key_array.length; i++) {
					if (obj.has(key_array[i])) {

						String value = obj.getString(key_array[i]);
						storeruntime(runtime[i], value.toString());

					} else {
						logger.info("Key not found..");
						return false;

					}

				}
				return true;
			} // end of if...
			else {
				logger.info("keys length not matched with the runtime parameter length..");
				return false;
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			return false;
		}

	}
	
	public Boolean datadisplay(String param_name,String param_value) {
		InitializeDependence.param_description_name=param_name;
		InitializeDependence.param_description_value=param_value;
		return true;


	}

	
}
