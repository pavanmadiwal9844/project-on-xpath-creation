package com.simplifyqa.method;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.StreamSupport;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.Utility.UtilEnum;
import com.simplifyqa.DTO.Object;
import com.simplifyqa.DTO.Setupexec;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.customMethod.Editorclassloader;
import com.simplifyqa.desktop.impl.DesktopImpl;
import com.simplifyqa.sap.Impl.SapImpl;

public class SapMethod implements com.simplifyqa.method.Interface.SapMethod {
	final static Logger logger = LoggerFactory.getLogger(DesktopImpl.class);

	public static Semaphore semCon;
	public static Semaphore semProd;
	private SapImpl sap = new SapImpl();
	private Process recorderProcess;
	private Process playbackProcess;
	private Setupexec curExecuiton;
	private Boolean captureScreenshot = false;
	private HashMap<String, String> header;
	private JSONObject status;
	private String[] attrName = { "id", "name", "type" };
	private String attrlaunch = "ExePath";
	private Integer i = -1;
	private Editorclassloader customMethod = null;
	@SuppressWarnings("rawtypes")
	private Class myClass = null;
	private String classname = "CustomMethodsmainframe";
	public static String launchprocess;
	public static Boolean release = false;

	@Override
	public JSONObject entertext(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "entertext", exename, type);
	}

	@Override
	public JSONObject click(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "click", exename,type);
	}

	public JSONObject waitforelement(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "waitforelement", exename,type);
	}

	@Override
	public JSONObject rightclick(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "rightclick", exename,type);
	}

	public Boolean stopplayback() {
		//String str = splitPath(recorderProcess);
		//sap.stopPlaybackExe(str);
		return sap.stopPlaybackExe(playbackProcess);
	}

	public String splitPath(String pathString) {
		Path path = Paths.get(pathString);
		String[] str = StreamSupport.stream(path.spliterator(), false).map(Path::toString).toArray(String[]::new);
		return str[str.length - 1];
	}

	public Boolean stopreccorder() {
		if (recorderProcess != null) {
			return sap.stopRecorderExe(recorderProcess);
		}
		return false;
	}

//	public JSONObject launchapplication(String identifierName, String value,String str) {
//		return postwithTD(identifierName, value,str, "launchapplication");
//
//	}

	@Override
	public JSONObject doubleclick(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "doubleclick", exename,type);
	}

	@Override
	public JSONObject keyboardaction(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "keyboardaction", exename,type);
	}

	@Override
	public JSONObject waitfortext(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "waitfortext", exename,type);
	}

	@Override
	public JSONObject validatetext(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "validatetext", exename,type);
	}

	@Override
	public JSONObject validatepartialtext(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "validatepartialtext", exename,type);
	}

	@Override
	public JSONObject deletetext(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "deletetext", exename,type);
	}

	@Override
	public JSONObject findonscreen(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "findonscreen", exename,type);
	}

	@Override
	public JSONObject readtextandstore(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "readtextandstore", exename,type);
	}

	@Override
	public JSONObject getfromruntime(String identifierName) {
		try {
			return InitializeDependence.serverCall.getruntimeParameter(curExecuiton, identifierName, header);
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean stopRecorder() {
		/*String str = splitPath(recorderProcess);
		sap.stopPlaybackExe(recorderProcess.);*/
		return sap.stopRecorderExe(recorderProcess);
	}

	@Override
	public void setDetail(Setupexec executionDetail, HashMap<String, String> header, Integer itr) {
		this.curExecuiton = executionDetail;
		this.header = header;
	}

	public JSONObject launchApp() {
		try {
			semProd.acquire();
			startPlayback();
			semProd.release();
			semCon.acquire();
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return null;
		}
	}

	public Boolean startPlayback() {
		try {
			if (recorderProcess != null) {
				recorderProcess.destroyForcibly();
			}
			playbackProcess = sap.startPlaybackExe(getSimplifyPlaybackExepath(), getSimplifyPlaypath());
			return true;
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	public Boolean startRecorder() {
		try {
			recorderProcess = sap.startRecorderExe(getSimplifyRecordExepath(), getSimplifyRecordpath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private String getSimplifyRecordExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.SAPFOLDER.value(),
				UtilEnum.SAPRECORDFOLDER.value(), UtilEnum.SAPRECORD.value() }).toString();
	}

	private String getSimplifyPlaybackExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.SAPFOLDER.value(),
				UtilEnum.SAPPLAYBACKFOLDER.value(), UtilEnum.SAPPLAYBACK.value() }).toString();
	}

	private String getSimplifyRecordpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { UtilEnum.SAPFOLDER.value(), UtilEnum.SAPRECORDFOLDER.value() }).toString();
	}

	private String getSimplifyPlaypath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { UtilEnum.SAPFOLDER.value(), UtilEnum.SAPPLAYBACKFOLDER.value() }).toString();
	}

	public Boolean startInspector() {
		try {
			recorderProcess = sap.startRecorderExe(getSimplifyInspectorExepath(), getSimplifyInspecotpath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Boolean startValidation() {
		try {
			recorderProcess = sap.startRecorderExe(getSimplifyvalidationpath(), getSimplifyValidatepath());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private String getSimplifyInspectorExepath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.SAPFOLDER.value(),
				UtilEnum.SAPOBJECTPICKER.value(), UtilEnum.SAPINSPECTFOLDER.value(), UtilEnum.SAPINSPECTOR.value() }).toString();
	}
	
	private String getSimplifyvalidationpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { UtilEnum.SAPFOLDER.value(),
				UtilEnum.SAPOBJECTPICKER.value(),UtilEnum.SAPVALIDATIONFOLDER.value(), UtilEnum.SAPVALIDATION.value() }).toString();
	}

	private String getSimplifyInspecotpath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.SAPFOLDER.value(), UtilEnum.SAPOBJECTPICKER.value(), UtilEnum.SAPINSPECTFOLDER.value() })
				.toString();
	}
	private String getSimplifyValidatepath() {
		return Paths
				.get(UserDir.getAapt().getAbsolutePath(),
						new String[] { UtilEnum.SAPFOLDER.value(),  UtilEnum.SAPOBJECTPICKER.value(), UtilEnum.SAPVALIDATIONFOLDER.value() })
				.toString();
	}
	
	public void release(JSONObject res) {
		try {
			status = res;
			semCon.release();
			Thread.sleep(200);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
	}

	public JSONObject click(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurclick(steObject.getAttributes(), i);
	}
	
	public JSONObject mouseclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurMouseclick(steObject.getAttributes(), i);
	}

	public JSONObject waitforelement(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return waitforelement(steObject.getAttributes(), i);
	}

	public JSONObject rightclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resRightclick(steObject.getAttributes(), i);
	}

	public JSONObject validatetext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatetext(steObject.getAttributes(), tcData, i);
	}

//	public JSONObject launchapplication(Object steObject, Boolean captureScreenshot) {
//		i = 0;
//		launchApp();
//		this.captureScreenshot = captureScreenshot;
//		return launchapplication(steObject.getAttributes(), i);
//	}

	public JSONObject keyboardaction(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return keyboardaction(steObject.getAttributes(), tcData, i);
	}

	public JSONObject readtextandstore(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return readtextandstore(steObject.getAttributes(), i);
	}

	public JSONObject deletetext(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return deletetext(steObject.getAttributes(), i);
	}

	public JSONObject waitfortext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return waitfortext(steObject.getAttributes(), tcData, i);
	}

	public JSONObject findonscreen(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return findonscreen(steObject.getAttributes(), tcData, i);
	}

	public JSONObject validatepartialtext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return validatepartialtext(steObject.getAttributes(), tcData, i);
	}

	private JSONObject findonscreen(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");
			if (attr != null) {
				if ((res = findonscreen(attr.getString("name"), attr.getString("value"), tcData, exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return findonscreen(attributes, tcData, ++i);
				}
			} else
				return findonscreen(attributes, tcData, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private String exename(List<JsonNode> Attributes, String identifierName) {
		for (int j = 0; j < Attributes.size(); j++) {
			if (Attributes.get(i).get(UtilEnum.NAME.value()).asText().equalsIgnoreCase(identifierName)) {
				return Attributes.get(i).get("value").asText();
			}
		}
		return null;
	}
	private String type(List<JsonNode> Attributes, String identifierName) {
		for (int j = 0; j < Attributes.size(); j++) {
			if (Attributes.get(j).get("name").asText().equals("Type")) {
				return Attributes.get(j).get("value").asText();
			}
		}
		return null;
	}

	private JSONObject validatetext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = validatetext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData, exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatetext(attributes, tcData, ++i);
				}
			} else {
				return validatetext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject waitfortext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = waitfortext(attr.getString("name"), attr.getString("value"), tcData, exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return waitfortext(attributes, tcData, ++i);
				}
			} else
				return waitfortext(attributes, tcData, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject deletetext(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = deletetext(attr.getString("name"), attr.getString("value"), exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return deletetext(attributes,  ++i);
				}
			} else
				return deletetext(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}
	

	private JSONObject readtextandstore(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = readtextandstore(attr.getString("name"), attr.getString("value"), exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return readtextandstore(attributes, ++i);
				}
			} else
				return readtextandstore(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject keyboardaction(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			if (i > 1) {
				return null;
			}
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
//			if (attr != null) {
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if ((res = keyboardaction(attr.getString("name"), attr.getString("value"), tcData, exename,type)).getString("play_back").toLowerCase()
					.equals(UtilEnum.PASSED.value().toLowerCase())) {
				return res;
			} else {
				return keyboardaction(attributes, tcData, ++i);
			}
//			} else
//				return keyboardaction(attributes, tcData, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject waitforelement(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = waitforelement(attr.getString("name"), attr.getString("value"), exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())
						|| attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return waitforelement(attributes, ++i);
				}
			} else
				return waitforelement(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject resCurclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type = type(attributes, "type");

			if (attr != null) {
				if ((res = click(attr.getString("name"), attr.getString("value"), exename,type)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()) || attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurclick(attributes, ++i);
				}
			} else
				return resCurclick(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurMouseclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type = type(attributes, "type");

			if (attr != null) {
				if ((res = mouseclick(attr.getString("name"), attr.getString("value"), exename,type)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()) || attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurMouseclick(attributes, ++i);
				}
			} else
				return resCurMouseclick(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}


	private JSONObject resRightclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = rightclick(attr.getString("name"), attr.getString("value"), exename,type)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()) || attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resRightclick(attributes, ++i);
				}
			} else
				return resRightclick(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject validatepartialtext(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = validatepartialtext(attr.getString("name").toLowerCase(), attr.getString("value"), tcData,
						exename,type)).getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return validatepartialtext(attributes, tcData, ++i);
				}
			} else {
				return validatepartialtext(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject postwithoutTD(String identifierName, String value, String action, String exename,String type) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			req.put("exename", exename);
			req.put("type", type);
			System.out.println(req.toString());
			GlobalDetail.sapsession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			System.out.println("status" + status.toString());
			return status;
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}
	}

	private JSONObject postwithTD(String identifierName, String value, String str, String action, String exename,String type) {
		try {
			semProd.acquire();
			JSONObject req = new JSONObject();
			req.put(UtilEnum.DATA.value(), "object");
			req.put(UtilEnum.IDENTIFIERNAME.value(), identifierName);
			req.put(UtilEnum.IDENTIFIERVALUE.value(), value);
			req.put(UtilEnum.SCREENSHOT.value(), captureScreenshot);
			req.put("action", action);
			req.put("tcData", str);
			req.put("exename", exename);
			req.put("type", type);
			GlobalDetail.sapsession.getBasicRemote().sendText(req.toString());
			semProd.release();
			Thread.sleep(200);
			semCon.acquire();
			System.out.println("status" + status.toString());
			return status;
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;

		}
	}

	public JSONObject entertext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurEnterText(steObject.getAttributes(), tcData, i);
	}

	public JSONObject keyboardentertext(Object steObject, String tcData, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurKeyboardEnterText(steObject.getAttributes(), tcData, i);
	}

	public JSONObject launchapplication(Object steObject, String tcData, Boolean captureScreenshot, String object_name)
			throws InterruptedException {
		i = 0;
		launchApp();
		this.captureScreenshot = captureScreenshot;
		return launchapplication(steObject.getAttributes(), tcData, i, object_name);
	}

	public JSONObject doubleclick(Object steObject, Boolean captureScreenshot) {
		i = 0;
		this.captureScreenshot = captureScreenshot;
		return resCurdoubleclick(steObject.getAttributes(), i);
	}

	private JSONObject resCurdoubleclick(List<JsonNode> attributes, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type=type(attributes,"type");

			if (attr != null) {
				if ((res = doubleclick(attr.getString("name"), attr.getString("value"), exename,type)).getString("play_back")
						.toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase()) || attr.getBoolean("unique")) {
					return res;
				} else {
					if (i >= attrName.length - 1) {
						return res;
					}
					return resCurdoubleclick(attributes, ++i);
				}
			} else
				return resCurdoubleclick(attributes, ++i);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}

	private JSONObject resCurEnterText(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type = type(attributes, "type");
			if (attr != null) {
				if ((res = entertext(attr.getString("name"), attr.getString("value"), tcData, exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					return res;
				}
			} else {
				return resCurEnterText(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}
	
	private JSONObject resCurKeyboardEnterText(List<JsonNode> attributes, String tcData, Integer i) {
		try {
			if (i > attrName.length)
				return null;
			JSONObject res = null;
			JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrName[i]);
			String exename = exename(attributes, "exename");
			String type = type(attributes, "type");
			if (attr != null) {
				if ((res = keyboardentertext(attr.getString("name"), attr.getString("value"), tcData, exename,type))
						.getString("play_back").toLowerCase().equals(UtilEnum.PASSED.value().toLowerCase())) {
					return res;
				} else {
					return res;
				}
			} else {
				return resCurKeyboardEnterText(attributes, tcData, ++i);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}

	}
	

	private JSONObject launchapplication(List<JsonNode> attributes, String tcData, Integer i, String object_name)
			throws InterruptedException {
//		try {
//		if (i > attrName.length)
//			return null;
//		JSONObject res = null;
//		JSONObject attr = InitializeDependence.findattrDesktop(attributes, attrlaunch);
//		String exename = exename(attributes,"exename");
//		if (attr != null) {
//			if ((res = launchapplication("xpath", attr.getString("value"), tcData, exename)).getString("play_back").toLowerCase()
//					.equals(UtilEnum.PASSED.value().toLowerCase())||attr.getBoolean("unique")) {
//				return res;
//			} else {
//				if(i>=attrName.length-1) {
//					return res;
//				}
//				return res;
//			}
//		} else
//			return res;
//		} catch (Exception e) {
//			return null;
//		}
//
		launchprocess = tcData;
		try {
			tcData = tcData.replaceAll(
					"[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]",
					"");
			Process launch = Runtime.getRuntime()
					.exec(new String[] { "cmd", "/c", Paths.get(tcData).toAbsolutePath().toString() });
			Thread.sleep(2000);
			return new JSONObject().put("play_back", "passed").put("tcData", tcData).put("captureScreenshot", "");
		} catch (IOException e) {
			return new JSONObject().put("play_back", "failed").put("tcData", tcData).put("captureScreenshot", "");
		}
	}

	@Override
	public JSONObject launchApplication() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean wait(Integer timeout) {
		try {
			Thread.sleep(timeout);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public JSONObject launchapplication(String identifierName, String value) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public JSONObject keyboardentertext(String identifierName, String value, String str, String exename,String type) {
		return postwithTD(identifierName, value, str, "keyboard entertext", exename,type);
	}

	@Override
	public JSONObject mouseclick(String identifierName, String value, String exename,String type) {
		return postwithoutTD(identifierName, value, "mouse click", exename,type);
	}


}
