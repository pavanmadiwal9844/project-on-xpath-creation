package com.simplifyqa.method;

import java.awt.Robot;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.GET;

import org.eclipse.jetty.util.ajax.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.sqadrivers.webdriver;
import com.simplifyqa.web.DTO.Webcapabilities;

public class CustomerMethod {
	int port = 9515;
	HashMap<String, String> headers = new HashMap<String, String>();
	String sessionId;
	String elementId;
	private Process process;
	private String hostAddrss;
	
	
	public static void main(String[] args) {
		CustomerMethod a = new CustomerMethod();
		
		a.capabilities(); 
		a.chromedriver();
		a.newsession("http://127.0.0.1:9515");
		a.launchurl("https://WWW.facebook.com");
		a.findelement("xpath", "//input[@name='email']");
		a.Entertext("hazarath22@gmail.com");
	    a.findelement("xpath", "//input[@id='pass']");
		a.enterpassword("hazarath@2324");
		a.findelement("xpath", "//button[@name='login']");
		a.login(); 
	   
		
		
		/*a.launchbrowser();
		a.newsession("http://127.0.0.1:9515");
		a.launchurlapp("https:qa.simplifyqa.app");
		a.findelement("xpath", "//input[@name='email']");
		a.elementclick();
		a.sendkeys("hajarath.ali@simplify3x.com");
    	a.findelement("xpath", "//button[contains(text(),'Login')]");
		a.loginclick();
		a.findelement("xpath", "//input[@name='password']");
		a.password("Lucky!@#2324");
		a.findelement("xpath", "//button[contains(text(),'Login')]");
		a.loginclick();	*/
	}

	/*public JSONObject capabilities() {
		JSONObject obj = new JSONObject("{\"capabilities\": { \"alwaysMatch\": {\"platformName\": \"windows\" },\"firstMatch\": [{\"browserName\": \"chrome\"}]}}");
		return obj;
		
	}
	
	public boolean launchbrowser() {
		try {
//		System.setProperty("chrome", "./libs/drivers/chromedriver.exe");
		ProcessBuilder builder = new ProcessBuilder(new String[] { "C:\\Program Files (x86)\\Simplify3x\\SimplifyQA\\libs\\drivers\\chromedriver.exe", String.valueOf(port) });
		Thread.sleep(500);
		builder.redirectErrorStream(true);
		Process process = builder.start();
		}catch (Exception e) {
			return false;
		}
		return true;
		
	}
	
	public boolean newsession(String hostAddrss) {
		try { 
			this.hostAddrss = hostAddrss;
			headers.put("content-type", "application/json");
			JSONObject res = capabilities();
			HttpUtility utility = new HttpUtility();
			res = new JSONObject(utility.SendPost(hostAddrss + "/session",res.toString(), headers));
			sessionId = res.getJSONObject("value").getString("sessionId");
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		

		return true;
		
	}
	
	public boolean launchurlapp(String url) {
		
	JSONObject jsonobject = new JSONObject();
	jsonobject.put("url", url);
		String host = hostAddrss + "/session" + "/" + sessionId + "/url";
		headers.put("content-type", "application/json");
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, jsonobject.toString(), headers);
			//elementId = res.getJSONObject("value").getString("elementId");
			 
			
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	public boolean findelement(String using, String value) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	 
		String host = hostAddrss + "/session/" + sessionId + "/element";
		JSONObject obj = new JSONObject();
		obj.put("using", using);
		obj.put("value", value);
		try {
			JSONObject res = HttpUtility.sendPost(host, obj.toString(), headers);
			JSONObject element = res.getJSONObject("value");
			Iterator<String> keys = element.keys();
			String key = null;
			while (keys.hasNext()) {
				key = keys.next();
			}
			elementId = element.getString(key);
}catch(Exception e) {
	e.printStackTrace();
	return false;
}
		return true;
}
	public boolean elementclick() {
		String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId + "/click";
		JSONObject res;
		JSONObject obj = new JSONObject();
		try {
			res = HttpUtility.sendPost(host, obj.toString(), headers);
	}catch(Exception e) {
		e.printStackTrace();
	}
		return true;
}
	public boolean sendkeys(String valuetoenter) {
		String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId + "/value";
		JSONObject obj = new JSONObject();
		obj.put("text", valuetoenter);
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, obj.toString(), headers);
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
		return true;
}
	
	
	/*public boolean element(String using, String value) {
		 this.elementId=elementId;
			String host = hostAddrss + "/session/" + sessionId + "/element";
			JSONObject obj = new JSONObject();
			obj.put("using", using);
			obj.put("value", value);
			try {
				JSONObject res = HttpUtility.sendPost(host, obj.toString(), headers);
				JSONObject element = res.getJSONObject("value");
				Iterator<String> keys = element.keys();
				String key = null;
				while (keys.hasNext()) {
					key = keys.next();
				}
				elementId = element.getString(key);
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
			return true;
	}
	public boolean loginclick() {
		
		String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId + "/click";
		JSONObject res;
		JSONObject obj = new JSONObject();
		try {
			res = HttpUtility.sendPost(host, obj.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	/*public boolean newelement(String using, String value) {
		 this.elementId=elementId;
			String host = hostAddrss + "/session/" + sessionId + "/element";
			JSONObject obj = new JSONObject();
			obj.put("using", using);
			obj.put("value", value);
			try {
				JSONObject res = HttpUtility.sendPost(host, obj.toString(), headers);
				JSONObject element = res.getJSONObject("value");
				Iterator<String> keys = element.keys();
				String key = null;
				while (keys.hasNext()) {
					key = keys.next();
				}
				elementId = element.getString(key);
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
			return true;
	}
	
	public boolean password(String valuetoenter) {
		
		String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId + "/value";
		JSONObject ab = new JSONObject();
		ab.put("text", valuetoenter);
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, ab.toString(), headers);
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
		return true;
	}
	/*public boolean newelement1(String using, String value) {
		 this.elementId=elementId;
			String host = hostAddrss + "/session/" + sessionId + "/element";
			JSONObject obj = new JSONObject();
			obj.put("using", using);
			obj.put("value", value);
			try {
				JSONObject res = HttpUtility.sendPost(host, obj.toString(), headers);
				JSONObject element = res.getJSONObject("value");
				Iterator<String> keys = element.keys();
				String key = null;
				while (keys.hasNext()) {
					key = keys.next();
				}
				elementId = element.getString(key);
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
			return true;
	}
	public boolean login() {
		
		String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId + "/click";
		JSONObject res;
		JSONObject obj = new JSONObject();
		try {
			res = HttpUtility.sendPost(host, obj.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
		
	}*/
	
	
	
	
	public JSONObject capabilities() {
		JSONObject ob = new JSONObject("{\"capabilities\": { \"alwaysMatch\": {\"platformName\": \"windows\" },\"firstMatch\": [{\"browserName\": \"chrome\"}]}}");
		
			
		return ob;
	}
	
	
	public boolean chromedriver() {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] {"C:\\Program Files (x86)\\Simplify3x\\\\SimplifyQA\\libs\\drivers\\chromedriver.exe", String.valueOf(port)});
			Thread.sleep(500);
			builder.redirectErrorStream(true);
			Process process = builder.start();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return true;
	}
	
	public boolean newsession(String hostAddrss) {
		try {
			this.hostAddrss = hostAddrss;
			headers.put("content-type" , "application/json");
			JSONObject res = capabilities();
			HttpUtility utility = new HttpUtility();
			res = new JSONObject(utility.SendPost(hostAddrss + "/session",res.toString(), headers));
			sessionId = res.getJSONObject("value").getString("sessionId");
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean launchurl(String url) {
		JSONObject jsonobject = new JSONObject();
		jsonobject.put("url", url);
		String host = hostAddrss + "/session" + "/" + sessionId + "/url";
		headers.put("content-type", "application/json");
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, jsonobject.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean findelement(String using, String value) {
		
		String host = hostAddrss + "/session/" + sessionId + "/element";
		JSONObject obj = new JSONObject();
		obj.put("using", using);
		obj.put("value", value);
		try {
			JSONObject res = HttpUtility.sendPost(host, obj.toString(), headers);
			JSONObject element = res.getJSONObject("value");
			Iterator<String> keys = element.keys();
			String key = null;
			while( keys.hasNext()) {
				key = keys.next();
			}
			elementId = element.getString(key);
			}catch(Exception e) {
				e.printStackTrace();
			}
		return true;
		
	}
	
	public boolean Entertext(String valuetoenter) {
		try {
			String host = hostAddrss + "/session/" + sessionId + "/element/" + elementId +"/value";
			JSONObject obj = new JSONObject();
			obj.put("text", valuetoenter);
			JSONObject res;
			res = HttpUtility.sendPost(host, obj.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean enterpassword(String valuetoenter) {
		try {
			this.elementId = elementId;
			String host = hostAddrss + "/session/" + sessionId +"/element/" + elementId + "/value";
			JSONObject obj = new JSONObject();
			obj.put("text", valuetoenter);
			JSONObject res;
			res = HttpUtility.sendPost(host, obj.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean login() {
		try {
			String host = hostAddrss +"/session/" + sessionId + "/element/" + elementId + "/click";
			JSONObject res;
			JSONObject obj = new JSONObject();
			res = HttpUtility.sendPost(host, obj.toString(), headers);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
		
		
	}
	 
}
