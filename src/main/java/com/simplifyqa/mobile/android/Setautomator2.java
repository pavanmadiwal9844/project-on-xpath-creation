package com.simplifyqa.mobile.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.Future;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.AgentStart;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.handler.SetautomaterService;
import com.simplifyqa.mobile.ios.Idevice;

public class Setautomator2 {
	private String c = SystemUtils.IS_OS_WINDOWS ? "chromedriver.exe" : "chromedriverMac";
	private ArrayList<String> Applist = new ArrayList<String>();
	private String appVersion = "0.60.1";
	private IDevice target;
	private String hostAddress = "127.0.0.1";
	private static String emulatorPathMac = "/Users/%s/Library/Android/sdk/emulator/emulator";
	private static String emulatorPathWin = "C:\\Users\\%s\\AppData\\Local\\Android\\Sdk\\emulator\\emulator.exe";
	private ProcessBuilder processBuilder;
	private File chdriver = new File(UserDir.driver(), c);

	private static final Logger logger = LoggerFactory.getLogger(Setautomator2.class);

	private Path Testapp() {
		return Paths.get(UserDir.setting().getAbsolutePath(), new String[] { "androidTest.apk" });
	}

	private Path Serverapp() {
		return Paths.get(UserDir.setting().getAbsolutePath(), new String[] { "androidServer.apk" });
	}

	private static String EmulatorPath() throws IOException {
		if (SystemUtils.IS_OS_MAC) {
			return getMacEmulatorPath();
		} else {
			return getWinEmulatoPath();
		}
	}

	private Path Settingapp() {
		return Paths.get(UserDir.setting().getAbsolutePath(), new String[] { "settings_apk-debug.apk" });
	}

	private Path dummyapp() {
		return Paths.get(UserDir.setting().getAbsolutePath(), new String[] { "SQAtester.apk" });
	}

	private Path applistapp() {
		return Paths.get(UserDir.setting().getAbsolutePath(), new String[] { "app-debug.apk" });
	}

	private void Applist()
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		this.target.executeShellCommand("pm list packages -f -e", (IShellOutputReceiver) new LineRecevier(Applist));
	}

	private static String getMacEmulatorPath() throws IOException {
		ProcessBuilder builder = new ProcessBuilder(new String[] { "dscl", ".", "list", "/Users" });
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream is = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = null;
		while ((line = reader.readLine()) != null) {
			if (new File(String.format(emulatorPathMac, line)).exists()) {
				return String.format(emulatorPathMac, line);
			}
		}
		return null;
	}

	public static String getWinEmulatoPath() throws IOException {
		boolean flag = false;
		File file = null;
		ArrayList<String> list = new ArrayList<>();
		ProcessBuilder builder = new ProcessBuilder(new String[] { "net", "users" });
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream is = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = null;
		while ((line = reader.readLine()) != null) {
			line = line.replaceAll("\\s+", " ");
			if (line.contains("completed successfully"))
				flag = false;
			if (flag) {
				if (line.split(" ").length == 3)
					file = new File(String.format(emulatorPathWin, line.split(" ")[1]));
				else
					file = new File(String.format(emulatorPathWin, line.split(" ")[0]));
				if (file.exists()) {
					return file.getAbsolutePath();
				}
			}
			if (line.contains("DefaultAccount"))
				flag = true;
		}
		return null;
	}

	private Path serverpath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { SystemUtils.IS_OS_WINDOWS ? "server.exe" : "server" });
	}

	public static void list_emulators() throws IOException {
		String emulator_path = EmulatorPath();
		if (emulator_path != null) {
			ProcessBuilder builder = new ProcessBuilder(new String[] { emulator_path, "-list-avds" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				DeviceManager.emulators.put(line, false);
			}
		}
	}

	public static boolean launch_emulator(String name) throws IOException, InterruptedException {
		String emulator_path = EmulatorPath();
		if (emulator_path != null) {
			DeviceManager.getInstance().destory();
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { emulator_path, "-avd", name, "-no-boot-anim", "-no-window" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("boot completed")) {
					System.out.println("Boot Completed!!!");
					Thread.sleep(1000);
					AgentStart.startDeviceManager();
					Thread.sleep(5000);
					return true;
				}
				if (line.contains("Running multiple emulators with the same AVD"))
					return true;
			}
		}
		return false;

	}

	private Boolean Checkapp(String appName) {
		try {
			for (String name : Applist) {
				if (name.contains(appName)) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("Failed to determine app {} is installed ", appName);
		}
		return false;
	}

	private String getVersion(String param)
			throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
		String version;
		param = String.format("dumpsys package %s | grep versionName", new Object[] { param });
		ArrayList<String> arrayList = new ArrayList<String>();
		this.target.executeShellCommand(param, (IShellOutputReceiver) new LineRecevier(arrayList));
		if (arrayList.isEmpty())
			return null;
		if ((version = ((String) arrayList.get(0)).replace("versionName=", "").replace(",", ""))
				.equalsIgnoreCase("null"))
			version = "N/A";
		return version;
	}

	public static Boolean killEmulator() {
		try {
			if (SystemUtils.IS_OS_WINDOWS) {
				String cmd = "tasklist";
				Runtime run = Runtime.getRuntime();
				Process pr = run.exec(cmd);
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					if (line.contains("qemu-system")) {
						System.out.println(line.split(" ")[0]);
						ProcessBuilder builder = new ProcessBuilder(
								new String[] { "Taskkill", "/IM", line.split(" ")[0], "/F" });
						builder.redirectErrorStream(true);
						Process p = builder.start();
						p.waitFor();
					}
				}
			} else {
				String cmd = "ps -x";
				Runtime run = Runtime.getRuntime();
				Process pr = run.exec(cmd);
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					if (line.contains("qemu-system")) {
						if (line.split(" ")[0] != "") {
							Idevice.killProcess(line.split(" ")[0]);
							continue;
						}
						Idevice.killProcess(line.split(" ")[1]);
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unused")
	public String init(IDevice target, Boolean playback) throws InstallException, IOException, TimeoutException,
			AdbCommandRejectedException, ShellCommandUnresponsiveException {

		this.target = target;
		this.Applist();
		String e = "";
		Boolean uninstall = false;
		Boolean checkApps = ((Checkapp("io.appium.settings")) && (Checkapp("io.appium.uiautomator2.server"))
				&& (Checkapp("io.appium.uiautomator2.server.test")));
		String version = getVersion("io.appium.uiautomator2.server");
		if (checkApps) {
			if (!this.appVersion.equalsIgnoreCase(version)) {
				logger.info("Preparing to install UiAutomator Server");
				uninstall = true;
				this.target.uninstallPackage("io.appium.settings");
				logger.info("uninstall io.appium.settings");
				this.target.uninstallPackage("io.appium.uiautomator2.server");
				logger.info("uninstall io.appium.uiautomator2.server");
				this.target.uninstallPackage("io.appium.uiautomator2.server.test");
				logger.info("uninstall io.appium.uiautomator2.server.test");
			}
		} else {
			logger.info("Device doesn't have UiAutomator Server installed");
		}

		if (!checkApps || uninstall) {
			this.target.installPackage(this.Settingapp().toString(), true);
			logger.info("installed io.appium.settings");
			this.target.installPackage(this.Serverapp().toString(), true);
			logger.info("installed io.appium.uiautomator2.server");
			this.target.installPackage(this.Testapp().toString(), true);
			logger.info("installed io.appium.uiautomator2.server.test");
		}

		if (!playback) {
			if (!(Checkapp("com.example.nettyhttpserver"))) {
				this.target.installPackage(this.applistapp().toString(), true);
				logger.info("installed com.example.nettyhttpserver");
			}
			if (!(Checkapp("com.sqa.sqatester"))) {
				this.target.installPackage(this.dummyapp().toString(), true);
				logger.info("installed com.sqa.sqatester");
			}
		}

		int n;
		try {
			final ServerSocket serverSocket;
			final int localPort = (serverSocket = new ServerSocket(0)).getLocalPort();
			serverSocket.close();
			n = localPort;
		} catch (IOException ex) {
			logger.error("Failed to find an open port for Appium server: {}", (Object) ex.getMessage());
			return null;
		}
		e = String.format("http://%s:%d/wd/hub", hostAddress, n);
		Future<?> future = SetautomaterService.g().e().submit(() -> {
			(processBuilder = new ProcessBuilder(new String[] { this.serverpath().toString(), "--address", hostAddress,
					"--port", Integer.toString(n), "--chromedriver-executable", chdriver.getAbsolutePath(),
					"--session-override" })).environment().put("ANDROID_HOME", UserDir.getadbPath().getAbsolutePath());
			processBuilder.environment().put("JAVA_HOME", System.getProperty("java.home"));
			if (SystemUtils.IS_OS_WINDOWS) {
				processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File("Appium.log")));
				processBuilder.redirectError(ProcessBuilder.Redirect.to(new File("Appium.log")));
			} else {
				processBuilder.redirectOutput(ProcessBuilder.Redirect.to(new File("/tmp/Appium.log")));
				processBuilder.redirectError(ProcessBuilder.Redirect.to(new File("/tmp/Appium.log")));
			}
			processBuilder.directory(new File(UserDir.getAapt().getAbsolutePath()));
			try {
				GlobalDetail.processList.add(processBuilder.start());
				logger.info("Appium Started");
				return;
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		});
		return e;
	}

	public static void main(String args[]) {
		killEmulator();
	}

}
