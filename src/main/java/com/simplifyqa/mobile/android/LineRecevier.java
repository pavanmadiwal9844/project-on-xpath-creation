package com.simplifyqa.mobile.android;

import java.util.Arrays;
import java.util.List;

import com.android.ddmlib.MultiLineReceiver;

public class LineRecevier extends MultiLineReceiver {
	
	private List<String> line;
	public LineRecevier(List<String> line) {
		this.line=line;
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

	@Override
	public void processNewLines(String[] arg0) {
		
		 this.line.addAll(Arrays.asList(arg0));
	}

}
