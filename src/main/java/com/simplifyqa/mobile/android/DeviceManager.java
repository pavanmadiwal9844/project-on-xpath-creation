package com.simplifyqa.mobile.android;

import java.util.HashMap;
import java.util.Map;


import com.android.ddmlib.IDevice;

public class DeviceManager {

	
	 
    private static DeviceManager INSTANCE = null;
 
  
    private AndroidDebugBridgeWrapper androidDebugBridgeWrapper;
    public static Map<String, IDevice> devices= new HashMap<String,IDevice>();
    public static Map<String, Boolean> emulators= new HashMap<String,Boolean>();
    private DeviceChangeListener deviceChangeListener;
 
    private DeviceManager()
    {
 
    }
 
   
    public static DeviceManager getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new DeviceManager();
        }
 
        return INSTANCE;
    }

    public void start()
    {
        androidDebugBridgeWrapper = new AndroidDebugBridgeWrapper();
        deviceChangeListener = new DeviceChangeListener();
 
        androidDebugBridgeWrapper.addDeviceChangeListener(deviceChangeListener);
        androidDebugBridgeWrapper.init(false);
 
        System.out.println("Device manager start successful.");
    }
 
    public void destory()
    {
        if (androidDebugBridgeWrapper == null)
        {
            return;
        }
 
        androidDebugBridgeWrapper.removeDeviceChangeListener(deviceChangeListener);
        androidDebugBridgeWrapper.terminate();
    }


}
