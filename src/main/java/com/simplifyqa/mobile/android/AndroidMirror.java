package com.simplifyqa.mobile.android;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.websocket.EncodeException;

import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;

public class AndroidMirror {
	private Logger LOG = LoggerFactory.getLogger(AndroidMirror.class);
	public static final String ABIS_ARM64_V8A = "arm64-v8a";
	public static final String ABIS_ARMEABI_V7A = "armeabi-v7a";
	public static final String ABIS_X86 = "x86";
	public static final String ABIS_X86_64 = "x86_64";

	private Queue<byte[]> dataQueue = new ConcurrentLinkedQueue<byte[]>();

	private Banner banner = new Banner();
	private int PORT = 1717;
	private IDevice device;
	private String REMOTE_PATH = "/data/local/tmp";
	private String ABI_COMMAND = "ro.product.cpu.abi";
	private String MINICAP_BIN = "libcompress.so";
	private String MINICAP_SO = "libturbojpeg.so";
	private String MINICAP_CHMOD_COMMAND = "chmod 777 %s/%s";
	private String MINICAP_START_COMMAND = "ANDROID_DATA=/data/local/tmp LD_LIBRARY_PATH=%s:/data/local/tmp CLASSPATH=/data/local/tmp/scrcpy-server app_process / com.genymobile.scrcpy.Server -r 24 -Q 60 -P %s ";
	private String LOCALPATH = "ANDROID_DATA=/data/local/tmp CLASSPATH=/data/local/tmp/scrcpy-server app_process / com.genymobile.scrcpy.Server -L";
	private boolean isRunning = false;
	private String size;
	private int virtualSize;

	public AndroidMirror(JSONObject object) {
		this.device = DeviceManager.devices.get(object.getString("id"));
		this.size = object.getString("size");
		String[] SizeArray = size.split("x");
		int width = Integer.parseInt(SizeArray[0].trim());
		if (width > 1000) {
			virtualSize = (width / 4);
			System.out.println(virtualSize);
		} else {
			virtualSize = (width / 3);
		}
		try {
			init();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void init() throws InterruptedException {

		String abi = device.getProperty(ABI_COMMAND);
		File minicapBinFile = new File(
				Paths.get(UserDir.getCompress().getAbsolutePath(), new String[] { abi, MINICAP_BIN }).toUri());
		File minicapSoFile = new File(
				Paths.get(UserDir.getLibjpeg().getAbsolutePath(), new String[] { abi, MINICAP_SO }).toUri());
		File adbpath = null;
		try {
			if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb");
			} else if (SystemUtils.IS_OS_WINDOWS) {
				adbpath = new File(UserDir.getadbPath(), "platform-tools" + File.separator + "adb.exe");
			}
			Process p1, p2, p3;
			p1 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "-s",
					this.device.getSerialNumber(), "push", minicapBinFile.getAbsolutePath(), REMOTE_PATH + "/" });
			p1.waitFor();
			InputStream stdIn = p1.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				System.out.println(line);
			p1.destroyForcibly();
			p2 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "-s",
					this.device.getSerialNumber(), "push", minicapSoFile.getAbsolutePath(), REMOTE_PATH + "/" });
			p2.waitFor();
			stdIn = p2.getInputStream();
			isr = new InputStreamReader(stdIn);
			br = new BufferedReader(isr);
			line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				System.out.println(line);
			p2.destroyForcibly();
			System.out.println(UserDir.getscrcpyserver().getAbsolutePath());
			System.out.println(UserDir.getscrcpyserver().getAbsolutePath());
			p3 = Runtime.getRuntime()
					.exec(new String[] { adbpath.getAbsolutePath(), "-s", this.device.getSerialNumber(), "push",
							UserDir.getscrcpyserver().getAbsolutePath(), REMOTE_PATH + "/scrcpy-server" });
			p3.waitFor();
			stdIn = p3.getInputStream();
			isr = new InputStreamReader(stdIn);
			br = new BufferedReader(isr);
			line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				System.out.println(line);
			p3.destroyForcibly();
			p3 = Runtime.getRuntime().exec(new String[] { adbpath.getAbsolutePath(), "shell", LOCALPATH });
			p3.waitFor();
			stdIn = p3.getInputStream();
			isr = new InputStreamReader(stdIn);
			br = new BufferedReader(isr);
			line = null;
			line = br.readLine();
			executeShellCommand(String.format(MINICAP_CHMOD_COMMAND, REMOTE_PATH, "/scrcpy-server"));
			ServerSocket serverSocket = null;
			try {
				serverSocket = new ServerSocket(0);
			} catch (IOException ex) {
				LOG.error("Failed to find a free port for minicap", ex);
			}
			PORT = serverSocket.getLocalPort();

			try {
				serverSocket.close();
			} catch (IOException ex2) {
				LOG.error("Failed to close minicap port {}", PORT, ex2);
			}
			device.createForward(PORT, 6612);
			System.out.println(PORT);
			final String startCommand = String.format(MINICAP_START_COMMAND, line, virtualSize);

			new Thread(new Runnable() {
				@Override
				public void run() {

					executeShellCommand(startCommand);
				}
			}).start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AdbCommandRejectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	private String executeShellCommand(String command) {
		CollectingOutputReceiver output = new CollectingOutputReceiver();
		try {
			device.executeShellCommand(command, output, 0);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AdbCommandRejectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ShellCommandUnresponsiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output.getOutput();
	}

	public void startScreenListener() {
		isRunning = true;
		service.g().e().submit(() -> {
			new ImageConverter().converter();
			return;
		});
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		service.g().e().submit(() -> {
			new ImageBinaryFrameCollector().Collector();
			return;
		});
	}

	public void stopScreenListener() {
		isRunning = false;
	}

	private static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
		byte[] byte_3 = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
		System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
		return byte_3;
	}

	private static byte[] subByteArray(byte[] byte1, int start, int end) {
		byte[] byte2 = new byte[end - start];
		System.arraycopy(byte1, start, byte2, 0, end - start);
		return byte2;
	}

	class ImageBinaryFrameCollector {
		@SuppressWarnings("unused")
		private Socket socket, socke;

//		@Override
		public void Collector() {

			// TODO Auto-generated method stub
			InputStream stream = null;
			DataInputStream input = null;
			try {
				socket = new Socket("localhost", PORT);
				socke = new Socket("localhost", PORT);
				stream = socket.getInputStream();
				input = new DataInputStream(stream);
				while (isRunning) {
					byte[] buffer;
					int len = 0;
					while (len == 0) {
						len = input.available();
					}
					buffer = new byte[len];
					input.read(buffer);
					dataQueue.add(buffer);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null && socket.isConnected()) {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (stream != null) {
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}
	}

	class ImageConverter {
		private int readBannerBytes = 0;
		private int bannerLength = 2;
		private int readFrameBytes = 0;
		private int frameBodyLength = 0;
		private byte[] frameBody = new byte[0];

//		@Override
		public void converter() {

			@SuppressWarnings("unused")
			long start = System.currentTimeMillis();
			while (isRunning) {
				byte[] binaryData = dataQueue.poll();
				if (binaryData == null) {
				} else {
					int len = binaryData.length;
					for (int cursor = 0; cursor < len;) {
						int byte10 = binaryData[cursor] & 0xff;
						if (readBannerBytes < bannerLength) {
							cursor = parserBanner(cursor, byte10);
						} else if (readFrameBytes < 4) {

							frameBodyLength += (byte10 << (readFrameBytes * 8)) >>> 0;
							cursor += 1;
							readFrameBytes += 1;
						} else {
							if (len - cursor >= frameBodyLength) {
								byte[] subByte = subByteArray(binaryData, cursor, cursor + frameBodyLength);
								frameBody = byteMerger(frameBody, subByte);

								byte[] finalBytes = subByteArray(frameBody, 0, frameBody.length);
								CompletableFuture.runAsync(() -> {
									try {
										GlobalDetail.websocket.get(device.getSerialNumber()).getBasicRemote()
												.sendObject(finalBytes);
										GlobalDetail.screenshot.put(device.getSerialNumber(), finalBytes);
									} catch (IOException | EncodeException e) {
										e.printStackTrace();
										stopScreenListener();
									}
									return;
								});
								long current = System.currentTimeMillis();
								start = current;
								cursor += frameBodyLength;
								frameBodyLength = 0;
								readFrameBytes = 0;
								frameBody = new byte[0];
							} else {
								byte[] subByte = subByteArray(binaryData, cursor, len);
								frameBody = byteMerger(frameBody, subByte);
								frameBodyLength -= (len - cursor);
								readFrameBytes += (len - cursor);
								cursor = len;
							}
						}
					}
				}
			}

		}

		private int parserBanner(int cursor, int byte10) {
			switch (readBannerBytes) {
			case 0:
				// version
				banner.setVersion(byte10);
				break;
			case 1:
				// length
				bannerLength = byte10;
				banner.setLength(byte10);
				break;
			case 2:
			case 3:
			case 4:
			case 5:
				// pid
				int pid = banner.getPid();
				pid += (byte10 << ((readBannerBytes - 2) * 8)) >>> 0;
				banner.setPid(pid);
				break;
			case 6:
			case 7:
			case 8:
			case 9:
				// real width
				int realWidth = banner.getReadWidth();
				realWidth += (byte10 << ((readBannerBytes - 6) * 8)) >>> 0;
				banner.setReadWidth(realWidth);
				break;
			case 10:
			case 11:
			case 12:
			case 13:
				// real height
				int realHeight = banner.getReadHeight();
				realHeight += (byte10 << ((readBannerBytes - 10) * 8)) >>> 0;
				banner.setReadHeight(realHeight);
				break;
			case 14:
			case 15:
			case 16:
			case 17:
				// virtual width
				int virtualWidth = banner.getVirtualWidth();
				virtualWidth += (byte10 << ((readBannerBytes - 14) * 8)) >>> 0;
				banner.setVirtualWidth(virtualWidth);

				break;
			case 18:
			case 19:
			case 20:
			case 21:
				// virtual height
				int virtualHeight = banner.getVirtualHeight();
				virtualHeight += (byte10 << ((readBannerBytes - 18) * 8)) >>> 0;
				banner.setVirtualHeight(virtualHeight);
				break;
			case 22:
				// orientation
				banner.setOrientation(byte10 * 90);
				break;
			case 23:
				// quirks
				banner.setQuirks(byte10);
				break;
			}

			cursor += 1;
			readBannerBytes += 1;

			if (readBannerBytes == bannerLength) {
				LOG.debug(banner.toString());
			}
			return cursor;
		}
	}
}
