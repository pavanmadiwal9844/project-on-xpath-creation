package com.simplifyqa.mobile.android;

import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.Utility.HttpUtility;

public class PackageDetail {

	private IDevice target;
	private JSONArray packobject = null;

	public JSONArray init() throws IOException, InterruptedException, InstallException {

		@SuppressWarnings("unused")
		Future<Boolean> future = service.g().e().submit(() -> {
			try {
				CollectingOutputReceiver output = new CollectingOutputReceiver();
				target.executeShellCommand("am start -n com.example.nettyhttpserver/.MainActivity", output, 0L,
						TimeUnit.SECONDS);
				JSONObject v = new JSONObject(
						HttpUtility.Get(DeviceChangeListener.localPort.get(target.getSerialNumber())));
				this.packobject = new JSONArray(v.getString("value"));
				return true;
			} catch (AdbCommandRejectedException | com.android.ddmlib.ShellCommandUnresponsiveException
					| IOException adbCommandRejectedException) {

				return false;
			} catch (TimeoutException paramIDevice) {

				return false;
			}
		});
		try {
			if ((future.get()) && (!packobject.isEmpty())) {
				return packobject;
			} else {
				return new JSONArray();
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new JSONArray();
		}

	}

	public PackageDetail(IDevice device) {
		this.target = device;
	}

}
