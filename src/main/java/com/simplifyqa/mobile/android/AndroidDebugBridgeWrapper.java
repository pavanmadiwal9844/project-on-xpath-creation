package com.simplifyqa.mobile.android;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.simplifyqa.Utility.UserDir;

public class AndroidDebugBridgeWrapper {

	

    private static File sdk=UserDir.getadbPath();
	public static IDevice[] devices;
	public static File adbPath;
 
    public AndroidDebugBridgeWrapper()
    {
    }
 
    public void init(boolean clientSupport)
    {
    	AndroidDebugBridge.init(clientSupport);
    	if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_LINUX) {
    		adbPath = FileUtils.getFile(sdk, "platform-tools", "adb");
        }
    	 else if (SystemUtils.IS_OS_WINDOWS) {
    		 adbPath = FileUtils.getFile(sdk, "platform-tools", "adb.exe");
         }
    	
    	AndroidDebugBridge.createBridge(adbPath.getAbsolutePath(), true);
      
    }
    

    public void addDeviceChangeListener(AndroidDebugBridge.IDeviceChangeListener listener)
    {
        AndroidDebugBridge.addDeviceChangeListener(listener);
    }
 
   
    public void removeDeviceChangeListener(AndroidDebugBridge.IDeviceChangeListener listener)
    {
        AndroidDebugBridge.removeDeviceChangeListener(listener);
    }
 
    public void terminate()
    {
        AndroidDebugBridge.terminate();
    }
 
    public void disconnectBridge()
    {
        AndroidDebugBridge.disconnectBridge();
    }

}