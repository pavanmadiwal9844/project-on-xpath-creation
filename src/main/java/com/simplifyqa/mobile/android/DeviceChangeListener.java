package com.simplifyqa.mobile.android;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.TimeoutException;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.jenkinsocket;

public class DeviceChangeListener implements AndroidDebugBridge.IDeviceChangeListener {
	static HashMap<String, Integer> localPort = new HashMap<String, Integer>();

	@Override
	public void deviceConnected(IDevice device) {
		System.out.println("Device connect " + device.getSerialNumber());
		if (GlobalDetail.notificationSession != null)
			GlobalDetail.notificationSession.getAsyncRemote()
					.sendObject(new JSONObject().put("Connect", device.getSerialNumber()).toString());

		DeviceManager.devices.put(device.getSerialNumber(), device);
		if (jenkinsocket.socket != null)
			jenkinsocket.socket.emit("mobileconnected",
					new JSONObject().put("deviceId", device.getSerialNumber()).put("agentId", jenkinsocket.socketUdid));

		try {
			final ServerSocket serverSocket = new ServerSocket(0);
			Throwable t = null;

			try {
				localPort.put(device.getSerialNumber(), serverSocket.getLocalPort());
				final Object[] array = { serverSocket.getLocalPort(), 6790, device.getSerialNumber() };
				serverSocket.close();
			}

			finally {
				if (t != null) {
					try {
						serverSocket.close();
					} catch (Throwable exception) {
						t.addSuppressed(exception);
					}
				} else {
					serverSocket.close();
				}
			}
			device.createForward(localPort.get(device.getSerialNumber()), 8081);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AdbCommandRejectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new AndroidDebugBridgeWrapper().terminate();
			new AndroidDebugBridgeWrapper().init(false);
		}
	}

	@Override
	public void deviceDisconnected(IDevice device) {
		System.out.println("Device disconnect " + device.getSerialNumber());
		if (GlobalDetail.notificationSession != null)
			GlobalDetail.notificationSession.getAsyncRemote()
					.sendText(new JSONObject().put("Disconnect", device.getSerialNumber()).toString());
		DeviceManager.devices.remove(device.getSerialNumber());
		if (jenkinsocket.socket != null)
			jenkinsocket.socket.emit("mobiledisconneced",
					new JSONObject().put("deviceId", device.getSerialNumber()).put("agentId", jenkinsocket.socketUdid));
	}

	@Override
	public void deviceChanged(IDevice device, int changeMask) {
		if (device.isOnline()) {
			System.out.println("Device change online " + device.getSerialNumber());
		} else {
			System.out.println("Device change offline " + device.getSerialNumber());
		}
	}

}
