

package com.simplifyqa.mobile.ios;

import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import java.util.Iterator;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.converters.Converter;

public final class convert implements Converter
{
    public final boolean canConvert(final Class clazz) {
        return Hierarchy.class.isAssignableFrom(clazz);
    }
    
    public final void marshal(final Object o, final HierarchicalStreamWriter hierarchicalStreamWriter, final MarshallingContext marshallingContext) {
        final Iterator<xml> iterator = ((Hierarchy)o).iterator();
        while (iterator.hasNext()) {
            this.a(hierarchicalStreamWriter, marshallingContext, iterator.next());
        }
    }
    
    private void a(final HierarchicalStreamWriter hierarchicalStreamWriter, final MarshallingContext marshallingContext, final xml b) {
        hierarchicalStreamWriter.startNode(b.b());
        if (b.c() != null) {
            hierarchicalStreamWriter.addAttribute("name", b.c());
        }
        hierarchicalStreamWriter.addAttribute("x", Integer.toString(b.d()));
        hierarchicalStreamWriter.addAttribute("y", Integer.toString(b.e()));
        hierarchicalStreamWriter.addAttribute("width", Integer.toString(b.f()));
        hierarchicalStreamWriter.addAttribute("height", Integer.toString(b.g()));
        if (b.h() != null) {
            hierarchicalStreamWriter.addAttribute("label", b.h());
        }
        if (b.i() != null) {
            hierarchicalStreamWriter.addAttribute("value", b.i());
        }
        if (b.k() != null && !b.k().isEmpty()) {
            final Iterator<xml> iterator = b.k().iterator();
            while (iterator.hasNext()) {
                this.a(hierarchicalStreamWriter, marshallingContext, iterator.next());
            }
        }
        hierarchicalStreamWriter.endNode();
    }
    
    public final Object unmarshal(final HierarchicalStreamReader hierarchicalStreamReader, final UnmarshallingContext unmarshallingContext) {
        return null;
    }
}
