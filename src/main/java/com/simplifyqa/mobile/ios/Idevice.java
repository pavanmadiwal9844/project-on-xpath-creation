package com.simplifyqa.mobile.ios;

import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.Utility.UserDir;
import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.grpc.grpc;
import com.simplifyqa.web.implementation.Automatic_Downloader;

import idb.Idb;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Idevice {
	private static Logger iosLog = LoggerFactory.getLogger(Idevice.class);

	private static String relayDevice() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner.exe" }).toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
				.toString();
	}

	private static String idb_companion() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "idb", "idb_companion" })
				.toString();
	}

	public static String webdriveragent() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "appium-webdriveragent" })
				.toString();
	}

	private static String infoDevice() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner.exe" }).toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
				.toString();
	}

	private static String xctestDevice() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner.exe" }).toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
				.toString();
	}

	public static String runnerListDevices() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner.exe" }).toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "runner", "runner" })
				.toString();
	}

	public static String mountImage() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths
					.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceimagemounter.exe" })
					.toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceimagemounter" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceimagemounter" })
				.toString();
	}

	private static String imagePath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "Images" }).toString();
	}

	private static String killAppiumserver() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "exit.sh" }).toString();
	}

	private static String installerDevice() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller.exe" })
					.toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller" })
				.toString();
	}

	private static String pairDevice() {
		if (SystemUtils.IS_OS_WINDOWS)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "idevicepair.exe" })
					.toString();
		if (SystemUtils.IS_OS_MAC)
			return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "idevicepair" })
					.toString();
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "idevicepair" }).toString();
	}

	private static String getLibZip() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "lib" }).toString();
	}

	private static String getMountImages(String version) {
		return Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { "Ios_tool", "Images", version, "DeveloperDiskImage.dmg" }).toString();
	}

	public static String getIdbComapnion() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "idb", "idb_companion" })
				.toString();
	}

	private static String getMountSignature(String version) {
		return Paths.get(UserDir.getAapt().getAbsolutePath(),
				new String[] { "Ios_tool", "Images", version, "DeveloperDiskImage.dmg.signature" }).toString();
	}

	public static boolean test(String udid) {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { xctestDevice(), "-u", udid.toString(), "xctest",
					"-B", "com.facebook.WebDriverAgentRunner.xctrunner" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("mount the Developer disk image"))
					return true;
				if (line.contains("DeveloperImage mounted successfully"))
					return false;
				if (line.contains("WebDriverAgent start successfully"))
					return false;
				if (line.contains("Download"))
					return true;
			}
			return true;
		} catch (Exception e) {
			iosLog.error("Unable to get deivces");
			return false;
		}
	}

	public static void getDeivces() {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { runnerListDevices(), "watch" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			IdeviceStreamRedirector outStreamRedirector = new IdeviceStreamRedirector(process.getInputStream(),
					System.out);
			IdeviceStreamRedirector errStreamRedirector = new IdeviceStreamRedirector(process.getErrorStream(),
					System.err);
			outStreamRedirector.start();
			errStreamRedirector.start();
		} catch (Exception e) {
			iosLog.error("Unable to get deivces");
		}
	}

	public static String iosVersion(String udid) throws IOException {
		List<String> info = new ArrayList<>();
		String iosversion = "";
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { infoDevice(), "-u", udid.toString(), "info" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.equals(""))
					info.add(line);
			}
			if (!info.isEmpty()) {
				for (int i = 0; i < info.size(); i++) {
					if (((String) info.get(i)).startsWith("ProductVersion")) {
						iosversion = ((String) info.get(i)).split(": ")[1].trim();
						break;
					}
				}
				process.destroyForcibly();

				if (iosversion.isEmpty()) {
					builder = new ProcessBuilder(
							new String[] { infoDevice(), "-u", udid.toString(), "info", "-k", "ProductVersion" });
					builder.redirectErrorStream(true);
					process = builder.start();
					is = process.getInputStream();
					reader = new BufferedReader(new InputStreamReader(is));
					line = null;
					while ((line = reader.readLine()) != null) {
						if (!line.equals(""))
							iosversion = line;
					}
					iosversion = iosversion.replaceAll("'", "");
				}
				return iosversion;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static String deviceinfo(String udid) throws IOException {
		List<String> info = new ArrayList<>();
		String name = "";
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { infoDevice(), "-u", udid.toString(), "info" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.equals(""))
					info.add(line);
			}
			if (!info.isEmpty()) {
				for (int i = 0; i < info.size(); i++) {
					if (((String) info.get(i)).startsWith("DeviceName")) {
						name = ((String) info.get(i)).split(": ")[1].trim();
						break;
					}
				}
			}
			if (name.isEmpty()) {
				builder = new ProcessBuilder(
						new String[] { infoDevice(), "-u", udid.toString(), "info", "-k", "DeviceName" });
				builder.redirectErrorStream(true);
				process = builder.start();
				is = process.getInputStream();
				reader = new BufferedReader(new InputStreamReader(is));
				line = null;
				while ((line = reader.readLine()) != null) {
					if (!line.equals(""))
						name = line;
				}
				name = name.replaceAll("'", "");
			}
			return name;
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean validatePair(String udid) throws IOException {
		try {
			boolean flag = false;
			Boolean error = Boolean.valueOf(false);
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { pairDevice(), "-u", udid.toString(), "validate" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("SUCCESS:")) {
					flag = true;
					break;
				}
				if (line.contains("ERROR:")) {
					error = Boolean.valueOf(true);
					break;
				}
			}
			reader.close();
			if (flag)
				return true;
			if (error.booleanValue())
				return pairDevice(udid);
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean getSimulators() {
		try {
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { idb_companion(), "--list", "1", "--only", "simulator" });
			JSONObject list = null;
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("{")) {
					list = new JSONObject(line);
					if (list.getString("os_version").split(" ")[0].equalsIgnoreCase("ios"))
						ideviceMaganger.devices.put(list.getString("udid"),
								(new JSONObject()).put("iosVersion", list.getString("os_version").split(" ")[1])
										.put("name", list.getString("name")).put("id", list.getString("udid"))
										.put("simulator", "true"));
				}
			}
			reader.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean pairDevice(String udid) throws IOException {
		try {
			boolean flag = false;
			ProcessBuilder builder = new ProcessBuilder(new String[] { pairDevice(), "-u", udid.toString(), "pair" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			process.waitFor();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.contains("SUCCESS:"))
					flag = true;
			}
			reader.close();
			if (flag)
				return false;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean StartWDA(String udid) {
		try {
			if (InitializeDependence.xctestrun.containsKey(udid)
					&& ((Boolean) InitializeDependence.xctestrun.get(udid)).booleanValue())
				return true;
			Boolean runtest = Boolean.valueOf(false);
			ProcessBuilder builder = new ProcessBuilder(new String[] { xctestDevice(), "-u", udid.toString(), "xctest",
					"-B", "com.facebook.WebDriverAgentRunner.xctrunner" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream stdIn = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdIn);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			Boolean xctrunnerquite = Boolean.valueOf(false);
			System.out.println("<OUTPUT>");
			GlobalDetail.Status = Boolean.valueOf(true);
			while ((line = br.readLine()) != null) {
				if (line.contains("quited")) {
					xctrunnerquite = Boolean.valueOf(true);
					break;
				}
				if (line.contains("invalid memory address")) {
					xctrunnerquite = Boolean.valueOf(true);
					break;
				}
				if (line.contains("WebDriverAgent start")) {
					runtest = Boolean.valueOf(true);
					break;
				}
				if (!line.contains("test manager"))
					continue;
				iosLog.info("WDA started");
				runtest = Boolean.valueOf(true);
				break;
			}
			if (xctrunnerquite.booleanValue()) {
				Thread.sleep(2000L);
				return StartWDA(udid);
			}
			if (runtest.booleanValue())
				return true;
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean TestWDA(String udid) throws InterruptedException {
		try {
			Thread.sleep(2000L);
			HttpUtility.udid = udid;
			JSONObject res = HttpUtility.getcall(String.format("http://localhost:%s/%s",
					new Object[] { GlobalDetail.Wdaproxy.get(udid), "status" }));
			if (res.has("Error"))
				return false;
			GlobalDetail.iosSession.put(udid, res.get("sessionId").toString());
			InitializeDependence.ios_device = true;
			return true;
		} catch (Exception e) {
			return TestWDA(udid);
		}
	}

	public static void killProcess(String pid) {
		try {
			String cmd = "kill -9 " + pid;
			Runtime run = Runtime.getRuntime();
			Process pr = run.exec(cmd);
			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null)
				System.out.println(line);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean EndRunnerapp() {
		try {
			if (SystemUtils.IS_OS_WINDOWS) {
				ProcessBuilder builder = new ProcessBuilder(new String[] { "Taskkill", "/IM", "runner.exe", "/F" });
				builder.redirectErrorStream(true);
				Process p = builder.start();
				p.waitFor();
			} else {
				String cmd = "ps -x";
				Runtime run = Runtime.getRuntime();
				Process pr = run.exec(cmd);
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					if (line.contains("libs/Ios_tool/")) {
						if (line.split(" ")[0] != "") {
							killProcess(line.split(" ")[0]);
							continue;
						}
						killProcess(line.split(" ")[1]);
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean EndServer() {
		try {
			Runtime.getRuntime().exec(killAppiumserver(), (String[]) null,
					new File(Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool" }).toString()));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean iproxyWDA(String udid) {
		try {
			ServerSocket serverSocket;
			String wdaproxy = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
			serverSocket.close();
			GlobalDetail.Wdaproxy.put(udid, wdaproxy);
			iosLog.info(wdaproxy);
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { relayDevice(), "-u", udid, "relay", wdaproxy, "8100" });
			builder.redirectErrorStream(true);
			builder.start();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean iproxyWDAScreen(String udid) {
		try {
			ServerSocket serverSocket;
			String screenProxy = String.valueOf((serverSocket = new ServerSocket(0)).getLocalPort());
			serverSocket.close();
			GlobalDetail.Wdascreen.put(udid, screenProxy);
			iosLog.info(screenProxy);
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { relayDevice(), "-u", udid, "relay", screenProxy, "9100" });
			builder.redirectErrorStream(true);
			builder.start();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean mountImage(String udid) {
		if (test(udid)) {
			ProcessBuilder builder = null;
			try {
				String Version = iosVersion(udid).trim();
				if ((Version.split("\\.")).length == 3) {
					Version = Version.substring(0, Version.lastIndexOf('.'));
				} else if (Version.equalsIgnoreCase("12.5")) {
					Version = "12.4";
				} else if (Version.equalsIgnoreCase("14.8")) {
					Version = "14.7";
				}
				if (!(new File(getMountSignature(Version)).exists()))
					downloadImage(Version);
				builder = new ProcessBuilder(
						new String[] { mountImage(), "-u", udid, getMountImages(Version), getMountSignature(Version) });
				builder.redirectErrorStream(true);
				Process process = builder.start();
				InputStream is = process.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (line.contains("Status: Complete"))
						return true;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			return true;
		}
		return false;
	}

	public static boolean downloadImage(String version) {
		try {
			File directory = new File(imagePath());
			if (!directory.isDirectory())
				directory.mkdir();
			HashMap<String, String> header = new HashMap<String, String>();
			header.put("content-type", "application/json");
			JSONObject object = new JSONObject();
			object.put("path", "ios/" + version);
			InputStream file = HttpUtility.getFilePost(Constants.FILES, object.toString(), header);
			FileUtils.copyInputStreamToFile(file, new File(imagePath() + "/image.zip"));
			Automatic_Downloader.unzip(imagePath() + "/image.zip", imagePath());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			new File(imagePath() + "/image.zip").delete();
			FileUtils.deleteDirectory(new File(imagePath() + "/__MACOSX"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean launchXcodeSimulator(String udid) {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { "bash",
					String.valueOf(webdriveragent()) + "/install.sh", udid,
					((JSONObject) ideviceMaganger.devices.get(udid)).getString("iosVersion"), webdriveragent() });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("ServerURLHere->")) {
					String port = line.substring(line.lastIndexOf(":") + 1, line.lastIndexOf("<-ServerURLHere"));
					GlobalDetail.Wdaproxy.put(udid, port);
					GlobalDetail.Wdascreen.put(udid, Integer.toString(Integer.parseInt(port) + 1000));
					break;
				}
			}
			System.out.println("Wdaproxy : " + (String) GlobalDetail.Wdaproxy.get(udid));
			System.out.println("WdaScreen : " + (String) GlobalDetail.Wdascreen.get(udid));
			reader.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean launchIDBSimulator(String udid) {
		try {
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { getIdbComapnion(), "--boot", udid, "--headless", "true" });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("\"state\":\"Booted\""))
					return true;
			}
			reader.close();
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean shutdownSimulator(String udid) {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { getIdbComapnion(), "--shutdown", udid });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("shutdown succeeded")) {
					GlobalDetail.GrpcPort.remove(udid);
					return true;
				}
			}
			reader.close();
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean launchApplication(String udid) {
		if (grpc.launchApp(Integer.parseInt((String) GlobalDetail.GrpcPort.get(udid))).booleanValue())
			return true;
		return false;
	}

	public static List<Bundles> listApps(String udid) {
		List<Bundles> bundles = new ArrayList<>();
		Idb.ListAppsResponse apps = grpc.listApps(Integer.parseInt((String) GlobalDetail.GrpcPort.get(udid)));
		for (int i = 0; i < apps.getAppsCount(); i++) {
			Bundles e = new Bundles();
			e.setBundleId(apps.getApps(i).getBundleId());
			e.setName(apps.getApps(i).getName());
			e.setVersion(" ");
			bundles.add(e);
		}
		return bundles;
	}

	public static boolean startGrpcServer(String udid) {
		try {
			int randomNum = ThreadLocalRandom.current().nextInt(10882, 20000);
			String port = String.valueOf(randomNum);
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { getIdbComapnion(), "--udid", udid, "--grpc-port", port });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("Address already in use"))
					return startGrpcServer(udid);
				if (line.contains("Started GRPC server")) {
					GlobalDetail.GrpcPort.put(udid, port);
					return true;
				}
			}
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean startSession(String udid) {
		try {
			if (((JSONObject) ideviceMaganger.devices.get(udid)).has("simulator")) {
				if (!GlobalDetail.GrpcPort.containsKey(udid))
					if (launchIDBSimulator(udid) && startGrpcServer(udid)) {
						iosLog.info("Simulator Launched!!");
						iosLog.info("GRPC Server Started!!!");
						Thread.sleep(5000L);
					} else {
						return false;
					}
				if (launchXcodeSimulator(udid)) {
					iosLog.info((String) GlobalDetail.Wdaproxy.get(udid));
					iosLog.info((String) GlobalDetail.Wdascreen.get(udid));
					return TestWDA(udid);
				}
				return false;
			} else {
				if (!InitializeDependence.xctestrun.containsKey(udid)) {
					CompletableFuture.runAsync(() -> {
						iproxyWDAScreen(udid);
					});
					CompletableFuture.runAsync(() -> {
						iproxyWDA(udid);
					});
				}
				if (StartWDA(udid) && !InitializeDependence.xctestrun.containsKey(udid)) {
					InitializeDependence.xctestrun.put(udid, Boolean.valueOf(true));
					return TestWDA(udid);
				}
				if (InitializeDependence.xctestrun.containsKey(udid)
						&& ((Boolean) InitializeDependence.xctestrun.get(udid)).booleanValue())
					return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static void linkLibZip() {
		File file = new File("/usr/local/opt/libzip");
		if (!file.exists())
			try {
				Files.createDirectories(file.toPath(), (FileAttribute<?>[]) new FileAttribute[0]);
				ProcessBuilder builder = new ProcessBuilder(new String[] { "ln", "-s", getLibZip(), file.toString() });
				builder.redirectErrorStream(true);
				builder.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public static boolean installIPA(String udid, String path) throws IOException {
		try {
			boolean flag = false;
			if (SystemUtils.IS_OS_MAC)
				linkLibZip();
			ProcessBuilder builder = new ProcessBuilder(
					new String[] { installerDevice(), "-u", udid.toString(), "-i", path });
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream is = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.contains("Complete")) {
					flag = true;
					break;
				}
				if (line.contains("Failed to verify code signature"))
					break;
			}
			reader.close();
			if (flag)
				return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
}
