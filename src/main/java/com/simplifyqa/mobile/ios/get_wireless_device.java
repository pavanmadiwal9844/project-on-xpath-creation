package com.simplifyqa.mobile.ios;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;



public class get_wireless_device {
	private static ArrayList<byte[]> ipbyte = new ArrayList<>();

	public static void get_devices() {
		try {
			final ExecutorService es = Executors.newFixedThreadPool(256);
			InetAddress localhost = InetAddress.getLocalHost();
			InetAddress address;
			byte[] ip = localhost.getAddress();
			final List<Future<Boolean>> futures = new ArrayList<>();
			for (int i = 1; i <= 254; i++) {
				ip[3] = (byte) i;
				address = InetAddress.getByAddress(ip);
				futures.add(portIsOpen(es, address.getAddress(), address.getHostAddress(), 8100, 200));

			}
			es.shutdown();
		} catch (Exception e) {
		}

	}

	public static void list_devices()
			throws ClientProtocolException, UnknownHostException, IOException, InterruptedException {
		while (true) {
			JSONObject value = null;	
			get_devices();
			for (Map.Entry<String, JSONObject> deivcemap : (Iterable<Map.Entry<String, JSONObject>>)ideviceMaganger.devices.entrySet()) {
				if(deivcemap.getValue().has("wireless"))
					ideviceMaganger.devices.remove(deivcemap.getKey());
					
			}
			for (int i = 0; i < ipbyte.size(); i++) {
				value = get_device_info(InetAddress.getByAddress(ipbyte.get(i)).getHostAddress());
				if (value != null)
					ideviceMaganger.devices.put(value.getString("uuid"), (new JSONObject()).put("iosVersion", "")
							.put("name", value.getString("name")).put("id", value.getString("uuid")).put("wireless", "true").put("address",InetAddress.getByAddress(ipbyte.get(i)).getHostAddress()));
			}
			Thread.sleep(5000);
		}

	}

	public static JSONObject get_device_info(String hostaddress) {
		@SuppressWarnings({ "deprecation", "resource" })
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet("http://" + hostaddress + ":8100/wda/device/info");
		getRequest.addHeader("accept", "application/json");
		HttpResponse response;
		try {
			response = httpClient.execute(getRequest);
			HttpEntity httpEntity = response.getEntity();
			JSONObject res = new JSONObject(EntityUtils.toString(httpEntity, "UTF-8"));
			return res.getJSONObject("value");
		} catch (IOException e) {
			return null;
		}
	}

	public static Future<Boolean> portIsOpen(final ExecutorService es, byte[] byteip, final String ip, final int port,
			final int timeout) {
		return es.submit(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				try {
					Socket socket = new Socket();
					socket.connect(new InetSocketAddress(ip, port), timeout);
					socket.close();
					ipbyte.add(byteip);
					return true;
				} catch (Exception ex) {
					return false;
				}
			}
		});
	}
}