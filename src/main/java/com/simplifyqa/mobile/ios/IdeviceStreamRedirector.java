package com.simplifyqa.mobile.ios;

import com.simplifyqa.agent.GlobalDetail;
import com.simplifyqa.agent.jenkinsocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdeviceStreamRedirector extends Thread {
	private InputStream inputStream;

	private OutputStream outputStream;

	private static Logger iosLog = LoggerFactory.getLogger(IdeviceStreamRedirector.class);

	public IdeviceStreamRedirector(InputStream inputStream, OutputStream outputStream) {
		this.inputStream = inputStream;
		this.outputStream = outputStream;
	}

	public void run() {
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.outputStream);
		InputStreamReader inputStreamReader = new InputStreamReader(this.inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String line = null;
		String devicedetail = "";
		try {
			while ((line = bufferedReader.readLine()) != null) {
				if (line.contains("__main__:"))
					line = line.split("__main__:")[1].split("] ")[1];
				devicedetail = String.valueOf(devicedetail) + line;
				if (line.contains("}")) {
					JSONObject device = new JSONObject(devicedetail);
					devicedetail = "";
					if (device.getString("MessageType").equals("Detached")) {
						iosLog.info("Idevice disconnected :{} "
								+ GlobalDetail.DeviceID.get(device.get("DeviceID").toString()));
										
						ideviceMaganger.devices.remove(GlobalDetail.DeviceID.get(device.get("DeviceID").toString()));
						if (jenkinsocket.socket != null)
							jenkinsocket.socket.emit("mobiledisconneced",
									new Object[] { (new JSONObject()).put("deviceId", GlobalDetail.DeviceID.get(device.get("DeviceID").toString()))
											.put("agentId", jenkinsocket.socketUdid) });
					}
					if (device.getString("MessageType").equals("Attached")) {
						Idevice.mountImage(device.getJSONObject("Properties").getString("SerialNumber"));
						iosLog.info("Idevice connected :{} "
								+ device.getJSONObject("Properties").getString("SerialNumber"));
						if (!Idevice.validatePair(device.getJSONObject("Properties").getString("SerialNumber"))) {
							if (Idevice.pairDevice(device.getJSONObject("Properties").getString("SerialNumber")))
								iosLog.info("paired :{}", device.getJSONObject("Properties").getString("SerialNumber"));
						} else {
							iosLog.info("paired :{}", device.getJSONObject("Properties").getString("SerialNumber"));
						}
						ideviceMaganger.devices
								.put(device.getJSONObject("Properties").getString("SerialNumber"),
										(new JSONObject())
												.put("iosVersion",
														Idevice.iosVersion(device.getJSONObject("Properties")
																.getString("SerialNumber")))
												.put("name",
														Idevice.deviceinfo(device.getJSONObject("Properties")
																.getString("SerialNumber")))
												.put("id",
														device.getJSONObject("Properties").getString("SerialNumber")));
						GlobalDetail.DeviceID.put(device.get("DeviceID").toString(), device.getJSONObject("Properties").getString("SerialNumber"));
						if (jenkinsocket.socket != null)
							jenkinsocket.socket
									.emit("mobileconnected",
											new Object[] { (new JSONObject())
													.put("deviceId",
															device.getJSONObject("Properties")
																	.getString("SerialNumber"))
													.put("agentId", jenkinsocket.socketUdid) });
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
