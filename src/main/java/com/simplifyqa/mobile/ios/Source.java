package com.simplifyqa.mobile.ios;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;

public class Source {

	private Hierarchy q;
	private xml p;
	private XStream F;

	public String a(JSONObject asJsonObject) {
		try {
			this.q = new Hierarchy();
			this.p = null;
			(this.F = new XStream()).processAnnotations(xml.class);
			this.F.registerConverter((Converter) new convert());
			this.F.aliasPackage("", this.getClass().getPackage().getName());
			this.F.alias("hierarchy", Hierarchy.class);
			try {
				asJsonObject.has("value");
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
			final String asString = asJsonObject.getString("value");
			final String[] split = asString.split("\\n ");
			for (int i = 0; i < split.length; ++i) {
				final String[] array = split;
				final int n = i;
				final String[] array2 = array;
				boolean b = false;
				Label_0209: {
					if (n + 1 < array2.length) {
						final String s = array2[n];
						final String s2 = array2[n + 1];
						final int n2 = (s.length() - s.replaceAll("^\\s+", "").length()) / 2;
						final int n3 = (s2.length() - s2.replaceAll("^\\s+", "").length()) / 2;
						if (this.p != null && n2 == 0 && n3 > 0) {
							b = true;
							break Label_0209;
						}
					}
					b = false;
				}
				if (!b) {
					final String s3 = split[i];
					final String replaceAll = s3.replaceAll("^\\s+", "");
					final int n4 = (s3.length() - replaceAll.length()) / 2;
					if (this.p == null && replaceAll.startsWith("\u2192Window")) {
						final String substring = replaceAll.substring(1, replaceAll.length());
						this.p = new xml();
						this.q.add(this.p);
						this.a(substring, 0);
					} else if (this.p == null && replaceAll.startsWith("\u2192Other")) {
						final String substring = replaceAll.substring(1, replaceAll.length());
						this.p = new xml();
						this.q.add(this.p);
						this.a(substring, 0);
					} else if (this.p != null && n4 == 0 && !replaceAll.startsWith("\u2192Window")) {
						this.p = null;
					} else if (this.p != null) {
						final xml p = new xml();
						if (this.p.a() < n4) {
							p.a(this.p);
							this.p.k().add(p);
						} else if (this.p.a() >= n4) {
							while (this.p.a() >= n4) {
								this.p = this.p.j();
							}
							p.a(this.p);
							this.p.k().add(p);
						} else if (this.p.j() == null) {
							p.a(this.p);
							this.p.k().add(p);
						} else {
							p.a(this.p.j());
							this.p.j().k().add(p);
						}
						this.p = p;
						this.a(replaceAll, n4);
					}
				}
			}
			return this.F.toXML((Object) this.q);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private void a(final String input, final int n) {
		this.p.a(n);
		final String substring = input.substring(0, input.indexOf(","));
		this.p.a(substring.endsWith("(Main)") ? substring.replace("(Main)", "").trim() : substring);
		final Matcher matcher;
		if ((matcher = Pattern.compile("\\{\\{(\\d+.\\d)+,.(\\d+.\\d)+\\},.\\{(\\d+.\\d)+,.(\\d+.\\d)+\\}\\}")
				.matcher(input)).find()) {
			final String group = matcher.group(1);
			final String group2 = matcher.group(2);
			final String group3 = matcher.group(3);
			final String group4 = matcher.group(4);
			this.p.b(Math.round(Float.parseFloat(group)));
			this.p.c(Math.round(Float.parseFloat(group2)));
			this.p.d(Math.round(Float.parseFloat(group3)));
			this.p.e(Math.round(Float.parseFloat(group4)));
		}
		this.p.c(a(input, "label"));
		this.p.d(a(input, "value"));
		this.p.b(a(input, "identifier"));
		if (StringUtils.isEmpty((CharSequence) this.p.i())) {
			this.p.d(a(input, "placeholderValue"));
		}
	}

	private static String a(final String input, final String str) {
		String group = null;
		final Matcher matcher;
		if ((matcher = Pattern.compile(str + ": ('?)(.*?)(\\1)((?!,\\s)\\n|,\\s|$)").matcher(input)).find()) {
			group = matcher.group(2);
		}
		return group;
	}

}
