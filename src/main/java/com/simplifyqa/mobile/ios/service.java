package com.simplifyqa.mobile.ios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class service {

	private static service g;

	public static service g() {
		if (g == null) {
			g = new service();
		}
		return g;
	}

	private final ExecutorService e = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue() );

	public ExecutorService e() {
		return this.e;
	}

}
