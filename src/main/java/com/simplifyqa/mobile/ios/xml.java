package com.simplifyqa.mobile.ios;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.util.ArrayList;
import java.util.List;

public class xml
{
  @XStreamOmitField
  private int a;
  @XStreamAsAttribute
  private String type;
  @XStreamAsAttribute
  private String name;
  @XStreamAsAttribute
  private int x;
  @XStreamAsAttribute
  private int y;
  @XStreamAsAttribute
  private int width;
  @XStreamAsAttribute
  private int height;
  @XStreamAsAttribute
  private String label;
  @XStreamAsAttribute
  private String value;
  @XStreamOmitField
  private xml j;
  
  public final int a() { return this.a; }


  
  public final void a(int paramInt) { this.a = paramInt; }


  
  public final String b() { return this.type; }


  
  public final void a(String paramString) { this.type = String.format("XCUIElementType%s", new Object[] { paramString }); }


  
  public final String c() { return this.name; }


  
  public final void b(String paramString) { this.name = paramString; }


  
  public final int d() { return this.x; }


  
  public final void b(int paramInt) { this.x = paramInt; }


  
  public final int e() { return this.y; }


  
  public final void c(int paramInt) { this.y = paramInt; }


  
  public final int f() { return this.width; }


  
  public final void d(int paramInt) { this.width = paramInt; }


  
  public final int g() { return this.height; }


  
  public final void e(int paramInt) { this.height = paramInt; }


  
  public final String h() { return this.label; }


  
  public final void c(String paramString) { this.label = paramString; }


  
  public final String i() { return this.value; }


  
  public final void d(String paramString) { this.value = paramString; }


  
  public final xml j() { return this.j; }


  
  public final void a(xml paramb) { this.j = paramb; }


  
  public final List<xml> k() { return this.k; }


  public final void a(boolean paramBoolean) { this.visible = true; }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
@XStreamImplicit
  private List<xml> k = new ArrayList();
  @XStreamAsAttribute
  private boolean visible;
}
