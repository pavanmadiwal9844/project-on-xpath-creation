package com.simplifyqa.mobile.ios;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simplifyqa.Utility.UserDir;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PackageDetail {
  private String target;
  
  private JSONArray packobject = null;
  
  private Logger iosLog = LoggerFactory.getLogger(PackageDetail.class);
  
  private String getIdeviceinstaller() {
    if (SystemUtils.IS_OS_WINDOWS)
      return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller.exe" }).toString(); 
    if (SystemUtils.IS_OS_MAC)
      return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller" }).toString(); 
    return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "Ios_tool", "ideviceinstaller" }).toString();
  }
  
  public String list_all(String udid) throws IOException {
    try {
      List<Bundles> bundles = new ArrayList<>();
      if (((JSONObject)ideviceMaganger.devices.get(udid)).has("simulator")) {
        bundles = Idevice.listApps(udid);
        this.iosLog.info("List of Devices:{}", (new ObjectMapper()).writeValueAsString(bundles));
        return (new ObjectMapper()).writeValueAsString(bundles);
      } 
      if (SystemUtils.IS_OS_MAC)
        Idevice.linkLibZip(); 
      List<String> apps = new ArrayList<>();
      ProcessBuilder builder = new ProcessBuilder(new String[] { getIdeviceinstaller(), "-l", "-o", "list_all" });
      builder.redirectErrorStream(true);
      Process process = builder.start();
      InputStream is = process.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));
      String line = null;
      while ((line = reader.readLine()) != null) {
        if (!line.equals(""))
          apps.add(line); 
      } 
      if (!apps.isEmpty())
        try {
          for (int i = 1; i < apps.size(); i++) {
            String[] app = ((String)apps.get(i)).split(",");
            if (app.length == 3) {
              Bundles e = new Bundles();
              e.setBundleId(app[0].trim());
              e.setVersion(app[1].replace("\"", "").trim());
              e.setName(app[2].replace("\"", "").trim());
              bundles.add(e);
            } 
          } 
          this.iosLog.info("List of Devices:{}", (new ObjectMapper()).writeValueAsString(bundles));
          return (new ObjectMapper()).writeValueAsString(bundles);
        } catch (Exception e) {
          e.printStackTrace();
          return null;
        }  
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    } 
  }
  
  public JSONArray init() throws IOException, InterruptedException {
    System.out.println(this.target);
    this.packobject = new JSONArray(list_all(this.target));
    return this.packobject;
  }
  
  public PackageDetail(String device) {
    this.target = device;
  }
}
