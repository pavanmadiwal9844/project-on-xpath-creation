package com.simplifyqa.mobile.ios;

import com.simplifyqa.method.IosMethod;

public class Timer {
	private Integer timeout = 1;

	public boolean until(IosMethod iosDrover, String name, String value) throws InterruptedException {
		try {
			while (timeout++ < 600) {
				Thread.sleep(100);
				if (iosDrover.FindElement(name, value))
					break;
			}
			if (timeout == 900)
				return false;
			else
				return true;
		} catch (Exception e) {
			return false;
		}
	}

}
