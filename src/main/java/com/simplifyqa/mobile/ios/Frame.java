package com.simplifyqa.mobile.ios;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.agent.GlobalDetail;

import net.sf.jipcam.axis.MjpegFrame;
import net.sf.jipcam.axis.MjpegInputStream;

public class Frame {
	private Logger iosLog = LoggerFactory.getLogger(Frame.class);
	private String idevice;

	public Frame(String ideviceId) {
		this.idevice = ideviceId;
	}

	public void init() {
		URL url = null;
		try {
			if (GlobalDetail.Wdascreen.get(idevice) != null)
				url = new URL(
						String.format("http://localhost:%s", new Object[] { GlobalDetail.Wdascreen.get(idevice) }));
		} catch (MalformedURLException ex) {
			iosLog.error("Invalid mirroring URL from device {}", (Object) ex);
		}
		MjpegInputStream mjpegInputStream;
		try {
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			mjpegInputStream = new MjpegInputStream(connection.getInputStream());
			service.g().e().submit(() -> {
				try {
					MjpegFrame mjpegFrame;
					while ((mjpegFrame = mjpegInputStream.readMjpegFrame()) != null) {

						if (!frameCaptured(mjpegFrame.getJpegBytes()))
							break;
					}
					return;
				} catch (IOException ex3) {
					iosLog.error("Failed to receive frame: {}", (Object) ex3.getMessage());
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
// TODO Auto-generated catch block
						e.printStackTrace();
					}
					new Frame(idevice).init();

				}
			});
		} catch (Exception e) {
			if (!(ideviceMaganger.devices.get(this.idevice)).has("simulator")) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				new Frame(idevice).init();
			}
		}

	}

	public Boolean frameCaptured(final byte[] array) {
		final ByteBuffer wrap = ByteBuffer.wrap(array);
		try {
// GlobalDetail.websocket.get(Idevice).getSession().getRemote().sendBytesByFuture(wrap);
			GlobalDetail.screenshot.put(idevice, array);
			if (!GlobalDetail.websocket.isEmpty())
				GlobalDetail.websocket.get(idevice).getBasicRemote().sendObject(wrap);
			return true;
		} catch (Exception ex) {
// new Frame(idevice).init();
			return false;
		}
	}
}