package com.simplifyqa.grpc;

import idb.CompanionServiceGrpc;
import idb.Idb;
import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.util.concurrent.CountDownLatch;

public class grpc {
  public static Idb.ListAppsResponse listApps(int port) {
    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build();
    CompanionServiceGrpc.CompanionServiceBlockingStub stub = CompanionServiceGrpc.newBlockingStub((Channel)channel);
    Idb.ListAppsRequest request = Idb.ListAppsRequest.newBuilder().setSuppressProcessState(false).build();
    Idb.ListAppsResponse response = stub.listApps(request);
    return response;
  }
  
  public static Boolean launchApp(int port) {
    try {
      ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build();
      CompanionServiceGrpc.CompanionServiceStub stub = CompanionServiceGrpc.newStub((Channel)channel);
      Idb.LaunchRequest.Start s = Idb.LaunchRequest.Start.newBuilder().setBundleId("com.facebook.WebDriverAgentRunner.xctrunner").setForegroundIfRunning(true).build();
      Idb.LaunchRequest request = Idb.LaunchRequest.newBuilder().setStart(s).build();
      stub.launch(new LaunchResponseStreamObserver()).onNext(request);
      Thread.sleep(1000L);
      return Boolean.valueOf(true);
    } catch (InterruptedException e) {
      return Boolean.valueOf(false);
    } 
  }
  
  public static Boolean installApp(int port, String path) {
    try {
      ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build();
      CompanionServiceGrpc.CompanionServiceStub stub = CompanionServiceGrpc.newStub((Channel)channel);
      final CountDownLatch finishLatch = new CountDownLatch(1);
      Idb.Payload payload = Idb.Payload.newBuilder().setFilePath(path).build();
      StreamObserver<Idb.InstallResponse> responseObserver = new StreamObserver<Idb.InstallResponse>() {
          public void onNext(Idb.InstallResponse value) {
            System.out.println(String.valueOf(value.getProgress()) + " " + value.getName() + " " + value.getUuid());
          }
          
          public void onError(Throwable t) {
            System.out.println(t.getMessage());
            finishLatch.countDown();
          }
          
          public void onCompleted() {
            System.out.println("Installed Successfully");
            finishLatch.countDown();
          }
        };
      StreamObserver<Idb.InstallRequest> requestObserver = stub.install(responseObserver);
      requestObserver.onNext(Idb.InstallRequest.newBuilder().setDestinationValue(0).build());
      requestObserver.onNext(Idb.InstallRequest.newBuilder().setPayload(payload).build());
      Thread.sleep(2000L);
      return Boolean.valueOf(true);
    } catch (InterruptedException e) {
      return Boolean.valueOf(false);
    } 
  }
}
