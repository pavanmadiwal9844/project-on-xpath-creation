package com.simplifyqa.grpc;

import idb.Idb;
import io.grpc.stub.StreamObserver;

public class LaunchResponseStreamObserver implements StreamObserver<Idb.LaunchResponse> {
  public void onError(Throwable throwable) {}
  
  public void onCompleted() {
    System.out.println("Launched Successfully");
  }
  
  public void onNext(Idb.LaunchResponse value) {}
}
