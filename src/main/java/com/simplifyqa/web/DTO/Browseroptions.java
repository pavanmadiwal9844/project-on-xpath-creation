package com.simplifyqa.web.DTO;

import java.util.HashMap;

public class Browseroptions {

	private HashMap<String, String> options=new HashMap<String, String>();

	public HashMap<String, String> getOptions() {
		return options;
	}

	public void setOptions(HashMap<String, String> options) {
		this.options = options;
	}
	
	
}
