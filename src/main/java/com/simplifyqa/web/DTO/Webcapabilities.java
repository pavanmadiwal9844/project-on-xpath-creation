package com.simplifyqa.web.DTO;

import org.json.JSONObject;

public class Webcapabilities {
	private JSONObject capabilities ;

	public JSONObject getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(JSONObject capabilities) {
		this.capabilities = capabilities;
	}

}
