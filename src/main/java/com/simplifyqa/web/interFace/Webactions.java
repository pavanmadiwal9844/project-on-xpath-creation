package com.simplifyqa.web.interFace;

import org.json.JSONArray;
import org.json.JSONObject;

public interface Webactions {

	public JSONObject getTimeout();

	public boolean setTimeout(long scripttimeout, long pageloadtimeout, long implicittimeout);

	public boolean launchUrl(String url);

	public String getUrl();

	public boolean back();

	public boolean forward();

	public boolean refresh();

	public String getTitle();

	public String getWindowhandle();

	public boolean switchtoWindow(String handle);

	public JSONArray getWindowhandles();

	public boolean switchtoFrame(int id);

	public boolean switchtoParentframe();

	public JSONObject getWindowrect();

	public boolean setWindowrect(int height, int width, int x, int y);

	public boolean maximizeScreen();

	public boolean minimizeScreen();

	public boolean fullsScreen();

	public JSONObject getActiveelement();

	public String findElement(String using, String value);

	public JSONArray findElements(String using, String value);

	public String findelementfromElement(String using, String value, String elementId);

	public JSONObject findelementsfromElement(String using, String value, String elementId);

	public boolean isElementselected(String elementid);

	public String getElementattribute(String elementid, String attribute);

	public String getElementproperty(String elementid, String property);

	public String getElementcssvalue(String elementid, String css);

	public String getElementtext(String elementid);

	public String getElementtagname(String elementid);

	public JSONObject getElementrect(String elementid);

	public boolean isElementenabled(String elementid);

	public String elementClick(String elementId);

	public boolean elementClear(String elementId);

	public boolean elementSendkeys(String elementId, String valuetoEnter);

	public String getPagesource();

	public boolean executeScript(String script);

	public boolean executeAsyncscript(String script);

	public JSONArray getallCookies();

	public JSONObject getnamedCookies(String cookieName);

	public boolean addCookie(String name, String value, String path, String domain, boolean secure, boolean httpOnly,
			int expiry);

	public boolean deleteCookie(String cookieName);

	public boolean deleteAllcookies();

	public boolean performAction();

	public boolean releaseAction();

	public boolean dismissAlert();

	public boolean acceptAlert();

	public String getAlerttext();

	public boolean sendAlerttext(String valuetoEnter);

	public String takeScreenshot();

	public String takeElementscreenshot(String elementId);

	public boolean isElementDisplayed(String elementId);

	public boolean closeWindow();
	
	public String getElementppt(String elementid, String attribute);
}
