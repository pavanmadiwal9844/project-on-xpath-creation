package com.simplifyqa.web.interFace;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;

public interface DriverSession {

	public String createSession(String hostaddress, String params)
			throws KeyManagementException, JSONException, NoSuchAlgorithmException, KeyStoreException, IOException;

	public boolean deleteSession();

	public JSONObject sessionStatus();

	public String createFirefoxSession(String hostaddress, String params);

}
