package com.simplifyqa.web.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.simplifyqa.Utility.Constants;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.UserDir;

import net.lingala.zip4j.core.ZipFile;

public class Automatic_Downloader {
	private String URL = "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_";
	private String platform = "";
	private String architecture = "";
	private String extension = "";
	private static Logger logger = LoggerFactory.getLogger(DriverOption.class);
	private String slash;
	
	public Automatic_Downloader(){
		if (SystemUtils.IS_OS_WINDOWS) {
			slash = "\\";
		}else {
			slash = "/";
		}
	}

	private static String driverPath() {
		return Paths.get(UserDir.getAapt().getAbsolutePath(), new String[] { "drivers" }).toString();
	}

	public String getLatestVersion(String Version) {
		if (Version != "") {
			URL url;
			StringBuffer content = null;
			try {
				url = new URL(URL + Version);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					content.append(inputLine);
				}
				in.close();
			} catch (IOException e) {
				logger.error("unable to get chrome latest version");
			}
			return content.toString();
		}
		return Version;

	}

	public boolean downloadDriver(String browser, String version) {
		try {
			HashMap<String, String> header = new HashMap<String, String>();
			header.put("content-type", "application/json");
			JSONObject object = new JSONObject();
			object.put("path", "drivers"+ slash + browser + slash + version);
			InputStream file = HttpUtility.getFilePost(Constants.FILES, object.toString(), header);
			FileUtils.copyInputStreamToFile(file, new File(driverPath() + slash+"driver.zip"));
			Automatic_Downloader.unzip(driverPath() +slash+"driver.zip", driverPath());
			new File(driverPath() +slash+"driver.zip").delete();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getChromeVersion() {
		String os = System.getProperty("os.name");
		Runtime run = Runtime.getRuntime();
		String Version = "";
		File file = null;
		if (os.toLowerCase().startsWith("mac")) {
			try {
				platform = "mac";
				architecture = "64";
				file = new File("/tmp/get_version.sh");
				run.exec("chmod 777 " + "/tmp/get_version.sh");
				FileWriter myWriter = new FileWriter("/tmp/get_version.sh");
				myWriter.write(
						"/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome --version > /tmp/output.txt");
				myWriter.close();
				run.exec("bash /tmp/get_version.sh");
				file = new File("/tmp/output.txt");
				if (file.exists()) {
					BufferedReader br = new BufferedReader(new FileReader(file));
					String st;
					while ((st = br.readLine()) != null)
						Version = st.split(" ")[2];
					Version = Version.substring(0, Version.lastIndexOf('.'));
					br.close();
				}
			} catch (IOException e) {
				logger.error("unable to get chrome version");
			}
		} else if (os.toLowerCase().startsWith("windows")) {
			try {
				platform = "win";
				architecture = "32";
				Process pr = run.exec("reg query HKEY_CURRENT_USER\\Software\\Google\\Chrome\\BLBeacon /v version");
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line = buf.readLine()) != null) {
					if (line.contains("version"))
						Version = Version + line;
				}
				if (Version != "") {
					Version = Version.split("   ")[3];
					Version = Version.substring(0, Version.lastIndexOf('.')).trim();
				}
			} catch (IOException e) {
				logger.error("unable to get chrome version");
			}

		} else {
			String executable = "";
			platform = "linux";
			architecture = "64";
			char check = 0;
			if (new File("/usr/bin/chromium-browser").isFile()) {
				executable = "chromium-browser";
				check = 'c';
			} else if (new File("/usr/bin/chromium").isFile()) {
				executable = "chromium";
				check = 'c';
			} else if (new File("/bin/google-chrome").isFile()) {
				executable = "google-chrome";
				check = 'g';
			}
			if (executable != "") {
				try {
					Process pr = run.exec(executable + " --version");
					BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
					String line = "";
					while ((line = buf.readLine()) != null) {
						if (check == 'g')
							Version = line.split(" ")[2];
						else
							Version = line.split(" ")[1];
					}
					Version = Version.substring(0, Version.lastIndexOf('.'));
				} catch (IOException e) {
					logger.error("unable to get chrome version");
				}
			}

		}
		return Version;

	}

	public static void unzip(String targetZipFilePath, String destinationFolderPath) {
		try {
			System.out.println(targetZipFilePath+" "+destinationFolderPath);
			ZipFile zipFile = new ZipFile(targetZipFilePath);
			zipFile.extractAll(destinationFolderPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean downloadChromeDriver(String Version) {
		boolean flag = false;
		if (Version != "") {
			try {
				try {
					String download_url = "https://chromedriver.storage.googleapis.com/" + Version + "/chromedriver_"
							+ platform + architecture + ".zip";
					InputStream file = HttpUtility.getFile(download_url, null, null);
					FileUtils.copyInputStreamToFile(file,
							new File(driverPath() +slash+"chromedriver_" + platform + architecture + ".zip"));
					unzip(driverPath() +slash+"chromedriver_" + platform + architecture + ".zip", driverPath());
					new File(driverPath() +slash+"chromedriver_" + platform + architecture + ".zip").delete();
					File hidden = new File(driverPath() +slash+".chrome");
					if (hidden.exists())
						hidden.delete();
					hidden.createNewFile();
					FileWriter myWriter = new FileWriter(driverPath() +slash+".chrome");
					myWriter.write(Version);
					myWriter.close();
					return true;
				} catch (Exception e) {
					logger.info("Retrying to download from local server");
					flag = true;
				}
				if (flag) {
					downloadDriver("chrome", Version);
					unzip(driverPath() +slash+ Version +slash+"chromedriver_" + platform + architecture + ".zip", driverPath());
					FileUtils.deleteDirectory(new File(driverPath()+slash+Version));
					File hidden = new File(driverPath() +slash+".chrome");
					if (hidden.exists())
						hidden.delete();
					hidden.createNewFile();
					FileWriter myWriter = new FileWriter(driverPath() +slash+".chrome");
					myWriter.write(Version);
					myWriter.close();
					return true;
				} else
					return true;

			} catch (Exception e) {
				logger.info("Unable to Update Chrome driver!!!");
				return false;
			}

		} else
			return false;

	}

	public String getLatestFirefoxVersion() {
		String Version = "";
		JSONObject object = HttpUtility.get(Constants.GECKO, null, null);
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Version = object.getString("tag_name");
		return Version;

	}

	public boolean downloadFirefoxDriver(String Version) {
		boolean flag = false;
		Runtime run = Runtime.getRuntime();
		try {
			try {
				String os = System.getProperty("os.name");
				if (os.toLowerCase().startsWith("mac")) {
					platform = "macos";
					architecture = "";
					extension = ".tar.gz";

				} else if (os.toLowerCase().startsWith("windows")) {
					platform = "win";
					extension = ".zip";
					if (System.getenv("ProgramFiles(x86)") != null) {
						architecture = "64";
					} else {
						architecture = "32";
					}
				} else {
					Process pr;
					platform = "linux";
					extension = ".tar.gz";
					try {
						pr = run.exec("java -d32 -version");
						BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
						String line = "";
						while ((line = buf.readLine()) != null) {
						}
						if (line.contains("Error")) {
							architecture = "64";
						} else {
							architecture = "32";
						}

					} catch (IOException e) {
						logger.error("unable to get java version");
					}
				}
				String download_url = "https://github.com/mozilla/geckodriver/releases/download/" + Version
						+ "/geckodriver-" + Version + "-" + platform + architecture + extension;
				InputStream file = HttpUtility.getFile(download_url, null, null);
				FileUtils.copyInputStreamToFile(file,
						new File(driverPath() +slash+"geckodriver-" + Version + "-" + platform + architecture + extension));
				if (extension.equalsIgnoreCase(".zip")) {
					unzip(driverPath() +slash+"geckodriver-" + Version + "-" + platform + architecture + extension, driverPath());
				} else {
					run.exec("tar -xzf " + driverPath() +slash+"geckodriver-" + Version + "-" + platform + architecture + extension
							+ " -C ./libs/drivers/");
				}
				new File(driverPath() +slash+"geckodriver-" + Version + "-" + platform + architecture + extension).delete();
				File hidden = new File(driverPath() +slash+".firefox");
				if (hidden.exists())
					hidden.delete();
				hidden.createNewFile();
				FileWriter myWriter = new FileWriter(driverPath() +slash+".firefox");
				myWriter.write(Version);
				myWriter.close();
				return true;
			} catch (Exception e) {
				logger.info("Retrying firefox update with local server!!!");
				flag = true;
			}
			if (flag) {
				String version = Version.replace("v", "");
				downloadDriver("firefox", version);
				if (extension.equalsIgnoreCase(".zip")) {
					unzip(driverPath() +slash+ version +slash+"geckodriver-" + Version.trim() + "-" + platform + architecture
							+ extension, driverPath());
				} else {
					run.exec("tar -xzf " + driverPath() + slash + version +slash+"geckodriver-" + Version + "-" + platform
							+ architecture + extension + " -C ./libs/drivers/");
				}
				FileUtils.deleteDirectory(new File(driverPath()+slash+version));
				File hidden = new File(driverPath() +slash+".firefox");
				if (hidden.exists())
					hidden.delete();
				hidden.createNewFile();
				FileWriter myWriter = new FileWriter(driverPath() +slash+".firefox");
				myWriter.write(Version);
				myWriter.close();
				return true;
			}
		} catch (Exception e) {
			logger.info("Unable to update Firexfox driver!!!");
		}
		return false;

	}

	public static String getLatestEdgeVersion() {
		try {
			String Version = "";
			Document doc = Jsoup.connect("https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/").get();
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Element el = doc.getElementsByClass("driver-download__meta").first();
			Version = el.html();
			Version = Version.split(":")[1].trim();
			return Version;

		} catch (IOException e) {
			logger.error("unable to get edge version");
			return null;
		}
	}

	public String getEdgeVersion() {
		try {
			String content = null;
			if (SystemUtils.IS_OS_WINDOWS) {
				ProcessBuilder builder = new ProcessBuilder(new String[] { "cmd", "/c",
						"wmic datafile where 'name=\"C:\\\\Program Files (x86)\\\\Microsoft\\\\Edge\\\\Application\\\\msedge.exe\"' get version" });
				builder.redirectErrorStream(true);
				Process process = builder.start();
				InputStream is = process.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (!line.trim().isEmpty() && !line.trim().equals("Version"))
						content=line;
				}
				return content.trim();

			} else if (SystemUtils.IS_OS_MAC) {
				ProcessBuilder builder = new ProcessBuilder(new String[] { "/bin/sh", "-c",
						"/Applications/Microsoft\\ Edge.app/Contents/MacOS/Microsoft\\ Edge --version" });
				builder.redirectErrorStream(true);
				Process process = builder.start();
				InputStream is = process.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				String line = null;
				while ((line = reader.readLine()) != null) {
					if (line == null)
						return null;
					else
						content = line;
				}
				return content.split(" ")[content.split(" ").length - 1];

			} else {
				return getLatestEdgeVersion();
			}
		} catch (IOException e) {
			logger.error("unable to get edge version");
		}
		return null;

	}

	public boolean downloadEdgeDriver(String Version) {
		boolean flag = false;
		try {
			try {
				String os = System.getProperty("os.name");
				if (os.toLowerCase().startsWith("mac")) {
					platform = "mac";
					architecture = "64";
				} else if (os.toLowerCase().startsWith("windows")) {
					platform = "win";
					if (System.getenv("ProgramFiles(x86)") != null) {
						architecture = "64";
					} else {
						architecture = "32";
					}
				} else {
					platform = "linux";
					architecture = "64";
				}
				Version = Version.trim();
				String download_url = "https://msedgedriver.azureedge.net/" + Version + "/edgedriver_" + platform
						+ architecture + ".zip";
				InputStream file = HttpUtility.getFile(download_url, null, null);
				FileUtils.copyInputStreamToFile(file,
						new File(driverPath() +slash+"edgedriver_" + platform + architecture + ".zip"));
				unzip(driverPath() +slash+"edgedriver_" + platform + architecture + ".zip", driverPath());
				new File(driverPath() +slash+"edgedriver_" + platform + architecture + ".zip").delete();
				File hidden = new File(driverPath() +slash+".edge");
				if (hidden.exists())
					hidden.delete();
				hidden.createNewFile();
				FileWriter myWriter = new FileWriter(driverPath() +slash+".edge");
				myWriter.write(Version);
				myWriter.close();
				return true;
			} catch (Exception e) {
				logger.info("Retrying Edge Update with local server!!!");
				flag = true;
			}
			if (flag) {
				downloadDriver("edge", Version);
				unzip(driverPath() +slash+Version +slash+"edgedriver_" + platform + architecture + ".zip", driverPath());
				FileUtils.deleteDirectory(new File(driverPath()+slash+Version));
				File hidden = new File(driverPath() +slash+".edge");
				if (hidden.exists())
					hidden.delete();
				hidden.createNewFile();
				FileWriter myWriter = new FileWriter(driverPath() +slash+".edge");
				myWriter.write(Version);
				myWriter.close();
				return true;
			}
		} catch (Exception e) {
			logger.info("Unable to update edge driver!!!");
		}
		return false;

	}

	public Boolean checkExist(String browser) {
		try {
			String os = System.getProperty("os.name");
			File file = null;
			if (os.toLowerCase().startsWith("windows")) {
				file = new File(driverPath() + "/." + browser);
			} else {
				file = new File(driverPath() + "/." + browser);
			}
			Scanner myReader;
			String data = "";
			if (file.exists()) {
				myReader = new Scanner(file);
				while (myReader.hasNextLine()) {
					data = data + myReader.nextLine();
				}
				myReader.close();
				if (browser.equalsIgnoreCase("chrome")) {
					String version = getLatestVersion(getChromeVersion());
					if (data.equalsIgnoreCase(version) || version.equalsIgnoreCase("")) {
						return true;
					} else {
						return false;
					}
				} else if (browser.equalsIgnoreCase("firefox")) {
					if (data.equalsIgnoreCase(getLatestFirefoxVersion())) {
						return true;
					} else {
						return false;
					}
				} else {
					if (data.equalsIgnoreCase(getEdgeVersion())) {
						return true;
					} else {
						return false;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}



}