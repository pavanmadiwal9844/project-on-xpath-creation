package com.simplifyqa.web.implementation;


import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.ElementClickInterceptedException;
import com.simplifyqa.Utility.HttpUtility;
import com.simplifyqa.Utility.InitializeDependence;
import com.simplifyqa.web.interFace.Webactions;

public class WebAutomationManager implements Webactions {

	private String sessionId;
	private String hostAddress;
	
	private int count=0;

	public HashMap<String, String> headers = new HashMap<String, String>();

	public String getSessionId() {
		return sessionId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public WebAutomationManager(String sessionId, String hostAddress) {
		this.sessionId = sessionId;
		this.hostAddress = hostAddress;
	}

	@Override
	public JSONObject getTimeout() {
		String host = hostAddress + "/session" + "/" + sessionId + "/timeouts";
		headers.put("content-type", "application/json");
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			return res.getJSONObject("value");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean setTimeout(long scripttimeout, long pageloadtimeout, long implicittimeout) {
		String host = hostAddress + "/session" + "/" + sessionId + "/timeouts";
		JSONObject param = new JSONObject();
		headers.put("content-type", "application/json");
		param.put("script", scripttimeout);
		param.put("pageLoad", pageloadtimeout);
		param.put("implicit", implicittimeout);
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.has("value"))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean launchUrl(String url) {
		JSONObject navigateurl = new JSONObject();
		navigateurl.put("url", url);
		String host = hostAddress + "/session" + "/" + sessionId + "/url";
		headers.put("content-type", "application/json");
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, navigateurl.toString(), headers);
//			if (res.get("value").equals(null))
//				return true;
//			else
//				return false;
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public String getUrl() {
		String host = hostAddress + "/session" + "/" + sessionId + "/url";
		headers.put("content-type", "application/json");
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			return res.getString("value");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean back() {
		String host = hostAddress + "/session/" + sessionId + "/back";
		JSONObject navigateurl = new JSONObject();
		try {
			HttpUtility.SendPost(host, navigateurl.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean forward() {
		String host = hostAddress + "/session/" + sessionId + "/forward";
		JSONObject navigateurl = new JSONObject();
		try {
			HttpUtility.SendPost(host, navigateurl.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean refresh() {
		String host = hostAddress + "/session/" + sessionId + "/refresh";
		JSONObject navigateurl = new JSONObject();
		try {
			HttpUtility.SendPost(host, navigateurl.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public String getTitle() {
		String host = hostAddress + "/session" + "/" + sessionId + "/title";
		headers.put("content-type", "application/json");
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			if (!res.get("value").equals(null))
				return res.getString("value");
			else
				return "Error";
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String getWindowhandle() {
		try {
			String host = hostAddress + "/session/" + sessionId + "/window";
			headers.put("content-type", "application/json");
			JSONObject response = HttpUtility.get(host, null, headers);
			return response.getString("value");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean closeWindow() {
		try {
			String host = hostAddress + "/session/" + sessionId + "/window";
			headers.put("content-type", "application/json");
			HttpUtility.delete(host);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean switchtoWindow(String handle) {
		String host = hostAddress + "/session/" + sessionId + "/window";
		JSONObject handlejson = new JSONObject();
		handlejson.put("handle", handle);
		headers.put("content-type", "application/json");
		try {
			HttpUtility.sendPost(host, handlejson.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean getElementpropertyboolean(String elementid, String property) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/property/" + property;
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		boolean result = false;
		try {
			resp = HttpUtility.get(host, param, headers);
			result = resp.getBoolean("value");
		} catch (Exception e) {
			
			
			e.printStackTrace();
			result=false;
		}
		return result;
	}

	@Override
	public JSONArray getWindowhandles() {
		try {
			String host = hostAddress + "/session/" + sessionId + "/window/handles";
			headers.put("content-type", "application/json");
			JSONObject response = HttpUtility.get(host, null, headers);
			return response.getJSONArray("value");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean switchtoFrame(int id) {
		try {
			String host = hostAddress + "/session/" + sessionId + "/frame";
			headers.put("content-type", "application/json");
			JSONObject body = new JSONObject();
			body.put("id", id);
//			HashMap<String, String> headers = new HashMap<String, String>();
			String res = HttpUtility.SendPost(host, body.toString(), headers);
			// {"value":null}
			if (res != "Error") {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {

			return false;
		}
	}

	@Override
	public boolean switchtoParentframe() {
		try {
			String host = hostAddress + "/session/" + sessionId + "/frame/parent";
			headers.put("content-type", "application/json");
//			HashMap<String, String> headers = new HashMap<String, String>();
			HttpUtility.SendPost(host, "{}", headers);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public JSONObject getWindowrect() {
//		String host = hostAddress + "/session/" + sessionId + "/frame/parent";
		String host = hostAddress + "/session/" + sessionId + "/window/rect";
		headers.put("content-type", "application/json");
		try {
			JSONObject response = HttpUtility.get(host, null, headers);
			return response.getJSONObject("value");
		} catch (Exception e) {
			return new JSONObject().put("error", e.getMessage());
		}
	}

	@Override
	public boolean setWindowrect(int height, int width, int x, int y) {
		String host = hostAddress + "/session/" + sessionId + "/frame/parent";
		headers.put("content-type", "application/json");
		JSONObject body = new JSONObject();
		body.put("height", height).put("width", width).put("x", x).put("y", y);
		try {
			HttpUtility.sendPost(host, body.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean maximizeScreen() {
		String host = hostAddress + "/session/" + sessionId + "/window/maximize";
		headers.put("content-type", "application/json");
		JSONObject body = new JSONObject();
		try {
			HttpUtility.sendPost(host, body.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean minimizeScreen() {
		String host = hostAddress + "/session/" + sessionId + "/window/minimize";
		headers.put("content-type", "application/json");
		JSONObject body = new JSONObject();
		try {
			HttpUtility.sendPost(host, body.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean fullsScreen() {
		String host = hostAddress + "/session/" + sessionId + "/window/fullscreen";
		headers.put("content-type", "application/json");
		JSONObject body = new JSONObject();
		try {
			HttpUtility.sendPost(host, body.toString(), headers);
			return true;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public JSONObject getActiveelement() {
		String host = hostAddress + "/session/" + sessionId + "/element" + "/active";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			resp.getJSONObject("value");
			return resp.getJSONObject("value");
		} catch (Exception e) {
			resp.put("error", e.toString());
			return resp;
		}

	}

	@Override
	public String findElement(String using, String value) {
		String elementId = null;
		String host = hostAddress + "/session/" + sessionId + "/element";
		JSONObject param = new JSONObject();
		if(InitializeDependence.Click_timeout)
			setTimeout(10,10,10);
		param.put("using", using);
		param.put("value", value);
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			JSONObject element = res.getJSONObject("value");
			Iterator<String> keys = element.keys();
			String key = null;
			while (keys.hasNext()) {
				key = keys.next();
			}
			elementId = element.getString(key);
			return elementId;
		} catch (NullPointerException e1) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	
	public String findElementforloader(String using, String value) {
		String elementId = null;
		String host = hostAddress + "/session/" + sessionId + "/element";
		JSONObject param = new JSONObject();
		param.put("using", using);
		param.put("value", value);
		try {
			JSONObject res = HttpUtility.sendPostforloader(host, param.toString(), headers);
			
			if(res==null) {
				return null;
			}
			JSONObject element = res.getJSONObject("value");
			Iterator<String> keys = element.keys();
			String key = null;
			while (keys.hasNext()) {
				key = keys.next();
			}
			elementId = element.getString(key);
			return elementId;
		} catch (Exception e) {
			// TODO: handle exception
			return elementId;
		}

	}

	@Override
	public JSONArray findElements(String using, String value) {
		count++;
		String host = hostAddress + "/session/" + sessionId + "/elements";
		JSONObject param = new JSONObject();
		param.put("using", using);
		param.put("value", value);
		JSONObject res = null;
		try {
			System.out.println("sending request");
			res = HttpUtility.sendPost(host, param.toString(), headers);
			System.out.println("response :" + res);
			if (res.has("value") && res.get("value") instanceof JSONObject && res.getJSONObject("value").has("error")) {
				count=0;
				return null;
			} else if (res.getJSONArray("value") != null) {
				count=0;
				return res.getJSONArray("value");
			} else {
				count=0;
				return null;
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException | org.json.JSONException e) {
			count=0;
			return null;
		} catch (ElementClickInterceptedException e) {
			count=0;
			return null;
		}
		catch (IllegalStateException e) {
			// TODO: handle exception
//			if(count<5) {
//				return findElements(using,value);
//			}
//			else {
				count=0;
				return null;
//			}
			
		}
	}

	@Override
	public String findelementfromElement(String using, String value, String elementId) {
		String respelement = null;
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/element";
		JSONObject param = new JSONObject();
		param.put("using", using);
		param.put("value", value);
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			JSONObject element = res.getJSONObject("value");
			Iterator<String> keys = element.keys();
			String key = null;
			while (keys.hasNext()) {
				key = keys.next();
			}
			respelement = element.getString(key);
			return respelement;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return e.toString();
		}

	}

	@Override
	public JSONObject findelementsfromElement(String using, String value, String elementId) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/elements";
		JSONObject param = new JSONObject();
		param.put("using", using);
		param.put("value", value);
		JSONObject res = null;
		JSONObject resp = null;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			JSONArray val = res.getJSONArray("value");
			for (int i = 0; i < val.length(); i++) {
				resp = val.getJSONObject(i);
			}
			return resp;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			new JSONObject().put("error", e.toString());
			return resp;
		}
	}

	@Override
	public boolean isElementselected(String elementid) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/selected";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			resp.getJSONObject("value");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String getElementattribute(String elementid, String attribute) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/attribute/" + attribute;
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.get("value").toString();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getElementproperty(String elementid, String property) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/property/" + property;
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		String result = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			result = resp.getString("value");
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return result;
	}

	@Override
	public String getElementcssvalue(String elementid, String css) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/css/" + css;
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.getString("value");

		} catch (Exception e) {
			return e.getMessage().toString();
		}
	}

	@Override
	public String getElementtext(String elementid) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/attribute/value";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.getString("value");
		} catch (Exception e) {
			return e.getMessage().toString();
		}
	}

	@Override
	public String getElementtagname(String elementid) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/name";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.getString("value");

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	@Override
	public JSONObject getElementrect(String elementid) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/rect";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.getJSONObject("value");

		} catch (Exception e) {
			return new JSONObject().put("error", e.getMessage());
		}
	}

	@Override
	public boolean isElementenabled(String elementid) {
		try {
			String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/enabled";
			HashMap<String, String> param = new HashMap<String, String>();
			JSONObject resp = null;
			resp = HttpUtility.get(host, param, headers);
			if (resp.has("value")) {
				if (resp.get("value") != null) {
					return resp.getBoolean("value");
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String elementClick(String elementId) {
		count++;
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/click";
		JSONObject res;
		JSONObject param = new JSONObject();
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null)) {
				count=0;
				return "true";
			}
			else {
				JSONObject val = res.getJSONObject("value");
				if (val.getString("message").contains("is not clickable at point")) {
					count=0;
					return "not clickable at point";
				} else {
					count=0;
					return "false";
				}
			}
		}
		catch (IllegalStateException e) {
			if(count<5) {
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return elementClick(elementId);
			}
			else {
				e.printStackTrace();
				return "false";
			}
		}
		
		catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			
			e.printStackTrace();
			return "false";
		}
	}

	@Override
	public boolean elementClear(String elementId) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/clear";
		JSONObject param = new JSONObject();
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean elementSendkeys(String elementId, String valuetoEnter) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/value";
		JSONObject param = new JSONObject();
		param.put("text", valuetoEnter);
		param.put("value", valuetoEnter.split(""));
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.has("value"))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getPagesource() {
		String host = hostAddress + "/session/" + sessionId + "/source";
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.getString("value");

		} catch (Exception e) {
			return "error";
		}
	}

	@Override
	public boolean executeScript(String script) {
		String host = hostAddress + "/session/" + sessionId + "/execute/sync";
		JSONObject param = new JSONObject();
		param.put("script", script);
		param.put("args", new JSONArray());
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.has("value"))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public JSONObject executeScript2(String script) {
		String host = hostAddress + "/session/" + sessionId + "/execute/sync";
		JSONObject param = new JSONObject();
		param.put("script", script);
		param.put("args", new JSONArray());
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			System.out.println(res);
			return res;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean executeAsyncscript(String script) {
		String host = hostAddress + "/session/" + sessionId + "/execute/async";
		JSONObject param = new JSONObject();
		param.put("script", script);
		param.put("args", new JSONArray());
		JSONObject res;
		try {
			res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.has("value"))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public JSONArray getallCookies() {
		String host = hostAddress + "/session/" + sessionId + "/cookie";
		JSONArray resp = null;
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			if (res.getJSONArray("value").length() > 0) {
				return res.getJSONArray("value");
			} else {
				return resp;
			}
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject getnamedCookies(String cookieName) {
		String host = hostAddress + "/session/" + sessionId + "/cookie/" + cookieName;
		JSONObject res = null;
		try {
			res = HttpUtility.get(host, null, headers);
			if (!res.get("value").equals(null)) {
				res.put(cookieName, res.get("value"));
			} else {
				res.put("error", "Error");
			}
		} catch (Exception e) {
			res.put("error", e.toString());
		}
		return res;
	}

	@Override
	public boolean addCookie(String name, String value, String path, String domain, boolean secure, boolean httpOnly,
			int expiry) {
		String host = hostAddress + "/session/" + sessionId + "/cookie";
		JSONObject cookie = new JSONObject();
		cookie.put("name", name);
		cookie.put("value", value);
		cookie.put("path", path);
		cookie.put("domain", domain);
		cookie.put("secure", secure);
		cookie.put("httpOnly", httpOnly);
		cookie.put("expiry", expiry);
		JSONObject param = new JSONObject();
		param.put("cookie", cookie);
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteCookie(String cookieName) {
		String host = hostAddress + "/session/" + sessionId + "/cookie/" + cookieName;
		try {
			JSONObject res = HttpUtility.delete(host);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteAllcookies() {
		String host = hostAddress + "/session/" + sessionId + "/cookie";
		try {
			JSONObject res = HttpUtility.delete(host);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean performAction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean releaseAction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean dismissAlert() {
		String host = hostAddress + "/session/" + sessionId + "/alert/dismiss";
		JSONObject param = new JSONObject();
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean acceptAlert() {
		String host = hostAddress + "/session/" + sessionId + "/alert/accept";
		JSONObject param = new JSONObject();
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getAlerttext() {
		String host = hostAddress + "/session/" + sessionId + "/alert/text";
		JSONObject res = HttpUtility.get(host, null, headers);
		String value = null;
		try {
			if (res.has("value")) {
				value = res.getString("value");
			} else {
				value = res.getJSONObject("value").getString("error");
			}
		} catch (JSONException e) {
			value = res.getJSONObject("value").getJSONObject("data").getString("text");
			return value;
		}
		return value;
	}

	@Override
	public boolean sendAlerttext(String valuetoEnter) {
		String host = hostAddress + "/session/" + sessionId + "/alert/text";
		JSONObject param = new JSONObject();
		param.put("text", valuetoEnter);
		try {
			JSONObject res = HttpUtility.sendPost(host, param.toString(), headers);
			if (res.get("value").equals(null))
				return true;
			else
				return false;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException
				| IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String takeScreenshot() {
		String host = hostAddress + "/session" + "/" + sessionId + "/screenshot";
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			return res.getString("value");
		} catch (Exception e) {
			return null;
		}
	}


	@Override
	public String takeElementscreenshot(String elementId) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/screenshot";
		try {
			JSONObject res = HttpUtility.get(host, null, headers);
			return res.getString("value");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean isElementDisplayed(String elementId) {
		try {
			String host = hostAddress + "/session/" + sessionId + "/element/" + elementId + "/displayed";
			HashMap<String, String> param = new HashMap<String, String>();
			JSONObject resp = null;
			resp = HttpUtility.get(host, param, headers);
			if (resp.has("value")) {
				if (resp.get("value") != null) {
					return resp.getBoolean("value");
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String getElementppt(String elementid, String attribute) {
		String host = hostAddress + "/session/" + sessionId + "/element/" + elementid + "/attribute/" + attribute;
		HashMap<String, String> param = new HashMap<String, String>();
		JSONObject resp = null;
		try {
			resp = HttpUtility.get(host, param, headers);
			return resp.get("value").toString();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
