package com.simplifyqa.web.implementation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class executor {

	private static executor g;

	public static executor g() {
		if (g == null) {
			g = new executor();
		}
		return g;
	}

	public ExecutorService parallelobject = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS,
			new SynchronousQueue());

	public ExecutorService parallelobject() {
		return this.parallelobject;
	}

	public ExecutorService restart() {
		parallelobject = new Threadpool(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue());
		return parallelobject;
	}

}
