package com.simplifyqa.web.implementation;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;

public class GetDriverVersion {
	Logger logger = LoggerFactory.getLogger(GetDriverVersion.class);
	private String driverPath(String driverName) {
		return Paths.get(UserDir.driver().getAbsolutePath(), new String[] { driverName }).toString();
	}
	
	public Integer get(String driverName) {
		try {
			String info;
			ProcessBuilder builder = new ProcessBuilder(new String[] {driverPath(driverName),"--version"});
			builder.environment().put("JAVA_HOME", System.getProperty("java.home"));
			Process process = builder.start();
			InputStream is = process.getInputStream();
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.equals("")) {
//					info.add(line);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Unable to get the driver versoin");
			return -1;
			
		}
		return null;
	}
}
