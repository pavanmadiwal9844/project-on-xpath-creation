package com.simplifyqa.web.implementation;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Paths;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplifyqa.Utility.UserDir;

public class DriverOption {

	private String browserName;
	private String hostAddress = "127.0.0.1";
	private Logger logger = LoggerFactory.getLogger(DriverOption.class);
	private Process process;
	private int PORT = 1717;

	private String driverPath(String driverName) {
		return Paths.get(UserDir.driver().getAbsolutePath(), new String[] { driverName }).toString();
	}

	public DriverOption(String browserName) {
		this.browserName = browserName;
	}

	public String init() {
		String PORT = utilBrowserOption();
//		Automatic_Downloader am = new Automatic_Downloader();
		try {
			ProcessBuilder builder;
			switch (this.browserName.toLowerCase()) {
			case "chrome":
//				if (!(am.checkExist("chrome"))) {
//					am.downloadChromeDriver(am.getLatestVersion(am.getChromeVersion()));
//				}
				if (SystemUtils.IS_OS_WINDOWS) {
//					System.setProperty("chrome", driverPath("chromedriver.exe"));
					System.setProperty("chrome", "./libs/drivers/chromedriver.exe");
					builder = new ProcessBuilder(new String[] { System.getProperty("chrome"), PORT });
					Thread.sleep(500);
					this.process = builder.start();
				} else if (SystemUtils.IS_OS_LINUX) {
					System.setProperty("chrome", this.driverPath("chromedriver"));
					builder = new ProcessBuilder(new String[] { System.getProperty("chrome"), PORT });
					Thread.sleep(500);
					this.process = builder.start();
				}

				else {
					System.setProperty("chrome", driverPath("chromedriver"));
					builder = new ProcessBuilder(new String[] { driverPath("chromedriver"), PORT });
					this.process = builder.start();
				}

				break;
			case "firefox":
//				if (!(am.checkExist("firefox"))) {
//					am.downloadFirefoxDriver(am.getLatestFirefoxVersion());
//				}
				if (SystemUtils.IS_OS_WINDOWS) {
					System.setProperty("gecko", driverPath("geckodriver.exe"));
//					Runtime.getRuntime().exec(new String[] { System.getProperty("gecko"), PORT }, null,
//							new File(UserDir.driver().getAbsolutePath()));
					builder = new ProcessBuilder(new String[] { System.getProperty("gecko"), PORT });
					Thread.sleep(500L);
					this.process = builder.start();
					
					
					
				} else if (SystemUtils.IS_OS_LINUX) {
					System.setProperty("gecko", this.driverPath("geckodriver"));
					builder = new ProcessBuilder(new String[] { System.getProperty("gecko"), PORT });
					Thread.sleep(500L);
					this.process = builder.start();
				} else {
					System.setProperty("gecko", driverPath("geckodriver"));
					builder = new ProcessBuilder(new String[] { System.getProperty("gecko"), PORT });
					this.process = builder.start();
				}
				break;
			case "edge":
//				if (!(am.checkExist("edge"))) {
//					am.downloadEdgeDriver(am.getEdgeVersion());
//				}
				if (SystemUtils.IS_OS_WINDOWS) {
					System.setProperty("mse", driverPath("msedgedriver.exe"));
					builder = new ProcessBuilder(new String[] { System.getProperty("mse"), PORT });
					this.process = builder.start();
				} else if (SystemUtils.IS_OS_LINUX) {
					System.setProperty("mse", this.driverPath("msedgedriver"));
					builder = new ProcessBuilder(new String[] { System.getProperty("mse"), PORT });
					Thread.sleep(500L);
					this.process = builder.start();
				} else {
					System.setProperty("mse", driverPath("msedgedriver"));
					builder = new ProcessBuilder(new String[] { System.getProperty("mse"), PORT });
					this.process = builder.start();
				}
				break;
			case "internet explorer":
				if (SystemUtils.IS_OS_WINDOWS) {
					System.setProperty("ie", driverPath("IEDriverServer.exe"));
					builder = new ProcessBuilder(new String[] { System.getProperty("ie"), PORT });
					this.process = builder.start();
				}
				break;

			case "safari":
				builder = new ProcessBuilder(new String[] { "/usr/bin/safaridriver", PORT });
				this.process = builder.start();
				break;
			}
		} catch (

		Exception e) {
			System.out.println(e);
			logger.error("Unable to start driver");
		}
		return String.format("http://%s:%s", hostAddress, this.PORT);

	}

	public String utilBrowserOption() {
		StringBuilder sb = new StringBuilder();
		try {
			ServerSocket serverSocket;
			PORT = (serverSocket = new ServerSocket(0)).getLocalPort();
			serverSocket.close();
			sb.append("--port=" + PORT);
			return sb.toString();
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Unable to get the browser options");
			return null;
		}
	}

	public boolean destroy() {
		boolean res = false;
		try {
			this.process.destroyForcibly();
			res = true;
		} catch (Exception e) {
			// TODO: handle exception
			res = false;
		}
		return res;
	}

}
