package idb;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 * <pre>
 * The idb companion service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.0.0-pre2)",
    comments = "Source: idb.proto")
public class CompanionServiceGrpc {

  private CompanionServiceGrpc() {}

  public static final String SERVICE_NAME = "idb.CompanionService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ConnectRequest,
      idb.Idb.ConnectResponse> METHOD_CONNECT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "connect"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ConnectRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ConnectResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.DebugServerRequest,
      idb.Idb.DebugServerResponse> METHOD_DEBUGSERVER =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "debugserver"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.DebugServerRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.DebugServerResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.TargetDescriptionRequest,
      idb.Idb.TargetDescriptionResponse> METHOD_DESCRIBE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "describe"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.TargetDescriptionRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.TargetDescriptionResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.InstallRequest,
      idb.Idb.InstallResponse> METHOD_INSTALL =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "install"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.InstallRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.InstallResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.InstrumentsRunRequest,
      idb.Idb.InstrumentsRunResponse> METHOD_INSTRUMENTS_RUN =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "instruments_run"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.InstrumentsRunRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.InstrumentsRunResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.LogRequest,
      idb.Idb.LogResponse> METHOD_LOG =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "log"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LogRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LogResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.XctraceRecordRequest,
      idb.Idb.XctraceRecordResponse> METHOD_XCTRACE_RECORD =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "xctrace_record"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctraceRecordRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctraceRecordResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.AccessibilityInfoRequest,
      idb.Idb.AccessibilityInfoResponse> METHOD_ACCESSIBILITY_INFO =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "accessibility_info"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.AccessibilityInfoRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.AccessibilityInfoResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.FocusRequest,
      idb.Idb.FocusResponse> METHOD_FOCUS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "focus"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.FocusRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.FocusResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.HIDEvent,
      idb.Idb.HIDResponse> METHOD_HID =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "hid"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.HIDEvent.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.HIDResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.OpenUrlRequest,
      idb.Idb.OpenUrlRequest> METHOD_OPEN_URL =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "open_url"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.OpenUrlRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.OpenUrlRequest.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.SetLocationRequest,
      idb.Idb.SetLocationResponse> METHOD_SET_LOCATION =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "set_location"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.SetLocationRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.SetLocationResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ApproveRequest,
      idb.Idb.ApproveResponse> METHOD_APPROVE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "approve"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ApproveRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ApproveResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ClearKeychainRequest,
      idb.Idb.ClearKeychainResponse> METHOD_CLEAR_KEYCHAIN =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "clear_keychain"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ClearKeychainRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ClearKeychainResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ContactsUpdateRequest,
      idb.Idb.ContactsUpdateResponse> METHOD_CONTACTS_UPDATE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "contacts_update"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ContactsUpdateRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ContactsUpdateResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.SettingRequest,
      idb.Idb.SettingResponse> METHOD_SETTING =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "setting"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.SettingRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.SettingResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.GetSettingRequest,
      idb.Idb.GetSettingResponse> METHOD_GET_SETTING =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "get_setting"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.GetSettingRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.GetSettingResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ListSettingRequest,
      idb.Idb.ListSettingResponse> METHOD_LIST_SETTINGS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "list_settings"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ListSettingRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ListSettingResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.LaunchRequest,
      idb.Idb.LaunchResponse> METHOD_LAUNCH =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "launch"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LaunchRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LaunchResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ListAppsRequest,
      idb.Idb.ListAppsResponse> METHOD_LIST_APPS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "list_apps"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ListAppsRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ListAppsResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.TerminateRequest,
      idb.Idb.TerminateResponse> METHOD_TERMINATE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "terminate"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.TerminateRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.TerminateResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.UninstallRequest,
      idb.Idb.UninstallResponse> METHOD_UNINSTALL =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "uninstall"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.UninstallRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.UninstallResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.AddMediaRequest,
      idb.Idb.AddMediaResponse> METHOD_ADD_MEDIA =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "add_media"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.AddMediaRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.AddMediaResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.RecordRequest,
      idb.Idb.RecordResponse> METHOD_RECORD =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "record"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.RecordRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.RecordResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.ScreenshotRequest,
      idb.Idb.ScreenshotResponse> METHOD_SCREENSHOT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "screenshot"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ScreenshotRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.ScreenshotResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.VideoStreamRequest,
      idb.Idb.VideoStreamResponse> METHOD_VIDEO_STREAM =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "video_stream"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.VideoStreamRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.VideoStreamResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.CrashLogQuery,
      idb.Idb.CrashLogResponse> METHOD_CRASH_DELETE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "crash_delete"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashLogQuery.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashLogResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.CrashLogQuery,
      idb.Idb.CrashLogResponse> METHOD_CRASH_LIST =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "crash_list"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashLogQuery.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashLogResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.CrashShowRequest,
      idb.Idb.CrashShowResponse> METHOD_CRASH_SHOW =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "crash_show"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashShowRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.CrashShowResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.XctestListBundlesRequest,
      idb.Idb.XctestListBundlesResponse> METHOD_XCTEST_LIST_BUNDLES =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "xctest_list_bundles"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestListBundlesRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestListBundlesResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.XctestListTestsRequest,
      idb.Idb.XctestListTestsResponse> METHOD_XCTEST_LIST_TESTS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "xctest_list_tests"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestListTestsRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestListTestsResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.XctestRunRequest,
      idb.Idb.XctestRunResponse> METHOD_XCTEST_RUN =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "xctest_run"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestRunRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.XctestRunResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.LsRequest,
      idb.Idb.LsResponse> METHOD_LS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "ls"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LsRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.LsResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.MkdirRequest,
      idb.Idb.MkdirResponse> METHOD_MKDIR =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "mkdir"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.MkdirRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.MkdirResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.MvRequest,
      idb.Idb.MvResponse> METHOD_MV =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "mv"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.MvRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.MvResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.PullRequest,
      idb.Idb.PullResponse> METHOD_PULL =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "pull"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.PullRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.PullResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.PushRequest,
      idb.Idb.PushResponse> METHOD_PUSH =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING,
          generateFullMethodName(
              "idb.CompanionService", "push"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.PushRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.PushResponse.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<idb.Idb.RmRequest,
      idb.Idb.RmResponse> METHOD_RM =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "idb.CompanionService", "rm"),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.RmRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(idb.Idb.RmResponse.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CompanionServiceStub newStub(io.grpc.Channel channel) {
    return new CompanionServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CompanionServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CompanionServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static CompanionServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CompanionServiceFutureStub(channel);
  }

  /**
   * <pre>
   * The idb companion service definition.
   * </pre>
   */
  public static abstract class CompanionServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Management
     * </pre>
     */
    public void connect(idb.Idb.ConnectRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ConnectResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CONNECT, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.DebugServerRequest> debugserver(
        io.grpc.stub.StreamObserver<idb.Idb.DebugServerResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_DEBUGSERVER, responseObserver);
    }

    /**
     */
    public void describe(idb.Idb.TargetDescriptionRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.TargetDescriptionResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_DESCRIBE, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.InstallRequest> install(
        io.grpc.stub.StreamObserver<idb.Idb.InstallResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_INSTALL, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.InstrumentsRunRequest> instrumentsRun(
        io.grpc.stub.StreamObserver<idb.Idb.InstrumentsRunResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_INSTRUMENTS_RUN, responseObserver);
    }

    /**
     */
    public void log(idb.Idb.LogRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.LogResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_LOG, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.XctraceRecordRequest> xctraceRecord(
        io.grpc.stub.StreamObserver<idb.Idb.XctraceRecordResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_XCTRACE_RECORD, responseObserver);
    }

    /**
     * <pre>
     * Interaction
     * </pre>
     */
    public void accessibilityInfo(idb.Idb.AccessibilityInfoRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.AccessibilityInfoResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_ACCESSIBILITY_INFO, responseObserver);
    }

    /**
     */
    public void focus(idb.Idb.FocusRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.FocusResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_FOCUS, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.HIDEvent> hid(
        io.grpc.stub.StreamObserver<idb.Idb.HIDResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_HID, responseObserver);
    }

    /**
     */
    public void openUrl(idb.Idb.OpenUrlRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.OpenUrlRequest> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_OPEN_URL, responseObserver);
    }

    /**
     */
    public void setLocation(idb.Idb.SetLocationRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.SetLocationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SET_LOCATION, responseObserver);
    }

    /**
     * <pre>
     * Settings
     * </pre>
     */
    public void approve(idb.Idb.ApproveRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ApproveResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_APPROVE, responseObserver);
    }

    /**
     */
    public void clearKeychain(idb.Idb.ClearKeychainRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ClearKeychainResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CLEAR_KEYCHAIN, responseObserver);
    }

    /**
     */
    public void contactsUpdate(idb.Idb.ContactsUpdateRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ContactsUpdateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CONTACTS_UPDATE, responseObserver);
    }

    /**
     */
    public void setting(idb.Idb.SettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.SettingResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SETTING, responseObserver);
    }

    /**
     */
    public void getSetting(idb.Idb.GetSettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.GetSettingResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_SETTING, responseObserver);
    }

    /**
     */
    public void listSettings(idb.Idb.ListSettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ListSettingResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_LIST_SETTINGS, responseObserver);
    }

    /**
     * <pre>
     * App
     * </pre>
     */
    public io.grpc.stub.StreamObserver<idb.Idb.LaunchRequest> launch(
        io.grpc.stub.StreamObserver<idb.Idb.LaunchResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_LAUNCH, responseObserver);
    }

    /**
     */
    public void listApps(idb.Idb.ListAppsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ListAppsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_LIST_APPS, responseObserver);
    }

    /**
     */
    public void terminate(idb.Idb.TerminateRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.TerminateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TERMINATE, responseObserver);
    }

    /**
     */
    public void uninstall(idb.Idb.UninstallRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.UninstallResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_UNINSTALL, responseObserver);
    }

    /**
     * <pre>
     * Video/Audio
     * </pre>
     */
    public io.grpc.stub.StreamObserver<idb.Idb.AddMediaRequest> addMedia(
        io.grpc.stub.StreamObserver<idb.Idb.AddMediaResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_ADD_MEDIA, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.RecordRequest> record(
        io.grpc.stub.StreamObserver<idb.Idb.RecordResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_RECORD, responseObserver);
    }

    /**
     */
    public void screenshot(idb.Idb.ScreenshotRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ScreenshotResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SCREENSHOT, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.VideoStreamRequest> videoStream(
        io.grpc.stub.StreamObserver<idb.Idb.VideoStreamResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_VIDEO_STREAM, responseObserver);
    }

    /**
     * <pre>
     * Crash Operations
     * </pre>
     */
    public void crashDelete(idb.Idb.CrashLogQuery request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CRASH_DELETE, responseObserver);
    }

    /**
     */
    public void crashList(idb.Idb.CrashLogQuery request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CRASH_LIST, responseObserver);
    }

    /**
     */
    public void crashShow(idb.Idb.CrashShowRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashShowResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CRASH_SHOW, responseObserver);
    }

    /**
     * <pre>
     * xctest operations
     * </pre>
     */
    public void xctestListBundles(idb.Idb.XctestListBundlesRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestListBundlesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_XCTEST_LIST_BUNDLES, responseObserver);
    }

    /**
     */
    public void xctestListTests(idb.Idb.XctestListTestsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestListTestsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_XCTEST_LIST_TESTS, responseObserver);
    }

    /**
     */
    public void xctestRun(idb.Idb.XctestRunRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestRunResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_XCTEST_RUN, responseObserver);
    }

    /**
     * <pre>
     * File Operations
     * </pre>
     */
    public void ls(idb.Idb.LsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.LsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_LS, responseObserver);
    }

    /**
     */
    public void mkdir(idb.Idb.MkdirRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.MkdirResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_MKDIR, responseObserver);
    }

    /**
     */
    public void mv(idb.Idb.MvRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.MvResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_MV, responseObserver);
    }

    /**
     */
    public void pull(idb.Idb.PullRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.PullResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PULL, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.PushRequest> push(
        io.grpc.stub.StreamObserver<idb.Idb.PushResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_PUSH, responseObserver);
    }

    /**
     */
    public void rm(idb.Idb.RmRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.RmResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_RM, responseObserver);
    }

    @java.lang.Override public io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_CONNECT,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ConnectRequest,
                idb.Idb.ConnectResponse>(
                  this, METHODID_CONNECT)))
          .addMethod(
            METHOD_DEBUGSERVER,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.DebugServerRequest,
                idb.Idb.DebugServerResponse>(
                  this, METHODID_DEBUGSERVER)))
          .addMethod(
            METHOD_DESCRIBE,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.TargetDescriptionRequest,
                idb.Idb.TargetDescriptionResponse>(
                  this, METHODID_DESCRIBE)))
          .addMethod(
            METHOD_INSTALL,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.InstallRequest,
                idb.Idb.InstallResponse>(
                  this, METHODID_INSTALL)))
          .addMethod(
            METHOD_INSTRUMENTS_RUN,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.InstrumentsRunRequest,
                idb.Idb.InstrumentsRunResponse>(
                  this, METHODID_INSTRUMENTS_RUN)))
          .addMethod(
            METHOD_LOG,
            asyncServerStreamingCall(
              new MethodHandlers<
                idb.Idb.LogRequest,
                idb.Idb.LogResponse>(
                  this, METHODID_LOG)))
          .addMethod(
            METHOD_XCTRACE_RECORD,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.XctraceRecordRequest,
                idb.Idb.XctraceRecordResponse>(
                  this, METHODID_XCTRACE_RECORD)))
          .addMethod(
            METHOD_ACCESSIBILITY_INFO,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.AccessibilityInfoRequest,
                idb.Idb.AccessibilityInfoResponse>(
                  this, METHODID_ACCESSIBILITY_INFO)))
          .addMethod(
            METHOD_FOCUS,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.FocusRequest,
                idb.Idb.FocusResponse>(
                  this, METHODID_FOCUS)))
          .addMethod(
            METHOD_HID,
            asyncClientStreamingCall(
              new MethodHandlers<
                idb.Idb.HIDEvent,
                idb.Idb.HIDResponse>(
                  this, METHODID_HID)))
          .addMethod(
            METHOD_OPEN_URL,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.OpenUrlRequest,
                idb.Idb.OpenUrlRequest>(
                  this, METHODID_OPEN_URL)))
          .addMethod(
            METHOD_SET_LOCATION,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.SetLocationRequest,
                idb.Idb.SetLocationResponse>(
                  this, METHODID_SET_LOCATION)))
          .addMethod(
            METHOD_APPROVE,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ApproveRequest,
                idb.Idb.ApproveResponse>(
                  this, METHODID_APPROVE)))
          .addMethod(
            METHOD_CLEAR_KEYCHAIN,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ClearKeychainRequest,
                idb.Idb.ClearKeychainResponse>(
                  this, METHODID_CLEAR_KEYCHAIN)))
          .addMethod(
            METHOD_CONTACTS_UPDATE,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ContactsUpdateRequest,
                idb.Idb.ContactsUpdateResponse>(
                  this, METHODID_CONTACTS_UPDATE)))
          .addMethod(
            METHOD_SETTING,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.SettingRequest,
                idb.Idb.SettingResponse>(
                  this, METHODID_SETTING)))
          .addMethod(
            METHOD_GET_SETTING,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.GetSettingRequest,
                idb.Idb.GetSettingResponse>(
                  this, METHODID_GET_SETTING)))
          .addMethod(
            METHOD_LIST_SETTINGS,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ListSettingRequest,
                idb.Idb.ListSettingResponse>(
                  this, METHODID_LIST_SETTINGS)))
          .addMethod(
            METHOD_LAUNCH,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.LaunchRequest,
                idb.Idb.LaunchResponse>(
                  this, METHODID_LAUNCH)))
          .addMethod(
            METHOD_LIST_APPS,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ListAppsRequest,
                idb.Idb.ListAppsResponse>(
                  this, METHODID_LIST_APPS)))
          .addMethod(
            METHOD_TERMINATE,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.TerminateRequest,
                idb.Idb.TerminateResponse>(
                  this, METHODID_TERMINATE)))
          .addMethod(
            METHOD_UNINSTALL,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.UninstallRequest,
                idb.Idb.UninstallResponse>(
                  this, METHODID_UNINSTALL)))
          .addMethod(
            METHOD_ADD_MEDIA,
            asyncClientStreamingCall(
              new MethodHandlers<
                idb.Idb.AddMediaRequest,
                idb.Idb.AddMediaResponse>(
                  this, METHODID_ADD_MEDIA)))
          .addMethod(
            METHOD_RECORD,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.RecordRequest,
                idb.Idb.RecordResponse>(
                  this, METHODID_RECORD)))
          .addMethod(
            METHOD_SCREENSHOT,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.ScreenshotRequest,
                idb.Idb.ScreenshotResponse>(
                  this, METHODID_SCREENSHOT)))
          .addMethod(
            METHOD_VIDEO_STREAM,
            asyncBidiStreamingCall(
              new MethodHandlers<
                idb.Idb.VideoStreamRequest,
                idb.Idb.VideoStreamResponse>(
                  this, METHODID_VIDEO_STREAM)))
          .addMethod(
            METHOD_CRASH_DELETE,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.CrashLogQuery,
                idb.Idb.CrashLogResponse>(
                  this, METHODID_CRASH_DELETE)))
          .addMethod(
            METHOD_CRASH_LIST,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.CrashLogQuery,
                idb.Idb.CrashLogResponse>(
                  this, METHODID_CRASH_LIST)))
          .addMethod(
            METHOD_CRASH_SHOW,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.CrashShowRequest,
                idb.Idb.CrashShowResponse>(
                  this, METHODID_CRASH_SHOW)))
          .addMethod(
            METHOD_XCTEST_LIST_BUNDLES,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.XctestListBundlesRequest,
                idb.Idb.XctestListBundlesResponse>(
                  this, METHODID_XCTEST_LIST_BUNDLES)))
          .addMethod(
            METHOD_XCTEST_LIST_TESTS,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.XctestListTestsRequest,
                idb.Idb.XctestListTestsResponse>(
                  this, METHODID_XCTEST_LIST_TESTS)))
          .addMethod(
            METHOD_XCTEST_RUN,
            asyncServerStreamingCall(
              new MethodHandlers<
                idb.Idb.XctestRunRequest,
                idb.Idb.XctestRunResponse>(
                  this, METHODID_XCTEST_RUN)))
          .addMethod(
            METHOD_LS,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.LsRequest,
                idb.Idb.LsResponse>(
                  this, METHODID_LS)))
          .addMethod(
            METHOD_MKDIR,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.MkdirRequest,
                idb.Idb.MkdirResponse>(
                  this, METHODID_MKDIR)))
          .addMethod(
            METHOD_MV,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.MvRequest,
                idb.Idb.MvResponse>(
                  this, METHODID_MV)))
          .addMethod(
            METHOD_PULL,
            asyncServerStreamingCall(
              new MethodHandlers<
                idb.Idb.PullRequest,
                idb.Idb.PullResponse>(
                  this, METHODID_PULL)))
          .addMethod(
            METHOD_PUSH,
            asyncClientStreamingCall(
              new MethodHandlers<
                idb.Idb.PushRequest,
                idb.Idb.PushResponse>(
                  this, METHODID_PUSH)))
          .addMethod(
            METHOD_RM,
            asyncUnaryCall(
              new MethodHandlers<
                idb.Idb.RmRequest,
                idb.Idb.RmResponse>(
                  this, METHODID_RM)))
          .build();
    }
  }

  /**
   * <pre>
   * The idb companion service definition.
   * </pre>
   */
  public static final class CompanionServiceStub extends io.grpc.stub.AbstractStub<CompanionServiceStub> {
    private CompanionServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CompanionServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CompanionServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CompanionServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * Management
     * </pre>
     */
    public void connect(idb.Idb.ConnectRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ConnectResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CONNECT, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.DebugServerRequest> debugserver(
        io.grpc.stub.StreamObserver<idb.Idb.DebugServerResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_DEBUGSERVER, getCallOptions()), responseObserver);
    }

    /**
     */
    public void describe(idb.Idb.TargetDescriptionRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.TargetDescriptionResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_DESCRIBE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.InstallRequest> install(
        io.grpc.stub.StreamObserver<idb.Idb.InstallResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_INSTALL, getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.InstrumentsRunRequest> instrumentsRun(
        io.grpc.stub.StreamObserver<idb.Idb.InstrumentsRunResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_INSTRUMENTS_RUN, getCallOptions()), responseObserver);
    }

    /**
     */
    public void log(idb.Idb.LogRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.LogResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(METHOD_LOG, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.XctraceRecordRequest> xctraceRecord(
        io.grpc.stub.StreamObserver<idb.Idb.XctraceRecordResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_XCTRACE_RECORD, getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * Interaction
     * </pre>
     */
    public void accessibilityInfo(idb.Idb.AccessibilityInfoRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.AccessibilityInfoResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_ACCESSIBILITY_INFO, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void focus(idb.Idb.FocusRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.FocusResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_FOCUS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.HIDEvent> hid(
        io.grpc.stub.StreamObserver<idb.Idb.HIDResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(METHOD_HID, getCallOptions()), responseObserver);
    }

    /**
     */
    public void openUrl(idb.Idb.OpenUrlRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.OpenUrlRequest> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_OPEN_URL, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setLocation(idb.Idb.SetLocationRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.SetLocationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SET_LOCATION, getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Settings
     * </pre>
     */
    public void approve(idb.Idb.ApproveRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ApproveResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_APPROVE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void clearKeychain(idb.Idb.ClearKeychainRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ClearKeychainResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CLEAR_KEYCHAIN, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void contactsUpdate(idb.Idb.ContactsUpdateRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ContactsUpdateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CONTACTS_UPDATE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setting(idb.Idb.SettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.SettingResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SETTING, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getSetting(idb.Idb.GetSettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.GetSettingResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_SETTING, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listSettings(idb.Idb.ListSettingRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ListSettingResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_LIST_SETTINGS, getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * App
     * </pre>
     */
    public io.grpc.stub.StreamObserver<idb.Idb.LaunchRequest> launch(
        io.grpc.stub.StreamObserver<idb.Idb.LaunchResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_LAUNCH, getCallOptions()), responseObserver);
    }

    /**
     */
    public void listApps(idb.Idb.ListAppsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ListAppsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_LIST_APPS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void terminate(idb.Idb.TerminateRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.TerminateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TERMINATE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void uninstall(idb.Idb.UninstallRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.UninstallResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_UNINSTALL, getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Video/Audio
     * </pre>
     */
    public io.grpc.stub.StreamObserver<idb.Idb.AddMediaRequest> addMedia(
        io.grpc.stub.StreamObserver<idb.Idb.AddMediaResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(METHOD_ADD_MEDIA, getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.RecordRequest> record(
        io.grpc.stub.StreamObserver<idb.Idb.RecordResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_RECORD, getCallOptions()), responseObserver);
    }

    /**
     */
    public void screenshot(idb.Idb.ScreenshotRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.ScreenshotResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SCREENSHOT, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.VideoStreamRequest> videoStream(
        io.grpc.stub.StreamObserver<idb.Idb.VideoStreamResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_VIDEO_STREAM, getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * Crash Operations
     * </pre>
     */
    public void crashDelete(idb.Idb.CrashLogQuery request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CRASH_DELETE, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void crashList(idb.Idb.CrashLogQuery request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CRASH_LIST, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void crashShow(idb.Idb.CrashShowRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.CrashShowResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CRASH_SHOW, getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * xctest operations
     * </pre>
     */
    public void xctestListBundles(idb.Idb.XctestListBundlesRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestListBundlesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_XCTEST_LIST_BUNDLES, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void xctestListTests(idb.Idb.XctestListTestsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestListTestsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_XCTEST_LIST_TESTS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void xctestRun(idb.Idb.XctestRunRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.XctestRunResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(METHOD_XCTEST_RUN, getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * File Operations
     * </pre>
     */
    public void ls(idb.Idb.LsRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.LsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_LS, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void mkdir(idb.Idb.MkdirRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.MkdirResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_MKDIR, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void mv(idb.Idb.MvRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.MvResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_MV, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void pull(idb.Idb.PullRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.PullResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(METHOD_PULL, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<idb.Idb.PushRequest> push(
        io.grpc.stub.StreamObserver<idb.Idb.PushResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(METHOD_PUSH, getCallOptions()), responseObserver);
    }

    /**
     */
    public void rm(idb.Idb.RmRequest request,
        io.grpc.stub.StreamObserver<idb.Idb.RmResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_RM, getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The idb companion service definition.
   * </pre>
   */
  public static final class CompanionServiceBlockingStub extends io.grpc.stub.AbstractStub<CompanionServiceBlockingStub> {
    private CompanionServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CompanionServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CompanionServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CompanionServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Management
     * </pre>
     */
    public idb.Idb.ConnectResponse connect(idb.Idb.ConnectRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CONNECT, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.TargetDescriptionResponse describe(idb.Idb.TargetDescriptionRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_DESCRIBE, getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<idb.Idb.LogResponse> log(
        idb.Idb.LogRequest request) {
      return blockingServerStreamingCall(
          getChannel(), METHOD_LOG, getCallOptions(), request);
    }

    /**
     * <pre>
     * Interaction
     * </pre>
     */
    public idb.Idb.AccessibilityInfoResponse accessibilityInfo(idb.Idb.AccessibilityInfoRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_ACCESSIBILITY_INFO, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.FocusResponse focus(idb.Idb.FocusRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_FOCUS, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.OpenUrlRequest openUrl(idb.Idb.OpenUrlRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_OPEN_URL, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.SetLocationResponse setLocation(idb.Idb.SetLocationRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SET_LOCATION, getCallOptions(), request);
    }

    /**
     * <pre>
     * Settings
     * </pre>
     */
    public idb.Idb.ApproveResponse approve(idb.Idb.ApproveRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_APPROVE, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.ClearKeychainResponse clearKeychain(idb.Idb.ClearKeychainRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CLEAR_KEYCHAIN, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.ContactsUpdateResponse contactsUpdate(idb.Idb.ContactsUpdateRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CONTACTS_UPDATE, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.SettingResponse setting(idb.Idb.SettingRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SETTING, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.GetSettingResponse getSetting(idb.Idb.GetSettingRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_SETTING, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.ListSettingResponse listSettings(idb.Idb.ListSettingRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_LIST_SETTINGS, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.ListAppsResponse listApps(idb.Idb.ListAppsRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_LIST_APPS, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.TerminateResponse terminate(idb.Idb.TerminateRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TERMINATE, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.UninstallResponse uninstall(idb.Idb.UninstallRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_UNINSTALL, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.ScreenshotResponse screenshot(idb.Idb.ScreenshotRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SCREENSHOT, getCallOptions(), request);
    }

    /**
     * <pre>
     * Crash Operations
     * </pre>
     */
    public idb.Idb.CrashLogResponse crashDelete(idb.Idb.CrashLogQuery request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CRASH_DELETE, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.CrashLogResponse crashList(idb.Idb.CrashLogQuery request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CRASH_LIST, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.CrashShowResponse crashShow(idb.Idb.CrashShowRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CRASH_SHOW, getCallOptions(), request);
    }

    /**
     * <pre>
     * xctest operations
     * </pre>
     */
    public idb.Idb.XctestListBundlesResponse xctestListBundles(idb.Idb.XctestListBundlesRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_XCTEST_LIST_BUNDLES, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.XctestListTestsResponse xctestListTests(idb.Idb.XctestListTestsRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_XCTEST_LIST_TESTS, getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<idb.Idb.XctestRunResponse> xctestRun(
        idb.Idb.XctestRunRequest request) {
      return blockingServerStreamingCall(
          getChannel(), METHOD_XCTEST_RUN, getCallOptions(), request);
    }

    /**
     * <pre>
     * File Operations
     * </pre>
     */
    public idb.Idb.LsResponse ls(idb.Idb.LsRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_LS, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.MkdirResponse mkdir(idb.Idb.MkdirRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_MKDIR, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.MvResponse mv(idb.Idb.MvRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_MV, getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<idb.Idb.PullResponse> pull(
        idb.Idb.PullRequest request) {
      return blockingServerStreamingCall(
          getChannel(), METHOD_PULL, getCallOptions(), request);
    }

    /**
     */
    public idb.Idb.RmResponse rm(idb.Idb.RmRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_RM, getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The idb companion service definition.
   * </pre>
   */
  public static final class CompanionServiceFutureStub extends io.grpc.stub.AbstractStub<CompanionServiceFutureStub> {
    private CompanionServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CompanionServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CompanionServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CompanionServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Management
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ConnectResponse> connect(
        idb.Idb.ConnectRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CONNECT, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.TargetDescriptionResponse> describe(
        idb.Idb.TargetDescriptionRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_DESCRIBE, getCallOptions()), request);
    }

    /**
     * <pre>
     * Interaction
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.AccessibilityInfoResponse> accessibilityInfo(
        idb.Idb.AccessibilityInfoRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_ACCESSIBILITY_INFO, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.FocusResponse> focus(
        idb.Idb.FocusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_FOCUS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.OpenUrlRequest> openUrl(
        idb.Idb.OpenUrlRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_OPEN_URL, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.SetLocationResponse> setLocation(
        idb.Idb.SetLocationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SET_LOCATION, getCallOptions()), request);
    }

    /**
     * <pre>
     * Settings
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ApproveResponse> approve(
        idb.Idb.ApproveRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_APPROVE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ClearKeychainResponse> clearKeychain(
        idb.Idb.ClearKeychainRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CLEAR_KEYCHAIN, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ContactsUpdateResponse> contactsUpdate(
        idb.Idb.ContactsUpdateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CONTACTS_UPDATE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.SettingResponse> setting(
        idb.Idb.SettingRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SETTING, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.GetSettingResponse> getSetting(
        idb.Idb.GetSettingRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_SETTING, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ListSettingResponse> listSettings(
        idb.Idb.ListSettingRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_LIST_SETTINGS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ListAppsResponse> listApps(
        idb.Idb.ListAppsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_LIST_APPS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.TerminateResponse> terminate(
        idb.Idb.TerminateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TERMINATE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.UninstallResponse> uninstall(
        idb.Idb.UninstallRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_UNINSTALL, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.ScreenshotResponse> screenshot(
        idb.Idb.ScreenshotRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SCREENSHOT, getCallOptions()), request);
    }

    /**
     * <pre>
     * Crash Operations
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.CrashLogResponse> crashDelete(
        idb.Idb.CrashLogQuery request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CRASH_DELETE, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.CrashLogResponse> crashList(
        idb.Idb.CrashLogQuery request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CRASH_LIST, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.CrashShowResponse> crashShow(
        idb.Idb.CrashShowRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CRASH_SHOW, getCallOptions()), request);
    }

    /**
     * <pre>
     * xctest operations
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.XctestListBundlesResponse> xctestListBundles(
        idb.Idb.XctestListBundlesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_XCTEST_LIST_BUNDLES, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.XctestListTestsResponse> xctestListTests(
        idb.Idb.XctestListTestsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_XCTEST_LIST_TESTS, getCallOptions()), request);
    }

    /**
     * <pre>
     * File Operations
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.LsResponse> ls(
        idb.Idb.LsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_LS, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.MkdirResponse> mkdir(
        idb.Idb.MkdirRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_MKDIR, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.MvResponse> mv(
        idb.Idb.MvRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_MV, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<idb.Idb.RmResponse> rm(
        idb.Idb.RmRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_RM, getCallOptions()), request);
    }
  }

  private static final int METHODID_CONNECT = 0;
  private static final int METHODID_DESCRIBE = 1;
  private static final int METHODID_LOG = 2;
  private static final int METHODID_ACCESSIBILITY_INFO = 3;
  private static final int METHODID_FOCUS = 4;
  private static final int METHODID_OPEN_URL = 5;
  private static final int METHODID_SET_LOCATION = 6;
  private static final int METHODID_APPROVE = 7;
  private static final int METHODID_CLEAR_KEYCHAIN = 8;
  private static final int METHODID_CONTACTS_UPDATE = 9;
  private static final int METHODID_SETTING = 10;
  private static final int METHODID_GET_SETTING = 11;
  private static final int METHODID_LIST_SETTINGS = 12;
  private static final int METHODID_LIST_APPS = 13;
  private static final int METHODID_TERMINATE = 14;
  private static final int METHODID_UNINSTALL = 15;
  private static final int METHODID_SCREENSHOT = 16;
  private static final int METHODID_CRASH_DELETE = 17;
  private static final int METHODID_CRASH_LIST = 18;
  private static final int METHODID_CRASH_SHOW = 19;
  private static final int METHODID_XCTEST_LIST_BUNDLES = 20;
  private static final int METHODID_XCTEST_LIST_TESTS = 21;
  private static final int METHODID_XCTEST_RUN = 22;
  private static final int METHODID_LS = 23;
  private static final int METHODID_MKDIR = 24;
  private static final int METHODID_MV = 25;
  private static final int METHODID_PULL = 26;
  private static final int METHODID_RM = 27;
  private static final int METHODID_DEBUGSERVER = 28;
  private static final int METHODID_INSTALL = 29;
  private static final int METHODID_INSTRUMENTS_RUN = 30;
  private static final int METHODID_XCTRACE_RECORD = 31;
  private static final int METHODID_HID = 32;
  private static final int METHODID_LAUNCH = 33;
  private static final int METHODID_ADD_MEDIA = 34;
  private static final int METHODID_RECORD = 35;
  private static final int METHODID_VIDEO_STREAM = 36;
  private static final int METHODID_PUSH = 37;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CompanionServiceImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(CompanionServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CONNECT:
          serviceImpl.connect((idb.Idb.ConnectRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ConnectResponse>) responseObserver);
          break;
        case METHODID_DESCRIBE:
          serviceImpl.describe((idb.Idb.TargetDescriptionRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.TargetDescriptionResponse>) responseObserver);
          break;
        case METHODID_LOG:
          serviceImpl.log((idb.Idb.LogRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.LogResponse>) responseObserver);
          break;
        case METHODID_ACCESSIBILITY_INFO:
          serviceImpl.accessibilityInfo((idb.Idb.AccessibilityInfoRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.AccessibilityInfoResponse>) responseObserver);
          break;
        case METHODID_FOCUS:
          serviceImpl.focus((idb.Idb.FocusRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.FocusResponse>) responseObserver);
          break;
        case METHODID_OPEN_URL:
          serviceImpl.openUrl((idb.Idb.OpenUrlRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.OpenUrlRequest>) responseObserver);
          break;
        case METHODID_SET_LOCATION:
          serviceImpl.setLocation((idb.Idb.SetLocationRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.SetLocationResponse>) responseObserver);
          break;
        case METHODID_APPROVE:
          serviceImpl.approve((idb.Idb.ApproveRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ApproveResponse>) responseObserver);
          break;
        case METHODID_CLEAR_KEYCHAIN:
          serviceImpl.clearKeychain((idb.Idb.ClearKeychainRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ClearKeychainResponse>) responseObserver);
          break;
        case METHODID_CONTACTS_UPDATE:
          serviceImpl.contactsUpdate((idb.Idb.ContactsUpdateRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ContactsUpdateResponse>) responseObserver);
          break;
        case METHODID_SETTING:
          serviceImpl.setting((idb.Idb.SettingRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.SettingResponse>) responseObserver);
          break;
        case METHODID_GET_SETTING:
          serviceImpl.getSetting((idb.Idb.GetSettingRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.GetSettingResponse>) responseObserver);
          break;
        case METHODID_LIST_SETTINGS:
          serviceImpl.listSettings((idb.Idb.ListSettingRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ListSettingResponse>) responseObserver);
          break;
        case METHODID_LIST_APPS:
          serviceImpl.listApps((idb.Idb.ListAppsRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ListAppsResponse>) responseObserver);
          break;
        case METHODID_TERMINATE:
          serviceImpl.terminate((idb.Idb.TerminateRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.TerminateResponse>) responseObserver);
          break;
        case METHODID_UNINSTALL:
          serviceImpl.uninstall((idb.Idb.UninstallRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.UninstallResponse>) responseObserver);
          break;
        case METHODID_SCREENSHOT:
          serviceImpl.screenshot((idb.Idb.ScreenshotRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.ScreenshotResponse>) responseObserver);
          break;
        case METHODID_CRASH_DELETE:
          serviceImpl.crashDelete((idb.Idb.CrashLogQuery) request,
              (io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse>) responseObserver);
          break;
        case METHODID_CRASH_LIST:
          serviceImpl.crashList((idb.Idb.CrashLogQuery) request,
              (io.grpc.stub.StreamObserver<idb.Idb.CrashLogResponse>) responseObserver);
          break;
        case METHODID_CRASH_SHOW:
          serviceImpl.crashShow((idb.Idb.CrashShowRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.CrashShowResponse>) responseObserver);
          break;
        case METHODID_XCTEST_LIST_BUNDLES:
          serviceImpl.xctestListBundles((idb.Idb.XctestListBundlesRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.XctestListBundlesResponse>) responseObserver);
          break;
        case METHODID_XCTEST_LIST_TESTS:
          serviceImpl.xctestListTests((idb.Idb.XctestListTestsRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.XctestListTestsResponse>) responseObserver);
          break;
        case METHODID_XCTEST_RUN:
          serviceImpl.xctestRun((idb.Idb.XctestRunRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.XctestRunResponse>) responseObserver);
          break;
        case METHODID_LS:
          serviceImpl.ls((idb.Idb.LsRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.LsResponse>) responseObserver);
          break;
        case METHODID_MKDIR:
          serviceImpl.mkdir((idb.Idb.MkdirRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.MkdirResponse>) responseObserver);
          break;
        case METHODID_MV:
          serviceImpl.mv((idb.Idb.MvRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.MvResponse>) responseObserver);
          break;
        case METHODID_PULL:
          serviceImpl.pull((idb.Idb.PullRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.PullResponse>) responseObserver);
          break;
        case METHODID_RM:
          serviceImpl.rm((idb.Idb.RmRequest) request,
              (io.grpc.stub.StreamObserver<idb.Idb.RmResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DEBUGSERVER:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.debugserver(
              (io.grpc.stub.StreamObserver<idb.Idb.DebugServerResponse>) responseObserver);
        case METHODID_INSTALL:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.install(
              (io.grpc.stub.StreamObserver<idb.Idb.InstallResponse>) responseObserver);
        case METHODID_INSTRUMENTS_RUN:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.instrumentsRun(
              (io.grpc.stub.StreamObserver<idb.Idb.InstrumentsRunResponse>) responseObserver);
        case METHODID_XCTRACE_RECORD:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.xctraceRecord(
              (io.grpc.stub.StreamObserver<idb.Idb.XctraceRecordResponse>) responseObserver);
        case METHODID_HID:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.hid(
              (io.grpc.stub.StreamObserver<idb.Idb.HIDResponse>) responseObserver);
        case METHODID_LAUNCH:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.launch(
              (io.grpc.stub.StreamObserver<idb.Idb.LaunchResponse>) responseObserver);
        case METHODID_ADD_MEDIA:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.addMedia(
              (io.grpc.stub.StreamObserver<idb.Idb.AddMediaResponse>) responseObserver);
        case METHODID_RECORD:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.record(
              (io.grpc.stub.StreamObserver<idb.Idb.RecordResponse>) responseObserver);
        case METHODID_VIDEO_STREAM:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.videoStream(
              (io.grpc.stub.StreamObserver<idb.Idb.VideoStreamResponse>) responseObserver);
        case METHODID_PUSH:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.push(
              (io.grpc.stub.StreamObserver<idb.Idb.PushResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    return new io.grpc.ServiceDescriptor(SERVICE_NAME,
        METHOD_CONNECT,
        METHOD_DEBUGSERVER,
        METHOD_DESCRIBE,
        METHOD_INSTALL,
        METHOD_INSTRUMENTS_RUN,
        METHOD_LOG,
        METHOD_XCTRACE_RECORD,
        METHOD_ACCESSIBILITY_INFO,
        METHOD_FOCUS,
        METHOD_HID,
        METHOD_OPEN_URL,
        METHOD_SET_LOCATION,
        METHOD_APPROVE,
        METHOD_CLEAR_KEYCHAIN,
        METHOD_CONTACTS_UPDATE,
        METHOD_SETTING,
        METHOD_GET_SETTING,
        METHOD_LIST_SETTINGS,
        METHOD_LAUNCH,
        METHOD_LIST_APPS,
        METHOD_TERMINATE,
        METHOD_UNINSTALL,
        METHOD_ADD_MEDIA,
        METHOD_RECORD,
        METHOD_SCREENSHOT,
        METHOD_VIDEO_STREAM,
        METHOD_CRASH_DELETE,
        METHOD_CRASH_LIST,
        METHOD_CRASH_SHOW,
        METHOD_XCTEST_LIST_BUNDLES,
        METHOD_XCTEST_LIST_TESTS,
        METHOD_XCTEST_RUN,
        METHOD_LS,
        METHOD_MKDIR,
        METHOD_MV,
        METHOD_PULL,
        METHOD_PUSH,
        METHOD_RM);
  }

}
